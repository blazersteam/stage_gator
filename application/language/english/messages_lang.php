<?php
if (! defined('BASEPATH'))
    exit('No direct script access allowed');
$lang ['stagegator'] = stripcslashes('StageGator');
$lang ['contact_developer'] = stripcslashes('Please contact Developer for this Action.');
$lang ['forbidden_function'] = stripcslashes('You are not allowed to perform this action.');
$lang ['forbidden_access'] = stripcslashes('You are not allowed to perform this operation.');
$lang ['error_default_error'] = stripcslashes('We are unable to process your request. Please refresh page and try again.');

/* login realted messages */
$lang ['user_authentication'] = stripcslashes('Username and password is incorrect');
$lang ['invalid_email'] = stripcslashes('Email you have entered not valid email');
$lang ['user_facebook_authentication'] = stripcslashes('We are anable to proccess your request please try again');
$lang ['login_facebook'] = stripcslashes('You have Successfull logged in with facebook');
$lang ['reset_password_failed'] = stripcslashes('Email Address you entered is not Valid');

/* password related messages */
$lang ['reset_password_success'] = stripcslashes('Please check your email to reset password');
$lang ['token_expired'] = stripcslashes('Your reset password link has been expired please request again');
$lang ['invali_url'] = stripcslashes('This URL is invalid');
$lang ['error_please_enter_email_address'] = stripcslashes('Please enter email address');
$lang ['error_please_enter_valid_email_address'] = stripcslashes('Please enter valid email address');
$lang ['error_email_address_maxlength'] = stripcslashes('You have crossed the email address input limit');
$lang ['error_please_enter_password'] = stripcslashes('Please enter the password');
$lang ['error_no_space_allowed'] = stripcslashes('Space not allowed in password');
$lang ['error_min_length_password'] = stripcslashes('Minimum password length should be greater than 6 characters');
$lang ['error_max_length_password'] = stripcslashes('Maximum password length less than 32 characters');
$lang ['error_confirm_password_notmatched'] = stripcslashes('Password and confirm password does not match.');
$lang ['error_please_enter_a_valid_phone_number'] = stripcslashes('Please enter a valid phone number');
$lang ['password_changed_successfully'] = stripcslashes('Your password has been updated successfully');
$lang ['token_invali_url'] = stripcslashes('Invalid request or url please send a new request to change password');
$lang ['token_expired'] = stripcslashes('Your request time has been expired please send a new request to change password');

/* user locked related message */
$lang ['user_account_locked'] = stripslashes('Due to too many invalid request your account has been locked. Kinldy forgot your password request to unlock your account');
$lang ['user_account_request_complete'] = stripslashes('Too many invalid try to login. Our system has locked your account for security resason. Kinldy forgot your password request to unlock your account.');
$lang ['user_deactive'] = stripcslashes('We apologize that your account has not been activated. Use the LiveChat feature below! Your message will go straight to the Lead Developer who will activate your account ASAP!');
$lang ['user_suspend'] = stripcslashes('Account Suspended !');

/* labes for login view */

$lang ['i_m_new_performer'] = stripcslashes("I am a New Performer");
$lang ['i_m_new_venue'] = stripcslashes("I am a New Venue");
$lang ['i_m_new_fan'] = stripcslashes("I am a New Fan");
$lang ['sign_in'] = stripcslashes("Sign In");
$lang ['i_m_new'] = stripcslashes("I'm New");
$lang ['log_in_with_facebook'] = stripcslashes("Login with Facebook");
$lang ['forgot_password'] = stripcslashes("Forgot Password");
$lang ['account_activated'] = stripcslashes('Your account has been activated successfully. Please Login to continue');
$lang ['account_activated_already'] = stripcslashes('You have already activated you acccount. Please Login to continue');

/* lable for login_reset_password.php */
$lang ['new_password'] = stripcslashes("New Password");
$lang ['confirm_password'] = stripcslashes("Confirm Password");
$lang ['change_password'] = stripcslashes("Change Password");
$lang ["lable_resetpassowrd_page_heading"] = stripcslashes("Reset Password");

/* lable for forget_password.php */
$lang ['reset_password'] = stripcslashes("Reset Password");
$lang ['error_no_space_allowed'] = stripcslashes('Space not allowed in password');
$lang ['error_please_enter_password'] = stripcslashes('Please enter the password');
$lang ['error_no_space_allowed'] = stripcslashes('Space not allowed in password');

/* lable for register_signup_view.php */
$lang ['register_new_account'] = stripcslashes('Create New Account');
$lang ['register_venue'] = stripcslashes('Venue/Agency Title');
$lang ['register_name'] = stripcslashes('Name');
$lang ['register_contact_person'] = stripcslashes('Name');
$lang ['register_account_type'] = stripcslashes('Account Type');
$lang ['register_phone'] = stripcslashes('Phone');
$lang ['register_email_address'] = stripcslashes('Email Address');
$lang ['register_password'] = stripcslashes('Password');
$lang ['register_confirm_password'] = stripcslashes('Confirm Password');
$lang ['register_verify_that_you_are_a_human'] = stripcslashes('Verify that you are a human');
$lang ['register_submit'] = stripcslashes('Submit');
$lang ['register_already_have_an_account'] = stripcslashes('Already have an account');
$lang ['error_please_enter_mobile'] = stripslashes('Please enter a valid mobile number');
$lang ['error_please_select_a_usertype'] = stripslashes('Please select a user type');
$lang ['error_please_enter_user_name'] = stripslashes('Please enter your name');
$lang ['error_please_enter_venue_title'] = stripslashes('Please enter venue name or title');
$lang ['please_select_a_valid_captcha'] = stripslashes('Please verify captcha');
$lang ['register_successfull'] = stripslashes('Account created successfully! Check your e-mail inbox and spam folder for a verification link from StageGator!');
$lang ['register_email_exists'] = stripslashes('This email is already exist');

/* Side menue */
$lang ['home'] = stripcslashes('HOME');
$lang ['terms_and_conditions'] = stripcslashes('TERMS AND CONDITIONS');
$lang ['privacy_policy'] = stripcslashes(' PRIVACY POLICY ');
$lang ['contact_us'] = stripcslashes('CONTACT US');
$lang ['update_profile'] = stripcslashes('EDIT/UPDATE PROFILE');
$lang ['overview'] = stripcslashes(' OVERVIEW');
$lang ['profile'] = stripcslashes('PROFILE');
$lang ['locations'] = stripcslashes('LOCATIONS');
$lang ['change_image'] = stripcslashes('Change Image');
$lang ['change_passsword'] = stripcslashes('CHANGE PASSWORD');
$lang ['show_manager'] = stripcslashes('SHOW MANAGER');
$lang ['create_a_show'] = stripcslashes('CREATE A SHOW');
$lang ['create_a_series'] = stripcslashes('CREATE A SERIES');
$lang ['manage_schedule'] = stripcslashes('MANAGE SCHEDULE');
$lang ['booking_requests_received'] = stripcslashes('BOOKING REQUESTS RECEIVED');
$lang ['booking_invitations_sent'] = stripcslashes('BOOKING INVITATIONS SENT');
$lang ['host_menu'] = stripcslashes('HOST MENU');
$lang ['search_for_performers'] = stripcslashes('SEARCH FOR PERFORMERS');
$lang ['view_bites'] = stripcslashes('VIEW_BITES');
$lang ['contact_us'] = stripcslashes('CONTACT US');
$lang ['bite_performers'] = stripcslashes('Bite Performers');
$lang ['search_for_shows'] = stripcslashes('Search for shows');
$lang ['event_line_ups'] = stripcslashes('Event line-ups');
$lang ['hosting_invitations'] = stripcslashes('Hosting invitations');
$lang ['trading_requests'] = stripcslashes('Trading requests');
$lang ['awaiting_approval_from_venue'] = stripcslashes('Awaiting approval from venue');
$lang ['manage_booking_requests'] = stripcslashes('Manage booking requests');
$lang ['my_shows'] = stripcslashes('My shows');
$lang ['new_host_invitation'] = stripcslashes('Host Requests');
$lang ['profile_overview'] = stripcslashes('Profile Overview');
$lang ['invite_performer_for_event'] = stripcslashes('Invite Performer For Event');
/* Header menu */
$lang ['my_profile'] = stripcslashes('Profile Overview');
$lang ['my_inbox'] = stripcslashes('My Inbox');
$lang ['settings'] = stripcslashes('Settings');
$lang ['help'] = stripcslashes('Help');
$lang ['delete_account'] = stripcslashes('Delete Account');
$lang ['logout'] = stripcslashes('Logout');

/* venue_overview_view.php lables */
$lang ['address'] = stripcslashes('Address');
$lang ['contact_person'] = stripcslashes('Contact Person');
$lang ['upcoming_shows'] = stripcslashes('Upcoming Shows');
$lang ['event_title'] = stripcslashes('Event Title');
$lang ['venue'] = stripcslashes('Venue');
$lang ['event_date'] = stripcslashes('Event Date');
$lang ['start_time'] = stripcslashes('Start Time');
$lang ['end_time'] = stripcslashes('End Time');

/* email tamplate related lable */
$lang ['email_subject_reset_your_password'] = stripcslashes('Reset your password');
$lang ['email_subject_stageGator_account_confirmation'] = stripcslashes('StageGator Account Confirmation');

/* venue_update_profile_view.php */
$lang ['profile_name'] = stripcslashes('Profile Name');
$lang ['email'] = stripcslashes('Email');
$lang ['style'] = stripcslashes('Style');

// $lang['register_phone'] get phone above*/
$lang ['country'] = stripcslashes('Country');
$lang ['state'] = stripcslashes('State');
$lang ['city'] = stripcslashes('City');
// $lang['address'] = stripcslashes('Address');
$lang ['url'] = stripcslashes('Url');
$lang ['description'] = stripcslashes('Description');
$lang ['save_changes'] = stripcslashes('Save Changes');
$lang ['cancle'] = stripcslashes('Cancle');
$lang ['profile_management'] = stripcslashes('Profile Management');
$lang ['profile_3'] = stripcslashes('Performer Information');
$lang ['profile_2'] = stripcslashes('Venue Information');
$lang ['profile_4'] = stripcslashes('Fan Information');
$lang ['cancel'] = stripslashes('Cancel');
/* update profile validation realted validations */
$lang ['error_select_your_style'] = stripslashes('Please select any style');
$lang ['error_select_country'] = stripslashes('Please select country');
$lang ['error_select_state'] = stripslashes('Please select state');
$lang ['error_select_city'] = stripslashes('Please select City');
$lang ['error_enter_address'] = stripslashes('Please enter you address');
$lang ['error_invalid_url'] = stripslashes('This url is not valid');
$lang ['profile_updated_successfully'] = stripslashes('Profile updated successfully');

/* Change password and emails change_password.php */

$lang ['change_account_credentials'] = stripslashes('Change Account Credentials');
$lang ['change_email_address'] = stripslashes('Change email address');
$lang ['new_email_address'] = stripslashes('New email address');
$lang ['current_password'] = stripslashes('Current password');
$lang ['new_password'] = stripslashes('New password');
$lang ['confirm_new_password'] = stripslashes('Confirm new password');
$lang ['oldpasswordCheck'] = stripslashes('Your old password did not matched');
$lang ['change_password'] = stripslashes('Change password');
$lang ['email_update_successfully'] = stripslashes('Your request for change email address accepted successfully, Please verify your email address ');
$lang ['confirmation_link'] = stripslashes('Confirmation Link');
$lang ['email_has_been_changed'] = stripslashes('You email has been changed. Please login with your new email');

/* venue overview message */

$lang ['add_your_profile_pic_and_help'] = stripslashes('Add your profile pic and help venues book you for your next gig!');
$lang ['new_image'] = stripslashes('New image');
$lang ['new_image_extantion'] = stripslashes('PNG, JPG, JPEG Only');

/* location related lables location_my_location_view.php */
$lang ['actions'] = stripslashes('Actions');
$lang ['title'] = stripslashes('Title');
$lang ['number_of_seats'] = stripcslashes('Occupancy');
$lang ['edit'] = stripcslashes('Edit');
$lang ['delete'] = stripcslashes('Delete');
$lang ['add_location'] = stripslashes('Add Location');
$lang ['my_locations'] = stripslashes('My Locations');
$lang ['add_new_location'] = stripslashes('Add New Location');
/* location related lables location_form_view.php */
$lang ['venue_name'] = stripslashes('Venue Name');
$lang ['address'] = stripcslashes('Address');
$lang ['save_location'] = stripcslashes('Save Location');
$lang ['edit_location'] = stripcslashes('Edit Location');
$lang ['error_number_of_seats'] = stripcslashes('Please select number of seats');
$lang ['error_address'] = stripcslashes('Please enter your address');
$lang ['location_added_successfully'] = stripcslashes('Location added successfully');
$lang ['location_update_successfully'] = stripcslashes('Location updated successfully');
$lang ['location_deleted_successfully'] = stripcslashes('Location deleted successfully');
$lang ['tooltip_location'] = stripcslashes('For our mapping functionality to work properly, please input your full address in the following format: Street Address, City, State, ZIP');

/* event module realated messages event_create_event_view.php */
$lang ['tooltip_the_create'] = stripslashes('The "Create A Show" feature is a full service tool for any show organizer. Shows like recurring open mics or a series with no end date are perfect for this feature. If a venue wants to create a recurring series of events with a set end date and promote these shows over the span of the series, the "Create A Series" feature perfectly suits your needs as a booker.');
$lang ['location'] = stripcslashes('Location');
$lang ['event_type'] = stripcslashes('Event type');
$lang ['event_type'] = stripcslashes('Event type');
$lang ['open_mic'] = stripcslashes('Open mic');
$lang ['showcase'] = stripcslashes('Show case');
$lang ['date'] = stripcslashes('Date');
$lang ['recurring'] = stripcslashes('Recurring');
$lang ['start_at'] = stripcslashes('Start at');
$lang ['end_at'] = stripcslashes('End at');
$lang ['re_invite'] = stripcslashes('Re-Invite');
$lang ['ranked'] = stripcslashes('Ranked');
$lang ['yes'] = stripcslashes('Yes');
$lang ['no'] = stripcslashes('No');
$lang ['lock_event'] = stripcslashes('Lock Event?');
$lang ['allow_walk_in'] = stripcslashes('Allow Walk-In?');
$lang ['cut_off_time'] = stripcslashes('Cut-off Time');
$lang ['performer_can_pick_time'] = stripcslashes('Performer Can Pick Time');
$lang ['schedule_algorithm'] = stripcslashes('Schedule Algorithm');
$lang ['adjust_from_hightest_to_lowest_ranking'] = stripcslashes('Adjust from Hightest to Lowest Ranking');
$lang ['adjust_from_lowest_to_highest_ranking'] = stripcslashes('Adjust from Lowest to Highest Ranking');
$lang ['shuffle_list'] = stripcslashes('Shuffle List');
$lang ['allow_host'] = stripcslashes('Allow host');
$lang ['allow_trading'] = stripcslashes('Allow trading');
$lang ['time_per_performance'] = stripcslashes('Time Per Performance');
$lang ['custom_time_allowed'] = stripcslashes('Custom-Time Allowed');
$lang ['accept_all_performer_requests'] = stripcslashes('Accept All Performer Requests');
$lang ['automatic_reminder_e_mails'] = stripcslashes('Automatic Reminder E-mails');
$lang ['automatic_reminder_e_mails'] = stripcslashes('Automatic Reminder E-mails');
$lang ['never'] = stripcslashes('Never');
$lang ['every_24_hours'] = stripcslashes('Every 24 Hours');
$lang ['every_48_hours'] = stripcslashes('Every 48 Hours');
$lang ['create_event'] = stripcslashes(' Create_event');
$lang ['one_time'] = stripcslashes('One time');
$lang ['daily'] = stripcslashes('Daily');
$lang ['weekly'] = stripcslashes('Weekly');
$lang ['monthly'] = stripcslashes('Monthly');
$lang ['start_date'] = stripcslashes('Start date');
$lang ['end_date'] = stripcslashes('End date');
$lang ['select_days'] = stripcslashes('Select days');
$lang ['first'] = stripcslashes('First');
$lang ['second'] = stripcslashes('Second');
$lang ['third'] = stripcslashes('Third');
$lang ['fourth'] = stripcslashes('Fourth');
$lang ['saturday'] = stripcslashes('Saturday');
$lang ['sunday'] = stripcslashes('Sunday');
$lang ['monday'] = stripcslashes('Monday');
$lang ['tuesday'] = stripcslashes('Tuesday');
$lang ['wednesday'] = stripcslashes('Wednesday');
$lang ['thursday'] = stripcslashes('Thursday');
$lang ['friday'] = stripcslashes('Friday');
$lang ['select_by_day'] = stripcslashes('Select by day of the week');
$lang ['select_by_day2'] = stripcslashes('Select by day of the month');
$lang ['repeating_every'] = stripcslashes('Repeating every');
$lang ['create_a'] = stripcslashes('Create a');
$lang ['show'] = stripcslashes('Show');
$lang ['series'] = stripcslashes('Series');

$lang ['show_title'] = stripcslashes('Show Title');
$lang ['trading_request'] = stripcslashes('Trading Request');
$lang ['have_host'] = stripcslashes('Have Host');
$lang ['your_bites'] = stripcslashes('Your Bites');

$lang ['error_please_select_type_of_event'] = stripcslashes('Please select type of event');
$lang ['error_please_enter_name'] = stripcslashes('Please enter name');
$lang ['error_please_enter_your_location'] = stripcslashes('Please enter your location');
$lang ['error_please_select_a_event_type'] = stripcslashes('Please select a event type');
$lang ['error_please_select_a_style'] = stripcslashes('Please select a style');
$lang ['error_please_select_valid_style'] = stripcslashes('Please select a valid style');
$lang ['error_please_select_recurrin_type'] = stripcslashes('Please select recurring type');
$lang ['error_please_select_a_valid_recurring_type'] = stripcslashes('Please select a valid recurring type');
$lang ['error_please_select_start_date'] = stripcslashes('Please select start date');
$lang ['error_please_select_end_date'] = stripcslashes('Please select end date');
$lang ['error_please_select_end_date'] = stripcslashes('Please select end date');
$lang ['error_please_select_days'] = stripcslashes('Please select days');
$lang ['error_please_select_days_in_month'] = stripcslashes('Please select days of month');
$lang ['error_please_select_recurrin_days'] = stripcslashes('Please select recurring days');
$lang ['error_please_select_start_time'] = stripcslashes('Please select strat time');
$lang ['error_please_select_end_time'] = stripcslashes('Please select end time');
$lang ['error_please_select_re_invite_option'] = stripcslashes('please select re invite option');
$lang ['error_please_select_a_rank'] = stripcslashes('Please select a rank');
$lang ['error_please_select_allow_walk_in'] = stripcslashes('Please select allow walk in');
$lang ['error_please_select_lock_status'] = stripcslashes('Please select lock status');
$lang ['error_please_select_cut_off_time'] = stripcslashes('Please select cut off time');
$lang ['error_please_select_performer_can_pick_time'] = stripcslashes('Please select performer can pick time ?');
$lang ['error_please_select_schedule_algorithm'] = stripcslashes('Please select schedule algorithm ?');
$lang ['error_please_select_allow_host'] = stripcslashes('Please select allow host option');
$lang ['error_please_select_allow_host'] = stripcslashes('Please select allow host option');
$lang ['error_please_select_allow_trading'] = stripcslashes('Please select allow trading');
$lang ['error_please_select_time_per_performance'] = stripcslashes('Please select time per performance');
$lang ['error_please_select_custome_time_allowed'] = stripcslashes('Please select custom time allowed');
$lang ['error_please_select_accept_all_performer_request'] = stripcslashes('Please select accept all performer request');
$lang ['error_please_select_automatic_reminder_e_mails'] = stripcslashes('Please select automatic reminder e mails');
$lang ['event_created_successfully'] = stripslashes('Event created Successfully');
$lang ['event_updated_successfully'] = stripslashes('Event updated Successfully');
$lang ['event_deleted_successfully'] = stripslashes('Event deleted Successfully');
$lang ['event_schedule_deleted_successfully'] = stripslashes('Event Schedule deleted Successfully');
$lang ['create_new_show'] = stripslashes('Create New Show');
$lang ['manage_schedule_series_show'] = stripslashes('Manage Show/Series');
$lang ['search_performers'] = stripslashes('Search Performers');
$lang ['user_unbitten'] = stripslashes('User unbitten.');
$lang ['user_bitten'] = stripslashes('User bitten.');
$lang ['manage_schedule2'] = stripslashes('Manage Schedule');
$lang ['booking_invitations_sent'] = stripslashes('Booking Invitations Sent');
$lang ['reminder_sent_successfully'] = stripslashes('Reminder Sent Successfully !');
$lang ['invite_could_not_be_cancelled'] = stripslashes('Invite could not be Cancelled');
$lang ['invite_cancelled_successfully'] = stripslashes('Invite Cancelled Successfully');
$lang ['error_please_select_timezone'] = stripcslashes('Please select end timezone');
$lang ['profile2'] = stripcslashes('Profile');
$lang ['booking_requests_received'] = stripcslashes('Booking Requests Received');
$lang ['request_approved_successfully'] = stripcslashes('Request Approved Successfully');
$lang ['request_could_not_be_approved'] = stripcslashes('Request could not be Approved');
$lang ['request_rejected_successfully'] = stripcslashes('Request rejected successfully');
$lang ['request_could_not_be_rejected'] = stripcslashes('Request could_not be rejected');

$lang ['create_event'] = stripcslashes('Create Event');

/* tooltips */
$lang ['tooltip_the_name'] = stripcslashes('The Name of your Event');
$lang ['tooltip_what_type'] = stripcslashes('What type of performances should potential attendees expect from your show');
$lang ['tooltip_the_day'] = stripcslashes('The day your event will occur');
$lang ['tooltip_only_applies'] = stripcslashes('Only applies to recurring events. Automatically sends to all performers that were invited to your previous event, an invitation to your upcoming show.');
$lang ['tooltip_if_you'] = stripcslashes("If you'd like the ability to rate your performers after the show, select 'Yes'. Otherwise, select 'No'. Rating performers gives the hardest working, most original artists StageGator has to offer top spots on the website's search pages. Ranking performers is a fundamental part ofwhat makes StageGator work for both venues and performers.");
$lang ['tooltip_if_you_d'] = stripcslashes("If you'd like performers to be able to sign-up for your event after it starts, select 'Yes'. Otherwise, select 'No'.");
$lang ['tooltip_no_more'] = stripcslashes("No more performers will be allowed to apply for the event after the event locks at the time of your choosing");
$lang ['tooltip_type_in'] = stripcslashes("Type in how many hours prior to the event start time the event will be locked (Integer Allowed)");
$lang ['tooltip_can_the'] = stripcslashes("Can the performers pick their position in the line-up order");
$lang ['tooltip_will_this'] = stripcslashes("Will this show have a host");
$lang ['tooltip_can_performers'] = stripcslashes("Can performers on your line-up trade positions in the show order");
$lang ['tooltip_how_many'] = stripcslashes("How many minutes of stage time will each performer get? (Integer Allowed). You can always set custom time for performance, see help for more details.");
$lang ['tooltip_you_can'] = stripcslashes("You can assign custom time to any performer, see help for more information");
$lang ['tooltip_automatically_accept'] = stripcslashes("Automatically accept all performer requests");
$lang ['tooltip_what_should'] = stripcslashes("What should interested potential audience members expect from your show");
$lang ['tooltip_is_your'] = stripcslashes("Is your event one-time or will it be occurring regularly in the future");
$lang ['tooltip_when_will'] = stripcslashes("When will your event start");
$lang ['tooltip_when_will2'] = stripcslashes("When will your event be over");

/* bulk inviate */
$lang ['bulk_invite'] = stripcslashes("Bulk Invite");
$lang ['un_authorized_access'] = stripcslashes("Un-Authorized Access!");
$lang ['performers_successfully_invited'] = stripcslashes('Performers Successfully Invited');
$lang ['this_performer_already_exists_in_the_registration_queue'] = stripcslashes('This performer already exists in the registration queue!');

$lang ['error_norecords_found'] = stripcslashes('No records found');
$lang ["error_record_not_updated"] = 'Record Not Updated!';
$lang ["warning_gap_between_performers"] = "There are spaces in between performers. These spots will collapse and performers will be bumped to earlier spots, in order to prevent gaps in between performances!";

/* host invitation */
$lang ['host_request'] = stripcslashes('Host Requests');
$lang ['host_management'] = stripcslashes('Host Management');
$lang ['request_could_not_be_rejected'] = stripcslashes('Request could not be Rejected');
$lang ['request_rejected_successfully'] = stripcslashes('Request Rejected Successfully');
$lang ['request_accepted_successfully'] = stripcslashes('Request Accepted Successfully');
$lang ['request_could_not_be_accepted'] = stripcslashes('Request could not be Accepted');
$lang ['access_revoked_successfully'] = stripcslashes('Access revoked Successfully');
$lang ['access_not_revoked_contact_administrator'] = stripcslashes('Access not Revoked. Please contact system administrator');
$lang ['record_updated_successfully'] = stripcslashes('Record Updated Successfully');
$lang ['host_invited_successfully'] = stripcslashes('Host Invited Successfully');
$lang ['host_already_selected_for_show'] = stripcslashes('Host Already Selected for the Show!');
$lang ["warning_gap_between_performers"] = "There are spaces in between performers. These spots will collapse and performers will be bumped to earlier spots, in order to prevent gaps in between performances!";
$lang ["error_record_not_updated"] = stripcslashes('Record Not Updated!');
$lang ["record_updated_successfully"] = stripcslashes('Record Updated Successfully');
$lang ["setting_updated"] = stripcslashes('Settings Updated Successfully');
$lang ["account_deleted_successfully"] = stripcslashes('Account deleted successfully');
$lang ['manage_hosts'] = stripcslashes('Manage Hosts');
$lang ["performer_stream"] = stripcslashes("Performer Stream");
$lang ["venue_stream"] = stripcslashes("Venue Stream");
$lang ['book_a_host'] = stripcslashes('Book a Host');
$lang ['view_bites'] = stripcslashes('View Bites');

/* profile url */
$lang ['allowed_alphanumeric_underscore_dash'] = stripcslashes('allowed only alphanumeric, underscore, dash characters');
$lang ['url_already_exist_enter_another'] = stripcslashes('this url already exist, please enter another');

$lang ['max_30_character_allowed'] = stripcslashes('maximum 30 characters allowed');

$lang ['error_not_found_event_detail'] = stripcslashes('Sorry! We are unable to find your requested show details.');

$lang ['max_250_character_allowed'] = stripcslashes('maximum 250 characters allowed');
$lang ['max_150_character_allowed'] = stripcslashes('maximum 150 characters allowed');
$lang ['max_5_character_allowed'] = stripcslashes('maximum 5 characters allowed');

$lang ['edit_event'] = stripcslashes('Edit Event');

$lang ['delete_message'] = stripcslashes('Deleting account will delete all your data from stagegator account and that can not be restored! Are you sure you want to continue?');

$lang ['tip_history'] = stripcslashes('Tip History');

$lang ['payment_history'] = stripcslashes('Payment History');

$lang ['success_tip_message'] = stripcslashes('Thanks for giving tip.');
$lang ['panding_tip_message'] = stripcslashes('Your transaction is under review. We will update you by email.');

$lang ['tooltip_wtihdrawal_bank_card'] = stripcslashes('You will receive your amount in requested card / bank.');

$lang ['success_delete_card'] = stripcslashes('Your card has been deleted successfully.');
$lang ['can_not_delete_all_external_account'] = stripcslashes('You can not delete all Bank/Card details.');
$lang ['success_delete_external_account'] = stripcslashes('Account has deleted successfully.');

$lang ['bites'] = stripcslashes('Bites');
$lang ['bite_to_me'] = stripcslashes('Bite to me');
$lang ['bitten_me'] = stripcslashes('Bitten me');

$lang ['msg_bite_plan_upgrade'] = stripcslashes('Someone has bitten you, Please upgrade your plan to know who has bitten you!');
$lang ['email_block_msg'] = stripcslashes('You can not do registaration with temporary email account.');
$lang ['error_payment_request']= stripcslashes('Your session has expired.');
$lang ['invalid_user'] = stripcslashes('Invalid User.');
$lang ['error_empty_access_level'] = stripcslashes('You have blank/empty access level');
$lang ['invalid_authentication_error'] = stripcslashes('you are not authorized to login permission');

$lang ['profile_description_label'] = stripcslashes('Tell us about yourself');
$lang ['venue_profile_description_label'] = stripcslashes('Venue Details');
$lang ['already_recurring_tip_message'] = stripcslashes('You already given tip to this performer using recurring method as per given below information.');
$lang ['error_strip_subscription_plan.'] = stripcslashes('Error for subscription to plan.');
$lang ['error_strip_create_plan.'] = stripcslashes('Error for creating plan.');
$lang ['error_strip_create_customer.'] = stripcslashes('Error for register customer to stripe.');
$lang ['enabled_recurring_tip'] = stripcslashes('Managing recurring tip');
$lang ['recurring_tip'] = stripcslashes('Recurring tip');
$lang ['success_cancel'] = stripcslashes('Succesfully cancelled subscription plan.');
$lang ['danger_cancel'] = stripcslashes('Not cancelled subscription plan.');
$lang ['my_current_location'] = stripcslashes('Use My Current Location');


