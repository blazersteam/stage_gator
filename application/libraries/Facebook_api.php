<?php

class Facebook_api
{
    public $fbvar = "";
    public $helper = "";

    /**
     * Used for set the facebook object
     */
    public function __construct()
    {
        require_once APPPATH . 'third_party/php-graph-sdk-facebook/src/Facebook/autoload.php';
        $this->fbvar = new Facebook\Facebook([
            'app_id' => FACEBOOK_APP_ID, // Replace {app-id} with your app id
            'app_secret' => FACEBOOK_APP_SECRET,
            'default_graph_version' => 'v2.2'
        ]);
    }

    /**
     * Used for connect with facebook , this function will generate the url for facebook login
     *
     * @param string $url, redirect url from we want to after login
     * @param string $permissionsArray, permissoin parameter for facebook login
     *       
     * @return string $loginUrl, generated url for facebook login
     */
    public function connectFacebook($url, $permissionsArray)
    {
        $this->helper = $this->fbvar->getRedirectLoginHelper();
        $loginUrl = $this->helper->getLoginUrl($url, $permissionsArray);
        return $loginUrl;
    }

    /**
     * Used for get the facebook user information after the logged in with facebook
     *
     * @return array $returnData, that conatin the user's data & error_msg
     */
    public function getUser()
    {
        $returnData ['user'] = '';
        $returnData ['error_msg'] = '';
        $accesToken = '';
        $this->helper = $this->fbvar->getRedirectLoginHelper();
        try {
            $accessToken = $this->helper->getAccessToken();
        }
        catch (Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            $returnData ['error_msg'] = 'Facebook Graph returned an error: ' . $e->getMessage();
            return $returnData;
        }
        catch (Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            $returnData ['error_msg'] = 'Facebook SDK returned an error: ' . $e->getMessage();
            return $returnData;
        }
        
        /* if (! empty($accesToken)) { */
        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $this->fbvar->getOAuth2Client();
        
        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        
        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId(FACEBOOK_APP_ID); // Replace {app-id} with your app id
        
        $tokenMetadata->validateExpiration();
        
        if (! $accessToken->isLongLived()) {
            // Exchanges a short-lived access token for a long-lived one
            try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            }
            catch (Facebook\Exceptions\FacebookSDKException $e) {
                $returnData ['error_msg'] = "Error getting long-lived access token: " . $helper->getMessage();
                return $returnData;
            }
            $accessToken = $accessToken->getValue();
        }
        
        $accessToken = (string) $accessToken;
        
        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $this->fbvar->get('/me?fields=id,name,email,gender,location,about,birthday,locale,picture.width(240).height(240),link', $accessToken);
        }
        catch (Facebook\Exceptions\FacebookResponseException $e) {
            $returnData ['error_msg'] = 'Graph returned an error: ' . $e->getMessage();
            return $returnData;
        }
        catch (Facebook\Exceptions\FacebookSDKException $e) {
            $returnData ['error_msg'] = 'Facebook SDK returned an error: ' . $e->getMessage();
            return $returnData;
        }
        
        $UserInfo = $response->getGraphUser();
        
        // make the user info array & return
        if (! empty($UserInfo)) {
            $returnData ['user'] ['id'] = $UserInfo ['id'];
            $returnData ['user'] ['name'] = $UserInfo ['name'];
            $returnData ['user'] ['email'] = $UserInfo ['email'];
            $returnData ['user'] ['gender'] = $UserInfo ['gender'];
            $returnData ['user'] ['link'] = $UserInfo ['link'];
            $returnData ['user'] ['picture'] ['url'] = $UserInfo ['picture'] ['url'];
        }
        
        return $returnData;
    }
}
