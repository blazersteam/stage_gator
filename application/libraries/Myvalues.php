<?php

class Myvalues
{
    var $homeDetails = [
        "controller" => "home",
        "model" => "home_model"
    ];
    var $loginDetails = [
        "controller" => "login",
        "model" => "login_model"
    ];
    var $profileDetails = [
        "controller" => "profile",
        "model" => "profile_model"
    ];
    var $eventDetails = [
        "controller" => "event",
        "model" => "event_model"
    ];
    var $fanDetails = [
        "controller" => "fan",
        "model" => "fan_model"
    ];
    var $userRegisterDetails = [
        "controller" => "register",
        "model" => "register_model"
    ];
    /*
     * var $userDetails = [ "controller" => "user", "model" => "user_model" ];
     */
    var $resourcesDetails = [
        "controller" => "resources",
        "model" => ""
    ];
    var $change_passwordDetails = [
        "controller" => "change_password",
        "model" => "change_password_model"
    ];
    var $locationDetails = [
        "controller" => "location",
        "model" => "location_model"
    ];
    var $performerDetails = [
        "controller" => "performer",
        "model" => "performer_model"
    ];
    var $invitationDetails = [
        "controller" => "invitation",
        "model" => "invitation_model"
    ];
    var $timeTableDetails = [
        "controller" => "timetable",
        "model" => "timetable_model"
    ];
    var $hostDetails = [
        "controller" => "host",
        "model" => "host_model"
    ];
    var $overviewDetails = [
        "controller" => "overview"
    ];
    var $biteDetails = [
        "controller" => "bite",
        "model" => "bite_model"
    ];
    var $rankDetails = [
        "controller" => "rank",
        "model" => "rank_model"
    ];
    var $subscriptionDetails = [
        "controller" => "subscription",
        "model" => "subscription_model"
    ];
    var $messageDetails = [
        "controller" => "message",
        "model" => "message_model"
    ];
    var $tradingDetails = [
        "controller" => "trading",
        "model" => "trading_model"
    ];
    var $pasteventsDetails = [
        "controller" => "past_events",
        "model" => "past_events_model"
    ];
    var $adminPerformerDetails = [
        "controller" => "performers",
        "model" => "performers_model"
    ];
    var $adminVenueDetails = [
        "controller" => "venues",
        "model" => "venues_model"
    ];
    var $adminEventDetails = [
        "controller" => "events",
        "model" => "events_model"
    ];
    var $tippingDetails = [
        "controller" => "tipping",
        "model" => "tipping_model"
    ];
    var $dashboardDetails = [
        "controller" => "dashboard",
        "model" => "dashboard_model"
    ];
    var $webhookDetails = [
        "controller" => "webhook",
        "model" => "webhook_model"
    ];
    var $adminTransactionDetails = [
        "controller" => "transaction_details",
        "model" => "transaction_details_model"
    ];
    var $adminFanDetails = [
        "controller" => "fans",
        "model" => "fans_model"
    ];
    
    var $applePaymentDetails = [
        "controller" => "apple_payment",
    ];
    
    /* Same Controller Names Routing Has To BE Set in Routes.php Config File. */
    var $routedControllers = [
        'resetPassword' => [
            "controller" => "resetPassword"
        ],
        'forgotPassword' => [
            "controller" => "forgotPassword"
        ]
    ];
    
    var $cronDetails = [
        "controller" => "cron_job",
        "model" => "cron_model"
    ];
}
