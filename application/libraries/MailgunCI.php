<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH. 'third_party/vendor/autoload.php';

use Mailgun\Mailgun;

/**
 * Mailgun Library
 *
 * @author siddheshd
 *        
 */
class MailgunCI
{
    //private $baseUrl = 'https://api.mailgun.net/v3/sandbox5b5abc494399427cbfd9319c40db375d.mailgun.org';
    //private $apiKey = 'key-c11250264254d9c955d9484ed063a60c'; // Your Api key
    //private $domain = 'sandbox5b5abc494399427cbfd9319c40db375d.mailgun.org'; // Your domain name
    
    private $baseUrl = 'https://api.mailgun.net/v3/stagegator.com';
    private $apiKey = 'key-40302639acf58377312fed5694f138c2'; // Your Api key
    private $domain = 'stagegator.com'; // Your domain name
    
    private $mgClient;

    public function __construct()
    {
        $this->mgClient = new Mailgun($this->apiKey);
    }

    /**
     * Send Email via HTTP mailgun
     *
     * @param array $emailParams
     * @return stdClass

        // Input
        $to = ['alex.test1990@gmail.com','qa1websoptimization@gmail.com']; // multiple users
        $from = 'SomeBrand <qa2websoptimization@gmail.com>';
        $subject = 'This is a test from AlexBrand';        
        $html = $this->load->view('email_view',NULL,true);
        $filePath1 = FCPATH . 'assets/images/1.png';
        $filePath2 = FCPATH . 'assets/images/2.png';
     */
    public function sendEmail($emailParams, $attachments = [], $tracking = false)
    {
        // Tracking on or off
        if ($tracking) {
            $emailParams ['o:tracking'] = $tracking;
        }
        
        // Email From default of not
        if (empty($emailParams ['from'])) {
            $emailParams ['from'] = "Some User <mailgun@$this->domain>";
        }
        
        try {
            // Send Email
            return $this->mgClient->sendMessage($this->domain, $emailParams, array(
                'attachment' => $attachments
            ));
        }
        catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Track events based on event type
     * 
     * @param string $eventType
     */
    public function getEvents($eventType = null)
    {
        try {
            $queryString = array(
               // 'begin' => 'Mon, 6 March 2017 09:00:00 -0000',
                'ascending' => 'yes',
                'limit' => 25
                // 'pretty' => 'yes',
                // 'subject' => 'test',
            );
            
            if ($eventType != null) {
                $queryString ['event'] = $eventType;
            }
            
            return $this->mgClient->get("$this->domain/events", $queryString);
        }
        catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
