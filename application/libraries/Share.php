<?php
class Share {

/**
     * twitterShareButton()
     * This function is used to generate twitter share button
     * 
     * Developer - Pravin Dabhi
     * Datetime - 7-11-2016 02:31
     * 
     * @param: $content: String content to share
     * @return: Twitter share button
     */
    public  function twitterShareButton($content)
    {
    	echo $button ='<a data-via="twitterapi" data-lang="en" data-count="vertical"
    			href="https://twitter.com/intent/tweet?text='.$content.'"><img
								src="'.EXTERNAL_PATH.'/images/tweet.png" />
    			</a>
    	
    			<!-- Twitter -->
    			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>';
    }
    
    /**
     * googlePlusShareButton()
     * This function is used to generate google plus share button
     *
     * Developer - Pravin Dabhi
     * Datetime - 7-11-2016 03:10
     *
     * @param: $content: String content to share
     * @return: Google plus share button
     */
    public  function googlePlusShareButton($url)
    { 
    	?>
    	<a href="https://plus.google.com/share?url=<?=$url?>" 
    	onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');">
    	<img src="<?php echo EXTERNAL_PATH ?>images/google-plus.png" />
    	</a>
    	<?php 
    }
    
    /**
     * fbShareButton()
     * This function is used to facebook share button
     *
     * Developer - Pravin Dabhi
     * Datetime - 7-11-2016 03:44
     *
     * @param: $content: String content to share
     * @return: Google plus share button
     */
    public  function fbShareButton($image = null, $title = null, $description = null,$url = null )
    {
    	?>
    	<a href="https://www.facebook.com/sharer/sharer.php?u=<?=$url?>&title=<?php echo $title; ?>
    		<?php if($image != ""){?>&picture=<?php echo $image; ?> <?php } ?>&description=<?php echo $description; ?>"
  		 onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
  		 target="_blank" title="Share on Facebook"><img src="<?php echo EXTERNAL_PATH ?>images/fb_share_s.png" />
		</a>
		
       <?php 
    }
}
    ?>