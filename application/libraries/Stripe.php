<?php
// Codeigniter access check, remove it for direct use
if (! defined('BASEPATH'))
    exit('No direct script access allowed');
    // Set the server api endpoint and http methods as constants
    
// Load Stripe library
require_once APPPATH . "third_party/stripe/init.php";

/**
 * A library to deal with stripe
 */
class Stripe
{

    /**
     * Constructor method
     *
     * @param array Configuration parameters for the library
     */
    public function __construct()
    {
        // Store the config values
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);
        // \Stripe\Stripe::setApiKey("sk_live_1hXRtU6FQ5EwEKAuVQfoGO31");
    }

    /**
     * Create and apply a charge to an existent user based on it's customer_id
     *
     * @param int The amount to charge in cents ( USD )
     * @param string The customer id of the charge subject
     * @param string A free form reference for the charge
     */
    public function charge_customer($amount, $customer_id, $desc)
    {
        $params = array(
            'amount' => $amount,
            'currency' => 'usd',
            'customer' => $customer_id,
            'description' => $desc
        );
        
        return $this->_send_request('charges', $params, STRIPE_METHOD_POST);
    }

    /**
     * Create and apply a charge based on credit card information
     *
     * @param int The amount to charge in cents ( USD )
     * @param mixed This can be a card token generated with stripe.js ( recommended ) or
     *        an array with the card information: number, exp_month, exp_year, cvc, name
     * @param string A free form reference for the charge
     */
    public function charge_card($amount, $card, $desc)
    {
        $params = array(
            'amount' => $amount,
            'currency' => 'usd',
            'card' => $card,
            'description' => $desc
        );
        
        return $this->_send_request('charges', $params, STRIPE_METHOD_POST);
    }

    /**
     * Retrieve information about a specific charge
     *
     * @param string The charge ID to query
     */
    public function charge_info($charge_id)
    {
        return $this->_send_request('charges/' . $charge_id);
    }

    /**
     * Refund a charge
     *
     * @param string The charge ID to refund
     * @param int The amount to refund, defaults to the total amount charged
     */
    public function charge_refund($charge_id, $amount = FALSE)
    {
        $amount ? $params = array(
            'amount' => $amount
        ) : $params = array();
        return $this->_send_request('charges/' . $charge_id . '/refund', $params, STRIPE_METHOD_POST);
    }

    /**
     * Get a list of charges, either general or for a certain customer
     *
     * @param int The number of charges to return, default 10, max 100
     * @param int Offset to apply to the list, default 0
     * @param string A customer ID to return only charges for that customer
     */
    public function charge_list($count = 10, $offset = 0, $customer_id = FALSE)
    {
        $params ['count'] = $count;
        $params ['offset'] = $offset;
        if ($customer_id)
            $params ['customer'] = $customer_id;
        $vars = http_build_query($params, NULL, '&');
        
        return $this->_send_request('charges?' . $vars);
    }

    /**
     * customer_create()
     * This method create a customer subscriptiion profile in stripe
     *
     * @param Array | $subscribe this parameters contain all the information of cards as well customer
     *       
     */
    public function customer_create($subscribe)
    {
        return \Stripe\Customer::create($subscribe);
    }

    /**
     * Retrieve information for a given customer
     *
     * @param string The customer ID to get information about
     */
    public function customer_info($customer_id)
    {
        return \Stripe\Customer::retrieve($customer_id);
    }

    /**
     * Update an existing customer record
     *
     * @param string The customer ID for the record to update
     * @param array An array containing the new data for the user, you may use the
     *        following keys: card, email, description
     */
    public function customer_update($customer_id, $newdata)
    {
        return $this->_send_request('customers/' . $customer_id, $newdata, STRIPE_METHOD_POST);
    }

    /**
     * Delete an existing customer record
     *
     * @param string The customer ID of the record to delete
     */
    public function customer_delete($customer_id)
    {
        return $this->_send_request('customers/' . $customer_id, array(), STRIPE_METHOD_DELETE);
    }

    /**
     * Get a list of customers record ordered by creation date
     *
     * @param int The number of customers to return, default 10, max 100
     * @param int Offset to apply to the list, default 0
     */
    public function customer_list($count = 10, $offset = 0)
    {
        $params ['count'] = $count;
        $params ['offset'] = $offset;
        $vars = http_build_query($params, NULL, '&');
        
        return $this->_send_request('customers?' . $vars);
    }

    /**
     * Subscribe a customer to a plan
     *
     * @param string The customer ID
     * @param string The plan identifier
     * @param array Configuration options for the subscription: prorate, coupon, trial_end(stamp)
     */
    public function customer_subscribe($customer_id, $plan_id, $options = array())
    {
        $options ['plan'] = $plan_id;
        
        return $this->_send_request('customers/' . $customer_id . '/subscription', $options, STRIPE_METHOD_POST);
    }

    /**
     * Cancel a customer's subscription
     *
     * @param string The customer ID
     * @param boolean Cancel the subscription immediately( FALSE ) or at the end of the current period( TRUE )
     */
    public function customer_unsubscribe($subs_id)
    {
        
        /*
         * ini_set('display_errors',1);
         * error_reporting(1);
         */
        $sub = \Stripe\Subscription::retrieve($subs_id);
        return $sub->cancel(array(
            "at_period_end" => true
        ));
    }

    /**
     * update_subscribtion()
     * This method update a user subscription in upgrade and downgrade way
     *
     * @param string| $sub_id is current logged in user's subscription id
     * @param integer| $plan is plan id user can change his plan anytime
     *       
     * @return object return json object to method
     */
    function update_subscribtion($sub_id, $plan)
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $subscription = \Stripe\Subscription::retrieve($sub_id);
        $subscription->plan = $plan;
        $subscription->save();
    }

    /**
     * Get the next upcoming invoice for a given customer
     *
     * @param string Customer ID to get the invoice from
     */
    public function customer_upcoming_invoice($customer_id)
    {
        return \Stripe\Invoice::upcoming(array(
            "customer" => $customer_id
        ));
        
        // return $this->_send_request( 'invoices/upcoming?customer='.$customer_id );
    }

    /**
     * Generate a new single-use stripe card token
     *
     * @param array An array containing the credit card data, with the following keys:
     *        number, cvc, exp_month, exp_year, name
     * @param int If the token will be used on a charge, this is the amount to charge for
     */
    public function card_token_create($card_data, $amount)
    {
        $params = array(
            'card' => $card_data,
            'amount' => $amount,
            'currency' => 'usd'
        );
        
        return $this->_send_request('tokens', $params, STRIPE_METHOD_POST);
    }

    /**
     * Get information about a card token
     *
     * @param string The card token ID to get the information
     */
    public function card_token_info($token_id)
    {
        return $this->_send_request('tokens/' . $token_id);
    }

    /**
     * Create a new subscription plan on the system
     *
     * @param string The plan identifier, this will be used when subscribing customers to it
     * @param int The amount in cents to charge for each period
     * @param string The plan name, will be displayed in invoices and the web interface
     * @param string The interval to apply on the plan, could be 'month' or 'year'
     * @param int Number of days for the trial period, if any
     */
    public function plan_create($plan_id, $amount, $name, $interval, $trial_days = FALSE)
    {
        $params = array(
            'id' => $plan_id,
            'amount' => $amount,
            'name' => $name,
            'currency' => 'usd',
            'interval' => $interval
        );
        if ($trial_days)
            $params ['trial_period_days'] = $trial_days;
        
        return $this->_send_request('plans', $params, STRIPE_METHOD_POST);
    }

    /**
     * Retrieve information about a given plan
     *
     * @param string The plan identifier you wish to get info about
     */
    public function plan_info($plan_id)
    {
        return $this->_send_request('plans/' . $plan_id);
    }

    /**
     * Delete a plan from the system
     *
     * @param string The identifier of the plan you want to delete
     */
    public function plan_delete($plan_id)
    {
        return $this->_send_request('plans/' . $plan_id, array(), STRIPE_METHOD_DELETE);
    }

    /**
     * Retrieve a list of the plans in the system
     */
    public function plan_list($count = 10, $offset = 0)
    {
        $params ['count'] = $count;
        $params ['offset'] = $offset;
        $vars = http_build_query($params, NULL, '&');
        
        return $this->_send_request('plans?' . $vars);
    }

    /**
     * Get infomation about a specific invoice
     *
     * @param string The invoice ID
     */
    public function invoice_info($invoice_id)
    {
        return $this->_send_request('invoices/' . $invoice_id);
    }

    /**
     * Get a list of invoices on the system
     *
     * @param string Customer ID to retrieve invoices only for a given customer
     * @param int Number of invoices to retrieve, default 10, max 100
     * @param int Offset to start the list from, default 0
     */
    public function invoice_list($customer_id = NULL, $count = 10, $offset = 0)
    {
        $params ['count'] = $count;
        $params ['offset'] = $offset;
        if ($customer_id)
            $params ['customer'] = $customer_id;
        $vars = http_build_query($params, NULL, '&');
        
        return $this->_send_request('invoices?' . $vars);
    }

    /**
     * Register a new invoice item to the upcoming invoice for a given customer
     *
     * @param string The customer ID
     * @param int The amount to charge in cents
     * @param string A free form description explaining the charge
     */
    public function invoiceitem_create($customer_id, $amount, $desc)
    {
        $params = array(
            'customer' => $customer_id,
            'amount' => $amount,
            'currency' => 'usd',
            'description' => $desc
        );
        
        return $this->_send_request('invoiceitems', $params, STRIPE_METHOD_POST);
    }

    /**
     * Get information about a specific invoice item
     *
     * @param string The invoice item ID
     */
    public function invoiceitem_info($invoiceitem_id)
    {
        return $this->_send_request('invoiceitems/' . $invoiceitem_id);
    }

    /**
     * Update an invoice item before is actually charged
     *
     * @param string The invoice item ID
     * @param int The amount for the item in cents
     * @param string A free form string describing the charge
     */
    public function invoiceitem_update($invoiceitem_id, $amount, $desc = FALSE)
    {
        $params ['amount'] = $amount;
        $params ['currency'] = 'usd';
        if ($desc)
            $params ['description'] = $desc;
        
        return $this->_send_request('invoiceitems/' . $invoiceitem_id, $params, STRIPE_METHOD_POST);
    }

    /**
     * Delete a specific invoice item
     *
     * @param string The invoice item identifier
     */
    public function invoiceitem_delete($invoiceitem_id)
    {
        return $this->_send_request('invoiceitems/' . $invoiceitem_id, array(), STRIPE_METHOD_DELETE);
    }

    /**
     * Get a list of invoice items
     *
     * @param string Customer ID to retrieve invoices only for a given customer
     * @param int Number of invoices to retrieve, default 10, max 100
     * @param int Offset to start the list from, default 0
     */
    public function invoiceitem_list($customer_id = FALSE, $count = 10, $offset = 0)
    {
        $params ['count'] = $count;
        $params ['offset'] = $offset;
        if ($customer_id)
            $params ['customer'] = $customer_id;
        $vars = http_build_query($params, NULL, '&');
        
        return $this->_send_request('invoiceitems?' . $vars);
    }

    /**
     * account_create
     * This method return create a customer in stripe
     *
     * @param is array with nesseccery information to create an account
     * @return Object it will return json data to controller
     */
    public function account_create($data)
    {
        return \Stripe\Account::create($data)->__toJSON();
    }

    /**
     *
     * @param unknown $countryISO
     * @return json $response is result of account fileds according to country
     */
    public function get_country_spacific_fields($countryISO)
    {
        $headers = array(
            'Authorization: Bearer sk_test_q6cIQLYB0AbNuKUTk3oEC1Uc'
        );
        $ch = curl_init("https://api.stripe.com/v1/country_specs/" . $countryISO);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    /**
     * Used for apple pay for payment to register your domain with Apple by stripe
     */
    public function apple_pay_domain_create()
    {
        return \Stripe\ApplePayDomain::create(array(
            'domain_name' => 'stagegator.com'
        ));
    }
}
	