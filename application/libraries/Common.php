<?php
class Common {
	public function getDateTime($format= "Y-m-d H:i:s")
	{
		return date($format);
	}
	
	public function getDate($date,$format= "Y-m-d")
	{
		return date($format, strtotime($date));
	}
	
	public function getRealIp()
	{
		$CI	=	& get_instance();
		return $CI->input->ip_address();
	}
}