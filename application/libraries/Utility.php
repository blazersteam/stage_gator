<?php

use function Composer\Autoload\includeFile;

class Utility
{
    
    /*
     * REFERENCE BY :
     * http://stackoverflow.com/questions/27633584/php-fatal-error-call-to-undefined-function-mcrypt-get-iv-size-in-appserv
     */
    public $skey = "STAGEGATOR_PRODUCT-CRTD16092016\0";

    function encodeText($value, $removeTags = false)
    {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $val = $removeTags ? strip_tags($v) : $v;
                $val = addslashes($val);
                $value [$k] = $val;
            }
        }
        else {
            $value = $removeTags ? strip_tags($value) : $value;
            $value = addslashes($value);
        }
        return $value;
    }

    function decodeText($value, $htmlEntity = true)
    {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $val = stripslashes($v);
                $value [$k] = $htmlEntity ? htmlentities($val) : $val;
            }
        }
        elseif (is_object($value)) {
            foreach ($value as $k => $v) {
                $val = stripslashes($v);
                $value->$k = $htmlEntity ? htmlentities($val) : $val;
            }
        }
        else {
            $value = stripslashes($value);
            $value = $htmlEntity ? htmlentities($value) : $value;
        }
        return $value;
    }

    public function safe_b64encode($string)
    {
        $data = base64_encode($string);
        $data = str_replace(array(
            '+',
            '/',
            '='
        ), array(
            '-',
            '_',
            ''
        ), $data);
        return $data;
    }

    public function safe_b64decode($string)
    {
        $data = str_replace(array(
            '-',
            '_'
        ), array(
            '+',
            '/'
        ), $string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    public function encode($value)
    {
        if (! $value) {
            return false;
        }
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->skey, $text, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext));
    }

    public function decode($value)
    {
        if (! $value) {
            return false;
        }
        $crypttext = $this->safe_b64decode($value);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->skey, $crypttext, MCRYPT_MODE_ECB, $iv);
        return (trim($decrypttext));
    }

    public function setFlashMessage($type, $message)
    {
        $CI = & get_instance();
        $template = '<div class="alert alert-' . $type . ' alert-dismissible text-center" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						   <span aria-hidden="true">&times;</span>
							</button>' . $message . '</div>';
        $CI->session->set_flashdata("myMessage", $template);
    }

    public function sendMailSMTP($data)
    {
        // return $this->sendMail($data);
//         $config ['protocol'] = "smtp";
//         $config ['smtp_host'] = SMTP_HOST;
//         $config ['smtp_port'] = SMTP_PORT;
//         $config ['smtp_user'] = SMTP_USER;
//         $config ['smtp_pass'] = SMTP_PASS;
//         $config ['smtp_timeout'] = 20;
//         $config ['priority'] = 1;
        
//         $config ['charset'] = 'utf-8';
//         $config ['wordwrap'] = TRUE;
//         $config ['crlf'] = "\r\n";
//         $config ['newline'] = "\r\n";
//         $config ['mailtype'] = "html";
        
//         $CI = & get_instance();
//         $message = $data ["message"];
//         $CI->load->library('email', $config);
//         $CI->email->clear();
//         $CI->email->from($config ['smtp_user'], $data ['from_title']);
//         $CI->email->to($data ["to"]);
//         if (isset($data ["bcc"])) {
//             $CI->email->bcc($data ["bcc"]);
//         }
//         $CI->email->reply_to($config ['smtp_user'], $data ['from_title']);
//         $CI->email->subject($data ["subject"]);
//         $CI->email->message($message);
//         $res = $CI->email->send();
        /*
         * var_dump($res);
         * echo $CI->email->print_debugger();
         * die;
         */
       
        $this->mailgun($data);
        
        return true;
    }
    
    public function mailgun($data){
        
        $CI = & get_instance();
        $CI->load->library('MailgunCI');
        
        $to    		= $data['to'];
//      $to    		= 'jashpalp@websoptimization.com';
        $from  		= "<no-reply@stagegator.com>";
        $subject   	= $data ["subject"];
        
        $emailParams = array(
            'from'    => $from,
            'to'      => $to,
            'subject' => $subject
        );
        
        $emailParams['html'] = $data ["message"];
        
        // set the Reply-To value as office email id
        $emailParams['h:Reply-To'] = $from;
        
        // Send email
        $result = $CI->mailgunci->sendEmail($emailParams);
    }
    
    // mail function
    function sendMail($row)
    {
        $CI = & get_instance();
        $to = $row ['to'];
        $subject = $row ['subject'];
        $message = $row ['message'];
        
        $headers = 'From: ' . 'StageGator' . '<noreply@stagegator.com>' . "\r\n";
        $headers .= 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type:  text/html; charset=utf-8' . "\r\n";
        $headers .= 'Reply-To: noreply@stagegator.com' . "\r\n";
        $headers .= 'Return-Path: noreply@stagegator.com' . "\r\n";
        return mail($to, $subject, $message, $headers);
    }

    /**
     * notification_email()
     * This is a common function to send notification mail
     *
     * @param unknown $idUser
     * @param unknown $inner_message
     * @param string $toVenue
     * @param string $link
     * @param string $subject
     */
    function notification_email($idUser, $inner_message, $toVenue = false, $link = "Please", $subject = "Notification Alert")
    {
        $CI = get_instance();
        $data ['from_title'] = "info@stagegator.com";
        $sql = '';
        if ($toVenue) {
            $sql = "select user.* from user join event on user_id=idVenue where idEvent=$idUser";
        }
        else {
            $sql = "select * from user where user_id=" . $idUser;
        }
        $user = $CI->db->query($sql)->row();
        if ($user->sendNotificationsEmail == 1) {
            $data ['username'] = ucwords($user->chrName);
            $data ['inner_message'] = $inner_message;
            $data ['link'] = $link;
            $data ['message'] = $CI->load->view("email_templates/send_notification_email", $data, true);
            $data ["to"] = trim($user->user_name);
            
            $data ["subject"] = $subject;
            $status = $this->sendMailSMTP($data);
            return $status;
        }
    }

    /**
     * Used for display the event or event schedule over view
     *
     * @param int $eventId, id of event
     * @param int $eventScheduleId, id of event schedule
     *       
     * @return string $overviewUrl, that contain the url for event or eventschedule over view
     */
    function generateOverviewUrl($eventId = "", $eventScheduleId = "")
    {
        if (! empty($eventId) && ! empty($eventScheduleId)) {
            $accessTocken = $this->encode($eventId . "||||" . $eventScheduleId);
        }
        elseif (! empty($eventId) && empty($eventScheduleId)) {
            $accessTocken = $this->encode($eventId);
        }
        
        $overviewUrl = SITEURL . "share/event?access=" . $accessTocken;
        
        return $overviewUrl;
    }

    /**
     * fbShareButton()
     * This function is used to facebook share button
     *
     * Developer - Pravin Dabhi
     * Datetime - 7-11-2016 03:44
     *
     * @param : $content: String content to share
     * @return : Google plus share button
     */
    public function fbShareButton($image = null, $title = null, $description = null, $url = null)
    {
        $description = urldecode($description);
        $description = str_replace([
            "</br>",
            "<br/>",
            "</p>"
        ], [
            "\r\n",
            "\r\n",
            "</p>\r\n"
        ], $description);
        $description = strip_tags($description);
        $description = urlencode($description);
        ?>
<a
	href="https://www.facebook.com/sharer/sharer.php?u=<?=$url?>&title=<?php echo trim($title); ?>
        		<?php if($image != ""){?>&picture=<?php echo $image; ?> <?php } ?>&description=<?php echo $description; ?>"
	onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
	target="_blank" title="Share on Facebook"><img
	src="<?php echo EXTERNAL_PATH ?>images/fb_share_s.png" /> </a>

<?php
    }

    /**
     * sendGCM
     * This method send push notification to mobile user
     *
     * @param string $title is notification message title
     * @param string $message is message to send on devic
     * @param integer $id is registration ids of device
     * @param string $device , that is android / ios
     *       
     * @return boolean true or false depend on response
     */
    function sendGCM($message, $id, $device)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        
        // check that sending to sigal device or multipal
        if (is_array($id)) {
            $key = 'registration_ids';
        }
        else {
            $key = 'to';
        }
        
        
        $fields = array(
            $key => $id,
            'notification' => array(
                "title" => "StageGator",
                "body" => $message
            )
        );
        $fields = json_encode($fields);
        
        
        if ($device == 'android') {
            
            // Code for android device to send the notification
                        
            $headers = array(
                'Authorization: key=' . "AIzaSyDcY06N5079p0oRj8ipCyAnUlbDghdpG9Y",
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            $result = curl_exec($ch);
            curl_close($ch);
        }
        
        if ($device == 'ios') {
            
            // Code for ios device to send the notification
            //'Authorization: key=' . "AIzaSyDhDo21WUoki01a1a-1eI1RKx-lTIc9iI0", -> old key
            $headers = array(
                'Authorization: key=' . "AIzaSyDgM8H6APYxVKu_00G4_hTZCykNQmoXOd4",
                'Content-Type: application/json'
            );
            
            $ch= curl_init();
                                                                                            
            // Setup curl, add headers and post parameters.
//             curl_setopt($chIphone, CURLOPT_CUSTOMREQUEST, "POST");
//             curl_setopt($chIphone, CURLOPT_POSTFIELDS, $fields);
//             curl_setopt($chIphone, CURLOPT_HTTPHEADER, $headers);

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            
            // Send the request
            $response = curl_exec($ch);
            curl_close($ch);
        }
    }
    
    
    /**
     * get abbreviation(sort-name) of timezone based on given timezone
     * 
     * @param string $timezone, timezone of date
     * 
     * @return string dateTime abbreviation, that contain sort-name of timezone
     */
    public function getAbbreviationOfTimezone($timezone){
        $dateTime = new DateTime();
        $dateTime->setTimeZone(new DateTimeZone($timezone));
        return $dateTime->format('T');
    }
}

?>
