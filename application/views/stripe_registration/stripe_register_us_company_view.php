<div class="alert alert-danger striperror_personal_dtl"
	style="display: none;"></div>
<div class="form-group">
	<label class="col-sm-3 control-label">Business Name <em>*</em>
	</label>
	<div class="col-sm-8">
		<input type="text" name="update[legal_entity__business_name]"
			maxlength="250" class="form-control required"
			placeholder="Enter Business Name" value="">
       <?=form_error('legal_entity__business_name')?>
    </div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">First Name <em>*</em>
	</label>
	<div class="col-sm-8">
		<input type="text" name="update[legal_entity__first_name]"
			maxlength="50" class="form-control required"
			placeholder="Enter First Name" value="">
       <?=form_error('legal_entity__first_name')?>
    </div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Last Name <em>*</em>
	</label>
	<div class="col-sm-8">
		<input type="text" name="update[legal_entity__last_name]"
			maxlength="50" class="form-control required"
			placeholder="Enter Last Name" value="">
       <?=form_error('legal_entity__last_name')?>
    </div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Date Of Birth <em>*</em>
	</label>
	<div class="col-sm-8">
		<input type="text" id="txtDate" value="" name="txtDate"
			placeholder="Date Of Birth" class="datepicker required form-control" readonly>
       <?=form_error('Txtdate_of_birth')?>
    </div>
	<input type="hidden" name="update[legal_entity__dob__day]" value=""> <input
		type="hidden" name="update[legal_entity__dob__month]" value=""> <input
		type="hidden" name="update[legal_entity__dob__year]" value="">
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Address line <em>*</em>
	</label>
	<div class="col-sm-8">
		<input type="text" name="update[legal_entity__address__line1]"
			maxlength="250" class="form-control required"
			placeholder="Enter Address line" value="">
       <?=form_error('legal_entity__address__line1')?>
    </div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">City<em>*</em>
	</label>
	<div class="col-sm-8">
		<input type="text" name="update[legal_entity__address__city]"
			id="locality" maxlength="50" class="form-control required"
			placeholder="Enter City" value="<?=@$address->mem_city?>" readonly>
       <?=form_error('legal_entity__address__city')?>
    </div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">State <em>*</em>
	</label>
	<div class="col-sm-8">
		<input type="text" name="update[legal_entity__address__state]"
			id="administrative_area_level_1" maxlength="50"
			class="form-control required" placeholder="Enter State"
			value="<?=@$address->mem_state?>" readonly>
       <?=form_error('legal_entity__address__state')?>
    </div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Postal Code <em>*</em>
	</label>
	<div class="col-sm-8">
		<input type="text" name="update[legal_entity__address__postal_code]"
			maxlength="10" class="form-control required"
			placeholder="Enter Postal code" value="">
       <?=form_error('legal_entity__address__postal_code')?>
    </div>
	<input type="hidden" name="update[legal_entity__type]"
		class="form-control required" readonly value="">
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">SSN Last 4 Digits <em>*</em>
	</label>
	<div class="col-sm-8">
		<input type="text" name="update[legal_entity__ssn_last_4]"
			maxlength="4" class="form-control required"
			placeholder="Enter SSN Last 4 Digites" value="0000">
       <?=form_error('legal_entity__ssn_last_4')?>
    </div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Business Tax ID<em>*</em>
	</label>
	<div class="col-sm-8">
		<input type="text" name="update[legal_entity__business_tax_id]"
			maxlength="15" class="form-control required"
			placeholder="Enter Business Tax ID" value="">
       <?=form_error('legal_entity__business_tax_id')?>
    </div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Personal ID Number <em>*</em>
	</label>
	<div class="col-sm-8">
		<input type="text" name="update[legal_entity__personal_id_number]"
			maxlength="255" class="form-control required"
			placeholder="Enter Personal ID Number" value="000000000">
       <?=form_error('legal_entity__personal_id_number')?>
    </div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label">Picture ID <em>*</em>
	</label>
	<div class="col-sm-8">
		<input type="file" name="update[legal_entity__verification__document]"
			class="required uploadimg" style="float: none;">
       <?=form_error('legal_entity__verification__document')?>
       <p>Note: Please take a picture of your picture ID upload it.</p>
	</div>
</div>
