<link href="<?=EXTERNAL_PATH?>css/custom_style.css" rel="stylesheet">
<div id="user-type">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 text-center">
            <a href="#">
              <img src="<?=EXTERNAL_PATH?>images/Logo.png" alt="logo"/>
            </a>
          </div>
        </div>
      </div>
      <div class="clearfix">
      </div>
      <!-- forms part-->
     <div class="container">
     <?php echo $this->session->flashdata('myMessage'); ?>
      <input type = "hidden" id = "base_url" value = "<?=SITEURL?>"/>
        <div class="row frmarea action">
          <div class="col-sm-6 col-lg-4 col-sm-offset-3 col-lg-offset-4">
            <div class="form-group input-group-lg">
             <a href="<?=SITEURL.$this->myvalues->loginDetails['controller']?>/signupfacebook/<?=PERFOMER_GROUP?>" class="form-control btn btn-sin" id="register">I am a New Performer</a>
            </div>
            <div class="form-group input-group-lg">
             <a href="<?=SITEURL.$this->myvalues->loginDetails['controller']?>/signupfacebook/<?=VENUE_GROUP?>" class="form-control btn btn-sin" id="register">I am a New Venue</a>
            </div>
            <div class="form-group input-group-lg">
              <a href="<?=SITEURL.$this->myvalues->loginDetails['controller']?>/signupfacebook/<?=FAN_GROUP?>" class="form-control btn btn-sin" id="register">I am a New Fan</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- forms part end-->
    
    
