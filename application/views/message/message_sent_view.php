<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>	
		</div>
<?=form_open(SITEURL.$controllerName.'/DeleteMessage/sentStatus','id="frm" onsubmit="return confirm_delete_message();"')?>
<?php if($result>0) { ?>
<div class="pull-left">
<input class="btn btn-danger" type="submit" name="deleteSent" value="Delete"/>
</div>
<?php } ?>
<div class="" style="margin-bottom: 20px; display: block; overflow: hidden; text-align: right;">


<a href="<?=SITEURL.$controllerName?>" class="btn btn-default">Inbox</a>
</div>
<table class="table table-hover dataTable">
<thead>
<tr>
	<th style="width: 15px;"><input type="checkbox" name="chkAll" onclick="setAll();"></th>
	<th style="width: 20%;">To</th>
	<th>Message</th>
	<th style="width: 15%;">Date</th>
</tr>
</thead>
<tbody>
<?php 
	$flag = false;
	foreach($result as $r){ 
		$flag = true;
		?>
	<tr>
		<td><input type="checkbox" name="chkstatus[]" value="<?php echo $this->utility->encode($r->idMsg); ?>" class="inpt_c1"></td>
		<td class="message_anchor_sent" data-message="<?php echo $r->idMsg; ?>"><?php echo $this->utility->decodeText(ucwords($r->sentTo)); ?></td>
		<td class="message_anchor_sent" data-message="<?php echo $r->idMsg; ?>"><?php echo $this->utility->decodeText(urldecode($r->msgBody)); ?></td>
		<td class="message_anchor_sent" data-message="<?php echo $r->idMsg; ?>"><?php echo dateDisplay(date('m/d/Y h:i A',$r->dtAdded),'m/d/Y h:i A'); ?></td>
	</tr>
<?php } ?>
</tbody>
</table>
<?php if ($flag){ ?>
<div style="display:block; ">
<input class="btn btn-danger" type="submit" name="deleteSent" value="Delete"/>
</div>
<?php } ?>
<?=form_close()?>
</div>
</div>
<script type="text/javascript" src="<?=EXTERNAL_PATH?>js/modules/invitation_sent.js"></script>
