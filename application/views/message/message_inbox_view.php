
<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>	
		</div>

<?=form_open(SITEURL.$controllerName.'/DeleteMessage/inboxStatus','id="frm" onsubmit="return confirm_delete_message();"')?>
<?php if(count($result)>0) { ?>
<div class="row">
<input class="btn btn-danger" type="submit" name="deleteSent" value="Delete"/>
</div>
<?php } ?>
<div class="" style="margin-bottom: 20px; display: block; overflow: hidden; text-align: right;">
<!-- <a href="#" class="btn btn-success">New Message</a> -->
<a href="<?=SITEURL.$controllerName."/sent"?>" class="btn btn-default hidden">Outbox</a>
</div>

<table class="table table-hover display dataTable">
<thead>
<tr>
	<th style="width: 15px;"><input type="checkbox" name="chkAll" onclick="setAll();"></th>
	<!-- <th style="width: 20%;">From</th> -->
	<th>Message</th>
	<th style="width: 15%;">Date</th>
</tr>
</thead>
<tbody>
<?php 

	$flag = false;
	foreach($result as $r){ 
		$flag = true;
	$class = "";
		if ($r->readStatus==0){
			$class= 'style="background: rgb(221, 221, 221);"';
		}
	?>
	<tr <?php echo $class; ?>>
		<td><input type="checkbox" name="chkstatus[]" value="<?php echo $this->utility->encode($r->idMsg); ?>" class="inpt_c1"></td>
		<td class="message_anchor_inbox" data-message="<?php echo $r->idMsg; ?>"><?php echo urldecode($r->msgBody); ?></td>
		<td class="message_anchor_inbox" data-message="<?php echo $r->idMsg; ?>"><?php echo dateDisplay(date('m/d/Y h:i A',$r->dtAdded),'m/d/Y h:i A'); ?></td>
	</tr>
<?php } ?>
</tbody>
</table>
<?php if ($flag){ ?>
<div style="display:block; ">
<input class="btn btn-danger" type="submit" name="deleteSent" value="Delete"/>
</div>
<?php } ?>
<?=form_close();?>
</div>
</div>
<script type="text/javascript" src="<?=EXTERNAL_PATH?>js/modules/invitation_sent.js"></script>
