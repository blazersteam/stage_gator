<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>	
										
					</div>
<form action="" method="post">
<?=form_open(SITEURL.$controllerName.'/settings')?>
<table class="table table-bordered">
<tr>
	<th>Action</th>
	<th>Status</th>
</tr>

	<tr>
		<td>Receive Notification Emails</td>
		<td>
		<div class="form-group">
		    <div class="radio-inline">
			<input type = "radio" name = "rdaNotificationEmail" value = "0"checked>Off
			</div>
			<div class="radio-inline">
			<input type = "radio" name = "rdaNotificationEmail" value = "1"<?=$user->sendNotificationsEmail == 1 ? 'checked':''?>>On
		   </div>
		</div>
		</td>
	</tr>
	<tr>
		<td>
		<?php if ($user->idGrp==2){ ?>
			Receive Messages From Other Venues
		<?php } else { ?>
			Receive Messages From Other Performers
		<?php } ?>
		</td>
		<td>
			<div class="form-group">
			<div class="radio-inline">
			<input type = "radio" name = "rdaAllowInbox" value = "0"checked>Off
			</div>
			<div class="radio-inline">
			<input type = "radio" name = "rdaAllowInbox" value = "1"<?=$user->allowInbox == 1 ? 'checked':''?>>On
		   </div>
		</div>
		</td>
	</tr>
	<tr>
		<td>Receive Text Notifications</td>
		<td>
			<div class="form-group">
			  <div class="radio-inline">
			   <input type = "radio" name = "rdaAllowTextNotifications" value = "0"checked>Off
			 </div>
			 <div class="radio-inline">
			  <input type = "radio" name = "rdaAllowTextNotifications" value = "1"<?=$user->allowTextNotifications == 1 ? 'checked':''?>>On
		     </div>
		</div>
		</td>
	</tr>

</table>
<input type="submit" name="updateSettings" class="btn btn-sm btn-primary" value="Update" />
<?=form_close()?>
</div>
</div>
