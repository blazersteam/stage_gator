<?php if (isset($record->user_id)){ ?>
            <div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
					<div id="Tick"></div>
					<div class="page_header">
						<?=$title.' '.$record->chrName?>				
							</div>
						
	<table class="table" style="border: 0px;">
	<tr>
	<td style="width: 25%;">
	<?php if (!empty($record->image) && file_exists("./external/images/profiles/".$record->image)){ ?>
		<img src="<?=EXTERNAL_PATH."images/profiles/".$record->image; ?>" style="width: 100%;">
	<?php } else if(!empty($record->image) && filter_var($record->image, FILTER_VALIDATE_URL) == true) { ?>
		<img src="<?=$record->image?>" style="width: 100%;">
	<?php }else{ ?>
<img src="<?=EXTERNAL_PATH?>images/noimage.png" style="width: 100%;">	
<?php
} ?>
	
	</td>
	
	<td style="text-align: left; width: 75%;" valign="top">
	<h2 style="margin-top: 0px;"><?php echo ucwords($record->chrName); ?></h2>
	<?php echo $record->descr; ?>
	<?php if ($record->idGrp == "2"){ ?>
	<B>Address:</B> <?php echo (!empty($record->mem_address))?$record->mem_address:"N/A"; ?><BR>
	<B>Contact Person:</B> <?php echo (!empty($record->contact_person))?$record->contact_person:"N/A"; ?><BR>
	<?php 
        }
	   if($this->pUserId == $record->user_id || $record->mem_phone_visibility == "Public"):
	?>
	<B>Phone:</B> <?php echo (!empty($record->mem_phone))?$record->mem_phone:"N/A"; ?><BR>
	<?php endif;?>
	<img src="<?php echo EXTERNAL_PATH."images/style_icon.png"; ?>" /> 
	<?php 
		$i=0;
		foreach($styles as $r) { 
			if ($i){
				echo ", ";
			}
			echo $r->nameStyle;
			$i++;
		} 
	
	?>
	<BR>
	<img src="<?php echo EXTERNAL_PATH."images/city_icon.png"; ?>"/> 
	<?php echo ucwords($record->mem_city); ?>
	<BR>
	
	<?php if($record->account_type>0){ ?>
	<?php echo $record->changeTip; ?>
	<a href="javascript:void(0);" class="btn btn-default" onclick="bookRequest()"; >Book Me</a>
	<script>
	function bookRequest(){
		var myWindow=window.open();
		myWindow.document.write("<h2>Send Booking Request to <?php echo $record->chrName ?></h2>");
		myWindow.document.write("<form action='MAILTO:<?php echo $record->user_name; ?>' method='post' enctype='text/plain'>");
		myWindow.document.write("Your Name: <input type='text' name='id'><br>");
		myWindow.document.write("E-mail Address: <input type='text' name='contact'><br>");
		myWindow.document.write("Date: <input type='text' name='date'><br>");
		myWindow.document.write("Pay: <input type='text' name='date'><br>");
		myWindow.document.write("Additional Details: <input type='text' name='deets'><br>");
		myWindow.document.write("<input type='submit' value='Send'><input type='reset' value='Reset'></form>");}
		</script> <?php } ?>
		</td>
	
	</table>
<div class="page_header">Upcoming Shows</div>
<div id="no-more-tables">
<table class="table table-hover table-bordered table-striped dataTable cf" id="listEvent">
<thead class="cf">
	<tr>
		<th>Event Title</th>
		<th>Event Date </th>
		<th class="">Start Time</th>
		<th class="">End Time</th>	
	</tr>
</thead>
<tbody>
	<?php 
		$i = 1;
		foreach($list as $l){
			if (!event_end_time_crossed($l->end_date,$l->endTime)){
			?>
			<tr>
				<td data-title="Event Title">
					<a href="<?php echo $this->utility->generateOverviewUrl($l->idEvent,$l->scheduleId);?>" ><?php echo $l->title; ?></a>
				</td>
				<td data-title="Event Date"><?php echo dateDisplay($l->schedule_date.' '.$l->startTime); ?></td>
				<td data-title="Start Time" class=""><?php echo dateDisplay($l->schedule_date.' '.$l->startTime,'h:i A'); ?></td>
               	<td data-title="End Time" class=""><?php  echo dateDisplay($l->end_date.' '.$l->endTime,'h:i A'); ?></td>
			</tr>
			<?php 
			}
		}
	?>
</tbody>
</table>
</div>
</div>
</div>

<?php } ?>