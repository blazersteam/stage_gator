    <!-- .content -->
            <div class="content" style="height: 100%;">

				<div class="row">
					<div id="Tick"></div>
					<div class="page_header">
						<?=$title?>					
					</div>
	<?php echo $this->session->flashdata('myMessage'); ?>
	
	
     <?php echo form_open(SITEURL.$controllerName.'/edit', 'id="formProfileUpdate" class="form-horizontal"'); ?>
<div class="col-xs-12 col-sm-10">
  <div class="form-group">
    <label class="col-sm-4 control-label"><?=$this->lang->line('profile_name')?><em>*</em> </label>
    <div class="col-sm-8">
      <input type="text" name="txtName" maxlength="250" class="form-control" placeholder="Enter Venue Title" required value="<?php echo (set_value('txtName'))?set_value('txtName'):$user->chrName; ?>">
      <?=form_error('txtName')?>
    </div>
  </div>
  <?php if ($user->idGrp==2){ ?>
  <div class="form-group">
    <label class="col-sm-4 control-label"><?=$this->lang->line('register_contact_person')?><em>*</em> </label>
    <div class="col-sm-8">
      <input type="text" maxlength="50" name="txtContact_person" class="form-control" placeholder="Contact Person Name" required value="<?php echo set_value('txtContact_person')?set_value('txtContact_person'):$user->contact_person; ?>">
      <?=form_error('txtContact_person')?>
    </div>
  </div>
  <?php } ?>
  <div class="form-group">
    <label class="col-sm-4 control-label"><?=$this->lang->line('email')?></label>
    <div class="col-sm-8">
      <input class="form-control" type="text" value="<?php echo $user->user_name;  ?>" disabled>
    </div>
  </div>
  
  <div class="form-group">
    <label class="col-sm-4 control-label"><?=$this->lang->line('style')?><em>*</em></label>
    <div class="col-sm-8">
      <select name="cmbStyle[]" class="form-control chosen-select" multiple='multiple' required>  
      <?php 
      
            $user_styles = set_value('cmbStyle') <> '' ? set_value('cmbStyle') : $user_styles;
	    foreach($style as $st){
      		$sel = '';
		
      		 if (in_array($st->idStyle,$user_styles)){
      				 $sel = 'selected';      				
      		 }
      		?>
      		<option value="<?php echo $st->idStyle ?>" <?php echo $sel; ?>><?php echo $st->nameStyle ?></option>	
      		<?php 
      	}
      ?>
      </select>
      <?=form_error('cmbStyle')?>
    </div>
  </div>
  
  <div class="form-group">
    <label class="col-sm-4 control-label"><?=$this->lang->line('register_phone')?><em>*</em></label>
    <div class="col-sm-4">
      <input class="form-control" type="text" maxlength="15" id  = "phone" value="<?php echo(set_value('txtPhone'))?set_value('txtPhone'):$user->mem_phone; ?>" required name="txtPhone">
    </div>
    <div class="col-sm-4 visibility-radio">
    <?php if($this->pidGroup == 3) {?> 
      <div class="radio-inline">
						<input type="radio" name="rdaPhonvisiblity" value="Public" checked>Public
	 </div>
	 <div class="radio-inline">
						<input type="radio" name="rdaPhonvisiblity" value="Private" <?=$user->mem_phone_visibility=="Private"?"checked":''?>>Private
	 </div>
       <?php } ?>
    </div>
    <div class="col-sm-8 col-sm-offset-4">
    <?php echo form_error('txtPhone'); ?><small>212-999-0983, 212 999 2344</small>
    </div>
  </div>
   <div class="form-group">
    <label class="col-sm-4 control-label"><?=$this->lang->line('city')?><em>*</em></label>
    <div class="col-sm-8">
     <input type="text" name="cmbCity" id = "locality" maxlength="250" class="form-control" placeholder="Enter City Name" required value="<?=set_value('cmbCity')!=''?set_value('cmbCity'):$user->mem_city?>">
     <?=form_error('cmbCity')?>
   </div>
  </div>
    <div class="form-group">
    <label class="col-sm-4 control-label"><?=$this->lang->line('state')?><em>*</em></label>
    <div class="col-sm-8">
      <input type="text" name="cmbState" id="administrative_area_level_1" maxlength="250" class="form-control" placeholder="Enter State Name" required value="<?=set_value('cmbState')!=''?set_value('cmbState'):$user->mem_state?>" readonly>
    </div>
  </div>
  <div class="form-group">
     <label class="col-sm-4 control-label"><?=$this->lang->line('country');?><em>*</em></label>
    <div class="col-sm-8">
     <input type="text" name="cmbCountry" id="country" maxlength="250" class="form-control" placeholder="Enter Country Name" required value="<?=set_value('countries_id')!=''?set_value('countries_id'):$user->mem_country?>" readonly>
    
    </div> 
  </div>
  
  <?php if($user->idGrp==2){ ?>
  <div class="form-group">
    <label class="col-sm-4 control-label"><?=$this->lang->line('address')?><em>*</em></label>
    <div class="col-sm-8">
     	<input class="form-control" maxlength="150" type="text" value="<?php echo $user->mem_address; ?>" name="txtAddress" id="txtAddress" required>
     	<?=form_error('txtAddress')?>
    </div>
  </div>
  <?php } ?>
     
  <!-- <div class="form-group">
    <label class="col-sm-4 control-label"><?=$this->lang->line('url')?><em>*</em></label>
    <div class="col-sm-8">
      <?php echo SITEURL.'view';?><input class="form-control" maxlength="30"  type="text" value="<?php echo (set_value('txtUrl'))?set_value('txtUrl'):$user->url; ?>" name = "txtUrl">
      <?=form_error('txtUrl')?>
    </div>
  </div>-->
  
   <div class="form-group">
    <label class="col-sm-4 control-label"><?php if ($user->idGrp == VENUE_GROUP ){ echo $this->lang->line('venue_profile_description_label'); } else { echo $this->lang->line('profile_description_label');} ?></label>
    <div class="col-sm-8">
      <textarea id="editor1" name="txtDescr"><?php echo set_value('txtDescr')?set_value('txtDescr'):$this->utility->decodeText($user->descr); ?></textarea>
    </div>
  </div>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10 text-center">
      <button type="submit" class="btn btn-success"><?=$this->lang->line('save_changes')?></button>
      <a href="<?php echo $this->session->userdata("UserData")->dashboardURL;?>"  class="btn btn-default"><?=$this->lang->line('cancel')?></a>
    </div>
  </div>
</div>
</div>
</div>
  <?=form_close(); ?>
<script type="text/javascript" src="<?=EXTERNAL_PATH?>js/ckeditorbasic/ckeditor.js"></script>
<script type="text/javascript" src="<?=EXTERNAL_PATH?>js/chosen.jquery.js"></script>
<script type="text/javascript" src="<?=EXTERNAL_PATH?>js/jquery.maskedinput.min.js"></script>

<script type="text/javascript" src="<?=EXTERNAL_PATH?>js/modules/update_profile.js"></script>
<link href="<?=EXTERNAL_PATH?>css/chosen.css" rel="stylesheet" type="text/css">
