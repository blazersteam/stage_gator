<script>		
$(document).ready(function() {  
    $('.raty').raty({
  	  halfShow : true,
        score: function() {
          return $(this).attr('data-score');
        },
        readOnly: function() {
          return 'true becomes readOnly' == 'true becomes readOnly';
        }
  });
});
</script>

<!-- .content -->
<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>					</div>
		<div
			class="tipSuccessMessage alert alert-success alert-dismissible text-center"
			style="display: none;"><a href="javascript:void(0)" class="close alert-hide" data-dismiss="alert" aria-label="close">&times;</a><span class="retrunmessage"></span></div>

		<!--<div class="alert alert-success alert-dismissable">
               Consider visiting our brand new <a href="http://www.youtube.com/watch?v=cHdSyOyyM1g">YouTube channel</a> for in-depth tutorials on how to best utilize the site!
               </div>-->
      
<?php if (empty($userInfo->image) || !file_exists("./external/images/profiles/".$userInfo->image)){ ?>
	<?php if ($userInfo->idGrp==3 && ($this->pUserId == $userInfo->user_id)){ ?>
		<div class="alert alert-warning">
		<?=$this->lang->line('add_your_profile_pic_and_help')?>
		</div>
	<?php } ?>
<?php } ?>
<table class="table visible-lg" style="border: 0px; width: 100%;">
			<tr>
				<td>
<?php
echo form_open(SITEURL . $controllerName . '/updateprofile', 'id="form" enctype="multipart/form-data" ');
if (! empty($userInfo->image) && file_exists("./external/images/profiles/" . $userInfo->image)) {
    ?>
		<img src="<?=EXTERNAL_PATH."images/profiles/".$userInfo->image; ?>"
					style="width: 100%;">
	<?php } else if(!empty($userInfo->image) && filter_var($userInfo->image, FILTER_VALIDATE_URL) == true) { ?>
		<img src="<?=$userInfo->image?>" style="width: 100%;">
	<?php }else{ ?>
<img src="<?=EXTERNAL_PATH?>images/noimage.png" style="width: 100%;">	
<?php
}
if ($this->pUserId == $userInfo->user_id && $this->router->class == "profile") {
    ?>

<input type="file" style="display: none" name="userfile"
					onClick="">
					<button type="button" id="btnUploadImage"
						class="btn btn-success marginTo10"><?=$this->lang->line('change_image')?></button>
<?php
}
echo form_close();
?>
</td>
				<td style="text-align: left; width: 75%;" valign="top">
					<h2 style="margin-top: 0px;"><?=$this->utility->decodeText($userInfo->chrName);?></h2>
					<p><?=$this->utility->decodeText($userInfo->descr, false);?></p>
					<?php if($userInfo->idGrp == 2) {?>
					<br> <b><?=$this->lang->line('address')?>: </b></br>
					<?=$userInfo->mem_address;?></br> <B><?=$this->lang->line('contact_person')?>: </B><?=$userInfo->contact_person?><BR>
					<?php } ?>
					 <?php
    if ($this->pUserId == $userInfo->user_id || $userInfo->mem_phone_visibility == "Public") :
        ?>
                    <B><?=$this->lang->line('register_phone')?>: </B><?=(!empty($userInfo->mem_phone))?$userInfo->mem_phone:"N/A";?><BR>
                    <?php endif;?>
					
					<img src="<?=EXTERNAL_PATH?>images/style_icon.png" /> 
<?php
$i = 0;
foreach ($userStyle as $value) {
    if ($i) {
        echo ", ";
    }
    echo $value->nameStyle;
    $i ++;
}

?>
<BR> <img src="<?=EXTERNAL_PATH?>images/city_icon.png" /> 
<?=ucwords($userInfo->mem_city);?> <?=!empty($userInfo->mem_state) ? ','.$userInfo->mem_state : "";?>
<BR> <!-- Not display the start for fan -->
<?php if ($userInfo->idGrp!=4 && @$this->pidGroup != 4){ ?>
<a href="#rating"><img src="<?=EXTERNAL_PATH?>images/rating_icon.png" /></a>
					<div class="raty" readonly data-score="<?=$userInfo->rate?>"
						style="display: inline;">
						</a>
					</div>
<?php } ?>

<!-- Tip Here site --> 
<?php if($userInfo->idGrp==3 && $this->pidGroup== 4 && !empty($userInfo->stripe_account_id) && $userInfo->stripe_status=='Verified'){?>

<input type="hidden" name="perfomerUserid"
					value="<?php echo $this->utility->encode($userInfo->user_id);?>" />

					<BR> <span href="javascript:void(0);"
					class="btn btn-success tipper_popup" title="Tip here"><i class="fa fa-usd"></i></span>
					<!-- Tip Here site -->
<?php } ?>					
				</td>
		
		</table>
<?php echo form_open(SITEURL.$controllerName.'/updateprofile', 'id="form"'); ?>
<div class="hidden-lg">
<?php if (!empty($userInfo->image) && file_exists("./external/images/profiles/".$userInfo->image)){ ?>
		<img src="<?=EXTERNAL_PATH."images/profiles/".$userInfo->image; ?>"
				style="width: 100%;">
	<?php } else if(!empty($userInfo->image) && filter_var($userInfo->image, FILTER_VALIDATE_URL) == true) { ?>
		<img src="<?=$userInfo->image?>" style="width: 100%;">
	<?php }else{ ?>
<img src="<?=EXTERNAL_PATH?>images/noimage.png" style="width: 100%;">	
<?php
}
?>
	<h2 style=""><?=$userInfo->chrName?></h2>
			<br>
			<div>
				<img src="<?=EXTERNAL_PATH?>images/style_icon.png" />
	<?php
$i = 0;
foreach ($userStyle as $value) {
    if ($i) {
        echo ", ";
    }
    echo $value->nameStyle;
    $i ++;
}
?>
		</div>
			<div style="margin-top: 10px;">
				<img src="<?=EXTERNAL_PATH?>images/city_icon.png" /> 
	<?=ucwords($userInfo->mem_city);?> <?= !empty($userInfo->mem_state) ? ','.$userInfo->mem_state: ''?>	</div>

			<!-- Not display the start for fan -->
	<?php if ($this->pidGroup!=4 && $userInfo->idGrp != 4 ){ ?>
			<div style="margin-top: 10px;">
				<img src="<?=EXTERNAL_PATH?>images/rating_icon.png" />
				<div class="raty" readonly data-score="<?=$userInfo->rate?>"
					style="display: inline;"></div>

			</div>
			<?php }?>

<?php if($userInfo->idGrp==3 && $this->pidGroup== 4 && !empty($userInfo->stripe_account_id) && $userInfo->stripe_status=='Verified'){?>

<input type="hidden" name="perfomerUserid"
				value="<?php echo $this->utility->encode($userInfo->user_id);?>" />

			<div style="margin-top: 10px;">
				<!-- Tip Here mobile -->
				<a class="btn btn-success tipper_popup"
					href="javascript:void(0);" title="Tip here"><i class="fa fa-usd"></i></a>
				<!-- Tip Here mobile -->
			</div>
<?php }?>			
			<br>
<?php
if ($this->pUserId == $userInfo->user_id) {
    ?>		
    <!-- Not display this change image functionality in mobile -->
<!-- <button type="button" id="btnUploadImage" class="btn btn-success"><?=$this->lang->line('change_image')?></button>  -->
<?php
}
echo form_close();

?>
	</div>
<?php if($userInfo->idGrp != FAN_GROUP) { ?>
		<div class="page_header">
			<?php if(@$homeLocation && $this->pidGroup == FAN_GROUP) { ?>
    			<div class="row">
    				<div class="col-sm-8 text-left"><?=$this->lang->line('upcoming_shows')?></div>
    				<div class="col-sm-4 text-right">
    					<a class="btn btn-success" href="<?php echo SITEURL.'view/'.$url; ?>" title="Show All Events">Show All Events</a>
    				</div>
    			</div>
			<?php }else{ ?>
			    	<?=$this->lang->line('upcoming_shows')?>
			<?php } ?>
		</div>
		<div id="no-more-tables">

			<table
				class="table table-hover table-bordered table-striped dataTable cf"
				id="listEvent">
				<!-- table striped -->
				<thead class="cf">
					<tr>
						<th><?=$this->lang->line('event_title')?></th>
						<th><?=$this->lang->line('venue')?></th>
						<th><?=$this->lang->line('event_date')?> </th>
						<th class=""><?=$this->lang->line('start_time')?></th>
						<th class=""><?=$this->lang->line('end_time')?></th>
					</tr>
				</thead>
				<tbody>
   <?php
$i = 1;

foreach ($list as $l) {
    if (! event_end_time_crossed($l->schedule_date, $l->startTime)) {
        
        ?>
			<tr>
						<td data-title="Event Title"><a
							href="<?php echo $this->utility->generateOverviewUrl($l->idEvent,$l->scheduleId); ?>"><?php echo $l->title; ?></a>
						</td>
						<td data-title="Venue"><a
							href="<?php echo SITEURL.'view/'.$l->url; ?>"><?php echo ucwords($l->chrName); ?></a>
						</td>
						<td data-title="Event Date "><?php echo dateDisplay($l->schedule_date.''.$l->startTime); ?></td>
						<td data-title="Start Time" class=""><?php echo dateDisplay($l->schedule_date.''.$l->startTime,'h:i A'); ?></td>
						<td data-title="End Time" class=""><?php echo dateDisplay($l->end_date.''.$l->endTime,'h:i A'); ?></td>

					</tr>
			<?php
    }
}
?>

	</tbody>
			</table>
		</div>
		<?php }  ?>
		<script type="text/javascript"
			src="<?=EXTERNAL_PATH?>js/modules/venue_overview.js"></script>

	</div>
</div>
  <?php if ($this->pidGroup == FAN_GROUP ) { ?>
  <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript" src="<?=EXTERNAL_PATH?>js/jquery.blockUI.js"></script>
<script>

/*-------------------------------------------------------------------------*/
    $('.tipper_popup').on('click',function(){
        
    	 //disable the button
        $('.tipper_popup').attr('disabled',true);
        var tippingcontroller = '<?php echo $this->myvalues->tippingDetails['controller'];?>';
        var performerUserid = $('input[name="perfomerUserid"]').val();      

        $.ajax({
 	       type:"POST",
 	       url: SITEURL + tippingcontroller + "/checkStripeIdUser",     	        
 	       data:{'_csrf' : $('input[name="_csrf"]').val(),'performerUserid' : performerUserid},     	       
 	       beforeSend: function() { 	 	       
 	    	  blockUiDisplay();	    	 
 	    	  $("#TipPopup").html("");
 	       },
 	       success:function(data){
  	    	  $.unblockUI();
 		      //enable the button
 		       $(".tipper_popup").removeAttr("disabled");
 	    	   $("#TipPopup").html(data);
 	    	   $('.mainTital').html('<?php echo 'Give your valuable tip to '.$this->utility->decodeText($userInfo->chrName);?>');
 		       $("#tipper").modal('show'); 		       
 	       },
 	      statusCode: {
              401:function() { window.location.reload(); }
            }
 	       	
 	     });
    }); 
    
</script>

<script type="text/javascript" src="<?=EXTERNAL_PATH?>js/jquery.blockUI.js"></script>

<!-- tip dialog model div start -->
<div class="modal fade" id="tipper" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document" id="TipPopup"></div>
</div>
<!-- tip dialog model div start -->
<?php } ?>

