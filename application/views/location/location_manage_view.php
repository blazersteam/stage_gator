 <div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
					<div id="Tick"></div>
					<div class="page_header">
						<?=$title?>					
					</div>
	
<div id="no-more-tables">
	<table class="table table-bordered table-striped table-hover dataTable cf">
	<thead class="cf">
		<tr>
			<th><?=$this->lang->line('title')?></th>
			<th><?=$this->lang->line('register_phone')?></th>
			<th><?=$this->lang->line('city')?></th>
			<th><?=$this->lang->line('state')?></th>
			<th><?=$this->lang->line('country')?></th>
			<th><?=$this->lang->line('number_of_seats')?></th>
			<th><?=$this->lang->line('actions')?></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($list as $l){ ?>
		<tr>
			<td data-title="Title"><?php echo $this->utility->decodeText(ucwords($l->location_title)); ?></td>
			<td data-title="Phone"><?php echo $this->utility->decodeText($l->phone); ?></td>
			<td data-title="City"><?php echo $this->utility->decodeText($l->city); ?></td>
			<td data-title="State"><?php echo $this->utility->decodeText($l->state); ?></td>
			<td data-title="Country"><?php echo $this->utility->decodeText($l->country); ?></td>
			<td data-title="Occupancy"><?php echo $this->utility->decodeText($l->numSeats); ?></td>
			<td data-title="Actions">
				<a href="<?=SITEURL.$controllerName.'/edit/'.$this->utility->encode($l->location_id)?>" class="btn btn-sm btn-primary"><?=$this->lang->line('edit')?></a>
				<a href="<?=SITEURL.$controllerName.'/delete/'.$this->utility->encode($l->location_id)?>" onclick="return delete_location()" class="btn btn-sm btn-danger"><?=$this->lang->line('delete')?></a>
			</td>
		</tr>
		<?php } ?>
	</tbody>
	</table>
	
</div>
<a href="<?=SITEURL.$controllerName.'/add'?>" class="btn btn-success marginTo10"><?=$this->lang->line('add_location')?></a>
</div>
</div>

