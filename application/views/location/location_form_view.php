
<div class="content" style="height: 100%;">

				<div class="row">
					<div id="Tick"></div>
					<div class="page_header">
						<?=$title?>					
					</div>

	<?php echo $this->session->flashdata('myMessage'); ?>
	<?php echo form_open($actionUrl,'id="form" class="form-horizontal location-edit" novalidate'); ?>
 <div class="col-xs-12 col-sm-10">
 <div class="form-group">
    <label class="col-sm-4 control-label"><?=$this->lang->line('venue_name')?><em>*</em></label>
    <div class="col-sm-8">
      <input class="form-control" type="text" maxlength="250" value="<?=set_value('txtTitle')?set_value('txtTitle'):@$record->location_title?>"  name="txtTitle">
      <?=form_error('txtTitle')?>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-4 control-label"><?=$this->lang->line('register_phone')?><em>*</em></label>
    <div class="col-sm-8">
      <input class="form-control" type="text" maxlength="15" value="<?=set_value('txtPhone')?set_value('txtPhone'):@$record->phone?>"  name="txtPhone">
      <?=form_error('txtPhone')?>
    </div>
  </div>
  
  <div class="form-group">
    <label class="col-sm-4 control-label"><?=$this->lang->line('city')?><em>*</em></label>
    <div class="col-sm-8">
     <input type="text" name="cmbCity" id = "locality" maxlength="250" class="form-control" placeholder="Enter City Name" required value="<?=set_value('cmbCity')!=''?set_value('cmbCity'):@$record->city?>">
   </div>
  </div>
    <div class="form-group">
    <label class="col-sm-4 control-label"><?=$this->lang->line('state')?><em>*</em></label>
    <div class="col-sm-8">
      <input type="text" name="cmbState" id="administrative_area_level_1" maxlength="250" class="form-control" placeholder="Enter State Name" required value="<?=set_value('cmbState')!=''?set_value('cmbState'):@$record->state?>" readonly>
    </div>
  </div>
  <div class="form-group">
     <label class="col-sm-4 control-label"><?=$this->lang->line('country');?><em>*</em></label>
    <div class="col-sm-8">
     <input type="text" name="cmbCountry" id="country" maxlength="250" class="form-control" placeholder="Enter Country Name" required value="<?=set_value('countries_id')!=''?set_value('countries_id'):@$record->country?>" readonly>
   </div> 
    
  </div>
  <span for="cmbCity" class="help-inline text-danger"></span>
  <div class="form-group">
    <label class="col-sm-4 control-label"><?=$this->lang->line('address')?><em>*</em></label>
    
    <div class="col-sm-8">
     	<input class="form-control" type="text" maxlength="150" value="<?=set_value('txtAddress')?set_value('txtAddress'):@$record->address?>" name="txtAddress" >
     	<?=form_error('txtAddress')?>
 <small data-original-title="<?=$this->lang->line('tooltip_location')?>" data-placement="top" class="tooltips"><img src="<?php echo EXTERNAL_PATH."images/comment.png"; ?>" class="comment_box"/></small>
        <span for="txtAddress" class="help-inline text-danger" style="display: none;"></span>
    </div>
    
  </div>
  
  <div class="form-group">
    <label class="col-sm-4 control-label"><?=$this->lang->line('number_of_seats')?></label>
    <div class="col-sm-8">
     	<input class="form-control onlydecimal" type="text" maxlength="5" value="<?=set_value('txtNumSeats')?set_value('txtNumSeats'):@$record->numSeats?>" name="txtNumSeats" >
     	<?=form_error('txtNumSeats')?>
    </div>
  </div>
  
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10 text-center">
      <button type="submit" class="btn btn-success"><?=isset($record->location_id)? $this->lang->line('edit_location'):$this->lang->line('save_location')?></button>
      <a href="<?=SITEURL.'location/';?>" class="btn btn-default"><?=$this->lang->line('cancel')?></a>
    </div>
  </div>
  </div>
  </form>
<?php  //form_close(); ?>
</div>
</div>
<script>
     var VenueState =  '<?=@$record->state?@$record->state:@set_value('cmbState')?>';
     var VenueCity =  '<?=@$record->city?@$record->city:@set_value('cmbCity')?>';
</script>
<script type="text/javascript" src="<?=EXTERNAL_PATH?>js/modules/location_autocomplete.js"></script>
<script type="text/javascript" src="<?=EXTERNAL_PATH?>js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?=EXTERNAL_PATH?>js/modules/location.js"></script>


