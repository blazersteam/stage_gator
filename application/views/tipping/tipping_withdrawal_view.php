<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
	<div class="row">
		<div id="Tick"></div>
		<div class="page_header"><?=$title?></div>
		<?php echo form_open(SITEURL . $controllerName.'/requestWithdrawal','id="withdrawalfrm" class="form-horizontal" enctype="multipart/form-data"'); ?>
		
		<div class="col-xs-12 col-sm-10">

			<div class="form-group">
				<label class="col-sm-4 col-xs-8 control-label">Available Balance <em>*</em>
				</label>
				<div class="col-sm-4 col-xs-2">
					<label class="control-label">$<span id="availableBal"><?=showBalance($stripeBalance['available'][0]['Amount']/100)?></span></label>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-4 col-xs-8 control-label">Pending Balance <em>*</em>
				</label>
				<div class="col-sm-4 col-xs-2">
					<label class="control-label">$<?=showBalance($stripeBalance['pending'][0]['Amount']/100)?></label>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-4 control-label">Withdrawal Amount <em>*</em>
				</label>
				<div class="col-sm-5 col-md-4">
					<input type="text" name="txtAmount" maxlength="250"
						class="form-control withdrawalAmt"
						placeholder="Enter Amount To Withdrawal" required value="">
              <?=form_error('txtAmount')?>
            </div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label"> <small
					data-original-title="<?=$this->lang->line('tooltip_wtihdrawal_bank_card')?>"
					class="tooltips"><img
						src="<?=EXTERNAL_PATH."images/comment.png"; ?>"
						class="comment_box" /></small> <span for="txtName"
					class="help-inline text-danger"></span> Select Bank / Card <em>*</em>
				</label>
				<div class="col-sm-7 col-md-5">
					<div>
              <?php
            
            $bankAndCards = array_merge($bankAndCards ['banks'], $bankAndCards ['cards']);
            $count = count($bankAndCards);
            for ($i = 0; $i < $count; $i ++) {
                if ($bankAndCards [$i] ['object'] == 'bank_account') {
                    $icon = 'fa-university';
                    $name = $bankAndCards [$i] ['account_holder_name'];
                }
                else {
                    $icon = 'fa-credit-card-alt';
                    $name = $bankAndCards [$i] ['name'];
                }
                ?>
    
    
			<div class="account-listing">
							<div class="col-sm-1">
								<input id="cardInfo" name="cardInfo"
									value="<?=$this->utility->encode($bankAndCards[$i]['id'])?>"
									type="checkbox" class="cardClass">
							</div>
							<div class="col-sm-8">
								<h4><?=$name?></h4>
								<label><i class="fa <?=$icon?>"></i> ***************<?=$bankAndCards[$i]['last4']?> </label>
							</div>
						</div>
			<?php
            }
            ?>
              </div>
              <?=form_error('txtAmount')?>
            </div>
			</div>
			<div class="form-group">
				<div class="col-sm-4"></div>
				<div class="col-sm-7 col-md-5">
					<button type="button" class="btn btn-success" id="withdrawalBtn">Withdrawal
						Money</button>
				<!-- </div>
				<div class="col-sm-2 text-left"> -->
					<button type="button" class="btn btn-danger" id="cancel"> Cancel
						</button>
				</div>
			</div>
		</div>
		
	 <?php
echo form_close();
?>
    		
	    </div>
</div>
</div>
<script>
$(document).on('click','.cardClass',function(){

	 $('input.cardClass').not(this).prop('checked', false);
	
	//var totalCardClassCheckBox =  $('.cardClass').length;
	// if the checkbox is more than one then user can select only one checkbox.
	// we put this condition for user can uncheck the chekbox if only one chcek box.
	/*if(totalCardClassCheckBox != 1)
	{		
        $('.cardClass').prop('checked',false);
    	$(this).prop('checked',true);
	}*/
});

$(document).ready(function(){

	/* Used for validate the form*/
    $('#withdrawalfrm').validate({
        rules: {
            ".required": {
                required: true
            },
            "txtAmount":{
            	withdrawalAmt: true
            }
      },
    });
    
    jQuery.validator.addMethod("withdrawalAmt", function(value, element) {     
      return this.optional(element) || parseInt(value) < parseInt($('#availableBal').text());
    }, "you can not withdraw greater than available balance.");

});

$(document).on('click','#withdrawalBtn',function(){

	if($('.cardClass:checked').length != 1)
	{
		   alert('Please select a card to withdrawal your payment');
		   return false;
	}
	
	if( $('#withdrawalfrm').valid())
	{
		$("#withdrawalBtn").prop('disabled',true);
		$("#withdrawalfrm").submit();
	}
	
});


$(document).on('click','#cancel',function(){
	window.location.href='<?php echo SITEURL.$this->myvalues->tippingDetails ['controller'].'/tipPerformerBalance';?>';
});
</script>


