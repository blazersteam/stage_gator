<div class="content" style="height: 100%;">
	<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
			<div class="row">
              <div class="col-md-8 text-left"><?=$title?></div>
              <div class="col-md-4 text-right"><a class="btn btn-success" href="<?php echo SITEURL.$controllerName.'/enabledrecurringtip';?>" title="<?=$this->lang->line('enabled_recurring_tip')?>" ><?=$this->lang->line('enabled_recurring_tip')?></a></div>
            </div>
		</div>
		<div class="alert alert-success text-center myMessage"
			style="display: none"></div>

<?php
echo $this->session->flashdata('myMessage');
?>
  
<?php
if (isset($result)) {
    if (count($result) > 0) {
        ?>
		<div id="no-more-tables">
			<table
				class="table table-bordered table-hover table-striped display dataTable cf">
				<thead class="cf">
					<tr role="row">
						<th style="width: 7%">Sr #</th>
						<th>Performer Name</th>
						<th style="width: 20%">Tip Amount</th>
						<th >Date</th>
						<th>Transaction Id</th>
					</tr>
				</thead>

				<tbody role="alert" aria-live="polite" aria-relevant="all">
       <?php
        $i = 1;
        $class = "";
        foreach ($result as $r) {
            if ($i % 2 == 0) {
                $class = 'class="gradeA even"';
            }
            else {
                $class = 'class="gradeA odd"';
            }
            ?>
				<tr <?php echo $class; ?>>
						<td data-title="Sr #"><?php echo $i; ?></td>
						<td data-title="Performer Name" class=""><a
							href="<?php echo SITEURL."view/".$r->url; ?>"
							title="Performer Overview"><?php echo $this->utility->decodeText(ucwords($r->chrName)); ?></a>
						</td>
						<td data-title="Tip Amount" title="Tip Amount"><?php echo "$".$r->amount;?></td>
						<td data-title="Date"><?php echo dateDisplay($r->dt_created_datetime); ?></td>
						<td data-title="Transaction Id"><?php  echo $r->txn_id; ?></td>

					</tr>
				<?php
            $i ++;
        }
        ?>    
         </tbody>
			</table>
		</div>
	
<?php  } else { ?>
	<div class="alert alert-danger">No Record Found</div>	
<?php
    }
}
?>
</div>
</div>
<link href="<?=EXTERNAL_PATH?>css/dataTables.bootstrap.min.css"
	rel="stylesheet" type="text/css">
<script src="<?=EXTERNAL_PATH?>js/jquery.dataTables.min.js"> </script>