<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>	
		</div>

		<div class="panel-group tipping-accordian" id="accordion">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" id="accordion1"><span
							class="glyphicon glyphicon-globe"></span> Country Selection</a>
					</h4>
				</div>

				<div id="collapseOne" class="panel-collapse collapse in">
                    <?php echo form_open(SITEURL . $controllerName . '/createAccount', 'id="accountForm" class="form-horizontal"'); ?>
                        <div class="panel-body">
						<div class="row">

							<!-- do changes start-->


							<div class="form-group">
								<label class="col-sm-3 control-label">Country <em>*</em>
								</label>
								<div class="col-sm-8">
									<select class="form-control required" name="CmbCountries">
										<option value="">Select Country</option>
                                        <?php
                                        
                                        foreach ($countries as $data) {
                                            
                                            ?>
                                            <option selected
											value="<?=$data->countries_iso_code_2?>"><?=$data->countries_name?></option>
                                        <?php }  ?>
                                        </select>
								</div>
							</div>
							<!-- <div class="form-group">
												<input type="Radio" value="individual" name="ChkAccountType"
													checked />Individual
													 <input type="Radio" value="company"
													name="ChkAccountType" />Company
											</div> -->

							<div class="form-group">
								<label class="col-sm-3 control-label">Account Type <em>*</em></label>

								<div class="col-sm-8">
									<div class="radio-inline">
										<label> <input type="radio" name="ChkAccountType"
											value="individual" checked> Individual
										</label>
									</div>
									<div class="radio-inline">
										<label> <input type="radio" name="ChkAccountType"
											value="company"> Company
										</label>
									</div>
								</div>

							</div>

							<div class="col-md-3"></div>

							<div class="col-md-6">
								<button type="button" onclick="return accountForm();"
									class="btn btn-success btn-sm">Next</button>

							</div>


							<!-- do changes end-->

						</div>


					</div>
                            <?=form_close()?>
                        </div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" id="accordian2"><span
							class="glyphicon glyphicon-user"></span> Personal Details</a>
					</h4>
				</div>
				<div id="collapseTwo" class="panel-collapse collapse">
					<div class="panel-body">
								
									
                                <?php echo form_open(SITEURL . $controllerName.'/newAccount','id="stripeFields" class="form-horizontal" enctype="multipart/form-data"'); ?>
                                   
                                    <div class="row" id="accountHtml"></div>
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<button type="button" onclick="stripeFields();"
								class="btn btn-success btn-sm">Next</button>
							<button type="button"
								onclick="showparrent('collapseOne','collapseTwo');"
								class="btn btn-danger btn-sm">Back</button>

						</div>
                                    <?php
                                    echo form_close();
                                    ?>
                                
							</div>
				</div>
			</div>
   <?php if(@$isUpdate != 'YES' ) {?>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" id="accordian3"><span
							class="glyphicon glyphicon-credit-card"></span> Payment Options</a>
					</h4>
				</div>
				<div id="collapseTwo3" class="panel-collapse collapse">
					<div class="panel-body">

						<div class="alert alert-danger striperror1 col-sm-12"
							style="display: none;"></div>
					
		<?php echo form_open(SITEURL . $controllerName.'/bankAccountDetails','id="accountDetails" class="form-horizontal" enctype="multipart/form-data"'); ?>
                                   
                                    <div class="row" id="bankDetails">

							<div class="col-sm-5">
								<h4>Bank Account</h4>

								<div class="form-group">
									<label class="col-sm-4 control-label">Account Number </label>
									<div class="col-sm-8">
										<input type="text" name="account" id="account" maxlength="250"
											class="form-control required"
											placeholder="Enter Account Number" value="">
										
       <?=form_error('account');?>
    </div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Account Holder </label>
									<div class="col-sm-8">
										<input type="text" name="account_holder_name" maxlength="250"
											class="form-control required validBank"
											placeholder="Enter Account Holder Name " value=""
											readonly>
       <?=form_error('account_holder_name');?>
       
    </div>
								</div>

								<div class="form-group">
									<label class="col-sm-4 control-label">Currency </label>
									<div class="col-sm-8">
										<input type="text" name="currency" maxlength="250"
											class="form-control required validBank"
											placeholder="Enter Currency " value="USD" readonly>
       <?=form_error('legal_entity__address__state');?>
    </div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">Routing Number </label>
									<div class="col-sm-8">
										<input type="text" name="routing_number" maxlength="250"
											class="form-control required validBank"
											placeholder="Enter Routing number" value="">
       <?=form_error('routing_number');?>
       <?=form_close();?>
       
    </div>
								</div>

							</div>


							<div class="ver-divider col-sm-1 col-xs-12">
								<div class="div-vertical">
									<span>OR</span>
								</div>
							</div>
						
						
							<?php echo form_open(SITEURL . $controllerName.'/cardDetails','id="cardDetails" class="form-horizontal" enctype="multipart/form-data"'); ?>
														
							<div class="col-sm-6">
								<h4>Debit Card</h4>

								<div id="bankDetails">
									<div class="alert alert-danger striperror"
										style="display: none;"></div>

									<div class="form-group">
										<label class="col-sm-4 control-label">Card Number</label>
										<div class="col-sm-8">
											<input data-stripe="number" title="Please enter card number." type="tel" data-length="20"
												id="number" name="card_number" class="form-control required notDisplayArrow numeric maxLength"
												placeholder="Enter Card Number" maxlength="20" value="" />
										</div>
									</div>

									<div class="form-group">
										<label class="col-sm-4 control-label">CVC</label>
										<div class="col-sm-8">
											<input data-stripe="cvc" title="Please enter cvc number." type="tel"
												placeholder="Enter CVC Number" name="cvc" maxlength="7" data-length="7"
												class="form-control required validCard notDisplayArrow numeric maxLength" value="" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label">Exp. Month</label>
										<div class="col-sm-8">
											<input data-stripe="exp-month"
												title="Please enter expiry month." maxlength="2" type="number" data-length="2"
												placeholder="Enter Expiry Month" name="exp-month"
												class="form-control required validCard notDisplayArrow numeric maxLength" value="" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label">Exp. Year</label>
										<div class="col-sm-8">
											<input data-stripe="exp-year"
												title="Please enter expiry year." maxlength="4" type="number" data-length="4"
												placeholder="Enter Expiry Year" name="exp-year"
												class="form-control required validCard notDisplayArrow numeric maxLength" value="" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label">Currency </label>
										<div class="col-sm-8">
											<input type="text" data-stripe="currency"
												name="card_currency" maxlength="250"
												class="form-control required validCard"
												placeholder="Enter Currency " value="USD" readonly>
											<input type="hidden" data-stripe="name" name="card_holder"
												value="">

										</div>
									</div>
								</div>

							</div>
							        <?php
    echo form_close();
    ?>
                                  
                            </div>
						<div class="row">

							<div class="col-sm-11 text-center">
								<button type="button" id="cardBtn"
									onclick="accountAndCardDetails();"
									class="btn btn-success btn-sm">Next</button>
								<button type="button"
									onclick="showparrent('collapseTwo','collapseTwo3');"
									class="btn btn-danger btn-sm">Back</button>


							</div>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" id="accordian4"><span
							class="glyphicon glyphicon-check"></span> Terms And Conditions</a>
					</h4>
				</div>
				<div id="collapsefour" class="panel-collapse collapse">
					<div class="panel-body">
								
									
                                <?php echo form_open(SITEURL . $controllerName.'/stripFormData','id="confirmationForm" class="form-horizontal" enctype="multipart/form-data"'); ?>
                                   
                                    <div class="row" id="accountHtml"></div>
						<div class="col-md-3"></div>
						<div class="col-md-6">

							<p>
								<input type="checkbox" name="chkConfirmmation" class="required"
									id="chkConfirmmation" value="agree" /> By registering your
								account, you agree to stripe's <a
									href="https://stripe.com/docs/connect/updating-accounts#tos-acceptance"
									target="_blank">Services Agreement</a> and the <a
									target="_blank" href="https://stripe.com/connect-account/legal"
									reget="_blank">Stripe Connected Account Agreement</a>.
							</p>
							<label for="chkConfirmmation" class="error"
								style="display: block;"></label>
							<button type="button" id="submitData"
								onclick="confirmationForm();" class="btn btn-success btn-sm">Submit</button>
  	<?php $open =  @$isUpdate == 'YES' ? 'collapseTwo':'collapseTwo3'; ?>
  	<button type="button"
								onclick="showparrent('<?=$open?>','collapsefour');"
								class="btn btn-danger btn-sm">Back</button>
						</div>
                                    <?php
                                    echo form_close();
                                    ?>
                      </div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<link
	href="<?=EXTERNAL_PATH?>js/datetimepicker/bootstrap-datetimepicker.min.css"
	rel="stylesheet" type="text/css">
<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/datetimepicker/moment.min.js"></script>
<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/datetimepicker/bootstrap-datetimepicker.min.js"></script>


<script>


$(document).ready(function(){
	
	$.validator.addMethod("extension", function(value,element) {
		var newvalue = value.split('.').pop();
		newvalue = newvalue.toLowerCase();
		var  myarray = ["jpg", "png","jpeg"];
		return this.optional(element) || jQuery.inArray(newvalue, myarray) !== -1;
	}, "Please uplaoad valid (jpg,png,jpeg) file.");

	jQuery.validator.addMethod("zipcode", function(value, element) {
  	  return this.optional(element) || /^\d{5}(?:-\d{4})?$/.test(value);
  	}, "Please provide a valid zipcode.");

	// code for accept bank account or debit card detail in payment options
    $(".validBank,.validCard").prop('disabled', true);
    
    // code for disable the bank detail/ card detail display disable first time
   /* $("#accountDetails input").keypress(function() { */
   $("#accountDetails input").on('keyup keypress blur change', function(e) {
        var formId = $(this).attr('id');
        if (formId == 'number') {
            if ($('#number').val() == '') {
                $(".validCard").prop('disabled', true);
            } else {
                $("#account").removeClass('required');
                $('.validCard').prop('disabled', false);
            }
        } else {
            if ($("#account").val() == '') {
                $(".validBank").prop('disabled', true);
            } else {
                $("#number").removeClass('required');
                $('.validBank').prop('disabled', false);
            }
        }
    });

 // $("#accordian2,#accordian3,#accordian4").prop('disabled', true);
    

    $('form').each(function() { // attach to all form elements on page
        $(this).validate({
            rules: {
                ".required": {
                    required: true
                },
                'update[legal_entity__verification__document]':{
                    required:true,
        			extension: true 
               },
               'update[legal_entity__address__postal_code]':{
            	    zipcode: true,
               },
               'chkConfirmmation':{
            	   required: true
               }
            },
        });
    });

    

  $("#accordian3").click(function(){
    var fullName = $("input[name = 'update[legal_entity__first_name]']").val()+' '+$("input[name = 'update[legal_entity__last_name]']").val();
    $("input[name = 'account_holder_name'],input[name = 'card_holder']").val(fullName);
  });
  
});

function showparrent(forShow,forHide)
{	
	$('#'+forShow).collapse('toggle');
	$('#'+forHide).collapse('toggle');
	$('html,body').animate({
        scrollTop: $('#'+forHide).offset().top - 100},
        'slow');
}

function accountForm(){
	
    // display the block ui 
	blockUiDisplay();
	  
	// check for if country is selected or not 
	if($("#accountForm").valid()=== false)
    {
		// hide the block ui
		$.unblockUI();
	    return false;
    } 
    
    var countryMatch = '<?=$address->mem_country?>';
    var countryName = $("select[name = 'CmbCountries']").val();
    var accountType = $("input[name='ChkAccountType']:checked").val();
    var counntryFullName = $("select[name = 'CmbCountries'] option:selected").text();
    if(countryMatch.toLowerCase() != counntryFullName.toLowerCase())
    {
    	$.unblockUI();
    	$("#stripeFields button").hide();
    	$("#accountHtml").html('<div class="alert alert-danger">Selected country is not same as your current country kindly update country in your profile</div><br><button class="btn btn-danger btn-sm" onclick=showparrent("collapseOne","collapseTwo"); type="button">Back</button>');
    	showparrent('collapseTwo','collapseOne');
        return false;
    }

    url = SITEURL + CONTROLLER_NAME + '/loadView/' + accountType + '/' + countryName;
    $.get(url, {}, function(response) {
        
        // hide the block ui
    	$.unblockUI();
        $("#accountHtml").html(response);
        
        // set the max date property for birtrhdate : today-1 year
        var maxDateVar = new Date();
        maxDateVar.setFullYear(maxDateVar.getFullYear() - 1);
        $('#txtDate').datetimepicker({
            ignoreReadonly: true,
            useCurrent: false,
            viewMode: 'years',
            maxDate : maxDateVar,  
            viewDate: maxDateVar,	
            format: 'MM-DD-YYYY'
        });
        $("input[name = 'update[legal_entity__type]']").val(accountType);
        $("#accordian2").prop('disabled', false);
        showparrent('collapseTwo','collapseOne');
}).fail(function(newData) {
    if(newData.status == '401'){
    	window.location.reload();
        return true;
    }
    
});
    if($("#accountForm").valid())
    {
      $("#accordian2").prop('disabled',false);
    }
}
function stripeFields()
{
	 // display the block ui 
	 blockUiDisplay();
	 
	 var fullName = $("input[name = 'update[legal_entity__first_name]']").val()+' '+$("input[name = 'update[legal_entity__last_name]']").val();
	    $("input[name = 'account_holder_name'],input[name = 'card_holder']").val(fullName);

	       if ($("#stripeFields").valid()) {
    	   // hide the block ui
       	    $.unblockUI();
            var attr = $("#accordian3").text();
            if(attr == ''){
            	
            	showparrent('collapsefour','collapseTwo');
            	
             }else{
                                 
            	 showparrent('collapseTwo','collapseTwo3');
            
             }
         }else{
        	 $.unblockUI();
         }
}

function devideDate(){
    var getDate = $("#txtDate").val();
    getDate = getDate.split("-");
    $("input[name = 'update[legal_entity__dob__day]']").val(parseInt(getDate[1]));
    $("input[name = 'update[legal_entity__dob__month]']").val(parseInt(getDate[0]));
    $("input[name = 'update[legal_entity__dob__year]']").val(parseInt(getDate[2]));
}
function confirmationForm(){

	// check if checkbox for agree terms & conditions checked or not
	if($('#confirmationForm').valid()){

    	 $("#submitData").prop('disabled',true);
    	devideDate();
    	
        var other_data = $("#accountForm, #stripeFields,#confirmationForm,#accountDetails,#cardDetails").serializeArray();
       
        var fd = new FormData();
        var file_data = $('input[type="file"]')[0].files; // for multiple files
        for(var i = 0;i<file_data.length;i++){
            fd.append("file_"+i, file_data[i]);
        }
        $.each(other_data,function(key,input){
            fd.append(input.name,input.value);
        });

      // varible for check if update tipping view is loaded or not
      var checkUpdateTipping = '<?php echo isset($isUpdate) ?'Yes':'No';?>';
      fd.append('checkUpdateTipping',checkUpdateTipping);

      var action = SITEURL + CONTROLLER_NAME + '/stripFormData';
      var redirectUrl = SITEURL+'<?=$this->myvalues->profileDetails['controller']?>';
      
        $.ajax({
            url  : action,
            type : 'POST',
            data : fd,
            beforeSend: function() {		       
    	    	   blockUiDisplay();  
    	       },
            processData: false,
            contentType: false,
            success : function(data1) {
               
            	$.unblockUI();

            	var stringVar = data1,
                substringPersonal = "Personal :: ";
            	substringPayment = "Payment :: ";
           
            
            // check for error msg if conatin   'Personal ::' or 'Payment ::' word       
            if(data1.indexOf(substringPersonal) !== -1 || data1.indexOf(substringPayment) !== -1)
            {
                if(data1.indexOf(substringPersonal) !== -1)
                {
                	personalDetails();
                	var msgDetail = data1.replace('Personal :: ', '');
            		$('#stripeFields').find('.striperror_personal_dtl').show().text(msgDetail).show(); 
                }else{
                	$("#cardBtn").click();
                	var msgDetail = data1.replace('Payment :: ', '');                	
                	$('#collapseTwo3').find('.striperror1').show().text(msgDetail).show();
                }
            }else{
                //success msg
            	window.location.href = redirectUrl;   
            }
            	
                $("#submitData").prop('disabled',false);
                return false;
            },
            statusCode: {
                401:function() { window.location.reload(); }
              }
        
        });
        return false; 
	}else{
		return false;
	}
}
 

		</script>

<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/jquery.blockUI.js"></script>
<script type="text/javascript">
// Fill in your publishable key pk_test_GyQPfm5ADQzY8tHGNwXND5U9
Stripe.setPublishableKey('<?=STRIPE_PUBLISH_KEY_TIP?>');
//Stripe.setPublishableKey('pk_live_XR8QYaGqKo4QBA3iBGsvLACY');

var stripeResponseHandler = function(status, response) {
    var $form = $('#accountDetails');
    $.unblockUI();
    if (response.error) {
    	
        // Show the errors on the form
        $form.find('.striperror').show().text(response.error.message).show();
        
        // enable the card detail form text boxes
        $('#cardDetails').find('input').prop('disabled', false);
        $('.cardBtn').prop('disabled', false);
    } else {
    	$("#accordian4").prop('disabled', false).click();
        // token contains id, last4, and card type
        var token = response.id;
        // Insert the token into the form so it gets submitted to the server
        $form.append($("<input type='hidden' name='stripeToken'>").val(token));
        /* we only need data token so we it gets only token to php controller*/
        $(".validCard").prop('disabled', true);
        showparrent('collapsefour','collapseTwo3');
        // and re-submit
        //$form.get(0).submit();
    }
};
function accountAndCardDetails(){

	blockUiDisplay();
	
    var $form = $('#accountDetails');
        if (!$("#accountDetails,#cardDetails").valid()) {
        	$.unblockUI();
            return false;
        }       
        // Disable the submit button to prevent repeated clicks
        $('.cardBtn').prop('disabled', true);
        
        // if user have entered the card number then only we have to generate the token
        if($('#number').val()){
        	$.unblockUI(); 
            Stripe.card.createToken($form, stripeResponseHandler);
        } else {
        	$.unblockUI();
        	showparrent('collapsefour','collapseTwo3');
        }
        // Prevent the form from submitting with the default action
        return false;

}

// function used for open the personal detail accordion for error
function personalDetails(){
	$.unblockUI();
	 /* var $form = $('#accountDetails');
     if (!$("#accountDetails").valid()) {
     	//$.unblockUI();
         return false;
     } */

     // Disable the submit button to prevent repeated clicks
     $('.cardBtn').prop('disabled', true);
     showparrent('collapsefour','collapseTwo');

     // Prevent the form from submitting with the default action
     return false;
}


//used for not allow more than max charecter length.
$('input.maxLength').keypress(function(e) {
var max = $(this).attr("data-length");
    if (e.which < 0x20) {
        // e.which < 0x20, then it's not a printable character
        // e.which === 0 - Not a character
        return;     // Do nothing
    }
    if (this.value.length == max) {
        e.preventDefault();
    } else if (this.value.length > max) {
        // Maximum exceeded
        this.value = this.value.substring(0, max);
    }
});

//bind keypress for accept numeric value
/* $('.numeric').bind('keypress', function(e) {
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		return false;
	}
}); */
  </script>
