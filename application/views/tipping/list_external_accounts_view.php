<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
	<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>	
		</div>		
		<div class="account-btn text-right">
			<a class="btn btn-info" data-toggle="modal" data-target="#myModal">Add
				Bank</a> <a class="btn btn-success" data-toggle="modal"
				data-target="#myModal1">Add Card</a>
		</div>
		<div class="account-listing-main">
    <?php
    
$bankAndCards = array_merge($bankAndCards ['banks'], $bankAndCards ['cards']);
    
    $count = count($bankAndCards);
    for ($i = 0; $i < $count; $i ++) {
        if ($bankAndCards [$i] ['object'] == 'bank_account') {
            $icon = 'fa-university';
            $name = $bankAndCards [$i] ['account_holder_name'];
        }
        else {
            $icon = 'fa-credit-card-alt';
            $name = $bankAndCards [$i] ['name'];
        }
        ?>
			<div class="account-listing">
				<div class="col-sm-12">
					<h4><?=$name?> <a class="deletebtn pull-right"
						data-externalId=<?php echo $this->utility->encode($bankAndCards[$i]['id']);?>
						href="javascript:void(0);"> <i class="fa fa-trash danger"></i>
					</a></h4>
					<label><i class="fa <?=$icon?>"></i> ***************<?=$bankAndCards[$i]['last4']?> </label>
				</div>

				<div class="col-sm-3 col-md-2 text-right pull-right">
				<?php 
/*
               * if($bankAndCards[$i]['object'] == 'bank_account') {
               * if($bankAndCards[$i]['status'] == 'validated'){
               * ?>
               *
               * <button class="btn btn-info btn-sm"
               * onclick = "$('#getBankId').val($(this).attr('data-bank-id'))"
               * data-toggle="modal" data-target="#modelVerfiy" data-bank-id = "<?php echo $bankAndCards[$i]['id'];
               * ?>">
               * <i class="fa fa-check" ></i> Verify
               * </button>
               * <?php } } else{?>
               * <button class="btn btn-info btn-sm" disabled>
               * <i class="fa fa-check"></i> Verified
               * </button>
               * <?php }
               */
        ?>	
					
				</div>
			</div>
			
			<?php  } ?>
		</div>


		<!--Add Bank Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">Add Bank Account Details</h4>
					</div>
					<div class="modal-body">
					<?php echo form_open(SITEURL . $controllerName.'/addBankAccount','id="addBank" class="form-horizontal" enctype="multipart/form-data"'); ?>
                        	<div class="form-group">
							<label class="col-sm-4 control-label">Account Number <em>*</em>
							</label>
							<div class="col-sm-8">
								<input name="account" id="account" maxlength="250" tabindex="1" autofocus 
									class="form-control required"
									placeholder="Enter Account Number" value="" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Account Holder <em>*</em>
							</label>
							<div class="col-sm-8">
								<input name="account_holder_name" maxlength="250" tabindex="2" autofocus
									class="form-control required valid"
									placeholder="Enter Account Holder Name "
									value="<?=$this->session->userdata('UserData')->chrName?>"
									type="text" readonly>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label">Country <em>*</em>
							</label>
							<div class="col-sm-8">
								<select class="form-control required" name="CmbCountries" tabindex="3" autofocus  >
									<option value="">Select Country</option>
                                        <?php
                                        
foreach ($countries as $data) {
                                            if ($data->countries_name == $userInfo->mem_country)?>
                                            <option selected
										value="<?=$data->countries_iso_code_2?>"><?=$data->countries_name?></option>
                                        <?php }  ?>
                                        </select>
							</div>
						</div>
						<div class="form-group hide">
							<label class="col-sm-4 control-label">Currency <em>*</em>
							</label>
							<div class="col-sm-8">
								<input name="currency" maxlength="250" 
									class="form-control required valid"
									placeholder="Enter Currency " value="USD" type="hidden">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Routing Number <em>*</em>
							</label>
							<div class="col-sm-8">
								<input name="routing_number" maxlength="250" tabindex="4" autofocus
									class="form-control required valid"
									placeholder="Enter Routing number" value="" type="text">

							</div>
						</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="saveBankAccount" tabindex="5" />
						Save
						</button>
						<? echo form_close(); ?>
					</div>
				</div>

			</div>
		</div>


		<!--Add Card Modal -->
		<div class="modal fade" id="myModal1" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">Add Debit Card Details</h4>
					</div>
					<div class="modal-body">
						<div class="alert alert-danger striperror" style="display: none;"></div>
						<?php echo form_open(SITEURL . $controllerName.'/addNewCard','id="addCard" class="form-horizontal" enctype="multipart/form-data"'); ?>
								<div class="form-group">
							<label class="col-sm-4 control-label">Card Number <em>*</em></label>
							<div class="col-sm-8">
								<input data-stripe="number" title="Please enter card number." tabindex="1" autofocus
									id="number" name="card_number" class="form-control required notDisplayArrow numeric maxLength" type="tel" data-length="20"
									placeholder="Enter Card Number" maxlength="20" value="">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label">CVC <em>*</em></label>
							<div class="col-sm-8">
								<input data-stripe="cvc" title="Please enter cvc number." tabindex="2" autofocus
									name="cvc" maxlength="7" placeholder="Enter CVC Number" type="number" data-length="7"
									class="form-control required validCard notDisplayArrow numeric maxLength" value="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Exp. Month <em>*</em></label>
							<div class="col-sm-8">
								<input data-stripe="exp-month" placeholder="Enter Expiry Month" tabindex="3" autofocus
									title="Please enter expiry month." maxlength="2" type="number" data-length="2"
									name="exp-month" class="form-control required validCard notDisplayArrow numeric maxLength"
									value="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Exp. Year <em>*</em></label>
							<div class="col-sm-8">
								<input data-stripe="exp-year" placeholder="Enter Expiry Year" tabindex="4" autofocus
									title="Please enter expiry year." maxlength="4" name="exp-year" type="number" data-length="4"
									class="form-control required validCard notDisplayArrow numeric maxLength" value="">
							</div>
						</div>
						<div class="form-group hide">
							<label class="col-sm-4 control-label">Currency <em>*</em>
							</label>
							<div class="col-sm-8">
								<input data-stripe="currency" name="card_currency"
									maxlength="250" class="form-control required valid"
									placeholder="Enter Currency " value="USD" type="hidden">
                                <input type="hidden" data-stripe="name" name="card_holder"
												value="<?=$this->session->userdata('UserData')->chrName?>">
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="saveCard" tabindex="5" >Save</button>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="modelVerfiy" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">We have made 2
							transactions in your bank/card</h4>
					</div>
					<div class="modal-body">
						<div class="alert alert-danger striperror" style="display: none;"></div>
						<?php echo form_open(SITEURL . $controllerName.'/verifyBankCardAmount','id="verifyBankCard" class="form-horizontal" enctype="multipart/form-data"'); ?>
								<div class="form-group">
							<label class="col-sm-4 control-label">Amount 1 <em>*</em></label>
							<div class="col-sm-8">
								<input title="Please Enter First Amount."
									placeholder="Please Enter First Amount." id="txtAmountFirst"  
									name="txtAmountFirst" class="form-control required"
									maxlength="20" value="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Amount 1 <em>*</em></label>
							<div class="col-sm-8">
								<input title="Please Enter Second Amount."
									placeholder="Please Enter Second Amount." id="txtAmountSecond"
									name="txtAmountSecond" class="form-control required"
									maxlength="20" value="">
							</div>
							<input type="hidden" name="hdnBankCardId" value="" id="getBankId">
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-primary"
							id="verifyBankCardBtn">Save</button>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/jquery.blockUI.js"></script>

<script>
$(document).on('click','#saveBankAccount',function(){
	blockUiDisplay();  
	
	// disable the button
	 $("#saveBankAccount").prop('disabled',true);
	 
	    if(!$("#addBank").valid()){
	    	//$("#saveBankAccount").removeAttr("disabled",false);	    	    	
	    	$("#saveBankAccount").prop('disabled',false);
	    	$.unblockUI();
	    	   return false;	    
	    }
	    
	    $('#addBank').submit();
		
	});

// related to save card
$(document).on('click','#saveCard',function(){
	blockUiDisplay();  
	
    //disable the button
	$("#saveCard").prop('disabled',true);
	
    if(!$("#addCard").valid()){    	
    	$.unblockUI();
    	$("#saveCard").removeAttr("disabled",false);
    	 //$("#saveCard").prop('disabled',false);
        return false;
    }
    
    accountAndCardDetails();
	
});

// for delete the card /bank details
$(document).on('click','.deletebtn',function(){
	blockUiDisplay();  
	 var r = confirm("Do you really want to continue or delete?");
	 if (r == true) {
			     
	     $(".deletebtn").prop('disabled',true);  
	     window.location.href='<?php echo SITEURL.$controllerName."/deleteExternalDetails/"; ?>' + $(this).attr('data-externalid');
	 }
	 else {	    
		 $.unblockUI();
	     $(".deletebtn").prop('disabled',false);
	 }
});

$(document).on('click','#verifyBankCardBtn',function(){

	if(!$("#verifyBankCard").valid()){
	    return false;
	}
	$("#verifyBankCard").submit();
    return true;	
});

$(document).ready(function(){
	
$('form').each(function() { // attach to all form elements on page
    $(this).validate({
        rules: {
            ".required": {
                required: true
            }
      },
    });
});


});
</script>

<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
//Fill in your publishable key pk_test_GyQPfm5ADQzY8tHGNwXND5U9
Stripe.setPublishableKey('<?=STRIPE_PUBLISH_KEY_TIP?>');
//Stripe.setPublishableKey('pk_live_XR8QYaGqKo4QBA3iBGsvLACY');

var stripeResponseHandler = function(status, response) {
    var $form = $('#addCard');
    
    if (response.error) {
    	$.unblockUI();    	
        $('#saveCard').prop('disabled', false);
    	// Show the errors on the form
      $('.striperror').text(response.error.message).show();
      return false;    
        //$('.cardBtn').prop('disabled', false);
    } else {
    	
        // token contains id, last4, and card type
        var token = response.id;
        // Insert the token into the form so it gets submitted to the server
        $form.append($("<input type='hidden' name='stripeToken'>").val(token));
        $("input:text").prop('readonly',true);
        $form.get(0).submit();
      
    }
};
function accountAndCardDetails(){
    var $form = $('#addCard');
    
        // Disable the submit button to prevent repeated clicks
        Stripe.card.createToken($form, stripeResponseHandler);
        // Prevent the form from submitting with the default action
        return false;
}

//used for not allow more than max charecter length.
$('input.maxLength').keypress(function(e) {
var max = $(this).attr("data-length");
    if (e.which < 0x20) {
        // e.which < 0x20, then it's not a printable character
        // e.which === 0 - Not a character
        return;     // Do nothing
    }
    if (this.value.length == max) {
        e.preventDefault();
    } else if (this.value.length > max) {
        // Maximum exceeded
        this.value = this.value.substring(0, max);
    }
});

//bind keypress for accept numeric value
$('.numeric').bind('keypress', function(e) {
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		return false;
	}
});



//Set the auto focus when the add bank popup is shown.
$('#myModal').on('shown.bs.modal', function () {
    $('#account').focus();
});

//Set the auto focus when the add card popup is shown.
$('#myModal1').on('shown.bs.modal', function () {
    $('#number').focus();
});
  </script>
