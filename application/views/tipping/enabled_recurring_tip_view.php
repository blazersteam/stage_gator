<div class="content" style="height: 100%;">
	<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
			<?=$title?>
		</div>
		<div class="alert alert-success text-center myMessage"
			style="display: none"></div>

<?php
echo $this->session->flashdata('myMessage');
?>
  
<?php
if (isset($result)) {
    if (count($result) > 0) {
        ?>
		<div id="no-more-tables">
			<table
				class="table table-bordered table-hover table-striped display dataTable cf">
				<thead class="cf">
					<tr role="row">
						<th style="width: 7%">Sr #</th>
						<th>Performer Name</th>
						<th>Recurring Type</th>
						<th>Tip Amount</th>
						<th>Next Recurring Date</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>

				<tbody role="alert" aria-live="polite" aria-relevant="all">
       <?php
        $i = 1;
        $class = "";
        
        foreach ($result as $r) {
            if ($i % 2 == 0) {
                $class = 'class="gradeA even"';
            }
            else {
                $class = 'class="gradeA odd"';
            }
            $Nextdate = '';
            //create next date of reucrring if recurring type 'weekly' and recurring status 'active'
            if($r->recurring_status == 'Active' && $r->recurringType == 'weekly'){
                $Nextdate = date("Y-m-d H:i:s",strtotime($r->created_date."+7 days"));
            }
            //create next date of reucrring if recurring type 'monthly' and recurring status 'active'
            if($r->recurring_status == 'Active' && $r->recurringType == 'monthly'){
                $Nextdate = date("Y-m-d H:i:s",strtotime($r->created_date."+1 month"));
            }
            //if empty '$Nextdate' then we take default date(created date)
            if(empty($Nextdate)){
                $Nextdate = $r->created_date;
            }
            
            ?>
				<tr <?php echo $class; ?>>
						<td data-title="Sr #"><?php echo $i; ?></td>
						<td data-title="Performer Name" class=""><a href="<?php echo SITEURL."view/".$r->url; ?>" title="Performer Overview"><?php echo $this->utility->decodeText(ucwords($r->chrName)); ?></a></td>
						<td data-title="Recurring Type"><?php  echo ucfirst($r->recurringType); ?></td>
						<td data-title="Tip Amount" title="Tip Amount"><?php echo "$".$r->amount;?></td>
						<td data-title="Date"><?php echo dateDisplay($Nextdate); ?></td>
						<td data-title="Status"><?php  echo $r->recurring_status; ?></td>
						<td data-title="Action">
							<?php  if($r->recurring_status == 'Active'){?> <a class="btn btn-success subscription_cancel" data-url="<?php echo SITEURL.$controllerName.'/updateStripeSubscriptionStatus/'.$this->utility->encode($r->recurring_id).'/'.$this->utility->encode($r->performer_id).'/'.$r->subscriptionId;?>" href="javascript:void(0);" title="Cancel Recurring Tips" >Cancel</a> <?php }else{ echo '--'; } ?>
						</td>
					</tr>
				<?php
            $i ++;
        }
        ?>    
         </tbody>
			</table>
		</div>
	
<?php  } else { ?>
	<div class="alert alert-danger">No Record Found</div>	
<?php
    }
}
?>
</div>
</div>
<script>
<!--

//-->
$('body').on('click','.subscription_cancel',function(){
	 var redirectUrl = $(this).attr('data-url');
	 bootbox.confirm("Are you sure want to cancel subscription to this performer?", function(result) {
     		console.log(result);
     	if(result === true ){
     		window.location.href = redirectUrl;
     	}
     });
});


// function perfomer_action(){
// 	if (confirm("Are you sure want to cancel subscription to this performer?")){
// 		return true;
// 	} else {
// 		return false;
// 	}
// }
</script>
<link href="<?=EXTERNAL_PATH?>css/dataTables.bootstrap.min.css"
	rel="stylesheet" type="text/css">
<script src="<?=EXTERNAL_PATH?>js/jquery.dataTables.min.js"> </script>