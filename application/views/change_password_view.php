<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
					<div id="Tick"></div>
					<div class="page_header">
						<?=$title?>					</div>

<?php echo form_open($registerUrl, 'id="form" class="form-horizontal"'); ?>
<div class="col-xs-12 col-sm-10">

   <div class="form-group" id="new_email_div">
    <label class="col-sm-4 control-label" for="exampleInputEmail1"><?=$this->lang->line('new_email_address')?><em>*</em></label>
    <div class="col-sm-8">
    <input class="form-control" name="txtNew_email" id="new_email" required type="text" placeholder="Enter New Email Address" value = "<?=set_value('txtNew_email')?set_value('txtNew_email'):$user->user_name;?>"/>
    <?=form_error('txtNew_email')?>
    </div>
  </div>
  <div class="col-sm-offset-2 col-sm-10 text-center">
  <button type="submit" id = "mailSubmit" class="btn btn-success"><?=$this->lang->line('register_submit')?></button>
 </div>
  <div class="form-group">
   <div class="col-sm-4"></div>
   <div class="col-sm-8">
  	<input type="checkbox" name="change_email" value="1" id="change_email" />
    <label for="" style="font-weight: normal;"><?=$this->lang->line('change_email_address')?></label>
    </div>
  </div>
  
  <?php 
  
  echo form_open($registerUrl, 'id="form1" enctype="multipart/form-data" class="visible-lg"'); ?>
 
  
  <div class="form-group">
    <label class="col-sm-4 control-label" for="exampleInputPassword1"><?=$this->lang->line('current_password')?><em>*</em></label>
     <div class="col-sm-8">
    <input name="txtOld_password" type="password" required class="form-control" placeholder="Current Password" maxlength = "32">
    <?=form_error('txtOld_password')?>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-4 control-label" for="exampleInputPassword1"><?=$this->lang->line('new_password')?><em>*</em></label>
  <div class="col-sm-8">
    <input name="txtPassword" type="password" id="password" required class="form-control" placeholder="New Password" maxlength = "32">
    <?=form_error('txtPassword')?>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-4 control-label" for="exampleInputPassword1"><?=$this->lang->line('confirm_new_password')?><em>*</em></label>
   <div class="col-sm-8">
    <input name="txtConfirmPassword" type="password" required class="form-control" placeholder="Confirm New Password" maxlength = "32">
    <?=form_error('txtConfirmPassword')?>
    </div>
  </div>
  <div class="col-sm-offset-2 col-sm-10 text-center">
  <button type="submit" id = "btnChangePassword" class="btn btn-success"><?=$this->lang->line('register_submit')?></button>
  </div>
</div>
<?=form_close(); ?>
</div>
</div>
<script type="text/javascript" src="<?=EXTERNAL_PATH?>js/modules/change_passowrd_email.js"></script>
