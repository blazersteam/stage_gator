<?php 	 
    $d = $event->date." ".$event->startTime; 
	$event_time = strtotime($d);
	$current_time_default = time();
	$disabled = "disabled";
	if ($event_time<=$current_time_default){
		$disabled = '';
	}	
?>
<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>					
		</div>
<div id="no-more-tables">		
<table class="table table-bordered table-hover table-striped cf">
	<thead class="cf">
	<tr>
		<th>Sr #</th>
		<th>Performer</th>
		<th>Attendance</th>
		<th>Rank</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>
<?php 
	$i = 1;
	$flag= false;
	//dd($list);
	foreach($list as $l){
	 ?>	 
	 <tr>
	 	<?php echo form_open(SITEURL . $controllerName . '/rankPerformer', 'id="form'.$i.'" enctype="multipart/form-data" '); ?>
	 	<td data-title="Sr #"><?php echo $i++; ?></td>
	 	<td data-title="Performer"><?php echo $l->chrName; ?></td>
	 	<td data-title="Attendance">
	 	<?php if($permission->doAttandance == 1){?>
	 		<?php 
	 			$disable_select_box = '';
	 			if ($l->attandance!='N/A'){
	 				$disable_select_box = 'disabled';
	 			}
	 		?>
	 		<select name="attandance" class="form-control attandance" id-rank="ranking-<?php echo $l->idRegister; ?>" <?php echo $disabled; ?> <?php echo $disable_select_box; ?> required>
	 		<option></option>
	 		<option value="2" <?php echo ($l->attandance=="Present")?'selected':''; ?>>Present</option>
	 		<option value="3" <?php echo ($l->attandance=="Excused Absent")?'selected':''; ?>>Excused - Absent</option>
	 		<option value="4" <?php echo ($l->attandance=="Un-Excused Absent")?'selected':''; ?>>Un-Excused - Absent</option>
	 		</select>
	 		<?php }?>
	 	</td>
	 	<td data-title="Rank">
	 	<?php if($permission->doRank == 1){?>
	 		<input type="hidden" name="rating" value="<?php echo (!is_null($l->rating) && is_numeric($l->rating))?$l->rating:0; ?>" id="hidden-ranking-<?php echo $l->idRegister; ?>" />
	 		<div class="raty" id="ranking-<?php echo $l->idRegister; ?>" data-score="<?php echo (!is_null($l->rating) && is_numeric($l->rating))?$l->rating:0; ?>" style="width: 100px;"></div>
	 	<?php }?>	
	 	</td>
	 	<td data-title="Action">
	 	<!--check if login user have rank or do attendace functionality -->
	 	<?php if($permission->doRank == 1 || $permission->doAttandance){?>
	 		<input type="hidden" name="performer" value="<?php echo $l->idPerformer; ?>" />
	 		<input type="hidden" name="idEvent" value="<?php echo $l->idEvent; ?>" /> 
	 		
	 		<?php if ($l->attandance=="N/A"){ ?>
	 		<input type="submit" id = "submitButton-<?=$l->idRegister?>" class="btn btn-sm btn-primary <?php echo $disabled; ?>" onclick = "return confirm('Are you sure you want to save attendance/ranking?');" value="Update" disabled = "disabled" />
	 		<?php } ?>
	 		
	 		<?php 
	 		
	 			if ($l->attandance=='N/A' && is_null($l->rank)){
	 				$falg=true;
	 				?>
	 				<span class="badge bg-danger">Pending</span>	 				
	 				<?php
	 			} 

	 			 else {
	 				?>
	 				<span class="badge bg-success">Updated</span>
	 				<?php 
	 			}
	 	 }?>
	 	</td>
	 	
	 	<?php echo form_close(); ?>
	 </tr>
	 <?php 
	}

?>
</tbody>
</table>
</div>
<?php 
	if (!$flag){
	?>
	<!-- <a href="javascript:void(0)" class="btn btn-success pull-right updateAllRakingBtn"><span id="updateAllRakingBtnSpan">Update All</span></a>  -->
	<?php 
	}
?>
<?php 

if ($i==1){
	echo "No Performer Registered for Event";
}
?>
</div>
</div>
<script type="text/javascript" src="<?=EXTERNAL_PATH?>js/modules/rank_performer.js"></script>



