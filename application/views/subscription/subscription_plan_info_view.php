<?php
$loadView = 'General';

if (! empty($this->input->cookie('fcmIdToken'))) {
    $fcmCookie = $this->utility->decode($this->input->cookie('fcmIdToken', TRUE));
    $tokenArray = explode("||||", $fcmCookie);
    if (isset($tokenArray [0]) && $tokenArray [0] == 'Iphone') {
        // view load only for iphone
        $loadView = 'IphoneView';
    }
}
?>

<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage');?>
	<?php if($this->pidGroup == "3"):?>
	<div class="row">
		<div id="Tick"></div>
		<div class="page_header">Upgrade Your StageGator Account</div>

		<!-- YOUR CONTENT COMES HERE -->
		<div class="plan">
			<div class="col-xs-12 col-sm-offset-4 col-sm-8 nopadding">
				<div class="col-xs-4 my_planHeader my_plan1">
					<div class="my_planTitle">Beginner</div>
					<div class="my_planPrice">Free</div>
					<div class="my_planDuration"></div>
					<div class="plan-buybtn">
					   <?php if(!in_array($active_plan, ["2","3","7","8"])): ?>
					   <a type="button" class="btn btn-default">Already Owned</a>
					   <?php else: ?>
					   <a type="button"
							href="<?=SITEURL . $controllerName ."/unsubscribe"; ?>"
							class="btn btn-default">Downgrade</a>
					   <?php endif; ?>
					</div>
				</div>
				<div class="col-xs-4 my_planHeader my_plan2">
					<div class="my_planTitle">Amateur</div>
					<div class="my_planPrice">$4.99/ Month</div>
					<br>
					<div class="my_planDuration">(10% discount for full year)</div>
					<div class="plan-buybtn">
					    <?php if(!in_array($active_plan, ["2","3","7", "8"])): ?>
						<a type="button" class="btn btn-default"
							onclick="loadPupUp('<?=$this->utility->encode("2");?>', '<?=$this->utility->encode("7");?>', 4.99, 53.89);">Upgrade</a>
						<?php elseif(in_array($active_plan, ["3","8"])): ?>
						<a type="button" class="btn btn-default"
							onclick="loadPupUp('<?=$this->utility->encode("2");?>', '<?=$this->utility->encode("7");?>', 4.99, 53.89);">Downgrade</a>
						<?php else: ?>
						<a type="button" class="btn btn-default">Already Owned</a>
						<?php endif;?>
					</div>
				</div>
				<div class="col-xs-4 my_planHeader my_plan3">
					<div class="my_planTitle">Pro</div>
					<div class="my_planPrice">$9.99/ Month</div>
					<br>
					<div class="my_planDuration">(10% discount for full year)</div>
					<div class="plan-buybtn">
					   <?php if(!in_array($active_plan, ["3","8"])): ?>
					   <a type="button" class="btn btn-default"
							onclick="loadPupUp('<?=$this->utility->encode("3");?>', '<?=$this->utility->encode("8");?>', 9.99, 107.89);">Upgrade</a>
					   <?php else: ?>
					   <a type="button" class="btn btn-default">Already Owned</a>
					   <?php endif;?>
					</div>
				</div>

			</div>
			<div class="my_featureRow">
				<div class="col-xs-12 col-sm-4 my_feature">Unlimited open Mic Access
				</div>
				<div class="col-xs-12 col-sm-8 nopadding">

					<div class="col-xs-4 col-sm-4 my_planFeature my_plan1">
						<span> 2/month</span>
					</div>
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan2">
						<i class="fa fa-check"></i>
					</div>
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan3">
						<i class="fa fa-check"></i>
					</div>

				</div>
			</div>
			<div class="my_featureRow">
				<div class="col-xs-12 col-sm-4 my_feature">Unlimited Showcase Access
				</div>
				<div class="col-xs-12 col-sm-8 nopadding">
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan1">
						<i class="fa fa-times"></i>
					</div>
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan2">
						<i class="fa fa-check"></i>
					</div>
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan3">
						<i class="fa fa-check"></i>
					</div>

				</div>
			</div>
			<div class="my_featureRow">
				<div class="col-xs-12 col-sm-4 my_feature">Tips</div>
				<div class="col-xs-12 col-sm-8 nopadding">

					<div class="col-xs-4 col-sm-4 my_planFeature my_plan1">
						<i class="fa fa-check"></i> <span>(-8%+$0.30)</span>
					</div>
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan2">
						<i class="fa fa-check"></i> <span>(-4.9%+$0.30)</span>
					</div>
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan3">
						<i class="fa fa-check"></i> <span>(-3.9%+$0.30)</span>
					</div>
				</div>
			</div>

			<div class="my_featureRow">
				<div class="col-xs-12 col-sm-4 my_feature">Fans Bites</div>
				<div class="col-xs-12 col-sm-8 nopadding">

					<div class="col-xs-4 col-sm-4 my_planFeature my_plan1">
						<i class="fa fa-times"></i>
					</div>
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan2">
						<i class="fa fa-check"></i>
					</div>
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan3">
						<i class="fa fa-check"></i>
					</div>

				</div>
			</div>



			<div class="my_featureRow">
				<div class="col-xs-12 col-sm-4 my_feature">Push Notification For
					Your Fans</div>
				<div class="col-xs-12 col-sm-8 nopadding">

					<div class="col-xs-4 col-sm-4 my_planFeature my_plan1">
						<i class="fa fa-times"></i>
					</div>
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan2">
						<i class="fa fa-check"></i>
					</div>
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan3">
						<i class="fa fa-check"></i>
					</div>

				</div>
			</div>
		</div>
		<!-- YOUR CONTENT ENDS HERE -->
<?php
    /* Load the button only in safari browser */
    if ((! empty($this->session->userdata('loadview')) && $this->session->userdata('loadview') == 'IphoneView') && empty($this->input->cookie('fcmIdToken'))) {
        echo "<center><a class ='btn-success btn' style= 'margin:10px' href='stagegator://" . SITEURL . $controllerName . "/history'>Back to app</a></center>";
    }
    ?>
	</div>
    
    <?php  elseif($this->pidGroup == "2"):?>
    <div class="row">
		<div id="Tick"></div>
		<div class="page_header">Upgrade Your StageGator Venue Account</div>

		<!-- YOUR CONTENT COMES HERE -->
		<div class="plan">
			<div class="col-xs-12 col-sm-offset-3 col-sm-9 nopadding">
				<div class="col-xs-4 my_planHeader my_plan1">
					<div class="my_planTitle">Beginner</div>
					<div class="my_planPrice">Free</div>
					<div class="my_planDuration"></div>
					<div class="plan-buybtn">
					    <?php if(!in_array($active_plan, ["5","6","9","10"])): ?>
						<a type="button" class="btn btn-default">Already Owned</a>
						<?php else:?>
						<a type="button"
							href="<?=SITEURL . $controllerName ."/unsubscribe"; ?>"
							class="btn btn-default">Downgrade</a>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-xs-4 my_planHeader my_plan2">
					<div class="my_planTitle">Amateur</div>
					<div class="my_planPrice">$4.99/ Month</div>
					<br>
					<div class="my_planDuration">(10% for full year)</div>
					<div class="plan-buybtn">
					   <?php if(!in_array($active_plan, ["5","9", "6", "10"])): ?>
						<a type="button" class="btn btn-default"
							onclick="loadPupUp('<?=$this->utility->encode("5");?>', '<?=$this->utility->encode("9");?>', 4.99, 53.89);">Upgrade</a>
						<?php elseif(in_array($active_plan, ["6","10"])): ?>
						<a type="button" class="btn btn-default"
							onclick="loadPupUp('<?=$this->utility->encode("5");?>', '<?=$this->utility->encode("9");?>', 4.99, 53.89);">Downgrade</a>
						<?php else: ?>
						<a type="button" class="btn btn-default">Already Owned</a>
						<?php endif;?>
					</div>
				</div>
				<div class="col-xs-4 my_planHeader my_plan3">
					<div class="my_planTitle">Pro</div>
					<div class="my_planPrice">$9.99/ Month</div>
					<br>
					<div class="my_planDuration">(recommended 10% for full year)</div>
					<div class="plan-buybtn">
						<?php if(!in_array($active_plan, ["6","10"])): ?>
						<a type="button" class="btn btn-default"
							onclick="loadPupUp('<?=$this->utility->encode("6");?>', '<?=$this->utility->encode("10");?>', 9.99, 107.89);">Upgrade</a>
						<?php else: ?>
						<a type="button" class="btn btn-default">Already Owned</a>
						<?php endif;?>
					</div>
				</div>
			</div>
			<div class="my_featureRow">
				<div class="col-xs-12 col-sm-3 my_feature">Open Mics</div>
				<div class="col-xs-12 col-sm-9 nopadding">

					<div class="col-xs-4 col-sm-4 my_planFeature my_plan1">
						<span> 4/month</span>
					</div>
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan2">
						<span>Unlimited</span>
					</div>
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan3">
						<span>Unlimited</span>
					</div>

				</div>
			</div>
			<div class="my_featureRow">
				<div class="col-xs-12 col-sm-3 my_feature">Performers</div>
				<div class="col-xs-12 col-sm-9 nopadding">
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan1">
						<span>Limit 10</span>
					</div>
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan2">
						<span>Limit 20</span>
					</div>
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan3">
						<span>Unlimited</span>
					</div>

				</div>
			</div>
			<div class="my_featureRow">
				<div class="col-xs-12 col-sm-3 my_feature">Showcases</div>
				<div class="col-xs-12 col-sm-9 nopadding">

					<div class="col-xs-4 col-sm-4 my_planFeature my_plan1">
						<i class="fa fa-times"></i>
					</div>
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan2">
						<span> 4/month</span>
					</div>
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan3">
						<span> Unlimited</span>
					</div>

				</div>
			</div>



			<div class="my_featureRow">
				<div class="col-xs-12 col-sm-3 my_feature">Fan Bites</div>
				<div class="col-xs-12 col-sm-9 nopadding">

					<div class="col-xs-4 col-sm-4 my_planFeature my_plan1">
						<span>Fan Bites</span>
					</div>
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan2">
						<span>Fan Bites</span>
					</div>
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan3">
						<span>Fan Bites Plus</span>
					</div>

				</div>
			</div>

			<div class="my_featureRow">
				<div class="col-xs-12 col-sm-3 my_feature">Support</div>
				<div class="col-xs-12 col-sm-9 nopadding">

					<div class="col-xs-4 col-sm-4 my_planFeature my_plan1">
						<span>Email & Live Chat</span>
					</div>
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan2">
						<span>Email & Live Chat</span>
					</div>
					<div class="col-xs-4 col-sm-4 my_planFeature my_plan3">
						<span>Email, Live Chat and Phone Support</span>
					</div>

				</div>
			</div>


		</div>
		<!-- YOUR CONTENT ENDS HERE -->
<?php
    /* Load the button only in safari browser */
    if ((! empty($this->session->userdata('loadview')) && $this->session->userdata('loadview') == 'IphoneView') && empty($this->input->cookie('fcmIdToken'))) {
        echo "<center><a class ='btn-success btn' style= 'margin:10px' href='stagegator://" . SITEURL . $controllerName . "/history'>Back to app</a></center>";
    }
    ?> 
	</div>
    <?php endif;?>
    
    <!-- Modal -->
	<div class="modal fade choose-plan" id="myModal" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Choose Subscription Type</h4>
				</div>
				<div class="modal-body">

					<div class="form-group">

						<div class="input-group">
							<div id="radioBtn" class="btn-group">
								<a class="btn btn-primary btn-sm active plan1"
									data-price="price1" data-toggle="happy" data-title="A">Monthly</a>
								<a class="btn btn-primary btn-sm notActive plan2"
									data-price="price2" data-toggle="happy" data-title="B">Yearly</a>
							</div>
						</div>
						<input type="hidden" id="happy" /> <label
							class="price price1 control-label"></label> <label
							class="price price2 control-label"></label>
					</div>


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary gotoPayment">Subscribe
						Plan</button>
				</div>
			</div>
		</div>
	</div>

</div>

<script>
function loadPupUp(planId1, planId2, price1, price2)
{
	$(".plan1").attr("data-title", planId1);
	$(".plan2").attr("data-title", planId2);
	$(".price1").text(" Price: $" + price1);
	$(".price2").text(" Price: $" + price2);
	$("#radioBtn a:eq(0)").click();
	$('#myModal').modal("show");
}

$(document).on("click", "#radioBtn a", function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    var price = $(this).data('price');

    $(".price").hide();
    $("." + price).show();
    $('#'+tog).val(sel);
    
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
});


$(document).on("click", ".gotoPayment", function(){
    <?php
    // if apple phone login with app then go to the IphoneView
    if ($loadView == 'IphoneView') {
        if (isset($tokenArray [0]) && $tokenArray [0] == 'Iphone' && isset($tokenArray [1]) && ! empty($tokenArray [1])) {
            // view load only for iphone
            // $fcmToken = $tokenArray [1];
            // set the current Time stemp
            $timeStemp = strtotime($this->my_model->datetime);
            $user_id = $this->pUserId;
            $accessToken = $this->utility->encode($timeStemp . '||||' . $user_id);
            ?>    
        	window.location.href = SITEURL + "apple_payment/subscribePlan/" + $("#happy").val()+'<?php echo "/".$accessToken;?>';
        	    <?php
        }
        ?>
    <?php }else{?>
    window.location.href = SITEURL + CONTROLLER_NAME + "/subscribePlan/" + $("#happy").val();
    <?php }?>

})
</script>
