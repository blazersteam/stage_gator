<!-- Load this view is only for safari browser -->
<?php if(empty($this->input->cookie('fcmIdToken'))){?>
<div class="content" style="height: 100%;">
	<div class="row">
		<div class="page_header"></div>
					<?php if($this->session->userdata('subscription_status') == 'success'){?>
					<h1>Thank you for subscription!</h1>
		<caption>
			Your Subscription has been updated successfully.<br>
		</caption>
		            <?php }else if($this->session->userdata('subscription_status') == 'error'){?>
		            <h2>Your Session has expired.</h2>
		            <?php }else if($this->session->userdata('subscription_status') == 'cancelled'){?>
		            <h2>Your request for subscription has cancelled.</h2>
		            <?php }?>
		            <caption>Please wait 2 seconds, we are redirecting on you
			our application or you can click on "Back to app" to redirect on our
			application.</caption>

		<caption><?php echo "<center><a class ='btn-success btn' style= 'margin:10px' href='stagegator://" . SITEURL ."subscription'>Back to app</a></center>";?></caption>
	</div>
</div>
 <?php header('Refresh:2;url=stagegator://'.SITEURL.'/subscription/history');?>
<?php

}
else {
    // added the above condition to not load this view in iphone app
    // if loaded in iphone then redirect to the subscription page in app.
    header('Refresh:0 ;url=stagegator://' . SITEURL . '/subscription/history');
}
?>