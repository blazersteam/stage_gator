<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<?php
$loadView = 'General';
// if session generted for Iphone device then set the Iphone View to display
if (! empty($this->session->userdata('loadview')) && $this->session->userdata('loadview') == 'IphoneView') {
    $loadView = 'IphoneView';
}

?>

<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
	<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
            <?=$title?>							
        </div>

		<!-- YOUR CONTENT COMES HERE -->
		<div class="col-xs-10">
			<!-- Now change all the name attributes on your credit card inputs to data-stripe instead -->
            <?php echo form_open(SITEURL . $controllerName."/create_payment", ["class" => "form-horizontal", "method" => "POST", "id" => "payment-form"])?>
               <!-- Add a section to display errors if you want -->
			<div class='alert alert-danger payment-errors text-center'
				style="display: none;"></div>
			<div class="col-xs-12">
				<div class="form-group">
					<label class="col-sm-3 control-label">Plan Name: </label>
					<div class="col-sm-8">
						<span><?=$planName;?></span>
					</div>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="form-group">
					<label class="col-sm-3 control-label">Plan Price: </label>
					<div class="col-sm-8">
						<span><?="$" . $price;?></span>
					</div>
				</div>
			</div>
               
                <?php
                if ($loadView == 'General') {
                    ?>
               
               <div class="col-xs-12">
				<div class="form-group">
					<label class="col-sm-3 control-label">Card Number: </label>
					<div class="col-sm-8">
						<input data-stripe="number" title="Please enter card number."
							id="number" name="number" class="form-control required"
							maxlength="20" value="" />
					</div>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="form-group">
					<label class="col-sm-3 control-label">CVC: </label>
					<div class="col-sm-8">
						<input data-stripe="cvc" title="Please enter cvc number."
							name="cvc" maxlength="7" class="form-control required" value="" />
					</div>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="form-group">
					<label class="col-sm-3 control-label">Exp. Month: </label>
					<div class="col-sm-8">
						<input data-stripe="exp-month" title="Please enter expiry month."
							maxlength="2" name="exp-month" class="form-control required"
							value="" />
					</div>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="form-group">
					<label class="col-sm-3 control-label">Exp. Year: </label>
					<div class="col-sm-8">
						<input data-stripe="exp-year" title="Please enter expiry year."
							maxlength="4" name="exp-year" class="form-control required"
							value="" />
					</div>
				</div>
			</div>
			<input type="hidden" data-stripe="name"
				value="<?=$this->session->userdata('UserData')->chrName?>" /> <input
				type="hidden" name="checkValidation" value="Yes" />

			<div class="col-xs-12">
				<div class="form-group">
					<label class="col-sm-3 control-label"></label>
					<div class="col-sm-8">
						<button type="submit" class="btn btn-primary">Submit Payment</button>
						<a type="button"
							onclick="window.location.href='<?=SITEURL . $controllerName; ?>'"
							class="btn btn-danger">Cancel</a>
					</div>
				</div>
			</div>
               
               <?php }elseif($loadView == 'IphoneView'){?>
               
                   <!-- Load the view for iphone device only -->
			<div class="col-xs-12">
				<div class="form-group">
					<label class="col-sm-3 control-label"></label>
					<div class="col-sm-8">
						<button id="apple-pay-button" type="button"
							name="apple-pay-button"></button>
						<a type="button"
							onclick="window.location.href='<?=SITEURL . 'apple_payment/subscription_cancelled'; ?>'"
							class="btn btn-danger">Cancel</a>
					</div>
				</div>
			</div>
               
               <?php }?>
            </form>
		</div>
		<!-- YOUR CONTENT ENDS HERE -->

	</div>
</div>

<script type="text/javascript">
                
    // Fill in your publishable key
   Stripe.setPublishableKey('<?=STRIPE_PUBLISH_KEY;?>');
   //Stripe.setPublishableKey('pk_live_XR8QYaGqKo4QBA3iBGsvLACY');

<?php
if ($loadView == 'General') {
    ?>
   
    var stripeResponseHandler = function(status, response) {
      var $form = $('#payment-form');

      if (response.error) {
        // Show the errors on the form
        $form.find('.payment-errors').text(response.error.message).show();
        $form.find('button').prop('disabled', false);
      } else {
        // token contains id, last4, and card type
        var token = response.id;
        // Insert the token into the form so it gets submitted to the server
        $form.append($("<input type='hidden' name='stripeToken'>").val(token));
        // and re-submit
        $form.get(0).submit();
      }
    };

    jQuery(function($) {
      $('#payment-form').submit(function(e) {
          if(!$("#payment-form").valid())
          {
              return false;
          }
        var $form = $(this);

        // Disable the submit button to prevent repeated clicks
        $form.find('button').prop('disabled', true);

        Stripe.card.createToken($form, stripeResponseHandler);

        // Prevent the form from submitting with the default action
        return false;
      });
    });
    
<?php }elseif($loadView == 'IphoneView'){?>
     
    Stripe.applePay.checkAvailability(function(available) {
    	  if (available) {
    	    document.getElementById('apple-pay-button').style.display = 'block';
    	  }
    });

    document.getElementById('apple-pay-button').addEventListener('click', beginApplePay);

    var PlanName = '<?=$planName;?>';
    var PlanAmount = '<?=$price;?>';
    var url = '<?=SITEURL . $controllerName."/applePayCreatePayment"?>';
    var successUrl = '<?=SITEURL . $this->myInfo ["controller"];?>';
    
    function beginApplePay() {
          var paymentRequest = {
            countryCode: 'US',
            currencyCode: 'USD',
            total: {
              label: PlanName ,
              amount: PlanAmount
            }
          };
          var session = Stripe.applePay.buildSession(paymentRequest,
        	        function(result, completion) {
        	    	
        	        $.post( url , { stripeToken: result.token.id ,'_csrf' : $('input[name="_csrf"]').val()}).done(function() {
        	    	      
        	    	completion(ApplePaySession.STATUS_SUCCESS);
        	    	
        	          // You can now redirect the user to a receipt page, etc.
        	          window.location.href = successUrl;
        	        }).fail(function() {
        	          completion(ApplePaySession.STATUS_FAILURE);
        	        });
    
        	      }, function(error) {
        	        console.log(error.message);
        	      });
        	      session.begin();
    }
<?php }?>
    
  </script>
