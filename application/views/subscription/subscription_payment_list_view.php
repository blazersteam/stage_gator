
<div class="content" style="height: 100%;">

	<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>					
					</div>
					<caption><div class="alert alert-info">Transaction time could take upto 30 minutes</div></caption>
		<div id="no-more-tables">
			<table
				class="table table-bordered table-hover table-striped display dataTable cf">
				
				<thead class="cf">
					<tr role="row">
						<th style="width: 7%" class="">Sr #</th>
						<th>Plan name</th>
						<th style="width: 20%" class="">Transaction ID</th>
						<th style="width: 15%" class="">Reference ID</th>
						<th style="width: 10%">Amount</th>
						<!--<th style="width: 10%">Rating OLD</th>-->
						<th style="width: 10%">Subtotal</th>
						<th style="width: 10%">Date</th>
					</tr>
				</thead>

				<tbody role="alert" aria-live="polite" aria-relevant="all">
       <?php
    $i = 1;
    $class = "";
    foreach ($result as $r) {
        if ($i % 2 == 0) {
            $class = 'class="gradeA even"';
        }
        else {
            $class = 'class="gradeA odd"';
        }
        
        ?>
				<tr <?php echo $class; ?>>
						<td data-title="Sr #"><?=$i?></td>
						<td data-title="Plan name"><?=$r->plan_name?></td>
						<td data-title="Transaction ID"><?=$r->transcatino_id?></td>
						<td data-title="Reference ID"><?=$r->data_id?></td>
						<td data-title="Amount"><?="$" . round(($r->amount) / 100,2);?></td>
						<td data-title="Subtotal"><?="$" . round(($r->sub_total) / 100,2);?></td>
						<td data-title="Date"><?=date("m/d/Y",$r->created_date)?></td>

					</tr>
				<?php
        $i ++;
    }
    ?>
    
         </tbody>

			</table>
		</div>



	</div>
</div>




