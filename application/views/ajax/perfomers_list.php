<?php if (count($performerlist) > 0){ ?>

<div class="page_header">Performer List</div>
<div id="no-more-tables">
		<table id="list" class="table table-hover table-bordered">
			<thead class="cf">
    			<tr>
    				<th class="visible-lg">Sr #</th>
    				<th>Start Time</th>
    				<th>End Time</th>
    				<th>Performer</th>				
    				<?php 
    				/* on open th where logged in user group is a fan */
    				if($this->pidGroup == FAN_GROUP || $this->pidGroup == VENUE_GROUP || $this->pidGroup == PERFOMER_GROUP) { ?>
    				<th>Action</th>
    				<?php } ?>
    				<th>Status</th>
    			</tr>
			</thead>
	<?php
    foreach ($performerlist as $k => $l) {
            if($l->status == 'onstage'){
                $bgcolor = 'style="background-color:#90EE90;"';   
            }else if($l->status == 'performed'){
                $bgcolor = 'style="background-color:#D3D3D3;"';
            }else{
                $bgcolor = '';
            }
        ?>
				<tr <?php echo $bgcolor; ?>>
				<td data-title="Sr #" class="visible-lg"><?php echo $l->slot_id; ?></td>
				<td data-title="Start Time"><?php echo dateDisplay($l->start_date.' '.$l->start_time,'h:i A'); ?></td>
				<td data-title="End Time"><?php echo dateDisplay($l->end_date.' '.$l->end_time,'h:i A'); ?></td>
				<td data-title="Performer">	
    			<?php
        if ($l->idPerformer == 0) {
            echo "";
        }
        else if ($l->idPerformer == - 1) {
            echo "Intermission";
        }
        else {
            echo ucwords($this->utility->decodeText($l->chrName));
        }
        ?>
                </td>
                <?php
                 $datauserid = "";
                /* show data for action th if user logged in as performer */
                 if($this->pidGroup == FAN_GROUP ||  $this->pidGroup == VENUE_GROUP || $this->pidGroup == PERFOMER_GROUP ) { ?>
            <td data-title="Action"><div><?php  if ($this->pidGroup == 4 && (!empty($l->stripe_account_id) && $l->stripe_status === 'Verified')) {
                $datauserid = 'data-userid="'.$l->user_id.'" data-account-exit="1"';
                ?>
                <a class="btn btn-success tipper_popup" data-userid=<?php echo $this->utility->encode($l->user_id);?> href="javascript:void(0);" title="Tip here" id="tipper_popup_<?php echo $l->user_id; ?>"><!-- <i class="fa fa-usd"></i> --> Tip</a>
                
            <?php
            }
            
                  
            /* this condition check if user has subscribed with any plan */
             /*  if (in_array($l->plan_id, $GLOBALS ["allowedBitePlans"])) { */

            // allowed venue , performer & fan to bite also satisfy the plan conditions.
            if (in_array($l->plan_id, $GLOBALS ["allowedBitePlans"]) && $l->idPerformer && $l->idPerformer!=-1 && $l->idPerformer!=0 && $l->idPerformer!= $this->pUserId){
                 /* check if user did not bitten or not and give button according to it */
                if ($l->followerId != $this->pUserId) {
                    // if(fan_model::checkIfFollowing($User->user_id, $r->user_id)){                     ?>
               			<a href="javascript:void(0)"
							data-following-id="<?php echo $this->utility->encode($l->user_id);?>" <?php echo $datauserid; ?> 
							class="btn btn-sm btn-success toggleBiteUnbite">Bite</a>
               			<?php }else{ ?>				
				
						
						<a href="javascript:void(0)"
							data-following-id="<?php echo $this->utility->encode($l->user_id);?>" <?php echo $datauserid; ?> 
							class="btn btn-sm btn-warning toggleBiteUnbite">Unbite</a>
               			<?php
               			}
                } ?></div>
                
            </td>
            <?php  } ?>
            <td data-title="Status"><?php echo ucfirst($l->status); ?></td>
			</tr>
			<?php
    }
    ?>
</table>
</div>
<?php } else { ?>
	<div class="alert alert-danger">Performer details not available right
			now!</div>
<?php } ?>
	</div>