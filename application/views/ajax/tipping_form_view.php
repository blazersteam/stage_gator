
<?php 
$recurringData = '';
if(!empty($recurringTipInfo)){
    $recurringData = $recurringTipInfo[0];
}
 
?>
<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<h4 class="modal-title mainTital" id="myModalLabel">Tip </h4>
	</div>
	<div class="modal-body">
		<?php if(!empty($recurringData)){ ?>
    		<div  class="alert alert-success" style="display: block;">
    				<?php echo $this->lang->line("already_recurring_tip_message"); ?>  
    		</div>
		<?php } ?>
		<div class="form-group">
    		<div class="row tippingamount">
    			<label class="col-sm-3 control-label text-right">Tips:<em>*</em></label>
    			<div class="col-sm-5">
    				<div class="radio-inline">
    					<label> <input name="tipstype" value="recurring" checked="checked" type="radio">Recurring</label>
    				</div>
    				<div class="radio-inline">
    					<label> <input name="tipstype" value="onetime" type="radio"> One Time </label> 
    				</div>
    			</div>
    		</div>
		</div>
		<div class="form-group" id="tipsSuggention">
			<div class="row tippingamount">
    			<label class="col-sm-3 control-label text-right">Recurring Type:</label>
    			<div class="col-sm-9">
            		<div class="radio-inline">
            			<label for="performance">
            				<input id="performance" value="performance" name="recurringtype" <?php if(empty($recurringData) || $recurringData->recurringType == 'performance'){ echo 'checked="checked"';} ?> type="radio"/>
            				Every Performance
            			</label>
            		</div>
            		<div class="radio-inline">
            			<label for="weekly">
            				<input id="weekly" name="recurringtype" value="weekly" <?php if(!empty($recurringData) && $recurringData->recurringType == 'weekly'){ echo 'checked="checked"';} ?> type="radio">
            				Weekly
            			</label>
            		</div>
            		<div class="radio-inline">
            			<label for="monthly">
            				<input id="monthly" name="recurringtype" value="monthly" <?php if(!empty($recurringData) && $recurringData->recurringType == 'monthly'){ echo 'checked="checked"';} ?> type="radio">
            				Monthly
            			</label>
            		</div>
        		</div>
    		</div>
		</div>
		
		<div class="form-group">

			<div
				class="text-center text-danger alert alert-danger paymentMessage"
				style="display: none;"></div>

			<div class="row tippingamount">
				<label class="col-sm-3 control-label text-right">Amount <em>*</em></label>
				<div class="col-sm-5">
					<input type="number" tabindex="1"
						min="<?php echo MIN_TIP_AMOUNT;?>"
						class="form-control required numeric maxLength" data-length="5" max="99999"
						value="<?php if(!empty($recurringData)){ echo $recurringData->amount; }?>" id="tipAmount" autofocus
						name="tipAmount" /> <i class="fa fa-usd"></i> <span
						for="tipAmount" id="err-tipAmount"
						style="display: none; font-size: 13px; color: #c00 !important;"
						class="help-inline text-danger">Please enter amount.</span> <span
						for="tipAmount" id="err-tipAmount-graterthan-5"
						style="display: none; font-size: 13px; color: #c00 !important;"
						class="help-inline text-danger">Min. amount must be $<?php echo MIN_TIP_AMOUNT;?>.</span>
				</div>
			</div>

		</div>
		<input type="hidden" name="perfomerUserid"
			value="<?php echo $performerUserId;?>" />
				
				<?php if(!empty($displayCards) && is_array($displayCards)){ ?>
                      
						<div class="account-listing">
			<div class="alert deleteStatus hide"></div>
			<div class="row">
						
						<?php foreach($displayCards as $cards) {?>
						<div class="deleteRow">
					<div class="col-xs-1">
						<input id="" name="cardPayment"
							value="<?php echo $this->utility->encode($cards['id']);?>"
							type="checkbox" class="card">
					</div>
					<div class="col-xs-9">
						<label><i class="fa fa-credit-card-alt"></i> ************<?php echo $cards['last4']?> </label>
					</div>
					<!-- Delete Button -->
					<div class="col-sm-2 text-right pull-right">
						<button type="button"
							id="<?php echo $this->utility->encode($cards['id']);?>"
							onclick="if(confirm('Are you sure to delete ?')) { deleteCard(); }"
							class="btn btn-default btn-sm deleteCard">
							<i class="fa fa-trash"></i>
						</button>
					</div>
					<!-- Delete Button -->
				</div>
					
					<?php }?>
					</div>
			<div class="row text-center payNowDiv" style="display: none;">
				<button type="button" class="btn btn-primary btnPayNow">Pay Now</button>
			</div>
		</div>


		<!-- <a class="btn btn-success" id="addNewCard" >Add New Card</a>  -->

		<h4 class="modal-title or-text text-center" id="myModalLabel">
			<span>OR</span>
		</h4>
				
				<?php
    }
    else if(!empty($displayCards)){
         echo $displayCards;
    }
    ?>	
						<?php $controllerName = $this->myvalues->tippingDetails['controller'];?>
						 <?php echo form_open(SITEURL.$controllerName.'/makePaymentTip', 'id="frmaddNewCard" class="form-horizontal addtipping" enctype="multipart/form-data" '); ?>	
						<div class="text-center text-danger alert alert-danger striperror"
			style="display: none;"></div>
		<div id="bankDetails">
			<div class="form-group">

				<div class="col-sm-6">
					<label class="control-label">Card Number <em>*</em></label> <input 
						data-stripe="number" title="Please enter card number."
						tabindex="2" id="number" name="card_number" type="tel"
						class="form-control numeric required notDisplayArrow maxLength" data-length="20"
						value="">
				</div>

				<div class="col-sm-6">
					<label class="control-label">CVC <em>*</em></label> <input
						data-stripe="cvc" title="Please enter cvc number." name="cvc" type="number" data-length="7"
						tabindex="3" maxlength="7" class="form-control numeric required validCard notDisplayArrow maxLength"
						value="">
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-6">
					<label class="control-label">Exp. Month <em>*</em></label> <input
						data-stripe="exp-month" title="Please enter expiry month."
						tabindex="3" maxlength="2" name="expmonth" type="number" data-length="2"
						class="form-control required numeric validCard notDisplayArrow maxLength" value="">
				</div>
				<div class="col-sm-6">
					<label class="control-label">Exp. Year <em>*</em></label> <input
						data-stripe="exp-year" title="Please enter expiry year."
						tabindex="4" maxlength="4" name="expyear" type="number" data-length="4" 
						class="form-control required numeric validCard notDisplayArrow maxLength" value="">
				</div>
			</div>
			<div class="form-group save-payment">
				<div class="col-xs-1">
					<input id="addCard" name="addCard" value="1" type="checkbox"
						tabindex="5" checked>
				</div>
				<div class="col-xs-10">
					<label>Save this card for faster payment.</label>
				</div>
			</div>
		</div>
						<?php form_close(); ?>
				</div>

	<div class="modal-footer text-center">
		<button type="button" class="btn btn-primary btnPayNow" tabindex="6">Pay
			Now</button>
	</div>

</div>

<script>

$('input.maxLength').keypress(function(e) {
var max = $(this).attr("data-length");
    if (e.which < 0x20) {
        // e.which < 0x20, then it's not a printable character
        // e.which === 0 - Not a character
        return;     // Do nothing
    }
    if (this.value.length == max) {
        e.preventDefault();
    } else if (this.value.length > max) {
        // Maximum exceeded
        this.value = this.value.substring(0, max);
    }
});
    
var lastId = "";
   // on change the checkbox with card class disable the second form            
$('input.card').on('change', function() {
	if($("input.card").is(':checked')){ 
		// show the pay now div
	   $('.payNowDiv').show();
	   
      //hide all delete button
	 $('.deleteCard').hide();
	 //alert($(this).val());
	 $('input.card').not(this).prop('checked', false);
	   //disable the form
	 $("#frmaddNewCard :input").prop("disabled", true);
	}else{
		// hide the pay now div
		$('.payNowDiv').hide();
		// hide the delete button		
		$('.deleteCard').hide();
		// enabel the form
		$("#frmaddNewCard :input").prop("disabled", false); 
	}
	lastId = $('.card:checked').val();
	$('#'+ lastId).show();
});
   
//Tip Popup Function
$('.btnPayNow').on('click',function(){	

	$('.btnPayNow').attr('disabled',true);
	
	var oldRecurringType = "<?php if(!empty($recurringData->recurringType)){ echo $recurringData->recurringType;} ?>";

	if($("input[name = 'recurringtype']:checked").val() != oldRecurringType && oldRecurringType != ''){
		bootbox.confirm("Are you sure want change recurring type ?", function(result) {
        	//	console.log(userid);
        	if(result === true ){
        		//call popup function
        		btnPayNow();
        	}else{
        		$(".btnPayNow").removeAttr("disabled");
            }
        });
	}else{
		//call popup function
		btnPayNow();
	}
	   		
});

function btnPayNow(){

	var valueTipAmount = $('#tipAmount').val();
	valueTipAmount = valueTipAmount.trim();
    if(!valueTipAmount){
    	$('#err-tipAmount').show();
    	$(".btnPayNow").removeAttr("disabled");
	}else{
		
		$('#err-tipAmount').hide();
		
		if( valueTipAmount < <?php echo MIN_TIP_AMOUNT; ?>)
		{
			// show error message.
			$('#err-tipAmount-graterthan-5').show();
			$('#tipAmount').focus();
			$(".btnPayNow").removeAttr("disabled");
		}else{
			$('#err-tipAmount-graterthan-5').hide();
			checkPaymentMethod();
		}	
   }
}

//Set the auto focus when the popup is shown.
$('#tipper').on('shown.bs.modal', function () {
    $('#tipAmount').focus();
});

$(document).ready(function(){
	
	//inital hide save card check box,savedcard detail and hr line div 
	$(".save-payment").hide();
	$(".account-listing").hide();
	$(".modal-title.or-text").hide();
	
	$("#frmaddNewCard").validate(
			{
				rules : {
					required : {
						required : true									
					},
					card_number : {
						required : true
					},
					cvc : {
						required : true
					},
					expmonth : {
						required : true
					},
					expyear : {
						required : true
					}
				},    					
				highlight : function(element, errorClass,
						validClass) {
					
					$(element).parents('.form-control').addClass(
							'has-error');
				},
				unhighlight : function(element, errorClass,
						validClass) {
					$(element).parents('.form-control')
							.removeClass('has-error');
					$(element).parents('.form-control').addClass(
							'has-success');
				}
			});        	            
});

// bind keypress for accept numeric value
$('.numeric').bind('keypress', function(e) {
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		return false;
	}
}); 


//For show/Hide Tip

$('input[name="tipstype"]:radio').change(function(){
     var type = $(this).val();
     if(type == 'onetime'){
    	 $("#tipsSuggention").hide();
		 $(".save-payment").show();
		 $(".account-listing").show();
		 $(".modal-title.or-text").show();
     }else{
    	 $("#tipsSuggention").show();
    	 $(".save-payment").hide();
    	 $(".account-listing").hide();
    	 $(".modal-title.or-text").hide();
    	 
     }
});  
</script>

<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/jquery.blockUI.js"></script>

<script type="text/javascript">

// Fill in your publishable key pk_test_GyQPfm5ADQzY8tHGNwXND5U9
Stripe.setPublishableKey('<?=STRIPE_PUBLISH_KEY_TIP?>');
//Stripe.setPublishableKey('pk_live_XR8QYaGqKo4QBA3iBGsvLACY');

var stripeResponseHandler = function(status, response) {
	$.unblockUI();
    var $form = $('#frmaddNewCard');
    $('.striperror').hide();
    if (response.error) {
        
    	$(".btnPayNow").removeAttr("disabled");
        // Show the errors on the form
        $('.striperror').text(response.error.message);
        $('.striperror').show();
      //  $('.cardBtn').prop('disabled', false);
    } else {
    	
        // token contains id, last4, and card type
        var token = response.id;
        // Insert the token into the form so it gets submitted to the server
        $form.append($("<input type='hidden' name='stripeToken'>").val(token));
        submitPaymentForm(null);
    }
};

// used for check if user pay by add the card or pay by add new card
function checkPaymentMethod()
{
	var currentCard = $('input.card:checked').val();
	var isDisabled = $('input[name~="card_number"]').prop('disabled');
		
    if(currentCard && isDisabled)
    {
	    // the card selected	
    	submitPaymentForm($('input.card:checked').val());
    }else{
        // card not selected & check validation for second form            	    	
    	$("#frmaddNewCard").valid();
    	accountAndCardDetails();
	}
}

// submit the form to make the payment.
function submitPaymentForm( selectedCardId )
{
	 var tippingcontroller = '<?php echo $this->myvalues->tippingDetails['controller'];?>';
     var perfomerUserid = $('input[name="perfomerUserid"]').val();
     var amount = $("#tipAmount").val();
     var addCard = $("#addCard").is(':checked');
     var stripeToken = $("input[name = 'stripeToken']").val();
     var tipstype = $("input[name = 'tipstype']:checked").val();
     var recurringtype = $("input[name = 'recurringtype']:checked").val();
     var selectedCardId = selectedCardId;
     
      $.ajax({
	       type:"POST",
	       url: SITEURL + tippingcontroller + "/makePaymentTip",     	        
	       data:{'_csrf' : $('input[name="_csrf"]').val(),'perfomerUserid' : perfomerUserid,'tipAmountValue' : amount,'addCard':addCard,'stripeToken':stripeToken,'selectedCardId':selectedCardId,'tipstype':tipstype,'recurringtype':recurringtype},     	       
	       beforeSend: function() {		       
	    	   blockUiDisplay();  
	       },
	     
	       success:function(result){
	    	   $.unblockUI();

	    	   $(".btnPayNow").removeAttr("disabled");

	    	   var data = jQuery.parseJSON(result);
				if (data.status == 'Success' || data.status == 'Pending') {					
					$('#tipper').modal('hide');
	    	    	//show the message in profile_overview_view view page.
	    	    	$('.retrunmessage').text(data.message);
	    	    	$('.tipSuccessMessage').show();
	    	    	
				}else{
					$('.paymentMessage').text(data.message);
	    	    	$('.paymentMessage').show();
				}
	       },
	       statusCode: {
               401:function() { window.location.reload(); }
             }
	     }); 
}

function accountAndCardDetails(){
    var $form = $('#frmaddNewCard');
        if (!$form.valid()) {
        	$(".btnPayNow").removeAttr("disabled");
            return false;
        }
        blockUiDisplay();
        Stripe.card.createToken($form, stripeResponseHandler);
        // Prevent the form from submitting with the default action
        return false;
}
//used for delete the card
function deleteCard()
{
	var f = $('.card:checked').val();	 	
	var url = SITEURL+'<?=$this->myvalues->tippingDetails['controller']?>'+'/deleteDetails/'+f;	
	blockUiDisplay();
	    $.get(url, {},
	    function(response) {
		    
	       $.unblockUI();
	    	
		   if(response == 1){			   
    		   $('.card:checked').closest('.deleteRow').remove();
    		   $('.deleteStatus').addClass('alert-success');    		   
    		   $('.deleteCardDiv').hide();
    		   $('.payNowDiv').hide();
    		   $('.deleteStatus').removeClass('hide').text('<?php echo $this->lang->line("success_delete_card");?>');
    		   return false;
		    }else{
		    	$('.deleteStatus').addClass('alert-danger');
				$('.deleteStatus').removeClass('hide').text(response)
		    	   
			}
		    return false;
	   }).fail(function(newData) {
		    if(newData.status == '401'){
		    	window.location.reload();
		        return true;
		    }
		});
	    
    return false;
}

  </script>

<style>
.deleteCard {
	display: none;
}
</style>

