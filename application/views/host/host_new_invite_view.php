<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>											
					</div>
		<div id="no-more-tables">
		<table class="table table-bordered table-hover table-striped dataTable cf"
			id="hostInvitation">
		<thead class="cf">
				<tr>
					<th class="">Sr #</th>
					<th>Event Title</th>
					<th>Event Date</th>
					<th class="">Start Time</th>
					<th class="">End Time</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
	<?php
$i = 1;
foreach ($list as $k => $l) {
    
        $stime = strtotime($l->scheduleDate . " " . $l->startTime);
        if ($stime > time()) {
            ?>
				<tr>
					<td data-title="Sr #" class=""><?php echo $k=$k+1; ?></td>
					<td data-title="Event Title"><a
						href="<?php echo $this->utility->generateOverviewUrl($l->idEvent,$l->table_id); ?>"><?php echo ucwords($this->utility->decodeText($l->title)); ?></a></td>
					<td data-title="Event Date"><?php echo dateDisplay($l->scheduleDate.''.$l->startTime); ?></td>
					<td data-title="Start Time" class=""><?php echo dateDisplay($l->scheduleDate.''.$l->startTime,'h:i A'); ?></td>
					<td data-title="End Time" class=""><?php echo dateDisplay($l->end_date.''.$l->endTime,'h:i A'); ?></td>
					<td data-title="Actions">
						<div style="text-align: center;">
							<a
								href="<?php echo SITEURL.$controller."/view_performers/".$this->utility->encode($l->table_id); ?>"
								class="btn btn-sm btn-info">View Performers</a>
						</div>
					</td>
				</tr>
			<?php
        }
    
}
?>
</tbody>
		</table>
		</div>
	</div>
</div>
