<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>											
					</div>
		<table class="table table-bordered table-hover table-striped">
			<tr>
				<th>Sr #</th>
				<th>Performer</th>
				<th>Action</th>
			</tr>
<?php
foreach ($list as $k => $l) {
    ?>
	 <tr>
				<td><?php echo $k=$k+1; ?></td>
				<td><?php echo $this->utility->decodeText($l->chrName); ?></td>
				<td>
	 		<?php    
    if (is_null($l->isHost)) {
        ?>
		 			<a
					href="<?php echo SITEURL.$controller."/inviteHost/".$this->uri->segment(3)."/".$this->utility->encode($l->user_id); ?>"
					class="btn btn-sm btn-info">Invite Host</a>
		 	<?php
    }
    else if ($l->isHost == 2) {
        ?><span class="badge bg-primary">Host Invite Sent</span><?php
    }
    else if ($l->isHost == 1) {
        ?><span class="badge bg-success">Host Invite Accepted</span><?php
    }
    else if ($l->isHost == 3) {
        ?><span class="badge bg-info">Host Invite Cancelled</span><?php
    }
    else if ($l->isHost == 4) {
        ?><span class="badge bg-info">Host Invite Rejected</span><?php
    }
    ?>
	 	</td>
			</tr>
	 <?php
}
?>
</table>
	</div>
</div>