<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>											
					</div>
		<div id="no-more-tables">
		<table class="table table-bordered table-hover table-striped dataTable"
			id="hostManagement">
			<thead class="cf">
				<tr>
					<th>Sr #</th>
					<th>Event Title</th>
					<th>Event Date</th>
					<th>Performer</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
	<?php
	
	foreach ( $list as $k => $l ) {	    
		if (! $this->this_model->check_ranking_completed_for_event ( $l->idEvent )) {
			?>
	 <tr>
					<td data-title="Sr #"><?php echo $k=$k+1; ?></td>
					<!-- Hear the eventId = event id & idEvent= event schedule id -->
					<td data-title="Event Title"><a
						href="<?php echo $this->utility->generateOverviewUrl($l->eventId,$l->idEvent);  ?>"
						title="Overview"><?php echo ucwords($l->title); ?></a></td>
					<td data-title="Performer"><?php echo dateDisplay($l->eventScheduleDate.''.$l->startTime); ?></td>
					<td data-title="Status"><a href="<?php echo SITEURL.'view/'.$l->url; ?>" title="Overview"><?php echo ucwords($l->chrName); ?></a></td>
					<td>
	 		<?php
			if ($l->isHost == 2) {
				?><span class="badge bg-primary">Host Invite Sent</span><?php
			} else if ($l->isHost == 1) {
				?>
	 				<span class="badge bg-success">Host Invite Accepted</span> <a
						href="<?php echo SITEURL.$controller."/cancel_host_invite/".$this->utility->encode($l->idRegister); ?>"
						class="btn btn-danger btn-sm"
						onclick="return (confirm('Are you sure you want to revoke this access?'))">Cancel</a>
							
		<?php echo form_open(SITEURL.$controller.'/host_management', 'novalidate="novalidate" role="form"'); ?>
		<div class="checkbox">
		<input type="checkbox" id="approval" name="approval"
								<?php echo ($l->doAccept==1)?'checked':''; ?> />
								<label for="approval">Request Approval</label>
						</div>
						<div class="checkbox">		
								<input type="checkbox" id="ranking" name="ranking"
								<?php echo ($l->doRank==1)?'checked':''; ?> />
								<label for="ranking">Ranking</label>
					</div>
				<div class="checkbox">
			<input id="attandance" type="checkbox" name="attandance"
								<?php echo ($l->doAttandance==1)?'checked':''; ?> />
								<label for="attandance">Attendance</label> 
					</div>
						<div class="checkbox">		
								<input id="list" type="checkbox" name="list"
								<?php echo ($l->doList==1)?'checked':''; ?> />
								<label for="list">List Management</label>
<input type="hidden" name="hdnRegId" value="<?php echo $this->utility->encode( $l->idRegister); ?>" />
						</div>		
 				<?php $assign_status = ($l->doAccept==1 || $l->doRank==1 || $l->doAttandance==1 || $l->doList==1)?'1':0; ?>
 				<input type="submit"
								class="btn btn-sm <?php echo ($assign_status==1)?'btn-danger':'btn-primary'; ?>"
								value="<?php echo ($assign_status==1)?'Update':'Assign'; ?>"
								name="btnAssign">
								
	<?php echo form_close(); ?>
					
 			<?php
			} else if ($l->isHost == 3) {
				?><span class="badge bg-info">Host Invite Cancelled</span><?php
			} else if ($l->isHost == 4) {
				?><span class="badge bg-info">Host Invite Rejected</span><?php
			}
			?>
	 	</td>
				</tr>
	 <?php
		}
	}
	?>
</tbody>
		</table>
		</div>
		<div class="clearfix"></div>
		<div class="alert alert-info">
			<ul>
				<li><b>Request Approval: </b> A host can approve/reject performer's
					request to perform on any show.</li>
				<li><b>Ranking: </b> A host can rank the performers after the show.</li>
				<li><b>Attendance:</b> A host can mark attendance for performers
					after the show.</li>
				<li><b>List Management:</b> A host can manage time slots for the
					show as well as order of performances.</li>
			</ul>
		</div>
	</div>
</div>
