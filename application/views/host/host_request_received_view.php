<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>											
					</div>
					<div id="no-more-tables">
		<table class="table table-bordered table-hover table-striped dataTable cf"
			id="hostreceivedInvitation">
			<thead class="cf">
				<tr>
					<th>Sr #</th>
					<th>Event Title</th>
					<th>Event Date</th>
					<th>Event Time</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
	<?php

foreach ($list as $k => $l) {
    if (! event_end_time_crossed($l->end_date, $l->endTime)) {
        ?>
			<tr>
					<td data-title="Sr #"><?php echo $K=$k+1; ?></td>
					<td data-title="Event Title"><?php echo ucwords($this->utility->decodeText($l->title)); ?></td>
					<td data-title="Event Date"><?php echo datedisplay($l->scheduleDate.''.$l->startTime,'m/d/Y'); ?></td>
					<td data-title="Event Time"><?php echo datedisplay($l->scheduleDate.''.$l->startTime,'h:i A') . " - " . datedisplay($l->end_date.''.$l->endTime,'h:i A'); ?></td>
					<td data-title="Status">
			 		
			 		<?php
        if ($l->isHost == 2) {
            ?>
						<a
						href="<?php echo SITEURL.$controller.'/acceptHost/'.$this->utility->encode($l->idRegister); ?>"
						class="btn btn-sm btn-primary" title="Accept"><span
							class="glyphicon glyphicon-ok"></span></a> <a
						href="<?php echo SITEURL.$controller.'/rejectHost/'.$this->utility->encode($l->idRegister); ?>"
						class="btn btn-sm btn-danger" title="Reject"><span
							class="glyphicon glyphicon-remove"></span></a>				 		
				 		<?php
        }
        else if ($l->isHost == 1) {
            ?><span class="badge bg-success">Host Invite Accepted</span><?php
        }
        else if ($l->isHost == 3) {
            ?><span class="badge bg-info">Host Invite Cancelled</span><?php
        }
        else if ($l->isHost == 4) {
            ?><span class="badge bg-info">Host Invite Declined</span><?php
        }
        ?>
			 	</td>
				</tr>
			<?php
    }
}
?>
</tbody>
		</table>
		</div>
	</div>
</div>