
<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>	
				
		</div>
<meta property="og:image" content="http://ia.media-imdb.com/rock.jpg"/>
<meta property="og:image:secure_url" content="https://secure.example.com/ogp.jpg" />


<table class="table table-hover table-bordered table-striped">
	<tr>
		<th style="width: 25%;">Venue Title: </th>
		<td><?php echo ucwords($record->chrName); ?></td>
	</tr>
	<tr>
		<th style="width: 25%;">Show Title: </th>
		<td><?php echo ucwords($record->title); ?></td>
	</tr>
	<tr>
		<th style="width: 25%;">Show City: </th>
		<td><?php echo empty($location->city)?ucwords($record->mem_city):ucwords($location->city); ?>, <?php echo empty($location->state)?ucwords($record->mem_state):ucwords($location->state); ?></td>
	</tr>
	<tr>
		<th style="width: 25%;">Show Address: </th>
		<td><?php echo $location->address; ?></td>
	</tr>
	<tr>
	        <th>Number of Seats:</th>
	        <td><?php echo $location->numSeats; ?></td>
	</tr>
	<tr>
		<th>Show Type: </th>
		<td><?php echo ucwords($record->type); ?></td>
	</tr>
	
	<tr>
		<th>Show Date: </th>
		<td><?php echo dateDisplay($record->start_date . " " . $record->startTime); ?></td>
	</tr>
	
	<tr>
		<th>Is Recurring?</th>
		<?php 
		switch ($record->isRecuring){
		    
            case 1:
		    $res = "One time";
		    break;
		    
		    case 2:
		    $res = "Daily";
		    break;
		    
		    case 3:
		    $res = "Weekly";
		    break;
		    
		    case 4:
		    $res = "Monthly";
            break;
            
		    default:
		    $res = "NO";
		        
		}
		
		
		?>
		<td><?php echo $res; ?></td>
	</tr>
	
	<?php if ($record->isRecuring>0){ ?>
		<tr>
			<th>End Date</th>
			<td><?php echo dateDisplay($record->start_date . " " . $record->endTime); ?></td>
		</tr>
	<?php } ?>
	
	<tr>
		<th>Start Time: </th>
		<td><?php  echo dateDisplay($record->start_date . " " . $record->startTime,'h:i A'); ?></td>
	</tr>
	
	<tr>
		<th>End Time: </th>
		<td><?php  echo dateDisplay($record->end_date . " " . $record->endTime,'h:i A');?></td>
	</tr>
	
	<tr>
		<th>Cut-Off Time: </th>
		<td><?php echo $record->hoursLockBefore; ?> Hours Before Starting</td>
	</tr>
	
	<tr>
		<th>Allow Walk-In?</th>
		<td><?php echo ($record->allowWalkInReal==1)?"Yes":"No"; ?></td>
	</tr>
	
	<tr>
		<th>Performer Can Pick Time?</th>
		<td><?php echo ($record->performerCanPickTime==1)?"Yes":"No"; ?></td>
	</tr>
	
	<?php if (!$record->performerCanPickTime){ ?>
		<tr>
			<th>Scheduling Algo</th>
			<td><?php echo $record->nameAlgo; ?></td>
		</tr>
	<?php } ?>
	
	<tr>
		<th>Have Host?</th>
		<td><?php echo ($record->haveHost==1)?"Yes":"No"; ?></td>
	</tr>
	
	<tr>
		<th>Allow Trading?</th>
		<td><?php echo ($record->allowTrading==1)?"Yes":"No"; ?></td>
	</tr>
	
	<tr>
		<th>Time Per Performance </th>
		<td><?php echo $record->timePerPerformance; ?> Minutes</td>
	</tr>
	
	<tr>
		<th>Event Description </th>
		<td><?php echo $record->descr; ?></td>
	</tr>

</table>
<?php if (count($list) > 0){ ?>

<div class="page_header">Performance List</div>

<table id="list" class="table table-hover table-bordered table-striped">
	<tr>
		<th class="visible-lg">Sr #</th>
		<th>Start Time</th>
		<th>End Time</th>
		<th>Performer</th>
		<?php /*if($this->pidGroup==4){ ?>
			<th>Tip</th>
		<?php } */?>
	</tr>
	
	<?php 
		foreach($list as $l){
			?>
				<tr>
					<td class="visible-lg"><?php echo $l->slot_id; ?></td>
					<td><?php echo dateDisplay($l->date . " " . $l->start_time,'h:i a');  ?></td>
					<td><?php echo dateDisplay($l->date . " " . $l->end_time,'h:i a'); ?></td>
					<td><a href="<?php echo SITEURL.'view/'.$l->url; ?>"><?php echo ucwords($l->chrName); ?></a></td>
					<?php /*if($this->pidGroup){ ?>
						<td><?php echo($l->changeTip) ?></td>
					<?php } */?>
				</tr>
			<?php 
		}
	?>
</table>

<?php } else { ?>
	<div class="alert alert-danger">Performance list not available right now!</div>
<?php } ?>

<div style="text-align: center;">
<a href="javascript:void(0);" class="btn btn-default" onclick="javascript: window.history.back();">Back</a>
<a href="http://www.stagegator.com/share/event?access=<?php echo $this->utility->encodeText($record->idEvent); ?>" class="btn btn-default">Visit Share Page</a>
<a href="javascript:void(0);" class="btn btn-default" onclick="exportEventTable();">Export List</a>

<script>function exportEventTable(){
    var w = window.open();
    w.document.body.innerHTML="<table border='1'>"+document.getElementById("list").innerHTML+"</table><p>Powered by StageGator.com</>";
}</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-share-button" data-href="http://www.stagegator.com/share/event?access=<?php echo $this->utility->encodeText($record->idEvent); ?>" data-layout="button"></div>

<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.stagegator.com/share/event?access=<?php echo $this->utility->encodeText($record->idEvent); ?>" data-text="I'm booked! Link contains details! Hope to see you there!" data-count="none">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
</div>
</div>
</div>
