<div class="content" style="height: 100%;">
<?=$this->session->flashdata('myMessage');?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>	
						</div>
<div class="alert alert-info">Please invite all performers you are interested in on the current page before moving to the next page!</div>
 <?=form_open(SITEURL.$controllerName."/bulkInvite/".$this->utility->encode($eventId),'id="frm"')?>
		<div id="no-more-tables">
		<table class="table table-bordered table-hover dataTable cf" id="bulk_event_invitation_table">
		<thead class="cf">
            <tr role="row">
            	<th style="width: 1%" class=""><input type="checkbox" name="chkAll" onclick="setAll();"></th>
            	<th style="width: 7%">Sr </th>
            	<th >Name </th>
            	<th style="width: 30%" > Styles </th>
            	<th style="width: 10%" >Image</th> 
            	<th style="width: 15%" >City</th>
            	<th style="width: 10%">Rating</th>
            </tr>
         </thead>
         
         <tbody role="alert" aria-live="polite" aria-relevant="all">
       <?php 
       		$i = 1;
			foreach($result as $r){		
				if (!in_array($r->user_id, $alreadyInQueue)){
				    $styles = $this->this_model->getUserStyles($r->user_id);
				?>
				<tr>
               		<td>
               		<input type="checkbox" name="chkstatus[]" value="<?php echo $this->utility->encode($r->user_id); ?>"> </td>
               		<td data-title="Sr"><?php echo $i; ?></td>
               		<td data-title="Name" class=""><a href="<?php echo SITEURL."view/".$r->url; ?>" title="Overview"><?php echo ucwords($r->chrName); ?></a></td>
               		<td data-title="Styles" class=""> 
               			<?php 
							$n=0;
							foreach($styles as $row) { 
								if ($n){
									echo ", ";
								}
								echo $row->nameStyle;
								$n++;
							} 
						?>
               		</td>
               		<td data-title="Image" align="center" class="">
               			<?php if (!empty($r->image) && file_exists(FCPATH."external/images/profiles/".$r->image)) { ?>
               				<img src="<?php echo EXTERNAL_PATH."images/profiles/".$r->image; ?>" style="width: 50px; height: 50px;" />
               			<?php } else { ?>
               				<img src="<?php echo EXTERNAL_PATH."images/profiles/eleven.png"; ?>" style="width: 50px; height: 50px;" />
               			<?php } ?>
               		</td>
               		<td data-title="City" class=""><?php echo ucwords($r->mem_city); echo ", "; echo $r->mem_state; ?></td>
               		<td data-title="Rating" class=""><?php echo number_format($r->rate,2); ?></td>
             	</tr>
				<?php 
				$i++;
				
			}}
       ?>
    
         </tbody>
		
		</table>
		</div>
<?php if ($i>1){ ?>
	<div style="display:block;">
		<input class="btn btn-warning" type="submit" name="BulkInvite" value="Send Invites"/>
	</div>
<?php } 

echo form_close();
?>


<!--<div class="alert alert-info">Invite All Performers By City!</div>
<form id="frm" method="post" action="<?php echo SITEURL."venue/bulk_invite/$event->idEvent"; ?>" onsubmit="return bulk_invite_check();">
		
		</table>-->
<!--<?php if ($i>1){ ?>-->
	<!--<div style="display:block; ">
		<input class="btn btn-warning" type="submit" name="BulkInviteByCity" value="Invite All From City"/>
	</div>-->
<!--<?php } ?>-->
</form>	
</div>
</div>
<script type="text/javascript" src="<?=EXTERNAL_PATH?>js/modules/bulk_invite.js"></script>


