<div class="content" style="height: 100%;">
	<div class="row">
		<div id="Tick"></div>
		<div class="page_header"><?=$title?></div>
            <?php echo $this->session->flashdata('myMessage'); ?>
            <div class="module_title"><?=$this->lang->line("tooltip_the_create")?></div>
            <?php echo form_open($registerUrl, 'id="event_form" class="form-horizontal" onsubmit= if($("#event_form").valid()) { $("#btnEventSubmit").prop("disabled",true); }  novalidate="novalidate" role="form"'); ?>
            <div class="col-xs-10">
			<div class="form-group">
				<label class="col-sm-4 control-label"><?=$this->lang->line('create_a')?>:<em>*</em></label>
				<div class="col-sm-8">
					<div class="radio-inline">
						<label> <input type="radio" name="rdaCreateType" value="show"
							checked>
                    		<?=$this->lang->line('show')?>
                    		</label>
					</div>
					<div class="radio-inline">
						<label> <input type="radio" name="rdaCreateType" value="series"
							<?=@set_value('rdaCreateType') == 'series' || @$event->main_event_type == 'Series'?'checked':''?>>
                    		<?=$this->lang->line('series')?>
                    		</label>
					</div>
                		<?php echo form_error('rdaCreateType'); ?>
                    </div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label"><?=$this->lang->line('event_title')?>:<em>*</em></label>
				<div class="col-sm-8">
					<input type="text" name="txtName" class="form-control"
						value="<?=@set_value('txtName')?set_value('txtName'):@$event->title?>"
						placeholder="Enter Title" required>
                          <?php echo form_error('txtName'); ?>
                          <small
						data-original-title="<?=$this->lang->line('tooltip_the_name')?>"
						class="tooltips"><img
						src="<?=EXTERNAL_PATH."images/comment.png"; ?>"
						class="comment_box" /></small> <span for="txtName"
						class="help-inline text-danger"></span>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label"><?=$this->lang->line('location')?>:<em>*</em></label>
				<div class="col-sm-8">
					<select name="cmbLocation_id" class="form-control chosen-select"
						id="eventLocationDropDown">
						<option></option>
                      	<?php
                    
                    foreach ($locations as $l) {
                        $selected = set_value('cmbLocation_id') || $event->location_id == $l->location_id ? 'selected' : '';
                        
                        ?>
                      		<option
							value="<?php echo set_value('cmbLocation_id')?@set_value('cmbLocation_id'):$l->location_id; ?>"
							<?=$selected?>><?php echo $this->utility->decodeText(ucwords($l->location_title)); ?></option>
                      	<?php } ?>
                      </select>
                      <?php echo form_error('cmbLocation_id'); ?>
                      <!-- <lable class="help-inline text-danger"
						id="eventLocationDropDownLabel"></lable> -->
					<span for="eventLocationDropDown" class="pull-left help-inline text-danger"></span>
				</div>
				<label class="col-md-10 col-lg-10 control-label" style="text-align:center;"><a href="<?php echo SITEURL.'location'; ?>" target="_blank"><?=$this->lang->line('add_location');?></a></label>
			</div>

			<div class="form-group">
				<label class="col-sm-4 control-label"><?=$this->lang->line('event_type')?>:<em>*</em></label>
				<div class="col-sm-8">
					<div class="radio-inline">
						<label> <input type="radio" name="rdaType" value="Open Mic"
							checked>
                    		<?=$this->lang->line('open_mic')?>
                    		</label>
					</div>
					<div class="radio-inline">
						<label> <input type="radio" name="rdaType" value="Showcase"
							<?=@set_value('rdaType') == "Showcase" || @$event->type == 'Showcase'?'checked':''?>>
                    		<?=$this->lang->line('showcase')?>
                    		
                  			</label>
					</div>
					<?php echo !empty(form_error('rdaType')) ? "<br/>" . form_error('rdaType') : ""; ?>                		
                    </div>
			</div>

			<div class="form-group">
				<label class="col-sm-4 control-label"><?=$this->lang->line('style')?>:<em>*</em></label>
				<div class="col-sm-8">
					<select name="cmbStyle[]" class="form-control chosen-select" required
						multiple='multiple' id="cmbStyle" style="width: 94%;">						
                      <?php
                    foreach ($styles as $st) {
                        $selected = in_array($st->idStyle, $style) ? 'selected' : '';
                        ?>
                      		<option value="<?php echo $st->idStyle ?>"
							<?=$selected?>><?php echo $this->utility->decodeText($st->nameStyle); ?></option>	
                      		<?php
                    }
                    ?>
                      </select> <small
						data-original-title="<?=$this->lang->line('tooltip_what_type')?>?"
						data-placement="top" class="tooltips"><img
						src="<?=EXTERNAL_PATH."images/comment.png"; ?>"
						class="comment_box" /></small> <span for="cmbStyle"
						class="help-inline text-danger" style="display: none"></span>
                      <?php echo form_error('cmbStyle[]'); ?>
                    </div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label"><?=$this->lang->line('recurring')?>?</label>
				<div class="col-sm-8">
					<select name="cmbIsRecuring" id="cmbIsRecuring"
						class="form-control">

						<option value="1"
							<?=@set_value('cmbIsRecuring') == 1 ||@$event->isRecuring==1?'selected':''?>><?=$this->lang->line('one_time')?></option>
						<option value="2"
							<?=@set_value('cmbIsRecuring') == 2 ||@$event->isRecuring==2?'selected':''?>><?=$this->lang->line('daily')?></option>
						<option value="3"
							<?=@set_value('cmbIsRecuring') == 3 ||@$event->isRecuring==3?'selected':''?>><?=$this->lang->line('weekly')?></option>
						<option value="4"
							<?=@set_value('cmbIsRecuring') == 4 ||@$event->isRecuring==4?'selected':''?>><?=$this->lang->line('monthly')?></option>

					</select>
                		 <?php echo form_error('cmbIsRecuring'); ?>
                		<small
						data-original-title="<?=$this->lang->line('tooltip_is_your')?>?"
						data-placement="top" class="tooltips"><img
						src="<?=EXTERNAL_PATH."images/comment.png";?>" class="comment_box"></small>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label"><?=$this->lang->line('start_date')?><em>*</em></label>
				<div class="col-sm-8">
					<div class="input-append date" id="dateEnd" style="z-index: 2000;">
						<span class="add-on"><input type="text" id="txtStartDate"
							value="<?=@set_value('txtStartDate')?@set_value('txtStartDate'):@convertFromGMT($event->date . " " . $event->startTime, "Y-m-d");?>"
							name="txtStartDate" placeholder="Start date"
							class="datepicker form-control" required readonly></span>
                		<?php echo form_error('txtStartDate'); ?>
                	  </div>
				</div>
			</div>
			<div class="form-group" style="display: none;" id="endDateDiv">
				<label class="col-sm-4 control-label"><?=$this->lang->line('end_date')?><em>*</em></label>
				<div class="col-sm-8">
					<div class="input-append date" id="dateEnd" style="z-index: 2000;">
						<span class="add-on"><input type="text" id="txtEndDate"
							placeholder="End date"
							value="<?=@set_value('txtEndDate')?@set_value('txtEndDate'):@convertFromGMT($event->endDate . " " . $event->endTime, "Y-m-d");?>"
							name="txtEndDate" class="form-control datepicker" required
							readonly></span>
                		<?php echo form_error('txtEndDate'); ?>
                		<span for="txtEndDate" class="pull-left help-inline text-danger"></span>
                	  </div>
				</div>
			</div>

			<div class="form-group" style="display: none;" id="weekdays">
				<label class="col-sm-4 control-label"><?=$this->lang->line('select_days')?> ?:</label>
				<div class="col-sm-8">
					<div class="checkbox-inline">
						<label> <input type="checkbox" name="chkDay[]" id="chkDay1"
							required class="reInvite" value="1">
                		<?=$this->lang->line('monday')?>
                		</label>
					</div>
					<div class="checkbox-inline">
						<label> <input type="checkbox" name="chkDay[]" id="chkDay2"
							required class="reInvite" value="2">
                		<?=$this->lang->line('tuesday')?>
                		</label>
					</div>
					<div class="checkbox-inline">
						<label> <input type="checkbox" name="chkDay[]" id="chkDay3"
							required class="reInvite" value="3">
                		<?=$this->lang->line('wednesday')?>
                		</label>
					</div>
					<div class="checkbox-inline">
						<label> <input type="checkbox" name="chkDay[]" id="chkDay4"
							required class="reInvite" value="4">
                		<?=$this->lang->line('thursday')?>
                		</label>
					</div>
					<div class="checkbox-inline">
						<label> <input type="checkbox" name="chkDay[]" id="chkDay5"
							required class="reInvite" value="5">
                		<?=$this->lang->line('friday')?>
                		</label>
					</div>
					<div class="checkbox-inline">
						<label> <input type="checkbox" name="chkDay[]" id="chkDay6"
							required class="reInvite" value="6">
                		<?=$this->lang->line('saturday')?>
                		</label>
					</div>
					<div class="checkbox-inline">
						<label> <input type="checkbox" name="chkDay[]" class="reInvite"
							id="chkDay7" required value="7">
                		<?=$this->lang->line('sunday')?>
                		</label>
					</div>
					<br>
                		<?php echo form_error('chkDay[]'); ?>
            		    <span for="chkDay[]" class="help-inline text-danger"
						style="display: none;"></span>
				</div>
			</div>
			<div class="form-group" style="display: none;" id="selectMonth">
				<label class="col-sm-4 control-label"><?=$this->lang->line('select_days')?>:<em>*</em></label>
				<div class="col-sm-8">
					<select name="cmbMonthDay[]" class="form-control chosen-select"
						multiple='multiple' id="eventStyleDropDown1" style="width: 94%;">
						<option value=""></option>
                  <?php
                
                for ($i = 1; $i <= 31; $i ++) {
                    $postData = $this->input->post();
                    $select = (@set_value('cmbMonthDay') == $i && empty($postData)) ? 'selected' : '';
                    ?>
                  		<option value="<?php echo $i ?>" <?=$select?>><?php echo $i; ?></option>	
                  		<?php
                }
                ?>
                  </select>
                  <?php echo form_error('cmbMonthDay[]'); ?>
                  <lable class="help-inline text-danger"></lable>
					<br> <a href="#" id="byweek" class="advanceSelect"><?=$this->lang->line('select_by_day');?></a>
				</div>
			</div>



			<div id="advanceEvent">
				<div class="form-group" style="display: none;" id="advanceRecurring">
					<label class="col-sm-4 control-label"><?=$this->lang->line('repeating_every')?>:<em>*</em></label>
					<div class="col-sm-8 mainRepeatingDiv">
						<div class="reaptingDiv repeating-field">
							<div class="col-sm-4">
								<div class="form-group">
									<select name="cmbDaysInMonth[0]" class="form-control" required
										style="width: 94%;">
										<option value="1"><?=$this->lang->line('first')?></option>
										<option value="2"><?=$this->lang->line('second')?></option>
										<option value="3"><?=$this->lang->line('third')?></option>
										<option value="4"><?=$this->lang->line('fourth')?></option>
									</select>
                    <?php echo form_error('cmbDaysInMonth[]'); ?>
                    </div>
							</div>

							<div class="col-sm-5 col-md-6">
								<div class="form-group">
									<select name="cmbWeekInMonth[0][]"
										class="form-control chosen-select" multiple="multiple"
										required style="width: 94%;">
										<option value="1"><?=$this->lang->line('monday')?></option>
										<option value="2"><?=$this->lang->line('tuesday')?></option>
										<option value="3"><?=$this->lang->line('wednesday')?></option>
										<option value="4"><?=$this->lang->line('friday')?></option>
										<option value="5"><?=$this->lang->line('thursday')?></option>
										<option value="6"><?=$this->lang->line('saturday')?></option>
										<option value="7"><?=$this->lang->line('sunday')?></option>
									</select>
                    <?php echo form_error('cmbWeekInMonth[]'); ?>
                    </div>
							</div>
							<div class="pull-right">
								<button type="button" class="btnAdd btn btn-sm btn-primary">
									<span class="glyphicon glyphicon-plus"></span>
								</button>
								<button type="button" class="btnRemove btn btn-sm btn-danger">
									<span class="glyphicon glyphicon-minus"></span>
								</button>
							</div>
						</div>
						<lable class="help-inline text-danger"
							id="eventLocationDropDownLabel"></lable>
						<a href="#" id="byMonth" class="advanceSelect pull-left"><?=$this->lang->line('select_by_day2');?></a>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-4 control-label"><?=$this->lang->line('start_at')?>: <em>*</em></label>
				<div class="col-sm-8">
					<div id="datepicker2" class="input-append date" style="z-index: 2000;">
    					<div class="txtStart_time col-sm-6 col-lg-6" data-placement="right" data-align="top" style="padding:0px;">
							<input type="text" name="txtStart_time" id="txtStart_time" value="<?=@set_value('txtStart_time')?@set_value('txtStart_time'):@convertFromGMT($event->startTime, "h:i: A");?>" class="form-control" readonly required>
							<?php echo form_error('txtStart_time'); ?>
                        </div>
						<small data-original-title="<?=$this->lang->line('tooltip_when_will')?>?" data-placement="top" class="tooltips"><img src="<?=EXTERNAL_PATH."images/comment.png";?>" class="comment_box" /></small>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-4 control-label"><?=$this->lang->line('end_at')?>: <em>*</em></label>
				<div class="col-sm-8">
					<div id="datepicker3" class="input-append date" style="z-index: 2000;">
    					<div class="txtEnd_time col-sm-6 col-lg-6" data-placement="right" data-align="top" data-autoclose="false" style="padding:0px;">
							<input type="text" name="txtEnd_time" id="txtEnd_time" class="form-control" value="<?=@set_value('txtEnd_time')?@set_value('txtEnd_time'):@@convertFromGMT($event->endTime, "h:i: A");?>" readonly required>
							<?php echo form_error('txtEnd_time'); ?>
                        </div>
						<small data-original-title="<?=$this->lang->line('tooltip_when_will2')?>?" data-placement="top" class="tooltips"><img src="<?=EXTERNAL_PATH."images/comment.png";?>" class="comment_box" /></small>
					</div>
				</div>
			</div>
			<div class="form-group hidden">
				<label class="col-sm-4 control-label">Timezone: <em>*</em></label>
				<div class="col-sm-8">
					<select name="cmbTimezone" id="cmbTimezone"
						class="form-control chosen-select" disabled>
						<option value="">Select Timezone</option>
    		  <?php
        foreach ($timezones as $k => $v) {
            $attrTime = @$event->timeZone == $v->code ? "selected" : "";
            echo "<option value='" . $v->code . "' " . $attrTime . ">" . $v->name . "</option>";
        }
        ?>
		</select>
		<?php echo form_error('cmbTimezone'); ?>
				</div>
			</div>
			<div class="form-group hidden">
				<label class="col-sm-4 control-label"><?=$this->lang->line('re_invite')?>?:</label>
				<div class="col-sm-8">
					<div class="radio-inline">
						<label> <input type="radio" name="rdaReInvite" class="reInvite"
							value="1" checked>
    		<?=$this->lang->line('yes')?>
    		
  			</label>
					</div>
					<div class="radio-inline">
						<label> <input type="radio" name="rdaReInvite" class="reInvite"
							value="0"
							<?=(set_value('rdaReInvite')== "0" || @$event->reInvite == "0")?'checked':''?>>
    		<?=$this->lang->line('no')?>
    		
  			</label> <small
							data-original-title="<?=$this->lang->line('tooltip_only_applies')?>"
							data-placement="top" class="tooltips"><img
							src="<?=EXTERNAL_PATH."images/comment.png"; ?>"
							class="comment_box" /></small>
					</div>
		<?php echo form_error('rdaReInvite'); ?>
		
				</div>
			</div>
			<!--	<br>
				<br>-->
			<div class="form-group">
				<label class="col-sm-4 control-label"><?=$this->lang->line('ranked')?>?:</label>
				<div class="col-sm-8">
					<div class="radio-inline">
						<label> <input type="radio" name="rdaUnranked" class="unranked"
							value="0" <?=(@set_value('rdaUnranked') || @$event->unranked)==0?'checked':''?>>
    		<?=$this->lang->line('yes')?>
  			</label>
					</div>
					<div class="radio-inline">
						<label> <input type="radio" name="rdaUnranked" class="unranked"
							value="1" <?=(@set_value('rdaUnranked') || @$event->unranked)==1?'checked':''?>>
    		<?=$this->lang->line('no')?>
    		</label> <small
							data-original-title="<?=$this->lang->line('tooltip_if_you')?>"
							data-placement="top" class="tooltips"><img
							src="<?=EXTERNAL_PATH."images/comment.png"; ?>"
							class="comment_box" /></small>
					</div>
		<?php echo form_error('rdaUnranked'); ?>
		
					</div>
				<!--<br>
					<br>-->

			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label"><?=$this->lang->line('allow_walk_in')?>:</label>
				<div class="col-sm-8">
					<div class="radio-inline">
						<label> <input type="radio" name="rdaAllowWalkin"
							class="allowWalkin" value="0"
							<?=(@set_value('rdaAllowWalkin')!="0" || @$event->allowWalkInReal != "0")?'checked':''?>>
    		<?=$this->lang->line('no')?>
    		
  			</label>
					</div>
					<div class="radio-inline">
						<label> <input type="radio" name="rdaAllowWalkin"
							class="allowWalkin" value="1"
							<?=(@set_value('rdaAllowWalkin')==1 || @$event->allowWalkInReal == 1) ?'checked':''?>>
    		<?=$this->lang->line('yes')?>
    		</label> <small
							data-original-title="<?=$this->lang->line('tooltip_if_you_d')?>"
							data-placement="top" class="tooltips"><img
							src="<?=EXTERNAL_PATH."images/comment.png"; ?>"
							class="comment_box" /></small>
					</div>
		<?php echo form_error('rdaAllowWalkin'); ?>
		
					</div>
			</div>
			<!--<br>
					<br>-->

			<div class="form-group hidden">
				<label class="col-sm-4 control-label"><?=$this->lang->line('lock_event')?></label>
				<div class="col-sm-8">
					<div class="radio-inline">
						<label> <input type="radio" name="rdaLockStatus" class="lockEvent"
							value="1" <?=@set_value('rdaLockStatus')!=1?'checked':''?>>
    		<?=$this->lang->line('yes')?>
    		
  			</label>
					</div>
					<div class="radio-inline">
						<label> <input type="radio" name="rdaLockStatus" value="0"
							class="lockEvent" <?=set_value('rdaLockStatus')==0?'checked':''?>>
    		<?=$this->lang->line('no')?>
    		</label> <small
							data-original-title="<?=$this->lang->line('tooltip_no_more')?>"
							class="tooltips"><img
							src="<?=EXTERNAL_PATH."images/comment.png"; ?>"
							class="comment_box" /></small>
					</div> 
		<?php echo form_error('rdaLockStatus'); ?>
		
						</div>

			</div>

			<div class="form-group">
				<label class="col-sm-4 control-label"><?=$this->lang->line('cut_off_time')?>:<em>*</em></label>
				<div class="col-sm-8">
					<input type="number" name="txtLockBefore" id="txtLockBefore" min="1"
						value="<?=@set_value('txtLockBefore') ? @set_value('txtLockBefore') : @$event->hoursLockBefore?>"
						class="form-control onlydecimal"
						placeholder="Hours Before Starting Event (1-24)" value="0"
						required checkvalue readonly>
      <?php echo form_error('txtLockBefore'); ?> 
      <small
						data-original-title="<?=$this->lang->line('tooltip_type_in')?>"
						data-placement="top" class="tooltips"><img
						src="<?=EXTERNAL_PATH."images/comment.png"; ?>"
						class="comment_box" /></small> <span for="txtLockBefore"
						class="help-inline text-danger" style="display: none;"></span>
				</div>

			</div>


			<div class="form-group">
				<label class="col-sm-4 control-label"><?=$this->lang->line('performer_can_pick_time')?>?</label>
				<div class="col-sm-8">
					<select name="cmbPerformPickTime" class="form-control"
						onchange="pickTimeChange(this.value);">
						<option value="1" 
							<?=@set_value('cmbPerformPickTime') == "1" || @$event->performerCanPickTime == "1" ? 'selected':''?>><?=$this->lang->line('yes')?></option>
						<option value="0" 
							<?=(@set_value('cmbPerformPickTime')=="0" || @$event->performerCanPickTime == "0")?'selected':''?>><?=$this->lang->line('no')?></option>
					</select>
		<small data-original-title="<?=$this->lang->line('tooltip_can_the')?>"
						class="tooltips"><img
						src="<?=EXTERNAL_PATH."images/comment.png"; ?>"
						class="comment_box" /></small>
						<span for="cmbPerformPickTime"
						class="help-inline text-danger" style="display: none;"></span>
						<?php echo form_error('cmbPerformPickTime'); ?>
				</div>

			</div>

			<div class="form-group hidden" id="schedule">
				<label class="col-sm-4 control-label"><?=$this->lang->line('schedule_algorithm')?></label>
				<div class="col-sm-8">
					<div class="radio">
						<label> <input type="radio" name="rdaIdAlgo" value="3"
							class="scheduleAlgorithm" checked disabled>
    		<?=$this->lang->line('shuffle_list')?>
    		</label>
					</div>
					<div class="radio">
						<label> <input type="radio" name="rdaIdAlgo" value="1"
							class="scheduleAlgorithm"
							<?=@set_value('rdaIdAlgo')==1 || @$event->idAlgo ==1?'checked':''?>
							disabled>
    		<?=$this->lang->line('adjust_from_hightest_to_lowest_ranking')?>
    		</label>
					</div>
					<div class="radio">
						<label> <input type="radio" name="rdaIdAlgo" value="2"
							class="scheduleAlgorithm"
							<?=@set_value('rdaIdAlgo')==2 || @$event->idAlgo == 2 ?'checked':''?>
							disabled>
    		<?=$this->lang->line('adjust_from_lowest_to_highest_ranking')?>
    		</label>
					</div>
		<?php echo form_error('rdaIdAlgo'); ?>
    </div>
			</div>

			<div class="form-group">
				<label class="col-sm-4 control-label"><?=$this->lang->line('allow_host')?>?</label>
				<div class="col-sm-8">
					<div class="radio-inline">
						<label> <input type="radio" name="rdaAllowHost" value="1" checked>
    		<?=$this->lang->line('yes')?>
    		</label>
					</div>
					<div class="radio-inline">
						<label> <input type="radio" name="rdaAllowHost" value="0"
							<?=(@set_value('rdaAllowHost')=="0" || @$event->haveHost == "0") ?'checked':''?>>
    		<?=$this->lang->line('no')?>
    		
  			</label> <small
							data-original-title="<?=$this->lang->line('tooltip_will_this')?>"
							data-placement="top" class="tooltips"><img
							src="<?=EXTERNAL_PATH."images/comment.png"; ?>"
							class="comment_box" /></small>
					</div>
		<?php echo form_error('rdaAllowHost'); ?>
		
						</div>

			</div>

			<div class="form-group">
				<label class="col-sm-4 control-label"><?=$this->lang->line('allow_trading')?>?</label>
				<div class="col-sm-8">
					<div class="radio-inline">
						<label> <input type="radio" name="rdaAllowTrading" class="trading"
							value="1" checked>
    		<?=$this->lang->line('yes')?>
    		
  			</label>
					</div>
					<div class="radio-inline">
						<label> <input type="radio" name="rdaAllowTrading" class="trading"
							value="0"
							<?=(@set_value('rdaAllowTrading')=="0" ||  @$event->allowTrading == "0")?'checked':''?>>
    		<?=$this->lang->line('no')?>
    		</label> <small
							data-original-title="<?=$this->lang->line('tooltip_can_performers')?>?"
							data-placement="top" class="tooltips"><img
							src="<?=EXTERNAL_PATH."images/comment.png"; ?>"
							class="comment_box" /></small>
					</div>
		<?php echo form_error('rdaAllowTrading'); ?>
		
						</div>

			</div>

			<div class="form-group">
				<label class="col-sm-4 control-label"><?=$this->lang->line('time_per_performance')?>:<em>*</em></label>
				<div class="col-sm-8">
					<input type="text"
						value="<?=@set_value('txtTimePerPerformance')?@set_value('txtTimePerPerformance'):@$event->timePerPerformance?>"
						name="txtTimePerPerformance" class="form-control onlydecimal"
						placeholder="In Minutes" required>
      
      <small
						data-original-title="<?=$this->lang->line('tooltip_how_many')?>"
						data-placement="top" class="tooltips"><img
						src="<?=EXTERNAL_PATH."images/comment.png"; ?>"
						class="comment_box" /></small>
						<span for="txtTimePerPerformance"
						class="help-inline text-danger" style="display: none;"></span>
						<?php echo form_error('txtTimePerPerformance'); ?> 
				</div>

			</div>

			<div class="form-group hidden">
				<label class="col-sm-4 control-label"><?=$this->lang->line('custom_time_allowed')?>?</label>
				<div class="col-sm-8">
					<div class="radio-inline">
						<label> <input type="radio" name="rdaCustomTimeAllowed" value="1"
							class=""
							<?=(@set_value('rdaCustomTimeAllowed')!="0" || @$event->customTimeAllowed != "0") ?'checked':''?>><?=$this->lang->line('yes')?></label>
					</div>
					<div class="radio-inline">
						<label> <input type="radio" name="rdaCustomTimeAllowed" value="0"
							class=""
							<?=(@set_value('rdaCustomTimeAllowed')=="0" || @$event->customTimeAllowed=="0")?'checked':''?>><?=$this->lang->line('no')?></label>
						<small
							data-original-title="<?=$this->lang->line('tooltip_you_can')?>!"
							data-placement="top" class="tooltips"><img
							src="<?=EXTERNAL_PATH."images/comment.png"; ?>"
							class="comment_box" /></small>
					</div>
		<?php echo form_error('rdaCustomTimeAllowed'); ?>
		
						</div>
			</div>
					<?php
					$class = "hidden"; 
					if(@$planInfo [0]->performers == "-1") {
                        $class = "";    
                    }
					?>
					<div class="form-group <?=$class;?>">
				<label class="col-sm-4 control-label"><?=$this->lang->line('accept_all_performer_requests')?>?</label>
				<div class="col-sm-8">
					<div class="radio-inline">
						<label> <input type="radio" name="rdaAutoAccept" value="1"
							class=""
							<?=(@set_value('rdaAutoAccept')=="1" || @$event->autoAccept == "1")?'checked':''?>> <?=$this->lang->line('yes')?> </label>
					</div>
					<div class="radio-inline">
						<label> <input type="radio" name="rdaAutoAccept" value="0"
							class=""
							<?=(@set_value('rdaAutoAccept') =="0" || (@set_value('rdaAutoAccept') =="" && empty($event)) || @$event->autoAccept == "0" || $class == "hidden")?'checked':''?>> <?=$this->lang->line('no')?> </label>
						<small
							data-original-title="<?=$this->lang->line('tooltip_automatically_accept')?>!"
							data-placement="top" class="tooltips"><img
							src="<?=EXTERNAL_PATH."images/comment.png"; ?>"
							class="comment_box" /></small>
					</div>
		<?php echo form_error('rdaAutoAccept'); ?>
		
						</div>
			</div>

			<div class="form-group">
				<label class="col-sm-4 control-label"><?=$this->lang->line('automatic_reminder_e_mails')?>? <em>*</em></label>
				<div class="col-sm-8">
					<div class="radio-inline">
						<label> <input type="radio" name="rdaAutoReminder" value="0"
							checked>
    		<?=$this->lang->line('never')?>
    		</label>
					</div>
					<div class="radio-inline">
						<label> <input type="radio" name="rdaAutoReminder" value="1"
							<?=@set_value('rdaAutoAccept')==1 || @$event->autoReminder ==1 ?'checked':''?>>
    		<?=$this->lang->line('every_24_hours')?>
    		
  			</label>
					</div>

					<div class="radio-inline">
						<label> <input type="radio" name="rdaAutoReminder" value="2"
							<?=@set_value('rdaAutoAccept')==2 || @$event->autoReminder ==2 ?'checked':''?>>
    		<?=$this->lang->line('every_48_hours')?>
    		</label>
					</div>
		<?php echo form_error('rdaAutoReminder'); ?>
    </div>
			</div>

			<div class="form-group">
				<label class="col-sm-4 col-xs-12 control-label"><?=$this->lang->line('description')?></label>
				<div class="col-sm-8 col-xs-12">
					<textarea id="txtDescr" name="txtDescr"><?=@set_value('txtDescr')?@set_value('txtDescr'):@$this->utility->decodeText($event->descr,false)?></textarea>
					<small
						data-original-title="<?=$this->lang->line('tooltip_what_should')?>"
						data-placement="top" class="tooltips"><img
						src="<?=EXTERNAL_PATH."images/comment.png"; ?>"
						class="comment_box" /></small>
				</div>

			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10 text-center">
					<input type="submit" class="btn btn-success" name="submitfrm"
						id="btnEventSubmit" value="<?=isset($event->idEvent)?$this->lang->line('edit_event'):$this->lang->line('create_event');?>" />
					<a
						href="<?php echo SITEURL.$this->myvalues->eventDetails['controller']; ?>"
						class="btn btn-default"><?=$this->lang->line('cancel')?></a>
				</div>
			</div>
		</div>



				
				
<?php echo form_close(); ?>
</div>
</div>

<link
	href="<?=EXTERNAL_PATH?>js/datetimepicker/bootstrap-datetimepicker.min.css"
	rel="stylesheet" type="text/css">
<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/datetimepicker/moment.min.js"></script>
<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
	
<script type="text/javascript" src="<?=EXTERNAL_PATH?>js/dist/jquery-clockpicker.min.js"></script>
<script type="text/javascript" src="<?=EXTERNAL_PATH?>js/dist/highlight.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=EXTERNAL_PATH?>js/dist/jquery-clockpicker.min.css">

<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/modules/event_create.js"></script>
<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/ckeditorbasic/ckeditor.js"></script>

<script>

  $(document).on("click",'.btnAdd',function(){
	   var value = '<div class = "reaptingDiv repeating-field"><div class="col-sm-4">'
		   +'<div class="form-group">'
		          +'<select name="cmbDaysInMonth[' + rowCount + ']" class="form-control" style="width:94%;">'
		          +'<option value="1"><?=$this->lang->line("first")?></option>'
    		      +'<option value="2"><?=$this->lang->line("second")?></option>'
    		      +'<option value="3"><?=$this->lang->line("third")?></option>'
    		      +'<option value="4"><?=$this->lang->line("fourth")?></option>'	
                  +'</select>'
                  +'</div>'
    		      +'</div>'
    		        +'<div class="col-sm-5 col-md-6">'
    		        +'<div class="form-group">'
                        +'<select name="cmbWeekInMonth[' + rowCount + '][]" class="form-control chosen-select" multiple="multiple"  style="width:94%;">'
                    		+'<option value="1"><?=$this->lang->line("monday")?></option>'
                    		+'<option value="2"><?=$this->lang->line("tuesday")?></option>'
                    		+'<option value="3"><?=$this->lang->line("wednesday")?></option>'
                    		+'<option value="4"><?=$this->lang->line("thursday")?></option>'
                    		+'<option value="5"><?=$this->lang->line("friday")?></option>'
                    		+'<option value="6"><?=$this->lang->line("saturday")?></option>'                    		
                    		+'<option value="7"><?=$this->lang->line("sunday")?></option>'	
                    	+'</select>'
                    	+'</div>'
                    +'</div>'
                    +'<div class="pull-right">'
                    +'<button type = "button"  class = "btnAdd btn btn-sm btn-primary"><span class="glyphicon glyphicon-plus"></span></button>' + '<button type = "button" class = "btnRemove btn btn-sm btn-danger"><span class="glyphicon glyphicon-minus"></span></button>'
                    +'</div>'
                     +'<span class="help-inline text-danger" for="eventDays" id = "eventDays"></span>'
                 +'</div>';
                 
	  $(this).closest('.reaptingDiv').after(value);
	  
	  if(occurrings) {
		  $('select[name="cmbDaysInMonth[' + rowCount + ']"]').val(occurrings).change();
	  }

	  if(daysList) {
		  $.each(daysList, function(key,value){
			  $('select[name="cmbWeekInMonth[' + rowCount + '][]"] option[value=' + value + ']').attr("selected",true).change();
		  });
		  
	  }
	  
	  rowCount = rowCount + 1;
	   $(".chosen-select").chosen();
	  });
  
  $(document).on("click",'.btnRemove',function(){
       if($(".reaptingDiv").length == 1){
          //alert('You can not remove all options.');
          return false;
       }
	   $(this).closest('.reaptingDiv').remove();
	  
	  });
 
  
  </script>
<script>
$(document).ready(function(){
	var loadDaysWeek = false;
	<?php

if (! empty($event->v_extra_data) && $event->isRecuring == "4" && empty($postData)) {
    $event->v_extra_data = json_decode($event->v_extra_data);
    ?>
	      <?php foreach ($event->v_extra_data as $k => $v): ?>
	          occurrings = '<?php echo $k; ?>';
              <?php if(is_array($v)): ?>
              daysList = $.parseJSON('<?php echo json_encode($v); ?>');
              loadDaysWeek = true;
              $(".btnAdd:last").click();
              <?php else: ?>
              $("select[name='cmbMonthDay[]'] option[value=" + <?=$v;?> +"]").attr("selected",true).change();
              <?php endif; ?>
                          
	      <?php
        
        usleep(100);
    endforeach
    ;
    ?>
	      $(".advanceSelect").click();
	      if(loadDaysWeek)
	      {
	    	    $("#byweek").click();
	    	    $(".btnRemove:eq(0)").click();
	      }
	<?php
}

if (! empty($event->v_extra_data) && $event->isRecuring == "3" && empty($postData)) {
    $event->v_extra_data = json_decode($event->v_extra_data);
    ?>
	      <?php foreach ($event->v_extra_data as $k => $v): ?>
	      $("#chkDay<?=$v;?>").prop("checked",true).change();                          
	      <?php
    endforeach
    ;
}
?>

//add /edit start event date, date picker functinality	      
	      
$('#txtStartDate').datetimepicker({
	ignoreReadonly : true,	
	minDate : <?php echo isset($event->idEvent) && date("Y-m-d") >= date("Y-m-d", strtotime($event->date))? "new Date(".date("Y",strtotime($event->date)).",".date("n",strtotime($event->date)) ." - 1,".date("d",strtotime($event->date)).")" : "'now'";?>,
	useCurrent : false,
	format : 'YYYY-MM-DD'
});
	      
});
</script>