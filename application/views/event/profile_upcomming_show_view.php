
<style>
.dataTables_filter {
	display: none;
}
</style>
<!-- .content -->
<div class="content" style="height: 100%;">

	<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>					
					</div>
					
					
					<?php
    /* we are using common view for profile so only this form will show when a fan account will be logged in */
    if ($this->pidGroup == FAN_GROUP) {
        ?>
<div class="row">
			<div class="text-right col-xs-12">					
<?=form_open(SITEURL.$controllerName.'/upcoming','id="frmSearchByDate" class="form-inline" role="form"')?>

			<div class="form-group" style="position: relative;">

					<div class="input-append date" style="z-index: 2000;">

						<input type="text" id="txtStartDate"
							value="<?php echo set_value('txtDate') ? set_value('txtDate') : date('m-d-Y') ; ?>"
							name="txtDate" placeholder="Select a date"
							class="form-control required" readonly />

					</div>
					<input type="hidden" name="hdnTime" id="hdnTime" value="" />
				</div>
				<button type="button" id="dateSearchSubmit" class="btn btn-success">Search</button>
				<label for="txtStartDate" class="error" style="margin-right: 145px;"><?php echo form_error('txtDate');?></label>


<?php echo form_close(); ?>
</div>
		</div>
<?php } ?>	
					
					
					<?php if($this->pidGroup == FAN_GROUP) {?>
		
		<div id="no-more-tables">

			<table
				class="table table-hover table-bordered table-striped dataTable cf"
				id="listEvent">
				<!-- table striped -->
				<thead class="cf">
					<tr>
						<!--<th><?=$this->lang->line('event_title')?></th>-->
						<th><?=$this->lang->line('venue')?></th>
						<th>Address</th>
						<th>Action</th>
						<!--<th><?=$this->lang->line('event_date')?> </th>
						<th class=""><?=$this->lang->line('start_time')?></th>
						<th class=""><?=$this->lang->line('end_time')?></th>-->
					</tr>
				</thead>
				<tbody>
   <?php
        $i = 1;
        
        foreach ($list as $l) {
            if (! event_end_time_crossed($l->schedule_date, $l->startTime)) {
                
                ?>
			<tr>
						<!--<td data-title="Event Title"><a href="<?php echo $this->utility->generateOverviewUrl($l->idEvent,$l->scheduleId); ?>"><?php echo $l->title; ?></a></td>-->
						<td data-title="Venue"><a
							href="<?php echo SITEURL.'view/'.$l->url.'?source=city'; ?>"><?php echo ucwords($l->chrName); ?></a>
						</td>
						<td data-title="City"><?php echo $l->city.', '.$l->state;?></td>
						<td data-title="Action">
        					<?php if($l->lockStatus==0){ ?>
        					   	<?php //echo $l->idVenue; print_r($venueFollowArray);  ?>
            					<?php if (in_array($l->idVenue, $venueFollowArray)) { ?>
            					   <a href="javascript:void(0)" data-following-id = "<?php echo $this->utility->encode($l->idVenue);?>" class="btn btn-sm btn-warning toggleBiteUnbite">Unbite</a>
            					<?php }else{ ?>
            					   <a href="javascript:void(0);" data-following-id="<?php echo $this->utility->encode($l->idVenue);?>" class="btn btn-sm btn-success toggleBiteUnbite">Bite</a>
            					<?php }?>
            				<?php } ?>	
        			
					   </td>
						<!--<td data-title="Event Date "><?php echo dateDisplay($l->schedule_date.''.$l->startTime); ?></td>
						<td data-title="Start Time" class=""><?php echo dateDisplay($l->schedule_date.''.$l->startTime,'h:i A'); ?></td>
						<td data-title="End Time" class=""><?php echo dateDisplay($l->end_date.''.$l->endTime,'h:i A'); ?></td>-->

					</tr>
			<?php
            }
        }
        ?>

	</tbody>
			</table>
		</div>
		<?php } ?>
	
  </div>
</div>
</div>
</div>
<?php  form_close(); ?>

<!-- Modal -->


<script>

/* this event check that search with date form is validate or not */
$(document).on('click','#dateSearchSubmit',function(){
	/* get current date of user's system date and append with date of datepicker */
    var now = new Date(Date.now());
    var getHours = now.getHours() > 9 ? now.getHours() : '0'+now.getHours();
    var getMinutes = now.getMinutes() > 9 ? now.getMinutes() : '0'+now.getMinutes();
    var second = now.getSeconds() > 9 ? now.getSeconds() : '0'+now.getSeconds();
    var formatted = getHours + ":" + getMinutes + ":" +second;
    
    $("#hdnTime").val(formatted);
    
	if($("#frmSearchByDate").valid())
	{
		$("#frmSearchByDate").submit();
		}else
		{
			return false;
			}
 });


</script>


<link
	href="<?=EXTERNAL_PATH?>js/datetimepicker/bootstrap-datetimepicker.min.css"
	rel="stylesheet" type="text/css">
<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/datetimepicker/moment.min.js"></script>
<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/datetimepicker/bootstrap-datetimepicker.min.js"></script>


<script type="text/javascript">
$(document).ready(function(){
$(function () {
	 $("#txtStartDate").datetimepicker({  
    ignoreReadonly : true,
   	minDate : 'now',
   	useCurrent : false,
   	format : 'MM-DD-YYYY'
    });
});

});

</script>


