<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>	
						<small data-original-title="Click on Show Title to View Details."
				data-placement="top" class="tooltips"><img
				src="<?=EXTERNAL_PATH?>images/comment.png" class="comment_box" /></small>
		</div>


<?=form_open(SITEURL.$controllerName.'/deleteEventSchedule','id="frm" onsubmit="return confirm_delete_events();"')?>
<div id="no-more-tables">
<table class="table table-bordered table-striped table-hover dataTable cf">
			<thead class="cf">
				<tr role="row">
					<th style="width: 1%" class=""><input type="checkbox" name="chkAll"
						onclick="setAll();"></th>
					<th style="width: 14%">Show Title</th>
					<th style="width: 5%" class="">Trading Request</th>
					<th style="width: 20%" class="eventListDateTime">Date</th>
					<th style="width: 7%" class="">Time</th>
					<th style="width: 10%" class="">End Time</th>
					<th style="width: 5%" class="">Have Host?</th>
					<th style="width: 5%" class="">Time Per Performance</th>
					<th style="width: 5%; text-align: left;" class="">Link</th>
					<th style="width: 30%; text-align: left;">Actions</th>
				</tr>
			</thead>
			<tbody role="alert" aria-live="polite" aria-relevant="all">
       <?php
    
    $i = 1;
    $flag = false;
    $class = "";
   
    foreach ($list as $r) {
        
        $flag = true;
        $style = "";
        if (! event_end_time_crossed($r->end_date,$r->endTime)) {
            // if (strtotime($r->date." ".$r->endTime)>time()){
            $style = 'style="background: rgb(221, 221, 221);"';
        
        
        ?>
				<tr <?php //echo $style; ?>>
					<td ><input type="checkbox" name="chkstatus[]"
						value="<?php echo $this->utility->encode($r->table_id); ?>"
						class="inpt_c1"></td>
					<td data-title="Show Title" class=""><a
						href="<?=$this->utility->generateOverviewUrl($r->idEvent,$r->table_id);?>"><?php echo $this->utility->decodeText(ucwords($r->title)); ?></a>
               			<?php /*if ($r->recurringNotice==1){ ?>
						<span class="badge bg-success">New Recurring</span>
						<?php } */ ?>
               		</td>
					<td data-title="Trading Request" class="">
               		<?php  if ($r->allowTrading==1){ ?>
               			<label class="label label-success">Allowed</label>
               		<?php } else { ?>
               			<label class="label label-danger">Not Allowed</label>
               		<?php } ?>
               		</td>
					<td data-title="Date" class="eventListDateTime"><?php  echo dateDisplay($r->start_date.''.$r->startTime); ?></td>
					<td data-title="Time" class="eventListDateTime"> <?php echo dateDisplay($r->start_date.''.$r->startTime,'h:i A'); ?></td>
					<td data-title="End Time" class=""><?php echo dateDisplay($r->end_date.''.$r->endTime,'h:i A'); ?></td>
					<td data-title="Have Host?" class=""><?php echo ($r->haveHost==0)?"No":"Yes"; ?></td>
					<td data-title="Time Per Performance" class=""><?php echo $this->utility->decodeText(ucwords($r->timePerPerformance)); ?></td>
					<td data-title="Link" style="text-align: center;" class=""><img
						src="<?=EXTERNAL_PATH."images/eventLink.png"?>"
						class="tooltips copyEventURL"
						data-urlid="<?php echo $this->utility->encode($r->table_id); ?>" data-placement="top"
						data-original-title="Copy Link to Clipboard" style="width: 20px;" data-clipboard-text="<?php echo $this->utility->generateOverviewUrl($r->idEvent,$r->table_id); ?>"/>
						
					</td>
					<td data-title="Actions" class="center sorting_1" style="text-align: left;">
						<div class="">
               			<?php if ($style!=''){ ?>
               				<a
								href="<?=SITEURL. $this->myvalues->timeTableDetails["controller"] ."?eventId=".$this->utility->encode($r->table_id); ?>"
								class="btn btn-sm btn-success">See the List</a> <a
								href="<?=SITEURL.$this->myvalues->performerDetails['controller']."/bulkInvite/".$this->utility->encode($r->table_id); ?>"
								class="btn btn-sm btn-info">Invite Performers</a>
               			<?php
        }
        else {
            ?>	
               				<a
								href="<?=$this->utility->generateOverviewUrl($r->idEvent,$r->table_id);?>"
								class="btn btn-sm btn-info">Show Details</a>
								
               				<a
								href="<?=SITEURL.$this->myvalues->rankDetails['controller']."/performerList/".$this->utility->encode($r->table_id); ?>"
								class="btn btn-sm btn-warning">Performers</a>
               			<?php
           }
           
        if (@in_array($r->table_id, $pending_ranking)) {
            ?>
               					<a
								href="<?=SITEURL.$this->myvalues->rankDetails['controller']."/performerList/".$this->utility->encode($r->table_id); ?> "><span
								class="badge bg-success">Ranking Pending</span></a>
               					<?php
        }
        ?>
               			
               			</div>
					</td>
				</tr>
				<?php
        	$i ++;
    }}
    ?>
    
         </tbody>
		</table>
		</div>
<?php if ($flag){ ?>
	<div style="display: block;">
			<input class="btn btn-danger" type="submit" name="deleteEvents"
				value="Delete" />
		</div>
<?php } ?>

    </div>
</div>

<?php echo form_close(); ?>
<ul class="pagination">
	<?php echo @$paging; ?>
</ul>
		<link href="<?=EXTERNAL_PATH?>css/chosen.css" rel="stylesheet"
			type="text/css">		
<script src="<?=EXTERNAL_PATH?>js/jquery.zeroclipboard.js"></script>
		<script>
$('.tooltips').tooltip();
function confirm_delete_events(){
	if (!Checkbox("frm", "chkstatus[]")){
		alert("Please select an Event Schedule to delete!");
		return false;	
	}
	return confirm('This will delete the event schedule(s) permanently. Are you sure?');
}
function Checkbox(TheForm, Field){
	var obj = document.forms[TheForm].elements[Field];
	var res = false;
	if(obj.length > 0){
		for(var i=0; i < obj.length; i++){
			if(obj[i].checked == true){
				res = true;
			}
		}
	}
	else{
		if(obj.checked == true){
				res = true;
		}
	}
	return (res);
}

function setAll(){
	if(frm.chkAll.checked == true){
		checkAll("frm", "chkstatus[]");
	}
	else{
		clearAll("frm", "chkstatus[]");
	}
}	

function checkAll(TheForm, Field){
	var obj = document.forms[TheForm].elements[Field];
	if(obj.length > 0){
		for(var i=0; i < obj.length; i++){
			obj[i].checked = true;
		}
	}
	else{
		obj.checked = true;
	}
}
function clearAll(TheForm, Field){
	var obj = document.forms[TheForm].elements[Field];
	if(obj.length > 0){
		for(var i=0; i < obj.length; i++){
			obj[i].checked = false;
		}
	}
	else{
		obj.checked = false;
	}
}

</script>

<script>
    var clipboard = new Clipboard('.copyEventURL');

    clipboard.on('success', function(e) {
        console.log(e);
        alert("Show URL Copied to Clipboard");
    });

    clipboard.on('error', function(e) {
        console.log(e);
    });
</script>
