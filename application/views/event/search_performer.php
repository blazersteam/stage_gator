
<div class="content" style="height: 100%;">

	<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>					
					</div>

		<div class="alert alert-success text-center myMessage"
			style="display: none"></div>
	<?php
$URL = SITEURL . $controllerName;
echo $this->session->flashdata('myMessage') . form_open($URL . "/searchPerformer/", 'id="frm" class = "form-inline marginBtm10"');
?>
  <div class="form-group" style="text-align: left;">
			<select name="cmbStyle[]" class="form-control chosen-select" multiple
				data-placeholder="Search Style">
				<option></option>
    <?php
    
    foreach ($styles as $s) {
        if (in_array($s->idStyle, set_value('cmbStyle'))) {
            $selected = 'selected';
        }
        else {
            $selected = '';
        }
        ?>
  		<option value="<?php echo $s->idStyle; ?>" <?=@$selected?>><?php echo $s->nameStyle; ?></option>
    <?php } ?>
    </select>
		</div>
		<div class="form-group">
			<input type="text" name="txtName" class="form-control"
				placeholder="Search Name" value="<?=@set_value('txtName')?>" />
		</div>

		<div class="form-group">
			<input type="text" name="txtCity" class="form-control"
				placeholder="Search City or State"
				value="<?=@set_value('txtCity')?>" />
		</div>

		<button type="submit" class="btn btn-success">Search</button>
<?php echo form_close(); ?>
<!-- </div> -->

<?php
if (isset($result)) {
    if (count($result) > 0) {
        ?>
		<div id="no-more-tables">
		<table
		class="table table-bordered table-hover table-striped display dataTable cf">
		<thead class="cf">
			<tr role="row">
				<th style="width: 7%" class="">Sr #</th>
				<th>Name</th>
				<th style="width: 20%" class="">Styles</th>
				<th style="width: 10%">Image</th>
				<th style="width: 15%" class="">City</th>
				<th style="width: 10%">Rating</th>
				<!--<th style="width: 10%">Rating OLD</th>-->
				<th style="width: 10%">Action</th>
			</tr>
		</thead>

		<tbody role="alert" aria-live="polite" aria-relevant="all">
       <?php
        $i = 1;
        $class = "";
        foreach ($result as $r) {
            if ($i % 2 == 0) {
                $class = 'class="gradeA even"';
            }
            else {
                $class = 'class="gradeA odd"';
            }
            
            $styles = $this->this_model->getUserStyles($r->user_id);
            ?>
				<tr <?php echo $class; ?>>
				<td data-title="Sr #" class=""><?php echo $i; ?></td>
               		<?php 
/*
                   * ?>
                   * <td class=""><a href="<?php echo base_url()."performer/overview"; ?>"><?php echo
                   * ucwords($r->chrName); ?></a></td>
                   */
            ?>
               		<td data-title="Name" class=""><a
					href="<?php echo SITEURL."view/".$r->url; ?>"
					title="Overview"><?php echo ucwords($r->chrName); ?></a></td>
				<td data-title="Styles" class=""> 
               			<?php
            $n = 0;
            foreach ($styles as $row) {
                if ($n) {
                    echo ", ";
                }
                echo $row->nameStyle;
                $n ++;
            }
            ?>
               		</td>
				<td data-title="Image" title="<?php echo $r->image; ?>" align="center" class="">
               			<?php if (!empty($r->image) && file_exists("./external/images/profiles/".$r->image)) { ?>
               				<img
					src="<?php echo EXTERNAL_PATH."images/profiles/".$r->image; ?>"
					style="width: 50px; height: 50px;" />
               			<?php } else { ?>
               				<img
					src="<?php echo EXTERNAL_PATH."images/profiles/eleven.png"; ?>"
					style="width: 50px; height: 50px;" />
               			<?php } ?>
               		</td>
				<td data-title="City" class=""><?php echo ucwords($r->mem_city); echo ", "; echo $r->mem_state; ?></td>

				<td data-title="Rating" class=""><?php  echo number_format($r->rate,2); ?></td>
               		<?php
            $User = $this->session->userdata("UserData");
            
            if ($r->followerId != $User->user_id) {
                // if(fan_model::checkIfFollowing($User->user_id, $r->user_id)){                 ?>
               			<td data-title="Action"><a
					href="javascript:void(0)"
					data-following-id = "<?php echo $this->utility->encode($r->user_id);?>"
					class="btn btn-sm btn-success toggleBiteUnbite">Bite</a>
               			<?php }else{ ?>				
				<td data-title="Action"><a
					href="javascript:void(0)"
					data-following-id = "<?php echo $this->utility->encode($r->user_id);?>"
					class="btn btn-sm btn-warning toggleBiteUnbite">Unbite</a>
               			<?php
            }
            if ($User->idGrp == 2) {
                ?>
               			<a
					href="<?php echo SITEURL.$this->myvalues->invitationDetails['controller']."/inviteForEvent/".$this->utility->encode($r->user_id); ?>"
					class="btn btn-sm btn-success">Invite</a>
               		<?php
            }
            // }             ?>
             	
			
			
			
			</tr>
				<?php
            $i ++;
        }
        ?>
    
         </tbody>

	</table>
	</div>
	
<?php  } else { ?>
	<div class="alert alert-danger">No Record Found</div>	
<?php
    }
}
?></div>
</div>

<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/chosen.jquery.js"></script>
<link href="<?=EXTERNAL_PATH?>css/chosen.css" rel="stylesheet"
	type="text/css">
<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/modules/search_performer.js"></script>


