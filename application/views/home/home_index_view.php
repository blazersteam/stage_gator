<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="<?=EXTERNAL_PATH?>images/favicon.png">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title><?=$title?></title>
<?php $newPath = EXTERNAL_PATH.'new_style/'; ?>
<!-- Bootstrap -->
<link href="<?=$newPath?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?=$newPath?>css/font-awesome.min.css" rel="stylesheet">
<link href="<?=$newPath?>css/style.css" rel="stylesheet">
<link href="<?=$newPath?>css/custom.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<?php 
/* For check iOS request or not */
$source = $this->input->get('source');
$showLoginRegistrationLink = 1;
if($this->input->cookie('source')){
    $showLoginRegistrationLink = 0;
}else if(!empty($source) && $source == 'iOS'){
    $showLoginRegistrationLink = 0;
    $cookie = array(
        'name'   => 'source',
        'value'  => 'iOS',
        'expire' => time() + 60 * 60 * 24 * 365
    );
    $this->input->set_cookie($cookie);
}
/* End Here */
?>
<body>
	<div id="wrap">
		<div class="background_index_img">

			<!-- Navigation -->
			<nav class="navbar navbar-fixed-top main-header" role="navigation">
				<div class="container">
					<div class="row hidden-xs">
						<div class="col-sm-12">
							<div class="top-header text-right">
								<ul class="list-inline">
									<li><a href="https://www.facebook.com/StageGator/"><i
											class="fa fa-facebook"></i></a></li>
									<li><a href="https://twitter.com/stage_gator"><i
											class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target=".navbar-ex1-collapse">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#"> <img class=" img-responsive"
							src="<?=EXTERNAL_PATH?>images/new-logo.png" alt="logo" /></a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<?php if($showLoginRegistrationLink){ ?>
						<div class="login-res-btn">
							<a href="<?=SITEURL.$this->myvalues->loginDetails['controller']?>" class="btn btn-hollow hidden-xs"><i class="fa fa-lock"></i>Login</a> 
							<a href="<?=SITEURL.$this->myvalues->userRegisterDetails['controller']?>" class="btn btn-green hidden-xs"><i class="fa fa-file-text-o"></i>Register</a>
						</div>
						<?php } ?>
						<ul class="nav navbar-nav navbar-right">
							<!-- Hidden li included to remove active class from about link when scrolled up past about section -->
							<li class="hidden"><a href="#">About Us</a></li>
							<li><a id="scroll_up2" href="#how-it-works">How It Works</a></li>
							<li class="hidden"><a href="#">FAQ</a></li>
							<li class="hidden"><a href="<?=SITEURL . "page/contact_us";?>">Contact Us</a></li>
							<?php if($showLoginRegistrationLink){ ?>
							<li class="visible-xs"><a href="<?=SITEURL.$this->myvalues->loginDetails['controller']?>">Login</a></li>
							<li class="visible-xs"><a href="<?=SITEURL.$this->myvalues->userRegisterDetails['controller']?>">Register</a></li>
							<?php } ?>
						</ul>

					</div>
					<!-- /.navbar-collapse -->
				</div>
				<!-- /.container -->
			</nav>

			<!-- .Hero -->
			<div class="hero text-center">
				<h1>
					RUN YOUR OPEN MIC OR SHOWCASE WITH <span>STAGEGATOR</span>
				</h1>

				<div class="col-xs-6 col-xs-offset-3 col-sm-4 col-sm-offset-4">

					<div class="video">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe src="https://www.youtube.com/embed/rdcIaOdQeW0"
								frameborder="0"> </iframe>
						</div>
					</div>
				</div>
			</div>
			<!-- /.Hero -->
			<div class="container">
				<div class="row">
					<div class="signup-free">
						<div class="col-xs-12 text-center">
							<p>
								Performers, Venues and Fans sign up <span>FREE!</span>
							</p>
							<a
								href="<?=SITEURL.$this->myvalues->userRegisterDetails['controller']?>"
								class="btn btn-hollow fontsize20"> Join now! <i
								class="fa fa-caret-right"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- .How it Works -->
		<section class="howit-works text-center" id="scroll_down"><!-- id="how-it-works" -->
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h2 class="heading">
							<span>How</span> it Works ?
						</h2>
						<hr class="heading-underline">
						<p class="para">
							Venues create events online, then Performers sign up for slots to
							perform. Venues and Performers can post their events on social<br
								class="hidden-xs hidden-sm"> media and invite guests to attend.
							Fans can now support their favorite local artists by knowing
							their exact time and place of the performance<br
								class="hidden-xs hidden-sm"> and even tip performers directly
							through the site.
						</p>
					</div>
				</div>
			</div>
		</section>
		<!-- /.How it Works -->
		<!-- .Venue -->
		<section class="venue">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-sm-push-6">
						<h2 class="sub-heading">Venue</h2>
						<p class="para">
							Venues can now manage their shows by simply creating a
							profile, then<br class="hidden-xs hidden-sm"> create an event.
							Afterwards, the venue can invite performers to<br
								class="hidden-xs hidden-sm"> participate, and performers can
							apply to participate through site.
						</p>
						<div class="row">
							<div class="col-sm-6">
								<div class="venue-steps">
									<i class="fa fa-check-square-o"></i> <span>Create a Profile</span>
								</div>
								<div class="venue-steps">
									<i class="fa fa-check-square-o"></i> <span>Create an Event</span>
								</div>
								<div class="venue-steps">
									<i class="fa fa-check-square-o"></i> <span>Invite Performers</span>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="venue-steps">
									<i class="fa fa-check-square-o"></i> <span>Book Performers</span>
								</div>
								<div class="venue-steps">
									<i class="fa fa-check-square-o"></i> <span>Promote event through
										Social Media</span>
								</div>
								<div class="venue-steps">
									<i class="fa fa-check-square-o"></i> <span>Manage the event</span>
								</div>
							</div>
						</div>
						<!-- <a class="read-more-link" href="#">Read More <i
							class="fa fa-caret-right"></i></a> -->
					</div>
					<div
						class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1 text-center col-sm-pull-6">
						<div class="venue-feature">
							<div class="row border-bottom">
								<div class="col-xs-6 border-right">
									<i class="fa fa-newspaper-o fa-3x"></i>
									<h3>Create a Profile</h3>
								</div>
								<div class="col-xs-6">
									<i class="fa fa-user fa-3x"></i>
									<h3>create Series</h3>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6 border-right">
									<i class="fa fa-user fa-3x"></i>
									<h3>Invite Performer</h3>
								</div>
								<div class="col-xs-6">
									<i class="fa fa-microphone-slash fa-3x"></i>
									<h3>Book performer</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- /.Venue -->
		<!-- .performer -->
		<section class="performer">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
						<h2 class="sub-heading">Performer</h2>
						<p class="para">Performers can now book their stage time in
							advance. No more sitting around and waiting for your chance. You
							can let your fans know exactly when you're performing next, grow
							your fan-base through the "Bite" feature, and receive tips
							through the site!</p>
						<div class="row">
							<div class="col-sm-6">
								<div class="venue-steps">
									<i class="fa fa-check-square-o"></i> <span>Create a Profile</span>
								</div>
								<div class="venue-steps">
									<i class="fa fa-check-square-o"></i> <span>Find an event</span>
								</div>
								<div class="venue-steps">
									<i class="fa fa-check-square-o"></i> <span>Book your time</span>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="venue-steps">
									<i class="fa fa-check-square-o"></i> <span>Promote your show</span>
								</div>
								<div class="venue-steps">
									<i class="fa fa-check-square-o"></i> <span>Tell People to
										"Bite" you</span>
								</div>
								<div class="venue-steps">
									<i class="fa fa-check-square-o"></i> <span>Get paid through
										tips</span>
								</div>
							</div>
						</div>
						<!-- <a class="read-more-link" href="#">Read More <i
							class="fa fa-caret-right"></i></a> -->
					</div>
					<div class="col-xs-12 col-sm-6 col-md-5 text-center">
						<div class="venue-feature">
							<div class="row border-bottom">
								<div class="col-xs-6">
									<i class="fa fa-newspaper-o fa-3x"></i>
									<h3>Create a Profile</h3>
								</div>
								<div class="col-xs-6 border-left">
									<i class="fa fa-user fa-3x"></i>
									<h3>Get invitations for Events/Series</h3>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<i class="fa fa-user fa-3x"></i>
									<h3>Book your Slot</h3>
								</div>
								<div class="col-xs-6 border-left">
									<i class="fa fa-microphone-slash fa-3x"></i>
									<h3>Get confirmation</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- /.performer -->
		<!-- .Fans -->
		<section class="venue padbtm-70">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-sm-push-6">
						<h2 class="sub-heading">Fans</h2>
						<p class="para">
							Now you can support your favorite Performers and Venues! You will
							have<br class="hidden-xs hidden-sm"> bragging rites and the
							ability to tell your friends "you saw them <br
								class="hidden-xs hidden-sm"> first". You can follow the careers
							of your favorite local performers and get<br
								class="hidden-xs hidden-sm"> push notifications whenever they're
							going to perform next.
						</p>
						<div class="row">
							<div class="col-sm-6">
								<div class="venue-steps">
									<i class="fa fa-check-square-o"></i> <span>Find a Performer or
										Event</span>
								</div>
								<div class="venue-steps">
									<i class="fa fa-check-square-o"></i> <span>Follow Performers or
										Venues through "Bites"</span>
								</div>
								<div class="venue-steps">
									<i class="fa fa-check-square-o"></i> <span>Support your local
										Performance community</span>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="venue-steps">
									<i class="fa fa-check-square-o"></i> <span>Tip directly through
										your phone</span>
								</div>
								<div class="venue-steps">
									<i class="fa fa-check-square-o"></i> <span>Be the first to find
										the shows that no one knows about but wishes they did</span>
								</div>
							</div>
						</div>
						<!-- <a class="read-more-link" href="#">Read More <i
							class="fa fa-caret-right"></i></a> -->
					</div>
					<div
						class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1 text-center col-sm-pull-6">
						<div class="venue-feature">
							<div class="row border-bottom">
								<div class="col-xs-12">
									<i class="fa fa-newspaper-o fa-3x"></i>
									<h3>Find a Performer Or show</h3>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6 border-right">
									<i class="fa fa-user fa-3x"></i>
									<h3>get notification of performer's show</h3>
								</div>
								<div class="col-xs-6">
									<i class="fa fa-microphone-slash fa-3x"></i>
									<h3>Give tips</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- /.Fans -->
		<!-- .Talent -->
		<section class="talent">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-8 col-sm-offset-2">
						<div class="video-main text-center">
							<h3>Watch the Video</h3>
							<div class="video">
								<div class="embed-responsive embed-responsive-16by9">
									<iframe src="https://www.youtube.com/embed/rdcIaOdQeW0"
										frameborder="0"> </iframe>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12">
						<h2 class="heading text-center">
							<span>Our</span> Talent
						</h2>
						<hr class="heading-underline">
						<div class="row text-center">
							<div class="controls">
								<a class="left fa fa-chevron-left fa-lg" href="#carousel123"
									data-slide="prev"></a><a
									class="right fa fa-chevron-right fa-lg" href="#carousel123"
									data-slide="next"></a>
							</div>
							<div class="col-md-12">
								<div class="row">
									<div class="carousel carousel-showmanymoveone slide"
										id="carousel123">
										<div class="carousel-inner">
											<div class="item active text-center">
												<div class="col-sm-3 col-xs-12">
													<div class="talent-overlay">
														<a href="#"> <img src="<?=$newPath?>images/talent-img.jpg"
															class="img-responsive">
															<div class="talent-overlay-text">
																Singer/Song writers
															</div>
														</a>
													</div>
												</div>
											</div>
											<div class="item text-center">
												<div class="col-sm-3 col-xs-12">
													<div class="talent-overlay">
														<a href="#"> <img
															src="<?=$newPath?>images/talent-img2.jpg"
															class="img-responsive">
															<div class="talent-overlay-text">
																comedians<br>
															</div>
														</a>
													</div>
												</div>
											</div>
											<!--<div class="item text-center">
												<div class="col-sm-3 col-xs-12">
													<div class="talent-overlay">
														<a href="#"> <img
															src="<?=$newPath?>images/talent-img3.jpg"
															class="img-responsive">
															<div class="talent-overlay-text">
																<br>
															</div>
														</a>
													</div>
												</div>
											</div>
											<div class="item text-center">
												<div class="col-sm-3 col-xs-12">
													<div class="talent-overlay">
														<a href="#"> <img
															src="<?=$newPath?>images/talent-img4.jpg"
															class="img-responsive">
															<div class="talent-overlay-text">
																<br>
															</div>
														</a>
													</div>
												</div>
											</div> -->
											<div class="item text-center">
												<div class="col-sm-3 col-xs-12">
													<div class="talent-overlay">
														<a href="#"> <img src="<?=$newPath?>images/talent-img5.jpg"
															class="img-responsive">
															<div class="talent-overlay-text">
																burlesque
															</div>
														</a>
													</div>
												</div>
											</div>
											<div class="item text-center">
												<div class="col-sm-3 col-xs-12">
													<div class="talent-overlay">
														<a href="#"> <img
															src="<?=$newPath?>images/talent-img6.jpg"
															class="img-responsive">
															<div class="talent-overlay-text">
																poets<br>
															</div>
														</a>
													</div>
												</div>
											</div>
											<div class="item text-center">
												<div class="col-sm-3 col-xs-12">
													<div class="talent-overlay">
														<a href="#"> <img
															src="<?=$newPath?>images/talent-img7.jpg"
															class="img-responsive">
															<div class="talent-overlay-text">
																story tellers<br>
															</div>
														</a>
													</div>
												</div>
											</div>
											<div class="item text-center">
												<div class="col-sm-3 col-xs-12">
													<div class="talent-overlay">
														<a href="#"> <img
															src="<?=$newPath?>images/talent-img8.jpg"
															class="img-responsive">
															<div class="talent-overlay-text">
																performance artists<br>
															</div>
														</a>
													</div>
												</div>
											</div>
											<div class="item text-center">
												<div class="col-sm-3 col-xs-12">
													<div class="talent-overlay">
														<a href="#"> <img
															src="<?=$newPath?>images/talent-img9.jpg"
															class="img-responsive">
															<div class="talent-overlay-text">
																drag<br>
															</div>
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- <button type="" class="btn btn-green">
										View All <i class="fa fa-caret-right"></i>
									</button> -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- /.Talent -->
		<!-- .Event -->
		<?php /* ---comment temporary-start--  
		<section class="event">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-8">
						<h3 class="sub-headingLarge">Up coming Events</h3>
						<div class="row">
							<div class="event-schedule">
								<div class="col-sm-12 col-md-6">
														   <?php
																	
																	for($i = 0; $i < count ( $events [0] ); $i ++) {
																		?>
																<div class="media">
										<a class="media-left" href="#"> <span><em><?=dateDisplay($events[0][$i]->start_date.' '.$events[0][$i]->startTime,'d')?></em></span>
											<span><?=dateDisplay($events[0][$i]->start_date.' '.$events[0][$i]->startTime,'M')?></span>
										</a>
										<div class="media-body">
											<h4 class="media-heading"><?=$events[0][$i]->title?></h4>
											<span>Comedy show</span>
											<ul class="list-inline">
												<li><i class="fa fa-map-marker"></i><?=$events[0][$i]->city?></li>
												<li><i class="fa fa-clock-o"></i><?=dateDisplay($events[0][$i]->start_date.' '.$events[0][$i]->startTime,'H:i').' to '.dateDisplay($events[0][$i]->start_date.' '.$events[0][$i]->endTime,'H:i')?></li>
											</ul>
										</div>
									</div>
															<?php } ?>
														</div>
								<div class="col-md-6 hidden-sm">
																<?php
																if (! empty ( $events [1] )) {
																	for($i = 0; $i < count ( $events [1] ); $i ++) {
																		?>
																<div class="media">
										<a class="media-left" href="#"> <span><em><?=dateDisplay($events[1][$i]->start_date.' '.$events[1][$i]->startTime,'d')?></em></span>
											<span><?=dateDisplay($events[1][$i]->start_date.' '.$events[1][$i]->startTime,'M')?></span>
										</a>
										<div class="media-body">
											<h4 class="media-heading"><?=$events[1][$i]->title?></h4>
											<span>Comedy show</span>
											<ul class="list-inline">
												<li><i class="fa fa-map-marker"></i><?=$events[1][$i]->city?></li>
												<li><i class="fa fa-clock-o"></i><?=dateDisplay($events[1][$i]->start_date.' '.$events[1][$i]->startTime,'H:i ').' to '.dateDisplay($events[1][$i]->start_date.' '.$events[1][$i]->endTime,'H:i')?></li>
											</ul>
										</div>
									</div>
															<?php } } ?>
																
														</div>
							</div>
						</div>
						<center>
							<!-- THIS BUTTON IS HIDDEN -->
							<button type="" class="hidden btn btn-hollow">
								View More <i class="fa fa-caret-right"></i>
							</button>
						</center>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="testinomial">
							<div class="carousel">
								<div class="carousel slide" id="fade-quote-carousel"
									data-ride="carousel" data-interval="3000">
									<!-- Carousel indicators -->
									<ol class="carousel-indicators">
										<li data-target="#fade-quote-carousel" data-slide-to="0"
											class="active"></li>
										<li data-target="#fade-quote-carousel" data-slide-to="1"></li>
										<li data-target="#fade-quote-carousel" data-slide-to="2"></li>
										<li data-target="#fade-quote-carousel" data-slide-to="3"></li>
									</ol>
									<!-- Carousel items -->
									<div class="carousel-inner text-center">
																<?php
																
																$i = 1;
																
																foreach ( $performers as $performer ) {
																	$class = $i == 1 ? 'item active' : 'item';
																	?>
																		<div class="<?=$class?>">
											<div class="profile-circle" style="">
												
												<?php if (!empty($performer->image) && file_exists("./external/images/profiles/".$performer->image)){ ?>
		<img class="img-responsive" src="<?=EXTERNAL_PATH."images/profiles/".$performer->image; ?>">
	<?php } else if(!empty($performer->image) && filter_var($performer->image, FILTER_VALIDATE_URL) == true) { ?>
		<img class="img-responsive" src="<?=$performer->image?>">
	<?php }else{ ?>
<img src="<?=EXTERNAL_PATH?>images/noimage.png" style="width: 100%;">	
<?php
} ?>
											</div>
											<h4><?=$performer->nameStyle?><span><?=$performer->chrName?></span><br>
																						<?=$performer->mem_city?></h4>
											<div class="rating col-md-12">
												<input type="hidden" name="rating"
													value="<?php echo (!is_null($performer->rate) && is_numeric($performer->rate))?$performer->rate:0; ?>"
													id="hidden-ranking" />
												<div class="raty" id="ranking"
													data-score="<?php echo (!is_null($performer->rate) && is_numeric($performer->rate))?$performer->rate:0; ?>"
													style="width: 100px;"></div>
											</div>
											<blockquote>
												<p><?=$performer->descr?></p>
											</blockquote>
										</div>
																		<?php
																	$i ++;
																}
																?>
																</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		 ---comment temporary-start-- */ ?>
		<!-- /.Event -->
		<!-- .Signup Newsletter -->
		<section class="signup-newsletter hidden">
			<div class="container">
				<div class="row">
					<div class="col-sm-5 col-md-4 col-lg-5">
						<h4>Signup</h4>
						<p>Our weekly newsletter and get news/events and more.</p>
					</div>
					<div class="col-sm-7 col-md-8 col-lg-7">
						<form id="">
							<input type="text" name="name" class="" required=""
								placeholder="Name"> <input type="text" name="newsletter"
								class="" required="" placeholder="Email"> <input type="submit"
								class="btn btn-green pull-right" value="Subscribe">
						</form>
					</div>
				</div>
			</div>
		</section>
		<!-- .Signup Newsletter -->
		<!-- .Footer -->
		<footer>
			<div class="container">
				<div class="row text-center">
					<div class="col-sm-12">
						<ul class="list-inline">
							<li><a href="<?=SITEURL;?>">Home</a></li>
							<!-- <li class="hidden"><a href="#">About Us</a></li> -->
							<li><a id="scroll_up2" href="#how-it-works">How it works</a></li>
							<!-- <li class="hidden"><a href="#">FAQ</a></li> -->
							<li><a href="<?=SITEURL . "page/contact_us";?>">Contact Us</a></li>
						</ul>
						<ul class="list-inline social">
							<li><a href="https://www.facebook.com/StageGator/"><i
									class="fa fa-facebook"></i></a></li>
							<li><a href="https://twitter.com/stage_gator"><i
									class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						</ul>
						<hr>
						<a href="#"><img src="<?=$newPath?>images/app-img.jpg" /> </a><a
							href="https://play.google.com/store/apps/details?id=com.stagegator&hl=en" target = "_blank"><img src="<?=$newPath?>images/app-img2.jpg" /> </a>
					</div>
				</div>
			</div>
			<!-- .bfooter -->
			<div class="bfooter text-center">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<p>
								© <?=date('Y')?> <a href="javascript:void(0)">StageGator.</a>All rights
								reserved. | <a href="<?=SITEURL?>page/terms_and_conditions">Terms
									and Conditions</a> | <a href="<?=SITEURL?>page/privacy_policy">Privacy
									Policy</a>
							</p>
						</div>
					</div>
				</div>
			</div>
			<!-- /.bfooter -->
		</footer>
		<!-- /.Footer -->
	</div>
	<!-- /.Wrap -->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

	<script type="text/javascript"
		src="<?=EXTERNAL_PATH?>js/jquery-1.10.2.min.js"></script>

	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<?=$newPath?>js/bootstrap.min.js"></script>
	<script>
	var EXTERNAL_PATH = '<?=EXTERNAL_PATH?>';
	</script>
	<script src="<?=$newPath?>js/home_page.js"></script>
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCj6VfOUCdV_MoYMvJgj1hBOARlUTEdqPM&libraries=places&callback=initAutocomplete"
		async defer></script>
	<link href="<?=EXTERNAL_PATH?>raty/jquery.raty.css" rel="stylesheet">
	<script type="text/javascript"
		src="<?=EXTERNAL_PATH?>raty/jquery.raty.js"></script>
	<script>
  $(document).ready(function() {
		
		$('.raty').raty({
			halfShow : true,
			readOnly : true,
		  score: function() {
		    return $(this).attr('data-score');
		  }
	});
  });
	</script>
	<script>
$('#scroll_up,#scroll_up2').on('click', function(e){
    e.preventDefault();
    var target= $('#scroll_down');
    $('html, body').stop().animate({
       scrollTop: target.offset().top - parseInt($(".main-header").height())
    }, 1000);
    
});
</script>
<script type='text/javascript'>

$('document').ready(function(){
	
	window.__lo_site_id = 73311;

	(function() {
    	var wa = document.createElement('script');
    	 wa.type = 'text/javascript';
    	 wa.async = true;
    	wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
    
    	var s =	document.getElementsByTagName('script')[0]; 
    	s.parentNode.insertBefore(wa, s);
	 })();
		
});
 

</script>
</body>
</html>
