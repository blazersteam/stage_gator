<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>	
						<small data-original-title="Click on Show Title to View Details."
				data-placement="top" class="tooltips"><img
				src="<?=EXTERNAL_PATH?>images/comment.png" class="comment_box" /></small>
		</div>

		<div id="no-more-tables">
			<table
				class="table event-managetable table-bordered table-striped table-hover dataTable cf">
				<thead class="cf">

					<th style="width: 14%">Show Title</th>
					<th style="width: 5%" class="">Trading Request</th>
					<th style="width: 20%" class="eventListDateTime">Dates</th>
					<th style="width: 7%" class="">Time</th>
					<th style="width: 10%" class="">End Time</th>
					<th style="width: 5%" class="">Have Host?</th>
					<th style="width: 5%" class="">Time Per Performance</th>
					<th style="width: 5%; text-align: left;" class="">Link</th>
					<th style="width: 30%; text-align: left;">Actions</th>
					</tr>
				</thead>
				<tbody role="alert" aria-live="polite" aria-relevant="all">
       <?php
    foreach ($list as $r) {
        if (event_end_time_crossed($r->end_date, $r->endTime)) {
            ?>
				<tr>

						<td data-title="Show Title" class=""><a
							href="<?= $this->utility->generateOverviewUrl($r->idEvent);?>"><?php echo $this->utility->decodeText(ucwords($r->title))."(".$this->utility->decodeText($r->type).")"; ?></a>
						</td>
						<td data-title="Trading Request" class="">
                       		<?php  if ($r->allowTrading==1){ ?>
                       			<label class="label label-success">Allowed</label>
                       		<?php } else { ?>
                       			<label class="label label-danger">Not Allowed</label>
                       		<?php } ?>
               		    </td>
						<td data-title="Dates" class="eventListDateTime"><?php  echo dateDisplay($r->date . " " . $r->startTime). ' - '. dateDisplay($r->endDate . " " . $r->endTime); ?></td>
						<td data-title="Time" class="eventListDateTime"> <?php echo dateDisplay($r->date . " " . $r->startTime, "h:i A"); ?></td>
						<td data-title="End Time" class=""><?php echo dateDisplay($r->date . " " . $r->endTime, "h:i A");; ?></td>
						<td data-title="Have Host?" class=""><?php echo ($r->haveHost==0)?"No":"Yes"; ?></td>
						<td data-title="Time Per Performance" class=""><?php echo $this->utility->decodeText(ucwords($r->timePerPerformance)); ?></td>
						<td data-title="Link" class=""><img
							src="<?=EXTERNAL_PATH."images/eventLink.png"?>"
							class="tooltips copyEventURL"
							data-urlid="<?php echo $this->utility->encode($r->idEvent); ?>"
							data-placement="top" data-original-title="Copy Link to Clipboard"
							style="width: 20px;" data-clipboard-text="<?php echo $this->utility->generateOverviewUrl($r->idEvent); ?>"/>
						</td>
						<td data-title="Actions" class="center sorting_1"
							style="text-align: left;">
							<div>
								<a
									href="<?=SITEURL.$controllerName.'/schedule/'.$this->utility->encode($r->idEvent)?>"
									class="btn btn-sm btn-success">Manage Schedule</a> 
							<?php
            /* -- Used for show ranking-start- */
            unset($eventScheduleArray);
            $eventScheduleArray = array();
            // find the event schedule id from event id
            $eventScheduleArray = $this->main_model->get_pending_ranking_event($this->pUserId, $r->idEvent);
            
            // if event schedule id and panding rankig event's schedule id matched then ranking panding button
            // display
            // for do all ranking functionality
            
            if (count(array_intersect($eventScheduleArray, $pending_ranking)) > 0) {
                ?>
               					<a
									href="<?=SITEURL.$this->myvalues->pasteventsDetails['controller'].'/schedule/'.$this->utility->encode($r->idEvent); ?>">
									<span class="badge bg-info">Ranking Pending</span>
								</a>
               					<?php
            }
            /* -- Used for show ranking-End- */
            ?>		
									
                          </div>
						</td>
					</tr>
				<?php
        }
    }
    ?>
    
         </tbody>
			</table>

		</div>
	</div>
</div>

<ul class="pagination">
	<?php echo @$paging; ?>
</ul>
<link href="<?=EXTERNAL_PATH?>css/chosen.css" rel="stylesheet"
	type="text/css">
<script src="<?=EXTERNAL_PATH?>js/jquery.zeroclipboard.js"></script>

<script>
$('.tooltips').tooltip();
</script>
<script>
    var clipboard = new Clipboard('.copyEventURL');

    clipboard.on('success', function(e) {
        console.log(e);
        alert("Show URL Copied to Clipboard");
    });

    clipboard.on('error', function(e) {
        console.log(e);
    });
</script>
