<script>		
$(document).ready(function() {    
    $('.raty').raty({
  	  halfShow : true,
        score: function() {
          return $(this).attr('data-score');
        }        
  });
});
</script>

<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>											
					</div>
		
		<div id="no-more-tables">
			<table
				class="table table-bordered table-striped table-hover dataTable cf"
				id="MyShow">
				<thead class="cf">
					<tr>
						<th class="">Sr #</th>
						<th>Event Title</th>
						<th>Trading Request</th>
						<th>Event Date</th>
						<th class="">Start Time</th>
						<th class="">End Time</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
	<?php
	$i=1;
foreach ($list as $k => $l) {
   
    //if ((! event_end_time_crossed($l->endDate, $l->endTime)) || in_array($l->idEvent, $pending_rankings) && ($l->status == 1)) {
    if ((! event_end_time_crossed($l->end_date, $l->endTime)) && (in_array($l->scheduleId, $pending_rankings) || in_array($l->scheduleId, $host_give_ranking) && ($l->status == 1))) 
    {
        
        ?>
			<tr>
						<td data-title="Sr #" class=""><?php echo $i; ?></td>
						<td data-title="Event Title"><a
							href="<?php echo $this->utility->generateOverviewUrl($l->idEvent,$l->scheduleId); ?>"><?php echo $this->utility->decodeText($l->title); ?></a>
					
				</td>
						<td data-title="Trading Request" class="">
               		<?php if ($l->allowTrading==1){ ?>
               			<label class="label label-success">Allowed</label>
               		<?php } else { ?>
               			<label class="label label-danger">Not Allowed</label>
               		<?php } ?>
               		</td>
						<td data-title="Event Date"><?php echo dateDisplay($l->schedule_date.''.$l->startTime); ?></td>
						<td data-title="Start Time" class=""><?php echo dateDisplay($l->schedule_date.''.$l->startTime,'h:i A'); ?></td>
						<td data-title="End Time" class=""><?php echo dateDisplay($l->end_date.''.$l->endTime,'h:i A'); ?></td>
						<td data-title="Actions">
					<?php if ((!event_end_time_crossed($l->end_date, $l->endTime)) ){  ?>
					<div class="">
								<a
									href="<?php echo SITEURL.$controllerName."/cancelApplication/".$this->utility->encode($l->scheduleId)."/".$this->utility->encode($l->idEvent); ?>"
									class="btn btn-sm btn-warning">Cancel</a>
								
						  <?php if ($l->isHost==1){ ?>
							<a
									href="<?php echo SITEURL.$this->myvalues->rankDetails['controller'].'/performerList/'.$this->utility->encode($l->scheduleId); ?>"
									class="btn btn-sm btn-primary">Host</a>
						<?php } ?>	
							
							
							<?php
            if ($l->isHost == 1 && $l->doList == 1) {
                $updateListRequired = $this->this_model->host_check_all_performers_chosed_time_slot_for_event($l->idEvent, $this->pUserId);
                ?>	
							<a
									href="<?php echo SITEURL.$this->myvalues->timeTableDetails['controller']."?eventId=".$this->utility->encode($l->scheduleId);?>"
									class="btn btn-sm btn-success">See the List</a>
						<?php } else { ?>
						<a
									href="<?php echo SITEURL.$this->myvalues->timeTableDetails['controller']."?eventId=".$this->utility->encode($l->scheduleId);?>"
									class="btn btn-sm btn-primary">See the List</a>
						<?php } ?>
							
						
						<?php
            if ($l->updateSlotNoticeToPerformer == 1) {
                ?>
								<span class="badge bg-success">Updates</span>
								<?php
            }
            
            if ($l->isHost == 1 && $l->doList == 1 && $updateListRequired) {
                ?>
            									<span class="badge bg-warning">Update Required</span>
            									<?php
            }
            
            if ($l->table_id == "") {
                
                ?>
								<span class="badge bg-success">Get on the List</span>
								<?php
            }
            ?>
					</div> <a
							href="http://twitter.com/home?status=Come check me out at '<?=$this->utility->decodeText($l->title)?>' on '<?=date('jS M, Y h:i A',strtotime($l->date." ".$l->startTime))?>'<?=$this->utility->generateOverviewUrl($l->idEvent,$l->scheduleId)?>"
							target="_blank" class="social_login"><img
								src="<?php echo EXTERNAL_PATH ?>images/tweet.png" /></a>
						 <?php $this->utility->fbShareButton(EXTERNAL_PATH.'/images/title.png','Come check me out at '.urlencode($this->utility->decodeText($l->title)).' on '.dateDisplay($l->schedule_date." ".$l->startTime,'jS M, Y h:i A'),urlencode($l->descr),$this->utility->generateOverviewUrl($l->idEvent,$l->scheduleId))?>
			<?php }else { 
            $performer_attandance = $this->this_model->check_attandance($l->scheduleId, $this->pUserId);
            
            if ($performer_attandance == 1) {
                ?>
					   <a href="javascript:void(0);" eventschedule_id="<?php echo $this->utility->encode($l->scheduleId); ?>"
							class="btn btn-info btn-sm rank_venue_intable">Rank Venue</a>
				 <?php } ?>
				 
				 <?php if ($l->isHost==1 && in_array($l->scheduleId, $host_give_ranking) ){ ?>
							<a
									href="<?php echo SITEURL.$this->myvalues->rankDetails['controller'].'/performerList/'.$this->utility->encode($l->scheduleId); ?>"
									class="btn btn-sm btn-primary">Host</a>
							<a
									href="<?php echo SITEURL.$this->myvalues->rankDetails['controller'].'/performerList/'.$this->utility->encode($l->scheduleId); ?>"
									class=""><span class="badge bg-success">Ranking Panding</span></a>		
						<?php } ?>
					
<?php } ?>
				</td>
					</tr>
			<?php $i=$i+1;
    }
  
}
?>
</tbody>
			</table>
		</div>
	</div>
</div>


<div class="modal fade bs-example-modal-sm " tabindex="-1"
			role="dialog" aria-labelledby="mySmallModalLabel" id="displayRatingModel">
			 <?php
                echo form_open(SITEURL . $controllerName . "/rankToVenue");
                ?>
			<div class="modal-dialog modal-sm" role="document" >
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">Rating</h4>
					</div>
					<div class="modal-body">
						<table class="table">
							<tr>
								<td class="col-xs-2 text-right"><strong>Venue : </strong></td>
								<td><span id="venueName"></span></td>
							</tr>
							<tr>
								<td class="col-xs-2 text-right"><strong>Rating: </strong></td>
								<td>
									<div class="raty" readonly data-score="0"
										style="display: inline;"></div>
								</td>
							</tr>
						</table>
					</div>
					<input type="hidden" name="venue_id" value=""  id="venue_id" /> 
		            <input type="hidden" name="eventSchedule_id" value="" id="eventSchedule_id" /> 
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary">Submit</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
			<?php echo form_close();?> 
</div>

<script>
$('.rank_venue_intable').click(function (e){
	e.preventDefault();

	//var hdnServiceOrderId= $('input[name="hdnServiceOrderId"]').val();
	var thisObj = $(this);
	var eventschedule_id = thisObj.attr('eventschedule_id');
	var url = '<?php echo SITEURL.$controllerName."/displayVenueDataForRating"; ?>';
	
	 $.ajax({
	       type:"POST",
	       url: url , 
	       data:{"eventScheduleId":eventschedule_id,'_csrf' : $('input[name="_csrf"]').val()},
	       success:function(response){
	    		var result=$.parseJSON(response);
	    	    if(result.status=='success')
	    	    {		    	    
	    	    	$("#venueName").text(result.venueName);
	    	    	$("#venue_id").val(result.venueUserid);
	    	    	$("#eventSchedule_id").val(result.eventScheduleId);
		            $("#displayRatingModel").modal('show');
		            
	    	    }else{
		    	    alert('No Date Found');
	    	    }
	       },
	       statusCode: {
               401:function() { window.location.reload(); }
             }
	     });   
});
</script>
