<?php $User = $this->session->userdata("UserData"); ?>

<div class="content" style="height: 100%;">
	<div class="alert alert-success text-center myMessage"
		style="display: none"></div>
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>	
				
		</div>
		<div id="no-more-tables">
			<table
				class="table table-bordered table-hover table-striped dataTable cf">
				<thead class="cf">
					<tr>
						<th class="">Sr #</th>
						<th>Following</th>
						<th>Location</th>
						<th>User</th>
					</tr>
				</thead>
				<tbody> 
	<?php
$i = 1;

foreach ($list as $data) {
    ?>
	<tr>

						<td data-title="Sr #" class=""><?php echo $i++; ?></td>
						<td data-title="Following"><a href="<?=SITEURL.'view/'.$data->url?>"
							target="_blank"><?=$data->chrName?></a></td>
						<td data-title="Location"><?=$data->mem_city.", ".$data->mem_state;?></td>
						<?php if($data->idGrp== VENUE_GROUP){
						          $userType = 'Venue';
						      }else if($data->idGrp== PERFOMER_GROUP){
						          $userType = 'Performer';
						      }else if($data->idGrp==FAN_GROUP){
						          $userType = 'Fan';
						      }
						    ?>
						<td data-title="User"><?php echo $userType;  ?></td>
					</tr>
	<?php } ?>
	
</tbody>
			</table>
		</div>
	</div>
</div>
