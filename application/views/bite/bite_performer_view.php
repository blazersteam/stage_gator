<style>
.hide{display:none;}
</style>
<div class="content" style="height: 100%;">

	<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>					
					</div>
		<div
			class="tipSuccessMessage alert alert-success alert-dismissible text-center"
			style="display: none;"><a href="javascript:void(0)" class="close alert-hide" data-dismiss="alert" aria-label="close">&times;</a><span class="retrunmessage"></span></div>

		<div class="alert alert-success text-center myMessage"
			style="display: none"></div>
	<?php
$URL = SITEURL . $controllerName;
echo $this->session->flashdata('myMessage') . form_open($URL . "/bitePerformer/", 'id="frm" class = "form-inline marginBtm10"');
?>
  <div class="form-group" style="width: 280px; text-align: left;">
			<select name="cmbStyle[]" class="form-control chosen-select" multiple
				data-placeholder="Search Style">
				<option></option>
    <?php
    
    foreach ($styles as $s) {
        if (in_array($s->idStyle, set_value('cmbStyle'))) {
            $selected = 'selected';
        }
        else {
            $selected = '';
        }
        ?>
  		<option value="<?php echo $s->idStyle; ?>" <?=@$selected?>><?php echo $s->nameStyle; ?></option>
    <?php } ?>
    </select>
		</div>
		<div class="form-group">
			<input type="text" name="txtName" class="form-control"
				placeholder="Search Name" value="<?=@set_value('txtName')?>" />
		</div>

		<div class="form-group">
			<input type="text" name="txtCity" class="form-control"
				placeholder="Search City or State"
				value="<?=@set_value('txtCity')?>" />
		</div>

		<button type="submit" class="btn btn-success">Search</button>
<?php echo form_close(); ?>


<?php
if (isset($result)) {
    if (count($result) > 0) {
        ?>
		<div id="no-more-tables">
			<table
				class="table table-bordered table-hover table-striped display dataTable cf">
				<thead class="cf">
					<tr role="row">
						
						<th>Name</th>
						<th style="width: 20%">Styles</th>
						<th style="width: 10%">Image</th>
						<th style="width: 15%">City</th>
						<?php if($this->pidGroup != "4") { ?>
						<th style="width: 10%">Rating</th>
						<?php } ?>
						<!--<th style="width: 10%">Rating OLD</th>-->
						<th style="width: 10%">Action</th>
					</tr>
				</thead>

				<tbody role="alert" aria-live="polite" aria-relevant="all">
       <?php
        $i = 1;
        $class = "";
        foreach ($result as $r) {
            if ($i % 2 == 0) {
                $class = 'class="gradeA even"';
            }
            else {
                $class = 'class="gradeA odd"';
            }
            
            $styles = $this->performer_model->getUserStyles($r->user_id);
            ?>
				<tr <?php echo $class; ?>>
						
               		<?php
            /*
             * ?>
             * <td class=""><a href="<?php echo base_url()."performer/overview"; ?>"><?php echo
             * ucwords($r->chrName); ?></a></td>
             */
            ?>
               		<td data-title="Name" class="">
							<!-- <a
					href="<?php echo SITEURL.$this->myvalues->performerDetails['controller']."/overview/".$this->utility->encode($r->user_id); ?>"
					title="Overview"><?php echo ucwords($r->chrName); ?></a>  --> <a
							href="<?php echo SITEURL."view/".$r->url; ?>"
							data-user-name="<?php echo ucwords($r->chrName); ?>"
							title="Overview"><?php echo ucwords($r->chrName); ?></a>


						</td>
						<td data-title="Style"> 
               			<?php
            $n = 0;
            foreach ($styles as $row) {
                if ($n) {
                    echo ", ";
                }
                echo $row->nameStyle;
                $n ++;
            }
            ?>
               		</td>
						<td data-title="Image" align="center" class="">
               			<?php if (!empty($r->image) && file_exists("./external/images/profiles/".$r->image)) { ?>
               			<img
							src="<?php echo EXTERNAL_PATH."images/profiles/".$r->image; ?>"
							style="width: 50px; height: 50px;" />
               			<?php } else { ?>
               					<img
							src="<?php echo EXTERNAL_PATH."images/profiles/eleven.png"; ?>"
							style="width: 50px; height: 50px;" />
               			<?php } ?>
               		</td>
						<td data-title="City"><?php echo ucwords($r->mem_city); echo ", "; echo $r->mem_state; ?></td>
                        <?php if($this->pidGroup != "4") { ?>
						<td data-title="Rating" class=""><?php  echo number_format($r->rate,2); ?></td>
               		<?php
                        }
                    ?>
                    
                    <td data-title="Action">
                    <?php
            $User = $this->session->userdata("UserData");
            $stripAccountExit = "";
            if ($this->pidGroup == FAN_GROUP && (! empty($r->stripe_account_id) && $r->stripe_status === 'Verified')) {
                $stripAccountExit = 1;
            }
            if (in_array($r->plan_id, $GLOBALS ["allowedBitePlans"]) || $this->pidGroup == PERFOMER_GROUP) {
                if ($r->followerId != $User->user_id) {
                    // if(fan_model::checkIfFollowing($User->user_id, $r->user_id)){                     ?>
               			<a href="javascript:void(0)"
							data-following-id="<?php echo $this->utility->encode($r->user_id);?>" data-userid="<?php echo $r->user_id;?>" data-account-exit="<?php echo $stripAccountExit; ?>" class="btn btn-md btn-success toggleBiteUnbite">Bite</a>
               			<?php }else{ ?>				
				
						
						<a href="javascript:void(0)"
							data-following-id="<?php echo $this->utility->encode($r->user_id);?>" data-userid="<?php echo $r->user_id;?>" data-account-exit="<?php echo $stripAccountExit; ?>" class="btn btn-md btn-warning toggleBiteUnbite">Unbite</a>
               			<?php
               			}
                }
            if ($this->pidGroup == FAN_GROUP && (! empty($r->stripe_account_id) && $r->stripe_status === 'Verified')) {
                ?>
                           			
                           						<div style="margin-top: 10px;">
            
            								<a class="btn btn-md btn-success tipper_popup"
            									data-userid=<?php echo $this->utility->encode($r->user_id);?>
            									href="javascript:void(0);" title="Tip here" id="tipper_popup_<?php echo $r->user_id; ?>">
            									<!-- <i class="fa fa-usd"></i> -->
            									Tip
            									</a>
            							</div>
                           			<?php
            
                                }
           
            if ($User->idGrp == 2) {
                ?>
               			<a
							href="<?php echo SITEURL.$this->myvalues->invitationDetails['controller']."/inviteForEvent/".$this->utility->encode($r->user_id); ?>"
							class="btn btn-sm btn-success">Invite</a>
               		<?php
            }
            // }             ?>
             	
			</td>
			
					
					</tr>
				<?php
            $i ++;
        }
        ?>
    
         </tbody>

			</table>
		</div>
	
<?php  } else { ?>
	<div class="alert alert-danger">No Record Found</div>	
<?php
    }
}
?>
</div>
</div>
<script type="text/javascript" src="<?=EXTERNAL_PATH?>js/chosen.jquery.js"></script>
<link href="<?=EXTERNAL_PATH?>css/chosen.css" rel="stylesheet" type="text/css">
<link href="<?=EXTERNAL_PATH?>css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<script src="<?=EXTERNAL_PATH?>js/jquery.dataTables.min.js"> </script>
<script type="text/javascript" src="<?=EXTERNAL_PATH?>js/modules/search_performer.js"></script>





<!-- tip dialog model div start -->
<div class="modal fade" id="tipper" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document" id="TipPopup"></div>
</div>

<!-- tip dialog model div start -->

<script>

$('.tipper_popup').on('click',function(){
    
	 //disable the button
   $('.tipper_popup').attr('disabled',true);
   var tippingcontroller = '<?php echo $this->myvalues->tippingDetails['controller'];?>';
   var performerUserid = $(this).attr('data-userid');
   var performerName = $(this).parent().parent().siblings("td:nth-child(2)").text();
   
   $.ajax({
       type:"POST",
       url: SITEURL + tippingcontroller + "/checkStripeIdUser",     	        
       data:{'_csrf' : $('input[name="_csrf"]').val(),'performerUserid' : performerUserid},     	       
       beforeSend: function() { 	 	       
    	  blockUiDisplay();	    	 
    	  $("#TipPopup").html("");
       },
       success:function(data){
    	   $.unblockUI();
	    	   	    	  
	      //enable the button
	       $(".tipper_popup").removeAttr("disabled");
    	   $("#TipPopup").html(data);
    	   $('.mainTital').html('<?php echo 'Tip '; ?> '+ performerName);
	       $("#tipper").modal('show');
	       $("#tipAmount").focus();   
	       //alert('#tipAmount').val('asdas'));
       },
       statusCode: {
           401:function() { window.location.reload(); }
       }
     });
}); 

$(window).load( function() {
	$(document).ready( function(){
    	setTimeout(function () {
    		$(".tipper_popup").removeClass("hide");   
        }, 100);
	});
    console.log('asdasd');
});

</script>

<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript" src="<?=EXTERNAL_PATH?>js/jquery.blockUI.js"></script>

