<?php $User = $this->session->userdata("UserData"); ?>

<div class="content" style="height: 100%;">
	<div class="alert alert-success text-center myMessage"
		style="display: none"></div>
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>	
				
		</div>
		<div id="no-more-tables">
			<table
				class="table table-bordered table-hover table-striped dataTable cf">
				<thead class="cf">
					<tr>
						<th class="">Sr #</th>
						<th>Following</th>
						<th>Location</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody> 
	<?php

$i = 1;

foreach ($list as $data) {
    ?>
	<tr>

						<td data-title="Sr #" class=""><?php echo $i++; ?></td>
						<td data-title="Following"><a href="<?=SITEURL.'view/'.$data->url?>"
							target="_blank"><?=$data->chrName?></a></td>
						<td data-title="Location"><?=$data->mem_city.", ".$data->mem_state;?></td>
						<td data-title="Action"><a
							class="btn btn-sm toggleBiteUnbite btn-warning"
							data-following-id="<?=$this->utility->encode($data->user_id)?>"
							href="javascript:void(0)">Unbite</a></td>
					</tr>
	<?php } ?>
	
</tbody>
			</table>
		</div>
	</div>
</div>
