<div class="content" style="height: 100%;">

	<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>					
					</div>
<div class="col-xs-12 help_line">
	<div class="help_label">
	<img src="<?=EXTERNAL_PATH?>images/overview_img.png"> <b>Overview</b>
	</div>
	<div class="">
	Profile Overview Page
	</div>
</div>

<div class="col-xs-12 help_line">
	<div class="help_label">
	<img src="<?=EXTERNAL_PATH?>images/personal_info_img.png"> <b>Personal Info</b>
	</div>
	<div class="">
	Update your personal information.
	</div>
</div>

<div class="col-xs-12 help_line">
	<div class="help_label">
	<img src="<?=EXTERNAL_PATH?>images/change_img.png"> <b>Change Image</b>
	</div>
	<div class="">
	Change your profile picture.
	</div>
</div>

<div class="col-xs-12 help_line">
	<div class="help_label">
	<img src="<?=EXTERNAL_PATH?>images/lock.png"> <b>Change Credentials</b>
	</div>
	<div class="">
	Change your log-in credentials
	</div>
</div>

<div class="col-xs-12 help_line">
	<div class="help_label">
	<i class="fa fa-envelope icon-help"></i> <b>My Inbox</b>
	</div>
	<div class="">
	The messages you received by venues and fellow performers.
	</div>
</div>

<div class="col-xs-12 help_line">
	<div class="help_label">
	<i class="fa fa-tasks icon-help"></i> <b>My Events</b>
	</div>
	<div class="">
	List of events you are going to perform on.
	</div>
</div>

<div class="col-xs-12 help_line">
	<div class="help_label">
	<i class="fa fa-star-half-empty icon-help"></i> <b>My Rankings</b>
	</div>
	<div class="">
	List of what ranking you get on your past shows.
	</div>
</div>

<div class="col-xs-12 help_line">
	<div class="help_label">
	<i class="fa fa-wrench icon-help"></i> <b>Settings</b>
	</div>
	<div class="">
	Your privacy settings.
	</div>
</div>

<div class="col-xs-12 help_line">
	<div class="help_label">
	<i class="fa fa-refresh icon-help"></i> <b>Trading Requests</b>
	</div>
	<div class="">
	Requests received by other performers to exchange performance time in a show.
	</div>
</div>
<div class="col-xs-12 help_line">
	<div class="help_label">
	<b>How to trade line-up positions on an event?</b>
	</div>
	<div>
		<ol type="1">
		  <li>Click the schedule button on the action tab of the event in question.</li>
		  <li>Select the performer on the event you'd like to trade positions with, then hit the Trade Slot button.</li>
		  <li>Hit the apply button next to the event you'd like to perform on.</li>
		  <li>Wait for the fellow performer to approve the trade.</li>
		</ol>
	</div>
</div>
<div class="col-xs-12 help_line">
	<div class="help_label">
	<img src="<?=EXTERNAL_PATH?>images/cane2.png" style="width: 20px;"> <b>Event Pending For Venue Approval </b>
	</div>
	<div class="">
	Check your event requests you made to certain venues to offer your service as a performer
	</div>
</div>

<div class="col-xs-12 help_line">
	<div class="help_label">
	<img src="<?=EXTERNAL_PATH?>images/hat_grey.png"> <b>Event Pending For Performer Approval: </b>
	</div>
	<div class="">
	Check the invitations you received to by certain shows to appear as a performer
	</div>
</div>

<div class="col-xs-12 help_line">
	<div class="help_label">
	<img src="<?=EXTERNAL_PATH?>images/microphone2_grey.png"> <b>Host Invites</b>
	</div>
	<div class="">
	Check if you've been invited to host a show that you're a performer on.
	</div>
</div>

<div class="col-xs-12 help_line">
	<div class="help_label">
	<img src="<?=EXTERNAL_PATH?>images/search_img.png"> <b>Search for Events</b>
	</div>
	<div class="">
	Look-up potential events looking for performers in your area.
	</div>
</div>

<div class="col-xs-12 help_line">
	<div class="help_label">
	<b>How to find events to perform on?</b>
	</div>
	<div>
		<ol type="1">
		  <li>Click the magnifying glass icon at the top of the page. This will direct the user to StageGator's Search Event page.</li>
		  <li>Search based on show style and/or based on how close the event is to you.</li>
		  <li>Hit the apply button next to the event you'd like to perform on.</li>
		  <li>Wait for the venue to approve your application.</li>
		</ol>
	</div>
</div>
<BR>
<a href="<?=EXTERNAL_PATH?>StageGatorUserManual.pdf" target="_blank">Click here</a> to download user manual.
<link href="<?=EXTERNAL_PATH?>css/help.css" rel="stylesheet">
</div>
</div>
