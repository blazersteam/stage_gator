<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
		<div class="row">
		<div id="Tick"></div>
		<div class="page_header">Contact US</div>

		<!-- YOUR CONTENT COMES HERE -->

		<?php echo form_open(SITEURL . "page/contact_us", ["class" => 'form-horizontal']); ?>
			<div class="col-xs-10">
				<div class="form-group">
					<label class="col-sm-4 control-label">Your Name:</label>
					<div class="col-sm-8">
						<input name="name" class="form-control" required placeholder="John Doe"
							type="text">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Email Address:</label>
					<div class="col-sm-8">
						<input name="email" class="form-control" required
							placeholder="example@domain.com" type="email">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Subject:</label>
					<div class="col-sm-8">
						<input name="subject" class="form-control" required
							placeholder="Contact Reason" type="text">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Message:</label>
					<div class="col-sm-8">
						<textarea class="form-control" name="message" required
							placeholder="Your Message!"></textarea>
					</div>
				</div>
				<div class="form-group hidden">
					<label class="col-sm-4 control-label">Captcha: </label>
					<div class="col-sm-8">
						<?php echo $captcha; ?> 
							<input name="captcha"
							class="form-control" placeholder="Enter code you see above"
							value="" type="text">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-10 text-center">
						<input class="btn btn-success" name="submitfrm"
							id="btnEventSubmit" value="Send Message" type="submit">
					</div>
				</div>
			</div>
		</form>
		<!-- YOUR CONTENT ENDS HERE -->
	</div>
</div>