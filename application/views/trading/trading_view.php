<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
    <div class="row">
		<div id="Tick"></div>
		<div class="page_header">Requests Made To You</div>
		<div id="no-more-tables">
		<table
			class="table table-bordered table-striped table-hover dataTable cf"
			id="searchEvent">
			<thead class="cf">
				<tr>
					<th>Sr #</th>
					<th>Request From</th>
					<th>Event Title</th>
					<th>Event Date</th>
					<th>Current Slot</th>
					<th>Trade With</th>
					<th>Status</th>
				</tr>
			</thead>
			
			
			<tbody>
            <?php 
            
            foreach ($toMe as $k => $v): ?>
                <tr>
					<td data-title="Sr #"><?php echo $k + 1; ?></td>
					<td data-title="Request From"><?php echo $v->chrName; ?></td>
					<td data-title="Event Title"><?php echo $v->title; ?></td>
					<td data-title="Event Date"><?php echo dateDisplay($v->scheduleStartDate. " " . $v->scheduleStartTime, "m/d/Y");?></td>
					<td data-title="Current Slot"><?php echo dateDisplay($v->showDate1. " " . $v->start_time1, "h:i A"); ?> - <?php echo dateDisplay($v->showDate1. " " . $v->end_time1, "h:i A"); ?></td>
					<td data-title="Trade With"><?php echo dateDisplay($v->showDate2. " " . $v->start_time2, "h:i A"); ?> - <?php echo dateDisplay($v->showDate2. " " . $v->end_time2, "h:i A"); ?></td>
					<td data-title="Status"> 
    					<?php
                if ($v->status == 0) {
                    echo "Rejected";
                }
                else if ($v->status == 1) {
                    echo "Accepted";
                }
                else if ($v->status == 3) {
                    echo "Cancelled";
                }
                else {
                    
                    if(!event_end_time_crossed($v->scheduleStartDate, $v->scheduleStartTime))
                    {
                    ?>
    							<a
						href="<?php echo SITEURL. $controllerName . "/acceptTrade/".$this->utility->encode($v->trade_id); ?>"
						class="btn btn-sm btn-success" title="Accept"
						onclick="return confirm_action();"><span
							class="glyphicon glyphicon-ok"></span></a> <a
						href="<?php echo SITEURL. $controllerName . "/rejectTrade/".$this->utility->encode($v->trade_id); ?>"
						class="btn btn-sm btn-danger" title="Reject"
						onclick="return confirm_action();"><span
							class="glyphicon glyphicon-remove"></span></a>
    							<?php
                                }
                           }
                     ?>				
    				</td>
				</tr>
            <?php endforeach;?>
			</tbody>
		</table>
		</div>
	</div>

	<div class="row">
		<div id="Tick"></div>
		<div class="page_header">Requests Made By You</div>
		<div id="no-more-tables">
		<table
			class="table table-bordered table-striped table-hover dataTable cf"
			id="searchEvent1">
			<thead class="cf">
				<tr>
					<th>Sr #</th>
					<th>Request To</th>
					<th>Event Title</th>
					<th>Event Date</th>
					<th>Current Slot</th>
					<th>Trade With</th>
					<th>Status</th>
				</tr>			
			</thead>
			
			
			<tbody>
            <?php

            foreach ($fromMe as $k => $v): ?>
                <tr>
					<td data-title="Status"><?php echo $k + 1; ?></td>
					<td data-title="Request To"><?php echo $v->chrName; ?></td>
					<td data-title="Event Title"><?php echo $v->title; ?></td>
					<td data-title="Event Date"><?php echo dateDisplay($v->scheduleStartDate. " " . $v->scheduleStartTime, "m/d/Y");?></td>
					<td data-title="Current Slot"><?php echo dateDisplay($v->showDate2. " " . $v->start_time2, "h:i A"); ?> - <?php echo dateDisplay($v->showDate2. " " . $v->end_time2, "h:i A"); ?></td>
					<td data-title="Trade With"><?php echo dateDisplay($v->showDate1. " " . $v->start_time1, "h:i A"); ?> - <?php echo dateDisplay($v->showDate1. " " . $v->end_time1, "h:i A"); ?></td>
					<td data-title="Status"> 
    					    					
    					<?php
                if ($v->status == 0) {
                    echo "Rejected";
                }
                else if ($v->status == 1) {
                    echo "Accepted";
                }
                else if ($v->status == 3) {
                    echo "Cancelled";
                }
                else {
                    if(!event_end_time_crossed($v->scheduleStartDate, $v->scheduleStartTime))
                    {
                    ?>
    							<a
						href="<?php echo SITEURL. $controllerName . "/cancelTrade/".$this->utility->encode($v->trade_id); ?>"
						class="btn btn-sm btn-danger" title="Cancel"
						onclick="return confirm_action();"><span
							class="glyphicon glyphicon-remove"></span></a>
    							<?php
                    }
                }
                ?>				
    				</td>
				</tr>
            <?php endforeach;?>
			</tbody>
		</table>
		</div>
	</div>
</div>
<script>
function confirm_action(){
	if (confirm("Are you sure you want to complete this action?")){
		return true;
	} else {
		return false;
	}
}
</script>