<?php 

echo "<table style = ''>

       <tr>
            <td colspan='2'>Dear ".$userData->chrName.",</td>
       </tr>
       
       <tr>
           <td colspan='2'>&nbsp;</td>
       </tr>
       
       <tr>
           <td colspan='2'>Congratulations! You've just received a tip through StageGator!</td>
       </tr>
      
       <tr>
           <td colspan='2'>&nbsp;</td>
       </tr>
       
       <tr>
           <td colspan='2'>Here's a summary of your tip:</td>
       </tr>
      
       <tr>
           <td colspan='2'>&nbsp;</td>
       </tr>
      
       <tr>
		 <td width='20%'>Tip Amount :</td>
		 <td>$ ".$amount."</td>
	   </tr>
	   
	   <tr>
		 <td width='20%'>StageGator Charge :</td>
		 <td>$ ".$chargeAmount." <a href='".SITEURL."subscription' >(Want to reduce?)</a></td>
	   </tr>
	   
	   <tr>
		 <td width='20%'>Net Amount :</td>
		 <td>$ ".$netAmount."</td>
	   </tr>
	  
	   <tr>
           <td colspan='2'>Keep up the the great work! The more your perform, the more tips you can receive!</td>
       </tr>
       
       <tr>
           <td colspan='2'>&nbsp;</td>
       </tr>
	  
	   <tr>
           <td colspan='2'>You are not going to be charged for withdrawing funds.</td>
       </tr>
       
       <tr>
           <td colspan='2'>&nbsp;</td>
       </tr>
	  
	   <tr>
		 <td>Thanks,</td>
	   </tr>
	   
	   <tr>
           <td colspan='2'>&nbsp;</td>
       </tr>
       
       <tr>
		 <td>Team StageGator</td>
	   </tr>
</table>";
?>
