<?php

echo "<table style =''>

       <tr>
            <td colspan='2'>Dear $userData->chrName,</td>
       </tr>
       
       <tr>
           <td colspan='2'>&nbsp;</td>
       </tr>
      
       <tr>
          <td colspan='2'>You have given a tip to $performerName.</td>
       </tr>
   	
       <tr>
           <td colspan='2'>&nbsp;</td>
       </tr>
       
       <tr>
		 <td colspan='2'>Below is summary of your transaction :</td>
	  </tr>
       
	  <tr>
           <td colspan='2'>&nbsp;</td>
       </tr>
       
	  <tr>
		 <td width='20%'>Transaction ID :</td>
		 <td>".$transactionId."</td>
	  </tr>
	  <tr>
		 <td width='20%'>Amount :</td>
		 <td>$ ".$amount."</td>
	  </tr>
	  
	  <tr>
           <td colspan='2'>&nbsp;</td>
       </tr>
	  
      <tr>
           <td colspan='2'>We appreciate your support of live performance!</td>
       </tr> 
       
       <tr>
           <td colspan='2'>&nbsp;</td>
       </tr>
	  
      <tr>
           <td colspan='2'>Please continue to enjoy your favorite performers on <a href =".SITEURL.">stagegator.com.</a></td>
       </tr>
       
        <tr>
           <td colspan='2'>&nbsp;</td>
       </tr>
       
	   <tr>
		 <td colspan='2'>Thanks,</td>
	  </tr>
	  
		 <tr>
           <td colspan='2'>&nbsp;</td>
       </tr>
      <tr>
		 <td colspan='2'>Team StageGator</td>
	  </tr>
</table>";
?>
