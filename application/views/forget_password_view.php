  <link href="<?=EXTERNAL_PATH?>css/custom_style.css" rel="stylesheet">
    <div id="user-type">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 text-center">
            <a href="<?=SITEURL;?>">
              <img src="<?=EXTERNAL_PATH?>images/logo_local.png" alt="logo"/>
            </a>
          </div>
        </div>
      </div>
      <div class="clearfix">
      </div>
      <!-- forms part-->
      

    <div id="respective">
      <div class="container">
          
        <div class="row frmarea">
         <div class="col-sm-6 col-lg-4 col-sm-offset-3 col-lg-offset-4">
         <?php echo $this->session->flashdata('myMessage'); ?>
         <?php echo form_open($forgotPasswordURL, 'id="myform" class="login-form" onSubmit="return submit_login();"'); ?>

              <div class="input-group input-group-lg form-group">
                  

                <span class="input-group-addon">
                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                 <!--  <img src="<?=EXTERNAL_PATH?>images/email.gif"align=""> -->
                </span> 
                <input type="text" 
                       name="txtEmail"
                       class="form-control"
                       id = "email" 
                       placeholder="Email"
                       aria-describedby="sizing-addon2"/>
                       <?php echo form_error('txtEmail');?>
              </div>
             <div class="form-group input-group-lg">
                <input type="submit" 
                       class="form-control btn btn-sin" 
                       id="submitbtn"
                       value="Forgot Password">
              </div>
            <?php form_close(); ?>
           
          </div>
        </div>
      </div>
    </div>
    
<script src="<?=EXTERNAL_PATH?>js/modules/login_form.js">
</script>

