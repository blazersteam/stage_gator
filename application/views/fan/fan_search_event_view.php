
<link href="<?=EXTERNAL_PATH?>css/chosen.css" rel="stylesheet"
	type="text/css">
<link href="<?=EXTERNAL_PATH?>css/custom.css" rel="stylesheet"
	type="text/css">

<script src="<?=EXTERNAL_PATH?>js/chosen.jquery.js"> </script>

<script>		
$(document).ready(function() {
    $('.chosen-select').chosen();
});
</script>

<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>	
										
					</div>
<div class="alert text-center myMessage" style="display:none"></div>
		<div class="alert"
			style="padding: 5px; text-align: center;">					
<?=form_open(SITEURL.$controllerName.'/search','id="frm" class="form-inline" role="form"')?>

<div class="form-group" style="width: auto; text-align: left;">
				<select name="style[]" class="form-control chosen-select" multiple
					data-placeholder="Search Style">
					<option></option>
  <?php

foreach ($styles as $s) {
    $sel = "";
    if (isset($_POST ['style'])) {
        if (in_array($s->idStyle, $_POST ["style"])) {
            $sel = "selected";
        }
    }
    ?>
    <option value="<?php echo $s->idStyle; ?>" <?php echo $sel; ?>><?php echo $s->nameStyle; ?></option>
    <?php } ?>
    </select>
    
		</div>
			<div class="form-group">
				<input type="text" name="searchName" class="form-control"
					placeholder="Search Show Name"
					value="<?=@set_value('searchName')?@set_value('searchName'):''?>" />
			</div>
			<div class="form-group">
				<input type="text" name="searchVenueName" class="form-control"
					placeholder="Search Agency Title"
					value="<?=@set_value('searchVenueName')?@set_value('searchVenueName'):''?>" />
			</div>
			<div class="form-group">
				<input type="text" name="city" class="form-control"
					placeholder="Search City or State"
					value="<?=@set_value('city')?@set_value('city'):''?>" />
			</div>
			<button type="submit" class="btn btn-success  pull-right">Search</button>


<?php echo form_close(); ?>
</div>
<div class="table-responsive search-shows">
<div id="no-more-tables">
		<table class="table table-striped table-bordered dataTable no-footer cf"
			id="searchEvent">
			<thead class="cf">
				<tr role="row">
					<th style="width: 5%" class="">Sr #</th>
					<th style="width: 5%">Action</th>
					<th style="width: 10%">Event Title</th>
					<th style="width: 10%">Agency Title</th>
					<th style="width: 10%">City</th>
					<th style="width: 10%">Date</th>
					<th style="width: 10%">Start Time</th>
					<th style="width: 10%" class="">End Time</th>
					<th style="width: 5%" class="">Time Per Performance</th>
				</tr>
			</thead>

			<tbody role="alert" aria-live="polite" aria-relevant="all">
                
                 <?php
                 $no = 1;
                if (count($eventResult) > 0) {
                    
                    foreach ($eventResult as $k => $v) :
                        $v = $this->utility->decodeText($v);
                    
                    if (event_end_time_crossed($v->start_date,$v->startTime)) {
                        continue;
                    }
                        ?>
          <tr role="row">
					<td data-title="Sr #"><?php echo $no++;?></td>
					<td data-title="Action"><?php if($v->lockStatus==0){ ?>
					<?php if (in_array($v->idVenue, $venueFollowArray)) {?>
    					   <a
						href="javascript:void(0)"
						data-following-id = "<?php echo $this->utility->encode($v->idVenue);?>"
						class="btn btn-sm btn-warning toggleBiteUnbite">Unbite</a>
    					<?php }else{?>
    					   <a
						href="javascript:void(0)"
						data-following-id = "<?php echo $this->utility->encode($v->idVenue);?>"
						class="btn btn-sm btn-success toggleBiteUnbite">Bite</a>
    					<?php }?>
    					
					<?php }else{?>
					   <a href="JavaScript:void(0);"
						class="btn btn-sm btn-success tooltips"
						data-original-title="You can not Apply for Locked Show!"
						data-placement="top">Cut-Off Time Passed</a>					
					<?php }?>
					</td>
					<td data-title="Event Title"><a href = "<?=$this->utility->generateOverviewUrl($v->idEvent,$v->table_id)?>"><?php echo $v->title;?></a></td>
					<td data-title="Agency Title"><a href="<?=SITEURL."view/".$v->url; ?>"><?php echo $v->chrName;?></a></td>
					<td data-title="City"><?php echo $v->city.', '.$v->state;?></td>
					<td data-title="Date"><?php echo dateDisplay($v->start_date.''.$v->startTime);?></td>
					<td data-title="Start Time"><?php echo dateDisplay($v->start_date.''.$v->startTime,'h:i A'); ?></td>
					<td data-title="End Time"><?php echo dateDisplay($v->end_date.''.$v->endTime,'h:i A');?></td>
					<td data-title="Time Per Performance"><?php echo $v->timePerPerformance;?></td>
				</tr>     
               <?php
                    endforeach
                    ;
                }
                else {
                    ?>
            <tr>
					<td colspan="11" align="center"><?php echo $this->lang->line("error_norecords_found"); ?></td>
				</tr>
      <?php
                }
                ?>  
			</tbody>
		</table>
		</div></div>
	</div>
</div>