
<div class="content" style="height: 100%;">
	<div class="row" style="height: 100%; margin-left: 0%; padding: 10px;">

		<div class="page_header">
                            <?=$title?>
                        </div>
 <?php echo form_open($resetPasswordURL.'/'.$datatoken, 'id="pwdform" class="form-horizontal" novalidate="novalidate" role="form"'); ?>
                            <div class="col-xs-10">
			<div class="form-group">
				<label class="col-sm-2 control-label"><?=$this->lang->line('new_password')?><em>*</em></label>
				<div class="col-sm-5">
					<input type="password" name="txtPassword" id="password" maxlength="32"
						class="form-control" placeholder="New Password">
                          <?php echo form_error('txtPassword'); ?>
               </div>
			</div>
		</div>
		<div class="col-xs-10">
			<div class="form-group">
				<label class="col-sm-2 control-label"><?=$this->lang->line('confirm_password')?><em>*</em></label>
				<div class="col-sm-5">
					<input type="password" name="txtConfirm_password" maxlength="32"
						id="confirm_password" class="form-control"
						placeholder="Confirm Password">
                     <?php echo form_error('txtConfirm_password'); ?>
                </div>
			</div>
		</div>
		<div class="col-xs-10">
			<div class="form-group">
				<label class="col-sm-2 control-label"></label>
				<div class="col-sm-5">
					<input type="submit" name="btnContact" id="btnContact"
						value="<?=$this->lang->line('change_password')?>" class="btn btn-success">
                                        <a href = "<?=SITEURL.'/'.$this->myvalues->loginDetails['controller']?>" class = "btn btn-success">Login</a>
				</div>
			</div>   
		</div>
                        
<?php form_close(); ?>
    </div>
</div>

<script src="<?=EXTERNAL_PATH?>js/modules/reset_password.js"></script>
