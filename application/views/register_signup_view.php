<link href="<?=EXTERNAL_PATH?>css/landing.css" rel="stylesheet">
<div class="wrap">
	<div class="brand">
		<a href="<?=SITEURL?>"><img src="<?=EXTERNAL_PATH?>images/landing_logo_new.png"
			title="StageGator" alt="StageGator" style=""></a>
	</div>


	<div class="register-form" style="width: 80%; margin-left: auto; margin-right: auto">
	<?php echo $this->session->flashdata('myMessage'); ?>
		<!-- <h1 style="color: #3FDF3F"><?=$title ?></h1> -->
		<div class="btn-facebook">
		<?php $value = ($userType == 'venue') ? VENUE_GROUP : (($userType == 'performer')? PERFOMER_GROUP: FAN_GROUP); ?>
		<a href="<?=SITEURL.$this->myvalues->loginDetails['controller'].'/facebooklogin/'.$value?>" class="btn btn-primary">Register with Facebook</a>
		</div>
     <?php echo form_open(SITEURL.$controllerName.'/add/'.$userType, 'id="form" class="create-account" onSubmit="return check_form();"'); ?>
     
			<div class="form-group">
			<label><?php echo $userType=="venue"?$this->lang->line('register_venue'):$this->lang->line('register_name'); ?><em>*</em></label>
			<input type="text" class="form-control"  autofocus
				placeholder="<?php echo $userType=="venue"?'Enter Venue/Agency Title':'Enter Full Name'; ?>"
				name="txtName" maxlength="50"
				value="<?=set_value('txtName')?>">
				<?=form_error('txtName')?>
		</div>

<?php if ($userType=='venue'){ ?>
  <div class="form-group">
			<label><?=$this->lang->line('register_contact_person')?><em>*</em></label> <input type="text"
				class="form-control"  placeholder="Contact Person Name" autofocus
				name="txtContact_person" maxlength="50" 
				value="<?=set_value('txtContact_person')?>">
				<?=form_error('txtContact_person')?>
		</div>
  <?php 
        } 
  
  ?>
  
  <div class="form-group">
			<label><?=$this->lang->line('register_account_type')?><em>*</em></label> 
			<select name="cmbType" class="form-control">
                <option value="<?=$value?>"><?=ucfirst($userType)?></option>
            </select>
            <?=form_error('cmbType')?>
		</div>


		<div class="form-group">
			<label><?=$this->lang->line('register_phone')?><em>*</em></label> <input type="tel" class="form-control <?php if($value != 4) { echo 'required'; } ?>"
				 placeholder="Contact Number" name="txtPhone" id="phone" autofocus
				maxlength="15" value="<?=set_value('txtPhone')?>">
				<?=form_error('txtPhone')?>
				<small style="display:block;">
				212-999-0983, 212 999 2344</small>
				
		</div>
		<div class="form-group">
			<label><?=$this->lang->line('register_email_address')?><em>*</em></label> <input type="email"
				class="form-control" id="email" placeholder="Enter email" autofocus
				name="txtEmail" onChange="verifyEmail(this.value)" 
				maxlength="50" value="<?=set_value('txtEmail')?>">
				<?=form_error('txtEmail')?>
			<div id="response" style="display: none;"></div>
		</div>
		<div class="form-group">
			<label><?=$this->lang->line('register_password')?><em>*</em></label> <input type="password" class="form-control"
				id="password" placeholder="Password" autofocus
				name="txtPassword"  maxlength="15">
				<?=form_error('txtPassword')?>
		</div>

		<div class="form-group">
			<label><?=$this->lang->line('register_confirm_password')?><em>*</em></label> <input type="password"
				class="form-control"  id="confirmPassword" autofocus
				placeholder="Confirm Password" name="txtConfirmPassword" 
				maxlength="15">
				<?=form_error('txtConfirmPassword')?>
			<div id="response1" style="display: none;"></div>
		</div>
		
		<div class="form-group">
			<label><?=$this->lang->line('register_verify_that_you_are_a_human')?><em>*</em></label>
			<div
				style="transform: scale(0.82) !important; -webkit-transform: scale(0.77) !important; transform-origin: 9px 50px 0px !important; -webkit-transform-origin: 0 0;"
				class="g-recaptcha" data-sitekey="<?=GOOGLE_CAPTCHA_KEY?>"></div>
			<!-- input type="hidden" class="hiddenRecaptcha" name="hiddenRecaptcha" id="hiddenRecaptcha"> -->
			<?=form_error('g-recaptcha-response')?>
			<span id = "googleCaptcha" style  = "color:red;"></span>
		</div>
		
		  
		
        <input type="submit" class="btn btn-info" name="submitreg"
			id="frmsubmit" value="Submit" /> <a href="<?php echo SITEURL . "login"; ?>"
			class="btn btn-default"><?=$this->lang->line('register_already_have_an_account')?></a>


  <?php form_close(); ?>

	</div>
</div>
<script type="text/javascript" src="<?=EXTERNAL_PATH?>js/jquery.maskedinput.min.js"></script>
<script src="<?=EXTERNAL_PATH?>js/modules/register.js">
</script>
<script src='https://www.google.com/recaptcha/api.js'></script>
