
<link href="<?=EXTERNAL_PATH?>css/custom_style.css" rel="stylesheet">
<div id="user-type">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<a href="<?=SITEURL?>"> <img src="<?=EXTERNAL_PATH?>images/logo_local.png" alt="logo" />
				</a>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>

<div id="respective">
	<div class="container">
		<div class="row frmarea">
			<div class="col-sm-6 col-lg-4 col-sm-offset-3 col-lg-offset-4">
      
    <?php echo $this->session->flashdata('myMessage'); ?>
         <?php echo form_open(SITEURL.'login', 'id="myform" class="login-form" onSubmit="return submit_login();"'); ?>

               <div class="input-group input-group-lg form-group">
					<span class="input-group-addon">
					<i class="fa fa-envelope-o" aria-hidden="true"></i>
					 <!-- <img src="<?=EXTERNAL_PATH?>images/email.gif" align=""> -->
					</span> <input type="email" name="txtEmail" class="form-control" autofocus
						id="email" placeholder="Email" aria-describedby="sizing-addon2" />
                      <?php echo form_error('txtEmail'); ?>
              </div>

				<div class="input-group input-group-lg form-group">
					<span class="input-group-addon pwd">
					 <i class="fa fa-lock" aria-hidden="true"></i>
					 <!-- <img src="<?=EXTERNAL_PATH?>images/passwrd.gif" align="">-->
					</span> <input type="password" name="txtPassword" autofocus
						class="form-control" id="password" placeholder="Password" />
                       <?php echo form_error('txtPassword'); ?>
              </div>
              
            	<div class="form-group input-group-lg">
					<input type="submit" class="form-control btn btn-sin" 
						id="submitbtn" value="<?=$this->lang->line('sign_in')?>">
				</div>
            <?php form_close(); ?>
            <div class="form-group input-group-lg">
<a href="<?=SITEURL.$this->myvalues->userRegisterDetails['controller']?>" class="form-control btn btn-imnew" id="register"><?=$this->lang->line('i_m_new')?></a>
					
				</div>
				<div class="form-group input-group-lg">
					<!-- <input type="submit" 
                     class="form-control btn btn-primary" 
                     value="Log in with Facebook"/>
                      -->
					<a href="<?=SITEURL.$controllerName.'/facebooklogin'?>"
						class="form-control btn btn-primary"><?=$this->lang->line('log_in_with_facebook')?></a>

				</div>
				<p class="text-center">
					<a href="<?=SITEURL?>forgotPassword" class="frgtpass"><?=$this->lang->line('forgot_password')?></a>
				</p>
			</div>
		</div>
	</div>
</div>

<script src="<?=EXTERNAL_PATH?>js/modules/login_form.js">
</script>
<script src="<?=EXTERNAL_PATH?>js/md5.js">
</script>

