<div class="content" style="height: 100%;">
    <?php echo $this->session->flashdata('myMessage'); ?>
	<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
            <?=$title?>	
		</div>
		<div id="no-more-tables">
		<table class="table table-hover table-striped table-bordered">
			<thead class="cf">
			<tr>
				<th width="15%">Action</th>
				<th class="">Sr #</th>
				<th>Start Time</th>
				<th>End Time</th>
				<th>Performer</th>
			</tr>
			</thead>
            <?php
            foreach ($list as $l) {
                
                ?>
            	<tr data-slot-id = "<?=$this->utility->encode($l->slot_id);?>">
            	<?php
            	    $status = "onstage";
            	    $statusTitle = "On Stage";
            	    $class="btn-warning";
            	    $clickAction = "return perfomer_action('onstage');";
            	    $links = SITEURL. $controllerName . "/changeStatus/". $this->utility->encode($l->idEvent) ."/". $this->utility->encode($l->slot_id)."/". $this->utility->encode($l->table_id)."/". $this->utility->encode($status);
            	?>
            				<td data-title="Action">
            					<?php if($this->pidGroup == '2' || ($this->pidGroup == '3' && trim($listPermission) == 'Yes')){ ?>
            					<a href="<?php echo $links; ?>" class="btn btn-sm <?php echo $class; ?>" onclick="<?php echo $clickAction; ?>"><?php echo $statusTitle; ?></a>
            					<?php }else{ echo '--'; } ?>
            				</td>
            				<td class="" data-title="Sr #"><?php echo $l->slot_id; ?></td>
            				<td data-title="Start Time"><?php echo dateDisplay($l->start_date.' '.$l->start_time,'h:i A'); ?></td>
							<td data-title="End Time"><?php echo dateDisplay($l->end_date.' '.$l->end_time,'h:i A'); ?></td>
            				<td data-title="Performer">
            		<?php
                if ($this->pidGroup == "3" && $listPermission != "Yes") {
                    
                    if ($l->idPerformer != "0") {
                        if ($l->idPerformer == "-1") {
                            echo "Intermission";
                        }
                        else {
                            echo ucwords($l->chrName);
                            if ($l->idPerformer == $this->pUserId) {
                                ?>
                    					<a
    					href="<?php echo SITEURL. $controllerName . "/resetSlot/". $this->utility->encode($l->idEvent) ."/". $this->utility->encode($l->slot_id); ?>"
    					class="btn btn-sm btn-danger pull-right" style="margin-left: 6px;"
    					onclick="return confirm_action();">Reset</a>
                        					<?php
                             }
                             // not allow the host or venue user to trade
                            if ($l->allowTrading == 1 && $isAssigned !== false) {
                                
                                if ($tradeStatus === false && $l->idPerformer != $this->pUserId) {
                                    ?>
                    							<a
					href="<?php echo SITEURL. $controllerName . "/tradeSlot/". $this->utility->encode($l->idEvent)."/". $this->utility->encode($l->slot_id); ?>"
					class="btn btn-sm btn-info pull-right"
					onclick="return confirm_action();">Trade Slot</a>
                    						<?php
                                }
                                elseif ($tradeStatus["toSlotId"] == $l->slot_id && $tradeStatus["requestFrom"] == $this->pUserId && $l->idPerformer == $tradeStatus["requestTo"]) {
                                    ?>
                    							<a href="javascript:void(0);"
					class="btn btn-sm btn-info pull-right" disabled>Trade Request Sent</a>
                    						<?php
                                }
                                elseif ($tradeStatus ["toSlotId"] == $l->slot_id) {
                                    ?>
                    							<a href="javascript:void(0);"
					class="btn btn-sm btn-info pull-right" disabled>Trade Request Received</a>
                    						<?php
                                }
                            } 
                            
                        }
                    }
                    else {
                        if ($isAssigned === false && $l->performerCanPickTime == "1") {
                            ?>
                    				<a
					href="<?php echo SITEURL . $controllerName . "/reserve/". $this->utility->encode($l->idEvent) ."/". $this->utility->encode($l->slot_id); ?>"
					class="btn btn-sm btn-primary pull-right">Reserve Time</a>
                    				<?php
                        }
                    }
                    ?>
                    		</td>
			</tr>
                    <?php
                }
                elseif ($this->pidGroup == "2" || $listPermission == "Yes") {
                    if ($l->idPerformer == "-1") {
                        echo "Intermission";
                        ?>
                            <a
				href="<?php echo SITEURL. $controllerName . "/resetSlot/". $this->utility->encode($l->idEvent) ."/". $this->utility->encode($l->slot_id)."/". $this->utility->encode($l->idPerformer); ?>"
				class="btn btn-sm btn-danger pull-right"
				onclick="return confirm_action();">Reset</a>
                           <?php
                    }
                    elseif ($l->idPerformer != "0") {
                        echo ucwords($l->chrName);
                        ?>
                            <a
				href="<?php echo SITEURL. $controllerName . "/resetSlot/". $this->utility->encode($l->idEvent) ."/". $this->utility->encode($l->slot_id)."/". $this->utility->encode($l->idPerformer); ?>"
				class="btn btn-sm btn-danger pull-right"
				onclick="return confirm_action();">Reset</a>
                           <?php
                    }
                    else {
                        ?>
                            <select disabled name="performer"
				class="form-control performerDropDown"
				table-id="<?php echo $l->table_id; ?>">
				<option value="0"></option>
								<?php foreach($performers as $p){ ?>
									<option
					value="<?php echo $this->utility->encode($p->user_id); ?>"><?php echo ucwords($p->chrName); ?></option>		
								<?php } ?>
								<option value="<?=$this->utility->encode("-1")?>">Intermission</option>
			</select>
                            
                            <?php
                    }
                }
            }
            
            ?>
		</table>
	</div>
	</div>
	<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
            Running/Completed Performer
		</div>
		<div id="no-more-tables">
		<table class="table table-hover table-striped table-bordered">
			<thead class="cf">
			<tr>
				<th class="" width="15%">Action</th>
				<th class="">Sr #</th>
				<th>Start Time</th>
				<th>End Time</th>
				<th>Performer</th>
				<th>Status</th>
			</tr>
			</thead>
			<?php if(!empty($runningList)){
			    $r='1';
			    foreach ($runningList as $rl) {
			    ?>
            <tr>
            	<td data-title="Action">
            		<?php if($this->pidGroup == '2' || ($this->pidGroup == '3' && trim($listPermission) == 'Yes')){ ?>
            		<?php if($rl->status == 'onstage'){ ?>
						<a href="<?php echo SITEURL. $controllerName . "/changeStatus/". $this->utility->encode($rl->idEvent) ."/". $this->utility->encode($rl->slot_id)."/". $this->utility->encode($rl->table_id)."/". $this->utility->encode('performed'); ?>" class="btn btn-sm btn-primary" onclick="return perfomer_action('performed');">Performed</a>
					<?php } ?>
					
			   		<a href="<?php echo SITEURL. $controllerName . "/changeStatus/". $this->utility->encode($rl->idEvent) ."/". $this->utility->encode($rl->slot_id)."/". $this->utility->encode($rl->table_id)."/". $this->utility->encode('upcoming'); ?>" class="btn btn-sm btn-danger" onclick="return perfomer_action('reset');">Back To OnStage</a>
			   		<?php }else{ echo "--"; }?>
			   		
				</td>
            	<td data-title="Sr #"><?php echo $rl->slot_id; ?></td>
            	<td data-title="Start Time"><?php if(@$rl->start_date) { echo dateDisplay($rl->start_date.' '.$rl->start_time,'h:i A'); } ?></td>
				<td data-title="End Time"><?php if(@$rl->end_date) { echo dateDisplay($rl->end_date.' '.$rl->end_time,'h:i A'); } ?></td>
				<td data-title="Performer"><?php if($rl->chrName){ echo ucwords($rl->chrName); }else { echo "--"; } ?></td>
				<td data-title="Status"><?php echo ucfirst($rl->status); ?></td>
            </tr>
            <?php $r++;} } else { ?>
            	<tr>
            		<td colspan="6" class="text-center"><?php echo $this->lang->line("error_norecords_found"); ?></td>
            	</tr>
            <?php } ?>
</table>
</div>
	</div>
</div>

<script>
$('.performerDropDown').prop('disabled',false);
$('.performerDropDown').change(function(e){
	window.location.href = SITEURL + CONTROLLER_NAME + "/reserve/<?=$eventId;?>/" + $(this).parents("tr").attr("data-slot-id") + "/" + $(this).val();
});

function confirm_action(){
	if (confirm("Are you sure you want to complete this action?")){
		return true;
	} else {
		return false;
	}
}

function perfomer_action(type){
	if(type == 'onstage'){
		var msg = 'Are you sure you want to mark this performer as "OnStage" ?';
	}
	if(type == 'performed'){
		var msg = 'Are you sure you want to mark this performer as "Performed" ?';
	}
	if(type == 'reset'){
		var msg = 'Are you sure you want to mark this performer as "Back To Stage" ?';
	}
	
	if (confirm(msg)){
		return true;
	} else {
		return false;
	}
}
</script>