<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>	
		</div>

		<table class="table table-bordered table-striped table-hover">
			
			<tr>
				<th style="width: 25%;">Show Title:</th>
				<td><?php echo ucwords($this->utility->decodeText($result->title)); ?></td>
			</tr>
			<tr>
				<th style="width: 25%;">Show City:</th>
				<td><?php echo ucwords($this->utility->decodeText($result->city)); ?>, <?php echo ucwords($this->utility->decodeText($result->state)); ?></td>
			</tr>
			<tr>
				<th>Show Address:</th>
				<td><?php echo $this->utility->decodeText($result->address); ?></td>
			</tr>
			
            <tr>
				<th>Show Date:</th>
				<td><?php echo dateDisplay($result->start_date . " " . $result->startTime,'m/d/Y'); ?></td>
			</tr>

			<tr>
				<th>Start Time:</th>
				<td><?php echo  dateDisplay($result->start_date . " " . $result->startTime,'h:i A'); ?></td>
			</tr>

			<tr>
				<th>End Time:</th>
				<td><?php echo dateDisplay($result->end_date . " " . $result->endTime,'h:i A');?></td>
			</tr>
			<tr>
				<th>Reserve Time:</th>
				<td><?php echo dateDisplay($result->start_date." ".$result->start_time,"h:i A") . " To " . dateDisplay($result->end_date." ".$result->end_time,"h:i A");?></td>
			</tr>

		</table>
		<?php 
        $eventDateEmail = $result->start_date . " " . $result->startTime;
		$eventDateEmail = convertFromGMT($eventDateEmail, "default", $result->timeZone);
		$eventDateEmail = date("jS M, Y", strtotime($eventDateEmail)) . " (" . $result->timeZone . ")";
		$message = urlencode($result->title). ' @ ' .$result->address. ' On '.$eventDateEmail .' '.dateDisplay($result->start_date . " " . $result->startTime,'h:i A') .' to '.dateDisplay($result->endTime,'h:i A');
		$message1 = $result->title. ' @ ' .$result->address. ' On '.$eventDateEmail .' '.dateDisplay($result->start_date . " " . $result->startTime,'h:i A') .' to '.dateDisplay($result->endTime,'h:i A');
		$url = $this->utility->generateOverviewUrl($result->idEvent,$result->table_id);
		$discription = urlencode($result->descr);
		?>
		<div class="text-center">
		  <label>Share your show's information on </label>
		<?php 
		$this->share->twitterShareButton($message1 .' '.$url);
		$this->share->googlePlusShareButton($url);
		$img =EXTERNAL_PATH.'/images/title.png';
		$this->share->fbShareButton($img,$message,$discription,$url);
        ?> 
        </div>
</div>
</div>
