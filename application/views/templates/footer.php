
<?php if (empty($noHeader)) : ?>
<!-- Closing DIV box-holder -->
</div>
<!-- Closing DIV site-holder -->
</div>
<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/modules/location_autocomplete.js"></script>
<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCj6VfOUCdV_MoYMvJgj1hBOARlUTEdqPM&libraries=places&callback=initAutocomplete"
	async defer></script>
 <?php if(empty($this->input->cookie('source'))){ //For check iOS request or not ?>
<div class="footer_top"></div>
<div class="footer">&copy; Stagegator. All Rights Reserved</div>
<?php } ?>

<?php if($this->pidGroup == FAN_GROUP ){?>

<!-- Modal for select location by fan-->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Select Location</h4>
			</div>
			<div class="modal-body">
          <?php echo form_open(SITEURL.$this->myvalues->profileDetails['controller'].'/setCitycookie', 'id="cityfrm" class =  "form-horizontal"'); ?>
             <div class="form-group">
					<label class="col-sm-2 control-label"><?=$this->lang->line('location')?><em>*</em></label>
					<div class="col-sm-10">
						<input type="text" name="cmbCity_1" id="locality_1"
							maxlength="250" class="form-control required"
							placeholder="Enter City Name" required value="">
							<?php
                            if (! empty($this->input->cookie('user_city') && $this->pidGroup == FAN_GROUP)) {
                                $cookieDataFooter = $this->input->cookie('user_city', true);
                                $getfullCookieFooter = json_decode($cookieDataFooter);
                                $cookieStateNameFooter = $getfullCookieFooter->state;
                                $cookieCountryNameFooter = $getfullCookieFooter->country;
                            }
                            ?>
							
						<span for="locality_1" id="err-enter-proper-location"
							style="display: none; font-size: 13px; color: #c00 !important;"
							class="help-inline text-danger">Please enter proper location.</span>
						<input type="hidden" name="cmbState_1"
							id="administrative_area_level_1_1"
							value="<?php echo @$cookieStateNameFooter;?>" /> <input
							type="hidden" name="cmbCountry_1" id="country_1"
							value="<?php echo @$cookieCountryNameFooter;?>" />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<label class="use-mycurrent-location" style="color:black; cursor:pointer; float:left;"><?php echo $this->lang->line('my_current_location'); ?></label>
				<!--    <input type="submit" class="btn btn-default" value = "Save"> -->
				<button type="button" class="btn btn-default" id="btnSubmitCity">Save</button>
			</div>
          <?php echo form_close(); ?>
     </div>
	</div>
</div>


<script>

// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

var  storagelocation = false;
//trace user latitude and longitude using geolocation and get user location detail 
function getLocation() {
	//alert('asdasd');
    var startPos;
    var geoOptions = {
      enableHighAccuracy: true
    }

    var geoSuccess = function(position) {
      startPos = position;
      storagelocation = true;
      var geocoder;
      geocoder = new google.maps.Geocoder();
      var latlng = new google.maps.LatLng(startPos.coords.latitude, startPos.coords.longitude);

      geocoder.geocode(
          {'latLng': latlng}, 
          function(results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                      if (results[0]) {
						  for(l=0; l<results[0].address_components.length; l++){
							  
							  if(results[0].address_components[l].types[0] == 'locality'){
								  city = results[0].address_components[l].long_name;
							  }
							  if(results[0].address_components[l].types[0] == 'administrative_area_level_1'){
								  state = results[0].address_components[l].long_name;
							  }
							  if(results[0].address_components[l].types[0] == 'country'){
								  country = results[0].address_components[l].long_name;
							  }
						  }

						  var fullAddress = city+','+state+','+country;
					      var locationstring = fullAddress;

					      $("#locality_1").val(city);
					      $("#administrative_area_level_1_1").val(state);
					      $("#country_1").val(country);
					      
						  //console.log(results[0]);
                          var jsonlocation = '{"city":"'+city.trim()+'","state":"'+state.trim()+'","country":"'+country.trim()+'","fullAddress":"'+fullAddress+'","locationstring":"'+locationstring+'"}';
                          setCookie('user_city',jsonlocation);
                      }
                      else  {
                          alert("address not found");
                      }
              }
               else {
                  alert("Geocoder failed due to: " + status);
              }
          }
      );
    };
    var geoError = function(error) {
      
      console.log('Error occurred. Error code: ' + error.code);
      if(document.cookie.indexOf("user_city=") <= 0){
      	$('#myModal').modal({show:true}) ; 
      }
      // error.code can be:
      //   0: unknown error
      //   1: permission denied
      //   2: position unavailable (error response from location provider)
      //   3: timed out
    };

    navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
  };

function setCookie(cname,cvalue) {
      var d = new Date();
      d.setTime(d.getTime() + (365*24*60*60*1000));
      var expires = "expires=" + d.toGMTString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
      locationValue = JSON.parse(cvalue);
      
      //console.log(locationValue);
      //console.log($('li a#openCityPopup').length);
      if(cvalue != "" && document.cookie.indexOf("user_city=") >= 0){
        if(document.cookie.indexOf("source=") >= 0){
        	$('ul#user-menu li.user-location').html('<a href = "javascript:void(0)" id = "openCityPopup" data-full-address = "'+locationValue.fullAddress+'" style = "color:black"><i class="fa fa-map-marker fa-2x"></i> '+locationValue.city+'</a><label class="use-mycurrent-location" style="color:black; cursor:pointer; margin:3px 0px;">Use My Current Location</label>');
        }else{
			$('#user-menu li.user-location').html('<a href = "javascript:void(0)" id = "openCityPopup" data-full-address = "'+locationValue.fullAddress+'" style = "color:black"><i class="fa fa-map-marker fa-2x"></i> '+locationValue.city+'</a><label class="use-mycurrent-location" style="color:black; cursor:pointer; margin:3px 0px;">Use My Current Location</label>');
        }
      }
}

$("#locality_1").focus(function(){
	   $("#err-enter-proper-location").hide();
	   $("#locality_1").val('');
	});

$(function () {	
    $("#locality_1").on('keyup keydown keypress', function (event) {
    	if(event.keyCode != 13 && event.keyCode != 9 && event.keyCode != 16)
    {  
    		$("#administrative_area_level_1_1,#country_1").val('');
    }
        if (event.keyCode == 13) {
          return false;
    }
    });
})


$(document).ready(function(){
	/* set model properties */
	$('#myModal').modal({backdrop: 'static', keyboard: false,show:false}) ;
	/*if  readCookie function return null means no cookie created till now so we open opup here onpageload and if cookie 
	created controller goes to else and popup will keep hide*/ 
// 	if(readCookie('user_city') == null || readCookie('user_city') == ''){
//   	  $('#myModal').modal({show:true}) ; 
//     }
	if(document.cookie.indexOf("user_city=") <= 0){
		setTimeout(function(){ if(storagelocation == false){ $('#myModal').modal({show:true}) ; } }, 10000); 
    }


/* validate city form which is created in popop */
$("#cityfrm").validate(
		{
			rules : {
				".required" : {
					required : true									
				}
			},    					
			highlight : function(element, errorClass,
					validClass) {
				
				$(element).parents('.form-control').addClass(
						'has-error');
			},
			unhighlight : function(element, errorClass,
					validClass) {
				$(element).parents('.form-control')
						.removeClass('has-error');
				$(element).parents('.form-control').addClass(
						'has-success');
			}
		});   
});
/* this id in header file when click on city we open popup again */
$(document).on('click','#openCityPopup',function(){
	
	$("#err-enter-proper-location").hide();
    
	$('#myModal').modal({show:true});
	
	$("#locality_1").val($(this).attr('data-full-address'));
});

/* this id in header file when click on city we open popup again */
$(document).on('click','.use-mycurrent-location',function(){
	if($('#myModal').hasClass('in')){
		$('#myModal').modal('toggle');
	}
	getLocation();
});

/* also chekcking form validiti is form validate or not */
$(document).on('click','#btnSubmitCity',function(){    
    if($("#cityfrm").valid())
    {  
        if($("#administrative_area_level_1_1").val() && $("#country_1").val()){
        	$("#err-enter-proper-location").hide();
        	$("#cityfrm").submit();
        }else{
            $("#err-enter-proper-location").show();          
        }
    	
    }else{
    	return false;
    }
});



</script>



<script>





/* set key press handler for google autocomplete */ 
/*
$(function () {	
    $("#locality").on('keyup keydown keypress', function (event) {
        if (event.keyCode == 13) {
          return false;
    }
    });
});
*/

/* this function read cookie */
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

</script>

<?php }?>
<?php endif; ?>

<?php
/*
 * ?><script>
 * // A $( document ).ready() block.
 * $( document ).ready(function() {
 * $('.btn-nav-toggle-responsive').click(function(){
 * $('.left-sidebar').toggleClass('show-fullsidebar');
 * });
 * $('.left-sidebar .nav > li > ul > li.active').parent().css('display','block');
 *
 * });
 * </script><?php
 */
?>
<script src="<?=EXTERNAL_PATH?>js/accordian.js"></script>
<script src="<?=EXTERNAL_PATH?>js/core.js"></script>

<script src="<?=EXTERNAL_PATH?>js/jquery.dataTables.min.js"> </script>
<script src="<?=EXTERNAL_PATH?>js/dataTables.bootstrap.min.js"> </script>
<link href="<?=EXTERNAL_PATH?>css/dataTables.bootstrap.min.css"
	rel="stylesheet" type="text/css">

<!-- <script src="<?=EXTERNAL_PATH?>js/jquery.dataTables.min.js"> </script> -->
<!-- <script src="<?=EXTERNAL_PATH?>/js/oldDatatable/bootstrap-datatables.js"></script>
<script src="<?=EXTERNAL_PATH?>/js/oldDatatable/dataTables-custom.js"></script> -->

<script>
var table;
var table1;
$(document).ready(function() {
	datatableDisplayLength = typeof(datatableDisplayLength)== "undefined" ? 10 : datatableDisplayLength;
	table = $(".dataTable").DataTable({"pageLength": datatableDisplayLength , "pagingType": "full_numbers","ordering":"false"});
	//$('.dataTables_info').addClass('marginBtm10');
	
	$(window).resize(function(){
		table.draw();
		$('.dataTables_info').addClass('marginBtm10');
	});	
	/* to manage responsive pagination in mobile devices */
	if ($(window).width() < 514) {
		$('#DataTables_Table_0_wrapper').removeClass('dataTables_wrapper');
		$('#listEvent_wrapper').removeClass('dataTables_wrapper');
	} 
	$(".tooltips").tooltip();
  
});
/* this condtion when a user will logged in with facebook and get hash url then remove hash url from this */
if (window.location.hash == '#_=_') {
    window.location.hash = ''; // for older browsers, leaves a # behind
    history.pushState('', document.title, window.location.pathname); // nice and clean
    e.preventDefault(); // no page reload
}	
 </script>
 
 
<script type='text/javascript'>

$('document').ready(function(){
	
	window.__lo_site_id = 73311;

	(function() {
    	var wa = document.createElement('script');
    	 wa.type = 'text/javascript';
    	 wa.async = true;
    	wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
    
    	var s =	document.getElementsByTagName('script')[0]; 
    	s.parentNode.insertBefore(wa, s);
	 })();
		
});
 

</script>

</body>