<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
?><head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<title>
    <?= $title; ?>
</title>
<?php  if(isset($tag) && !empty($tag)){
foreach($tag as $key => $value)
{ ?>
<meta property="<?=$key?>" content="<?= strip_tags(str_replace(["</br>", "<br/>"], ["\r\n", "\r\n"], $value));?>" />
<?php }
} ?>

<meta name="description" content="<?=SITENAME?>">
<meta name="author" content="<?=SITENAME?>">
<link rel="shortcut icon" href="<?=EXTERNAL_PATH?>images/favicon.png">
<link href="<?=EXTERNAL_PATH?>css/bootstrap.css" rel="stylesheet" />
<link href="<?=EXTERNAL_PATH?>css/style.css" rel="stylesheet" />
<link href="<?=EXTERNAL_PATH?>css/font-awesome.min.css" rel="stylesheet" />
<link href="<?=EXTERNAL_PATH?>raty/jquery.raty.css" rel="stylesheet"> 

<script src="<?=EXTERNAL_PATH?>js/jquery-1.10.2.min.js">
</script>
<script src="<?=EXTERNAL_PATH?>/js/bootstrap.min.js">
</script>
<script src="<?=EXTERNAL_PATH?>/js/bootbox.min-4.4.0.js">
</script>
    <?php
    $controllerName = isset($controllerName) ? $controllerName : "";
				$loadData ["view"] = $view;
				$this->load->view ( "templates/headerFiles", $loadData );
				?>
    <script>
		var SITEURL = '<?php echo SITEURL;?>';
		var CONTROLLER_NAME = '<?php echo $controllerName;?>';
		var EXTERNAL_PATH = '<?=EXTERNAL_PATH?>';
	</script>
<!-- <script type="text/javascript" async="async" defer="defer"
	data-cfasync="false"
	src="https://mylivechat.com/chatinline.aspx?hccid=41304575"></script>  -->
<link href="<?=EXTERNAL_PATH?>css/custom.css" rel="stylesheet" type="text/css">
</head>

<noscript>
	<!--
        NOTE:
        HTML5 spec is currently reviewing deprecation of noscript tags as it is considered best practices that
        web apps should be progressively enhance with JavaScript than assume JavaScript is enabled by default
        -->
	<meta http-equiv="refresh"
		content="0;url=<?php echo SITEURL.'resources/scriptdisable'; ?>">
</noscript>
<script src="<?=EXTERNAL_PATH?>js/modules/my_functions.js"></script>
<script src="<?=EXTERNAL_PATH?>js/clipboard.min.js"></script>
<body>
<?php

/* For check iOS request or not */
$showSideNavMenu = 1;
if($this->input->cookie('source') == 'iOS'){
    $showSideNavMenu = 0;
}else if($this->input->get('source') == 'iOS'){
    $showSideNavMenu = 0;
    $cookie = array(
        'name'   => 'source',
        'value'  => 'iOS',
        'expire' => time() + 60 * 60 * 24 * 365
    );
    $this->input->set_cookie($cookie);
}
/* End Here */

echo form_open ();
echo form_close ();

if (empty ( $noHeader )) :
	
	?>
    
			 <div class="site-holder">
		<?php if($showSideNavMenu) { ?>
		<div class="top-bar"></div>
		<nav class="navbar mobile-menu-main" role="navigation">
			<span class="logo"><a href="<?=SITEURL?>">
        <?php /*?><img src="<?=EXTERNAL_PATH?>images/logo.png"><?php */?>
        <img src="<?=EXTERNAL_PATH?>images/logo.png">
			</a></span>
			
			<div class="navbar-header"> <!-- style="height: 65px;" -->
				<a class="navbar-brand" href="javascript:void(0);"> <i
					class="fa fa-list btn-nav-toggle-responsive"></i>
				</a>
				
				
			</div>
		<?php if($this->session->userdata("UserData") && $this->pidGroup != ADMIN_GROUP) { ?>
			
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav user-menu navbar-right " id="user-menu">
				 <?php
                            /* this list item only for fan if fan already selected city we show this list items and show city below
                             * and after click on this city we will open a popup also to ask a new city*/
							 if(!empty($this->pidGroup == FAN_GROUP)) {
							     
							    $cookieData = $this->input->cookie('user_city', true);
							    $getfullCookie = json_decode($cookieData);
							    @$cookieCityName = $getfullCookie->city;
							    @$cookieFullAddress = $getfullCookie->fullAddress;  
							     
							   ?>
							
							<li class="user-location hidden-xs"><a href = "javascript:void(0)" id = "openCityPopup" data-full-address = "<?=$cookieFullAddress?>" style = "color:black"><i class="fa fa-map-marker fa-2x"></i> <?=$cookieCityName?></a><label class="use-mycurrent-location" style="color:black; cursor:pointer; margin:3px 0px;"><?php echo $this->lang->line('my_current_location'); ?></label></li>
						<?php } ?>
			
				<?php  if($this->pidGroup != FAN_GROUP){?>
				<!-- Not display the subscription link -->
				<!-- <li class=""><a href="<?=SITEURL . $this->myvalues->subscriptionDetails["controller"];?>"><i class="fa fa-usd fa-2x tooltips" data-original-title="Subscription" data-placement="bottom"  aria-hidden="true"></i></a></li> -->
					<?php } ?>
					<li class=""><a href="<?php echo SITEURL."page/contact_us"; ?>"
						style="padding-top: 15px;"><img
							src="<?=EXTERNAL_PATH?>images/bulb.png" data-placement="bottom"
							data-original-title="Suggestions/ Bug" class="tooltips"
							style="width: 33px;"></a></li>
				    <?php if($this->pidGroup == PERFOMER_GROUP) {?>
					<li class=""><a href="<?php echo SITEURL. $this->myvalues->tradingDetails['controller']; ?>"
						title=""><img src="<?=EXTERNAL_PATH?>images/trade.png"
							data-placement="bottom" data-original-title="Trading Requests"
							class="tooltips"><span
							class="badge bg-success notificationCountTrading"></span></a></li>
							<?php } 
							$tooltipSearch = "";
							$searchUrl = "";
							if($this->pidGroup == PERFOMER_GROUP || $this->pidGroup == FAN_GROUP){
							     $searchUrl = SITEURL.$this->myvalues->eventDetails['controller']."/search";
							     $tooltipSearch = "Search Events"; 
							}
							elseif($this->pidGroup == VENUE_GROUP){
							    $searchUrl = SITEURL.$this->myvalues->performerDetails['controller']."/searchPerformer";
							    $tooltipSearch = "Search Performers";
							}
							?>
					<li class="visible-lg visible-md"><a
						href="<?=$searchUrl?>"
						title=""><img
							src="<?=EXTERNAL_PATH?>images/glyphicons_027_search.png"
							data-placement="bottom" data-original-title="<?=$tooltipSearch?>"
							class="tooltips"></a></li>
						
						<?php  if($this->pidGroup != FAN_GROUP) {?>
					<li><a
						href="<?php echo SITEURL.$this->myvalues->invitationDetails['controller']."/sent"; ?>"
						style="padding: 20px 10px;"><img
							src="<?=EXTERNAL_PATH?>images/cane.png" data-placement="bottom"
							data-original-title="Awaiting Approval from Venue"
							class="tooltips" style="width: 35px;"><span
							class="badge bg-success notificationCountBadgeTop"></span></a></li>
					<li><a
						href="<?php echo SITEURL.$this->myvalues->invitationDetails['controller']."/received"; ?>"
						title=""><img src="<?=EXTERNAL_PATH?>images/hat.png"
							data-placement="bottom"
							data-original-title="Booking Requests Received" class="tooltips"
							style="width: 25px;"><span
							class="badge bg-success notice2BadgeTop"></span></a></li>
							<?php if($this->pidGroup == VENUE_GROUP) {
							  $hostUrl = SITEURL.$this->myvalues->hostDetails['controller']."/host_management";
							}
							if($this->pidGroup == PERFOMER_GROUP) {
							    $hostUrl = SITEURL.$this->myvalues->hostDetails['controller']."/hostReceived";
							}
							 ?>
					<li class="visible-lg visible-md"><a
						href="<?=$hostUrl?>" title=""><img
							src="<?=EXTERNAL_PATH?>images/microphone2.png"
							style="width: 13px; height: 28px;" data-placement="bottom"
							data-original-title="Host Invitations" class="tooltips"><span
							class="badge bg-success hostInviteCount"></span></a></li>
							<?php } ?>
					<?php /*		
					<li class="visible-lg visible-md">
					<?php $msgCount = $this->main_model->getUnreadMessage();
					  if($msgCount > 0){
					?>
					<div id="noti_Counter" class="msgcount"><?=$msgCount>99?'99+':$msgCount?></div>
					<?php } ?>
					
					<a
						href="<?=SITEURL.$this->myvalues->messageDetails['controller']?>" title=""><img
							src="<?=EXTERNAL_PATH?>images/envelope.png"
							data-placement="bottom" data-original-title="New Messages"
							class="tooltips"><span class="badge bg-success inboxCountBadge"></span></a></li>
							
							*/?>
							
							
				<li class="visible-lg visible-md">
					<?php $msgCount = $this->main_model->getUnreadMessage();
					  if($msgCount > 0){
					?>
					<div id="noti_Counter" class="msgcount"><?=$msgCount>99?'99+':$msgCount?></div>
					<?php } ?>
					
					<a
						href="<?=SITEURL.$this->myvalues->messageDetails['controller']?>" title="">
						<i class="fa fa-bell fa-2x tooltips" data-placement="bottom" data-original-title="New Messages"
							 aria-hidden="true"></i>
						<span class="badge bg-success inboxCountBadge"></span></a></li>			
							
					 <li id="noti_Container" class = "hidden">
						<div id="noti_Counter"></div> <!--SHOW NOTIFICATIONS COUNT.--> <!--A CIRCLE LIKE BUTTON TO DISPLAY NOTIFICATION DROPDOWN.-->
						<div id="noti_Button">
							<i class="fa fa-bell fa-lg" aria-hidden="true"></i>
						</div> 
					</li>
					<?php if($this->pidGroup == PERFOMER_GROUP){
					    
					if($tipDetails->stripe_status == 'Verified'){
					?>
					<li class="hidden-sm hidden-xs"><a class="tipping-bal" href="<?php echo SITEURL.$this->myvalues->tippingDetails ['controller']."/tipPerformerBalance";?>">Tipping Balance: <strong>$ <?=$tipDetails->d_stripe_balance==''?0:($tipDetails->d_stripe_balance)/100?></strong></a></li>
					<?php } else {
            			if(($tipDetails->stripe_account_id != '') && in_array($tipDetails->stripe_status,['Rejected','Unverified'])) {?>
                    	<li class="hidden-sm hidden-xs"><a style="float: right;"
                    			href="<?=SITEURL.$this->myvalues->tippingDetails['controller'].'/updateTipping'?>"
                    			class="btn-success btn tipping-btn">Update Tipping</a></li>
                    	<?php } elseif($tipDetails->stripe_account_id == '' && $tipDetails->stripe_status == 'NA') {?>
                    	<li class="hidden-sm hidden-xs"><a style="float: right;"
                    			href="<?=SITEURL.$this->myvalues->tippingDetails['controller']?>"
                    			class="btn-success btn tipping-btn">Register to accept tips</a></li>	
                      <?php }  ?>
					<?php }  } ?>
					<li><a href="#" class="dropdown-toggle" data-toggle="dropdown"
						style="padding-top: 13px;"> <span class="username"><?php
		$profile_img = EXTERNAL_PATH."images/profiles/eleven.png";
	    $filePath = './external/images/profiles/'.$this->session->userdata("UserData")->image;	    
	    
		if (! empty ( $this->session->userdata("UserData")->image ) && file_exists($filePath)) {
			$profile_img = EXTERNAL_PATH."images/profiles/".$this->session->userdata("UserData")->image;
		}else if(!empty($this->session->userdata("UserData")->image) && filter_var($this->session->userdata("UserData")->image, FILTER_VALIDATE_URL) == true) {
		    $profile_img = $this->session->userdata("UserData")->image;
		}
		?><div class="user-avatar userImageTopNavigation" style="background-image:url('<?=$profile_img; ?>')"></div><?php echo $this->utility->decodeText($this->session->userdata("UserData")->chrName); ?> <b
								class="caret"></b></span>
					</a>
						<ul class="dropdown-menu">
							<li><a
								href="<?php echo SITEURL.$this->myvalues->profileDetails['controller']; ?>"><i
									class="fa fa-user"></i><?=$this->lang->line('my_profile')?></a></li>
							<li><a href="<?php echo SITEURL. $this->myvalues->messageDetails['controller']; ?>"><i
									class="fa fa-envelope"></i><?=$this->lang->line('my_inbox')?><span
									class="badge bg-success inboxCountBadge"></span></a></li>
							<li><a
								href="<?php echo SITEURL.$this->myvalues->profileDetails['controller']."/settings"; ?>"><i
									class="fa fa-wrench"></i><?=$this->lang->line('settings')?></a></li>
							<li><a href="<?php echo SITEURL."page/help"; ?>"><i
									class="fa fa-file"></i><?=$this->lang->line('help')?></a></li>
							<li><a
								href="javascript:void(0);" id="accountDelete"
								class="text-danger"><i class="fa fa-trash-o"></i><?=$this->lang->line('delete_account')?></a></li>
							<li class="divider"></li>
							<li><a href="<?php echo SITEURL."logout"; ?>" class="text-danger"><i
									class="fa fa-lock"></i> <?=$this->lang->line('logout')?></a></li>
						</ul></li>

				</ul>
			</div>
			<?php } else { ?>
			<!-- /.navbar-collapse -->
			
			<?php }
			
			if($this->pidGroup == PERFOMER_GROUP){
			?>
			
			<div class="visible-xs visible-sm">
				<ul class="list-unstyled mobile-menu">
					<?php if($this->pidGroup == PERFOMER_GROUP){
					if($tipDetails->stripe_status == 'Verified'){
					?>
					<li><a class="tipping-bal" href="<?php echo SITEURL.$this->myvalues->tippingDetails ['controller']."/tipPerformerBalance";?>">Tipping Balance: <strong>$ <?=$tipDetails->d_stripe_balance==''?0:($tipDetails->d_stripe_balance)/100?></strong></a></li>
					<?php } else {
			if(($tipDetails->stripe_account_id != '') && in_array($tipDetails->stripe_status,['Rejected','Unverified'])) {?>
	<li><a href="<?=SITEURL.$this->myvalues->tippingDetails['controller'].'/updateTipping'?>"
			class="btn-success btn tipping-btn">Update Tipping</a></li>
	<?php } elseif($tipDetails->stripe_account_id == '' && $tipDetails->stripe_status == 'NA') {?>
	<li><a href="<?=SITEURL.$this->myvalues->tippingDetails['controller']?>"
			class="btn-success btn tipping-btn">Registration For Tipping</a></li>	
  <?php }  ?>
					<?php } ?>
				</ul>
				</div> 
			<?php 
				}
			}
			
			if($this->pidGroup == FAN_GROUP){
			?>
			<div class="visible-xs">
				<ul class="list-unstyled mobile-menu">
				<?php 
                /* this list item only for fan if fan already selected city we show this list items and show city below
                * and after click on this city we will open a popup also to ask a new city*/
				if(!empty($this->pidGroup == FAN_GROUP)) {
					$cookieData = $this->input->cookie('user_city', true);
					$getfullCookie = json_decode($cookieData);
					$cookieCityName = $getfullCookie->city;
					//$cookieFullAddress = $getfullCookie->fullAddress;
					$cookieFullAddress = $getfullCookie->locationstring;
				?>
				<li class="user-location" style="float: right;"><a href = "javascript:void(0)" id = "openCityPopup" data-full-address = "<?=$cookieFullAddress?>" style = "color:black"><i class="fa fa-map-marker fa-2x"></i> <?=$cookieCityName?></a><label class="use-mycurrent-location" style="color:black; cursor:pointer; margin:3px 0px;"><?php echo $this->lang->line('my_current_location'); ?></label></li>
			<?php }
			?>
			</ul>
			</div>
			<?php
			}
			?>
		</nav>
		<div class="top-bar" style="height: 5px;"></div>
		<?php }else{ ?>
			<?php  if($this->pidGroup != FAN_GROUP){ ?>
			<div class="navbar-collapse">
				<?php if($this->pidGroup == PERFOMER_GROUP){ ?>
    			<ul>
        			<?php if($tipDetails->stripe_status == 'Verified'){ ?>
						<li class="hidden-sm hidden-xs" style="display:block !important;"><a class="tipping-bal" href="<?php echo SITEURL.$this->myvalues->tippingDetails ['controller']."/tipPerformerBalance";?>">Tipping Balance: <strong>$ <?=$tipDetails->d_stripe_balance==''?0:($tipDetails->d_stripe_balance)/100?></strong></a></li>
					<?php } else {
            			if(($tipDetails->stripe_account_id != '') && in_array($tipDetails->stripe_status,['Rejected','Unverified'])) {?>
                    		<li class="hidden-sm hidden-xs" style="display:block !important;"><a style="float: right;" href="<?=SITEURL.$this->myvalues->tippingDetails['controller'].'/updateTipping'?>" class="btn-success btn tipping-btn">Update Tipping</a></li>
                    	<?php } elseif($tipDetails->stripe_account_id == '' && $tipDetails->stripe_status == 'NA') {?>
                    		<li class="hidden-sm hidden-xs" style="display:block !important;"><a style="float: right;" href="<?=SITEURL.$this->myvalues->tippingDetails['controller']?>" class="btn-success btn tipping-btn">Register to accept tips</a></li>	
                      <?php }  ?>
					<?php } ?>
    			</ul>
    			<?php } ?>
			</div>
			<?php }else{ ?>
			    <div class="navbar-collapse">
    				<ul class="user-menu" id="user-menu" style="display:block !important;">
    					<?php
    					//if(!empty($this->input->cookie('user_city'))){
        					$cookieData = $this->input->cookie('user_city', true);
        					$getfullCookie = json_decode($cookieData);
        					@$cookieCityName = $getfullCookie->city;
        					@$cookieFullAddress = $getfullCookie->fullAddress;
    					?>
    					<li class="user-location hidden-sm hidden-xs" style="display:block !important;"><a href = "javascript:void(0)" id = "openCityPopup" data-full-address = "<?=$cookieFullAddress?>" style = "color:black"><i class="fa fa-map-marker fa-2x"></i> <?=$cookieCityName?></a><label class="use-mycurrent-location" style="color:black; cursor:pointer; margin:3px 0px;"><?php echo $this->lang->line('my_current_location'); ?></label></li>
    					<?php //} ?>
    				</ul>
				</div>
			<?php } ?>
		<?php } ?>
		<!-- /.navbar -->
		
		
		<div class="box-holder">		
		<?php
	
	/* Loads Left slide bar */
	if (isset ( $showMenu ) && $showMenu === false) {
	} else {
	    if($showSideNavMenu){
		  $this->load->view ( "templates/side_menu" );
	    }
	}
	
	?>
<?php
endif;
?>


	<script>
    $(document).ready(function () {

        // ANIMATEDLY DISPLAY THE NOTIFICATION COUNTER.
        $('#noti_Counter')
            .css({ opacity: 0 })
            //.text('7')              // ADD DYNAMIC VALUE (YOU CAN EXTRACT DATA FROM DATABASE OR XML).
            //.css({ top: '-10px' })
            .animate({ opacity: 1 }, 500);

        $('#noti_Button').click(function () {

            // TOGGLE (SHOW OR HIDE) NOTIFICATION WINDOW.
            $('#notifications').fadeToggle('fast', 'linear', function () {
                if ($('#notifications').is(':hidden')) {
                    //$('#noti_Button').css('background-color', '#2E467C');
                }
               // else $('#noti_Button').css('background-color', '#FFF');        // CHANGE BACKGROUND COLOR OF THE BUTTON.
            });

            $('#noti_Counter').fadeOut('slow');                 // HIDE THE COUNTER.

            return false;
        });

        // HIDE NOTIFICATIONS WHEN CLICKED ANYWHERE ON THE PAGE.
        $(document).click(function () {
            $('#notifications').hide();

            // CHECK IF NOTIFICATION COUNTER IS HIDDEN.
            if ($('#noti_Counter').is(':hidden')) {
                // CHANGE BACKGROUND COLOR OF THE BUTTON.
               // $('#noti_Button').css('background-color', '#2E467C');
            }
        });

        $('#notifications').click(function () {
            return false;       // DO NOTHING WHEN CONTAINER IS CLICKED.
        });

        function handleAjaxErr(status) {
        	// if status 401 redirect to login page
        	if (status === 401) {
        		window.location.href = "<?=SITEURL.$this->myvalues->loginDetails["controller"];?>";
        	}
        }
    });
</script>