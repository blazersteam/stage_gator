<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/chosen.jquery.js"></script>
<link href="<?=EXTERNAL_PATH?>css/chosen.css" rel="stylesheet"
	type="text/css">
<script src="<?=EXTERNAL_PATH?>js/validate.js"></script>
<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>raty/jquery.raty.js"></script>

<!-- delete account popup -->
<div class="modal fade bs-example-modal-sm " tabindex="-1" role="dialog"
	aria-labelledby="mySmallModalLabel" id="deleteAccountModel">
			 <?php
    //echo form_open(SITEURL . $this->myvalues->profileDetails ['controller'] . "/delete");
    ?>
			<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel"><?php echo $this->lang->line('delete_account');?></h4>
			</div>
			<div class="modal-body">
				<table class="table">
					<tr>
						<td>
									<?php echo $this->lang->line('delete_message');?>
								</td>
					</tr>
				</table>
			</div>

			<div class="modal-footer">
				<button type="button" id="btnDeleteAccount" class="btn btn-primary"><?php echo $this->lang->line('delete_account');?></button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
			<?php //echo form_close();?> 
</div>


<script>
$('#accountDelete').click(function (e){
	e.preventDefault();
	$("#deleteAccountModel").modal('show');
});

$('#btnDeleteAccount').click(function (){
	window.location.href = "<?php echo SITEURL . $this->myvalues->profileDetails ['controller'] . '/delete';?>";	
});	


</script>