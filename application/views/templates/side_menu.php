<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<!-- .left-sidebar -->
<div class="left-sidebar">
	<div class="sidebar-holder" style="padding: 0px;">
		<ul class="nav  nav-list">
		<?php 
	
		if(!$this->session->userdata('UserData')){?>
           <li
				class='left_navigation <?php echo ($this->uri->segment(2)=='my_performers')?"active":""; ?>'>
				<a href='<?php echo SITEURL; ?>'
				data-original-title='My Performer Stream'> <span
					class='hidden-minibar'><?=$this->lang->line('home')?></a>
			</li>

			<li
				class='left_navigation <?php echo ($this->uri->segment(2)=='terms_and_conditions')?"active":""; ?>'>
				<a href='<?php echo SITEURL."page/terms_and_conditions/"; ?>'
				data-original-title='My Venue Stream'> <span class='hidden-minibar'><?=$this->lang->line('terms_and_conditions')?></span>
			</a>
			</li>

			<li
				class='left_navigation <?php echo ($this->uri->segment(2)=='privacy_policy')?"active":""; ?>'>
				<a href='<?php echo SITEURL."page/privacy_policy"; ?>'
				data-original-title='Search for New Users To Bite'> <span
					class='hidden-minibar'><?=$this->lang->line('privacy_policy')?></span>
			</a>
			</li>
			<li
				class='left_navigation <?php echo ($this->uri->segment(2)=='contact_us')?"active":""; ?>'>
				<a href='<?php echo SITEURL."page/contact_us"; ?>'
				data-original-title='Search for Shows'> <span class='hidden-minibar'><?=$this->lang->line('contact_us')?></span>
			</a>
			</li>
			<?php
}

else if ($this->pidGroup == 2) {
    /* start menu below for venue */
    ?>
			  
                  <li class='submenu  left_navigation'><a
				class='dropdown' href='javascript:void(0);'
				data-original-title='Update Profile'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/edit_img.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'><?=$this->lang->line('update_profile')?></span>
			</a>
				<ul>
					<li
						class=' <?php echo ($this->uri->segment(1)==$this->myvalues->profileDetails['controller'] && $this->uri->segment(2)=='')?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->profileDetails['controller']; ?>'
						data-original-title='Over View'> <img
							src="<?=EXTERNAL_PATH?>images/overview_img.png"
							style="padding-left: 10px;"> <span class='hidden-minibar'><?=$this->lang->line('overview')?></span>
					</a>
					</li>
					<li
						class=' <?php echo ( $this->uri->segment(1)==$this->myvalues->profileDetails['controller'] && $this->uri->segment(2)=='edit')?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->profileDetails['controller']."/edit"; ?>'
						data-original-title='Personal Info'> <img
							src="<?=EXTERNAL_PATH?>images/personal_info_img.png"
							style="padding-left: 10px;"> <span class='hidden-minibar'><?=$this->lang->line('profile')?></span>
					</a>
					</li>
					
					<li
						class=' <?php echo ($this->uri->segment(1)== $this->myvalues->locationDetails['controller'])?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->locationDetails['controller']; ?>'
						data-original-title='Show Locations'> <i class="fa fa-building"
							style="margin: 0px 0px 0px 10px;"></i> <span
							class='hidden-minibar'><?=$this->lang->line('locations')?></span>
					</a>
					</li>

					<li
						class='<?php echo ($this->uri->segment(1) == $this->myvalues->change_passwordDetails['controller']) ? "active" : ""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->change_passwordDetails['controller']; ?>'
						data-original-title='Change Password'> <img
							src="<?=EXTERNAL_PATH?>images/lock.png"
							style="padding-left: 10px;"> <span class='hidden-minibar'><?=$this->lang->line('change_password')?></span>
					</a>
					</li>						
                  		<?php
    /*
     * ?>
     * <li class=' ' >
     * <a href='#' data-original-title='Privacy Settings'>
     * <img src="<?php echo SITEURL."assets/"; ?>images/privacy.png" style="padding-left: 10px;">
     * <span class='hidden-minibar'> Privacy Settings</span>
     * </a>
     * </li>
     */
    ?>
                  </ul></li>
			<li class='submenu  left_navigation'><a class='dropdown'
				href='javascript:void(0);' data-original-title=''>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/event_icon.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'><?=$this->lang->line('show_manager')?></span>
			</a>
				<ul>

					<li
						class=' <?php echo ( $this->uri->segment(1) == $this->myvalues->eventDetails['controller']  && ($this->uri->segment(2)=='add' || $this->uri->segment(2)=='edit'))?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->eventDetails['controller']."/add"; ?>'
						data-original-title='Create Event'> <img
							src="<?=EXTERNAL_PATH?>images/event_create.png"
							style="padding-left: 10px;"> <span class='hidden-minibar'><?=$this->lang->line('create_a_show')?></span>
					</a>
					</li>


					<li
						class=' <?php echo ($this->uri->segment(1)==$this->myvalues->eventDetails['controller'] && ($this->uri->segment(2)==''||$this->uri->segment(2)=='schedule') )?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->eventDetails['controller']; ?>'
						data-original-title='Event Management'> <img
							src="<?=EXTERNAL_PATH?>images/event_list.png"
							style="padding-left: 10px;"> <span class='hidden-minibar'><?=$this->lang->line('manage_schedule')?></span>
					</a>
					</li>
					
					<li
						class=' <?php echo ($this->uri->segment(1)==$this->myvalues->pasteventsDetails['controller'] && ($this->uri->segment(2)==''||$this->uri->segment(2)=='schedule') )?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->pasteventsDetails['controller']; ?>'
						data-original-title='Event Management'> <img
							src="<?=EXTERNAL_PATH?>images/event_list.png"
							style="padding-left: 10px;"> <span class='hidden-minibar'>Past events</span>
					</a>
					</li>
				</ul></li>
    
			<li
				class='left_navigation <?php echo ($this->uri->segment(1)==$this->myvalues->invitationDetails['controller'] && $this->uri->segment(2)=='received')?"active":""; ?>'>
				<a
				href='<?php echo SITEURL.$this->myvalues->invitationDetails['controller'].'/received'; ?>'
				data-original-title='Apply'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/cane2.png"
							style="padding: 7px 0px 0px 8px; width: 33px;">
					</div> <span class='hidden-minibar'><?=$this->lang->line('booking_requests_received')?></span>
			</a>
			</li>

			<li
				class='left_navigation <?php echo ($this->uri->segment(1)== $this->myvalues->invitationDetails['controller'] && $this->uri->segment(2)=='sent')?"active":""; ?>'>
				<a
				href='<?php echo SITEURL.$this->myvalues->invitationDetails['controller'].'/sent'; ?>'
				data-original-title='Performance Invites'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/hat_grey.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'><?=$this->lang->line('booking_invitations_sent')?></span>
			</a>
			</li>

			<li class='submenu  left_navigation'><a class='dropdown'
				href='javascript:void(0);' data-original-title=''>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/microphone2_grey.png"
							style="padding: 7px 0px 0px 15px;">
					</div> <span class='hidden-minibar'><?=$this->lang->line('host_menu')?><span
						class="badge bg-success hostInviteCount"></span></span>
			</a>
				<ul>
					<li
						class=' <?php echo ($this->uri->segment(1)== $this->myvalues->hostDetails['controller'] && $this->uri->segment(2)=='host_management')?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->hostDetails['controller']."/host_management"; ?>'
						data-original-title='Host Management'> <i class="fa fa-list"></i>
							<span class='hidden-minibar'><?=$this->lang->line('manage_hosts')?><span
								class="badge bg-success hostInviteCount"></span></span>
					</a>
					</li>

					<li
						class=' <?php echo ($this->uri->segment(1)== $this->myvalues->hostDetails['controller'] && $this->uri->segment(2)=='new_host_invite')?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->hostDetails['controller']."/new_host_invite"; ?>'
						data-original-title='New Host Invite'> <i class="fa fa-plus"></i>
							<span class='hidden-minibar'><?=$this->lang->line('book_a_host')?></span>
					</a>
					</li>
				</ul></li>

			<li
				class='left_navigation <?php echo ($this->uri->segment(1)== $this->myvalues->performerDetails['controller'] && $this->uri->segment(2)=='searchPerformer')?"active":""; ?>'>
				<a
				href='<?php echo SITEURL.$this->myvalues->performerDetails['controller'].'/searchPerformer'; ?>'
				data-original-title='Search for Events'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/search_img.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'><?=$this->lang->line('search_for_performers')?></span>
			</a>
			</li>

			
			<!-- For bite functioanlity start-->
			
			<li class='submenu  left_navigation'><a class='dropdown'
				href='javascript:void(0);' data-original-title=''>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/overview_img.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'><?=$this->lang->line('bites')?><span
						class="badge bg-success"></span></span>
			</a>
				<ul>
					<li
						class='<?php echo ($this->uri->segment(1)==$this->myvalues->biteDetails['controller'] && $this->uri->segment(2)=='')?"active":""; ?>'>
						<a
						href='<?=SITEURL.$this->myvalues->biteDetails['controller']?>'
						data-original-title='<?=$this->lang->line('view_bites')?>'> <img
							src="<?=EXTERNAL_PATH?>images/overview_img.png"
							style="padding-left: 10px;">
							<span class='hidden-minibar'><?=$this->lang->line('view_bites')?><span
								class="badge bg-success "></span></span>
					</a>
					</li>

                    					<?php 
                    $this->load->model($this->myvalues->subscriptionDetails ['model'], 'subscription_model');
                    $resultPlanID = $this->subscription_model->getCurrentPlan($this->pUserId, $this->pidGroup);
                    
                    // not display the bitten me for beginner plan
                    if ($resultPlanID != 4){
                    ?>
					<li
						class=' <?php echo ($this->uri->segment(1)== $this->myvalues->biteDetails['controller'] && $this->uri->segment(2)=='bittenMe')?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->biteDetails['controller']."/bittenMe"; ?>'
						data-original-title='<?=$this->lang->line('bite_to_me')?>'> <img
							src="<?=EXTERNAL_PATH?>images/overview_img.png"
							style="padding-left: 10px;">
							<span class='hidden-minibar'><?=$this->lang->line('bite_to_me')?></span>
					</a>
					</li>
					<?php }?>
				</ul></li>
			
			
			<!-- For bite functioanlity END-->
			<!-- Not display the subcription start -->
			<!-- 
			<li class='submenu  left_navigation'><a class='dropdown'
				href='javascript:void(0);' data-original-title=''>
					<div class="left_nav_menu_img">
					<i class="fa fa-usd fa-2x" style="margin: 5px 10px 0 9px" aria-hidden="true"></i>
					</div> <span class='hidden-minibar'>Subscription</span>
			</a>
				<ul>

					<li
						class=' <?php echo ( $this->uri->segment(1) == $this->myvalues->subscriptionDetails['controller']  && $this->uri->segment(2)=='')?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->subscriptionDetails['controller']; ?>'
						data-original-title='Subscribe Plan'> <img
							src="<?=EXTERNAL_PATH?>images/event_create.png"
							style="padding-left: 10px;"> <span class='hidden-minibar'>Subscribe
								plan</span>
					</a>
					</li>


					<li
						class=' <?php echo ($this->uri->segment(1)==$this->myvalues->subscriptionDetails['controller'] && $this->uri->segment(2)=='history' )?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->subscriptionDetails['controller']."/history"; ?>'
						data-original-title='Payment History'> <img
							src="<?=EXTERNAL_PATH?>images/event_list.png"
							style="padding-left: 10px;"> <span class='hidden-minibar'>Payment
								history</span>
					</a>
					</li>
				</ul></li>  -->

			<li class='left_navigation <?php  echo ($this->uri->segment(2)=='contact_us')?"active":""; ?>'>
			 <a href='<?php echo SITEURL."page/contact_us";  ?>'
				data-original-title='Contact Us'>
					<div class="left_nav_menu_img">
						<i class="fa fa-phone-square" style="margin: 12px 10px 0 9px"></i>
					</div> <span class='hidden-minibar'><?=$this->lang->line('contact_us')?></span>
			</a></li>

			<li class='left_navigation'><a href='<?php echo SITEURL."logout"; ?>'>
					<div class="left_nav_menu_img">
						<i class="fa fa-lock" style="margin: 12px 10px 0 9px"></i>
					</div> <span class='hidden-minibar'><?=$this->lang->line('logout')?></span>
			</a></li>


	<?php } else if($this->pidGroup == 3) {?>
	   
	   	           <li class='left_navigation'><a class='dropdown'
				href='javascript:void(0);' data-original-title='Update Profile'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/edit_img.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'><?=$this->lang->line('update_profile')?></span>
			</a>
				<ul>
					<li
						class=' <?php echo ($this->uri->segment(1)==$this->myvalues->profileDetails['controller'] && $this->uri->segment(2)=='')?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->profileDetails['controller']; ?>'
						data-original-title='Over View'> <img
							src="<?=EXTERNAL_PATH?>images/overview_img.png"
							style="padding-left: 10px;"> <span class='hidden-minibar'><?=$this->lang->line('overview')?></span>
					</a>
					</li>
					<li
						class=' <?php echo ($this->uri->segment(1)==$this->myvalues->profileDetails['controller'] && $this->uri->segment(2)=='edit')?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->profileDetails['controller']."/edit"; ?>'
						data-original-title='Personal Info'> <img
							src="<?=EXTERNAL_PATH?>images/personal_info_img.png"
							style="padding-left: 10px;"> <span class='hidden-minibar'><?=$this->lang->line('profile')?></span>
					</a>
					</li>

					<li
						class='<?php echo ($this->uri->segment(1)== $this->myvalues->change_passwordDetails['controller'])?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->change_passwordDetails['controller']; ?>'
						data-original-title='Change Password'> <img
							src="<?=EXTERNAL_PATH?>images/lock.png"
							style="padding-left: 10px;"> <span class='hidden-minibar'><?=$this->lang->line('change_password')?></span>
					</a>
					</li>
					<?php
					if($this->pidGroup == PERFOMER_GROUP){
					    
					if($tipDetails->stripe_account_id != '' && $tipDetails->stripe_status == 'Verified') { ?>
					<li
						class='<?php echo ($this->uri->segment(1)== $this->myvalues->tippingDetails['controller'] && $this->uri->segment(2)== 'paymentOptions')?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->tippingDetails ['controller'].'/paymentOptions'; ?>'
						data-original-title='Change Password'> <img
							src="<?=EXTERNAL_PATH?>images/lock.png"
							style="padding-left: 10px;"> <span class='hidden-minibar'>Manage Bank/Cards</span>
					</a>
					</li>
                  		<?php
					}
                    }
    /*
     * ?>
     * <li class=' ' >
     * <a href='#' data-original-title='Privacy Settings'>
     * <img src="<?php echo SITEURL."assets/"; ?>images/privacy.png" style="padding-left: 10px;">
     * <span class='hidden-minibar'> Privacy Settings</span>
     * </a>
     * </li>
     */
    ?>
                  	
                  	</ul></li>

			<li
				class='left_navigation <?php echo ($this->uri->segment(1)== $this->myvalues->performerDetails["controller"]  && $this->uri->segment(2)=='shows')?"active":""; ?>'>
				<a
				href='<?php echo SITEURL . $this->myvalues->performerDetails["controller"]."/shows"; ?>'
				data-original-title='My Shows'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/event_icon.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'><?=$this->lang->line('my_shows')?> <span
						class="badge bg-success performerEventsUpdates"></span><span
						class="badge bg-success pending_time_reservation"></span></span>
			</a>
			</li>
			
			<li
				class='left_navigation <?php echo ($this->uri->segment(1)== $this->myvalues->pasteventsDetails["controller"]  && $this->uri->segment(2)=='shows')?"active":""; ?>'>
				<a
				href='<?php echo SITEURL . $this->myvalues->pasteventsDetails["controller"]."/shows"; ?>'
				data-original-title='My Shows'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/event_icon.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'>Past shows<span
						class="badge bg-success performerEventsUpdates"></span><span
						class="badge bg-success pending_time_reservation"></span></span>
			</a>
			</li>

			<li
				class='left_navigation <?php echo ($this->uri->segment(1)== $this->myvalues->invitationDetails['controller'] && $this->uri->segment(2)=='received')?"active":""; ?>'>
				<a
				href='<?php echo SITEURL.$this->myvalues->invitationDetails['controller']."/received"; ?>'
				data-original-title='Manage Booking Requests'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/hat_grey.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'><?=$this->lang->line('manage_booking_requests')?> </span>
			</a>
			</li>

			<li
				class='left_navigation <?php echo ($this->uri->segment(1)== $this->myvalues->invitationDetails['controller'] && $this->uri->segment(2)=='sent')?"active":""; ?>'>
				<a
				href='<?php echo SITEURL.$this->myvalues->invitationDetails['controller']."/sent"; ?>'
				data-original-title='Apply'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/cane2.png"
							style="padding: 7px 0px 0px 8px; width: 33px;">
					</div> <span class='hidden-minibar'> <?=$this->lang->line('awaiting_approval_from_venue')?> </span>
			</a>
			</li>

			<li
				class='left_navigation <?php echo ($this->uri->segment(1)== $this->myvalues->tradingDetails["controller"])?"active":""; ?>'>
				<a href='<?php echo SITEURL . $this->myvalues->tradingDetails["controller"]; ?>' data-original-title='Trading Requests'>
					<div class="left_nav_menu_img">
						<i class="fa fa-refresh" style="margin: 12px 10px 0 9px"></i>
					</div> <span class='hidden-minibar'><?=$this->lang->line('trading_requests')?> <span
						class="badge bg-success notificationCountTrading"></span></span>
			</a>
			</li>

			<li
				class='left_navigation <?php echo ($this->uri->segment(1)==$this->myvalues->hostDetails['controller'] && $this->uri->segment(2)=='hostReceived')?"active":""; ?>'>
				<a
				href='<?php echo SITEURL.$this->myvalues->hostDetails['controller']."/hostReceived"; ?>'
				data-original-title='Host Invites'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/microphone2_grey.png"
							style="padding: 7px 0px 0px 15px;">
					</div> <span class='hidden-minibar'><?=$this->lang->line('hosting_invitations')?> <span
						class="badge bg-success hostInviteCount"></span></span>
			</a>
			</li>

			<li
				class='left_navigation <?php echo ($this->uri->segment(1)==$this->myvalues->invitationDetails['controller'] && $this->uri->segment(2)=='event_line_ups')?"active":""; ?>'>
				<a
				href='<?php echo SITEURL.$this->myvalues->invitationDetails['controller']."/event_line_ups";  ?>'
				data-original-title='Host Invites'>
					<div class="left_nav_menu_img">
						<i class="fa fa-list" style="margin: 12px 10px 0 9px"></i>
					</div> <span class='hidden-minibar'> <?=$this->lang->line('event_line_ups')?> <span
						class="badge bg-success hostEventLineUpNotice"></span></span>
			</a>
			</li>

			<li
				class='left_navigation <?php echo ($this->uri->segment(1)==$this->myvalues->eventDetails['controller'] && $this->uri->segment(2)=='search')?"active":""; ?>'>
				<a
				href='<?php echo SITEURL.$this->myvalues->eventDetails['controller']."/search"; ?>'
				data-original-title='Search for Events'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/search_img.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'><?=$this->lang->line('search_for_shows')?>  </span>
			</a>
			</li>

			<li
				class='left_navigation <?php echo ($this->uri->segment(1)==$this->myvalues->biteDetails['controller'] && $this->uri->segment(2)=='bitePerformer')?"active":""; ?>'>
				<a
				href='<?php echo SITEURL.$this->myvalues->biteDetails['controller']."/bitePerformer"; ?>'
				data-original-title='Search for New Users To Bite'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/search_img.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'><?=$this->lang->line('bite_performers')?></span>
			</a>
			</li>

			<!-- <li
				class='left_navigation <?php echo ($this->uri->segment(1)== $this->myvalues->biteDetails['controller'] && $this->uri->segment(2)=='')?"active":""; ?>'>
				<a
				href='<?php echo SITEURL.$this->myvalues->biteDetails['controller']; ?>'
				data-original-title='View Bites'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/overview_img.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'><?=$this->lang->line('view_bites')?></span>
			</a>
			</li> -->
			
			
			<!-- For bite functioanlity start-->
			
			<li class='submenu  left_navigation'><a class='dropdown'
				href='javascript:void(0);' data-original-title=''>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/overview_img.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'><?=$this->lang->line('bites')?><span
						class="badge bg-success"></span></span>
			</a>
				<ul>
					<li
						class='<?php echo ($this->uri->segment(1)==$this->myvalues->biteDetails['controller'] && $this->uri->segment(2)=='')?"active":""; ?>'>
						<a
						href='<?=SITEURL.$this->myvalues->biteDetails['controller']?>'
						data-original-title='<?=$this->lang->line('view_bites')?>'> <img
							src="<?=EXTERNAL_PATH?>images/overview_img.png"
							style="padding-left: 10px;">
							<span class='hidden-minibar'><?=$this->lang->line('view_bites')?><span
								class="badge bg-success "></span></span>
					</a>
					</li>
                     
                     <?php 
                    $this->load->model($this->myvalues->subscriptionDetails ['model'], 'subscription_model');
                    $resultPlanID = $this->subscription_model->getCurrentPlan($this->pUserId, $this->pidGroup);
                    
                    // not display the bitten me for beginner plan
                    if ($resultPlanID != 1){
                    ?>   
					
					<li
						class=' <?php echo ($this->uri->segment(1)== $this->myvalues->biteDetails['controller'] && $this->uri->segment(2)=='bittenMe')?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->biteDetails['controller']."/bittenMe"; ?>'
						data-original-title='<?=$this->lang->line('bite_to_me')?>'> <img
							src="<?=EXTERNAL_PATH?>images/overview_img.png"
							style="padding-left: 10px;">
							<span class='hidden-minibar'><?=$this->lang->line('bite_to_me')?></span>
					</a>
					</li>
					<?php }?>
				</ul></li>
			
			
			<!-- For bite functioanlity END-->
			
			<!-- Not display the subcription start -->
			<!-- <li class='submenu  left_navigation'><a class='dropdown'
				href='javascript:void(0);' data-original-title=''>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/event_icon.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'>Subscription</span>
			</a>
				<ul>

					<li
						class=' <?php echo ( $this->uri->segment(1) == $this->myvalues->subscriptionDetails['controller']  && $this->uri->segment(2)=='')?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->subscriptionDetails['controller']; ?>'
						data-original-title='Subscribe Plan'> <img
							src="<?=EXTERNAL_PATH?>images/event_create.png"
							style="padding-left: 10px;"> <span class='hidden-minibar'>Subscribe
								plan</span>
					</a>
					</li>


					<li
						class=' <?php echo ($this->uri->segment(1)==$this->myvalues->subscriptionDetails['controller'] && $this->uri->segment(2)=='history' )?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->subscriptionDetails['controller']."/history"; ?>'
						data-original-title='Payment History'> <img
							src="<?=EXTERNAL_PATH?>images/event_list.png"
							style="padding-left: 10px;"> <span class='hidden-minibar'>Payment
								history</span>
					</a>
					</li>
				</ul></li> -->
			<!-- Not display the subcription end -->

			<li class='left_navigation <?php echo ($this->uri->segment(2)=='contact_us')?"active":""; ?>'><a href='<?php echo SITEURL."page/contact_us"; ?>'
				data-original-title='Contact Us'>
					<div class="left_nav_menu_img">
						<i class="fa fa-phone-square" style="margin: 12px 10px 0 9px"></i>
					</div> <span class='hidden-minibar'> <?=$this->lang->line('contact_us')?></span>
			</a></li>

			<li class='left_navigation'><a href='<?php echo SITEURL."logout"; ?>'>
					<div class="left_nav_menu_img">
						<i class="fa fa-lock" style="margin: 12px 10px 0 9px"></i>
					</div> <span class='hidden-minibar'><?=$this->lang->line('logout')?></span>
			</a></li>
	
	<?php } else if($this->pidGroup == 4) { ?>
	       
	   	           <li class='left_navigation'><a class='dropdown'
				href='javascript:void(0);' data-original-title='Update Profile'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/edit_img.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'><?=$this->lang->line('update_profile')?></span>
			</a>
				<ul>
					<li
						class=' <?php echo ($this->uri->segment(1)==$this->myvalues->profileDetails['controller'] && $this->uri->segment(2)=='')?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->profileDetails['controller']; ?>'
						data-original-title='Over View'> <img
							src="<?=EXTERNAL_PATH?>images/overview_img.png"
							style="padding-left: 10px;"> <span class='hidden-minibar'><?=$this->lang->line('overview')?></span>
					</a>
					</li>
					<li
						class=' <?php echo ($this->uri->segment(1)==$this->myvalues->profileDetails['controller'] && $this->uri->segment(2)=='edit')?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->profileDetails['controller']."/edit"; ?>'
						data-original-title='Personal Info'> <img
							src="<?=EXTERNAL_PATH?>images/personal_info_img.png"
							style="padding-left: 10px;"> <span class='hidden-minibar'><?=$this->lang->line('profile')?></span>
					</a>
					</li>


					<li
						class=' <?php echo ( $this->uri->segment(1)==$this->myvalues->change_passwordDetails['controller'] && $this->uri->segment(2)=='')?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->change_passwordDetails['controller']; ?>'
						data-original-title='Change Password'> <img
							src="<?=EXTERNAL_PATH?>images/lock.png"
							style="padding-left: 10px;"> <span class='hidden-minibar'><?=$this->lang->line('change_password')?></span>
					</a>
					</li>
					<li
						class=' <?php echo ( $this->uri->segment(1)==$this->myvalues->tippingDetails['controller'] && $this->uri->segment(2)=='enabledrecurringtip')?"active":""; ?>'>
						<a
						href='<?php echo SITEURL.$this->myvalues->tippingDetails['controller']."/enabledrecurringtip"; ?>'
						data-original-title='Recurring tip'> <img
							src="<?=EXTERNAL_PATH?>images/event_icon.png"
							style="padding-left: 10px;"> <span class='hidden-minibar'><?=$this->lang->line('recurring_tip')?></span>
					</a>
					</li>
                  		<?php 
           /*
           * ?>
           * <li class=' ' >
           * <a href='#' data-original-title='Privacy Settings'>
           * <img src="<?php echo SITEURL."assets/"; ?>images/privacy.png" style="padding-left: 10px;">
           * <span class='hidden-minibar'> Privacy Settings</span>
           * </a>
           * </li>
           */
    ?>
                  	
                  	</ul></li>
                  	
                <li
				class='left_navigation <?php echo ($this->uri->segment(1)==$this->myvalues->eventDetails["controller"] && $this->uri->segment(2)=='upcoming')?"active":""; ?>'>
				<a
				href='<?php echo SITEURL.$this->myvalues->eventDetails['controller']."/upcoming"; ?>'
				data-original-title='Search for New Users To Bite'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/search_img.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'>Upcoming Shows</span>
			</a>
			</li>

			<!--<li
				class='left_navigation <?php echo ($this->uri->segment(1)==$this->myvalues->fanDetails["controller"] && $this->uri->segment(2)=='performerStream')?"active":""; ?>'>
				<a
				href='<?php echo SITEURL . $this->myvalues->fanDetails["controller"]."/performerStream"; ?>'
				data-original-title='My Shows'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/event_icon.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'>My performer stream<span
						class="badge bg-success performerEventsUpdates"></span><span
						class="badge bg-success pending_time_reservation"></span></span>
			</a>
			</li>

			<li
				class='left_navigation <?php echo ($this->uri->segment(1)==$this->myvalues->fanDetails["controller"] && $this->uri->segment(2)=='venueStream')?"active":""; ?>'>
				<a
				href='<?php echo SITEURL . $this->myvalues->fanDetails["controller"]."/venueStream"; ?>'
				data-original-title='My Shows'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/event_icon.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'>My venue stream<span
						class="badge bg-success performerEventsUpdates"></span><span
						class="badge bg-success pending_time_reservation"></span></span>
			</a>
			</li>-->
			
			<li
				class='left_navigation <?php echo ($this->uri->segment(1)==$this->myvalues->fanDetails["controller"] && $this->uri->segment(2)=='myStream')?"active":""; ?>'>
				<a
				href='<?php echo SITEURL . $this->myvalues->fanDetails["controller"]."/myStream"; ?>'
				data-original-title='My Shows'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/event_icon.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'>My stream<span
						class="badge bg-success performerEventsUpdates"></span><span
						class="badge bg-success pending_time_reservation"></span></span>
			</a>
			</li>

			<li
				class='left_navigation <?php echo ($this->uri->segment(1)==$this->myvalues->biteDetails["controller"] && $this->uri->segment(2)=='bitePerformer')?"active":""; ?>'>
				<a
				href='<?php echo SITEURL.$this->myvalues->biteDetails['controller']."/bitePerformer"; ?>'
				data-original-title='Search for New Users To Bite'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/search_img.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'><?=$this->lang->line('bite_performers')?></span>
			</a>
			</li>

			<li
				class='left_navigation <?php echo ($this->uri->segment(1)==$this->myvalues->eventDetails['controller'] && $this->uri->segment(2)=='search')?"active":""; ?>'>
				<a
				href='<?php echo  SITEURL.$this->myvalues->eventDetails['controller']."/search"; ?>'
				data-original-title='Search for New Users To Bite'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/search_img.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'>discover upcoming shows</span>
			</a>
			</li>
						
			<li
				class='left_navigation <?php echo ($this->uri->segment(1)==$this->myvalues->tippingDetails ['controller'] && $this->uri->segment(2)=='tipHistory')?"active":""; ?>'>
				<a
				href='<?php echo SITEURL . $this->myvalues->tippingDetails ['controller']."/tipHistory"; ?>'
				data-original-title='My Shows'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/event_icon.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'><?php echo $this->lang->line('tip_history');?><span
						class="badge bg-success performerEventsUpdates"></span><span
						class="badge bg-success pending_time_reservation"></span></span>
			</a>
			</li>

			
			<li
				class='left_navigation <?php echo ($this->uri->segment(1)== $this->myvalues->biteDetails['controller'] && $this->uri->segment(2)=='')?"active":""; ?>'>
				<a
				href='<?php echo SITEURL.$this->myvalues->biteDetails['controller']; ?>'
				data-original-title='View Bites'>
					<div class="left_nav_menu_img">
						<img src="<?=EXTERNAL_PATH?>images/overview_img.png"
							style="padding: 10px 0px 0px 10px;">
					</div> <span class='hidden-minibar'>view bites</span>
			</a>
			</li>

			<li class='left_navigation <?php echo ($this->uri->segment(2)=='contact_us')?"active":""; ?>'><a href='<?php echo SITEURL."page/contact_us"; ?>'
				data-original-title='Contact Us'>
					<div class="left_nav_menu_img">
						<i class="fa fa-phone-square" style="margin: 12px 10px 0 9px"></i>
					</div> <span class='hidden-minibar'> <?=$this->lang->line('contact_us')?></span>
			</a></li>
			<li class='left_navigation'><a href='<?php echo SITEURL."logout"; ?>'>
					<div class="left_nav_menu_img">
						<i class="fa fa-lock" style="margin: 12px 10px 0 9px"></i>
					</div> <span class='hidden-minibar'><?=$this->lang->line('logout')?></span>
			</a></li>
	<?php
}
else if($this->pidGroup == ADMIN_GROUP){
?>
<li class=' <?php echo ($this->uri->segment(2)=='dashboard')?"active":""; ?>' >
						<a href='<?php echo SITEURL_ADMIN . "dashboard"; ?>' data-original-title='Dashboard'>
							<i class="fa fa-dashboard"></i>
							<span class='hidden-minibar'> Dashboard</span>
						</a>
					</li>
					
					<li class=' <?php echo ($this->uri->segment(2)=='performers')?"active":""; ?>' >
						<a href='<?php echo SITEURL_ADMIN?>performers' data-original-title='Performers'>
							<i class="fa fa-user"></i>
							<span class='hidden-minibar'>Performers</span>
						</a>
					</li>
					
					<li class=' <?php echo ($this->uri->segment(2)=='venues')?"active":""; ?>' >
						<a href='<?php echo SITEURL_ADMIN?>venues' data-original-title='Venues'>
							<i class="fa fa-user"></i>
							<span class='hidden-minibar'> Venues</span>
						</a>
					</li>
					
					<li class=' <?php echo ($this->uri->segment(2)=='fans')?"active":""; ?>' >
						<a href='<?php echo SITEURL_ADMIN?>fans' data-original-title='Venues'>
							<i class="fa fa-user"></i>
							<span class='hidden-minibar'>Fans</span>
						</a>
					</li>
					
					<li class=' <?php echo ($this->uri->segment(2)=='events')?"active":""; ?>' >
						<a href='<?php echo SITEURL_ADMIN?>events' data-original-title='Events'>
							<i class="fa fa-briefcase"></i>
							<span class='hidden-minibar'> Events</span>
						</a>
					</li>
						<li class=' <?php echo ($this->uri->segment(2)=='transaction_details')?"active":""; ?>' >
						<a href='<?php echo SITEURL_ADMIN.$this->myvalues->adminTransactionDetails['controller']?>' data-original-title='Change password'>
								<i class="fa fa-lock"></i>
							<span class='hidden-minibar'>Transaction Details</span>
						</a>
					</li>
					  <li class=' <?php echo ($this->uri->segment(1)=='change_password')?"active":""; ?>' >
						<a href='<?php echo SITEURL.$this->myvalues->change_passwordDetails['controller']?>' data-original-title='Change password'>
								<i class="fa fa-lock"></i>
							<span class='hidden-minibar'> Change Password</span>
						</a>
					</li>
						<li>
						<a href='<?php echo SITEURL?>logout' data-original-title='Log out'>
							<i class="fa fa-lock"></i>
							<span class='hidden-minibar'> Logout</span>
						</a>
					</li>
<?php } ?>
	

       </ul>
	</div>
</div>