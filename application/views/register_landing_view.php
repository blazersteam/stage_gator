<link href="<?=EXTERNAL_PATH?>css/custom_style.css" rel="stylesheet">
<div id="user-type">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<a href="<?=SITEURL?>"> <img src="<?=EXTERNAL_PATH?>images/logo_local.png" alt="logo" />
				</a>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<!-- forms part-->
	<div class="container">
    	<div class="row frmarea action">
			<div class="col-sm-6 col-lg-4 col-sm-offset-3 col-lg-offset-4">
				<div class="form-group input-group-lg">
					<a href="<?=SITEURL.$addUserController.'/add/performer'?>"
						class="form-control btn btn-sin" id="register"><?=$this->lang->line('i_m_new_performer')?></a>
				</div>
				<div class="form-group input-group-lg">
					<a href="<?=SITEURL.$addUserController.'/add/venue'?>"
						class="form-control btn btn-sin" id="register"><?=$this->lang->line('i_m_new_venue')?></a>
				</div>
				<div class="form-group input-group-lg">
					<a href="<?=SITEURL.$addUserController.'/add/fan'?>"
						class="form-control btn btn-sin" id="register"><?=$this->lang->line('i_m_new_fan')?></a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- forms part end-->

</div>
