<div class="content" style="height: 100%;">
<?php  echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<?php if($this->pidGroup == VENUE_GROUP || $this->pidGroup == PERFOMER_GROUP || ($this->pidGroup == FAN_GROUP && empty($mobileDevice))){ ?>
		<div class="page_header"> <?=$title?> </div>
  		<div class = "visible-lg visiblityShow">
    		<table class="table table-bordered table-striped table-hover">
    			<tr>
    				<th style="width: 25%;">Venue Title:</th>
    				<td><?php echo ucwords($this->utility->decodeText($result->chrName)); ?></td>
    			</tr>
    			<tr>
    				<th style="width: 25%;">Show Title:</th>
    				<td><?php echo ucwords($this->utility->decodeText($result->title)); ?></td>
    			</tr>
    			<tr>
    				<th style="width: 25%;">Show City:</th>
    				<td><?php echo empty(@$location->city)?ucwords($this->utility->decodeText(@$record->mem_city)):ucwords($this->utility->decodeText(@$location->city)); ?>, <?php echo empty(@$location->state)?ucwords($this->utility->decodeText(@$record->mem_state)):ucwords($this->utility->decodeText(@$location->state)); ?></td>
    			</tr>
    			<tr>
    				<th>Show Address:</th>
    				<td><?php echo $this->utility->decodeText(@$location->address); ?></td>
    			</tr>
    			<tr>
    				<th>Number of Seats:</th>
    				<td><?php echo @$location->numSeats; ?></td>
    			</tr>
    			<tr>
    				<th>Show Type:</th>
    				<td><?php echo ucwords($this->utility->decodeText($result->type)); ?></td>
    			</tr>
    
    			<tr>
    				<th>Show Date:</th>
    				<td><?php echo dateDisplay($result->start_date . " " . $result->startTime,'m/d/Y'); ?></td>
    			</tr>
    
    			<tr>
    				<th>Start Time:</th>
    				<td><?php echo  dateDisplay($result->start_date . " " . $result->startTime,'h:i A'); ?></td>
    			</tr>
    
    			<tr>
    				<th>End Time:</th>
    				<td><?php echo dateDisplay($result->end_date . " " . $result->endTime,'h:i A');?></td>
    			</tr>
    
    			<?php if($this->pUserId!=0){ ?>
        			<tr>
        				<th>Is Recurring?</th>
                              <?php
                                switch ($result->isRecuring) {
                                    
                                    case 1:
                                        $res = "One time";
                                        break;
                                    
                                    case 2:
                                        $res = "Daily";
                                        break;
                                    
                                    case 3:
                                        $res = "Weekly";
                                        break;
                                    
                                    case 4:
                                        $res = "Monthly";
                                        break;
                                    
                                    default:
                                        $res = "NO";
                                }
                              ?>
        		        <td><?php echo $res; ?></td>
        			</tr>
        			<tr>
        				<th>Cut-Off Time:</th>
        				<td><?php echo $result->hoursLockBefore; ?> Hours Before Starting</td>
        			</tr>
        			<tr>
        				<th>Allow Walk-In?</th>
        				<td><?php echo ($result->allowWalkInReal==1)?"Yes":"No"; ?></td>
        			</tr>
        
        			<tr>
        				<th>Performer Can Pick Time?</th>
        				<td><?php echo ($result->performerCanPickTime==1)?"Yes":"No"; ?></td>
        			</tr>
        	
                	<?php if (!$result->performerCanPickTime){ ?>
                		<tr>
        				<th>Scheduling Algo</th>
        				<td><?php echo $result->nameAlgo; ?></td>
        			</tr>
                	<?php } ?>
        	
        	       <tr>
        				<th>Have Host?</th>
        				<td><?php echo ($result->haveHost==1)?"Yes":"No"; ?></td>
        			</tr>
        
        			<tr>
        				<th>Allow Trading?</th>
        				<td><?php echo ($result->allowTrading==1)?"Yes":"No"; ?></td>
        			</tr>
        			
        	   <?php } ?>
    			
    			<tr>
    				<th>Time Per Performance</th>
    				<td><?php echo $result->timePerPerformance; ?> Minutes</td>
    			</tr>
    
    			<tr>
    				<th>Show Description</th>
    				<td><?php echo $this->utility->decodeText($result->descr,false); ?></td>
    			</tr>
    		</table>
		</div>
		<button type="submit" class="btn btn-success visible-xs" id="btnViewMore">Show More</button>
		<?php } ?>
		<div class="alert alert-success text-center myMessage tipSuccessMessage alert-dismissable" style="display: none;"><a href="javascript:void(0)" class="close alert-hide" data-dismiss="alert" aria-label="close">&times;</a><span class="retrunmessage"></span></div>
		<div id="performerlist" class="performerlist">
			<?php if (count($performerlist) > 0){ ?>

        		<div class="page_header">Performer List</div>
        		<div id="no-more-tables">
        		<table id="list" class="table table-hover table-bordered">
        			<thead class="cf">
        			<tr>
        				<th class="visible-lg">Sr #</th>
        				<th>Start Time</th>
        				<th>End Time</th>
        				<th>Performer</th>				
        				<?php 
        				/* on open th where logged in user group is a fan */
        				if($this->pidGroup == FAN_GROUP || $this->pidGroup == VENUE_GROUP || $this->pidGroup == PERFOMER_GROUP) { ?>
        				<th>Action</th>
        				<?php } ?>
        				<th>Status</th>
        			</tr>
        			</thead>
        	
        	<?php
            foreach ($performerlist as $k => $l) {
                    if($l->status == 'onstage'){
                        $bgcolor = 'style="background-color:#90EE90;"';   
                    }else if($l->status == 'performed'){
                        $bgcolor = 'style="background-color:#D3D3D3;"';
                    }else{
                        $bgcolor = '';
                    }
                ?>
        				<tr <?php echo $bgcolor; ?>>
        				<td data-title="Sr #" class="visible-lg"><?php echo $l->slot_id; ?></td>
        				<td data-title="Start Time"><?php echo dateDisplay($l->start_date.' '.$l->start_time,'h:i A'); ?></td>
        				<td data-title="End Time"><?php echo dateDisplay($l->end_date.' '.$l->end_time,'h:i A'); ?></td>
        				<td data-title="Performer">	
            			<?php
                if ($l->idPerformer == 0) {
                    echo "";
                }
                else if ($l->idPerformer == - 1) {
                    echo "Intermission";
                }
                else {
                    echo ucwords($this->utility->decodeText($l->chrName));
                }
                ?>
                        </td>
                        <?php
                        $datauserid = "";
                        /* show data for action th if user logged in as performer */
                         if($this->pidGroup == FAN_GROUP ||  $this->pidGroup == VENUE_GROUP || $this->pidGroup == PERFOMER_GROUP ) { ?>
                    <td data-title="Action"><div><?php  if ($this->pidGroup == 4 && (!empty($l->stripe_account_id) && $l->stripe_status === 'Verified')) {
                            $datauserid = 'data-userid="'.$l->user_id.'" data-account-exit="1"';
                        ?>
                        <a class="btn btn-success tipper_popup" data-userid=<?php echo $this->utility->encode($l->user_id);?> href="javascript:void(0);" title="Tip here" id="tipper_popup_<?php echo $l->user_id; ?>"><!-- <i class="fa fa-usd"></i> --> Tip</a>
                        
                    <?php
                    }
                    
                          
                    /* this condition check if user has subscribed with any plan */
                     /*  if (in_array($l->plan_id, $GLOBALS ["allowedBitePlans"])) { */
        
                    // allowed venue , performer & fan to bite also satisfy the plan conditions.
                    if (in_array($l->plan_id, $GLOBALS ["allowedBitePlans"]) && $l->idPerformer && $l->idPerformer!=-1 && $l->idPerformer!=0 && $l->idPerformer!= $this->pUserId){
                         /* check if user did not bitten or not and give button according to it */
                        if ($l->followerId != $this->pUserId) {
                            // if(fan_model::checkIfFollowing($User->user_id, $r->user_id)){                     ?>
                       			<a href="javascript:void(0)"
        							data-following-id="<?php echo $this->utility->encode($l->user_id);?>" <?php echo $datauserid; ?> 
        							class="btn btn-sm btn-success toggleBiteUnbite">Bite</a>
                       			<?php }else{ ?>				
        				
        						
        						<a href="javascript:void(0)"
        							data-following-id="<?php echo $this->utility->encode($l->user_id);?>" <?php echo $datauserid; ?> 
        							class="btn btn-sm btn-warning toggleBiteUnbite">Unbite</a>
                       			<?php
                       			}
                        } ?></div>
                        
                       <?php if($this->pidGroup == VENUE_GROUP){ ?>
                    		<?php if($l->status == 'onstage' || $l->status == 'upcoming'){ ?>
        						<a href="<?php echo SITEURL. $timetableController. "/changeStatus/". $this->utility->encode($l->idEvent) ."/". $this->utility->encode($l->slot_id)."/". $this->utility->encode($l->table_id)."/". $this->utility->encode('performed') . "/". $controllerName."/".$accssToken; ?>" class="btn btn-sm btn-primary" onclick="return perfomer_action('performed');">Performed</a>
        					<?php } ?>
    			   		<?php } ?>
                    </td>
                    <?php  } ?>
                    <td data-title="Status"><?php echo ucfirst($l->status); ?></td>
        			</tr>
        			<?php
            }
            ?>
        </table>
        </div>
        <?php } else { ?>
        	<div class="alert alert-danger">Performer details not available right now!</div>
        <?php } ?>
      </div>
      <?php if($this->pidGroup == FAN_GROUP && !empty($mobileDevice)){ ?>
	  <div class="page_header"> <?=$title?>	 </div>
      <div class = "visible-lg visiblityShow">
		<table class="table table-bordered table-striped table-hover">
			<tr>
				<th style="width: 25%;">Venue Title:</th>
				<td><?php echo ucwords($this->utility->decodeText($result->chrName)); ?></td>
			</tr>
			<tr>
				<th style="width: 25%;">Show Title:</th>
				<td><?php echo ucwords($this->utility->decodeText($result->title)); ?></td>
			</tr>
			<tr>
				<th style="width: 25%;">Show City:</th>
				<td><?php echo empty(@$location->city)?ucwords($this->utility->decodeText(@$record->mem_city)):ucwords($this->utility->decodeText(@$location->city)); ?>, <?php echo empty(@$location->state)?ucwords($this->utility->decodeText(@$record->mem_state)):ucwords($this->utility->decodeText(@$location->state)); ?></td>
			</tr>
			<tr>
				<th>Show Address:</th>
				<td><?php echo $this->utility->decodeText(@$location->address); ?></td>
			</tr>
			<tr>
				<th>Number of Seats:</th>
				<td><?php echo @$location->numSeats; ?></td>
			</tr>
			<tr>
				<th>Show Type:</th>
				<td><?php echo ucwords($this->utility->decodeText($result->type)); ?></td>
			</tr>

			<tr>
				<th>Show Date:</th>
				<td><?php echo dateDisplay($result->start_date . " " . $result->startTime,'m/d/Y'); ?></td>
			</tr>

			<tr>
				<th>Start Time:</th>
				<td><?php echo  dateDisplay($result->start_date . " " . $result->startTime,'h:i A'); ?></td>
			</tr>

			<tr>
				<th>End Time:</th>
				<td><?php echo dateDisplay($result->end_date . " " . $result->endTime,'h:i A');?></td>
			</tr>

			<?php if($this->pUserId!=0){ ?>
    			<tr>
    				<th>Is Recurring?</th>
                          <?php
                            switch ($result->isRecuring) {
                                
                                case 1:
                                    $res = "One time";
                                    break;
                                
                                case 2:
                                    $res = "Daily";
                                    break;
                                
                                case 3:
                                    $res = "Weekly";
                                    break;
                                
                                case 4:
                                    $res = "Monthly";
                                    break;
                                
                                default:
                                    $res = "NO";
                            }
                          ?>
    		        <td><?php echo $res; ?></td>
    			</tr>
    			<tr>
    				<th>Cut-Off Time:</th>
    				<td><?php echo $result->hoursLockBefore; ?> Hours Before Starting</td>
    			</tr>
    			<tr>
    				<th>Allow Walk-In?</th>
    				<td><?php echo ($result->allowWalkInReal==1)?"Yes":"No"; ?></td>
    			</tr>
    
    			<tr>
    				<th>Performer Can Pick Time?</th>
    				<td><?php echo ($result->performerCanPickTime==1)?"Yes":"No"; ?></td>
    			</tr>
    	
            	<?php if (!$result->performerCanPickTime){ ?>
            		<tr>
    				<th>Scheduling Algo</th>
    				<td><?php echo $result->nameAlgo; ?></td>
    			</tr>
            	<?php } ?>
    	
    	       <tr>
    				<th>Have Host?</th>
    				<td><?php echo ($result->haveHost==1)?"Yes":"No"; ?></td>
    			</tr>
    
    			<tr>
    				<th>Allow Trading?</th>
    				<td><?php echo ($result->allowTrading==1)?"Yes":"No"; ?></td>
    			</tr>
    			
    	   <?php } ?>
			
			<tr>
				<th>Time Per Performance</th>
				<td><?php echo $result->timePerPerformance; ?> Minutes</td>
			</tr>

			<tr>
				<th>Show Description</th>
				<td><?php echo $this->utility->decodeText($result->descr,false); ?></td>
			</tr>
		</table>
	</div>
	<button type="submit" class="btn btn-success visible-xs" id="btnViewMore">Show More</button>
	<?php } ?>
</div>
<?php if(!event_end_time_crossed($result->end_date,$result->endTime)) {?>
<div class="text-center social-btn eventschedule-sharebtn">
<?php if($this->pidGroup == 3){
    $dataText = "I'm booked! Link contains details! Hope to see you there!";
}
else{
    $dataText = "Come check out "."'".urlencode($this->utility->decodeText($result->title))."'".' on '."'".dateDisplay($result->start_date . " " . $result->startTime,'jS M Y h:i A')."'";
}
$dataText1 = "Come check out "."'".$this->utility->decodeText($result->title)."'".' on '."'".dateDisplay($result->start_date . " " . $result->startTime,'jS M Y h:i A')."'";
?>
<?php  
$this->utility->fbShareButton($tag['og:image'],$dataText,urlencode($tag['og:description']),urlencode($tag['og:url']));
?>
<a href="https://twitter.com/home?status=<?=$dataText1?>" class="twitter-share-button" data-url="<?=$this->utility->generateOverviewUrl($result->idEvent,$result->table_id)?>" data-text="<?=$dataText1?>" data-count="none">Tweet</a></div>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

</div>
<?php } ?>

<script>
/* this click event call when website open in mobile screen and show hide the event details page*/
$(document).on('click','#btnViewMore',function(){
	var btnText ;
	/* when click on show less button then we add visible-lg so it will be hide in small screen*/
	if($(this).text() == 'Show Less'){
		/* remove class*/
		$(".visiblityShow").addClass('visible-lg');
    		/* set value Show More after hiding */
    		  btnText = 'Show More';
		}else{
        	/* when click on show more then comes here and remove visible-lg class to show */
        	$(".visiblityShow").removeClass('visible-lg');
        	/* set btnText value show less*/
        	btnText = 'Show Less';
		}
	/* append button text as per condition */
	$(this).text(btnText);
});

function perfomer_action(type){
	if(type == 'onstage'){
		var msg = 'Are you sure you want to mark this performer as "OnStage" ?';
	}
	if(type == 'performed'){
		var msg = 'Are you sure you want to mark this performer as "Performed" ?';
	}
	if(type == 'reset'){
		var msg = 'Are you sure you want to mark this performer as "Back To Stage" ?';
	}
	
	if (confirm(msg)){
		return true;
	} else {
		return false;
	}
}
</script>

<?php if($this->pidGroup == FAN_GROUP) { ?>

<!-- tip dialog model div start -->
<div class="modal fade" id="tipper" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document" id="TipPopup"></div>
</div>

<!-- tip dialog model div start -->
<script>
/*this click event copied from bite_performer_view.php this is used to tip performer */
$('body').on('click','.tipper_popup',function(){
    
	 //disable the button
   $('.tipper_popup').attr('disabled',true);
   var tippingcontroller = '<?php echo $this->myvalues->tippingDetails['controller'];?>';
   var performerUserid = $(this).attr('data-userid');
   var performerName = $(this).parent().parent().siblings("td:nth-child(4)").text();

   $.ajax({
       type:"POST",
       url: SITEURL + tippingcontroller + "/checkStripeIdUser",     	        
       data:{'_csrf' : $('input[name="_csrf"]').val(),'performerUserid' : performerUserid},     	       
       beforeSend: function() { 	 	       
    	  blockUiDisplay();	    	 
    	  $("#TipPopup").html("");
       },
       success:function(data){
    	   $.unblockUI();
	    	   	    	  
	      //enable the button
	       $(".tipper_popup").removeAttr("disabled");
    	   $("#TipPopup").html(data);
    	   $('.mainTital').html('<?php echo 'Give your valuable tip to'; ?> '+ performerName);
	       $("#tipper").modal('show');
	       $("#tipAmount").focus();   
	       //alert('#tipAmount').val('asdas'));
       },
       statusCode: {
           401:function() { window.location.reload(); }
       }
     });
}); 

//For Performer List Table Update Every One Minute
setInterval( function() {
	        //console.log('ajax');
	        var currentRequest = null;   
	        var idEvent = "<?php echo $performerlist[0]->idEvent; ?>";
	        if(idEvent != ""){
	        //console.log(currentRequest);
	        currentRequest = $.ajax({
	            type:"POST",
	            url: SITEURL + "share/eventschedule/"+idEvent,     	        
	            data:{'_csrf' : $('input[name="_csrf"]').val()},
	            beforeSend : function()    {           
	                if(currentRequest != null) {
	                    currentRequest.abort();
	                }
	            },     	       
	            success:function(data){
	         	   	$("#performerlist").html(data);
	            }
	          });
	        }
}, 15000); 

</script>

<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/jquery.blockUI.js"></script>
<?php } ?>
