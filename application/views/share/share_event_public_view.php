
<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>	
					
		</div>

		<table class="table table-bordered table-striped table-hover">
			<tr>
				<th style="width: 25%;">Venue Title:</th>
				<td><?php echo ucwords($this->utility->decodeText($result->chrName)); ?></td>
			</tr>
			<tr>
				<th style="width: 25%;">Show Title:</th>
				<td><?php echo ucwords($this->utility->decodeText($result->title)); ?></td>
			</tr>
			<tr>
				<th style="width: 25%;">Show City:</th>
				<td><?php echo empty($location->city)?ucwords($this->utility->decodeText($record->mem_city)):ucwords($this->utility->decodeText($location->city)); ?>, <?php echo empty($location->state)?ucwords($this->utility->decodeText($record->mem_state)):ucwords($this->utility->decodeText($location->state)); ?></td>
			</tr>
			<tr>
				<th>Show Address:</th>
				<td><?php echo $this->utility->decodeText($location->address); ?></td>
			</tr>
			<tr>
				<th>Number of Seats:</th>
				<td><?php echo $this->utility->decodeText($location->numSeats); ?></td>
			</tr>
			<tr>
				<th>Show Type:</th>
				<td><?php echo ucwords($this->utility->decodeText($result->type)); ?></td>
			</tr>

			<tr>
				<th>Event Start Date:</th>
				<td><?php echo dateDisplay($result->date . " " . $result->startTime,'m/d/Y'); ?></td>
			</tr>

			<tr>
				<th>Event End Date:</th>
				<td><?php echo dateDisplay($result->endDate . " " . $result->endTime,'m/d/Y'); ?></td>
			</tr>
			<tr>
				<th>Start Time:</th>
				<td><?php echo  dateDisplay($result->date . " " . $result->startTime,'h:i A'); ?></td>
			</tr>

			<tr>
				<th>End Time:</th>
				<td><?php echo dateDisplay($result->endDate . " " . $result->endTime,'h:i A');?></td>
			</tr>

			<?php if($this->pUserId!=0){?>
			<tr>
				<th>Is Recurring?</th>
                        		<?php
    switch ($result->isRecuring) {
        
        case 1:
            $res = "One time";
            break;
        
        case 2:
            $res = "Daily";
            break;
        
        case 3:
            $res = "Weekly";
            break;
        
        case 4:
            $res = "Monthly";
            break;
        
        default:
            $res = "NO";
    }
    
    ?>
		        <td><?php echo $res; ?></td>
			</tr>
			<tr>
				<th>Cut-Off Time:</th>
				<td><?php echo $result->hoursLockBefore; ?> Hours Before Starting</td>
			</tr>
			<tr>
				<th>Allow Walk-In?</th>
				<td><?php echo ($result->allowWalkInReal==1)?"Yes":"No"; ?></td>
			</tr>

			<tr>
				<th>Performer Can Pick Time?</th>
				<td><?php echo ($result->performerCanPickTime==1)?"Yes":"No"; ?></td>
			</tr>
	
        	<?php if (!$result->performerCanPickTime){ ?>
        		<tr>
				<th>Scheduling Algo</th>
				<td><?php echo $result->nameAlgo; ?></td>
			</tr>
        	<?php } ?>
	
	       <tr>
				<th>Have Host?</th>
				<td><?php echo ($result->haveHost==1)?"Yes":"No"; ?></td>
			</tr>

			<tr>
				<th>Allow Trading?</th>
				<td><?php echo ($result->allowTrading==1)?"Yes":"No"; ?></td>
			</tr>
			
	<?php }?>
			<tr>
				<th>Time Per Performance</th>
				<td><?php echo $result->timePerPerformance; ?> Minutes</td>
			</tr>

			<tr>
				<th>Event Description</th>
				<td><?php echo $this->utility->decodeText($result->descr,false); ?></td>
			</tr>

		</table>
		
		<?php if (count($eventScheduleList) > 0){ ?>

<div class="page_header">Event Schedule List</div>
		<div id="no-more-tables">
		<table id="list"
			class="table table-hover table-bordered table-striped">
			<thead class="cf">
			<tr>
				<th class="visible-lg">Sr #</th>
				<th>Event Schedule Date</th>
				<th>Start Time</th>
				<th>End Time</th>
				<th>Actions</th>

			</tr>
			</thead>
	<?php
    foreach ($eventScheduleList as $k => $l) {
        ?>
				<tr>
				<td data-title="Sr #" class="visible-lg"><?php echo $k=$k+1; ?></td>
				<td data-title="Schedule Date"><?php echo dateDisplay($l->start_date." ".$result->startTime); ?></td>
				<td data-title="Start Time"><?php echo dateDisplay($l->start_date . " " . $result->startTime,'h:i A');  ?></td>
				<td data-title="End Time"><?php echo dateDisplay($l->end_date . " " . $result->endTime,'h:i A'); ?></td>
				<td data-title="Actions"><a
					href="<?=$this->utility->generateOverviewUrl($l->idEvent,$l->table_id); ?>"
					class="btn btn-sm btn-success">See More</a></td>
			</tr>
			<?php
    }
    ?>
</table>
</div>
<?php } else { ?>
	<div class="alert alert-danger">Event Schedule not available right now!</div>
<?php } ?>
	</div>

 
 <?php 
     if(!event_end_time_crossed($result->endDate,$result->endTime)){
 
 if($this->pidGroup == 3){
    
     $dataText = "I'm booked! Link contains details! Hope to see you there!";
}
else{
    
    $dataText = "Come check out "."'".urlencode($this->utility->decodeText($result->title))."'".' on '."'".dateDisplay($result->date . " " . $result->startTime,'jS M Y h:i A')."'";
    $dataText1 = "Come check out "."'".$this->utility->decodeText($result->title)."'".' on '."'".dateDisplay($result->date . " " . $result->startTime,'jS M Y h:i A')."'";
}
?>
 <div class="text-center social-btn eventschedule-sharebtn">
<?php  
$this->utility->fbShareButton($tag['og:image'],$dataText,urlencode($tag['og:description']),urlencode($tag['og:url']));
?>
<a href="https://twitter.com/home?status=<?=$dataText1?>" class="twitter-share-button" data-url="<?=$this->utility->generateOverviewUrl($result->idEvent)?>" data-text="<?=$dataText1?>" data-count="none">Tweet</a></div>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
</div>
<?php } ?>