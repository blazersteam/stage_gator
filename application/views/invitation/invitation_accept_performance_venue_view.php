<div class="content" style="height: 100%;">

	<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>					
					</div>
	<?php echo $this->session->flashdata('myMessage'); ?>
	<div id="no-more-tables">
<table class="table table-bordered table-striped table-hover dataTable cf">
<thead class="cf">
	<tr>
		<th>Event Title</th>
		<th class="">Date</th>
		<th>Time</th>
		<th>Performer</th>
		<th class="">Date Applied</th>
		<th class="">Status</th>
		<th>Action</th>
	</tr>
</thead>
<tbody>	
	<?php 
		foreach($list as $l){
			if (!event_end_time_crossed($l->end_date, $l->endTime)){
			//if(strtotime($l->date." ".$l->endTime)>time()){
			    $eventController = SITEURL.$this->myvalues->eventDetails['controller'];
			?>
			<tr>
				<td data-title="Event Title"><a href="<?php echo $this->utility->generateOverviewUrl($l->idEvent,$l->table_id); ?>" title="Overview"><?php echo ucwords($l->title); ?></a></td>
				<td data-title="Date"><?php echo dateDisplay($l->start_date.' '.$l->startTime); ?></td>
				<td data-title="Time"><?php echo dateDisplay($l->start_date.' '.$l->startTime,'h:i A'); ?></td>
				
				<td data-title="Performer"><a href="<?php echo SITEURL."view/".$l->url; ?>" title="Overview"><?php echo ucwords($l->chrName); ?></a></td>
				<td data-title="Date Applied" class=""><?php echo dateDisplay(date("Y-m-d H:i:s", $l->dtApplied)); ?></td>	
				<td data-title="Status" class=""> <?php echo $l->status; ?></td>
				<td data-title="Action">
					<div class="text-right">
					<?php if ($l->status=='Cancelled' || $l->status=='Accepted'){ ?>
						<a href="javascript:void(0);" class="btn btn-sm btn-success" disabled title="Approve"><span class="glyphicon glyphicon-ok"></span></a>
					<?php } else { ?>
						<a href="<?php echo  SITEURL.$controllerName."/venueAcceptRequest/".$this->utility->encode($l->idRegister).'/'.$this->utility->encode($l->idEvent).'/'.$this->utility->encode($l->idPerformer); ?>" class="btn btn-sm btn-primary" title="Approve"><span class="glyphicon glyphicon-ok"></span></a>
					<?php } ?>
					
					<?php if ($l->status=='Cancelled' || $l->status=='Declined' || $l->status=='Rejected'){ ?>
						<a href="javascript:void(0);" class="btn btn-sm btn-danger" disabled title="Decline"><span class="glyphicon glyphicon-remove"></span></a>
					<?php } else { ?>
						<a href="<?php echo SITEURL.$controllerName."/venueRejectRequest/".$this->utility->encode($l->idRegister).'/'.$this->utility->encode($l->idEvent).'/'.$this->utility->encode($l->idPerformer); ?>" class="btn btn-sm btn-danger" title="Decline"><span class="glyphicon glyphicon-remove"></span></a>
					<?php } ?>
					</div>
				</td>
			</tr>
			<?php 
			}
		}
	?>
</tbody>
</table>
</div>
<ul class="pagination">
<?php echo @$paging; ?>
</ul>
</div>
</div>
<script src="<?=EXTERNAL_PATH?>js/chosen.jquery.js"> </script>