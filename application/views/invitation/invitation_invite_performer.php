
<div class="content" style="height: 100%;">

	<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>					
					</div>
	<?php echo $this->session->flashdata('myMessage'); ?>
<div class="col-md-12">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">
						Performer Details <span class="pull-right"> <a href="#"
							class="panel-minimize"><i class="fa fa-chevron-up"></i></a>

						</span>
					</h3>
				</div>
				<div class="panel-body">
					<div class="panel panel-default">
						<div class="panel-body">
						<div class="table-responsive">
						
							<table class="table table-bordered table-striped table-hover">
								<tr>
									<th>Name</th>
									<td><?php echo ucwords($performer->chrName); ?></td>
								</tr>

								<tr>
									<th>Email</th>
									<td><?php echo $performer->user_name; ?></td>
								</tr>

								<tr>
									<th>Rating</th>
									<td><?php echo $performer->rating; ?></td>
								</tr>

								<tr>
									<th>Contact</th>
									<td><?php echo $performer->mem_phone; ?></td>
								</tr>

								<tr>
									<th>City</th>
									<td><?php echo $performer->mem_city;?></td>
								</tr>
							</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-12">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Select Event</h3>
				</div>
				<div class="panel-body">
					<div class="panel panel-default">
						<div class="panel-body">
           	<?php echo form_open(SITEURL.$controllerName.'/inviteForEvent', 'id="invite_form" class="form-horizontal"'); ?>	
            	
 					<div class="col-xs-12 col-sm-10">
								<div class="form-group">
									<label class="col-sm-4 control-label">Select Event <em>*</em></label>
									<div class="col-sm-8">
										<select name="idEvent" id="idEventInvite"
											class="form-control chosen-select" required>
											<option></option>
						      <?php
            
foreach ($events as $st) {
                
                if (! event_end_time_crossed($st->end_date, $st->endTime)) {
                    ?>
						      		<option value="<?php echo $st->table_id; ?>"><?php echo $st->title. '        ('.dateDisplay($st->start_date.' '.$st->startTime,'m/d/y h:i A').'   -     '.dateDisplay($st->end_date.' '.$st->endTime,'m/d/y h:i A').')'; ?></option>	
						      		<?php
                }
            }
            ?>
						      </select> <span class="help-inline text-danger"
											for="idEventInvite"></span>
									</div>

								</div>
<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10 text-center">
									<input type="hidden" name="idPerformer"
										value="<?php echo $this->utility->encode($performer->user_id); ?>" />
									<button type="submit" class="btn btn-success">Send Invite</button>
									<a
										href="<?php echo SITEURL.$this->myvalues->performerDetails['controller']."/searchPerformer"; ?>"
										class="btn btn-default">Cancel</a>
								</div>
							</div>

							
							</div>
 				<?php echo form_close(); ?>
           </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/chosen.jquery.js"></script>
<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/modules/invite_performer.js"></script>
<link href="<?=EXTERNAL_PATH?>css/chosen.css" rel="stylesheet"
	type="text/css">
