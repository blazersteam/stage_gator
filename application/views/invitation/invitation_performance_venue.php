<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>	
		</div>

<?=form_open(SITEURL.$controllerName.'/sendReminder','id="frm"')?>
<div id="no-more-tables">

			<table
				class="table table-bordered table-striped table-hover dataTable cf"
				id="sentList">
				<thead class="cf">
					<tr>

						<th class="no-sort">
			<?php if($this->pidGroup == 2){?>			
			<?php if (count($list)>0){ ?>
			<input type="checkbox" name="chkAll" onclick="setAll();"> 
			<?php } ?>
			<?php } ?>
			Event Title
		</th>
						<th class="">Date</th>
						<th class="">Time</th>
						<th>Performer</th>
						<th class="">Date Invited</th>
						<th class="">Status</th>
						<th>Action</th>
					</tr>
				
				
				<thead>
				
				
				<tbody>	
	<?php
$current_time = time();
$flag = false;

foreach ($list as $l) {
    if (! event_end_time_crossed($l->end_date, $l->endTime)) {
        ?>
			<tr>
				<?php $timeExpected = strtotime(date('',$current_time)." - 1 day"); ?>	
				<td class="no-sort" data-title="Event Title">
				<?php if ($l->status=='Pending'){ ?>
					<?php
            
            if ((strtotime($l->start_date . " " . $l->startTime) > $current_time) && $l->ReminderSentOn < $timeExpected) {
                $flag = true;
                ?>
					<input type="hidden" name="idEvent[]"
							value="<?php echo $this->utility->encode($l->idEvent);?>"> <input
							type="checkbox" name="chkstatus[]"
							value="<?php echo $this->utility->encode($l->idEvent."||".$l->idRegister);?>">					
					<?php } ?>
				<?php } ?>
				<a
							href="<?php echo $this->utility->generateOverviewUrl($l->idEvent,$l->table_id); ?>"
							title="Overview"><?php echo ucwords($l->title); ?></a>
						</td>
						<td data-title="Date" class=""><?php echo dateDisplay($l->start_date.' '.$l->startTime); ?></td>
						<td data-title="Time" class=""><?php echo dateDisplay($l->start_date.' '.$l->startTime,'h:i A'); ?></td>
						<td data-title="Performer"><a
							href="<?php echo SITEURL."view/".$l->url; ?>"
							title="Overview"><?php echo $l->chrName; ?></a></td>
						<td data-title="Date Invited" class=""><?php echo dateDisplay(date("Y-m-d H:i:s", $l->dtApplied)); ?></td>
						<td data-title="Status" class=""> <?php echo $l->status; ?></td>
						<td data-title="Action">
							<div class="text-left">
					<?php if ($l->status=='Pending'){ ?>
						
						<?php if( (strtotime($l->start_date." ".$l->startTime) > $current_time) && $l->ReminderSentOn<$timeExpected && $this->pidGroup == 2){ ?>
						<a
									href="<?php echo SITEURL.$controllerName."/sendReminder/".$this->utility->encode($l->idRegister).'/'.$this->utility->encode($l->idEvent); ?>"
									class="btn btn-sm btn-primary tooltips" data-placement="top"
									data-original-title="Only One Reminder Every 24 Hours is Allowed">Send
									Reminder</a>
						<?php
            }
            ?>						
						<a
									href="<?php echo SITEURL.$controllerName."/venueCancelInvite/".$this->utility->encode($l->idRegister).'/'.$this->utility->encode($l->idEvent).'/'. $this->utility->encode($l->idPerformer); ?>"
									class="btn btn-sm btn-danger" title="Cancel"
									onclick="return confirm('Are you sure you want to complete this action?');">Cancel</a>					
					<?php } else { ?>
						<a
									href="<?php echo SITEURL.$controllerName."/venueCancelInvite/".$this->utility->encode($l->idRegister).'/'.$this->utility->encode($l->idEvent).'/'. $this->utility->encode($l->idPerformer); ?>"
									class="btn btn-sm btn-danger" title="Cancel"
									onclick="return confirm('Are you sure you want to complete this action?');">Cancel</a>
					<?php } ?>
	
					</div>
						</td>
					</tr>
			<?php
    }
}
?>
				
				
				
				<tbody>
				</tbody>
			</table>
		</div>
		<!-- Reminder functionality is Only allowed for venue user-->
<?php if ($flag && $this->pidGroup == 2){ ?>
	<div style="display: block;">
			<input class="btn btn-primary" type="submit" name="bulkInvites"
				value="Send Bulk Reminder" />
		</div>
<?php } ?>
<?php echo form_close(); ?>
</div>
</div>
<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/modules/invitation_sent.js"></script>
