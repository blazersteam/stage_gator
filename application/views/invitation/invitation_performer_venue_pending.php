<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>	
		</div>

		<table class="table table-bordered table-striped table-hover dataTable" id="searchEvent">
			<thead>
				<tr>
					<th>Event Title</th>
					<th>Date</th>
					<th>Start Time</th>
					<th>Date Applied</th>
					<th>Status</th>
				</tr>
			
			
			<thead>
			
			
			<tbody>	
	<?php
$current_time = time();
$flag = false;
foreach ($list as $l) {
    ?>
			<tr>
					<td><a
						href="<?php echo $this->utility->generateOverviewUrl($l->idEvent,$l->table_id); ?>"
						title="Overview"><?php echo ucwords($l->title); ?></a></td>
					<td><?php echo date('m/d/y',strtotime($l->date))." ".$l->startTime; ?></td>
					<td><?php echo (date('i',strtotime($l->startTime)) == '00') ?date('h A',strtotime($l->startTime)):date('h:i A',strtotime($l->startTime)); ?></td>
					<td class="visible-lg"><?php echo date('m/d/y',$l->dtApplied); ?></td>
					<td>
						<div class="pull-left">
							<span class="badge bg-sucess"><?=$l->status?></span> <a
								href="<?php echo SITEURL.$controllerName."/cancelInvite/".$this->utility->encode($l->idEvent)."/".$this->utility->encode($l->idRegister); ?>"
								class="btn btn-sm btn-danger" title="Cancel"
								onclick="return confirm('Are you sure you want to complete this action?');">Cancel</a>
						</div>
					</td>
				</tr>
			<?php
}
?>

			
			
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/modules/invitation_sent.js"></script>
<link href="<?=EXTERNAL_PATH?>css/dataTables.bootstrap.min.css"
	rel="stylesheet" type="text/css">
<script src="<?=EXTERNAL_PATH?>js/chosen.jquery.js"> </script>
<script src="<?=EXTERNAL_PATH?>js/jquery.dataTables.min.js"> </script>
<script>
		
$(document).ready(function() {
    //$('#searchEvent').DataTable( {"bSort" : false});
    $('.chosen-select').chosen();
});
</script>
	