<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Events extends Admin_controller
{
    public $myInfo = "";

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->adminEventDetails;
        $this->folder = './admin';
        $this->load->model($this->myInfo ["model"], "this_model");
        /* most of method we are using of performers model */
        $this->load->model($this->myvalues->adminPerformerDetails ['model'], 'performers_model');
        $this->redirectUrl = SITEURL_ADMIN.$this->myInfo['controller'];
    }

    /**
     * index()
     * This method load for index view it contains all event schedule  listing
     */
    public function index()
    {
        $data ["title"] = 'Events';
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["eventResult"] = $this->this_model->getEventsList();
        $this->myView("admin_events_view", $data);
    }

    
    /**
     * completedEvent()
     * This method load for index view it contains all past event schedule  listing
     */
    public function completedEvent()
    {
        $data ["title"] = 'Events';
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["eventResult"] = $this->this_model->getPastEventsList();
        $this->myView("admin_completed_events_view", $data);
    }
    
    /**
     * delete()
     * This method will delete singal or multipal events
     *
     * @return return session flash messages
     */
    public function delete_events()
    {
        /* we get schedule id in post*/
        if ($this->input->post()) {
            /* Load event model to delete event schedules we used event model method*/
            $this->load->model($this->myvalues->eventDetails['model'], 'event_model');
            $status = $this->event_model->deleteEventSchedule();
            if ($status) {
                $this->utility->setFlashMessage('success', $this->lang->line('event_deleted_successfully'));
            }
            else {
                $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
            }
        }
        /* redirect to back page */
        redirect($this->redirectUrl);
    }
}
