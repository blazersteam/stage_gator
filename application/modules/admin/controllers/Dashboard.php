<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends Admin_controller
{
    public $myInfo = "";

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->dashboardDetails;
        $this->folder = './admin';
        $this->load->model($this->myInfo ["model"], "this_model");
        $this->redirectUrl = SITEURL_ADMIN.$this->myInfo['controller'];
    }

    /**
     * index()
     * This method load for index view it contains all statistics of Performer, venus and Events completed
     */
    public function index()
    {
        $data ["title"] = 'Dashboard';
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["eventsResult"] = $this->this_model->getEventsCount();
        $data ["usersResult"] = $this->this_model->getUsersCount();
        $this->myView("admin_dashboard_view", $data);
    }
}
