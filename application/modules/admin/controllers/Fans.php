<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Fans extends Admin_controller
{
    public $myInfo = "";

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->adminFanDetails;
        $this->load->model($this->myInfo ["model"], "this_model");
        $this->eventInfo = $this->myvalues->eventDetails;
        $this->redirectUrl = SITEURL_ADMIN.$this->myInfo['controller'];
    }
    /**
     * index()
     * This method load for index view it contains all venue listing
     */
    public function index()
    {
        $this->load->model($this->myvalues->performerDetails ['model'],'performer_model');
        $data ["title"] = 'Fans';
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["result"] = $this->this_model->getFanList();
        $this->myView("admin_fans_view", $data);
    }
    /**
     * suspend_user()
     * This method for change fan suspend status
     * @param string $idUser is user id who will suspend by admin
     * @return redirect to http_referer back page
     */
    public function suspend_user($idUser)
    {
        $idUser = $this->utility->decode($idUser);
        $this->this_model->suspend_user($idUser, 1);
        $this->utility->setFlashMessage("success", "User Suspended Successfully");
        redirect($this->redirectUrl);
    }
    /**
     * activate_user()
     * This method for active a fan by admin
     * @param string $idUser is encoded user id its user who will active by admin
     * @return redirect to back padge
     */
    function activate_user($idUser)
    {
        $idUser = $this->utility->decode($idUser);
        $this->this_model->suspend_user($idUser, 0);
        $this->utility->setFlashMessage("success", "User Activated Successfully");
        redirect($this->redirectUrl);
    }
    /**
     * delete_user()
     * This method delete fan all data from corresponds table 
     * @param String $idUser is user id who will get delete by admin
     * @param int $grp this is group id it will always be a venue
     * @return redirect back to back page
     */
    public function delete_user($grp,$idUser)
    {
        /* load profile model to delete a user using profile model's method*/
        $this->load->model($this->myvalues->profileDetails ['model'], 'profile_model');
        $idUser = $this->utility->decode($idUser);
        $this->profile_model->deleteAccount($idUser, $grp);
        $this->utility->setFlashMessage("success", "User Deleted Successfully");
        redirect($this->redirectUrl);
    }
    /**
     * verify_user()
     * This method verify fan user by admin
     * @param String $idUser is encoded user id who will verify by admin
     * @return redirect to back
     */
    public function verify_user($idUser)
    {
        $idUser = $this->utility->decode($idUser);
        $this->this_model->verify_user($idUser);
        $this->utility->setFlashMessage("success", "User Verified Successfully");
        redirect($this->redirectUrl);
    }
}
