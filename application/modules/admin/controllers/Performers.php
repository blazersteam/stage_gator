<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Performers extends Admin_controller
{
    public $myInfo = "";

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->adminPerformerDetails;
        $this->load->model($this->myInfo ["model"], "this_model");
        $this->eventInfo = $this->myvalues->eventDetails;
        $this->redirectUrl = SITEURL_ADMIN . $this->myInfo ['controller'];
    }

    /**
     * index()
     * This method load for index view it contains all venue listing
     */
    public function index()
    {
        $this->load->model($this->myvalues->performerDetails ['model'], 'performer_model');
        $data ["title"] = 'Performers';
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["result"] = $this->this_model->getPerformerList();
        $this->myView("admin_performers_view", $data);
    }

    /**
     * suspend_user()
     * This method for change performer suspend status
     *
     * @param string $idUser is user id who will suspend by admin
     * @return redirect to http_referer back page
     */
    public function suspend_user($idUser)
    {
        $idUser = $this->utility->decode($idUser);
        $this->this_model->suspend_user($idUser, 1);
        $this->utility->setFlashMessage("success", "User Suspended Successfully");
        redirect($this->redirectUrl);
    }

    /**
     * activate_user()
     * This method for active a performer by admin
     *
     * @param string $idUser is encoded user id its user who will active by admin
     * @return redirect to back padge
     */
    function activate_user($idUser)
    {
        $idUser = $this->utility->decode($idUser);
        $this->this_model->suspend_user($idUser, 0);
        $this->utility->setFlashMessage("success", "User Activated Successfully");
        redirect($this->redirectUrl);
    }

    /**
     * delete_user()
     * This method delete performer all data from corresponds table
     *
     * @param String $idUser is user id who will get delete by admin
     * @param int $grp this is group id it will always be a venue
     * @return redirect back to back page
     */
    public function delete_user($grp, $idUser)
    {
        /* load profile model to delete a user using profile model's method */
        $this->load->model($this->myvalues->profileDetails ['model'], 'profile_model');
        $idUser = $this->utility->decode($idUser);
        $this->profile_model->deleteAccount($idUser, $grp);
        $this->utility->setFlashMessage("success", "User Deleted Successfully");
        redirect($this->redirectUrl);
    }

    /**
     * verify_user()
     * This method verify performer user by admin
     *
     * @param String $idUser is encoded user id who will verify by admin
     * @return redirect to back
     */
    public function verify_user($idUser)
    {
        $idUser = $this->utility->decode($idUser);
        $this->this_model->verify_user($idUser);
        $this->utility->setFlashMessage("success", "User Verified Successfully");
        redirect($this->redirectUrl);
    }

    /**
     * Used for display the perfromer tipping history , used by admin only
     * also show the available, panding balance
     *
     * @param string $idPerformer, encoded performer id.
     *       
     * @return load the view for performer's tipping history
     */
    public function tippingHistory($idPerformer)
    {
        if (! empty($idPerformer) && is_numeric($this->utility->decode($idPerformer))) {
            
            $performerID = $this->utility->decode($idPerformer);
            
            // get the stripe_account_key from tipping model
            $this->load->model($this->myvalues->tippingDetails ["model"], "tipping_model");
            $stripe_account_key = $this->tipping_model->getStripeKey($performerID);
            
            // load the stripe functions by helper
            $this->load->helper('stripe_connect_helper');
            $data ['stripeBalance'] = getStripeBalance($stripe_account_key);
            
            // load the profile model to get the user's data
            $this->load->model($this->myvalues->profileDetails ['model'], 'profile_model');
            $userInfo = $this->profile_model->getUserInformation($performerID);
            
            // set this id for doing the filtering with date
            $data ["performerId"] = $idPerformer;
            
            $data ["title"] = $userInfo->chrName . "'s Tipping History";
            
            // set the lastId null first time
            $lastId = null;
            $tempDataArray = array();
            
            // if get the post data that is start_date & end date then set data
            if ($this->input->post()) {
                // convert date m-d-Y to Y-m-d format
                $start_date = DateTime::createFromFormat('m-d-Y', $this->input->post('txtStartDate'));
                
                $start_date = $start_date->format('Y-m-d');
                
                $end_date = DateTime::createFromFormat('m-d-Y', $this->input->post('txtEndDate'));
                
                $end_date = $end_date->format('Y-m-d');
            }
            else {
                // set the end date as today if page loaded first time
                $end_date = date('Y-m-d');
                // set the end date as today -90 day so that start date is previus 90 days.
                $start_date = date('Y-m-d', strtotime("-3 months", strtotime($end_date)));
            }
            
            // set the go to label to call
            FetchAppData:
            
            // call the app data from helper
            $result = getStripeBalanceTransactions($stripe_account_key, 100, $lastId, $start_date, $end_date);
            // if data get & status =1 then get another data by calling the app, because this result the data only
            // 100
            // records.
            if (! empty($result ['status']) && $result ['status'] == 1) {
                
                $resultAppData = array();
                // store data in array
                $resultAppData = $result ['message'] ['data'];
                // merge data in array
                $tempDataArray = array_merge($tempDataArray, $result ['message'] ['data']);
                // find the length of fetched data to check if record is 100
                $legthOfData = count($result ['message'] ['data']);
                
                $lastArray = array();
                
                // if length of data =100 then it may be possible another data to fetch
                if ($legthOfData == 100) {
                    // fetch the last element in fetch array to get the last transaction id , to fetch another data
                    // by
                    // app.
                    $lastArray = end($resultAppData);
                    
                    $lastId = $lastArray ['id'];
                    unset($lastArray);
                    // call to label to make the loop.
                    goto FetchAppData;
                }
            }
            
            $data ['result'] = $tempDataArray;
            
            $data ["controllerName"] = $this->myInfo ["controller"];
            // load the view
            $this->myView("admin_performers_tipping_history_view", $data);
        }
        else {
            redirect($this->redirectUrl);
        }
    }
}
