<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction_details extends Admin_controller
{
    public $myInfo = "";

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->adminTransactionDetails;
        $this->load->model($this->myInfo ["model"], "this_model");
        $this->redirectUrl = SITEURL_ADMIN . $this->myInfo ['controller'];
    }

    /**
     * index()
     * This method called first and load a index page of transaction details
     */
    public function index()
    {
        $data ["title"] = 'Transcation Details';
        $data ["controllerName"] = $this->myInfo ["controller"];
        /* proccess to show payment details if any charge id get in post */
        if ($this->input->post()) {
            /* Load stripe helper */
            $this->load->helper('stripe_connect_helper');
            /* get result by charge id from tipping history table */
            $result = $this->this_model->getTipHistory();
            if(empty($result)){
                $this->utility->setFlashMessage("danger", "No Record Found");
                redirect($this->redirectUrl);
            }
            
            $data ['result'] = $result[0];
            /* this method called to get application fee id and charge id */
            
            $response = retrieveStripeChargeDetails($data ['result']->txn_id);
            
            $data ['stagegator_fee'] = 0;
            /* stripe fee details method only call when get stripe fee id */
            if (! empty($response ['message'] ['application_fee'])) {
                /* This method will retrive detials about application fee */
                $newres = retrieveStripeApplicationFee($response ['message'] ['application_fee']);
                $data ['stagegator_fee'] = $newres ['message'] ['amount'];
            }
            
            $data ['charge_id'] = $response ['message'] ['id'];
            $data ['balance_transaction_id'] = $response ['message'] ['balance_transaction'];
            $data ['stagegator_fee_id'] = $response ['message'] ['application_fee'];
            $data ['main_amount'] = $response ['message'] ['amount'];
            $data ['status'] = $response ['message'] ['status'];
            /* call this method to get stripe fee details */
            $balanceTransaction = retrieveStripeBalanceTransaction($data ['balance_transaction_id']);
            
            $data ['stripe_fee_id'] = $balanceTransaction ['message'] ['id'];
            $data ['available_on'] = date('Y-m-d h:i:s', $balanceTransaction ['message'] ['available_on']);
            $count = count($balanceTransaction ['message'] ['fee_details']);
            /* there are many kind of fee array so we need only stripe fee so we just using a loop */
            for ($i = 0; $i < $count; $i ++) {
                /* condition set to get only stripe fee */
                if (strtolower($balanceTransaction ['message'] ['fee_details'] [$i] ['type']) == 'stripe_fee') {
                    $data ['stripe_fee'] = $balanceTransaction ['message'] ['fee_details'] [$i] ['amount'];
                }
            }
            /* get net amount after less stagegator fee and stripe fee */
            $data ['net_amount'] = $data ['main_amount'] - $data ['stagegator_fee'] - $data ['stripe_fee'];
        }
        /* set all maindetory data in $data array and load it to view */
        $this->myView('admin_detailed_transaction_view.php', $data);
    }
}


