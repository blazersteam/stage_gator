<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Venues extends Admin_controller
{
    public $myInfo = "";

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->adminVenueDetails;
        $this->load->model($this->myInfo ["model"], "this_model");
        /* most of method we are using of performers model  */
        $this->load->model($this->myvalues->adminPerformerDetails['model'],'performers_model');
        $this->redirectUrl = SITEURL_ADMIN.$this->myInfo['controller'];
        
    }
    /**
     * index()
     * This method load for index view it contains all venue listing
     */
    public function index()
    {
        $this->load->model($this->myvalues->performerDetails ['model']);
        $data ["title"] = 'Venues';
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["result"] = $this->this_model->getVenuesList();
        $this->myView("admin_venues_view", $data);
    }
    /**
     * suspend_user()
     * This method for change user suspend status
     * @param string $idUser is user id who will suspend by admin
     * @return redirect to http_referer back page
     */
    public function suspend_user($idUser)
    {
        /* decode user id  using utility */
        $idUser = $this->utility->decode($idUser);
        $this->performers_model->suspend_user($idUser, 1);
        $this->utility->setFlashMessage("success", "User Suspended Successfully");
        /* redirect to same page where request come from */
        redirect($this->redirectUrl);
        
    }
    /**
     * activate_user()
     * This method for active a venue by admin
     * @param string $idUser is encoded user id its user who will active by admin
     * @return redirect to back padge
     */
    function activate_user($idUser)
    {
        $idUser = $this->utility->decode($idUser);
        $this->performers_model->suspend_user($idUser, 0);
        $this->utility->setFlashMessage("success", "User Activated Successfully");
        redirect($this->redirectUrl);
    }
    /**
     * delete_user()
     * This method delete user all data from corresponds table to this user
     * @param String $idUser is user id who will get delete by admin
     * @param int $grp this is group id it will always be a venue
     * @return redirect back to back page 
     */
    public function delete_user($grp,$idUser)
    {
        /* Load profile model to load delete account method from profile model*/
        $this->load->model($this->myvalues->profileDetails ['model'], 'profile_model');
        $idUser = $this->utility->decode($idUser);
        /* delete user from profile model */
        $this->profile_model->deleteAccount($idUser, $grp);
        $this->utility->setFlashMessage("success", "User Deleted Successfully");
        redirect($this->redirectUrl);
    }
    /**
     * verify_user()
     * This method verify venue user by admin
     * @param String $idUser is encoded user id who will verify by admin
     * @return redirect to back 
     */
    public function verify_user($idUser)
    {
        $idUser = $this->utility->decode($idUser);
        $this->performers_model->verify_user($idUser);
        $this->utility->setFlashMessage("success", "User Verified Successfully");
        redirect($this->redirectUrl);
    }
}
