<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Transaction_details_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }
    
    
    /**
     * getTipHistory
     * This method get tipping historty data from tipping history table
     *
     * @param return tipping details and user details information
     */
    public function getTipHistory()
    {
        
        $chargeId = $this->input->post('txtName');
        $data ['select'] = [
            'tipHistory.from_user_id',
            'tipHistory.to_user_id',
            'tipHistory.amount',
            'tipHistory.dt_created_datetime',
            'tipHistory.txn_id',
            'user1.chrName as to_user',
            'user2.chrName as from_user',
            
        ];
        $data ['where'] = [
            "tipHistory.txn_id" => $chargeId
        ];
        $data ["join"] = [
            TABLE_USER . " as user1" => [
                "user1.user_id = tipHistory.to_user_id",
                "INNER"
            ],
            TABLE_USER . " as user2" => [
                "user2.user_id = tipHistory.from_user_id",
                "INNER"
            ]
        ];
        $data ['table'] = TABLE_TIPPING_HISTORY . " as tipHistory";
        $result = $this->selectFromJoin($data);
        
        return $result;
    }

}

?>
