<?php

class Events_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }
    /**
     * getVenuesList()
     * get venue list 
     * @return Object $res
     */
    public function getEventsList()
    {
        
         $data ['select'] = [
            'evt_schedule.idEvent',
            'evt_schedule.table_id',
            'evt_schedule.start_date',
            'evt_schedule.end_date',
            'evt.title',
            'evt.idVenue',
            'evt.startTime',
            'evt.endTime',
            'evt.haveHost',
            'evt.timeZone',
            'evt.hoursLockBefore',
            'evt.allowWalkInReal',
            'evt.lockStatus',
            'evt.allowTrading',
            'evt.location_id',
            'user.chrName',
            'user.url',
            'location.city',
            'location.state'
        ];
        
        $data ['table'] = TABLE_EVENT_SCHEDULES . ' as  evt_schedule';
        
        $data ["join"] = [
            TABLE_EVENT . " as evt" => [
                "evt.idEvent = evt_schedule.idEvent",
                "INNER"
            ],
            TABLE_EVENT_STYLE . " as evt_style" => [
                "evt.idEvent = evt_style.idEvent",
                "INNER"
            ],
            TABLE_USER . " as user" => [
                "evt.idVenue = user.user_id",
                "INNER"
            ],
            TABLE_LOCATIONS . " as location" => [
                "evt.location_id = location.location_id",
                "INNER"
            ]
        ];
        
        $data ['where'] = [
            'evt_schedule.start_date >=' => convertToGMT($this->datetime, "Y-m-d"),
            'evt_schedule.b_is_disabled' => 0
        ];
        
        // $data ["groupBy"] = 'evt_schedule.idEvent';
        $data ["groupBy"] = 'evt_schedule.table_id';
        
        $data ["order"] = 'evt_schedule.start_date asc';
        
        $result = $this->selectFromJoin($data);
        
        return $result;
    }
    
    
    /**
     * getPastEventsList()
     * get past venue/event list
     * @return Object $res
     */
    public function getPastEventsList()
    {
        
        $data ['select'] = [
            'evt_schedule.idEvent',
            'evt_schedule.table_id',
            'evt_schedule.start_date',
            'evt_schedule.end_date',
            'evt.title',
            'evt.idVenue',
            'evt.startTime',
            'evt.endTime',
            'evt.haveHost',
            'evt.timeZone',
            'evt.hoursLockBefore',
            'evt.allowWalkInReal',
            'evt.lockStatus',
            'evt.allowTrading',
            'evt.location_id',
            'user.chrName',
            'user.url',
            'location.city',
            'location.state'
        ];
        
        $data ['table'] = TABLE_EVENT_SCHEDULES . ' as  evt_schedule';
        
        $data ["join"] = [
            TABLE_EVENT . " as evt" => [
                "evt.idEvent = evt_schedule.idEvent",
                "INNER"
            ],
            TABLE_EVENT_STYLE . " as evt_style" => [
                "evt.idEvent = evt_style.idEvent",
                "INNER"
            ],
            TABLE_USER . " as user" => [
                "evt.idVenue = user.user_id",
                "INNER"
            ],
            TABLE_LOCATIONS . " as location" => [
                "evt.location_id = location.location_id",
                "INNER"
            ]
        ];
        
        $data ['where'] = [
            'evt_schedule.end_date <=' => convertToGMT($this->datetime, "Y-m-d")
        ];
        
        // $data ["groupBy"] = 'evt_schedule.idEvent';
        $data ["groupBy"] = 'evt_schedule.table_id';
        
        $data ["order"] = 'evt_schedule.start_date asc';
        
        $result = $this->selectFromJoin($data);
        return $result;
    }

   
}

?>
