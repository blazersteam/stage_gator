<?php

class Venues_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * getVenuesList()
     * get venue list
     * 
     * @return Object $res
     */
    public function getVenuesList()
    {
        $data ["select"] = [
            'u.user_id',
            'u.user_name',
            'u.contact_person',
            'u.idGrp',
            'u.mem_id',
            'u.status_id',
            'u.statusMem',
            'u.chrName',
            'u.islock',
            'u.ip',
            'u.suspended',
            "u.rate",
            "u.image",
            "u.url",
            "mem.mem_city",
            "mem.mem_state",
            "mem.mem_phone"
        ];
        $data ["table"] = TABLE_USER . " as u";
        
        $data ["join"] = [
            TABLE_MEMBERS . " as mem" => [
                "u.mem_id = mem.mem_id",
                "INNER"
            ]
        ];
        $data ["where"] = [
            'u.idGrp' => VENUE_GROUP
        ];
        $data ['order'] = 'u.rate desc,u.chrName asc';
        
        $res = $this->selectFromJoin($data);
        
        return $res;
    }
}

?>
