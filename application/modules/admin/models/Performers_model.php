<?php

class Performers_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }
    /**
     * getPerformerList()
     * This method for get performer listing 
     * @return Object $res
     */
    public function getPerformerList()
    {
        $data ["select"] = [
            'u.user_id',
            'u.user_name',
            'u.contact_person',
            'u.idGrp',
            'u.mem_id',
            'u.status_id',
            'u.statusMem',
            'u.chrName',
            'u.islock',
            'u.ip',
            'u.suspended',
            "u.rate",
            "u.stripe_status",
            "u.image",
            "u.url",
            "mem.mem_city",
            "mem.mem_state",
            "mem.mem_phone"
        ];
        $data ["table"] = TABLE_USER . " as u";
        
        $data ["join"] = [
            TABLE_MEMBERS . " as mem" => [
                "u.mem_id = mem.mem_id",
                "INNER"
            ]
        ];
        $data ["where"] = [
            'u.idGrp' => PERFOMER_GROUP
        ];
        $data ['order'] = 'u.rate desc,u.chrName asc';
        
        $res = $this->selectFromJoin($data);
        return $res;
    }
    /**
     * suspend_user()
     * This method used for suspend a user in user table
     * @param is user id $idUser
     * @param status either 0 or 1 $status
     */
    public function suspend_user($idUser, $status)
    {
        $data ["where"] = [
            "user_id" => $idUser
        ];
        $data ["update"] ["suspended"] = $status;
        $data ["table"] = TABLE_USER;
        $this->updateRecords($data);
        $this->db->query("DELETE FROM ci_sessions_new where data like '%user_id\";s:". strlen($idUser) .":\"$idUser\";%'");
    }
    /**
     * verify_user()
     * This method used for suspend a user in user table
     * @param is user id $idUser
     * @param status_id  1 becoz this method only call when a user will verify
     */
    public function verify_user($idUser)
    {
        $data ["where"] = [
            "user_id" => $idUser
        ];
        $data ["update"] ["status_id"] = 1;
        $data ["table"] = TABLE_USER;
        $this->updateRecords($data);
    }
}

?>
