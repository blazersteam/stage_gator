<?php

class Dashboard_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * This function sends count of all events that are completed
     *
     * @return count of events
     */
    public function getEventsCount()
    {
        $data ["select"] = [
            "count(es.idEvent) as total"
        ];
        $data ["table"] = TABLE_EVENT_SCHEDULES . " as es";
        $data ["order"] = "es.end_date";
        $data ["where"] = [
            "ADDTIME(es.end_date, e.endTime) <= " => date("Y-m-d H:i:s")
        ];
        $data ["join"] = [
            TABLE_EVENT . " as e" => [
                "e.idEvent = es.idEvent",
                "INNER"
            ]
        ];
        $result = $this->selectFromJoin($data);
        return $result [0]->total;
    }

    /**
     * This function sends count of all performers and Venues those are acitve
     *
     * @return count of users by array
     */
    public function getUsersCount()
    {
        $data ["select"] = [
            "count(u.user_id) as total",
            "idGrp"
        ];
        $data ["table"] = TABLE_USER . " as u";
        $data["where"] = ["status_id" => "1"];
        $data ["where_in"] = [
            "idGrp" => [
                2,
                3,
                4
            ]
        ];
        $data ["groupBy"] = ["u.idGrp"];
        $result = $this->selectFromJoin($data);

        $counts = ["Venues" => 0, "Performers" => 0];
        foreach ($result as $k => $v)
        {
            if($v->idGrp == "2") {
                $counts ["Venues"] = $v->total; 
            } elseif($v->idGrp == "3") {
                $counts ["Performers"] = $v->total;
            }
            elseif($v->idGrp == "4") {
                $counts ["Fans"] = $v->total;
            }
        }
        return $counts;
    }
}

?>
