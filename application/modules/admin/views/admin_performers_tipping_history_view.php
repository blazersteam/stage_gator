<div class="content" style="height: 100%;">
	<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>					
		</div>
		<div class="alert alert-success text-center myMessage"
			style="display: none"></div>


		<div style="padding: 5px;" class="col-xs-12 col-sm-12 row">		
			<div class="form-group">
				<label class="col-sm-4 control-label">Available Balance: </label>
				<div class="col-sm-8">
					<label class="control-label"><?php echo '$ '.isset ($stripeBalance ['available'][0]['Amount'] )  ? showBalance($stripeBalance ['available'][0]['Amount']/100) : 0; ?></label>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-4 control-label">Pending Balance: </label>
				<div class="col-sm-8">
					<label class="control-label"><?php echo '$ '.isset ($stripeBalance ['pending'][0]['Amount'] )  ? showBalance($stripeBalance ['pending'][0]['Amount']/100) : 0; ?></label>
				</div>
			</div>
           
		</div>

		<div class="filterDiv" style="padding: 5px; text-align: center;" class="col-xs-12 col-sm-12">					
<?=form_open(SITEURL.'/admin/'.$controllerName.'/tippingHistory/'.$performerId,'id="frmPerformerTipHistory" class="form-inline" role="form"')?>

			<div class="form-group">
				<div class="input-append date" id="dateEnd" style="z-index: 2000;">
					<span class="add-on"><input type="text" id="txtStartDate" required
						value="<?=@set_value('txtStartDate')?@set_value('txtStartDate'):date("m-d-Y", strtotime("-3 months"));?>"
						name="txtStartDate" placeholder="Start date" class="form-control"
						required readonly></span>
                		<?php echo form_error('txtStartDate'); ?>
                	  </div>

			</div>

			<div class="form-group">

				<div class="input-append date" id="dateEnd" style="z-index: 2000;">
					<span class="add-on "><input type="text" id="txtEndDate" required
						placeholder="End date"
						value="<?=@set_value('txtEndDate')?@set_value('txtEndDate'):date('m-d-Y');?>"
						name="txtEndDate" class="form-control datepicker" required
						readonly></span>
                		<?php echo form_error('txtEndDate'); ?>
                		<span for="txtEndDate"
						class="pull-left help-inline text-danger"></span>
				</div>

			</div>

			<button type="submit" class="btn btn-success">Search</button>


<?php echo form_close(); ?>
</div>		
<?php
echo $this->session->flashdata('myMessage');
?>
  
<?php
if (isset($result)) {
    if (count($result) > 0) {
        ?>
		<div id="no-more-tables">
			<table class="table table-bordered display dataTable cf">
				<thead class="cf">
					<tr role="row">
						<th style="width: 7%">Sr #</th>
						<th>Tip Amount</th>
						<!-- <th style="width: 20%">Tip Amount</th> -->
						<th>Date</th>
						<th>Type</th>
						<!-- <th>Description</th>  -->
						<!-- <th>Status</th>  -->
						<th>Available On</th>
					</tr>
				</thead>

				<tbody role="alert" aria-live="polite" aria-relevant="all">
       <?php
        $i = 1;
        $class = "";
        $class1 = "";
        foreach ($result as $r) {
            
            if ($r ['status'] == 'pending') {
                $class1 = 'lightpink';
            }
            else {
                $class1 = '';
            }
            
            ?>
				<tr class="<?php echo $class1;?>">
						<td data-title="Sr #"><?php echo $i; ?></td>
						<td data-title="Tip Amount" title="Tip Amount"><?php echo "$".(abs($r['net'])/100);?></td>
						<td data-title="Date"><?php echo dateDisplay(date("m/d/Y", $r['created']));?></td>
						<td data-title="Type"><?php  echo $r['amount'] >= 0 ? 'Credit' : 'Debit'; ?></td>
						<!-- <td data-title="Description" title="Description"><?php echo $r['description'];?></td>  -->
						<!-- <td data-title="Status" title="Status"><?php echo $r['status'];?></td>  -->
						<td data-title="Available On" title="Available On"><?php echo dateDisplay(date("m/d/Y", $r['available_on']));?></td>
					</tr>
				<?php
            $i ++;
        }
        ?>    
         </tbody>
			</table>
		</div>
	
<?php  } else { ?>
	<div class="alert alert-danger">No Record Found</div>	
<?php
    }
}
?>
</div>
</div>

<script src="<?=EXTERNAL_PATH?>js//validate.js"></script>

<link
	href="<?=EXTERNAL_PATH?>js/datetimepicker/bootstrap-datetimepicker.min.css"
	rel="stylesheet" type="text/css">
<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/datetimepicker/moment.min.js"></script>
<script type="text/javascript"
	src="<?=EXTERNAL_PATH?>js/datetimepicker/bootstrap-datetimepicker.min.js"></script>


<script type="text/javascript">

$(function () {

    $("#txtStartDate").datetimepicker({  
        ignoreReadonly : true,
        //useCurrent : true,
    	format : 'MM-DD-YYYY', 
    });
    
    $("#txtEndDate").datetimepicker({   
        ignoreReadonly : true,
    	//maxDate : 'now',	
    	//useCurrent : true,	
    	format : 'MM-DD-YYYY',    
    });
    
    $("#txtStartDate").on("dp.change", function() {
        
    	var startDate = $("#txtStartDate").val();
    	var startDateVar = startDate.split("-");
    	
    	var setEndDate = new Date(startDateVar[2], startDateVar[0], startDateVar[1]);
    	var setEndDateVar = new Date(new Date(setEndDate).setMonth(setEndDate.getMonth()+3));	

    	// set the minDate for end date that is startdate + 3 months        
        $('#txtEndDate').data("DateTimePicker").maxDate(setEndDateVar);
    });
    
    $("#txtEndDate").on("dp.change", function() {
       
        var endDate = $("#txtStartDate").val();
        var endDateVar = endDate.split("-");
    	
    	var setStartDate = new Date(endDateVar[2], endDateVar[0], endDateVar[1]);
    	var setStartDateVar = new Date(new Date(setStartDate).setMonth(setStartDate.getMonth()-3));
    	
    	// set the minDate for end date that is startdate + 3 months        
        $('#txtStartDate').data("DateTimePicker").minDate(setStartDateVar);
        
    });
});

$("#frmPerformerTipHistory")
.validate(
		{
			rules : {
				required : {
					required : true,				
				},
				
				txtStartDate : {
					required : true
				},
				txtEndDate : {
					required : true,					
				}
			},

			errorClass : "help-inline text-danger",
			errorElement : "span",
			highlight : function(element,
					errorClass, validClass) {
				$(element).parents('.form-group')
						.addClass('has-error');
			},
			errorPlacement: function (error, element) {
				//alert(error.text());
	            //check whether chosen plugin is initialized for the element
	            if (element.data().chosen) { 
	                element.next().after(error);
	            } else {
	                element.after(error);
	            }
			},
			unhighlight : function(element,
					errorClass, validClass) {
				$(element).parents('.form-group')
						.removeClass('has-error');
				$(element).parents('.form-group')
						.addClass('has-success');
			}

		});

var datatableDisplayLength = 50;
</script>