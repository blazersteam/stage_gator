
<div class="content" style="height: 100%;">

	<div class="row">
		<div id="Tick"></div>
		<?php echo $this->session->flashdata('myMessage'); ?>
		<div class="page_header">
						<?=$title?>					
					</div>
<?php
if (isset($result)) {
    if (count($result) > 0) {
        ?>
         <div class="table-responsive search-shows">
		<div id="no-more-tables">
		<table
		class="table table-bordered table-hover table-striped display dataTable cf">
		<thead class="cf">
			<tr role="row">
				<th style="width: 7%">Status</th>
				<th>Name</th>
				<th style="width: 20%">Email Address</th>
				<th style="width: 10%">Phone Number</th>
				<th style="width: 15%">IP ?</th>
				<th style="width: 10%">Styles </th>
				<th style="width: 10%">Image </th>
				<th style="width: 10%">City </th>
				
				<!--<th style="width: 10%">Rating OLD</th>-->
				<th style="width: 10%">Action</th>
			</tr>
		</thead>

		<tbody role="alert" aria-live="polite" aria-relevant="all">
       <?php
        $i = 1;
        $class = "";
        foreach ($result as $r) {
            if ($i % 2 == 0) {
                $class = 'class="gradeA even"';
            }
            else {
                $class = 'class="gradeA odd"';
            }
            
            $styles = $this->performer_model->getUserStyles($r->user_id);
            $userid = $this->utility->encode($r->user_id);
            ?>
				<tr <?php echo $class; ?>>
				<td data-title="status">
				<?php if ($r->status_id==1){ ?>
               				<span class="badge bg-success">Verified</span>
               			<?php } else { ?>
               				<span class="badge bg-danger marginBtm10">Not Verified</span>
               				<a href="<?php echo SITEURL_ADMIN.$controllerName."/verify_user/$userid"; ?>" class="btn btn-sm btn-info">Verify</a>
               			<?php } ?>
				
				</td>
				<td data-title="Name" class="">
               		<a
					href="<?php echo SITEURL."view/".$r->url; ?>"
					title="Overview"><?php echo ucwords($r->chrName); ?></a>
				</td>
				<td data-title="Email address"><?=$r->user_name?></td>
				<td data-title="Phone number"><?=$r->mem_phone?></td>
				<td data-title="Email address"><?=$r->ip?></td>
               		<?php 
/*
                   * ?>
                   * <td class=""><a href="<?php echo base_url()."performer/overview"; ?>"><?php echo
                   * ucwords($r->chrName); ?></a></td>
                   */
            ?>
               		
				<td data-title="Style"> 
               			<?php
            $n = 0;
            foreach ($styles as $row) {
                if ($n) {
                    echo ", ";
                }
                echo $row->nameStyle;
                $n ++;
            }
            ?>
               		</td>
				<td data-title="Image" align="center" class="">
               			<?php if (!empty($r->image) && file_exists("./external/images/profiles/".$r->image)) { ?>
               			<img
					src="<?php echo EXTERNAL_PATH."images/profiles/".$r->image; ?>"
					style="width: 50px; height: 50px;" />
               			<?php } else { ?>
               					<img
					src="<?php echo EXTERNAL_PATH."images/profiles/eleven.png"; ?>"
					style="width: 50px; height: 50px;" />
               			<?php } ?>
               		</td>
				<td data-title="City"><?php echo ucwords($r->mem_city).' '.$r->mem_state; ?></td>

				
               		<td align="center" class="hidden-print">
               			<?php if ($r->suspended==0){ ?>
               				<a href="<?php echo SITEURL_ADMIN.$controllerName."/suspend_user/$userid"; ?>" class="btn btn-sm btn-info" onclick = "return confirm('Are you sure you want to Suspend ?');">Suspend</a>
               			<?php } else { ?>
               				<a href="<?php echo SITEURL_ADMIN.$controllerName."/activate_user/$userid"; ?>" class="btn btn-sm btn-success" onclick = "return confirm('Are you sure you want to Active ?');">Active</a>
               			<?php } ?>
               			<a href="<?php echo SITEURL_ADMIN.$controllerName."/delete_user/".FAN_GROUP."/$userid"; ?>" class="btn btn-sm btn-danger" onclick=" return confirm('Are you sure you want to Delete ?');">Delete</a>
               		</td>
            </tr>
				<?php
            $i ++;
        }
        ?>
    
         </tbody>

	</table>
	</div>
	</div>
<?php  } else { ?>
	<div class="alert alert-danger">No Record Found</div>	
<?php
    
}
}
?>
</div>
</div>

</div>
<link href="<?=EXTERNAL_PATH?>css/dataTables.bootstrap.min.css"rel="stylesheet" type="text/css">
<script src="<?=EXTERNAL_PATH?>js/jquery.dataTables.min.js"> </script>



