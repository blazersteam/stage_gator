<style>

</style>
<div class="content" style="height: 100%;">
	<div class="row">
		<div id="Tick"></div>
		<?php echo $this->session->flashdata('myMessage'); ?>
		<div class="page_header"><?=$title?></div>

		<div class="container" style="margin-top: 40px">
			<!-- <div class="row"> -->
				<div class="col-xs-12 col-sm-10">

					<div class="panel panel-default panel-horizontal dashborad-panel">
						
						<div class="panel-heading">
							<a href="<?php echo SITEURL_ADMIN.'venues'; ?>">
								<h3 class="panel-title">Total Venues</h3>
								<h4><?=$usersResult["Venues"];?></h4>
							</a>
						</div>
						<div class="panel-heading">
							<a href="<?php echo SITEURL_ADMIN.'performers'; ?>">
								<h3 class="panel-title">Total Performers</h3>
								<h4><?=$usersResult["Performers"];?></h4>
							</a>
						</div>
						<div class="panel-heading">
							<a href="<?php echo SITEURL_ADMIN.'fans'; ?>">
								<h3 class="panel-title">Total Fans</h3>
								<h4><?=$usersResult["Fans"];?></h4>
							</a>
						</div>
						<div class="panel-heading">
							<a href="<?php echo SITEURL_ADMIN.'events/completedevent'; ?>">
								<h3 class="panel-title">Total Events Completed</h3>
								<h4><?=$eventsResult;?></h4>
							</a>
						</div>
						
					</div>

				</div>
			<!-- </div> -->
		</div>


	</div>
</div>
