<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>	
		</div>
        <?=form_open(SITEURL_ADMIN.$controllerName,'id="frm" class="form-inline marginBtm10"')?>
        
        
      
      
                <div class="form-group transcation-search col-sm-5 col-md-4">
                    <div class="input-group">
                    <input name="txtName" class="form-control" placeholder="Search With Transaction ID"value="" type="text">
                        <span class="input-group-btn">
                            <button class="btn btn-success" type="submit">Search</button>
                        </span>
                                             
                    </div>
                      <label class="error" for="txtName"></label>  
                </div>
      
        
        <!-- <div class="col-sm-5">
          <div class="form-group">
			<input name="txtName" class="form-control" placeholder="Search With Transaction ID" value="" type="text">
		</div>
		 <button type="submit" class="btn btn-success">Search</button>
		  <label class="error" for="txtName"></label>
		 </div> -->
	      
      <?php 
      echo form_close();
      if(isset($result))
      {
      
      ?>
      
		<table class="table table-bordered table-striped table-hover">
			<tr>
				<th style="width: 25%;">Tip From:</th>
				<td><?=$result->from_user?></td>
			</tr>
			<tr>
				<th style="width: 25%;">Tip To:</th>
				<td><?=$result->to_user?></td>
			</tr>
			<tr>
				<th style="width: 25%;">Amount:</th>
				<td>$<?=$result->amount?></td>
			</tr>
			<tr>
				<th>Transaction Date:</th>
				<td><?=date('m-d-Y H:i:s',strtotime($result->dt_created_datetime))?></td>
			</tr>
			<tr>
				<th></th>
				<th></th>
			</tr>

			<tr>
				<th>Tip Amount:</th>
				<td>$<?=showBalance($main_amount/100)?></td>
			</tr>

			<tr>
				<th>StageGator Fee:</th>
				<td>$<?=showBalance($stagegator_fee/100)?></td>
			</tr>
			<tr>
				<th>Stripe Fee:</th>
				<td>$<?=showBalance($stripe_fee/100)?></td>
			</tr>

			<tr>
				<th>Net Amount:</th>
				<td>$<?=showBalance($net_amount/100)?></td>
			</tr>
			
			<tr>
				<th>Available On:</th>
				<td><?=date('m-d-Y H:i:s',strtotime($available_on))?></td>
			</tr>
			
			<tr>
				<th>Status:</th>
				<td><?=ucfirst($status)?></td>
			</tr>
			
			<tr>
				<th></th>
				<th></th>
			</tr>
			
			<tr>
				<th>Transaction ID:</th>
				<td><?=$charge_id?></td>
			</tr>
			
			<tr>
				<th>Balance Transaction ID:</th>
				<td><?=$balance_transaction_id?></td>
			</tr>
			
			
			
			<tr>
				<th>Application Fee ID:</th>
				<td><?=$stagegator_fee_id?></td>
			</tr>
 </table>
		<?php } ?>
</div>
</div>

<script>
$(document).ready(function(){

	$("#frm").validate(
			{
				rules : {
					txtName : {
						required : true									
					},
				},    					
				highlight : function(element, errorClass,
						validClass) {
					
					$(element).parents('.form-control').addClass(
							'has-error');
				},
				unhighlight : function(element, errorClass,
						validClass) {
					$(element).parents('.form-control')
							.removeClass('has-error');
					$(element).parents('.form-control').addClass(
							'has-success');
				}
			});        	            
});
</script>

 

