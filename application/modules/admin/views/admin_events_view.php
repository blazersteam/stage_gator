
<div class="content" style="height: 100%;">
<?php echo $this->session->flashdata('myMessage'); ?>
				<div class="row">
		<div id="Tick"></div>
		<div class="page_header">
						<?=$title?>	
										
					</div>
<?=form_open(SITEURL_ADMIN.$controllerName.'/delete_events','id="frm" onsubmit="return confirm_delete_events();"')?>	
<div class="table-responsive search-shows">
<div id="no-more-tables">
		<table class="table table-striped table-hover table-bordered dataTable no-footer cf"
			id="searchEvent">
			<thead class="cf">
				<tr role="row">
					<th style="width: 1%" class=""><input type="checkbox"
							name="chkAll" onclick="setAll();"></th>
					<th style="width: 10%">Event Title</th>
					<th style="width: 10%">Venue Title</th>
					<th style="width: 10%">City</th>
					<th style="width: 10%">Date</th>
					<th style="width: 10%">Start Time</th>
					<th style="width: 10%" class="">End Time</th>
				    <th style="width: 5%" class="">Have Host?</th>
					
				</tr>
			</thead>

			<tbody role="alert" aria-live="polite" aria-relevant="all">
                
                 <?php
                 
                if (count($eventResult) > 0) {
                    
                    foreach ($eventResult as $k => $v) :
                        $v = $this->utility->decodeText($v);
                    if (event_end_time_crossed($v->end_date,$v->endTime)) {
                        continue;
                    }
                        ?>
          <tr role="row">
					<td><input type="checkbox" name="chkstatus[]"
							value="<?php echo $this->utility->encode($v->table_id); ?>"
							class="inpt_c1"></td>
					<td data-title="Event Title">
					   <a href="<?php echo $this->utility->generateOverviewUrl($v->idEvent,$v->table_id);?>" ><?php echo $v->title;?></a>
					</td>
					<td data-title="Agency Title"><a href="<?=SITEURL."view/". $v->url; ?>"><?php echo $v->chrName;?></a></td>
					<td data-title="City"><?php echo $v->city.', '.$v->state;?></td>
					<td data-title="Date"><?php echo dateDisplay($v->start_date.''.$v->startTime);?></td>
					<td data-title="Start Time"><?php echo dateDisplay($v->start_date.''.$v->startTime,'h:i A'); ?></td>
					<td data-title="End Time"><?php echo dateDisplay($v->end_date.''.$v->endTime,'h:i A');?></td>
				    <td data-title="Have Host?"><?php echo $v->haveHost==0 ? "No":"Yes"; ?></td>
					
				</tr>     
               <?php
                    endforeach;
                }
                else {
                    ?>
            <tr>
					<td data-title="Date" colspan="11" align="center"><?php echo $this->lang->line("error_norecords_found"); ?></td>
				</tr>
      <?php
                }
                ?>  
			</tbody>
		</table>
		<div style="display: block;">
			<input class="btn btn-danger" type="submit" name="deleteEvents"
				value="Delete" />
		</div>
		</div></div>
		
	</div>
	<?php echo form_close(); ?>
	
</div>
<script>
function confirm_delete_events(){
	if (!Checkbox("frm", "chkstatus[]")){
		alert("Please select an Event to delete!");
		return false;	
	}
	return confirm('This will delete the event(s) permanently. Are you sure?');
}
function Checkbox(TheForm, Field){
	var obj = document.forms[TheForm].elements[Field];
	var res = false;
	if(obj.length > 0){
		for(var i=0; i < obj.length; i++){
			if(obj[i].checked == true){
				res = true;
			}
		}
	}
	else{
		if(obj.checked == true){
				res = true;
		}
	}
	return (res);
}

function setAll(){
	if(frm.chkAll.checked == true){
		checkAll("frm", "chkstatus[]");
	}
	else{
		clearAll("frm", "chkstatus[]");
	}
}	

function checkAll(TheForm, Field){
	var obj = document.forms[TheForm].elements[Field];
	if(obj.length > 0){
		for(var i=0; i < obj.length; i++){
			obj[i].checked = true;
		}
	}
	else{
		obj.checked = true;
	}
}
function clearAll(TheForm, Field){
	var obj = document.forms[TheForm].elements[Field];
	if(obj.length > 0){
		for(var i=0; i < obj.length; i++){
			obj[i].checked = false;
		}
	}
	else{
		obj.checked = false;
	}
}
</script>