<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trading extends General_controller
{
    public $myInfo = "";

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->tradingDetails;
        $this->folder = $this->myInfo ["controller"];
        $this->load->model($this->myInfo ["model"], "this_model");
    }

    /**
     * index()
     * This method use for recive list of those performer who applied for trading slot
     */
    public function index()
    {
        $data ["toMe"] = $this->this_model->get_trading_request_to_me($this->pUserId);
        $data ["fromMe"] = $this->this_model->get_trading_request_from_me($this->pUserId);
        
        $data ['controllerName'] = $this->myInfo ['controller'];
        $data ["title"] = "Trading Requests Received";
        $this->myView($this->folder . "/trading_view", $data);
    }

    function acceptTrade($trade_id = "")
    {
        $r = $this->this_model->accept_trade($this->utility->decode($trade_id), $this->pUserId);
        if ($r == 0) {
            $this->utility->setFlashMessage('danger', "Action Couldn't Be Completed.");
        }
        else if ($r == 1) {
            $this->utility->setFlashMessage('success', 'Trade Request Accepted! Please Check Your New Schedule.');
        }
        redirect(SITEURL . $this->myInfo ["controller"]);
    }

    function cancelTrade($trade_id)
    {
        $result = $this->this_model->cancel_trade($this->utility->decode($trade_id), $this->pUserId);
        if ($result == 0) {
            $this->utility->setFlashMessage('danger', "Action Couldn't Be Completed");
        }
        else if ($result == 1) {
            $this->utility->setFlashMessage('success', 'Trade Request Cancelled');
        }
        redirect(SITEURL . $this->myInfo ["controller"]);
    }

    function rejectTrade($trade_id)
    {
        $result = $this->this_model->reject_trade($this->utility->decode($trade_id), $this->pUserId);
        if ($result == 0) {
            $this->utility->setFlashMessage('danger', "Action Couldn't Be Completed");
        }
        else if ($result == 1) {
            $this->utility->setFlashMessage('success', 'Trade Request Declined');
        }
        redirect(SITEURL . $this->myInfo ["controller"]);
    }
}
