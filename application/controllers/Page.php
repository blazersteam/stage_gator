<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Page extends Guest_controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function contact_us()
    {
        if ($this->input->post()) {
            $this->contactusform($this->input->post());
            $this->utility->setFlashMessage('success', 'We received your message! You will be contacted by the relevant department as soon as possible!');
            redirect(SITEURL . 'page/contact_us');
        }
        
        $data ["title"] = "Contact Us";
        $this->myView("page/contact_us_view", $data);
    }

    public function terms_and_conditions()
    {
        $data ["title"] = "Terms & Conditions";
        $this->myView("page/terms_and_conditions_view", $data);
    }

    public function privacy_policy()
    {
        $data ["title"] = "Privacy Policy";
        $this->myView("page/privacy_policy_view", $data);
    }

    public function contactusform($array)
    {
        extract($array);
        $subjectEmail = "Contact Us Form - StageGator";
        $data['name'] = $name;
        $data['email'] = $email;
        $data['subject'] = $subject;
        $data['message'] = $message;
        $messageEmail = $this->load->view("email_templates/contact_us_email",$data,true);
        $to = "john@stagegator.com";
        //$to = "alex.test1990@gmail.com";
        // $header = "From: StageGator <noreply@stagegator.com>\r\n";
        // $header .= "MIME-Version: 1.0\n";
        // $header .= "Content-Type: text/html; charset=\"iso-8859-1\"\n";
        // $header .= "X-Priority: 1 (Highest)\n";
        // $header .= "X-MSMail-Priority: High\n";
        // $header .= "Importance: High\n";
        // @mail($to, $subjectEmail, $messageEmail, $header);
        $data ["to"] = $to;
        $data ["from_title"] = "StageGator";
        $data ["message"] = $messageEmail;
        $data ["subject"] = $subjectEmail;
        $this->utility->sendMailSMTP($data);
    }
    
    public function help(){
        $data ["title"] = "Help";
        $this->myView("page/page_help_view", $data);
    }
}
