<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Apple_payment extends Guest_controller
{
    public $myInfo = "";

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->applePaymentDetails;
        $this->folder = $this->myInfo ["controller"];
        // $this->load->model($this->myInfo ["model"], "this_model");
    }

    /**
     * subscribePlan()
     * This method view new load to pay customer and create a subscribtion account for apple pay (apple device
     * only) , used for generate the all session and redirect to the subscription url 
     *
     * @param $newPlanId is new plan's encrypted ID
     *       
     * @return String | return to a view
     */
    public function subscribePlan($newPlanId = "", $accessToken = "")
    {
        $newPlanId = $this->utility->decode($newPlanId);
        if (! ctype_digit($newPlanId)) {
            show_404();
            exit();
        }
        
        $tokenArray = explode("||||", $this->utility->decode($accessToken));
        
        if (isset($tokenArray [0]) && isset($tokenArray [1]) && ! empty($tokenArray [0]) && ! empty($tokenArray [1])) {
            // $fcmTokenData = $tokenArray [0];
            // time stemp data
            $oldTimeStempData = $tokenArray [0];
            $currentTimeStempData = strtotime($this->my_model->datetime);
            $userId = $tokenArray [1];
            $resultUserData = $this->main_model->getUserInfo($userId);
            
            $userDataFcmToken = $resultUserData->fcm_iphone;
            
            // compare Timestemp data & make difference in minutes
            
            $interval = abs($currentTimeStempData - $oldTimeStempData);
            $timeInMinutes = round($interval / 60);
            
            // if difference is less then 10 minutes then do payment
            if ($timeInMinutes <= 10) {
                
                $resultUserData->iscompleted = 1;
                
                /* $this->session->set_userdata("UserData", $sessionData); */
                $this->session->set_userdata('newPlanId', $newPlanId);
                
                $getRedirect = '';
                
                /* if Admin login that time we set admin login url becoz this is a deiffrent module */
                if ($resultUserData->idGrp == ADMIN_GROUP) {
                    $getRedirect = SITEURL_ADMIN . $this->myvalues->dashboardDetails ['controller'];
                }
                /* if fan will login then redirect to upcomming shows */
                elseif ($resultUserData->idGrp == FAN_GROUP) {
                    $getRedirect = SITEURL . $this->myvalues->eventDetails ['controller'] . "/upcoming";
                }
                /* if performer login then redirect to my shows page */
                elseif ($resultUserData->idGrp == PERFOMER_GROUP) {
                    $getRedirect = SITEURL . $this->myvalues->performerDetails ['controller'] . "/shows";
                }
                /* if venue login then redirect to manage schedule page */
                elseif ($resultUserData->idGrp == VENUE_GROUP) {
                    $getRedirect = SITEURL . $this->myvalues->eventDetails ['controller'];
                }
                
                $resultUserData->dashboardURL = $getRedirect;
                // generate session
                
                $this->session->set_userdata("UserData", $resultUserData);
                
                // make new session varible for display apple pay in view
                $this->session->set_userdata('loadview', 'IphoneView');
                
                // pr($this->session->userdata());
                $url = SITEURL . $this->myvalues->subscriptionDetails ["controller"] . '/subscribePlan/' . $this->utility->encode($newPlanId);
                // var_dump($url);
                
                redirect($url);
            }
            else {
                // Show error for invalid page request. please try gain from app.
                $data ["title"] = "Plan Subscription";
                $this->session->set_userdata('subscription_status', 'error');
                //$this->utility->setFlashMessage('danger', $this->lang->line('error_payment_request'));
                $this->myView($this->myvalues->subscriptionDetails ["controller"] . "/subscription_plan_success_error_view", $data);
                
                //show_404();
                //exit();
            }
        }
        else {
            show_404();
            exit();
        }
    }
    

    /**
     * Used for display the cancelled payment view
     */
    function subscription_cancelled()
    {
        $data ["title"] = "Plan Subscription";
        $this->session->set_userdata('subscription_status', 'cancelled');
        $this->myView($this->myvalues->subscriptionDetails ["controller"] . "/subscription_plan_success_error_view", $data);
    }
    
}
