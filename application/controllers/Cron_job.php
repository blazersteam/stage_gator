<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Cron_job extends Guest_controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->cronDetails;
        $this->load->model($this->myInfo ["model"], "this_model");
        $this->load->helper('stripe_connect_helper');
    }
    
    public function index()
    {
      
        $c = $r = 0;
        $i = 1;
        echo $date = date("Y-m-d",strtotime(date("Y-m-d")."-1 days"));
        echo "<br>";
        //get performer list
        $performerList = $this->this_model->getEventTimetableForCron($date,'performed');
        foreach ($performerList as $performer){
          $performerDate = date("Y-m-d H:i:s",strtotime($performer->date.$performer->start_time));
            //get recurring tip info where recurring tip type is only 'performance'
            $recurringInfo = $this->this_model->getRecurringTipInfoByPerformer($performer->idPerformer, $performerDate);
            
            if(!empty($recurringInfo)){
                
                //tip amount
                $tipAmount = $recurringInfo[0]->amount;
                // user id of performer
                $perfomerUserid = $performer->idPerformer;
                //user od of fan
                $fanInfoUserid = $recurringInfo[0]->fans_id;
                //load plan detail model for get plan information
                $this->load->model($this->myvalues->subscriptionDetails ['model'], 'subscriptionModel');
                $planid = $this->subscriptionModel->getCurrentPlanDetails($perfomerUserid, 3);
                //load profile detail model for get profile information
                $this->load->model($this->myvalues->profileDetails ['model'], 'profileModel');
                $userInfo = $this->profileModel->getUserInformation($perfomerUserid);
                // fetch the stripe id of performer
                $accountId = $userInfo->stripe_account_id;
                // fetch the detail of fan
                $fanInfo = $this->profileModel->getUserInformation($fanInfoUserid);
                
                $description = "Tip from :" . $fanInfo->chrName;
                $statement_descriptor = "Stagegator Tip";
                /* pssing both performer and fan id as metadata to stripe */
                $metadata = [
                    'performerId' => $perfomerUserid,
                    'fanId' => $fanInfo->user_id
                ];
                
                //get stripe secret key based on performer id
                $this->load->model($this->myvalues->tippingDetails['model'], 'tippingDetails');
                $stripsecretkey = $this->tippingDetails->getStripeKey($perfomerUserid);
                
                //get Strip Customer Info from 'fans_performer_sxtrip_info'
                $fansPerformerStripCustomerInfo = $this->tippingDetails->getFanPerformerStripInfo($fanInfo->user_id, $perfomerUserid);
                $stripCustomerId = $fansPerformerStripCustomerInfo[0]->stripe_customer_id;
                
                // round the application fee for stripe, also added the additional charges accrding to plan
                $applicationFee = round(($planid [0]->tips_percentage * $tipAmount) / 100 + $planid [0]->tips_additional, 2);
                
                // done payment using Customer Id
                $resultPayment = chargeStripeCustomerByCustomerID($accountId, $stripsecretkey, $stripCustomerId, $tipAmount, $description, "usd", $applicationFee, $metadata, $statement_descriptor);
                
                // make the entry in log table
                if($resultPayment['status']){
                    $this->tippingDetails->makeTipplingHistoryLog($fanInfo->user_id, $perfomerUserid, $tipAmount, $resultPayment['chargeId'], 'Pending');
                    $c++;
                }
                $i++;
                $r +=count($recurringInfo);
                // echo "<pre>";
                //   print_r($resultPayment);
                // echo "</pre>";
            }
        }
        
        // echo "<pre>";
        // print_r($performerList);
        // echo "</pre>";
        echo "Total Performer who is performed => ".count($performerList)."<br/>";
        echo "Total recurring tip info by perfomer  => ".$r."<br/>";
        echo "Total Suceefully charge => ".$c;

        die;
    }
    
}
