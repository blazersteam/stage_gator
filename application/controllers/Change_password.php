<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Change_password extends General_controller
{
    public $myInfo = "";
    public $myInfoRoutes = "";

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->change_passwordDetails;
        $this->load->model($this->myInfo ["model"], "this_model");
    }

    /**
     * index()
     * This is default index method when page load
     */
    public function index()
    {
        $this->changePasswordUser();
    }

    /**
     * setRules()
     * This function is used to set rules for form
     */
    public function setRulesforChangePassword()
    {
        if ($this->input->post('change_email')) {
            $this->rules [] = [
                "field" => "txtNew_email",
                "label" => $this->lang->line('email_address'),
                "rules" => "trim|required|valid_email|max_length[250]",
                "errors" => [
                    'required' => $this->lang->line('error_please_enter_email_address'),
                    'valid_email' => $this->lang->line('error_please_enter_valid_email_address'),
                    'max_length' => $this->lang->line('error_email_address_maxlength')
                ]
            ];
        }
        else {
            $this->rules [] = [
                "field" => "txtOld_password",
                "label" => $this->lang->line('password'),
                "rules" => "trim|required|callback_oldpasswordCheck",
                "errors" => [
                    'required' => $this->lang->line('error_please_enter_password'),
                    'oldpasswordCheck' => $this->lang->line('oldpasswordCheck')
                ]
            ];
            
            $this->rules [] = [
                "field" => "txtPassword",
                "label" => $this->lang->line('password'),
                "rules" => "trim|required|callback_passwordCheck|max_length[32]|min_length[6]",
                "errors" => [
                    'required' => $this->lang->line('error_please_enter_password'),
                    'passwordCheck' => $this->lang->line('error_no_space_allowed'),
                    'max_length' => $this->lang->line('error_max_length_password'),
                    'min_length' => $this->lang->line('error_min_length_password')
                ]
            ];
            
            $this->rules [] = [
                "field" => "txtConfirmPassword",
                "label" => $this->lang->line('confirm_password'),
                "rules" => "trim|matches[txtPassword]",
                "errors" => [
                    'matches' => $this->lang->line('error_confirm_password_notmatched')
                ]
            ];
        }
    }

    /**
     * passwordCheck remove whitespace from password
     *
     * @param string $str is passowrd
     *       
     * @return boolean ture or false
     */
    public function passwordCheck($str)
    {
        if (preg_match('/\s/', $str)) {
            return false;
        }
        return true;
    }

    /**
     * oldpasswordCheck()
     * this method check old password from user table
     *
     * @param unknown $password #return boolean true or flase
     */
    public function oldpasswordCheck()
    {
        return $this->this_model->checkPassword($this->pUserId);
    }

    /**
     * changePasswordUser(0
     * This method used for either change username or password
     *
     * @return return to view
     */
    public function changePasswordUser()
    {
        if ($this->input->post()) {
            $this->setRulesforChangePassword();
            $valid = $this->validateData($this->rules);
            if ($valid) {
                
                if ($this->input->post('change_email')) {
                    // if getting email in post then we update user email
                    $msg = $this->this_model->updateUserEmail($this->pUserId);
                                       
                    //$this->session->unset_userdata('UserData');
                    $this->utility->setFlashMessage($msg [0], $this->lang->line($msg [1]));
                    //redirect(SITEURL . $this->myvalues->loginDetails ['controller']);
                }
                else {
                    // if not getting email then we update password
                    $msg = $this->this_model->updateUserPassoword($this->pUserId);
                    $this->utility->setFlashMessage($msg [0], $this->lang->line($msg [1]));
                    redirect(SITEURL . $this->myInfo ['controller']);
                }
            }
        }
        
        $data ["title"] = $this->lang->line("change_account_credentials");
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["registerUrl"] = SITEURL . $this->myInfo ["controller"] . '/changePasswordUser';
        $data ['user'] = $this->this_model->getUserInformation($this->pUserId);
        $this->myView("change_password_view", $data);
    }
}
