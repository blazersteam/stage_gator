<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Timetable extends General_controller
{
    public $myInfo = "";

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->timeTableDetails;
        $this->folder = $this->myInfo ["controller"];
        $this->load->model($this->myInfo ["model"], "this_model");
    }

    /**
     * This method loads all schedules generated for given time slot from venue and loads with users
     */
    function index()
    {
        $scheduleEventId = $this->utility->decode($this->input->get("eventId"));
        
        $this->load->model($this->myvalues->eventDetails ['model'], 'event_model');
        if (! empty($scheduleEventId)) {
            // check if event schedule is deleted
            
            $checkEventSchduleDelete = $this->event_model->CheckEventSchduleDelete($scheduleEventId);
            
            if ($checkEventSchduleDelete == 0) {
                $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
                $this->load->library('user_agent');
                redirect($this->agent->referrer());
            }
            // this function used for check if event date passed
            $this->main_model->CheckEventPassed($scheduleEventId);
        }
        
        $eventInfo = $this->event_model->getEventSchedule($scheduleEventId);
        $eventDateEmail = $eventInfo->start_date . " " . $eventInfo->startTime;
        $eventDateEmail = convertFromGMT($eventDateEmail, "default", $eventInfo->timeZone);
        $eventDateEmail = date("jS M, Y", strtotime($eventDateEmail)) . " (" . $eventInfo->timeZone . ")";
        $data ["title"] = "Event Schedule " . $eventInfo->title . " " . $eventDateEmail;
        
        $data ["list"] = $this->this_model->getEventTimetable($scheduleEventId,'venue');
        
        //Running/Performed Event Data List
        $data ["runningList"] = $this->this_model->getRunningEventTimetable($scheduleEventId);
        
        $data ["eventId"] = $this->input->get("eventId");
        
        /* For checking that performer is already allotted time or not */
        $list = json_decode(json_encode($data ["list"]), true);
        
        /* Checks if it is found in array then we will show Trade button else not */
        $data ["isAssigned"] = array_search($this->pUserId, array_column($list, "idPerformer"));
        
        /* Checks if user has already sent trading request or not */
        $data ["tradeStatus"] = $this->this_model->checkTradePending($this->pUserId, $scheduleEventId);
        
        /* check if user is host and have do list permission that is assigned by venue user */
        if ($this->pidGroup == "3") {
            $data ["listPermission"] = $this->this_model->CheckListPermission($this->pUserId, $scheduleEventId);
        }
        else {
            $data ["listPermission"] = 'Yes';
        }
        /* Loads Accepted entries of Performers from register table for Venue */
        if ($this->pidGroup == "2" || $data ["listPermission"] == 'Yes') {
            $data ["performers"] = $this->this_model->getAcceptedUsersForTimetable($scheduleEventId);
        }
        
        $data ["controllerName"] = $this->myInfo ["controller"];
        $this->myView($this->folder . "/timetable_view", $data);
    }

    /**
     * This funtction will be called when performer will reserve any slot for him self.
     *
     * @param int $slotId - id of slot which he/she want to reserve
     */
    public function reserve($eventId = "", $slotId = "", $userId = "")
    {
        if (! empty($eventId) && ! empty($slotId) && is_numeric($this->utility->decode($slotId)) && is_numeric($this->utility->decode($eventId))) {
            
            if (! empty($this->utility->decode($eventId))) {
                // check if event schedule is deleted
                $this->load->model($this->myvalues->eventDetails ['model'], 'event_model');
                $checkEventSchduleDelete = $this->event_model->CheckEventSchduleDelete($this->utility->decode($eventId));
                
                if ($checkEventSchduleDelete == 0) {
                    $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
                    $this->load->library('user_agent');
                    redirect($this->agent->referrer());
                }
                
                // this used for check if event date passed(expired)
                $resultEventData = $this->event_model->getEventSchedule($this->utility->decode($eventId));
                
                // if event time passed then return true
                if (event_end_time_crossed($resultEventData->end_date, $resultEventData->endTime)) {
                    $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
                    
                    if ($this->pidGroup == VENUE_GROUP) {
                        redirect(SITEURL . $this->myvalues->eventDetails ['controller'] );
                    }
                    else if ($this->pidGroup == PERFOMER_GROUP) {
                        redirect(SITEURL . $this->myvalues->performerDetails ['controller'] . "/shows");
                    }
                }
            }
            
            $userId = $userId == "" ? $this->pUserId : $this->utility->decode($userId);
            $result = $this->this_model->reserveSlot($userId, $this->utility->decode($eventId), $this->utility->decode($slotId));
            
            if ($result === false) {
                $this->utility->setFlashMessage('danger', $this->lang->line('error_record_not_updated'));
            }
            else if ($result === true || $result === - 1) {
                if ($this->pidGroup == "2" || $userId != $this->pUserId) {
                    // not an interval
                    if($userId != -1)
                    {
                        $this->utility->setFlashMessage('success', "Time reserved for performer successfully.");
                    }else{
                        // set interval that time
                        $this->utility->setFlashMessage('success', "Time reserved for intermission successfully.");
                    }
                }
                else {
                    $customEncoding = $this->utility->encode(0 . "||||" . $this->utility->decode($eventId));
                    $this->utility->setFlashMessage('success', "And... You're booked! Consider sharing this exciting news with your fans on Facebook and Twitter Doing so on ranked events will add a star to your rank!");
                    return redirect(SITEURL . $this->myInfo ["controller"] . '/getRedirectOnSideChanges/' . $eventId . '/' . $slotId);
                }
            }
            redirect(SITEURL . $this->myInfo ["controller"] . '?eventId=' . $eventId);
        }
        else {
            show_404();
            exit();
        }
    }

    /**
     * resetSlot()
     * This function will reset slot by removing performId from there
     *
     * @param string $slotId
     */
    public function resetSlot($eventId = "", $slotId = "", $userId = "")
    {
        if (! empty($eventId) && ! empty($slotId) && is_numeric($this->utility->decode($slotId)) && is_numeric($this->utility->decode($eventId))) {
            
            if (! empty($this->utility->decode($eventId))) {
                // check if event schedule is deleted
                $this->load->model($this->myvalues->eventDetails ['model'], 'event_model');
                $checkEventSchduleDelete = $this->event_model->CheckEventSchduleDelete($this->utility->decode($eventId));
                
                if ($checkEventSchduleDelete == 0) {
                    $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
                    $this->load->library('user_agent');
                    redirect($this->agent->referrer());
                }
                
                // this used for check if event date passed(expired)
                
                $resultEventData = $this->event_model->getEventSchedule($this->utility->decode($eventId));
                
                // if event time passed then return true
                if (event_end_time_crossed($resultEventData->end_date, $resultEventData->endTime)) {
                    $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
                    
                    if ($this->pidGroup == VENUE_GROUP) {
                        redirect(SITEURL . $this->myvalues->eventDetails ['controller'] );
                    }
                    else if ($this->pidGroup == PERFOMER_GROUP) {
                        redirect(SITEURL . $this->myvalues->performerDetails ['controller'] . "/shows");
                    }
                }
            }
            
            $userId = $userId == "" ? $this->pUserId : $this->utility->decode($userId);
            $result = $this->this_model->resetSlot($userId, $this->utility->decode($slotId), $this->utility->decode($eventId));
            if ($result) {
                $this->utility->setFlashMessage('success', "Booking has been cancelled as per your request. Please reserve again as per your suitable timings.!");
            }
            else {
                $this->utility->setFlashMessage('danger', $this->lang->line("error_default_error"));
            }
            redirect(SITEURL . $this->myInfo ["controller"] . '?eventId=' . $eventId);
        }
        else {
            show_404();
            die();
        }
    }

    public function tradeSlot($idEvent = '', $slotId = '')
    {
        if (! empty($idEvent) && ! empty($slotId) && is_numeric($this->utility->decode($slotId)) && is_numeric($this->utility->decode($idEvent))) {
            if (! empty($this->utility->decode($idEvent))) {
                // check if event schedule is deleted
                $this->load->model($this->myvalues->eventDetails ['model'], 'event_model');
                $checkEventSchduleDelete = $this->event_model->CheckEventSchduleDelete($this->utility->decode($idEvent));
                
                if ($checkEventSchduleDelete == 0) {
                    $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
                    $this->load->library('user_agent');
                    redirect($this->agent->referrer());
                }
                
                // this used for check if event date passed(expired)
                
                $resultEventData = $this->event_model->getEventSchedule($this->utility->decode($idEvent));
                
                // if event time passed then return true
                if (event_end_time_crossed($resultEventData->end_date, $resultEventData->endTime)) {
                    $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
                    if ($this->pidGroup == PERFOMER_GROUP) {
                        redirect(SITEURL . $this->myvalues->performerDetails ['controller'] . "/shows");
                    }
                }
            }
            
            $result = $this->this_model->doTrade($this->utility->decode($idEvent), $this->utility->decode($slotId), $this->pUserId);
            if ($result == 0) {
                $this->utility->setFlashMessage('danger', 'Trade Request Already Sent');
            }
            else if ($result == 1) {
                $this->utility->setFlashMessage('success', 'Trade Request Sent');
            }
            else if ($result == 2) {
                $this->utility->setFlashMessage('danger', "Action Couldn't Be Completed");
            }
            redirect(SITEURL . $this->myInfo ["controller"] . "?eventId=" . $idEvent);
        }
        else {
            show_404();
            exit();
        }
    }

    /**
     * getRedirectOnSideChanges()
     * This method load event realted details when performer reserve a time
     *
     * @param interger $idEvent is event id
     * @param integer $slotId
     */
    public function getRedirectOnSideChanges($idEvent, $slotId)
    {
        $this->load->library('share');
        $data ["title"] = "Performing Details";
        $idEvent = $this->utility->decode($idEvent);
        $slotId = $this->utility->decode($slotId);
        $data ['result'] = $this->this_model->getEventDetails($idEvent, $slotId);
        $this->myView($this->folder . '/timetable_event_details_view', $data);
    }
    
    
    /**
     * changeStatus()
     * This function will change Status of sheduling performer of event
     *
     * @param $slotId string encode formate, slotId of event
     * @param $eventId string encode formate, event Id
     * @param $userId string encode formate, perfomer timetable primery id
     * @param $status string encode formate, status of performer
     */
    public function changeStatus($eventId = "", $slotId = "", $userId = "", $status="", $controller="",$access="")
    {
        
        if (! empty($eventId) && ! empty($slotId) && is_numeric($this->utility->decode($slotId)) && is_numeric($this->utility->decode($eventId))) {
            
            if (! empty($this->utility->decode($eventId))) {
                // check if event schedule is deleted
                $this->load->model($this->myvalues->eventDetails ['model'], 'event_model');
                $checkEventSchduleDelete = $this->event_model->CheckEventSchduleDelete($this->utility->decode($eventId));
                
                if ($checkEventSchduleDelete == 0) {
                    $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
                    $this->load->library('user_agent');
                    redirect($this->agent->referrer());
                }
                
                // this used for check if event date passed(expired)
                
                $resultEventData = $this->event_model->getEventSchedule($this->utility->decode($eventId));
                
                // if event time passed then return true
                if (event_end_time_crossed($resultEventData->end_date, $resultEventData->endTime) && empty($controller)) {
                    $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
                    
                    if ($this->pidGroup == VENUE_GROUP) {
                        redirect(SITEURL . $this->myvalues->eventDetails ['controller'] );
                    }
                    else if ($this->pidGroup == PERFOMER_GROUP) {
                        redirect(SITEURL . $this->myvalues->performerDetails ['controller'] . "/shows");
                    }
                }
            }
            
            $userId = $userId == "" ? $this->pUserId : $this->utility->decode($userId);
            
            //Flash Message Depend on Status
            if($this->utility->decode($status) == 'onstage'){
                $msg = "Performer is Running !";
            }else if($this->utility->decode($status) == 'performed'){
                $msg = "Performer is Completed !";
            }else if($this->utility->decode($status) == 'upcoming'){
                $msg = "Performer is reset !";
            }
            
            $result = $this->this_model->changeStatus($userId, $this->utility->decode($slotId), $this->utility->decode($eventId), $this->utility->decode($status));
            if ($result) {
                $this->utility->setFlashMessage('success', $msg);
            }
            else {
                $this->utility->setFlashMessage('danger', $this->lang->line("error_default_error"));
            }
            if($controller == 'share'){
                redirect(SITEURL . $controller . '/event?access=' . $access);
            }else{
                redirect(SITEURL . $this->myInfo ["controller"] . '?eventId=' . $eventId);
            }
            
        }
        else {
            show_404();
            die();
        }
    }
}
