<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Performer extends General_controller
{
    public $myInfo = "";

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->performerDetails;
        $this->folder = $this->myInfo ["controller"];
        $this->load->model($this->myInfo ["model"], "this_model");
        $this->eventInfo = $this->myvalues->eventDetails;
    }

    /**
     * *
     * bulkInvite()
     * This method find invite performer and send an notification mail them
     *
     * @param integer $id is the event id
     *       
     * @param return to view after getting result
     */
    function bulkInvite($id = NULL)
    {
        $id = $this->utility->decode($id);
        if (ctype_digit($id)) {
            
            // this function used for check if event date passed            
            $this->main_model->CheckEventPassed($id);
            
            //check if event schedule is deleted 
            $this->load->model($this->myvalues->eventDetails ['model'], 'event_model');
            $checkEventSchduleDelete = $this->event_model->CheckEventSchduleDelete($id);
                        
            if ($checkEventSchduleDelete == 0) {
                $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
                $this->load->library('user_agent');             
                redirect($this->agent->referrer());
            }
            
            /* get event information by event id */
            $data ["event"] = $this->this_model->getEventBySchuduleId($id);
            
            if (! isset($data ["event"]->idVenue) || $data ["event"]->idVenue != $this->pUserId || (event_end_time_crossed($data ["event"]->endDate, $data ["event"]->endTime))) {
                
                $this->utility->setFlashMessage('danger', $this->lang->line('un_authorized_access'));
                redirect(SITEURL . $this->eventInfo ['controller']);
            }
            /* if get some data in post then call a model method and send a notification mail to user */
            if ($this->input->post("BulkInvite")) {
                if ($this->input->post("chkstatus")) {
                    
                    /* If VENUE login then we will check its monthly limit for performers according to its plan */
                    if ($this->pidGroup == VENUE_GROUP) {
                        $planInfo = $this->subscription_model->getCurrentPlanDetails($this->pUserId, $this->pidGroup);
                        
                        if ($planInfo [0]->performers == "0") {
                            $this->utility->setFlashMessage('danger', $this->lang->line('forbidden_function') . " Please upgrade your plan ! <a href='".SITEURL . $this->myvalues->subscriptionDetails["controller"] ."'>Click Here</a>");
                            redirect(SITEURL . $this->myInfo ['controller'] . "/bulkInvite/" . $this->utility->encode($id));
                        }
                        elseif ($planInfo [0]->performers != "-1") {
                            $this->load->model($this->myvalues->invitationDetails["model"], "invitation_model");
                            $counts = $this->invitation_model->getAcceptPendingCount($this->pUserId, $id);
                            $newTotal = ($counts + count($this->input->post("chkstatus"))); 
                            if ( $planInfo [0]->performers - $counts == 0) {
                                $this->utility->setFlashMessage('danger', "You have reached your monthly allowed performer's limit. Please upgrade your plan ! <a href='".SITEURL . $this->myvalues->subscriptionDetails["controller"] ."'>Click Here</a>");
                                redirect(SITEURL . $this->myInfo ['controller'] . "/bulkInvite/" . $this->utility->encode($id));
                            }
                            elseif ($newTotal > $planInfo [0]->performers) {
                                $displayTotal = $planInfo [0]->performers - $counts;
                                $this->utility->setFlashMessage('danger', "You can invite only ". $displayTotal ." performers. Please upgrade your plan ! <a href='".SITEURL . $this->myvalues->subscriptionDetails["controller"] ."'>Click Here</a>");
                                redirect(SITEURL . $this->myInfo ['controller'] . "/bulkInvite/" . $this->utility->encode($id));
                            }
                        }
                    }
                    
                    $r = $this->this_model->sendBulkInvite($id);
                    $this->utility->setFlashMessage('success', $this->lang->line('performers_successfully_invited'));
                    redirect(SITEURL . $this->eventInfo ['controller']);
                }
                else {
                    $this->utility->setFlashMessage('danger', "Please select atleast one performer.");
                    redirect(SITEURL . $this->myInfo ['controller'] . "/bulkInvite/" . $this->utility->encode($id));
                }
            }
            // $user = $this->user;
            $data ["eventId"] = $id;
            $data ["styles"] = $this->this_model->getPerformerStyleMatchingWithCurrentEvent($id);
            $data ['controllerName'] = $this->myInfo ['controller'];
            $data ["alreadyInQueue"] = $this->this_model->getPerformersInQueue($id);
            $data ["result"] = $this->this_model->searchPerformer($data ["styles"]);
            $data ["title"] = $this->lang->line("bulk_invite") . ' ' . ucwords($data ["event"]->title);
            $this->myView("event/bulk_invite", $data);
        }
        else {
            $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
            redirect(SITEURL . $this->eventInfo ['controller']);
        }
    }

    /**
     * searchPerformer()
     * This method returns list of performer as per search input or as per user
     *
     * @param $this->input->post() is user search input request
     * @param $this->user is current user session object
     *       
     * @return get performer list and load view
     */
    function searchPerformer()
    {
        
        // $data ["styles"] = $this->user_model->get_performer_style_matching_with_venue ( $data ["user"]->user_id
        // );
        $data ["styles"] = $this->main_model->selectStyleMatchingWithAllUser($this->pUserId, PERFOMER_GROUP, 'venue_style');
        $data ["result"] = $this->this_model->searchPerformer($data ["styles"]);
        $data ['controllerName'] = $this->myInfo ['controller'];
        $data ["title"] = $this->lang->line('search_performers');
        $data ["page"] = $this->myView($this->eventInfo ['controller'] . "/search_performer", $data);
    }

    /**
     * followUnfollowPerformer()
     * This method bite or unbite a per former
     *
     * @param $this->user->user_id is current user session id
     * @param $this->input->get("id") is the performer id
     *       
     * @return all performer data to view
     */
    function followUnfollowPerformer($id = NULL)
    {
        $id = $this->utility->decode($id);
        if (ctype_digit($id)) {
            // call model method with two params and get performer list
            $r = $this->this_model->followUnfollowPerformer($id, $this->pUserId);
            if (! $r) {
                $this->utility->setFlashMessage('success', $this->lang->line('user_unbitten'));
            }
            else {
                $this->utility->setFlashMessage('success', $this->lang->line('user_bitten'));
            }
        }
        else {
            $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
        }
        redirect(SITEURL . $this->myInfo ['controller'] . '/searchPerformer');
    }

    /**
     * overview()
     * this method will show performer information
     *
     * @param unknown $id
     */
    public function overview($id)
    {
        $id = $this->utility->decode($id);
        if (ctype_digit($id)) {
            
            $this->load->model($this->myvalues->profileDetails ['model'], 'profile_model');
            $data ["title"] = $this->lang->line('profile_overview');
            $data ["controllerName"] = $this->myInfo ["controller"];
            $data ['userInfo'] = $this->profile_model->getUserInformation($id);
             
            // if user info not found then not show 404.
            if(empty($data ['userInfo'])){
                show_404();
                exit();
            }
            
            $data ['userStyle'] = $this->profile_model->getUserStyleForOverview($id);
            // $data ["registerUrl"] = SITEURL . $this->myInfo ["controller"] . '/updateprofile';
            if ($data ["userInfo"]->idGrp == 2) {
                $this->load->model($this->myvalues->eventDetails ['model'], 'event_model');
                $data ["list"] = $this->event_model->get_event_list($id, true);
            }
            else {
                $this->load->model($this->myvalues->performerDetails ['model'], 'performer_model');
                $data ["list"] = $this->performer_model->get_performer_events($id);
            }
            
            $this->myView($this->myvalues->profileDetails ['controller'] . "/profile_overview_view", $data);
        }
        else {
            redirect(SITEURL . $this->myInfo ['controller'] . '/searchPerformer');
        }
    }

    public function index()
    {}

    public function add()
    {}

    public function edit()
    {}

    public function delete()
    {}

    /**
     * Used for display the all the event Shows related to login perfomer
     */
    public function shows()
    {
        $data ["title"] = $this->lang->line('my_shows');
        $data ["controllerName"] = $this->myInfo ["controller"];
        
        // this is the event schedule ids ,which has performer have remaining to rank to venue user. 
        $data ["pending_rankings"] = $this->this_model->get_events_pending_for_ranking_by_performer($this->pUserId);
           
        // this is the event schedule ids ,which has (HOST)performer have remaining to rank to Other perfomer user.
        $data ["host_give_ranking"] = $this->this_model->get_events_panding_ranking_by_host($this->pUserId);
        
        $data ["list"] = $this->this_model->get_performer_events($this->pUserId);
        $this->myView($this->myInfo ["controller"] . "/perfomer_myshow_view", $data);
    }

    /**
     * Used for cancelled the applied event
     *
     * @param string $idEvtScheduleId , incoded the scheduled id
     * @param string $idEvent, incoded evnt id
     *       
     */
    function cancelApplication($idEvtScheduleId = "", $idEvent = "")
    {
        $idScheduleId = $this->utility->decode($idEvtScheduleId);
        $idEvent = $this->utility->decode($idEvent);
        
        if (! ctype_digit($idScheduleId) || ! ctype_digit($idEvent)) {
            show_404();
            exit();
        }
        
        $r = $this->this_model->cancel_application($this->pUserId, $idScheduleId, $idEvent);
        
        if ($r == 0) {
            // $this->session->set_flashdata('red_msg','Record Not Updated!');
            $this->utility->setFlashMessage('danger', $this->lang->line('error_record_not_updated'));
        }
        else {
            // $this->session->set_flashdata('green_msg','Record Updated Successfully!');
            $this->utility->setFlashMessage('success', $this->lang->line('record_updated_successfully'));
        }
        
        redirect(SITEURL . $this->myInfo ["controller"] . '/shows');
    }

    /**
     * Used for display ratting functionality after the event completed
     *
     * @param string $eventId, encoded event id
     */
    function displayVenueDataForRating()
    {
        $eventScheduleId = $this->input->post("eventScheduleId");
        
        $eventScheduleId = $this->utility->decode($eventScheduleId);
        
        if (! is_numeric($eventScheduleId)) {
            show_404();
            exit();
        }
        
        $resultData = $this->this_model->getEventBySchuduleId($eventScheduleId);
        
        $this->load->model($this->myvalues->eventDetails ['model'], 'event_model');
        $resultEventData = $this->event_model->getEventById($resultData->idEvent);
        
        if (! empty($resultEventData)) {
            die(json_encode([
                "venueName" => $this->utility->decodeText($resultEventData->chrName),
                "eventScheduleId" => $this->utility->encode($eventScheduleId),
                "venueUserid" => $this->utility->encode($resultEventData->idVenue),
                "status" => "success"
            ]));
        }
        else {
            die(json_encode([
                "status" => "error"
            ]));
        }
    }

    /**
     * used for do ranking functionality after event completed by performer
     *
     * @param int score, that contain the rating value
     * @param string $event_id, encoded event id
     * @param string $venue_id , encoded venue id
     *       
     */
    function rankToVenue()
    {
        
        // pr($this->input->post());
        $rating = $this->input->post('score');
        if (empty($rating)) {
            $rating = 0;
        }
        $eventSchedule_id = $this->utility->decode($this->input->post('eventSchedule_id'));
        $venue_id = $this->utility->decode($this->input->post('venue_id'));
        
        $r = $this->this_model->rankToVenue($rating, $eventSchedule_id, $venue_id, $this->pUserId);
        
        if ($r == 0) {
            // $this->session->set_flashdata('red_msg', 'Record Not Updated!');
            $this->utility->setFlashMessage('danger', $this->lang->line('error_record_not_updated'));
        }
        else {
            // $this->session->set_flashdata('green_msg', 'Record Updated Successfully!');
            $this->utility->setFlashMessage('success', $this->lang->line('record_updated_successfully'));
        }
        
        redirect($_SERVER['HTTP_REFERER']);
        
    }
}
