<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Resources extends MY_controller
{
    public $myInfo = "";

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->resourcesDetails;
    }

    /* script disable then exit the whole sysyem */
    public function scriptdisable()
    {
        echo " Javascript support is required to view this site and your browser has disabled it. If you wish use the site, please enable javascript and reload the page  !!!";
        echo "<p>OOPS !! .. Looks like you lost :(</p><h5><a href=" . SITEURL . ">Let me guide you home :)</a></h5>";
        exit();
    }

    /**
     * getStateList()
     * This method return response to ajax request
     *
     * @param unknown $id
     * @return string Response return html options with city data
     */
    public function getStateList($id)
    {
        $data ["state"] = $this->main_model->getState($id);
        
        $this->load->view("ajax/state_list", $data);
    }

    /**
     * getCityList()
     * This method fetch all cities accortding to state and cities
     *
     * @param string $country is the country code
     * @param string state iso code $state
     * @return return a view to select option
     */
    function getCityList($country, $state)
    {
        $data ["cities"] = $this->main_model->getCityByState($country, $state);
        $this->load->view("ajax/city_list", $data);
    }
}
