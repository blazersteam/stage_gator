<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Location extends Venue_controller
{
    public $myInfo = "";

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->locationDetails;
        $this->folder = $this->myInfo ["controller"];
        $this->load->model($this->myInfo ["model"], "this_model");
    }

    public function index()
    {
        $data ["list"] = $this->this_model->getCurrentUserLocations($this->pUserId);
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data["title"] = "My Locations";
        $this->myView($this->folder . "/location_manage_view", $data);
    }

    /**
     * setRules()
     *
     * @param integer $extraFiled checks if user is registerd as a venue than we check extra fileds
     *        This function is used to set rules for form
     */
    public function setRules()
    {
        $this->rules [] = [
            "field" => "txtTitle",
            "label" => $this->lang->line('venue_name'),
            "rules" => "trim|required|max_length[250]",
            "errors" => [
                'required' => $this->lang->line('error_please_enter_user_name'),
                'max_length' => $this->lang->line('max_char_250')
            ]
        ];
        
        $this->rules [] = [
            "field" => "txtPhone",
            "label" => $this->lang->line('register_phone'),
            "rules" => "trim|required|callback_phoneValidation",
            "errors" => [
                'required' => $this->lang->line('error_please_enter_mobile'),
                'phoneValidation' => $this->lang->line('error_please_enter_a_valid_phone_number')
            ]
        ];
        $this->rules [] = [
            "field" => "cmbCountry",
            "label" => $this->lang->line('country'),
            "rules" => "required|max_length[250]",
            "errors" => [
                'required' => $this->lang->line('error_select_country')
            ]
        ];
        $this->rules [] = [
            "field" => "cmbState",
            "label" => $this->lang->line('state'),
            "rules" => "required|max_length[250]",
            "errors" => [
                'required' => $this->lang->line('error_select_state')
            ]
        ];
        $this->rules [] = [
            "field" => "cmbCity",
            "label" => $this->lang->line('city'),
            "rules" => "required|max_length[255]",
            "errors" => [
                'required' => $this->lang->line('error_select_city')
            ]
        ];
        
        $this->rules [] = [
            "field" => "txtAddress",
            "label" => $this->lang->line('address'),
            "rules" => "required|max_length[150]",
            "errors" => [
                'required' => $this->lang->line('error_address'),
                'max_length' => $this->lang->line('max_150_character_allowed')
            ]
        ];
        
//         $this->rules [] = [
//             "field" => "txtNumSeats",
//             "label" => $this->lang->line('number_of_seats'),
//             "rules" => "trim|required|integer|max_length[5]",
//             "errors" => [
//                 'required' => $this->lang->line('error_number_of_seats'),
//                 'max_length' => $this->lang->line('max_5_character_allowed')
//             ]
//         ];
    }

    /**
     * phoneValidation()
     * This method for us mobile number validation
     *
     * @param unknown $mob
     * @return boolean
     */
    function phoneValidation($mob)
    {
        if (preg_match('/^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/', $mob)) {
            
            return true;
        }
        
        return false;
    }

    /**
     * add()
     * this method add a new location of user in location table
     *
     * @return to load view view
     */
    public function add()
    {
        /* no one user can access this method apart from a venue */
        if($this->pidGroup != VENUE_GROUP){
            redirect($this->session->userdata('UserData')->dashboardURL);
        }
        $data ["title"]  = "Add New Location";
        if ($this->input->post()) {
            
            $this->setRules();
            $valid = $this->validateData($this->rules);
            
            if ($valid) {
                
                $dataId = $this->this_model->saveLocation(NULL, $this->pUserId);
                if ($dataId) {
                    $this->utility->setFlashMessage('success', $this->lang->line('location_added_successfully'));
                }
                else {
                    $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
                }
                redirect(SITEURL . $this->myInfo ["controller"]);
                // update data of user
            }
        }
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["actionUrl"] = SITEURL . $this->myInfo ["controller"] . '/add';
        $this->myView($this->folder . "/location_form_view", $data);
    }

    /**
     * edit()
     * This method edit user locations
     *
     * @param unknown $id
     */
    public function edit($id = "")
    {   
        if ($this->input->post()) {
            
            $this->setRules();
            $valid = $this->validateData($this->rules);
            if ($valid) {
                $id = $this->utility->decode($id);
                if (! ctype_digit($id)) {
                    show_404();
                    exit();
                }
                $this->this_model->saveLocation($id, $this->pUserId);
                $this->utility->setFlashMessage('success', $this->lang->line('location_update_successfully'));
                redirect(SITEURL . $this->myInfo ['controller'] . '/');
                // update data of user
            }
        }
        
        $res = $this->this_model->getCurrentUserLocationsById($this->utility->decode($id));
        $data ['record'] = $res [0];
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["actionUrl"] = SITEURL . $this->myInfo ["controller"] . '/edit/' . $id;
        $data ["title"]  = "Edit Location";
        $this->myView($this->folder . "/location_form_view", $data);
    }

    /**
     * delete()
     * This method delete location from location table
     *
     *
     * @param unknown $id
     */
    function delete($id = "")
    {
        $id = $this->utility->decode($id);
        if (! ctype_digit($id)) {
            show_404();
            exit();
        }
        $this->this_model->deleteUserLocation($id, $this->pUserId);
        $this->utility->setFlashMessage('success', $this->lang->line('location_deleted_successfully'));
        redirect(SITEURL . $this->myInfo ['controller'] . '/');
    }

    function search()
    {}

    function toggleStatus()
    {}
}
