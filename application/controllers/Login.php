<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends Guest_controller
{
    public $myInfo = "";
    public $myInfoRoutes = "";

    public function __construct()
    {
        parent::__construct();
        
        /* check user logged in */
        if ($this->session->userdata("UserData") && $this->router->fetch_method() != "resetPassword") {
            /* check dashbord url and redirect to it */
            if (! empty($this->session->userdata("UserData")->dashboardURL)) {
                redirect($this->session->userdata("UserData")->dashboardURL, "location");
            }
        }
        // load facebook library for login with facebook
       
        $this->load->library('Facebook_api');
        // set loginDetails to myinfo variable
        $this->myInfo = $this->myvalues->loginDetails;
        $this->eventInfo = $this->myvalues->eventDetails;
        $this->profileInfo = $this->myvalues->profileDetails;
        $this->registerInfo = $this->myvalues->userRegisterDetails;
        
        // load model
        $this->load->model($this->myInfo ["model"], "this_model");
        
        $this->controller = SITEURL . $this->myInfo ["controller"];
        $this->access_level = array();
    }

    /**
     * setRules()
     * This function is used to set rules for form
     */
    public function setRules()
    {
        $this->rules [] = [
            "field" => "txtEmail",
            "label" => $this->lang->line('email_address'),
            "rules" => "trim|required|valid_email|max_length[250]",
            "errors" => [
                'required' => $this->lang->line('error_please_enter_email_address'),
                'valid_email' => $this->lang->line('error_please_enter_valid_email_address'),
                'max_length' => $this->lang->line('error_email_address_maxlength')
            ]
        ];
        
        $this->rules [] = [
            "field" => "txtPassword",
            "label" => $this->lang->line('password'),
            "rules" => "trim|required|max_length[32]|min_length[6]",
            "errors" => [
                'required' => $this->lang->line('error_please_enter_password'),
                'max_length' => $this->lang->line('error_max_length_password'),
                'min_length' => $this->lang->line('error_min_length_password')
            ]
        ];
    }

    /**
     * setRulesEmail()
     * This function is used to set rules for form
     */
    public function setRulesEmail()
    {
        $this->rules [] = [
            "field" => "txtEmail",
            "label" => $this->lang->line('email_address'),
            "rules" => "trim|required|valid_email|max_length[250]",
            "errors" => [
                'required' => $this->lang->line('error_please_enter_email_address'),
                'valid_email' => $this->lang->line('error_please_enter_valid_email_address'),
                'max_length' => $this->lang->line('error_email_address_maxlength')
            ]
        ];
    }

    /**
     * setPasswordRules()
     * Passowrd validation server side
     */
    public function setPasswordRules()
    {
        $this->rules [] = [
            "field" => "txtPassword",
            "label" => $this->lang->line('password'),
            "rules" => "trim|required|callback_passwordCheck|max_length[32]|min_length[6]",
            "errors" => [
                'required' => $this->lang->line('error_please_enter_password'),
                'passwordCheck' => $this->lang->line('error_no_space_allowed'),
                'max_length' => $this->lang->line('error_max_length_password'),
                'min_length' => $this->lang->line('error_min_length_password')
            ]
        ];
        $this->rules [] = [
            "field" => "txtConfirm_password",
            "label" => $this->lang->line('confirm_password'),
            "rules" => "trim|matches[txtPassword]",
            "errors" => [
                'matches' => $this->lang->line('error_confirm_password_notmatched')
            ]
        ];
    }

    /**
     * passwordCheck remove whitespace from password
     *
     * @param string $str is passowrd
     *       
     * @return boolean ture or false
     */
    public function passwordCheck($str)
    {
        if (preg_match('/\s/', $str)) {
            return false;
        }
        return true;
    }

    /**
     * index()
     * This function is used to Login process using email, password and display login page
     *
     * @param string $emailAddress unique emailAddress for user
     * @param string $password password for user
     */
    public function index()
    {
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["addUserController"] = $this->registerInfo ["controller"];
        $data ['email'] = $this->utility->encodeText($this->input->post('txtEmail'));
        
        $data ["showMenu"] = false;
        $data ["noHeader"] = true;
        
        //$this->load->helper('cookie');
        // if cookie exist then update the cookie
        if ($this->input->get()) {
            
            // generate the cookie
            if (($this->input->get('device') == 'Android' || $this->input->get('device') == 'Iphone') && ! empty($this->input->get('token'))) {
                
                // if already exist thne update the cookie
                if (! empty($this->input->cookie('fcmIdToken'))) {
                    
                    // delete the old cookie
                    delete_cookie("fcmIdToken");
                }
                // generate the cookie
                $cookieValue = $this->utility->encode($this->input->get('device') . "||||" . $this->input->get('token'));
                
                $cookie = [
                    'name' => 'fcmIdToken',
                    'value' => $cookieValue,
                    'expire' => time() + 60 * 60 * 24 * 365
                ];
                $this->input->set_cookie($cookie);
            }
        }
        
        if ($this->input->post()) {
            
            // Change 1-5-17 show 404
            //show_404();
            
            $this->setRules();
            // validate post data
            $valid = $this->validateData($this->rules);
            // check server side validation for email
            
            if ($valid) {
                
                // get user email and password and get response from user table using login_model's method
                $result = $this->this_model->authenticate();
                
                // set validation rule
                if (count($result) == 0) {
                    // set flash message and load login view
                    // isUserLock method check if user has locked or not if locked it will redirect to login page
                    // with
                    // validation message
                    $msg = $this->this_model->userLockStatus($data ['email']);
                    
                    // redirected to login page
                    $this->utility->setFlashMessage('danger', $this->lang->line($msg));
                    redirect($this->controller);
                }
                else {
                    // a common method for generate users session and login them
                    $this->this_model->generateSession($result [0]);
                }
            }
        }
        
        $this->myView("login_view", $data);
    }

    /**
     * facebooklogin()
     * This method will get information of user who loggin in using facebook
     *
     * @return redirect on facebook and get response
     */
    public function facebooklogin($id = '')
    {
        /*
         * $data ['login_url'] = $this->facebook->getLoginUrl(array(
         * 'redirect_uri' => SITEURL . $this->myInfo ["controller"] . '/facebookCallBack/'.$id,
         * 'scope' => array(
         * 'email'
         * )
         * )); // permissions here
         */
        $data ['login_url'] = $this->facebook_api->connectFacebook(SITEURL . $this->myInfo ["controller"] . '/facebookCallBack/' . $id, array(
            'email'
        ));
        redirect($data ['login_url']);
    }

    /**
     * facebookCallBack()
     * Facebook call backfunction where we get user's data if user is exist we create new user otherwise direct
     * login them
     *
     * @param integer $id is group id it id of performer , fan or it can be a venue
     *       
     * @return if user is exist then logged in directly otherwise load view to login
     */
    public function facebookCallBack($id = '')
    {
        // set callback url
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["showMenu"] = false;
        $data ["noHeader"] = true;
        
        // get the user info related to facebook login user
        $user_profile = $this->facebook_api->getUser();
       
        if (! empty($user_profile ['user']) && empty($user_profile ['error_msg'])) {
            
            $this->session->set_userdata("facebook_details", $user_profile['user']);
            
            // check with this token user is exist or not in database
            $res = $this->this_model->authenticate($user_profile ['user']);
            
            // if we get row then direct login user no need to create new account in database
            if (count($res) == 1) {
                
                $this->session->unset_userdata('facebook_details');
                // a common method for login users login them a
                $this->this_model->generateSession($res [0]);
            }
            
            else {
                /*
                 * check if user is loggin in from registration page means we are getting grpId in this
                 * function then directoly call signupfacebbok method no need to show 1 more view
                 */
                if (! empty($id)) {
                    /* calling this method to logged in user */
                    $this->signupfacebook($id);
                }
                else {
                    $this->utility->setFlashMessage('success', $this->lang->line('login_facebook'));
                    $this->myView('social-login', $data);
                }
            }
        }
        else {
            // display the error if exist
            $this->utility->setFlashMessage('danger', $user_profile ['error_msg']);
            redirect($this->controller);
        }
    }

    

    /**
     * signupfacebook()
     * This method insert facebook user's data in database and create new user
     *
     * @param integer $idGrp is what is user type venue = 2 , performer = 3, fan = 4
     *       
     * @return call genrate session method and login user
     */
    function signupfacebook($idGrp = "")
    {
        // load register model to register a user in user table
        $this->load->model($this->registerInfo ["model"], "register_model");
        
        // we create user if facebook_details session is exist
        if (! empty($this->session->userdata('facebook_details'))) {
            // get value from facebook session
            $userDetails = $this->session->userdata('facebook_details');
            //pr($userDetails);
            //pr($userDetails['picture']);
            // create a new user in user table
            $sessionid = $this->register_model->createFacebookUser($userDetails, $idGrp);
            
            // after creating user get information of last user
            $res = $this->this_model->authenticate($userDetails);
            
            // a common method to login users
            $this->this_model->generateSession($res [0]);
        }
        else {
            // if facebook session is not set already then redirect to login page
            $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
            redirect($this->controller);
        }
    }

    /**
     * forgotPassword()
     * This function is used forgot password process
     */
    public function forgotPassword()
    {
        $this->load->library('MailgunCI');
        
        $data ['controllerName'] = $this->myvalues->routedControllers ['forgotPassword'] ["controller"];
        $data ["showMenu"] = false;
        $data ["noHeader"] = true;
        $data ["forgotPasswordURL"] = SITEURL . $this->myvalues->routedControllers ['forgotPassword'] ["controller"];
        
        if ($this->input->post()) {
            
            $this->setRulesEmail();
            // validate post data
            $valid = $this->validateData($this->rules);
            // not valid load forgot password view
            if ($valid) {
                
                // once we check that requested email is exist in database or not
                $r = $this->this_model->checkDuplicate();
                // if user email matched
                if ($r) {
                    
                    $getStatus = $this->this_model->insert_reset_password();
                    
                    $this->utility->setFlashMessage('success', $this->lang->line('reset_password_success'));
                    
                    // if email does not match or not a valid email
                }
                else {
                    
                    $this->utility->setFlashMessage('danger', $this->lang->line('reset_password_failed'));
                    redirect($data ["forgotPasswordURL"]);
                }
            }
        }
        
        // log forgot password view
        $this->myView("forget_password_view", $data);
    }

    /**
     * reset_password()
     * This method check request token if request token has been expired then redirect to login page otherwise
     * redirect to reset passowrd page
     *
     *
     * @param string $token is token from url
     *       
     * @return redirect to login page if token is invalid
     */
    function resetPassword($token)
    {
        // unset the all session data, so user will logout
        // $this->session->sess_destroy();
        // $this->session->unset_userdata('UserData');
        $data ['controllerName'] = $this->myvalues->routedControllers ['resetPassword'] ["controller"];
        $data ["showMenu"] = true;
        // $data ["noHeader"] = true;
        $data ["resetPasswordURL"] = SITEURL . $this->myvalues->routedControllers ['resetPassword'] ["controller"];
        $data ["title"] = $this->lang->line("lable_resetpassowrd_page_heading");
        $data ["datatoken"] = $token;
        
        if ($this->input->post()) {
            // validate post data
            $this->setPasswordRules();
            $valid = $this->validateData($this->rules);
            if ($valid) {
                
                $data ['token'] = $token;
                $getMessage = $this->this_model->update_password($data);
                
                if (strtolower($getMessage [0]) == "success") {
                    $this->session->unset_userdata("UserData");
                }
                
                $this->utility->setFlashMessage($getMessage [0], $this->lang->line($getMessage [1]));
                redirect($this->controller);
            }
        }
        else {
            // call a model method check_link and get response from model
            $getStatus = $this->this_model->check_link($token);
            // if token has been matched then redirect to reset_password page
            if ($getStatus == 1) {
                
                $data ["datatoken"] = $token;
            }
            else {
                
                $msg = ($getStatus == 'Error') ? 'token_invali_url' : 'token_expired';
                $this->utility->setFlashMessage('danger', $this->lang->line($msg));
                redirect($this->controller);
            }
        }
        
        $this->myView('login_reset_password', $data);
    }

    /**
     * activate_account()
     * This method activate account of user from user email link
     *
     * @param string $access_key
     * @return return to login page with success message
     */
    function activate_account($access_key = null)
    {
        if (empty($access_key)) {
            // if key not found then redirect to login page with default error message
            
            redirect($this->controller);
        }
        else {
            // model method to check user account is activated yet or not
            $msg = $this->this_model->activate_account($access_key);
            $this->utility->setFlashMessage($msg [0], $this->lang->line($msg [1]));
            redirect($this->controller);
        }
    }

    /**
     * change_email()
     * This method change email of requested user
     *
     * @param string $key is email reset key
     * @return return to login page
     */
    public function change_email($key)
    {
        if (empty($key)) {
            
            $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
            redirect($this->controller);
        }
        
        $msg = $this->this_model->change_email($key);
        $this->utility->setFlashMessage($msg [0], $this->lang->line($msg [1]));
        redirect($this->controller);
    }

    public function show404()
    {
        $data ['controllerName'] = $this->myvalues->routedControllers ['resetPassword'] ["controller"];
        $data ["showMenu"] = false;
        $this->myView("404_page", $data);
    }
}
