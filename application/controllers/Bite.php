<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bite extends General_controller
{
    public $myInfo = "";

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->biteDetails;
        $this->folder = $this->myInfo ["controller"];
        $this->load->model($this->myInfo ["model"], "this_model");
    }

    /**
     * index()
     * This method show bited performers list
     *
     * @return return list of performer who bited
     */
    public function index()
    {
        $data ['list'] = $this->this_model->getBittenUser($this->pUserId);
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["title"] = $this->lang->line("your_bites");
        $this->myView($this->folder . "/bite_view_followed_view", $data);
    }

    public function biteUnbite($followingId = "", $biteUnbite = "")
    {
        if (! empty($followingId) && is_numeric($this->utility->decode($followingId)) && in_array($biteUnbite, [
            "Bite",
            "Unbite"
        ])) {
            $result = $this->this_model->biteUnbite($this->pUserId, $this->utility->decode($followingId), $biteUnbite);
            echo json_encode($result);
        }
        else {
            show_404();
            exit();
        }
    }

    /**
     * bitePerformer()
     * This method use to bite other performer
     */
    public function bitePerformer()
    {
        // $data ["styles"] = $this->user_model->get_performer_style_matching_with_venue ( $data ["user"]->user_id
        // );
        $this->load->model($this->myvalues->performerDetails ['model'], 'performer_model');
        $data ["styles"] = $this->main_model->selectStyleMatchingWithAllUser($this->pUserId, PERFOMER_GROUP, $this->pidGroup == 4 ? 'fan_style' : 'performer_style');
        $data ["result"] = $this->performer_model->searchPerformer($data ["styles"]);
        $data ['controllerName'] = $this->myInfo ['controller'];
        $data ["title"] = $this->lang->line('bite_performers');
        $data ["page"] = $this->myView($this->folder . "/bite_performer_view", $data);
    }

    /**
     * Used for Display the list of user who has bite the login user.
     * this functionality is only for venue & performer user only.
     */
    public function bittenMe()
    {
        if ($this->pUserId != FAN_GROUP) {
            $this->load->model($this->myvalues->subscriptionDetails ['model'], 'subscription_model');
            //$resultPlanID = $this->subscription_model->getCurrentPlan($this->pUserId, $this->pidGroup);
            
            // check the plan if not beginner then fetch the following user id
            // not allow the beginner plan
            if (($this->pidGroup == VENUE_GROUP) || ($this->pidGroup == PERFOMER_GROUP)) {
                $data ['list'] = $this->this_model->getBittenMe($this->pUserId);
                
                $data ["controllerName"] = $this->myInfo ["controller"]; 
                $data ["title"] = $this->lang->line("bite_to_me");
                $this->myView($this->folder . "/bite_bitten_me_view", $data);
            }
            else {                
                redirect(SITEURL . $this->myInfo ['controller']);
            }
        }
    }
}
