<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Subscription extends General_controller
{
    public $myInfo = "";

    public function __construct()
    {
        parent::__construct();
        
        // currently not display the subscription details, so set the 404 page
        show_404();
        exit;
        
        $this->myInfo = $this->myvalues->subscriptionDetails;
        $this->folder = $this->myInfo ["controller"];
        $this->folder = "subscription";
        $this->load->model($this->myInfo ["model"], "this_model");
        $this->load->library('stripe');
        $this->load->library("form_validation");
    }

    /**
     * This function loads current available plans and loads also Current Active plan for that user.
     */
    public function index()
    {
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["title"] = "Choose your subscription plan";
        $data ['active_plan'] = $this->this_model->getCurrentPlan($this->pUserId, $this->pidGroup);
        $this->myView($this->folder . "/subscription_plan_info_view", $data);
    }

    /**
     * subscribePlan()
     * This method view new load to pay customer and create a subscribtion account
     *
     * @param $newPlanId is new plan's encrypted ID
     *       
     * @return String | return to a view
     */
    public function subscribePlan($newPlanId = "")
    {
        $newPlanId = $this->utility->decode($newPlanId);
        if (! ctype_digit($newPlanId)) {
            show_404();
            exit();
        }
        
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["title"] = "Plan Subscription";
        $data ['active_plan'] = $this->this_model->getCurrentPlan($this->pUserId, $this->pidGroup);
        
        $this->session->set_userdata('newPlanId', $newPlanId);
        // we confirm that logged in user already subscribe with any plan or not if not show payment page otherwise
        // not
        $getStripeCustomerId = $this->this_model->getStripeCustomerInformation($this->pUserId);
        if (empty($getStripeCustomerId)) {
            $data ["planInfo"] = $this->this_model->getPlanInfo($newPlanId);
            $data ["planName"] = $data ["planInfo"] [0]->plan_name;
            $data ["price"] = $data ["planInfo"] [0]->price;
            $this->myView($this->folder . "/subscription_payment_process", $data);
        }
        else {
            if (! empty($getStripeCustomerId->subs_id)) {
                $info = $this->session->userdata('newPlanId');
                // if it was already subscribed user then we will only update his plan upgrade to downgrade
                $this->stripe->update_subscribtion($getStripeCustomerId->subs_id, $info);
                if($this->session->userdata('loadview')=='IphoneView'){
                    $this->session->set_userdata('subscription_status', 'success');
                    $this->myView($this->folder . "/subscription_plan_success_error_view", $data);
                }else{
                    $this->utility->setFlashMessage('success', 'Your subscription update request accepted successfully. We will update it shortly.');
                    redirect(SITEURL . $this->myInfo ["controller"]);
                }
            }
            else {
                $this->utility->setFlashMessage('success', $this->lang->line("error_default_error"));
                redirect(SITEURL . $this->myInfo ["controller"]);
            }
           
        }
        
        // $this->myView($this->folder . "/subscription_plan_info_view", $data);
    }

    /**
     * create_payment()
     * This create a customer's subcription profile in stripe
     *
     * @param string $info is a encrypted token of user's card details(number,cvv,exmp_mont,expyear)
     *       
     * @return JSON response from stripe either error or success json format object
     */
    function create_payment()
    {
        $checkValidationStaus = $this->paymentFormValidation();
        
        if ($checkValidationStaus === false) {
            
            $this->utility->setFlashMessage('danger', validation_errors());
            redirect(SITEURL . $this->myInfo ["controller"] . "/subscribePlan/" . $this->utility->encode($this->session->userdata("newPlanId")));
        }
        
        $info ['source'] = $this->input->post('stripeToken');
        $info ['plan'] = $this->session->userdata('newPlanId');
        $info ['email'] = $this->session->userdata("UserData")->user_name;
        
        // call a library method to create a subscription profile in stripe
        $result = $this->stripe->customer_create($info);
        $data ['user_id'] = $this->pUserId;
        $data ['plan_id'] = $info ['plan'];
        $data ['is_webhook'] = 0;
        // call model method and send array of data what we need to store
        $this->this_model->createNewPlan($data);
        $this->utility->setFlashMessage('success', 'Your subscription update request accepted successfully. We will update it shortly.');
        redirect(SITEURL . $this->myInfo ["controller"]);
    }

    /**
     * Used for payment with apple pay to subscribe the new plan in iphone
     *
     * @param string stripeToken , stripe token from stripe to make payment
     *       
     */
    function applePayCreatePayment()
    {
        /*
         * $data ["title"] = "Plan Subscription";
         * $this->session->set_userdata('subscription_status', 'success');
         * // load the view
         * $this->myView($this->folder . "/subscription_plan_success_error_view", $data);
         */
        
        $info ['source'] = $this->input->post('stripeToken');
        $info ['plan'] = $this->session->userdata('newPlanId');
        $info ['email'] = $this->session->userdata("UserData")->user_name;
        
        // to register your domain with Apple by stripe
        $domainRegistartion = $this->stripe->apple_pay_domain_create();
        // call a library method to create a subscription profile in stripe
        $result = $this->stripe->customer_create($info);
        
        $dataInfo ['user_id'] = $this->pUserId;
        $dataInfo ['plan_id'] = $info ['plan'];
        $dataInfo ['is_webhook'] = 0;
        
        // call model method and send array of data what we need to store
        $resultPlan = $this->this_model->createNewPlan($dataInfo);
        $data ["title"] = "Plan Subscription";
        
        // set the session for subscription.
        $this->session->set_userdata('subscription_status', 'success');
        // load the view
        $this->myView($this->folder . "/subscription_plan_success_error_view", $data);
    }

    /**
     * paymentFormValidation()
     * This method handel serverside validation
     *
     * @return boolean
     */
    function paymentFormValidation()
    {
        
        // server side validation
        $this->form_validation->set_rules('stripeToken', 'Stripe card', 'required');
        $this->form_validation->set_rules('number', 'Card number', 'trim|required|integer');
        $this->form_validation->set_rules('cvc', 'cvc', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('exp-month', 'expiration month', 'trim|required|min_length[2]');
        $this->form_validation->set_rules('exp-year', 'expiratoin year', 'trim|required|min_length[2]');
        if ($this->form_validation->run() === FALSE) {
            
            return false;
        }
        
        else {
            return true;
        }
    }

    /**
     * stripenotify()
     * this method recive webhook response from stripe and store response to payment_histry table
     *
     * @param Object $response is json object
     * @return Boolean true or false
     */
    function stripenotify()
    {
        $response = file_get_contents("php://input");
        $data ["message"] = $response;
        $data ["from_title"] = 'StageGator (Live) stripe response';
        $data ["to"] = 'alex.test1990@gmail.com';
        $data ["subject"] = 'Webhook response (Live)';
        // send mail using utilitys
        $this->utility->sendMailSMTP($data);
        
        unset($data);
        
        $getResponseInArray = json_decode($response, true);
        
        // Consider what event called by stripe webhook according to stripe event we manage over database
        switch ($getResponseInArray ['type']) {
            
            case 'invoice.payment_succeeded':
                // this method call whenever customer payment recived
                $this->saveUserPaymentDetails($getResponseInArray);
                break;
            
            case 'customer.subscription.created':
                // this method call whenever customer payment recived
                $this->updatePlanDetails($getResponseInArray);
                break;
            
            case 'customer.subscription.deleted':
                // this method call when customer subscribe cancel
                $this->updateOldPlanStatus($getResponseInArray);
                break;
            
            case 'customer.subscription.updated':
                // this method call when user subscribtion profile will update
                $this->updatePlanDetails($getResponseInArray);
                break;
        }
        echo "Response received successfully.";
    }

    /**
     * UpdatePlanDetails()
     * This method update user user_plan table once its disable old plan and active new updated plan id
     *
     * @param Array $planArray is the new subscribtion profile details
     * @return boolean
     */
    function UpdatePlanDetails($planArray)
    {
        $data ['plan_id'] = $planArray ['data'] ['object'] ['plan'] ['id'];
        $data ['user_id'] = $this->getCustomerInfo($planArray ['data'] ['object'] ['customer']);
        $data ['subs_id'] = $planArray ['data'] ['object'] ['id'];
        $data ['cus_id'] = $planArray ['data'] ['object'] ['customer'];
        $data ['plan_amount'] = $planArray ['data'] ['object'] ['plan'] ['amount'];
        $data ['start_date'] = date('Y-m-d h:i:s', $planArray ['data'] ['object'] ['current_period_start']);
        $data ['canceled_at'] = date('Y-m-d h:i:s', $planArray ['data'] ['object'] ['current_period_end']);
        /*
         * we check if user has unsbscribed his account besause weather user unsbscribe profile or update profile
         * both time customer.subscription.updated event call so when user update subscribtion we get canceled_at
         * NULL and when cancle canceled_at has timestamp
         */
        if (empty($planArray ['data'] ['object'] ['canceled_at'])) {
            
            if ($planArray ['type'] == 'customer.subscription.created') {
                $data ['old_plan_id'] = $planArray ['data'] ['object'] ['plan'] ['id'];
                $data ['canceled_at'] = date('Y-m-d h:i:s', $planArray ['data'] ['object'] ['current_period_end']);
                $data ['is_active'] = '1';
                $this->this_model->updateUersPlanStatus($data);
            }
            else {
                $data ['old_plan_id'] = $planArray ['data'] ['previous_attributes'] ['plan'] ['id'];
                $data ['canceled_at'] = date('Y-m-d h:i:s');
                $data ['is_active'] = '0';
                // first disable active staus 1 to 0 and update plan end date
                $this->this_model->updateUersPlanStatus($data);
                // insert new row for new subscribtion
                $this->this_model->createNewPlan($data);
            }
        }
        else {
            $data ['old_plan_id'] = $planArray ['data'] ['object'] ['plan'] ['id'];
            $data ['is_active'] = '0';
            $data ['canceled_at'] = date('Y-m-d h:i:s');
            // first disable active staus 1 to 0 and update plan end date
            $this->this_model->updateUersPlanStatus($data);
        }
        
        return true;
    }

    /**
     * updateOldPlanStatus()
     * This method call when current login user unsbscribe plan when customer.subscription.deleted event accure
     *
     * @param array $planArray is response of cus
     * @return boolean return true always to webhook
     */
    function updateOldPlanStatus($planArray)
    {
        $data ['old_plan_id'] = $planArray ['data'] ['object'] ['plan'] ['id'];
        $data ['user_id'] = $this->getCustomerInfo($planArray ['data'] ['object'] ['customer']);
        $data ['canceled_at'] = date('Y-m-d h:i:s', $planArray ['data'] ['object'] ['canceled_at']);
        $this->this_model->updateUersPlanStatus($data);
        
        return true;
    }

    /**
     * saveUserPaymentDetails()
     * This method call when invoice.payment_succeeded event accure in stripe webhook we store payment details
     *
     * @param array $getResponseInArray is payment related information
     * @return boolean return true to stripe webhook
     */
    function saveUserPaymentDetails($getResponseInArray)
    {
        // conbine data in singal array
        foreach ($getResponseInArray ['data'] ['object'] ['lines'] ['data'] as $value) {
            
            $data ['transcatino_id'] = $getResponseInArray ['id'];
            $data ['data_id'] [] = $value ['id'];
            // get sustomer user_id by stripe customer id
            $data ['user_id'] = $this->getCustomerInfo($getResponseInArray ['data'] ['object'] ['customer']);
            $data ['plan'] [] = $value ['plan'] ['id'];
            $data ['transcation_date'] = $getResponseInArray ['created'];
            $data ['total'] [] = $value ['amount'];
            $data ['subtotal'] = $getResponseInArray ['data'] ['object'] ['subtotal'];
        }
        // store transaction details
        $response = $this->this_model->storeUserTransactionDetails($data);
        
        return true;
    }

    /**
     * unsibscribe()
     * This method cancle logged in user's subscribtion profile
     *
     * @return redirect to view with success
     */
    function unsubscribe()
    {
        // get subs_id when user unsbscribe his profile
        $getStripeCustomerId = $this->this_model->getStripeCustomerInformation($this->pUserId);
        
        if (! empty($getStripeCustomerId->subs_id)) {
            // call library method to unsbscribe a customer
            $res = $this->stripe->customer_unsubscribe($getStripeCustomerId->subs_id);
        }
        $this->utility->setFlashMessage('success', 'Your subscription update request accepted successfully. We will update it shortly.');
        
        redirect(SITEURL . $this->myInfo ["controller"]);
    }

    /**
     * getCustomerInfo()
     * This method returns userid using customer id
     *
     * @param String $getStripeId is customer id from stripe
     * @return String $res is user_id of user fetched from user table
     */
    public function getCustomerInfo($cus_id)
    {
        $response = $this->stripe->customer_info($cus_id);
        $getResponseInArray = $response->__toArray(true);
        $res = $this->this_model->getUserByEmailAddress($getResponseInArray ['email']);
        
        return $res;
    }

    /**
     * history()
     * This method return user payment history details
     *
     * @return return to view
     */
    public function history()
    {
        $data ['result'] = $this->this_model->getUserPlanHistory($this->pUserId);
        $data ["title"] = "Payment history";
        $data ["controllerName"] = $this->myInfo ['controller'];
        $this->myView($this->folder . "/subscription_payment_list_view", $data);
    }
}