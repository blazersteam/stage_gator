<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Webservice extends Guest_controller
{
    // public $myInfo = "";
    public function __construct()
    {
        parent::__construct();
        $this->access_level = array();
    }

    /**
     * Used for login functionality
     *
     * @return jason array that conatin the data,message,status 0/1
     *         if 1 then login else 0 for not login
     *        
     */
    public function login()
    {
        
        // load the login model
        $this->load->model($this->myvalues->loginDetails ["model"], "login_model");
        
        //$a = file_put_contents(time() . 'responce.txt', $this->input->post());
        // echo json_encode($this->input->post()); exit;
        $returnArray = array();
        $returnArray ['status'] = 0;
        $returnArray ['data'] = "";
        
        $this->setRulesLogin();
        // validate post data
        $valid = $this->validateData($this->rules);
        // check server side validation for email
        
        if ($valid) {
            
            if(empty($this->input->post('access_level'))){
                $returnArray ['message'] = $this->lang->line('error_empty_access_level');
                die(json_encode($returnArray));
            }
            
            if ($this->input->post('access_level') == 'Performer-Venue') {
                $this->access_level['performer_venue'][] = VENUE_GROUP;
                $this->access_level['performer_venue'][] = PERFOMER_GROUP;
            }
            else if($this->input->post('access_level') == 'Fan'){
                $this->access_level['fan'] = FAN_GROUP;
            }
            else{
                $returnArray ['message'] = $this->lang->line('un_authorized_access');
                die(json_encode($returnArray));
            }
            
            
            // get user email and password and get response from user table using login_model's method
            $result = $this->login_model->authenticate();
            
            // set validation rule
            if (count($result) == 0) {
                // isUserLock method check if user has locked or not if locked it will redirect to login page
                // with validation message
                $msg = $this->login_model->userLockStatus($this->utility->encodeText($this->input->post('txtEmail')));
                $returnArray ['message'] = $this->lang->line($msg);
                die(json_encode($returnArray));
            }
            else {
                
                $result = $result [0];
                
                // only allowed the fan user to login with api now
                /*if ($result->idGrp == FAN_GROUP) {*/
                
                    // check if user is locked already we are not going to login them
                    if ($result->islock == 1) {
                        $returnArray ['message'] = $this->lang->line('user_account_locked');
                        die(json_encode($returnArray));
                    }
                    // if status id 0 then redirect to login
                    if ($result->status_id == 0 && $this->input->post('access_level') != 'Fan') {
                        $returnArray ['message'] = $this->lang->line('user_deactive');
                        die(json_encode($returnArray));
                    }
                    
                    // if account is already suspended then redirect to login page
                    if ($result->suspended == 1) {
                        $returnArray ['message'] = $this->lang->line('user_suspend');
                        die(json_encode($returnArray));
                    }
                    
                    
                    // if user type is not to 'fan' then redirect to login page
                    if ($result->idGrp != FAN_GROUP && !empty($this->access_level['fan'])) {
                        $returnArray ['message'] = $this->lang->line('invalid_authentication_error');
                        die(json_encode($returnArray));
                    }
                    
                    // if user type is not to 'performer' and 'venue' then redirect to login page
                    if (($result->idGrp != VENUE_GROUP && $result->idGrp != PERFOMER_GROUP) && !empty($this->access_level['performer_venue'])) {
                        $returnArray ['message'] = $this->lang->line('invalid_authentication_error');
                        die(json_encode($returnArray));
                    }
                    
                    //$result->iscompleted = 0;
                    // if user style or user_city got blank then redirect to profile page always
                    $status = $this->login_model->getMemberandStyleStatus($result->user_id);
                    $result->iscompleted = 1;
                    
                    /*
                     * add one more condition when admin login that time this condition will not be true so iscompleted always
                     * be 1
                     */
                    if ((empty($status->idStyle) || empty($status->mem_city)) && ($result->idGrp != ADMIN_GROUP)) {
                        $result->iscompleted = 0;
                    }
                    
                    
                    // if user has performer and not registerd in strip account then redirect to tip registration page always
                    $stripStatus= $this->login_model->checkMemberStripAccountExit($result->user_id);
                    $result->tipRegistration = 1;
                    /*
                     * add one more condition when admin or fan or venue login that time this condition will not be true so tipRegistration always
                     * be 0
                     */
                    if (empty($stripStatus->stripe_account_id) && ($result->idGrp == PERFOMER_GROUP)) {
                        $result->tipRegistration = 0;
                    }
                    
                    // Update last login time and set message
                    $n = $this->login_model->setSessionData($result);
                    
                    if ($n != 1) {
                        $returnArray ['message'] = 'Welcome to Stagegator, ' . $result->chrName . "!";
                        //for iOS User Only
                        if (!empty($this->input->post('fcm_token'))){
                            $this->login_model->updateFcmTokenValue('Iphone', $this->input->post('fcm_token'), $result->user_id);
                        }
                    }
                    else {
                        $returnArray ['message'] = 'Welcome Back, ' . trim($result->chrName) . "!";
                        //for iOS User Only
                        if (!empty($this->input->post('fcm_token'))){
                            $this->login_model->updateFcmTokenValue('Iphone', $this->input->post('fcm_token'), $result->user_id);
                        }
                    }
                    $returnArray ['status'] = 1;
                    $returnArray ['data'] ['userId'] = $result->user_id;
                    // $returnArray ['data'] ['userGroupId'] = $result->idGrp;
                    
                    if ($result->idGrp == ADMIN_GROUP) {
                        $returnArray ['data'] ['userGroupType'] = 'ADMIN';
                    }
                    else if ($result->idGrp == PERFOMER_GROUP) {
                        $returnArray ['data'] ['userGroupType'] = 'PERFOMER';
                    }
                    else if ($result->idGrp == VENUE_GROUP) {
                        $returnArray ['data'] ['userGroupType'] = 'VENUE';
                    }
                    else {
                        $returnArray ['data'] ['userGroupType'] = 'FAN';
                    }
                    
                    
                    die(json_encode($returnArray));
               /* }else{
                    //display the invalid username password for other user
                    $returnArray ['message'] = $this->lang->line('user_authentication');
                    die(json_encode($returnArray));
                }*/
            }
        }
        else {
            // if validation eror found then return the error
            $returnArray ['message'] = validation_errors();
            $returnArray ['message']= str_replace('<span class="help-inline text-danger">','',$returnArray ['message']);
            $returnArray ['message']= str_replace('</span>','.',$returnArray ['message']);
            $returnArray ['message'] = trim(preg_replace('/\s+/', ' ',  $returnArray ['message']));
            die(json_encode($returnArray));
        }
    }

    /**
     * setRules()
     * This function is used to set rules for form
     */
    public function setRulesLogin()
    {
        $this->rules [] = [
            "field" => "txtEmail",
            "label" => $this->lang->line('email_address'),
            "rules" => "trim|required|valid_email|max_length[250]",
            "errors" => [
                'required' => $this->lang->line('error_please_enter_email_address'),
                'valid_email' => $this->lang->line('error_please_enter_valid_email_address'),
                'max_length' => $this->lang->line('error_email_address_maxlength')
            ]
        ];
        
        $this->rules [] = [
            "field" => "txtPassword",
            "label" => $this->lang->line('password'),
            "rules" => "trim|required|max_length[32]|min_length[6]",
            "errors" => [
                'required' => $this->lang->line('error_please_enter_password'),
                'max_length' => $this->lang->line('error_max_length_password'),
                'min_length' => $this->lang->line('error_min_length_password')
            ]
        ];
    }

    /**
     * setRulesRegistration()
     *
     * @param integer $extraFiled checks if user is registerd as a venue than we check extra fileds
     *        This function is used to set rules for form
     */
    public function setRulesRegistration($extraFiled)
    {
        $this->rules [] = [
            "field" => "txtName",
            "label" => $this->lang->line('register_name'),
            "rules" => "trim|required|max_length[250]",
            "errors" => [
                'required' => $extraFiled == 1 ? $this->lang->line('error_please_enter_venue_title') : $this->lang->line('error_please_enter_user_name')
            ]
        ];
        
        if ($extraFiled == 1) {
            $this->rules [] = [
                "field" => "txtContact_person",
                "label" => $this->lang->line('register_contact_person'),
                "rules" => "trim|required|max_length[250]",
                "errors" => [
                    'required' => $this->lang->line('error_please_enter_user_name')
                ]
            ];
        }
        
        $this->rules [] = [
            "field" => "cmbType",
            "label" => $this->lang->line('register_account_type'),
            "rules" => "trim|required|integer|max_length[1]",
            "errors" => [
                'required' => $this->lang->line('error_please_select_a_usertype')
            ]
        ];
        
        
        if(!empty($this->input->post('fb_token')) && !empty($this->input->post('txtEmail'))){
            
            $this->rules [] = [
                "field" => "txtEmail",
                "label" => $this->lang->line('register_email_address'),
                "rules" => "trim|required|valid_email|max_length[250]|is_unique[user.user_name]|callback_emailBlockCheck",
                "errors" => [
                    'required' => $this->lang->line('error_please_enter_email_address'),
                    'valid_email' => $this->lang->line('error_please_enter_valid_email_address'),
                    'max_length' => $this->lang->line('error_email_address_maxlength'),
                    'is_unique' => $this->lang->line('register_email_exists'),
                    'emailBlockCheck' => $this->lang->line('email_block_msg')
                ]
            ];
        }
        
        
        if(empty($this->input->post('fb_token'))){
            
            $this->rules [] = [
                "field" => "txtEmail",
                "label" => $this->lang->line('register_email_address'),
                "rules" => "trim|required|valid_email|max_length[250]|is_unique[user.user_name]|callback_emailBlockCheck",
                "errors" => [
                    'required' => $this->lang->line('error_please_enter_email_address'),
                    'valid_email' => $this->lang->line('error_please_enter_valid_email_address'),
                    'max_length' => $this->lang->line('error_email_address_maxlength'),
                    'is_unique' => $this->lang->line('register_email_exists'),
                    'emailBlockCheck' => $this->lang->line('email_block_msg')
                ]
            ];
           /*  
            if($this->input->post('cmbType') != 4 ){
                $this->rules [] = [
                    "field" => "txtPhone",
                    "label" => $this->lang->line('register_phone'),
                    "rules" => "trim|required|callback_phoneValidation",
                    "errors" => [
                        'required' => $this->lang->line('error_please_enter_mobile'),
                        'phoneValidation' => $this->lang->line('error_please_enter_a_valid_phone_number')
                    ]
                ];
            } */
            
            $this->rules [] = [
                "field" => "txtPassword",
                "label" => $this->lang->line('register_password'),
                "rules" => "trim|required|callback_passwordCheck|max_length[32]|min_length[6]",
                "errors" => [
                    'required' => $this->lang->line('error_please_enter_password'),
                    'passwordCheck' => $this->lang->line('error_no_space_allowed'),
                    'max_length' => $this->lang->line('error_max_length_password'),
                    'min_length' => $this->lang->line('error_min_length_password')
                ]
            ];
            $this->rules [] = [
                "field" => "txtConfirmPassword",
                "label" => $this->lang->line('register_confirm_password'),
                "rules" => "trim|matches[txtPassword]",
                "errors" => [
                    'matches' => $this->lang->line('error_confirm_password_notmatched')
                ]
            ];
        }
    }

    /**
     * Used for block the email id that contain this domain name
     *
     * @param string $email, user's entred email id
     *       
     * @return boolean
     */
    public function emailBlockCheck($email)
    {
        list ($user, $domain) = explode('@', $email);
        
        $blockDomainArray = array();
        
        if (in_array($domain, $blockDomainArray)) {
            return false;
        }
        return true;
    }

    /**
     * passwordCheck remove whitespace from password
     *
     * @param string $str is passowrd
     *       
     * @return boolean ture or false
     */
    public function passwordCheck($str)
    {
        if (preg_match('/\s/', $str)) {
            
            return false;
        }
        return true;
    }

    /**
     * phoneValidation()
     * This method for us mobile number validation
     *
     * @param unknown $mob
     * @return boolean
     */
    function phoneValidation($mob)
    {
        if (preg_match('/^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/', $mob)) {
            
            return true;
        }
        
        return false;
    }
    
    /**
     * setRulesEmail()
     * This function is used to set rules for form
     */
    public function setRulesEmail()
    {
        $this->rules [] = [
            "field" => "txtEmail",
            "label" => $this->lang->line('email_address'),
            "rules" => "trim|required|valid_email|max_length[250]",
            "errors" => [
                'required' => $this->lang->line('error_please_enter_email_address'),
                'valid_email' => $this->lang->line('error_please_enter_valid_email_address'),
                'max_length' => $this->lang->line('error_email_address_maxlength')
            ]
        ];
    }
    
    
    /**
     * Used for do the Registration for new user
     */
    public function Registration()
    {
        // load the login model
        $this->load->model($this->myvalues->userRegisterDetails ["model"], "registration_model");
        // load the login model
        $this->load->model($this->myvalues->loginDetails ["model"], "login_model");
        $returnArray = array();
        $returnArray ['status'] = 0;
        $returnArray ['data'] = "";
       
       
        /* if requested url did not match with three array pameters then redirect to login */
        if (empty($this->input->post('userType')) || ! in_array($this->input->post('userType'), [
            "performer",
            "venue",
            "fan"
        ])) {
            $returnArray ['message'] = $this->lang->line('invalid_user');
            die(json_encode($returnArray));
        }
        $userType = $this->input->post('userType');
        
        $extraFiled = $userType == "venue" ? 1 : 0;
        
        $this->setRulesRegistration($extraFiled);
        $valid = $this->validateData($this->rules);
        
        if ($valid) {
            
            $respose = $this->registration_model->create_user();
            
            if ($respose) {
                
                if($this->input->post('userType') == 'fan'){
                    
                    $this->access_level['fan'] = FAN_GROUP;
                    //modify password to md5() formate when login after registration
                    $_POST['txtPassword'] = md5($this->input->post('txtPassword'));
                    // get user email and password and get response from user table using login_model's method
                    $result = $this->login_model->authenticate();
                    
                    // set validation rule
                    if (count($result) == 0) {
                        // isUserLock method check if user has locked or not if locked it will redirect to login page
                        // with validation message
                        $msg = $this->login_model->userLockStatus($this->utility->encodeText($this->input->post('txtEmail')));
                        $returnArray ['message'] = $this->lang->line($msg);
                        die(json_encode($returnArray));
                    }else{
                        
                        $result = $result [0];
                        
                        // only allowed the fan user to login with api now
                        
                        // check if user is locked already we are not going to login them
                        if ($result->islock == 1) {
                            $returnArray ['message'] = $this->lang->line('user_account_locked');
                            die(json_encode($returnArray));
                        }
                        
                        // if account is already suspended then redirect to login page
                        if ($result->suspended == 1) {
                            $returnArray ['message'] = $this->lang->line('user_suspend');
                            die(json_encode($returnArray));
                        }
                        
                        
                        // if user type is not to 'fan' then redirect to login page
                        if ($result->idGrp != FAN_GROUP && !empty($this->access_level['fan'])) {
                            $returnArray ['message'] = $this->lang->line('invalid_authentication_error');
                            die(json_encode($returnArray));
                        }
                        
                        
                        //$result->iscompleted = 0;
                        // if user style or user_city got blank then redirect to profile page always
                        $status = $this->login_model->getMemberandStyleStatus($result->user_id);
                        $result->iscompleted = 1;
                        
                        /*
                         * add one more condition when admin login that time this condition will not be true so iscompleted always
                         * be 1
                         */
                        if ((empty($status->idStyle) || empty($status->mem_city)) && ($result->idGrp != ADMIN_GROUP)) {
                            $result->iscompleted = 0;
                        }
                        
                        
                        // if user has performer and not registerd in strip account then redirect to tip registration page always
                        $stripStatus= $this->login_model->checkMemberStripAccountExit($result->user_id);
                        $result->tipRegistration = 1;
                        
                        // Update last login time and set message
                        $n = $this->login_model->setSessionData($result);
                        
                        if ($n != 1) {
                            $returnArray ['message'] = 'Welcome to Stagegator, ' . $result->chrName . "!";
                            //for iOS User Only
                            if (!empty($this->input->post('fcm_token'))){
                                $this->login_model->updateFcmTokenValue('Iphone', $this->input->post('fcm_token'), $result->user_id);
                            }
                        }
                        else {
                            $returnArray ['message'] = 'Welcome Back, ' . trim($result->chrName) . "!";
                            //for iOS User Only
                            if (!empty($this->input->post('fcm_token'))){
                                $this->login_model->updateFcmTokenValue('Iphone', $this->input->post('fcm_token'), $result->user_id);
                            }
                        }
                        $returnArray ['status'] = 1;
                        $returnArray ['data'] ['userId'] = $result->user_id;
                        // $returnArray ['data'] ['userGroupId'] = $result->idGrp;
                        
                        $returnArray ['data'] ['userGroupType'] = 'FAN';
                        
                        die(json_encode($returnArray));
                    }
                    
                } else {
                    $this->utility->setFlashMessage('success', $this->lang->line('register_successfull'));
                    $returnArray ['status'] = 1;
                    $returnArray ['message'] = $this->lang->line('register_successfull');
                    die(json_encode($returnArray));
                }
            }
            else {
                $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
                $returnArray ['status'] = 0;
                $returnArray ['message'] = $this->lang->line('error_default_error');
                die(json_encode($returnArray));
            }
        }
        else {
            // if validation eror found then return the error
           $returnArray ['message'] = validation_errors();
           $returnArray ['message']= str_replace('<span class="help-inline text-danger">','',$returnArray ['message']);
           $returnArray ['message']= str_replace('</span>','.',$returnArray ['message']);
           $returnArray ['message'] = trim(preg_replace('/\s+/', ' ',  $returnArray ['message']));
           die(json_encode($returnArray));
        }
    }
    
    
    /**
     * Used for Login functionality with social site
     *
     * @return jason array that conatin the data,message,status 0/1
     *  if 1 then login else 0 for not login
     *
     */
    public function socialLogin(){
        
        // load the login model
        $this->load->model($this->myvalues->loginDetails ["model"], "login_model");
        
        $returnArray = array();
        $returnArray ['status'] = 0;
        $returnArray ['data'] = "";
        
        if (!empty($this->input->post('fb_token'))) {
            
            $fbToken['id'] = $this->input->post('fb_token');
            $fbToken['email'] = $this->input->post('txtEmail');
            
            if ($this->input->post('access_level') == 'Performer-Venue') {
                $this->access_level['performer_venue'][] = VENUE_GROUP;
                $this->access_level['performer_venue'][] = PERFOMER_GROUP;
            }else {
                $this->access_level['fan'] = FAN_GROUP;
            }
            
            // check with this token user is exist or not in database
            $result = $this->login_model->authenticate($fbToken);
            
            // if we get row then direct login user no need to create new account in database
            if (count($result) == 1) {
                
                $result = $result [0];
                
                // only allowed the fan user to login with api now
                /*if ($result->idGrp == FAN_GROUP) {*/
                
                // check if user is locked already we are not going to login them
                if ($result->islock == 1) {
                    $returnArray ['message'] = $this->lang->line('user_account_locked');
                    die(json_encode($returnArray));
                }
                // if status id 0 then redirect to login
                if ($result->status_id == 0) {
                    $returnArray ['message'] = $this->lang->line('user_deactive');
                    die(json_encode($returnArray));
                }
                
                // if account is already suspended then redirect to login page
                if ($result->suspended == 1) {
                    $returnArray ['message'] = $this->lang->line('user_suspend');
                    die(json_encode($returnArray));
                }
                
                // if user type is not to 'fan' then redirect to login page
                if ($result->idGrp != FAN_GROUP && !empty($this->access_level['fan'])) {
                    $returnArray ['message'] = $this->lang->line('invalid_authentication_error');
                    die(json_encode($returnArray));
                }
                
                // if user type is not to 'performer' and 'venue' then redirect to login page
                if (($result->idGrp != VENUE_GROUP && $result->idGrp != PERFOMER_GROUP) && !empty($this->access_level['performer_venue'])) {
                    $returnArray ['message'] = $this->lang->line('invalid_authentication_error');
                    die(json_encode($returnArray));
                }
                
                //$result->iscompleted = 0;
                // if user style or user_city got blank then redirect to profile page always
                $status = $this->login_model->getMemberandStyleStatus($result->user_id);
                $result->iscompleted = 1;
                
                /*
                 * add one more condition when admin login that time this condition will not be true so iscompleted always
                 * be 1
                 */
                if ((empty($status->idStyle) || empty($status->mem_city)) && ($result->idGrp != ADMIN_GROUP)) {
                    $result->iscompleted = 0;
                }
                
                // Update last login time and set message
                $n = $this->login_model->setSessionData($result);
                
                if ($n != 1) {
                    $returnArray ['message'] = 'Welcome to Stagegator, ' . $result->chrName . "!";
                    //for iOS User Only
                    if (!empty($this->input->post('fcm_token'))){
                        $this->login_model->updateFcmTokenValue('Iphone', $this->input->post('fcm_token'), $result->user_id);
                    }
                }
                else {
                    $returnArray ['message'] = 'Welcome Back, ' . trim($result->chrName) . "!";
                    //for iOS User Only
                    if (!empty($this->input->post('fcm_token'))){
                        $this->login_model->updateFcmTokenValue('Iphone', $this->input->post('fcm_token'), $result->user_id);
                    }
                }
                $returnArray ['status'] = 1;
                $returnArray ['data'] ['userId'] = $result->user_id;
                // $returnArray ['data'] ['userGroupId'] = $result->idGrp;
                
                if ($result->idGrp == ADMIN_GROUP) {
                    $returnArray ['data'] ['userGroupType'] = 'ADMIN';
                }
                else if ($result->idGrp == PERFOMER_GROUP) {
                    $returnArray ['data'] ['userGroupType'] = 'PERFOMER';
                }
                else if ($result->idGrp == VENUE_GROUP) {
                    $returnArray ['data'] ['userGroupType'] = 'VENUE';
                }
                else {
                    $returnArray ['data'] ['userGroupType'] = 'FAN';
                }
                
                
                die(json_encode($returnArray));
            }
            else{
                
                $returnArray ['status'] = "register";
                die(json_encode($returnArray));
            }
        }
        else {
            // display the error if exist
            $returnArray ['message'] = $this->lang->line('user_facebook_authentication');
            die(json_encode($returnArray));
        }
        
    }
    
    /**
     * Used for Register functionality with social site
     *
     * @return jason array that conatin the data,message,status 0/1
     *  if 1 then register with login else 0 for not register
     *
     */
    public function socialRegister(){
        
        // load the login model and register model
        $this->load->model($this->myvalues->loginDetails ["model"], "login_model");
        $this->load->model($this->myvalues->userRegisterDetails["model"], "register_model");
        
        $returnArray = array();
        $returnArray ['status'] = 0;
        $returnArray ['data'] = "";
        
        //check facebook token not empty/blank 
        if($this->input->post('fb_token')){
            
            $this->setRulesRegistration('');
            $valid = $this->validateData($this->rules);
            
            //check basic validation 
            if ($valid) {
                //set request data
                $userDetails['id'] = $this->input->post('fb_token');
                $userDetails['name'] = $this->input->post('txtName');
                $userDetails['email'] = $this->input->post('txtEmail');
                $userDetails['gender'] = $this->input->post('gender');
                $userDetails['link'] = $this->input->post('link');
                $userDetails['picture'] ['url'] = $this->input->post('pictureUrl');
                //set userType Group
                $idGrp = $this->input->post('cmbType');
                
                //call new user create function
                $sessionid = $this->register_model->createFacebookUser($userDetails, $idGrp);
                
                if($sessionid){
                            
                    $this->socialLogin();
                    
                }else{
                    // display the error if exist
                    $returnArray ['message'] = $this->lang->line('error_default_error');
                    die(json_encode($returnArray));
                }
            }else{
                // if validation eror found then return the error
                $returnArray ['message'] = validation_errors();
                $returnArray ['message']= str_replace('<span class="help-inline text-danger">','',$returnArray ['message']);
                $returnArray ['message']= str_replace('</span>','.',$returnArray ['message']);
                $returnArray ['message'] = trim(preg_replace('/\s+/', ' ',  $returnArray ['message']));
                die(json_encode($returnArray));
            }
        }else{
            $returnArray ['message'] = $this->lang->line('user_facebook_authentication');
            die(json_encode($returnArray));
        }
    }
    
    
    /**
     * forgotPassword()
     * This function is used forgot password process
     */
    public function forgotPassword()
    {
        // load the login model
        $this->load->model($this->myvalues->loginDetails ["model"], "login_model");
        
        $returnArray = array();
        $returnArray ['status'] = 0;
        $returnArray ['data'] = "";
        
        if ($this->input->post()) {
            
            $this->setRulesEmail();
            // validate post data
            $valid = $this->validateData($this->rules);
            // not valid load forgot password view
            if ($valid) {
                
                // once we check that requested email is exist in database or not
                $r = $this->login_model->checkDuplicate();
                // if user email matched
                if ($r) {
                    
                    $getStatus = $this->login_model->insert_reset_password();
                    $returnArray ['message'] = $this->lang->line('reset_password_success');
                    $returnArray ['status'] = 1;
                    die(json_encode($returnArray));
                    
                    // if email does not match or not a valid email
                }
                else {
                    
                    $returnArray ['message'] = $this->lang->line('reset_password_failed');
                    die(json_encode($returnArray));
                }
            }else{
                // if validation eror found then return the error
                $returnArray ['message'] = validation_errors();
                $returnArray ['message']= str_replace('<span class="help-inline text-danger">','',$returnArray ['message']);
                $returnArray ['message']= str_replace('</span>','.',$returnArray ['message']);
                $returnArray ['message'] = trim(preg_replace('/\s+/', ' ',  $returnArray ['message']));
                die(json_encode($returnArray));
            }
            
        }else{
            $returnArray ['message'] = $this->lang->line('reset_password_failed');
            die(json_encode($returnArray));
        }
        
    }
    
}
   