<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Overview extends Guest_controller
{
    public $myInfo = "";
    public $myInfoRoutes = "";

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->overviewDetails;
        $this->folder = $this->myInfo ["controller"];
        $this->load->model($this->myvalues->profileDetails ["model"], "this_model");
    }
    /**
     * index()
     * This function show any user information with url
     * @param string $url is user profile url
     */
    public function index($url="")
    {
        $homeLocation = $this->input->get('source');
        
        $res = $this->this_model->getUserIdByUrl($url);
        
        if(!isset($res [0]))
        {
            show_404();exit;
        }
        
        if(!empty($homeLocation) && $homeLocation != 'city')
        {
            show_404();exit;
        }
        
        $res = $res [0];
        $id = $res->user_id;
        
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["title"] = $this->lang->line('profile_overview');
        $data ["userInfo"] = $this->this_model->getUserInformation($id);
        $data ['userStyle'] = $this->this_model->getUserStyleForOverview($id);
        
        $data ['homeLocation'] = 0;
        if ($res->idGrp == 2) {
           
            /* checking if cookie has any city then take first this city in model */
            if (! empty($this->input->cookie('user_city'))) {
                $cookieData = $this->input->cookie('user_city', true);
                $getfullCookie = json_decode($cookieData);
                // $city = $getfullCookie->city;
                $fullAddress = $getfullCookie->fullAddress;
                $fullAddress = explode(',', $fullAddress);
                $city = $getfullCookie->city;
            }
            else {
                $this->load->model($this->myvalues->profileDetails ['model'], 'profile_model');
                /* if above cookie city is empty then get user default city form member table */
                $getUserCity = $this->profile_model->getUserInformation($res->user_id);
                
                $city = $getUserCity->mem_city;
            }
            
            if($homeLocation){
                $homeLocation = $city;
            }
            
            $this->load->model($this->myvalues->eventDetails ['model'], 'event_model');
            $data ["list"] = $this->event_model->get_event_list($id, true,'',$homeLocation);
            $data ['homeLocation'] = $homeLocation;
            $data ['url'] = $url;
        }
        else {
            $this->load->model($this->myvalues->performerDetails ['model'], 'performer_model');
            $data ["list"] = $this->performer_model->get_performer_events($id);
        }
        $data ["title"] = $this->lang->line("profile2");
        //$this->myView($this->myvalues->profileDetails ["controller"] . "/profile_by_url_view", $data);
        $this->myView($this->myvalues->profileDetails ["controller"] . "/profile_overview_view", $data);
    }
    
}
