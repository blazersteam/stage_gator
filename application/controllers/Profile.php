<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends General_controller
{
    public $myInfo = "";
    public $myInfoRoutes = "";

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->profileDetails;
        $this->folder = $this->myInfo ["controller"];
        $this->load->model($this->myInfo ["model"], "this_model");
    }

    public function index()
    {
        $this->overview();
    }

    /**
     * overview()
     * This method shows overview of current logged in user
     *
     * @return return to view
     */
    public function overview()
    {
        $id = $this->pUserId;
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["title"] = $this->lang->line('profile_overview');
        $data ['userInfo'] = $this->this_model->getUserInformation($this->pUserId);
        $data ['userStyle'] = $this->this_model->getUserStyleForOverview($this->pUserId);
        if ($this->session->userdata('UserData')->idGrp == VENUE_GROUP) {
            // load the event model
            $this->load->model($this->myvalues->eventDetails ['model'], 'event_model');
            $data ["list"] = $this->event_model->get_event_list($this->pUserId, true);
        }
        else {
            
            // load the perfomer model
            $this->load->model($this->myvalues->performerDetails ['model'], 'perfomer_model');
            $data ["list"] = $this->perfomer_model->get_performer_events($this->pUserId);
        }
        
        // $data ["registerUrl"] = SITEURL . $this->myInfo ["controller"] . '/updateprofile';
        
        $this->myView($this->folder . "/profile_overview_view", $data);
    }

    /**
     * updateprofile()
     * This method change user profile and show it
     *
     * @return return to view
     */
    public function updateprofile()
    {
        // get user information
        $oldImage = $this->this_model->getUserInformation($this->pUserId);
        $config ['upload_path'] = 'external/images/profiles';
        $config ['allowed_types'] = 'jpg|png|jpeg';
        $config ['encrypt_name'] = TRUE;
        
        $this->load->library('upload', $config);
        
        if ($this->upload->do_upload()) {
            $image = $this->upload->data();
            $config ['image_library'] = 'gd2';
            $config ['source_image'] = $image ["full_path"];
            $config ['create_thumb'] = FALSE;
            $config ['maintain_ratio'] = false;
            $config ['width'] = 250;
            $config ['height'] = 250;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            // deleting old profile from file system
            @unlink('./external/images/profiles/' . $oldImage->image);
            // @unlink(EXTERNAL_PATH."images/profiles/".$image["file_name"]);
            $this->this_model->change_image($image ["file_name"], $this->pUserId);
            $this->utility->setFlashMessage('success', $this->lang->line('profile_updated_successfully'));
            
            $sessionData = $this->session->userdata("UserData");
            $sessionData->image = $image ["file_name"];
            $this->session->set_userdata("UserData", $sessionData);
        }
        else {
            $this->utility->setFlashMessage('danger', $this->upload->display_errors());
        }
        
        redirect(SITEURL . $this->myInfo ["controller"] . '/overview');
    }

    /**
     * setRules()
     *
     * @param integer $extraFiled checks if user is registerd as a venue than we check extra fileds
     *        This function is used to set rules for form
     */
    public function setRules($extraFiled)
    {
        $this->rules [] = [
            "field" => "txtName",
            "label" => $this->lang->line('profile_name'),
            "rules" => "trim|required|max_length[250]",
            "errors" => [
                'required' => $extraFiled == 2 ? $this->lang->line('error_please_enter_venue_title') : $this->lang->line('error_please_enter_user_name'),
                'max_length' => $this->lang->line('max_250_character_allowed')
            ]
        ];
        
        if ($extraFiled == 2) {
            $this->rules [] = [
                "field" => "txtContact_person",
                "label" => $this->lang->line('register_contact_person'),
                "rules" => "trim|required|max_length[250]",
                "errors" => [
                    'required' => $this->lang->line('error_please_enter_user_name'),
                    'max_length' => $this->lang->line('max_250_character_allowed')
                ]
            ];
        }
        
        $this->rules [] = [
            "field" => "txtPhone",
            "label" => $this->lang->line('register_phone'),
            "rules" => "trim|required|callback_phoneValidation",
            "errors" => [
                'required' => $this->lang->line('error_please_enter_mobile'),
                'phoneValidation' => $this->lang->line('error_please_enter_a_valid_phone_number')
            ]
        ];
        
        $this->rules [] = [
            "field" => "rdaPhonvisiblity",
            "label" => $this->lang->line('register_phone'),
            "rules" => "trim|in_list[Public,Private]",
            "errors" => [
                'in_list' => 'Please select phone visiblity'
            ]
        ];
        $this->rules [] = [
            "field" => "cmbCountry",
            "label" => $this->lang->line('country'),
            "rules" => "required|max_length[250]",
            "errors" => [
                'required' => $this->lang->line('error_select_country')
            ]
        ];
        $this->rules [] = [
            "field" => "cmbState",
            "label" => $this->lang->line('state'),
            "rules" => "required|max_length[250]",
            "errors" => [
                'required' => $this->lang->line('error_select_state')
            ]
        ];
        $this->rules [] = [
            "field" => "cmbCity",
            "label" => $this->lang->line('city'),
            "rules" => "required|max_length[255]",
            "errors" => [
                'required' => $this->lang->line('error_select_city')
            ]
        ];
        if ($extraFiled == 2) {
            $this->rules [] = [
                "field" => "txtAddress",
                "label" => $this->lang->line('address'),
                "rules" => "trim|required|max_length[150]",
                "errors" => [
                    'required' => $this->lang->line('error_enter_address'),
                    'max_length' => $this->lang->line('max_150_character_allowed')
                ]
            ];
        }
        
//         $this->rules [] = [
//             "field" => "txtUrl",
//             "label" => $this->lang->line('url'),
//             // "rules" => "integer|callback_isValidTimeStamp",
//             "rules" => "trim|callback_isValidProfileUrl|callback_checkDuplicateProfileUrl|max_length[30]",
//             "errors" => [
//                 // 'isValidTimeStamp' => $this->lang->line('error_invalid_url'),
//                 'isValidProfileUrl' => $this->lang->line('allowed_alphanumeric_underscore_dash'),
//                 'checkDuplicateProfileUrl' => $this->lang->line('url_already_exist_enter_another'),
//                 'max_length' => $this->lang->line('max_30_character_allowed')
//             ]
//         ];
        // 'integer' => $this->lang->line('error_invalid_url')
    }

    /**
     * Used for check if profile url already exist for other user or not
     *
     * @param string $url , that contain the profile url
     *       
     * @return boolean
     */
    public function checkDuplicateProfileUrl()
    {
        $profileUrl = $this->utility->encodeText(trim($this->input->get_post('txtUrl')));
        
        $currentUserId = $this->pUserId;
        
        $result = $this->this_model->checkDuplicateProfileUrl($profileUrl, $currentUserId);
        
        if ($this->input->post()) {
            
            if ($result == "1") {
                
                return false;
            }
            
            return true;
        }
        else if ($this->input->get()) {
            // set result flase if duplicate data or true
            if ($result == "1") {
                echo "false";
            }
            else {
                echo "true";
            }
        }
    }

    /**
     * Used for check if profile url contain only alphanumeric, underscore, dash only
     *
     * @param string $url, that contain the profile url
     *       
     * @return boolean
     */
    function isValidProfileUrl($url)
    {
        if (preg_match('/^[a-zA-Z0-9_\-]+$/', $url)) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    /**
     * phoneValidation()
     * This method for us mobile number validation
     *
     * @param unknown $mob
     * @return boolean
     */
    function phoneValidation($mob)
    {
        if (preg_match('/^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/', $mob)) {
            
            return true;
        }
        
        return false;
    }

    /**
     * edit()
     * This method edit user current logged in user's data
     */
    public function edit()
    {
        
        // get information of current logged in user
        $data ['user'] = $this->this_model->getUserInformation($this->pUserId);
        
        if ($this->input->post()) {

            $this->setRules($this->pidGroup);
            $valid = $this->validateData($this->rules);
            if ($valid) {
                
                $this->utility->setFlashMessage('success', $this->lang->line('profile_updated_successfully'));
                // update data of user
                $this->this_model->updateProfile($data ['user']->user_id, $data ['user']->mem_id);
                $userData = $this->session->userdata("UserData");
                $userData->iscompleted = 1;
                $userData = $this->session->set_userdata("UserData", $userData);
                redirect(SITEURL . $this->myInfo ['controller'] . '/overview');
            }
        }
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["title"] = $this->lang->line('profile_' . $this->pidGroup);
        // $data ["country"] = $this->main_model->getAllCountries();
        // $data ["state"] = $this->main_model->getState($data ['user']->countries_id);
        // $data ["cities"] = $this->main_model->getCityByState($data ['user']->countries_id, $data
        // ['user']->stateAbbrev);
        $data ["user_styles"] = $this->this_model->getUserStyle($this->pUserId);
        $data ["style"] = $this->this_model->getAllStyles($this->pidGroup);
        // $data ["registerUrl"] = SITEURL . $this->myInfo ["controller"] . '/edit';
        $this->myView($this->folder . "/profile_update_profile_view", $data);
    }

    /**
     * settings()
     * this method change settings for current logged in user
     */
    public function settings()
    {
        if ($this->input->post()) {
            $response = $this->this_model->updateSettings($this->pUserId);
            if ($response) {
                $this->utility->setFlashMessage('success', $this->lang->line('setting_updated'));
            }
            else {
                $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
            }
            redirect(SITEURL . $this->myInfo ['controller'] . '/settings');
        }
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["user"] = $this->this_model->getUserInformation($this->pUserId);
        $data ["title"] = $this->lang->line("settings");
        $this->myView($this->folder . "/profile_settings_view", $data);
    }

    /**
     * delete()
     * This method use to delete current logged in user's account
     */
    public function delete()
    {
        $this->this_model->deleteAccount($this->pUserId, $this->pidGroup);
        $this->session->unset_userdata('UserData');
        $this->utility->setFlashMessage('danger', $this->lang->line('account_deleted_successfully'));
        redirect(SITEURL . $this->myvalues->loginDetails ['controller']);
    }

    /**
     * This method used to create cookie of city after created cookie it will redirect to index method
     *
     * @return after created cookie redirect to same controller's index method
     */
    public function setCitycookie()
    {
        /* first we check if any city get in post if not then redirect */
        if ($this->input->post("cmbCity_1")) {
           /*  $cityName = $this->input->post("cmbCity");
            $mainName = explode(',', $cityName);
            $array = [
                'city' => $mainName [0],
                'fullAddress' => $cityName
            ]; */
            
            $cityName = $this->input->post("cmbCity_1");
            $mainName = explode(',', $cityName);            
            $fullAddress = $mainName [0].",". $this->input->post("cmbState_1").",". $this->input->post("cmbCountry_1");
            $array = [
                'city' => $mainName [0],
                'locationstring' => $cityName,
                'state' => $this->input->post("cmbState_1"),
                'country' => $this->input->post("cmbCountry_1"),
                'fullAddress' => $fullAddress
            ];
            
            if (! empty($mainName)) {
                $cityName = json_encode($array);
            }
            else {
                $cityName = '';
            }
            /* load cookie halper */
            //$this->load->helper('cookie');
            /*
             * delete user_city cookie purpose: whenever get new city in post we delete old one and create new one
             */
            if($this->input->cookie('user_city')){
                delete_cookie('user_city');
            }
            $cookie = [
                'name' => 'user_city',
                'value' => $cityName,
                'expire' => time() + 60 * 60 * 24 * 365
            ];
            /* set a cookie */
            $this->input->set_cookie($cookie);
            // var_dump($this->input->cookie('user_city', true));
        }
        redirect(SITEURL . $this->myvalues->eventDetails ['controller'] . '/upcoming');
    }
}
