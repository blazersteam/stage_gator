<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Webhook extends Guest_controller
{
    public $myInfo = "";

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->webhookDetails;
        $this->folder = $this->myInfo ["controller"];
        $this->load->model($this->myInfo ["model"], "this_model");
    }

    /**
     * index
     * This method called when any webhook called from stripe stripe we have switch case in
     */
    public function index()
    {
        $response = file_get_contents("php://input");
        
        if (! empty($response)) {
            file_put_contents(FCPATH . "stripe_response/stripe_" . date("Y-m-d H:i:s") . ".html", $response);
        }
        
        $getResponseInArray = json_decode($response, true);
        
        switch ($getResponseInArray ['type']) {
            
            case 'account.updated':
                // this method call whenever sucessfully checked the Account Verification data return responce related to that fileds data
                echo "Called Account Updated.";
                $this->saveAccountUpdateResponse($getResponseInArray);
                break;
            
            case 'account.external_account.updated':
                // this method call whenever bank account status changed / updated any details
                echo "Called External Account Updated";
                $this->saveExternalAccountUpdateResponse($getResponseInArray);
                
                break;
            
            case 'charge.succeeded':
                if(!empty($getResponseInArray ["data"] ["object"] ["metadata"])){
                    // this method call whenever stripe updates charge status
                    echo "Called Charge Succeeded";
                    $this->saveChargeSuccessUpdateResponse($getResponseInArray);
                    $accountId = $getResponseInArray ["data"] ["object"] ["destination"];
                    $created = $getResponseInArray ["data"] ["object"] ["created"];
                    if(empty($accountId)){
                        $accountId = $getResponseInArray ["account"];
                    }
                    if(!empty($accountId)){
                        $this->updateTotalStripeBalance($accountId,$created);
                    }
                }
                break;
            case 'charge.failed':
                if(!empty($getResponseInArray ["data"] ["object"] ["metadata"])){
                    // this method call whenever stripe updates charge status failed
                    echo "Called Charge Failed";
                    $this->saveChargeFailedUpdateResponse($getResponseInArray);
                    $accountId = $getResponseInArray ["data"] ["object"] ["destination"];
                    $created = $getResponseInArray ["data"] ["object"] ["created"];
                    if(empty($accountId)){
                        $accountId = $getResponseInArray ["account"];
                    }
                    if(!empty($accountId)){
                        $this->updateTotalStripeBalance($accountId,$created);
                    }
                }
                break;
            case 'charge.refunded':
                if(!empty($getResponseInArray ["data"] ["object"] ["metadata"])){
                    // this method call whenever stripe updates charge status refuned
                    echo "Called Charge Refunded";
                    $this->saveChargeRefundedUpdateResponse($getResponseInArray);
                    $accountId = $getResponseInArray ["data"] ["object"] ["destination"];
                    $created = $getResponseInArray ["data"] ["object"] ["created"];
                    if(empty($accountId)){
                        $accountId = $getResponseInArray ["account"];
                    }
                    if(!empty($accountId)){
                        $this->updateTotalStripeBalance($accountId,$created);
                    }
                }
                break;
            case 'invoice.payment_succeeded':
                // this method call whenever stripe updates invoice payment status succeeded
                echo "Called invoice payment Succeeded";
                $this->saveInvoicePaymentSuccessUpdateResponse($getResponseInArray);
                $accountId = $getResponseInArray ["data"] ["object"] ["lines"] ["data"] [0] ["metadata"] ['performerId'];
                $created = $getResponseInArray ["data"] ["object"] ["date"];
                $this->updateTotalStripeBalance($accountId,'recurring',$created);
                break;
            case 'invoice.payment_failed':
                // this method call whenever stripe updates invoice payment status failed
                echo "Called invoice payment Failed";
                $this->saveInvoicePaymentFailedUpdateResponse($getResponseInArray);
                $accountId = $getResponseInArray ["data"] ["object"] ["lines"] ["data"] [0] ["metadata"] ['performerId'];
                $created = $getResponseInArray ["data"] ["object"] ["date"];
                $this->updateTotalStripeBalance($accountId,'recurring',$created);
                break;
            case 'customer.subscription.updated':
                // this method call whenever stripe updates invoice payment status failed
                echo "Called subscription updated";
                $this->saveSubscriptionUpdateResponse($getResponseInArray);
                $accountId = $getResponseInArray ["data"] ["object"] ["metadata"] ['performerId'];
                $created = $getResponseInArray ["data"] ["object"] ["created"];
                $this->updateTotalStripeBalance($accountId,'recurring',$created);
                break;
            case 'customer.subscription.deleted':
                // this method call whenever stripe updates invoice payment status failed
                echo "Called subscription deleted";
                $this->saveSubscriptionDeletedUpdateResponse($getResponseInArray);
                $accountId = $getResponseInArray ["data"] ["object"] ["metadata"] ['performerId'];
                $created = $getResponseInArray ["data"] ["object"] ["created"];
                $this->updateTotalStripeBalance($accountId,'recurring',$created);
                break;
        }
    }

    /**
     * updateTotalStripeBalance
     * This method update stripe balance in user table
     *
     * @param object $accountId is stripe acount id of performer
     * @return return true
     */
    public function updateTotalStripeBalance($accountId,$type=null,$created=null)
    {
        if($type == 'recurring'){
            $secretKey = $this->this_model->getStripeKeyByUserId($accountId);
            $secretKey->user_id = $accountId;
        }else{
            $secretKey = $this->this_model->getStripeKey($accountId);
        }
        
        
        $this->load->helper('stripe_connect_helper');
        $getBalance = getStripeBalance($secretKey->v_secret_key);
        
        $count = count($getBalance ['available']);
        $mainBalanceAvailable = 0;
        /* set a loop to get usd dollor available balace from stripe */
        for ($i = 0; $i < $count; $i ++) {
            /* comes in condition if currency type USD and amount is available */
            if (strtolower($getBalance ['available'] [$i] ['Currecny']) == 'usd') {
                $mainBalanceAvailable = $getBalance ['available'] [$i] ['Amount'] + $mainBalanceAvailable;
            }
        }
        
        $count = count($getBalance ['pending']);
        $mainBalancePending = 0;
        /* and this second one for get pending balance from stripe */
        for ($i = 0; $i < $count; $i ++) {
            /* comes in condition if currency type is USD */
            if (strtolower($getBalance ['pending'] [$i] ['Currecny']) == 'usd') {
                $mainBalancePending = $getBalance ['pending'] [$i] ['Amount'] + $mainBalancePending;
            }
        }
        
        /*
         * add both balance available and pending and update in database where account id is current account id
         * passing in function right below
         */
        $mainBalance = $mainBalanceAvailable + $mainBalancePending;
        $this->this_model->updateStripeBalance($secretKey->user_id, $mainBalance, $created);
    }

    /**
     * This function is written for Stripe Webhook Response receiver for account.update type webhook
     *
     * @param string $response json string received from STRIPE
     *       
     */
    public function saveAccountUpdateResponse($response = '')
    {
        /* Defining default variables */
        $status = "Pending";
        $reason = "Stripe is verifying your details.";
        $fields = [];
        $accountId = isset($response ["data"] ["object"] ["id"]) ? $response ["data"] ["object"] ["id"] : false;
        /**
         * POSSIBLE STATUSES
         *
         * 1. unverified : Stripe is not able to verify this entity right now, either because
         * verification has failed or we do not have enough information to attempt verification
         * 2. pending : Stripe is currently trying to verify this entity
         * 3. verified : Stripe has successfully verified this entity
         */
        
        /* Checking if verification status is not verified */
        if (! empty($response ["data"] ["object"] ["legal_entity"] ["verification"] ["status"]) && strtolower($response ["data"] ["object"] ["legal_entity"] ["verification"] ["status"]) != "verified") {
            /* Saving Status and Reason from legal entity object. */
            $status = $response ["data"] ["object"] ["legal_entity"] ["verification"] ["status"];
            
            $reason = $response ["data"] ["object"] ["legal_entity"] ["verification"] ["details"];
            
            /* If reason is empty then checks if fields needed to start verification process or not */
            if (empty($reason)) {
                /*
                 * Checks if stripe need some fields for verification then retrieve from response and save it in
                 * variable
                 */
                if (! empty($response ["data"] ["object"] ["verification"] ["fields_needed"]) && $response ["data"] ["object"] ["verification"] ["disabled_reason"] == "fields_needed") {
                    $reason = "Provide required information to verify the account.";
                    $fields = $response ["data"] ["object"] ["verification"] ["fields_needed"];
                }
            }
        }
        elseif ($response ["data"] ["object"] ["legal_entity"] ["verification"] ["status"] == "verified") {
            /* If Account is verified */
            $status = "Verified";
            $reason = "All details verified successfully.";
        }
        $data = [
            'status' => $status,
            'reason' => $reason,
            'accountId' => $accountId,
            'created' => $response ['created']
            
        ];
        if ($accountId) {
            // update the last webhook call time in table for log
            $responseWebhooklog = $this->this_model->updateWebhookLog($accountId, $response ['created']);
            if($responseWebhooklog){
                $this->this_model->updateAccountStatus($data);
            }
        }
        /* WRITE YOUR CODE HERE TO HANDLE STATUS AND REASON AND REMAING FIELDS NAMES */
        
        die();
    }

    /**
     * This function is written for Stripe Webhook Response receiver for external_account.update type webhook
     * Means when Stripe Update any details like Status of your Bank or Card Account
     *
     * @param string $response json string received from STRIPE
     */
    function saveExternalAccountUpdateResponse($response)
    {
        $status = "New";
        $reason = "Stripe is verifying your details.";
        $fields = [];
        
        /**
         * POSSIBLE STATUSES
         *
         * 1. new : you have added new account and Stripe not validated yet.
         * 2. validated : Stripe validated your account by depositing some amount in respective account. So now you
         * _______________need to verify same
         * 3. verified : Stripe has successfully verified this entity
         * 4. verification_failed : If the verification failed for any reason, such as microdeposit failure
         * 5. errored : If a transfer sent to this bank account fails, we’ll set the status to errored and will not
         * _____________continue to send transfers until the bank details are updated.
         */
        
        /* Checking if verification status is not verified */
        if (! empty($response ["data"] ["object"] ["status"])) {
            /* Saving Status and Reason from legal entity object. */
            $status = $response ["data"] ["object"] ["status"];
            $accountId = $response ['data'] ['object'] ['account'];
            switch (strtolower($status)) {
                case "new":
                    $reason = "Card added/updates by you";
                    break;
                case "validated":
                    $reason = "Please verify account by entering small deposites deposited by Stripe";
                    $this->SendMailOfAccountUpdate('validated', $response);
                    break;
                case "verified":
                    $reason = "Your account is verified";
                    break;
                case "verification_failed":
                    $reason = "Entered deposited amounts are not valid.";
                    $this->SendMailOfAccountUpdate('verification_failed', $response);
                    break;
                case "errored":
                    $reason = "Striep is not able validate your account. Please update its details.";
                    $this->SendMailOfAccountUpdate('errored', $response);
                    break;
                default:
                    $reason = "Unknown status passed";
                    break;
            }
            
            /* WRITE YOUR CODE HERE TO HANDLE STATUS AND REASON */
            echo $status . " : " . $reason;
        }
    }

    /**
     * SendMailOfAccountUpdate
     * Comman method to send mail when user account bank verification proccess status
     *
     * @param string account status $status
     * @param object stripe response $response
     */
    public function SendMailOfAccountUpdate($status, $response)
    {
        $accountId = $response ['data'] ['object'] ['account'];
        $userInfo = $this->this_model->getUserInfoByStripeAccountId($accountId);
        /* get user info his mail and name to send status mail */
        if ($userInfo) {
            $data ['userData'] = $userInfo;
            $data ['last4'] = $response ['data'] ['object'] ['last4'];
            ;
            $data ['tippingController'] = $this->myvalues->tippingDetails ['controller'];
            $message = $this->load->view('email_templates/webhook_' . $status . '_template', $data, true);
            $data ["message"] = $message;
            $data ["from_title"] = EMAIL_TITLE;
            $data ["to"] = $userInfo->user_name; // 'shaktis@websoptimization.com';
            $data ["subject"] = 'Stagegator Bank Account Verification';
            // send mail using utilitys
            $this->utility->sendMailSMTP($data);
        }
    }

    /**
     * This function is written for Stripe Webhook Response receiver for charge.succeeded type webhook
     * Means when Stripe Update any Status of your charged transaction to succeeded
     *
     * @param string $response json string received from STRIPE
     */
    public function saveChargeSuccessUpdateResponse($response)
    {
        
        /* Defining default variables */
        $status = "succeeded";
        $chargeId = isset($response ["data"] ["object"] ["id"]) ? $response ["data"] ["object"] ["id"] : false;
        $amount = isset($response ["data"] ["object"] ["amount"]) ? $response ["data"] ["object"] ["amount"] : 0;
        $transcationId = isset($response ["data"] ["object"] ["balance_transaction"]) ? $response ["data"] ["object"] ["balance_transaction"] : false;
        $application_fee_id = ! empty($response ["data"] ["object"] ["application_fee"]) ? $response ["data"] ["object"] ["application_fee"] : false;
        
        if ($chargeId && $transcationId) {
            $data = [
                'status' => $status,
                'txnId' => $chargeId
            ];
            $this->this_model->updateChargeStatus($data);
            // used for display the transaction charges, load the helper and call the app.
            $this->load->helper('stripe_connect_helper');
            
            // used for fetch the transection fee.
            // $resultTransaction = retrieveStripeBalanceTransaction($transcationId);
            
            $application_fee = 0;
            if ($application_fee_id) {
                $responseApplicationFee = retrieveStripeApplicationFee($application_fee_id);
                if ($responseApplicationFee ['status'] == 1) {
                    $application_fee = intval($responseApplicationFee ['message'] ['amount']);
                }
            }
            
            /*
             * if ($resultTransaction ['status'] == 1) {
             * $data ['transactionFee'] = intval($resultTransaction ['message'] ['fee_details'] [0] ['amount']) /
             * 100;
             * }
             * else {
             * $data ['transactionFee'] = 0;
             * }
             * $transcationFee = $data ['transactionFee'];
             */
            $this->sendChargeStatusMail($status, $chargeId, $amount, $application_fee);
        }
    }

    /**
     * This function is written for Stripe Webhook Response receiver for charge.failed type webhook
     * Means when Stripe Update any Status of your charged transaction to failed
     *
     * @param string $response json string received from STRIPE
     */
    public function saveChargeFailedUpdateResponse($response)
    {
        /* Defining default variables */
        $status = "failed";
        $chargeId = isset($response ["data"] ["object"] ["id"]) ? $response ["data"] ["object"] ["id"] : false;
        $amount = isset($response ["data"] ["object"] ["amount"]) ? $response ["data"] ["object"] ["amount"] : 0;
        
        if ($chargeId) {
            $data = [
                'status' => $status,
                'txnId' => $chargeId
            ];
            $this->this_model->updateChargeStatus($data);
            
            $this->sendChargeStatusMail($status, $chargeId, $amount, 0);
        }
    }

    /**
     * This function is written for Stripe Webhook Response receiver for charge.refunded type webhook
     * Means when Stripe Update any Status of your charged transaction to refunded
     *
     * @param string $response json string received from STRIPE
     */
    public function saveChargeRefundedUpdateResponse($response)
    {
        /* Defining default variables */
        $status = "refunded";
        $chargeId = isset($response ["data"] ["object"] ["id"]) ? $response ["data"] ["object"] ["id"] : false;
        if ($chargeId) {
            $data = [
                'status' => $status,
                'txnId' => $chargeId
            ];
            $this->this_model->updateChargeStatus($data);
        }
    }
    
    /**
     * This function is written for Stripe Webhook Response receiver for invoice.payment_failed type webhook
     * Means when Stripe Update any Status of your recurring transaction to failed
     *
     * @param string $response json string received from STRIPE
     */
    public function saveInvoicePaymentFailedUpdateResponse($response)
    {
        /* Defining default variables */
        $status = "failed";
        $chargeId = isset($response ["data"] ["object"] ["id"]) ? $response ["data"] ["object"] ["id"] : false;
        $amount = isset($response ["data"] ["object"] ["lines"] ["data"] [0] ["amount"]) ? $response ["data"] ["object"] ["lines"] ["data"] [0] ["amount"] : 0;
        $metadata = isset($response ["data"] ["object"] ["lines"] ["data"] [0] ["metadata"]) ? $response ["data"] ["object"] ["lines"] ["data"] [0] ["metadata"] : array();
        
        //divid amount by 100 for converting cents to exactly payment
        $amount = $amount/100;
        
        if ($chargeId && !empty($metadata)) {
            $data = [
                'fromUserID'=> $metadata['fanId'],
                'toUserId'=> $metadata['performerId'],
                'amount' => $amount,
                'status' => $status,
                'txnId' => $chargeId
            ];
            $this->this_model->updateAddChargeStatus($data);
            
            //$this->sendChargeStatusMail($status, $chargeId, $amount, 0);
        }
    }
    
    /**
     * This function is written for Stripe Webhook Response receiver for invoice.payment_succeeded type webhook
     * Means when Stripe Update any Status of your recurring transaction to succeeded
     *
     * @param string $response json string received from STRIPE
     */
    public function saveInvoicePaymentSuccessUpdateResponse($response)
    {
        
        /* Defining default variables */
        $status = "succeeded";
        $chargeId = isset($response ["data"] ["object"] ["id"]) ? $response ["data"] ["object"] ["id"] : false;
        $amount = isset($response ["data"] ["object"] ["lines"] ["data"] [0] ["amount"]) ? $response ["data"] ["object"] ["lines"] ["data"] [0] ["amount"] : 0;
        $metadata = isset($response ["data"] ["object"] ["lines"] ["data"] [0] ["metadata"]) ? $response ["data"] ["object"] ["lines"] ["data"] [0] ["metadata"] : array();
        
        //divid amount by 100 for converting cents to exactly payment
        $amount = $amount/100;
        
        if ($chargeId && !empty($metadata)) {
            
            $data = [
                'fromUserID'=> $metadata['fanId'],
                'toUserId'=> $metadata['performerId'],
                'amount' => $amount,
                'status' => $status,
                'txnId' => $chargeId
            ];
            $this->this_model->updateAddChargeStatus($data);
            // used for display the transaction charges, load the helper and call the app.
            $this->load->helper('stripe_connect_helper');
            
            // used for fetch the transection fee.
            // $resultTransaction = retrieveStripeBalanceTransaction($transcationId);
            
            $application_fee = 0;
//             if ($application_fee_id) {
//                 $responseApplicationFee = retrieveStripeApplicationFee($application_fee_id);
//                 if ($responseApplicationFee ['status'] == 1) {
//                     $application_fee = intval($responseApplicationFee ['message'] ['amount']);
//                 }
//             }
            
            /*
             * if ($resultTransaction ['status'] == 1) {
             * $data ['transactionFee'] = intval($resultTransaction ['message'] ['fee_details'] [0] ['amount']) /
             * 100;
             * }
             * else {
             * $data ['transactionFee'] = 0;
             * }
             * $transcationFee = $data ['transactionFee'];
             */
            //$this->sendChargeStatusMail($status, $chargeId, $amount, $application_fee);
        }
    }
    
    /**
     * This function is written for Stripe Webhook Response receiver for customer.subscription.updated type webhook
     * Means when Stripe Update any Status of your Subscription to canceled
     *
     * @param string $response json string received from STRIPE
     */
    public function saveSubscriptionUpdateResponse($response)
    {
        /* Defining default variables */
        $status = "Cancelled";
        $subId = isset($response ["data"] ["object"] ["id"]) ? $response ["data"] ["object"] ["id"] : false;
        $subStatus = isset($response ["data"] ["object"] ["status"]) ? $response ["data"] ["object"] ["status"] : false;
        $metadata = isset($response ["data"] ["object"] ["metadata"]) ? $response ["data"] ["object"] ["metadata"] : array();
        
        
        if ($subId && !empty($metadata) && $subStatus == 'canceled') {
            $data = [
                'fromUserID'=> $metadata['fanId'],
                'toUserId'=> $metadata['performerId'],
                'status' => $status,
                'subId' => $subId
            ];
            $this->this_model->updateSubscriptionStatus($data);
            
            //$this->sendChargeStatusMail($status, $chargeId, $amount, 0);
        }
    }
    
    /**
     * This function is written for Stripe Webhook Response receiver for customer.subscription.deleted type webhook
     * Means when Stripe Update any Status of your Subscription to canceled
     *
     * @param string $response json string received from STRIPE
     */
    public function saveSubscriptionDeletedUpdateResponse($response)
    {
        /* Defining default variables */
        $status = "Cancelled";
        $subId = isset($response ["data"] ["object"] ["id"]) ? $response ["data"] ["object"] ["id"] : false;
        $subStatus = isset($response ["data"] ["object"] ["status"]) ? $response ["data"] ["object"] ["status"] : false;
        $metadata = isset($response ["data"] ["object"] ["metadata"]) ? $response ["data"] ["object"] ["metadata"] : array();
        
        
        if ($subId && !empty($metadata)) {
            $data = [
                'fromUserID'=> $metadata['fanId'],
                'toUserId'=> $metadata['performerId'],
                'status' => $status,
                'subId' => $subId
            ];
            $this->this_model->updateSubscriptionStatus($data);
            
            //$this->sendChargeStatusMail($status, $chargeId, $amount, 0);
        }
    }

    /**
     * Used for sending the mail after getting the stripe webhook responce
     *
     * @param string $status, it may be succeeded / failed
     * @param string $chargeId , unique chargeId in transaction
     * @param int $amount, tip amount
     * @param int $application_fee , application fees in transaction
     *       
     */
    public function sendChargeStatusMail($status, $chargeId, $amount, $application_fee)
    {
        $dataUser = $this->this_model->getUserTippingUserInfo($chargeId);
        
        if ($dataUser) {
            
            // divide by 100 for amount
            $application_fee = $application_fee / 100;
            $amount = $amount / 100;
            
            $this->load->model($this->myvalues->profileDetails ['model'], 'profile_model');
            $fanInfo = $this->profile_model->getUserInformation($dataUser->from_user_id);
            $performerInfo = $this->profile_model->getUserInformation($dataUser->to_user_id);
            
            // faninfo data for sending mail to fan
            $data ['userData'] = $fanInfo;
            $data ['performerName'] = $performerInfo->chrName;
            $data ['transactionId'] = $chargeId;
            $data ['amount'] = showBalance($amount);
            
            // load the email template for fan mail
            $message = $this->load->view('email_templates/tipping/charge_' . $status . '_fan', $data, true);
            $data ["message"] = $message;
            $data ["from_title"] = EMAIL_TITLE;
            $data ["to"] = $fanInfo->user_name;
            $subjectMsg = $status == 'succeeded' ? 'You have given a tip to ' . $performerInfo->chrName : 'Your transaction for giving tip to ' . $performerInfo->chrName . ' has been failed or declined';
            $data ["subject"] = $subjectMsg;
            
            // send mail using utilitys
            $this->utility->sendMailSMTP($data);
            $toUser = $fanInfo->user_id;
            $fromUser = $performerInfo->user_id;
            /* send notification to fan or performer */
            $this->main_model->saveMessage($toUser, $fromUser, 'Tip Notification', $data ["subject"]);
            // if status = succeeded then mail send to performer
            if ($status == 'succeeded') {
                
                /* $data ['transactionFee'] = showBalance($transcationFee); */
                $data ['chargeAmount'] = showBalance($application_fee);
                // make the calculation for display the net amount in email.
                $data ['netAmount'] = showBalance($amount - $application_fee);
                $data ['userData'] = $performerInfo;
                
                $data ['fanName'] = $fanInfo->chrName;
                // load the template view for email.
                $message = $this->load->view('email_templates/tipping/charge_' . $status . '_performer', $data, true);
                $data ["message"] = $message;
                $data ["from_title"] = EMAIL_TITLE;
                $data ["to"] = $performerInfo->user_name;
                $data ["subject"] = 'Congratulations, you have just received tip from ' . $fanInfo->chrName;
                
                // send mail using utilitys
                $this->utility->sendMailSMTP($data);
                $toUser = $performerInfo->user_id;
                $fromUser = $fanInfo->user_id;
                /* send notification to fan or performer */
                $this->main_model->saveMessage($toUser, $fromUser, 'Tip Notification', $data ["subject"]);
            }
        }
    }
}