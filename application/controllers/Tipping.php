<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tipping extends General_controller
{
    public $myInfo = "";
    public $myInfoRoutes = "";

    public function __construct()
    {
        parent::__construct();
        
        $this->myInfo = $this->myvalues->tippingDetails;
        $this->folder = $this->myvalues->tippingDetails ['controller'];
        $this->loginDetailsInfo = $this->myvalues->loginDetails;
        $this->load->model($this->myInfo ["model"], "this_model");
        $this->load->helper('stripe_connect_helper');
        /* need to load stripe lib becoz we are using methods from lib */
    }

    /**
     * index
     * this method loads tipping registration form when a new performer want to create his stripe account
     * to get tip from fan
     */
    public function index()
    {
        /* get all countries where stripe serivces are available */
        $this->load->model($this->myvalues->profileDetails ['model'], 'profile_model');
        $data ['countries'] = $this->this_model->getCountries();
        $data ['title'] = 'Registration For Tipping';
        $data ['controllerName'] = $this->myInfo ['controller'];
        /* get user information for autofill his few information in tipping register form */
        $data ['address'] = $this->profile_model->getUserInformation($this->pUserId);
        if ($data ['address']->stripe_account_id != '' || $this->pidGroup != 3) {
            redirect(SITEURL, "location");
        }
        
        $this->myView($this->folder . "/tipping_register_view", $data);
    }

    /**
     * loadView
     * this method will be used only when ajax request call to load view based on account type which we get in
     * request
     *
     * @param string $accountType is account type
     * @return load a view according to ajax request
     */
    public function loadView($accountType, $country)
    {
        $this->load->model($this->myvalues->profileDetails ['model'], 'profile_model');
        $view = 'stripe_registration/stripe_register_' . strtolower($country) . '_' . strtolower($accountType) . '_view';
        $data ['address'] = $this->profile_model->getUserInformation($this->pUserId);
        $this->load->view($view, $data);
    }

    public function stripFormData()
    {    // $bankAccountResponse = $this->this_model->addExernalAccounts();
        // $updateResponse = $this->this_model->updateStripeAccount();
        /* once customer apply for ragistration first time this this method will call */
        $response = $this->this_model->genrateStripeAccount();
        /* this method true only when get true from above method */
        if ($response) {
            /* this method add an exernal account of customer as we have asked bank information in form */
            $response = $this->this_model->addExernalAccounts();
            /* after creating bank account called below method */
            if ($response === true) {
                /* update stripe account whenever user will update his info only this method will call */
                $response = $this->this_model->updateStripeAccount();
                
                if ($response != 'Account updated successfully.') {
                    $response = "Personal :: " . $response;
                    // if update tipping view loaded then not call delete stripe account method
                    if ($this->input->post('checkUpdateTipping') == 'No') {
                        // call the delete account stripe
                        
                        $this->this_model->deleteStripeAccount($this->pUserId);
                    }
                }
                
            }
            else {
                // call the delete account stripe
                $response = "Payment :: " . $response;
                $this->this_model->deleteStripeAccount($this->pUserId);
            }
        }
        /* if get this message from stripe we set a flash message to show success alert */
        if ($response == 'Account updated successfully.') {
            if($this->input->post('checkUpdateTipping') =='Yes' )
            {                
                $this->utility->setFlashMessage('success', 'Tipping Account Updated Successfully');
            }else{               
                $this->utility->setFlashMessage('success', 'Tipping Account Created Successfully');
            }
        }
        /* response from stripe method either true or some error of stripe */
        echo $response;
    }

    /**
     * paymentOptions
     * This method shows user existing cards and bank account detials performer can add more cards and banks
     * as well as can delete olds
     *
     * @return return to view and create listing of account and bank cards
     */
    public function paymentOptions()
    {
        $accountId = $this->this_model->getStripeAccountId();
        if (! empty($accountId)) {
            
            $response = getStripeExternalAccounts($accountId);
            $data ['bankAndCards'] = $response ['message'];
            $this->load->model($this->myvalues->profileDetails ['model'], 'profile_model');
            $data ['controllerName'] = $this->myInfo ['controller'];
            $data ['title'] = 'Account Listing';
            $data ['countries'] = $this->this_model->getCountries();
            $data ['userInfo'] = $this->profile_model->getUserInformation($this->pUserId);
            $this->myView($this->folder . "/list_external_accounts_view", $data);
        }
        else {
            // redirect to register for tipping view
            redirect(SITEURL . $this->myInfo ['controller']);
        }
    }

    /**
     * This function is used for checking if login user(fan) have stripe id & other data
     */
    public function checkStripeIdUser()
    {
        $data = array();
        // function return the user email id, if user have not stripe account id
        $resultCheckStripeId = $this->this_model->checkStripeIdUser($this->pUserId);
        
        if (! is_numeric($this->utility->decode($this->input->post('performerUserid')))) {
            redirect(SITEURL, "location");
        }
        else {
            $data ['performerUserId'] = $this->input->post('performerUserid');
        }
        
        if ($resultCheckStripeId === true) {
            // stripe id found for login user & fetch the card list
            $this->load->model($this->myvalues->profileDetails ['model'], 'profileModel');
            $fanInfo = $this->profileModel->getUserInformation($this->pUserId);
            $cardDetails = getStripeCustomerCards($fanInfo->stripe_customer_id);
            $data ['displayCards'] = $cardDetails ['message'];
            $data ['recurringTipInfo'] = $this->this_model->getRecurringTipInfo($this->pUserId,$this->utility->decode($this->input->post('performerUserid')));
        }
        else {
            // stripe id not found for login user
            $description = "Stagegator fan : " . $this->pUserId;
            $resultStripeId = createStripeCustomer($resultCheckStripeId, $description);
            
            // Update the stripe id in user tabel
            if (isset($resultStripeId ['customerId']) && ! empty($resultStripeId ['customerId'])) {
                $resUpdate = $this->this_model->updateStripCustomerId($resultStripeId ['customerId'], $this->pUserId);
                $data ['displayCards'] = array();
                $data ['recurringTipInfo'] = array();
            }
        }
        
        $this->myViewAjax('ajax/tipping_form_view', $data);
    }

    /**
     * This function is used for make the payment , to give the tip to performer by fan
     *
     * @param int tipAmountValue , amout to be transfer
     * @param string stripeToken, stripe token
     * @param string perfomerUserid , performer user id
     * @param boolean addCard, checkbox value for add card
     * @param string selectedCardId, selected card id of allreday added card, that is in encoded form
     *       
     * @return string $message, that conatin the error / success message
     */
    public function makePaymentTip()
    {
        if ($this->input->post()) {
            
            if (is_numeric($this->utility->decode($this->input->post('perfomerUserid')))) {
                
                $tipAmount = $this->input->post('tipAmountValue');
                $stripeToken = $this->input->post('stripeToken');
                
                //Recurring Info
                $tipsType = $this->input->post('tipstype');
                $recurringType = $this->input->post('recurringtype');
                
                // user id of performer
                $perfomerUserid = $this->utility->decode($this->input->post('perfomerUserid'));
                $this->load->model($this->myvalues->subscriptionDetails ['model'], 'subscriptionModel');
                $planid = $this->subscriptionModel->getCurrentPlanDetails($perfomerUserid, 3);
                
                $this->load->model($this->myvalues->profileDetails ['model'], 'profileModel');
                $userInfo = $this->profileModel->getUserInformation($perfomerUserid);
                // fetch the stripe id of performer
                $accountId = $userInfo->stripe_account_id;
                $fanInfo = $this->profileModel->getUserInformation($this->pUserId);
                
                $description = "Tip from :" . $fanInfo->chrName;
                $statement_descriptor = "Stagegator Tip";
                /* pssing both performer and fan id as metadata to stripe */
                $metadata = [
                    'performerId' => $perfomerUserid,
                    'fanId' => $fanInfo->user_id
                ];
                
                if (! empty($planid) && ! empty($accountId)) {
                    
                    //make the entry of recurring type info if tipstype 'recurring'
                    if($tipsType == 'recurring'){
                           
                           $stripeToken = $this->input->post('stripeToken');
                           
                           $resultRecurring = $this->this_model->saveRecurringTipInfo($this->pUserId, $perfomerUserid, $tipAmount, $tipsType, $recurringType, $stripeToken);
                           
                           if($resultRecurring['status']){
                               $status = 'Success';
                               $message = $this->lang->line("success_tip_message");
                           }else{
                               $status = 'Error';
                               if($resultRecurring['message']){
                                   $message = $resultRecurring['message'];
                               }else{
                                   $message = $this->lang->line("error_default_error");
                               }
                           }
                           
                           die(json_encode([
                               "status" => $status,
                               "message" => $message
                           ]));
                        
                         exit();
                    }else{
                        // round the application fee for stripe, also added the additional charges accrding to plan
                        $applicationFee = round(($planid [0]->tips_percentage * $tipAmount) / 100 + $planid [0]->tips_additional, 2);
                        
                        // got the stripe token by add new card
                        if (! empty($this->input->post('stripeToken'))) {
                            
                            $stripeToken = $this->input->post('stripeToken');
                            // if checkbox for remember my card checked then save the card details
    
                            if ($this->input->post('addCard') == 'true') {
                                
                                // add the stripe Customer Card by this method
                                $resultAddCard = addStripeCustomerCard($fanInfo->stripe_customer_id, $stripeToken);
                                
                                // card sucessfully added & make the payment
                                if ($resultAddCard ['status'] == 1) {
                                    // make the payment by this method
                                    $resultbyNewcardPayment = chargeStripeCustomerBySavedCard($accountId, $fanInfo->stripe_customer_id, $resultAddCard ['message'] ['id'], $tipAmount, $description, $applicationFee, "usd", $metadata, $statement_descriptor);
                                    
                                    // payment is succeeded
                                    if ($resultbyNewcardPayment ['status'] === true) {
                                        // if stripe status is success then display this msg
                                        if ($resultbyNewcardPayment ['stripeStatus'] == 'succeeded') {
                                            $status = 'Success';
                                            $message = $this->lang->line("success_tip_message");
                                        }
                                        else if ($resultbyNewcardPayment ['stripeStatus'] == 'pending') {
                                            // if stripe status is pending then display this msg
                                            $status = 'Pending';
                                            $message = $this->lang->line("panding_tip_message");
                                        }
                                        else {
                                            // defult error found then display this.
                                            $status = 'Error';
                                            $message = $resultbyNewcardPayment ['message'];
                                        }
                                        
                                        // make the entry in log table
                                        $this->this_model->makeTipplingHistoryLog($this->pUserId, $perfomerUserid, $tipAmount, $resultbyNewcardPayment ['chargeId'], $resultbyNewcardPayment ['stripeStatus']);
                                        
                                        
                                        die(json_encode([
                                            "status" => $status,
                                            "message" => $message
                                        ]));
                                    }
                                    else {
                                        // if status != true then found error , display that msg
                                        die(json_encode([
                                            "status" => 'Error',
                                            "message" => $resultbyNewcardPayment ['message']
                                        ]));
                                    }
                                    exit();
                                }
                            }
                            else {
                                // done payment without check the check box remember my card.this pament is done by
                                // entering the card data
                                $resultPayment = chargeStripeCustomerByToken($accountId, $stripeToken, $tipAmount, $description, "usd", $applicationFee, $metadata, $statement_descriptor);
                                
                                // payment is succeeded
                                if ($resultPayment ['status'] === true) {
                                    if ($resultPayment ['stripeStatus'] == 'succeeded') {
                                        // if stripe status is succeeded then display this msg
                                        $status = 'Success';
                                        $message = $this->lang->line("success_tip_message");
                                    }
                                    else if ($resultPayment ['stripeStatus'] == 'pending') {
                                        // if stripe status is pending then display this msg
                                        $status = 'Pending';
                                        $message = $this->lang->line("panding_tip_message");
                                    }
                                    else {
                                        // defult error found then display this.
                                        $status = 'Error';
                                        $message = $resultPayment ['message'];
                                    }
                                    // make the entry in log table
                                    $this->this_model->makeTipplingHistoryLog($this->pUserId, $perfomerUserid, $tipAmount, $resultPayment ['chargeId'], 'Pending');
                                    
                                    
                                    die(json_encode([
                                        "status" => $status,
                                        "message" => $message
                                    ]));
                                }
                                else {
                                    // if status != true then found error , display that msg
                                    die(json_encode([
                                        "status" => 'Error',
                                        "message" => $resultPayment ['message']
                                    ]));
                                }
                                exit();
                            }
                        }
                        else {
                            // done payment with alreday added card
                            $cardId = $this->utility->decode($this->input->post('selectedCardId'));
                            $resultbycardPayment = chargeStripeCustomerBySavedCard($accountId, $fanInfo->stripe_customer_id, $cardId, $tipAmount, $description, $applicationFee, "usd", $metadata, $statement_descriptor);
                            
                            if ($resultbycardPayment ['status'] === true) {
                                // if stripe status is succeeded then display this msg
                                if ($resultbycardPayment ['stripeStatus'] == 'succeeded') {
                                    $status = 'Success';
                                    $message = $this->lang->line("success_tip_message");
                                }
                                else if ($resultbycardPayment ['stripeStatus'] == 'pending') {
                                    // if stripe status is pending then display this msg
                                    $status = 'Pending';
                                    $message = $this->lang->line("panding_tip_message");
                                }
                                else {
                                    // defult error found then display this.
                                    $status = 'Error';
                                    $message = $resultbycardPayment ['message'];
                                }
                                // make the entry in log table
                                $this->this_model->makeTipplingHistoryLog($this->pUserId, $perfomerUserid, $tipAmount, $resultbycardPayment ['chargeId'], 'Pending');
                                
                                die(json_encode([
                                    "status" => $status,
                                    "message" => $message
                                ]));
                            }
                            else {
                                // if status != true then found error , display that msg
                                die(json_encode([
                                    "status" => 'Error',
                                    "message" => $resultbycardPayment ['message']
                                ]));
                            }
                            exit();
                        }
                    }
                }
            }
        }
    }

    /**
     * updateTipping()
     * This method shows update user form to update performer information in stripe
     *
     * @return return to view after getting needed datas
     */
    public function updateTipping()
    {
        /* using profile model to get profile informations */
        $this->load->model($this->myvalues->profileDetails ['model'], 'profile_model');
        /* getting user information */
        $data ['address'] = $this->profile_model->getUserInformation($this->pUserId);
        /*
         * if logged in perfomer's stripe_account_id not null and status not matched with below statuses then
         * redirect to siteurl
         */
        if ($data ['address']->stripe_status == 'Rejected' || $data ['address']->stripe_status == 'Unverified') {
            
            /* set status isUpdate and using this variable */
            $data ['isUpdate'] = 'YES';
            /* gettting countries for showing autofill country state city in form */
            $data ['countries'] = $this->this_model->getCountries();
            $data ['controllerName'] = $this->myInfo ['controller'];
            $this->myView($this->folder . "/tipping_register_view", $data);
        }
        else {
            redirect(SITEURL, "location");
        }
    }

    /**
     * addBankAccount()
     * Method adds new bank account in stripe
     *
     * @return return to paymenrOptions controller
     */
    public function addBankAccount()
    {
        /* only method call when data get in post method */
        if ($this->input->post()) {
            
            /* getting current user account id from database */
            $accountId = $this->this_model->getStripeAccountId();
            
            /* getting form data in post */
            $accHolderName = $this->input->post('account_holder_name');
            $country = $this->input->post('CmbCountries');
            $currency = $this->input->post('currency');
            $bankAccNo = $this->input->post('account');
            $routingNo = $this->input->post('routing_number');
            
            // Used for check if entered bank account data are already registered or not
            $resultCheckDuplicate = $this->this_model->checkDuplicateBankAccount($accountId, $bankAccNo, $routingNo);
            
            if ($resultCheckDuplicate ['status'] != true) {
                // not found duplicate bank details
                
                /* Calling createStripeBankAccount method to create new bank account in stripe */
                $response = createStripeBankAccount($accountId, $accHolderName, $bankAccNo, $country, $currency, $routingNo);
                /*
                 * from stripe response not get false then set success message and send redirect otherwise set
                 * defualt
                 * error message of stripe
                 */
                if (! empty($response ['status'])) {
                    $this->utility->setFlashMessage('success', 'Bank Account Added Successfully');
                }
                else {
                    
                    $this->utility->setFlashMessage('danger', $response ['message']);
                }
            }
            else {
                // found duplicate bank details
                $this->utility->setFlashMessage('danger', $resultCheckDuplicate ['message']);
            }
            
            redirect(SITEURL . $this->myInfo ['controller'] . '/paymentOptions');
        }
    }

    /**
     * addNewCard
     * This method add new card of logged in performer to create new card in stripe account
     */
    public function addNewCard()
    {
        /* when data comes in post that time we proccess to create a card in stripe */
        if ($this->input->post()) {
            
            $token = $this->input->post('stripeToken');
            $accountId = $this->this_model->getStripeAccountId();
            $expMonthVar = $this->input->post('exp-month');
            $expYearVar = $this->input->post('exp-year');
            $cardNumber = $this->input->post('card_number');
            
            $resultCheckDuplicate = $this->this_model->checkDuplicateDebitCard($accountId, $expMonthVar, $expYearVar, $cardNumber);
            
            if ($resultCheckDuplicate ['status'] != true) {
                
                // not found duplicate debit card details
                $response = createStripeCardAccount($accountId, $token);
                
                /*
                 * if status if ture from stripe then show success msg otherwise show error message and redirect to
                 * paymentOptions method
                 */
                if (! empty($response ['status'])) {
                    $this->utility->setFlashMessage('success', 'Debit Card Added Successfully');
                }
                else {
                    $this->utility->setFlashMessage('danger', $response ['message']);
                }
            }
            else {
                // found duplicate debit card details
                $this->utility->setFlashMessage('danger', $resultCheckDuplicate ['message']);
            }
        }
        redirect(SITEURL . $this->myInfo ['controller'] . '/paymentOptions');
    }

    /**
     * deleteDetails
     * This method delete an external account from stripe also it is calling from ajax request
     * used for fan cards only
     *
     * @param string $id is id of external account
     * @return boolean when an ajax request would be called otherwise after deleting redirect to same method
     */
    public function deleteDetails($id)
    {
        $this->load->model($this->myvalues->profileDetails ['model'], 'profile_model');
        $customer = $this->profile_model->getUserInformation($this->pUserId);
        
        $customerId = $customer->stripe_customer_id;
        $externalId = $this->utility->decode($id);
        
        $response = deleteStripeSavedCard($customerId, $externalId);
        if ($this->input->is_ajax_request()) {
            if ($response ['status']) {
                echo true;
            }
            else {
                echo $response ['message'];
            }
            return false;
        }
        if ($response ['status']) {
            $this->utility->setFlashMessage('success', $this->lang->line('success_delete_card'));
        }
        else {
            $this->utility->setFlashMessage('danger', $response ['message']);
        }
        redirect(SITEURL . $this->myInfo ['controller'] . '/paymentOptions');
    }

    /**
     * Used for delete the card or bank details , called from performer user.
     * used only by performer, called from manage Bank Cards menu for delete card.
     * Note : if the defult card/ bank detail want to delete then this function will set the another external
     * account as default
     *
     * @param string, $id that encoded value of card id
     *       
     * @return boolean true/false
     */
    public function deleteExternalDetails($id)
    {
        $accountId = $this->this_model->getStripeAccountId();
        $externalId = $this->utility->decode($id);
        
        $toMakeDefultCard ['id'] = array();
        
        // found the card & bank data.
        $responseGetAllData = getStripeExternalAccounts($accountId);
        
        if ($responseGetAllData ['status'] == 1) {
            $responseAllAccountData = array_merge($responseGetAllData ['message'] ['banks'], $responseGetAllData ['message'] ['cards']);
            
            // check if only one remaing card want to delete ?
            if (count($responseAllAccountData) != 1) {
                
                // fetch the card / bank details which want to delete
                $resultFetchDataAccount = getStripeExternalAccountData($accountId, $externalId);
                
                // check if we want to delete the default external account
                if ($resultFetchDataAccount ['status'] == 1 && $resultFetchDataAccount ['message'] ['default_for_currency'] == 1 && $resultFetchDataAccount ['message'] ['id'] == $externalId) {
                    
                    foreach ($responseAllAccountData as $key => $value) {
                        
                        // get the other external id which want to set default account.
                        
                        if ($value ['default_for_currency'] != 1 && empty($toMakeDefultCard ['id'])) {
                            
                            // set this external id as default
                            $toMakeDefultCard ['id'] = $value ['id'];
                        }
                    }
                    
                    // make the toMakeDefultCard account as default no need to uset the current account from the
                    // default
                    $resulttemp1 = setDefaultStripeExternalAccount($accountId, $toMakeDefultCard ['id'], true);
                }
                
                // code for delete the account
                $response = deleteStripeExternalAccount($accountId, $externalId);
                
                if ($this->input->is_ajax_request()) {
                    if ($response ['status']) {
                        echo true;
                    }
                    else {
                        echo $response ['message'];
                    }
                    return false;
                }
                
                if ($response ['status']) {
                    $this->utility->setFlashMessage('success', $this->lang->line('success_delete_external_account'));
                }
                else {
                    $this->utility->setFlashMessage('danger', $response ['message']);
                }
            }
            else {
                // display error if delete remaining one card
                $this->utility->setFlashMessage('danger', $this->lang->line('can_not_delete_all_external_account'));
            }
        }
        else {
            // display error if found the issue while fetching the bank / card details
            $this->utility->setFlashMessage('danger', $responseGetAllData ['message']);
        }
        redirect(SITEURL . $this->myInfo ['controller'] . '/paymentOptions');
    }

    /**
     * updateTipping()
     * This method show form show update form and user can update their details
     */
    /*
     * public function updateTipping()
     * {
     * $this->load->model($this->myvalues->profileDetails ['model'], 'profile_model');
     * $data ['address'] = $this->profile_model->getUserInformation($this->pUserId);
     * if ($data ['address']->stripe_account_id != '' && ($data ['address']->stripe_status != 'Rejected' || $data
     * ['address']->stripe_status == 'Unverified')) {
     * redirect(SITEURL, "location");
     * }
     * // set status isUpdate and using this variable
     * $data ['isUpdate'] = 'YES';
     * $data ['countries'] = $this->this_model->getCountries();
     * $data ['controllerName'] = $this->myInfo ['controller'];
     * $data ['address'] = $this->profile_model->getUserInformation($this->pUserId);
     * $this->myView($this->folder . "/tipping_register_view", $data);
     * }
     */
    /**
     * Used for display the tip History which is given by fan (login user), this view used for fan user only.
     */
    public function tipHistory()
    {
        $data ['result'] = $this->this_model->getTipHistory($this->pUserId);
        $data ['controllerName'] = $this->myInfo ['controller'];
        $data ["title"] = $this->lang->line('tip_history');
        $this->myView($this->folder . "/tipping_history_view", $data);
    }

    /**
     * Used for display the tip History (that performer got) which was given by fan,this view used for Performer
     * user only , this display recordes of only 90 days.
     */
    public function tipPerformerBalance()
    {
        $stripe_account_key = $this->this_model->getStripeKey($this->pUserId);
        $this->load->model($this->myvalues->profileDetails ['model'], 'profile_model');
        $userInfo = $this->profile_model->getUserInformation($this->pUserId);
        
        $data ["title"] = $this->lang->line('payment_history');
        
        if ($userInfo->stripe_status == 'Verified') {
            
            // set the lastId null first time
            $lastId = null;
            $tempDataArray = array();
            
            // if get the post data that is start_date & end date then set data
            if ($this->input->post()) {
                
                $start_date = DateTime::createFromFormat('m-d-Y', $this->input->post('txtStartDate'));
                
                $start_date = $start_date->format('Y-m-d');
                
                $end_date = DateTime::createFromFormat('m-d-Y', $this->input->post('txtEndDate'));
                
                $end_date = $end_date->format('Y-m-d');
            }
            else {
                // set the end date as today if page loaded first time
                $end_date = date('Y-m-d');
                // set the end date as today -90 day so that start date is previus 90 days.
                $start_date = date('Y-m-d', strtotime("-3 months", strtotime($end_date)));
            }
            
            // set the go to label to call
            FetchAppData:
            
            // call the app data from helper
            $result = getStripeBalanceTransactions($stripe_account_key, 100, $lastId, $start_date, $end_date);
            // if data get & status =1 then get another data by calling the app, because this result the data only
            // 100
            // records.
            if (! empty($result ['status']) && $result ['status'] == 1) {
                
                $resultAppData = array();
                // store data in array
                $resultAppData = $result ['message'] ['data'];
                // merge data in array
                $tempDataArray = array_merge($tempDataArray, $result ['message'] ['data']);
                // find the length of fetched data to check if record is 100
                $legthOfData = count($result ['message'] ['data']);
                
                $lastArray = array();
                
                // if length of data =100 then it may be possible another data to fetch
                if ($legthOfData == 100) {
                    // fetch the last element in fetch array to get the last transaction id , to fetch another data
                    // by
                    // app.
                    $lastArray = end($resultAppData);
                    
                    $lastId = $lastArray ['id'];
                    unset($lastArray);
                    // call to label to make the loop.
                    goto FetchAppData;
                }
            }
            
            $data ['result'] = $tempDataArray;
            
            $data ['controller'] = $this->myvalues->tippingDetails ['controller'];
            // load the view
            $this->myView($this->folder . "/tipping_performer_payment_history_view", $data);
        }
        else {
            redirect(SITEURL, "location");
        }
    }

    /**
     * requestWithdrawal
     * This method called withdrawal payment from stripe
     */
    public function requestWithdrawal()
    {
        /* get secret key of performer */
        $getSecretKey = $this->this_model->getStripeKey($this->pUserId);
        
        /*
         * if amount get in post then proccess to withdrawal and redirect after error or success method otherwise
         * again load tipping withdrawal view
         */
        if ($this->input->post()) {
            $cardOrBankInfo = $this->utility->decode($this->input->post('cardInfo'));
            $txtAmount = $this->input->post('txtAmount');
            $discription = 'Withdrawal Request Of ' . $txtAmount . ' On ' . date('Y-m-d h:i:s');
            $response = createStripeTransfer($getSecretKey, $cardOrBankInfo, $txtAmount, $discription);
            /* if withdrawal request successfully done by stripe then save data to withdrawal history table */
            if ($response ['status']) {
                $this->this_model->saveWithdrawalHistory($response);
                $this->utility->setFlashMessage('success', 'Withdrawal Request Submited Successfully');
            }
            else {
                $this->utility->setFlashMessage('danger', $response ['message']);
            }
            
            redirect(SITEURL . $this->myInfo ['controller'] . '/requestWithdrawal');
        }
        
        $data ['stripeBalance'] = getStripeBalance($getSecretKey);
        $accountId = $this->this_model->getStripeAccountId();
        $response = getStripeExternalAccounts($accountId);
        $data ['bankAndCards'] = $response ['message'];
        $data ['title'] ='Withdrawal Payment';
        $data ['controllerName'] = $this->myInfo ['controller'];
        $res = $this->myView($this->folder . '/tipping_withdrawal_view', $data);
    }

    /**
     * verifyBankCardAmount
     * This method called when user verify his account in stripe
     * user transfer two types of amount in stripe if amount matched in stripe database means user account is
     * authenticated otherwise show errors
     *
     * @return return to paymentOptioncontroller
     *        
     */
    public function verifyBankCardAmount()
    {
        $accountId = $this->session->userdata("UserData")->stripe_account_id;
        $bankId = $this->input->post('hdnBankCardId');
        $amount1 = $this->input->post('txtAmountSecond');
        $amount2 = $this->input->post('txtAmountFirst');
        $response = verifyStripeAccountBank($accountId, $bankId, $amount1, $amount2);
        if (! empty($response ['status'])) {
            $this->utility->setFlashMessage('success', $response ['message']);
        }
        else {
            $this->utility->setFlashMessage('danger', $response ['message']);
        }
        redirect(SITEURL . $this->myInfo ['controller'] . '/paymentOptions');
    }
    
    /**
     * Used for display the tip History which is given by fan (login user), this view used for fan user only.
     */
    public function enabledrecurringtip()
    {
        $data ['result'] = $this->this_model->getEnabledRecurringTip($this->pUserId);
//         $data ['result'] = $this->this_model->getTipHistory($this->pUserId);
        $data ['controllerName'] = $this->myInfo ['controller'];
        $data ["title"] = $this->lang->line('enabled_recurring_tip');
        $this->myView($this->folder . "/enabled_recurring_tip_view", $data);
    }
    
    /**
     * Used for display the tip History which is given by fan (login user), this view used for fan user only.
     */
    public function updateStripeSubscriptionStatus($recurringId,$performerId,$subscriptionId)
    {
        $recurringId = $this->utility->decode($recurringId);
        $performerId = $this->utility->decode($performerId);
        $subscriptionId = $subscriptionId;
        
        if(!empty($recurringId) && !empty($recurringId) && !empty($recurringId)){
            
            $subscriptionResponce = $this->this_model->updateStripeSubscriptionStatus($recurringId,$performerId,$subscriptionId);
            if($subscriptionResponce){
                $this->utility->setFlashMessage('success', $this->lang->line('success_cancel'));
            }else{
                $this->utility->setFlashMessage('danger', $this->lang->line('danger_cancel'));
            }
        }else{
            $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
        }
        redirect(SITEURL . $this->myInfo ['controller'] . '/enabledrecurringtip');
    }
}
