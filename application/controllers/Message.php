<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Message extends General_controller
{
    public $myInfo = "";

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->messageDetails;
        $this->folder = $this->myInfo ["controller"];
        $this->load->model($this->myInfo ["model"], "this_model");
    }

    public function index()
    {
        $data ["result"] = $this->this_model->getInboxList($this->pUserId);
        $data ["controllerName"] = $this->myInfo ['controller'];
        $data ["title"] = "Notifications";
        $this->myView($this->folder . "/message_inbox_view", $data);
    }

    public function sent()
    {
        $data ["result"] = $this->this_model->getSenList($this->pUserId);
        $data ["controllerName"] = $this->myInfo ['controller'];
        $data ["title"] = "Message Sent";
        $this->myView($this->folder . "/message_sent_view", $data);
    }

    public function DeleteMessage($status)
    {
        if (! $this->input->post("chkstatus")) {
            $this->utility->setFlashMessage('danger', "Please select alteast one message");
        }
        else {
            $res = $this->this_model->updateMessageTableStatus($status, $this->pUserId);
            if ($res) {
                $this->utility->setFlashMessage('success', "Message Removed Successfully");
            }
            else {
                $this->utility->setFlashMessage('danger', $this->lang->line("error_default_error"));
            }
        }
        $method = $status == "sentStatus" ? '/sent' : '';
        redirect(SITEURL . $this->myInfo ['controller'] . $method);
    }
}
