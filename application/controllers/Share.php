<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Share extends Guest_controller
{
    public $myInfo = "";

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Used for share(display) the event data
     * called from share url that is in event manage(list) page from venue side
     *
     * @param string $access, that contain the encoded event id
     *       
     */
    public function event()
    {
        $accssToken = $this->input->get("access");
        $accssToken = $this->utility->decode($accssToken);
        
        // explode the event id & event schedule id
        $idArray = explode("||||", $accssToken);
        
        // check if event schedule id is int
        if (array_key_exists(1, $idArray)) {
            if (! ctype_digit($idArray [1])) {
                show_404();
                exit();
            }
        }
        
        // check if event id is int
        if (array_key_exists(0, $idArray)) {
            if (! ctype_digit($idArray [0])) {
                show_404();
                exit();
            }
        }
        
        // get the event schedule id
        if (array_key_exists(1, $idArray) && ! empty($idArray [1]) && ctype_digit($idArray [1])) {
            
            // call the event schedule info view
            $this->eventschedule($idArray [1]);
        }
        else if (array_key_exists(0, $idArray) && ! empty($idArray [0]) && ctype_digit($idArray [0])) {
            
            $idEvent = $idArray [0];
            
            $this->load->model($this->myvalues->eventDetails ["model"], "event_model");
            $data ['result'] = $this->event_model->getEventById($idEvent);
            // set og tags for socail network crowlers
            if (! empty($data ['result'])) {
                $data ['tag'] ['og:title'] = $data ['result']->title . ' ' . dateDisplay($data ['result']->start_date . ' ' . $data ['result']->startTime) . " To " . dateDisplay($data ['result']->start_date . ' ' . $data ['result']->startTime);
                $data ['tag'] ['og:image'] = EXTERNAL_PATH . 'images/title.png';
                $data ['tag'] ['og:url'] = $this->utility->generateOverviewUrl($data ['result']->idEvent);
                $data ['tag'] ['og:description'] = $data ['result']->descr;
            }
            
            $data ['title'] = "Event Information ";
            $data ["showMenu"] = true;
            $this->load->model($this->myvalues->locationDetails ['model'], 'location_model');
            $resLocation = $this->location_model->getCurrentUserLocationsById($data ['result']->location_id);
            $data ['location'] = $resLocation [0];
            
            // get all event schedule list
            $data ['eventScheduleList'] = $this->event_model->getEventListByEventId($idEvent);
            
            if (empty($data ['result'])) {
                $this->utility->setFlashMessage('success', $this->lang->line('error_not_found_event_detail'));
                redirect(SITEURL);
            }
            $this->myView('share/share_event_public_view', $data);
        }
        else {
            show_404();
            exit();
        }
    }

    /**
     * Used for share(display) the event scheule data
     * called from share url by event function
     *
     * @param $idEventSchedule, that contain the encoded event schedule id
     */
    public function eventschedule($idEventSchedule)
    {
        /*
         * $idEventSchedule = $this->input->get("access");
         * $idEventSchedule = $this->utility->decode($idEventSchedule);
         *
         * if (! ctype_digit($idEventSchedule)) {
         * show_404();
         * exit();
         * }
         */
        $this->load->model($this->myvalues->eventDetails ["model"], "event_model");
        $data ['result'] = $this->event_model->getEventSchedule($idEventSchedule);
        
        // set og tags for socail network crowlers
        if (! empty($data ['result'])) {
            $data ['tag'] ['og:title'] = $data ['result']->title . ' ' . dateDisplay($data ['result']->start_date . '' . $data ['result']->startTime);
            $data ['tag'] ['og:image'] = EXTERNAL_PATH . 'images/title.png';
            $data ['tag'] ['og:url'] = $this->utility->generateOverviewUrl($data ['result']->idEvent, $data ['result']->table_id);
            $data ['tag'] ['og:description'] = $data ['result']->descr;
        }
        $data ['title'] = "Show Information ";
        $this->load->model($this->myvalues->locationDetails ['model'], 'location_model');
        $resLocation = $this->location_model->getCurrentUserLocationsById($data ['result']->location_id);
        $data ['location'] = ! empty($resLocation) ? $resLocation [0] : NULL;
        
        // get the perfomer list based on event schedule
        $this->load->model($this->myvalues->timeTableDetails ['model'], 'timetable_model');
        $data ["performerlist"] = $this->timetable_model->getEventTimetable($idEventSchedule);
        $data ["timetableController"] = $this->myvalues->timeTableDetails ['controller'];
        $data ["controllerName"] = 'share';
        $data ["accssToken"] = $this->input->get("access");
        
        //detect mobile device
        $data ["mobileDevice"] = '';
        if (!empty($this->input->cookie('fcmIdToken'))) {
            $fcmCookie = $this->utility->decode($this->input->cookie('fcmIdToken', TRUE));
            $tokenArray = explode("||||", $fcmCookie);
            if (! empty($tokenArray)) {
                $data ["mobileDevice"] = $tokenArray[0];
            }
        }
        
        if($this->input->is_ajax_request()){
            $this->myViewAjax('ajax/perfomers_list', $data);
        }else{
            if (empty($data ['result'])) {
                $this->utility->setFlashMessage('success', $this->lang->line('error_not_found_event_detail'));
                redirect(SITEURL);
            }
            
            $this->myView('share/share_event_schdule_public_view', $data);
        }
        
    }
}