<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Past_events extends General_controller
{
    public $myInfo = "";
    

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->pasteventsDetails;
        $this->folder = $this->myInfo ["controller"];
        $this->load->model($this->myInfo ["model"], "this_model");
        
    }

    /**
     * show()
     * This method list all event data in a table
     *
     * @return return a view and show it
     */
    public function index()
    {
        // panding_ranking conatin the id of eventschedules which has event ranking is panding        
        $data ["pending_ranking"] = $this->main_model->get_pending_ranking_event($this->pUserId);
        /* get all past event  list */
        $data ["list"] = $this->this_model->getPastEventList($this->pUserId);
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["title"] = "Manage past Show/Series";
        $this->myView($this->folder."/past_events_manage_view", $data);
    }
    
 /**
     * schedule()
     * This method is check all event shedule for current event
     *
     * @param string $id ,encoded event id
     */
    public function schedule($id = NULL)
    {
        $id = $this->utility->decode($id);
        /* check is schedule id is integer*/
        if (ctype_digit($id)) {
            
            $data ["list"] = $this->this_model->getEventSheduleList($id);
            $data ["title"] = $this->lang->line("manage_schedule2");
            $data ["pending_ranking"] = $this->main_model->get_pending_ranking_event($this->pUserId);
            $data ["controllerName"] = $this->myInfo ["controller"];
            $this->myView($this->folder . "/past_event_schedule_view", $data);
        }
        else {
            
            $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
            redirect(SITEURL . $this->myInfo ['controller']);
        }
    }
    /**
     * shows()
     * This method show past event or shedule for performer 
     */
   public function shows(){
       
       $data ["title"] = 'Past shows';
       $data ["controllerName"] = $this->myInfo ["controller"];
       
       $this->load->model($this->myvalues->performerDetails['model'],'performer_model');
       
       /* check if ranking panding by performer for any venue*/
       $data ["pending_rankings"] = $this->performer_model->get_events_pending_for_ranking_by_performer($this->pUserId);
       
      // dd($data ["pending_rankings"]);
       
       // this is the event schedule ids ,which has (HOST)performer have remaining to rank to Other perfomer user.
       $data ["host_give_ranking"] = $this->performer_model->get_events_panding_ranking_by_host($this->pUserId);
       
       /* get performer past events */
       $data ["list"] = $this->this_model->getPerformerPastEvents($this->pUserId);
      
       $this->myView($this->myInfo ["controller"] . "/past_events_myshow_view", $data);
   }
}

