<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Invitation extends General_controller
{
    public $myInfo = "";

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->invitationDetails;
        $this->folder = $this->myInfo ["controller"];
        $this->load->model($this->myInfo ["model"], "this_model");
        
        // $this->perfomerInfo = $this->myvalues->performerDetails;
        // $this->load->model($this->perfomerInfo ["model"], "perfomer_model");
    }

    /**
     * sent()
     * get booking request where booking request send by venue
     */
    public function sent()
    {
        $requestByPerformer = $this->pidGroup == "3" ? 1 : 0;
        $data ["list"] = $this->this_model->venueInviteEvents($this->pUserId, $requestByPerformer);
        $data ['controllerName'] = $this->myInfo ['controller'];
        $data ["title"] = $this->lang->line('booking_invitations_sent');
        $this->myView($this->folder . "/invitation_performance_venue", $data);
    }

    /**
     * received()
     * This method use for recive list of those performer who applied for event
     */
    public function received()
    {
        // function contain second params 1 where performer has applied for event
        $requestByPerformer = $this->pidGroup == "3" ? 0 : 1;
        $data ["list"] = $this->this_model->venueInviteEvents($this->pUserId, $requestByPerformer);
        
        $data ['controllerName'] = $this->myInfo ['controller'];
        $data ["title"] = $this->lang->line('booking_requests_received');
        $this->myView($this->folder . "/invitation_accept_performance_venue_view", $data);
    }

    /**
     * inviteForEvent()
     * This method invite performer
     *
     * @param string $id
     */
    public function inviteForEvent($id = NULL)
    {
        if ($this->input->post()) {
            $this->load->model($this->myvalues->performerDetails ['model'], 'performer_model');
            
            /* If VENUE login then we will check its monthly limit for performers according to its plan */
            if ($this->pidGroup == VENUE_GROUP) {
                $planInfo = $this->subscription_model->getCurrentPlanDetails($this->pUserId, $this->pidGroup);
                
                if ($planInfo [0]->performers == "0") {
                    $this->utility->setFlashMessage('danger', $this->lang->line('forbidden_function') . " Please upgrade your plan ! <a href='".SITEURL . $this->myvalues->subscriptionDetails["controller"] ."'>Click Here</a>");
                    redirect(SITEURL . $this->myvalues->performerDetails ['controller'] . "/searchPerformer/");
                }
                elseif ($planInfo [0]->performers != "-1") {
                    $this->load->model($this->myvalues->invitationDetails ["model"], "invitation_model");
                    $counts = $this->invitation_model->getAcceptPendingCount($this->pUserId, $this->utility->decode($id));
                    if ($counts >= $planInfo [0]->performers) {
                        $this->utility->setFlashMessage('danger', "You have reached your monthly allowed performer's limit. Please upgrade your plan ! <a href='".SITEURL . $this->myvalues->subscriptionDetails["controller"] ."'>Click Here</a>");
                        redirect(SITEURL . $this->myvalues->performerDetails ['controller'] . "/searchPerformer/");
                    }
                }
            }
            
            // this function used for check if event date passed
            
            $this->main_model->CheckEventPassed($this->input->post('idEvent'));
            
            // check event schedule delete or not before invite
            $this->load->model($this->myvalues->eventDetails ['model'], 'event_model');
            $checkEventSchduleDelete = $this->event_model->CheckEventSchduleDelete($this->input->post('idEvent'));
            
            if ($checkEventSchduleDelete == 0) {
                $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
                redirect(SITEURL . $this->myvalues->performerDetails ['controller'] . '/searchPerformer');
            }
            
            $status = $this->performer_model->sendBulkInvite();
            if ($status) {
                $this->utility->setFlashMessage('success', $this->lang->line('performers_successfully_invited'));
            }
            else {
                $this->utility->setFlashMessage('danger', $this->lang->line('this_performer_already_exists_in_the_registration_queue'));
            }
            redirect(SITEURL . $this->myvalues->performerDetails ['controller'] . '/searchPerformer');
        }
        
        $id = $this->utility->decode($id);
        if (ctype_digit($id)) {
            $this->load->model($this->myvalues->profileDetails ['model'], 'profile_model');
            $this->load->model($this->myvalues->eventDetails ['model'], 'event_model');
            $data ["title"] = $this->lang->line('invite_performer_for_event');
            $data ["controllerName"] = $this->myInfo ["controller"];
            $data ["performer"] = $this->profile_model->getUserInformation($id);
            $styles = $this->profile_model->getUserStyleForOverview($id);
            
            foreach ($styles as $k => $v) {
                $data ["style"] [] = $v->idStyle;
            }
            
            /* By passing TRUE as second parameter this function will return all schedulues list */
            $data ["eventsData"] = $this->event_model->get_event_list($this->pUserId, true, $data ["style"]);
            $idEvent = [
                0
            ];
            
            foreach ($data ["eventsData"] as $value) {
                
                $idEvent [] = $value->idEvent;
            }
            
            $data ["events"] = $this->event_model->getEventListByEventId($idEvent);
            $this->myView($this->folder . "/invitation_invite_performer", $data);
        }
        else {
            $this->utility->setFlashMessage('danger', 'default_error_message');
            redirect(SITEURL . $this->myvalues->performerDetails ['controller'] . '/searchPerformer');
        }
    }

    /**
     * venueCancelInvite()
     * This method use for cancle a invitatation
     *
     * @param unknown $idRegister
     */
    function venueCancelInvite($idRegister = "", $idEvent = "", $idPerformer = "")
    {
        $this->load->model($this->myvalues->eventDetails ['model'], 'event_model');
        $idRegister = $this->utility->decode($idRegister);
        $idPerformer = $this->utility->decode($idPerformer);
        $idEvent = $this->utility->decode($idEvent);
        
        if (ctype_digit($idRegister) && ctype_digit($idEvent) && ctype_digit($idPerformer)) {
            
            // this function used for check if event date passed
            $checkEventPassed = $this->main_model->CheckEventPassed($this->event_model->getEventIdByRegisterId($idRegister));
            
            $r = $this->event_model->venueUpdateRequest("Cancelled", $idRegister, $idEvent, $idPerformer);
            if ($r == 0) {
                $this->utility->setFlashMessage('danger', $this->lang->line('invite_could_not_be_cancelled'));
            }
            else {
                $this->utility->setFlashMessage('success', $this->lang->line('invite_cancelled_successfully'));
            }
        }
        else {
            $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
        }
        redirect(SITEURL . $this->myInfo ['controller'] . '/sent');
    }

    /**
     * sendReminder()
     * This method for send reminder for multipal and singal performer
     *
     * @param unknown $eventId
     *
     * @return return boolean result either true or false
     */
    public function sendReminder($regID = NULL, $idEvent = NULL)
    {
        $this->load->model($this->myvalues->eventDetails ['model'], 'event_model');
        if ($this->input->post('chkstatus')) {
            
            $result = $this->event_model->sendInvitationReminder();
        }
        else {
            
            $result = $this->event_model->sendInvitationReminder([
                $regID
            ], [
                $idEvent
            ], $this->pUserId);
        }
        
        if ($result) {
            $this->utility->setFlashMessage('success', $this->lang->line('reminder_sent_successfully'));
        }
        else {
            $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
        }
        
        redirect(SITEURL . $this->myvalues->invitationDetails ['controller'] . '/sent');
    }

    /**
     * used for accept the request of venue by performer user
     *
     * @param int $id, id of register table
     * @param int $idEvent, id of event.
     * @param string $idPerformer, id of performer user
     */
    public function venueAcceptRequest($id = "", $idEvent = "", $idPerformer = "")
    {
        $id = $this->utility->decode($id);
        $idEvent = $this->utility->decode($idEvent);
        $idPerformer = $this->utility->decode($idPerformer);
        
        $this->load->model($this->myvalues->eventDetails ['model'], 'event_model');
        
        // this function used for check if event date passed
        $checkEventPassed = $this->main_model->CheckEventPassed($this->event_model->getEventIdByRegisterId($id));
        
        // used for redirect to the page from which request is comming.
        $this->load->library('user_agent');
        
        // used for check if register table data exist in tabel or not for event / event schedule delete
        
        $checkRegisterId = $this->event_model->CheckEventRegisterIdDelete($id);
        
        if ($checkRegisterId == 0) {
            $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
            redirect($this->agent->referrer());
        }
        
        if (ctype_digit($id) && ctype_digit($idEvent) && ctype_digit($idPerformer)) {
            $this->load->model($this->myvalues->eventDetails ['model'], 'event_model');
            
            /* If VENUE login then we will check its monthly limit for performers according to its plan */
            if ($this->pidGroup == VENUE_GROUP) {
                $planInfo = $this->subscription_model->getCurrentPlanDetails($this->pUserId, $this->pidGroup);
                
                if ($planInfo [0]->performers == "0") {
                    $this->utility->setFlashMessage('danger', $this->lang->line('forbidden_function') . " Please upgrade your plan ! <a href='".SITEURL . $this->myvalues->subscriptionDetails["controller"] ."'>Click Here</a>");
                    redirect(SITEURL . $this->myInfo ['controller'] . "/received");
                }
                elseif ($planInfo [0]->performers != "-1") {
                    $getSchdeuleId = $this->this_model->getScheduleFromRegisterId($id);
                    $counts = $this->this_model->getAcceptPendingCount($this->pUserId, $getSchdeuleId [0]->idEvent);
                    if ($counts >= $planInfo [0]->performers) {
                        $this->utility->setFlashMessage('danger', "You have reached your monthly allowed performer's limit. Please upgrade your plan ! <a href='".SITEURL . $this->myvalues->subscriptionDetails["controller"] ."'>Click Here</a>");
                        redirect(SITEURL . $this->myInfo ['controller'] . "/received");
                    }
                }
            }
            
            /* If VENUE login then we will check its monthly limit for performers according to its plan */
            if ($this->pidGroup == PERFOMER_GROUP) {
                $this->load->model($this->myvalues->eventDetails ["model"], "event_model");
                $eventInfo = $this->event_model->getEventById($idEvent);
                
                /* This method check all validation for plan */
                $resultLimit = $this->event_model->checkPerformerLimit($eventInfo->type);
                if ($resultLimit !== true) {
                    $this->utility->setFlashMessage('danger', $resultLimit);
                    redirect(SITEURL . $this->myInfo ['controller'] . "/received");
                }
            }
            
            $response = $this->event_model->performerUpdateRequest("Accepted", $id, $idEvent, $idPerformer);
            if ($response) {
                $this->utility->setFlashMessage('success', $this->lang->line('request_approved_successfully'));
            }
            else {
                $this->utility->setFlashMessage('danger', $this->lang->line('request_could_not_be_approved'));
            }
        }
        else {
            $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
        }
        
        redirect($this->agent->referrer());
        
        // redirect(SITEURL . $this->myInfo ['controller'] . "/received");
    }

    /**
     * used for reject the request of venu user by performer user
     *
     * @param int $id, id of register table
     * @param int $idEvent, id of event.
     * @param string $idPerformer, id of performer user
     */
    public function venueRejectRequest($id = "", $idEvent = "", $idPerformer = "")
    {
        $id = $this->utility->decode($id);
        $idEvent = $this->utility->decode($idEvent);
        $idPerformer = $this->utility->decode($idPerformer);
        
        $this->load->model($this->myvalues->eventDetails ['model'], 'event_model');
        
        // this function used for check if event date passed
        $checkEventPassed = $this->main_model->CheckEventPassed($this->event_model->getEventIdByRegisterId($id));
        
        // used for check if register table data exist in tabel or not for event / event schedule delete
        $checkRegisterId = $this->event_model->CheckEventRegisterIdDelete($id);
        
        if ($checkRegisterId == 0) {
            $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
            redirect(SITEURL . $this->myInfo ['controller'] . "/received");
        }
        
        if (ctype_digit($id) && ctype_digit($idEvent) && ctype_digit($idPerformer)) {
            
            $response = $this->event_model->performerUpdateRequest("Rejected", $id, $idEvent, $idPerformer);
            if ($response) {
                $this->utility->setFlashMessage('success', $this->lang->line('request_rejected_successfully'));
            }
            else {
                $this->utility->setFlashMessage('danger', $this->lang->line('request_could_not_be_rejected'));
            }
        }
        else {
            $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
        }
        redirect(SITEURL . $this->myInfo ['controller'] . "/received");
    }

    /**
     * This method use for receive list of those performer who applied for event for host
     */
    public function event_line_ups()
    {
        // function contain second params 1 where performer has applied for event
        $requestByPerformer = 1;
        $eventAccess = $this->this_model->getHostSchedules($this->pUserId);
        $data ["list"] = $this->this_model->venueInviteEvents($this->pUserId, $requestByPerformer, $eventAccess);
        
        $data ['controllerName'] = $this->myInfo ['controller'];
        $data ["title"] = "Host Event Line Ups";
        $this->myView($this->folder . "/invitation_accept_performance_venue_view", $data);
    }
}
