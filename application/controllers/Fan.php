<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Fan extends Fan_controller
{
    public $myInfo = "";
    public $myInfoRoutes = "";

    public function __construct()
    {
        parent::__construct();
        
        $this->myInfo = $this->myvalues->fanDetails;
        $this->folder = $this->myInfo ["controller"];
        $this->load->model($this->myInfo ["model"], "this_model");
    }

    /**
     * performerStream()
     * This function load all data of perfromer upcommin events of performer those performer bited by fan
     *
     * @return return to a view
     */
    public function performerStream()
    {
        $data ["list"] = $this->this_model->getPerformerStream($this->pUserId);        
        $data ["title"] = "Recent StageGator Bookings";
        $data['msg'] = 'will perform on';
        $this->myView($this->folder . "/fan_performer_stream_view", $data);
    }

    /**
     * venueStream()
     * This function get venue stream
     */
    public function venueStream()
    {
        $data ["list"] = $this->this_model->getVenueStream($this->pUserId);
        $data ["title"] = "Upcoming StageGator Events";
        $data['msg'] = 'will host'; 
        
        $this->myView($this->folder . "/fan_performer_stream_view", $data);
    }
    
    /**
     * myStream() ( Combind stream of both performer and venue)
     * This function get comebind stream
     */
    public function myStream()
    {
        
        $data ["list"] = $this->this_model->getMyStream($this->pUserId);
        $data ["title"] = "Recent/Upcoming StageGator Events";
        $data['msg'] = 'will host';
        $this->myView($this->folder . "/fan_performer_stream_view", $data);
    }
}
