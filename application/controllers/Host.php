<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Host extends General_controller
{
    public $myInfo = "";

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->hostDetails;
        $this->folder = $this->myInfo ["controller"];
        $this->load->model($this->myInfo ["model"], "this_model");
    }

    public function index()
    {
        $this->host_management();
    }

    /**
     * Used for get the list of events for book host that are created by venue user
     */
    public function new_host_invite()
    {
        $data ["title"] = $this->lang->line('new_host_invitation');
        $data ["controller"] = $this->myInfo ["controller"];
        
        // If the venue must rank performers on an already passed show before continuing, an error message will
        // notify the venue and redirect the user to the event list page.
        /*
         * if (performers_pending_ranking_by_venue()){
         * $this->session->set_flashdata("red_msg","You must rank past performers before continuing!");
         * redirect("event/event_list");
         * }
         */
        
        $data ["list"] = $this->this_model->get_venue_events($this->pUserId);
        $this->myView($this->myInfo ["controller"] . "/host_new_invite_view", $data);
    }

    /**
     * used for apply the invite perfomer
     *
     * @param string $evtSchdeuleId, event schdule id
     */
    public function view_performers($evtSchdeuleId = "")
    {
        $evtSchdeuleId = $this->utility->decode($evtSchdeuleId);
        
        if (! ctype_digit($evtSchdeuleId)) {
            show_404();
            exit();
        }
        
        $data ["controller"] = $this->myInfo ["controller"];
        
        $this->load->model($this->myvalues->performerDetails ['model'], 'perfomer_model');
        $eventInfo = $this->perfomer_model->getEventBySchuduleId($evtSchdeuleId);
        
        $data ["title"] = $this->utility->decodeText(@$eventInfo->title) . ' Event Performers';
        
        // $this->load->model($this->myvalues->timeTableDetails ['model'], 'timeTable_model');
        $data ["list"] = $this->this_model->getAcceptedUsers($evtSchdeuleId);
        $this->myView($this->myInfo ["controller"] . "/host_see_performers_view", $data);
    }

    /**
     * Used for invite the host
     *
     * @param string $evtSchdeuleId , event schdule id
     * @param string $perfomerId , perfomer User id
     */
    public function inviteHost($evtSchdeuleId = "", $perfomerId = "")
    {
        $data ["controller"] = $this->myInfo ["controller"];
        
        $evtSchdeuleId = $this->utility->decode($evtSchdeuleId);
        $perfomerId = $this->utility->decode($perfomerId);
        
        if (! ctype_digit($evtSchdeuleId) || ! ctype_digit($perfomerId)) {
            show_404();
            exit();
        }
        
        $r = $this->this_model->invite_host($evtSchdeuleId, $perfomerId, $this->pUserId);
        
        if ($r == 0) {
            // $this->session->set_flashdata ( 'red_msg', 'Record Not Updated' );
            
            $this->utility->setFlashMessage('danger', $this->lang->line('error_record_not_updated'));
        }
        else if ($r == - 1) {
            // $this->session->set_flashdata ( 'red_msg', 'Host Already Selected for the Show!' );
            
            $this->utility->setFlashMessage('danger', $this->lang->line('host_already_selected_for_show'));
        }
        else if ($r == 1) {
            // $this->session->set_flashdata ( 'green_msg', 'Host Invited Successfully' );
            $this->utility->setFlashMessage('success', $this->lang->line('host_invited_successfully'));
        }
        redirect(SITEURL . $this->myInfo ["controller"] . '/host_management');
    }

    /**
     * Used for manage the host functionality by venue user
     */
    function host_management()
    {
        // this is remaining
        /*
         * if (performers_pending_ranking_by_venue()){
         * $this->utility->setFlashMessage('danger', "you must rank past performers before continuing!");
         * redirect(SITEURL.$this->myvalues->eventDetails['controller']."/event");
         * }
         */
        
        if (! empty($this->input->post())) {
            
            // var_dump($this->input->post());exit;
            $registerId = $this->utility->decode($this->input->post('hdnRegId'));
            
            if (! ctype_digit($registerId)) {
                show_404();
                exit();
            }
            
            $r = $this->this_model->assign_rights($this->pUserId);
            if ($r == 0) {
                // $this->session->set_flashdata('red_msg', 'Record Not Updated');
                $this->utility->setFlashMessage('danger', $this->lang->line('error_record_not_updated'));
            }
            else {
                // $this->session->set_flashdata('green_msg', 'Record Updated Successfully');
                $this->utility->setFlashMessage('success', $this->lang->line('record_updated_successfully'));
            }
            redirect(SITEURL . $this->myInfo ["controller"] . '/host_management');
        }
        
        $data ["controller"] = $this->myInfo ["controller"];
        $data ["title"] = $this->lang->line('host_management');
        $data ["list"] = $this->this_model->get_host_requests($this->pUserId);
         
        $this->myView($this->myInfo ["controller"] . "/host_management_view", $data);
    }

    /**
     * Used for cancel the host by venue user
     *
     * @param unknown $idRegister
     */
    function cancel_host_invite($idRegister = "")
    {
        $registerId = $this->utility->decode($idRegister);
        
        if (! ctype_digit($registerId)) {
            show_404();
            exit();
        }
        
        $r = $this->this_model->cancel_host_invite($registerId, $this->pUserId);
        
        if ($r == 0) {
            // $this->session->set_flashdata ( 'red_msg', 'Access not Revoked. Please contact system
            // administrator.' );
            $this->utility->setFlashMessage('danger', $this->lang->line('access_not_revoked_contact_administrator'));
        }
        else {
            // $this->session->set_flashdata ( 'green_msg', 'Access revoked Successfully' );
            $this->utility->setFlashMessage('success', $this->lang->line('access_revoked_successfully'));
        }
        redirect(SITEURL . $this->myInfo ["controller"] . '/host_management');
    }

    /**
     * Used for display accept the invitation page related to perfomer, this invitation is send from venue user
     * this also called from mail link that is send when venue send invitation request to perfomer user
     */
    function hostReceived()
    {
        /*
         * this is remainig
         * if (performers_pending_ranking_by_venue()){
         * $this->utility->setFlashMessage('danger', "you must rank past performers before continuing!");
         * redirect(SITEURL.$this->myvalues->performerDetails['controller']."/event");
         * }
         */
        $data ["list"] = $this->this_model->hostReceivedRequests($this->pUserId);
        $data ["controller"] = $this->myInfo ["controller"];
        $data ["title"] = $this->lang->line('host_request');
        $this->myView($this->myInfo ["controller"] . "/host_request_received_view", $data);
    }

    /**
     * Used for accept the host request by perfomer
     *
     * @param string $idRegister, incodeed register id
     */
    function acceptHost($idRegister = "")
    {
        $idRegister = $this->utility->decode($idRegister);
        
        if (! ctype_digit($idRegister)) {
            show_404();
            exit();
        }
        
        $r = $this->this_model->accept_host($idRegister);
        if ($r == 0) {
            // $this->session->set_flashdata('red_msg', 'Request could not be Accepted');
            $this->utility->setFlashMessage('danger', $this->lang->line('request_could_not_be_accepted'));
        }
        else {
            // $this->session->set_flashdata('green_msg', 'Request Accepted Successfully');
            $this->utility->setFlashMessage('success', $this->lang->line('request_accepted_successfully'));
        }
        redirect(SITEURL . $this->myInfo ["controller"] . '/hostReceived');
    }

    /**
     * Used for reject the host request by perfomer
     *
     * @param string $idRegister, incodeed register id
     */
    function rejectHost($idRegister = "")
    {
        $idRegister = $this->utility->decode($idRegister);
        
        if (! ctype_digit($idRegister)) {
            show_404();
            exit();
        }
        
        $r = $this->this_model->reject_host($idRegister);
        if ($r == 0) {
            // $this->session->set_flashdata('red_msg', 'Request could not be Rejected');
            $this->utility->setFlashMessage('danger', $this->lang->line('request_could_not_be_rejected'));
        }
        else {
            // $this->session->set_flashdata('green_msg', 'Request Rejected Successfully');
            $this->utility->setFlashMessage('success', $this->lang->line('request_rejected_successfully'));
        }
        redirect(SITEURL . $this->myInfo ["controller"] . '/hostReceived');
    }
}