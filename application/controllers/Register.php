<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Register extends Guest_controller
{
    public $myInfo = "";
    public $myInfoRoutes = "";

    public function __construct()
    {
        parent::__construct();
        
        $this->myInfo = $this->myvalues->userRegisterDetails;
        $this->loginDetailsInfo = $this->myvalues->loginDetails;
        $this->load->model($this->myInfo ["model"], "this_model");
    }

    /**
     * setRules()
     *
     * @param integer $extraFiled checks if user is registerd as a venue than we check extra fileds
     *        This function is used to set rules for form
     */
    public function setRules($extraFiled)
    {
        $this->rules [] = [
            "field" => "txtName",
            "label" => $this->lang->line('register_name'),
            "rules" => "trim|required|max_length[250]",
            "errors" => [
                'required' => $extraFiled == 1 ? $this->lang->line('error_please_enter_venue_title') : $this->lang->line('error_please_enter_user_name')
            ]
        ];
        
        if ($extraFiled == 1) {
            $this->rules [] = [
                "field" => "txtContact_person",
                "label" => $this->lang->line('register_contact_person'),
                "rules" => "trim|required|max_length[250]",
                "errors" => [
                    'required' => $this->lang->line('error_please_enter_user_name')
                ]
            ];
        }
        
        $this->rules [] = [
            "field" => "cmbType",
            "label" => $this->lang->line('register_account_type'),
            "rules" => "trim|required|integer|max_length[1]",
            "errors" => [
                'required' => $this->lang->line('error_please_select_a_usertype')
            ]
        ];
        
        if($this->input->post('cmbType') == 4){
            $this->rules [] = [
                "field" => "txtPhone",
                "label" => $this->lang->line('register_phone'),
                "rules" => "trim|required|callback_phoneValidation",
                "errors" => [
                    'required' => $this->lang->line('error_please_enter_mobile'),
                    'phoneValidation' => $this->lang->line('error_please_enter_a_valid_phone_number')
                ]
            ];
        }
        
        $this->rules [] = [
            "field" => "txtEmail",
            "label" => $this->lang->line('register_email_address'),
            "rules" => "trim|required|valid_email|max_length[250]|is_unique[user.user_name]|callback_emailBlockCheck",
            "errors" => [
                'required' => $this->lang->line('error_please_enter_email_address'),
                'valid_email' => $this->lang->line('error_please_enter_valid_email_address'),
                'max_length' => $this->lang->line('error_email_address_maxlength'),
                'is_unique' => $this->lang->line('register_email_exists'),
                'emailBlockCheck' => $this->lang->line('email_block_msg')
            ]
        ];
        
        $this->rules [] = [
            "field" => "txtPassword",
            "label" => $this->lang->line('register_password'),
            "rules" => "trim|required|callback_passwordCheck|max_length[32]|min_length[6]",
            "errors" => [
                'required' => $this->lang->line('error_please_enter_password'),
                'passwordCheck' => $this->lang->line('error_no_space_allowed'),
                'max_length' => $this->lang->line('error_max_length_password'),
                'min_length' => $this->lang->line('error_min_length_password')
            ]
        ];
        $this->rules [] = [
            "field" => "txtConfirmPassword",
            "label" => $this->lang->line('register_confirm_password'),
            "rules" => "trim|matches[txtPassword]",
            "errors" => [
                'matches' => $this->lang->line('error_confirm_password_notmatched')
            ]
        ];
        
        $this->rules [] = [
            "field" => "g-recaptcha-response",
            "label" => $this->lang->line('register_verify_that_you_are_a_human'),
            "rules" => "required",
            "errors" => [
                'required' => $this->lang->line('please_select_a_valid_captcha')
            ]
        ];
    }

    /**
     * Used for block the email id that contain this domain name
     * 
     * @param string $email, user's entred email id
     * 
     * @return boolean
     */
    public function emailBlockCheck($email)
    {
        list ($user, $domain) = explode('@', $email);
        
        $blockDomainArray = array();
        
        if (in_array($domain, $blockDomainArray)) {
            return false;
        }
        return true;
    }

    /**
     * passwordCheck remove whitespace from password
     *
     * @param string $str is passowrd
     *       
     * @return boolean ture or false
     */
    public function passwordCheck($str)
    {
        if (preg_match('/\s/', $str)) {
            
            return false;
        }
        return true;
    }

    /**
     * phoneValidation()
     * This method for us mobile number validation
     *
     * @param unknown $mob
     * @return boolean
     */
    function phoneValidation($mob)
    {
        if (preg_match('/^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/', $mob)) {
            
            return true;
        }
        
        return false;
    }

    /**
     * index()
     * This is index is landing pageod registration flow
     */
    public function index()
    {
        $data ["showMenu"] = false;
        $data ["noHeader"] = true;
        $data ["addUserController"] = $this->myInfo ["controller"];
        $this->myView("register_landing_view", $data);
    }

    /**
     * add()
     * This method create a user account and send a verification link to user
     *
     * @param string $userType
     * @return return to Login controller if Register user successfully
     */
    public function add($userType = '')
    {
        
        /* if requested url did not match with three array pameters then redirect to login */
        if (empty($userType) || ! in_array($userType, [
            "performer",
            "venue",
            "fan"
        ])) {
            redirect(SITEURL . $this->loginDetailsInfo ['controller']);
        }
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["showMenu"] = false;
        $data ["noHeader"] = true;
        $data ["userType"] = $userType;
        // $data ["registerUrl"] = SITEURL . $this->myInfo ["controller"] . '/add/' . $userType;
        // $data ['title'] = $this->lang->line('register_new_account');
        $data ['userType'] = $userType;
        
        if ($this->input->post()) {
            
            $extraFiled = $userType == "venue" ? 1 : 0;
            $this->setRules($extraFiled);
            $valid = $this->validateData($this->rules);
            // check server side validation for email
            if ($valid) {
                $g_recaptcha_response = $this->utility->encodeText($this->input->post('g-recaptcha-response'));
                // call method and check googe captcha response
                $getResult = $this->getGoogleCaptchaResponse($g_recaptcha_response);
                $status = json_decode($getResult, true);
                /* if google captcha response success not 1 then return to same page with validation error */
                if (isset($status ['success']) && $status ['success'] != 1) {
                    
                    $this->utility->setFlashMessage('danger', $this->lang->line('please_select_a_valid_captcha'));
                }
                else {
                    $this->utility->setFlashMessage('danger', $getResult);
                }
                
                $respose = $this->this_model->create_user();
                
                if ($respose) {
                    
                    if($this->input->post('cmbType') == FAN_GROUP){
                        
                        $this->load->model($this->myvalues->loginDetails ["model"], "login_model");
                        //modify password to md5() formate when login after registration
                        $_POST['txtPassword'] = md5($this->input->post('txtPassword'));
                        // get user email and password and get response from user table using login_model's method
                        $result = $this->login_model->authenticate();
                        // a common method for generate users session and login them
                        $this->login_model->generateSession($result [0]);
                        
                    }else{
                        $this->utility->setFlashMessage('success', $this->lang->line('register_successfull'));
                        redirect(SITEURL . $this->loginDetailsInfo ['controller'], 'location');
                    }
                }
                else {
                    
                    $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
                    // redirect(SITEURL . $this->loginDetailsInfo ['controller'], 'location');
                }
            }
        }
        
        $this->myView("register_signup_view", $data);
    }

    public function edit()
    {}

    public function delete()
    {}

    public function search()
    {}

    /**
     * checkDuplicateEmail()
     * This method for check duplicate email
     *
     * @param string $email first parameter
     *       
     * @return bool
     */
    function checkDuplicateEmail()
    {
        echo $this->this_model->checkDuplicate();
    }

    /**
     * getGoogleCaptchaResponse()
     * This method get google captcha url and get response from the url
     *
     * @param string $url a url which gets response from google
     *       
     * @return Object $culData return json response
     */
    function getGoogleCaptchaResponse($g_recaptcha_response)
    {
        $url = "https://www.google.com/recaptcha/api/siteverify?secret=" . GOOGLE_CAPTCHA_SECRET . "&response=" . $g_recaptcha_response . "&remoteip;=" . $this->common->getRealIp();
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
        $curlData = curl_exec($curl);
        $error = curl_error($curl);
        curl_close($curl);
        if ($error) {
            return error;
        }
        
        return $curlData;
    }
    
}
