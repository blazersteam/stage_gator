<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Event extends General_controller
{
    public $myInfo = "";
    public $editEventId = "";

    public function __construct()
    {
        parent::__construct();
        $this->myInfo = $this->myvalues->eventDetails;
        $this->folder = $this->myInfo ["controller"];
        $this->load->model($this->myInfo ["model"], "this_model");
        
        // $this->perfomerInfo = $this->myvalues->performerDetails;
        // $this->load->model($this->perfomerInfo ["model"], "perfomer_model");
        
        /* Allowed methods for Performer */
        $allowedFunctions = [
            "search",
            "apply"
        ];
        $commonMethod = [
            "overview"
        ];
        
        if (($this->pidGroup == "3" && (! in_array($this->router->method, $allowedFunctions)) || ($this->pidGroup == "2" && in_array($this->router->method, $allowedFunctions))) && ! in_array($this->router->method, $commonMethod)) {
            show_404();
            exit();
        }
    }

    /**
     * show()
     * This method list all event data in a table
     *
     * @return return a view and show it
     */
    public function index()
    {
        
        /* get all event list */
        // panding_ranking conatin the id of eventschedules which has event ranking is panding
        $data ["pending_ranking"] = $this->main_model->get_pending_ranking_event($this->pUserId);
        $data ["list"] = $this->this_model->get_event_list($this->pUserId);
        $data ["title"] = $this->lang->line("manage_schedule_series_show");
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["registerUrl"] = SITEURL . $data ["controllerName"] . "/add";
        $data ["title"] = "Manage Show/Series";
        $this->myView($this->folder . "/event_manage_view", $data);
    }

    /**
     * setRules()
     * This method check server side validation
     */
    function setRules()
    {
        $this->rules [] = [
            
            "field" => "rdaCreateType",
            "label" => $this->lang->line('create_a'),
            "rules" => "trim|required",
            "errors" => [
                'required' => $this->lang->line('error_please_select_type_of_event')
            ]
        ];
        
        $this->rules [] = [
            "field" => "txtName",
            "label" => $this->lang->line('event_title'),
            "rules" => "trim|required",
            "errors" => [
                'required' => $this->lang->line('error_please_enter_name')
            ]
        ];
        $this->rules [] = [
            "field" => "cmbLocation_id",
            "label" => $this->lang->line('location'),
            "rules" => "trim|required",
            "errors" => [
                'required' => $this->lang->line('error_please_enter_your_location')
            ]
        ];
        $this->rules [] = [
            "field" => "rdaType",
            "label" => $this->lang->line('event_type'),
            "rules" => "trim|required|callback_checkVenueLimit",
            "errors" => [
                'required' => $this->lang->line('error_please_select_a_event_type')
            ]
        ];
        
        $this->rules [] = [
            "field" => "cmbStyle[]",
            "label" => $this->lang->line('style'),
            "rules" => "trim|required|integer",
            "errors" => [
                'required' => $this->lang->line('error_please_select_a_style'),
                'integer' => $this->lang->line('error_please_select_valid_style')
            ]
        ];
        $this->rules [] = [
            "field" => "cmbIsRecuring",
            "label" => $this->lang->line('recurring'),
            "rules" => "trim|required|integer",
            "errors" => [
                'required' => $this->lang->line('error_please_select_recurrin_type'),
                'integer' => $this->lang->line('error_please_select_a_valid_recurring_type')
            ]
        ];
        
        /* cmbMonthDay reamning */
        
        $this->rules [] = [
            "field" => "txtStartDate",
            "label" => $this->lang->line('start_date'),
            "rules" => "trim|required",
            "errors" => [
                'required' => $this->lang->line('error_please_select_start_date')
            ]
        ];
        if ($this->input->post('cmbIsRecuring') > 2) {
            $this->rules [] = [
                "field" => "txtEndDate",
                "label" => $this->lang->line('end_date'),
                "rules" => "trim|required",
                "errors" => [
                    'required' => $this->lang->line('error_please_select_end_date')
                ]
            ];
        }
        
        if ($this->input->post('cmbIsRecuring') == 3) {
            $this->rules [] = [
                "field" => "chkDay[]",
                "label" => $this->lang->line('select_days'),
                "rules" => "trim|required",
                "errors" => [
                    'required' => $this->lang->line('error_please_select_days')
                ]
            ];
        }
        if ($this->input->post('cmbIsRecuring') == "4") {
            
            if (! $this->input->post("cmbWeekInMonth")) {
                $this->rules [] = [
                    "field" => "cmbMonthDay[]",
                    "label" => $this->lang->line('select_days'),
                    "rules" => "trim|required",
                    "errors" => [
                        'required' => $this->lang->line('error_please_select_days_in_month')
                    ]
                ];
            }
            else {
                $this->rules [] = [
                    "field" => "cmbDaysInMonth[]",
                    "label" => $this->lang->line('repeating_every'),
                    "rules" => "trim|required",
                    "errors" => [
                        'required' => $this->lang->line('error_please_select_recurrin_days')
                    ]
                ];
                
                $cmbWeekInMonthArray = $this->input->post('cmbWeekInMonth');
                
                if (! empty($cmbWeekInMonthArray)) {
                    // Loop through hotels and add the validation
                    foreach ($cmbWeekInMonthArray as $id => $data) {
                        // $this->form_validation->set_rules('hotel[' . $id . '][contact_first_name]', 'First
                        // name', 'required|trim');
                        $this->rules [] = [
                            "field" => "cmbWeekInMonth[" . $id . "]",
                            "label" => $this->lang->line('repeating_every'),
                            "rules" => "trim|required",
                            "errors" => [
                                'required' => $this->lang->line('error_please_select_recurrin_days')
                            ]
                        ];
                    }
                }
                
                /*
                 * $this->rules [] = [ "field" => "cmbWeekInMonth[]", "label" =>
                 * $this->lang->line('repeating_every'), "rules" => "trim|required", "errors" => [ 'required' =>
                 * $this->lang->line('error_please_select_recurrin_days') ] ];
                 */
            }
        }
        
        $this->rules [] = [
            "field" => "txtStart_time",
            "label" => $this->lang->line('start_at'),
            "rules" => "trim|required",
            "errors" => [
                'required' => $this->lang->line('error_please_select_start_time')
            ]
        ];
        $this->rules [] = [
            "field" => "txtEnd_time",
            "label" => $this->lang->line('end_at'),
            "rules" => "trim|required",
            "errors" => [
                'required' => $this->lang->line('error_please_select_end_time')
            ]
        ];
        /*
         * $this->rules [] = [ "field" => "cmbTimezone", "label" => "Timezone", "rules" => "trim|required",
         * "errors" => [ 'required' => $this->lang->line('error_please_select_timezone') ] ];
         */
        
        $this->rules [] = [
            "field" => "rdaReInvite",
            "label" => $this->lang->line('re_invite'),
            "rules" => "trim|required",
            "errors" => [
                'required' => $this->lang->line('error_please_select_re_invite_option')
            ]
        ];
        $this->rules [] = [
            "field" => "rdaUnranked",
            "label" => $this->lang->line('ranked'),
            "rules" => "trim|required",
            "errors" => [
                'required' => $this->lang->line('error_please_select_a_rank')
            ]
        ];
        /*
         * $this->rules [] = [ "field" => "rdaAllowWalkin", "label" => $this->lang->line('allow_walk_in'), "rules"
         * => "trim|required", "errors" => [ 'required' => $this->lang->line('error_please_select_allow_walk_in') ]
         * ];
         */
        $this->rules [] = [
            "field" => "rdaLockStatus",
            "label" => $this->lang->line('lock_event'),
            "rules" => "trim|required",
            "errors" => [
                'required' => $this->lang->line('error_please_select_lock_status')
            ]
        ];
        /*
         * $this->rules [] = [ "field" => "txtLockBefore", "label" => $this->lang->line('cut_off_time'), "rules" =>
         * "trim|required", "errors" => [ 'required' => $this->lang->line('error_please_select_cut_off_time') ] ];
         */
        $this->rules [] = [
            "field" => "cmbPerformPickTime",
            "label" => $this->lang->line('performer_can_pick_time'),
            "rules" => "trim|required",
            "errors" => [
                'required' => $this->lang->line('error_please_select_performer_can_pick_time')
            ]
        ];
        if ($this->input->post('cmbPerformPickTime') == 0) {
            $this->rules [] = [
                "field" => "rdaIdAlgo",
                "label" => $this->lang->line('schedule_algorithm'),
                "rules" => "trim|required",
                "errors" => [
                    'required' => $this->lang->line('error_please_select_schedule_algorithm')
                ]
            ];
        }
        $this->rules [] = [
            "field" => "rdaAllowHost",
            "label" => $this->lang->line('allow_host'),
            "rules" => "trim|required",
            "errors" => [
                'required' => $this->lang->line('error_please_select_allow_host')
            ]
        ];
        $this->rules [] = [
            "field" => "rdaAllowTrading",
            "label" => $this->lang->line('allow_trading'),
            "rules" => "trim|required",
            "errors" => [
                'required' => $this->lang->line('error_please_select_allow_trading')
            ]
        ];
        
        $this->rules [] = [
            "field" => "txtTimePerPerformance",
            "label" => $this->lang->line('time_per_performance'),
            "rules" => "trim|required",
            "errors" => [
                'required' => $this->lang->line('error_please_select_time_per_performance')
            ]
        ];
        
        $this->rules [] = [
            "field" => "rdaCustomTimeAllowed",
            "label" => $this->lang->line('custom_time_allowed'),
            "rules" => "trim|required",
            "errors" => [
                'required' => $this->lang->line('error_please_select_custome_time_allowed')
            ]
        ];
        
        $this->rules [] = [
            "field" => "rdaAutoAccept",
            "label" => $this->lang->line('accept_all_performer_requests'),
            "rules" => "trim|required",
            "errors" => [
                'required' => $this->lang->line('error_please_select_accept_all_performer_request')
            ]
        ];
        
        $this->rules [] = [
            "field" => "rdaAutoReminder",
            "label" => $this->lang->line('automatic_reminder_e_mails'),
            "rules" => "trim|required",
            "errors" => [
                'required' => $this->lang->line('error_please_select_automatic_reminder_e_mails')
            ]
        ];
    }

    /**
     * add()
     * This method show edit add event form
     */
    public function add()
    {
        /* no one user can access this method apart from a venue */
        if ($this->pidGroup != VENUE_GROUP) {
            redirect($this->session->userdata('UserData')->dashboardURL);
        }
        if ($this->input->post()) {
            
            $this->setRules();
            $validation = $this->validateData($this->rules);
            
            if ($validation) {
                // insert event
                $status = $this->this_model->insert_event();
                if ($status) {
                    
                    $this->utility->setFlashMessage('success', $this->lang->line('event_created_successfully'));
                    redirect(SITEURL . $this->myInfo ['controller']);
                }
                else {
                    $this->utility->setFlashMessage('success', $this->lang->line('error_default_error'));
                }
            }
        }
        
        /* load location model to get current user's location */
        $this->load->model("location_model");
        /* load profile model to get current user's style */
        $data ['style'] = $this->input->post("cmbStyle");
        $this->load->model("profile_model");
        $data ["locations"] = $this->location_model->getCurrentUserLocations($this->pUserId);
        $data ['styles'] = $this->profile_model->getUserStyleForOverview($this->pUserId);
        $data ['timezones'] = $this->this_model->getTimezones();
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["page_heading"] = $this->lang->line('create_new_show');
        $data ['registerUrl'] = SITEURL . $this->myInfo ["controller"] . "/add";
        $data ['planInfo'] = $this->subscription_model->getCurrentPlanDetails($this->pUserId, $this->pidGroup);
        $data ["title"] = "Create New Show/Series";
        $this->myView($this->folder . "/event_create_event_view", $data);
    }

    /**
     * edit()
     * This method edit an event
     *
     * @param integer $id is event id
     * @return return success if event and corresponds table deleted successfully
     */
    public function edit($id = "")
    {
        $id = $this->utility->decode($id);
        $this->editEventId = $id;
        if (! ctype_digit($id)) {
            show_404();
            exit();
        }
        
        // check if event is delted before edit
        $checkEventDelete = $this->this_model->CheckEventDelete($id);
        
        if ($checkEventDelete == 0) {
            $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
            redirect(SITEURL . $this->myInfo ["controller"]);
        }
        
        $data ['style'] = $this->this_model->getStyleByEventId($id);
        if ($this->input->post()) {
            $data ['style'] = $this->input->post("cmbStyle");
            $this->setRules();
            
            $validation = $this->validateData($this->rules);
            // pr(validation_errors());die;
            if ($validation) {
                // update event
                $status = $this->this_model->update_event($id);
                if ($status) {
                    $this->utility->setFlashMessage('success', $this->lang->line('event_updated_successfully'));
                    // redirect(SITEURL . $this->myInfo ['controller'] . "/edit/" . $this->utility->encode($id));
                    redirect(SITEURL . $this->myInfo ['controller']);
                }
                else {
                    $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
                }
            }
        }
        
        $data ['event'] = $this->this_model->getEventById($id);
        
        $id = $this->utility->encode($id);
        /* load location model to get current user's location */
        $this->load->model("location_model");
        /* load profile model to get current user's style */
        $this->load->model("profile_model");
        
        $data ["locations"] = $this->location_model->getCurrentUserLocations($this->pUserId);
        $data ['styles'] = $this->profile_model->getUserStyleForOverview($this->pUserId);
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["registerUrl"] = SITEURL . $this->myInfo ["controller"] . '/edit/' . $id;
        $data ['timezones'] = $this->this_model->getTimezones();
        $data ['planInfo'] = $this->subscription_model->getCurrentPlanDetails($this->pUserId, $this->pidGroup);
        $data ["title"] = 'Edit Show/Series';
        $this->myView($this->folder . "/event_create_event_view", $data);
    }

    /**
     * delete()
     * This method will delete singal or multipal event schedules
     *
     * @return return session flash messages
     */
    public function deleteEventSchedule()
    {
        if ($this->input->post()) {
            
            $status = $this->this_model->deleteEventSchedule();
            if ($status) {
                $this->utility->setFlashMessage('success', $this->lang->line('event_schedule_deleted_successfully'));
            }
            else {
                $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
            }
            redirect(SITEURL . $this->myInfo ['controller']);
        }
    }

    /**
     * delete()
     * This method will delete singal or multipal events
     *
     * @return return session flash messages
     */
    public function delete()
    {
        if ($this->input->post()) {
            
            $status = $this->this_model->deleteEvent();
            
            if ($status) {
                $this->utility->setFlashMessage('success', $this->lang->line('event_deleted_successfully'));
            }
            else {
                $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
            }
            
            redirect(SITEURL . $this->myInfo ['controller']);
        }
    }

    /**
     * search()
     * This method is used for seraching the event for performer
     */
    public function search()
    {
        // this is remaining
        /*
         * if (event_ranking_pending_by_performer ()) { $this->session->set_flashdata ( "red_msg", "You must rank
         * past events before continuing!" ); redirect ( "performer/my_events" ); }
         */
        $data ["title"] = "Search Shows";
        $data ["controllerName"] = $this->myInfo ["controller"];
        
        // matching the style for login user(perfomer)
        // $data ["styles"] contain the style id related to venue.
        
        if ($this->pidGroup == "4") {
            $viewName = $this->myvalues->fanDetails ['controller'] . '/fan_search_event_view';
            $data ["styles"] = $this->main_model->selectStyleMatchingWithAllUser($this->pUserId, VENUE_GROUP, 'fan_style');
        }
        else {
            $viewName = $this->folder . '/event_search_event_view';
            $data ["styles"] = $this->main_model->selectStyleMatchingWithAllUser($this->pUserId, VENUE_GROUP, 'performer_style');
        }
        // venue user's style id
        $stayle_array = array();
        
        foreach ($data ["styles"] as $k => $v) {
            array_push($stayle_array, $v->idStyle);
        }
        
        if ($this->input->post()) {
            $data ["eventResult"] = $this->this_model->search_event($this->input->post(), $stayle_array);
        }
        
        // this array contain the vaneu id which perfomer is follow
        $data ['venueFollowArray'] = array();
        
        $resultVenueFollowId = $this->this_model->getFollowingVenue($this->pUserId);
        
        foreach ($resultVenueFollowId as $k => $v) {
            array_push($data ['venueFollowArray'], $v->followingId);
        }
        
        // venue user's style id
        $stayle_array = array();
        
        foreach ($data ["styles"] as $k => $v) {
            array_push($stayle_array, $v->idStyle);
        }
        
        if ($this->input->post()) {
            $data ["eventResult"] = $this->this_model->search_event($this->input->post(), $stayle_array);
        }
        else {
            $data ["eventResult"] = $this->this_model->search_event(array(
                'city' => '',
                'searchName' => '',
                'searchVenueName' => ''
            ), $stayle_array);
        }
        
        $this->myView($viewName, $data);
    }

    /**
     * schedule()
     * This method is check all event shedule for current event
     *
     * @param string $id ,encoded event id
     */
    public function schedule($id = NULL)
    {
        $id = $this->utility->decode($id);
        
        if (ctype_digit($id)) {
            
            // panding_ranking conatin the id of eventschedules which has event ranking is panding
            
            $data ["pending_ranking"] = $this->main_model->get_pending_ranking_event($this->pUserId);
            
            $data ["list"] = $this->this_model->getEventListByEventId($id);
            $data ["title"] = $this->lang->line("manage_schedule2");
            $data ["controllerName"] = $this->myInfo ["controller"];
            $this->myView($this->folder . "/event_schedule_view", $data);
        }
        else {
            
            $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
            redirect(SITEURL . $this->myInfo ['controller']);
        }
    }

    /**
     * overview()
     * this method get all event details and print it in a view
     *
     * @return return load a view
     */
    public function overview($id = "")
    {
        $idEvent = $this->utility->decode($id);
        if (ctype_digit($idEvent)) {
            $this->load->model($this->myvalues->locationDetails ['model'], 'location_model');
            $res = $this->this_model->getEventById($idEvent);
            $data ["user"] = $this->pUserId;
            $data ["record"] = $res;
            $resLocation = $this->location_model->getCurrentUserLocationsById($res->location_id);
            $data ['location'] = $resLocation [0];
            $data ["list"] = $this->this_model->getTimeTableForPublicReview($idEvent);
            
            $data ["title"] = "Show Overview";
            
            $this->myView($this->folder . "/event_overview", $data);
        }
        else {
            
            $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
            redirect(SITEURL . $this->myvalues->invitationDetails ['controller'] . '/sent');
        }
    }

    /**
     * Used for apply the event by perfomer afetr searching
     *
     * @param string $idEventScheduleId , increpted event Schedule id
     *       
     */
    public function apply($idEventScheduleId = "")
    {
        $eventScheduleId = $this->utility->decode($idEventScheduleId);
        
        if (! ctype_digit($eventScheduleId)) {
            show_404();
            exit();
        }
        
        // this function used for check if event cutoff time passed & event date passed
        $checkEventPassed = $this->main_model->CheckEventPassed($eventScheduleId, true);
        
        $checkEventSchduleDelete = $this->this_model->CheckEventSchduleDelete($eventScheduleId);
        
        if ($checkEventSchduleDelete == 0) {
            $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
            redirect(SITEURL . $this->myInfo ["controller"] . "/search");
        }
        
        $r = $this->this_model->performer_apply_event($eventScheduleId, $this->pUserId);
        if ($r [0]) {
            $this->utility->setFlashMessage('success', $r [1]);
            if (strpos($r [1], "Application Submitted, Waiting for Venue Approval") === 0) {
                redirect(SITEURL . $this->myvalues->invitationDetails ["controller"] . "/sent");
            }
            else {
                redirect(SITEURL . $this->myvalues->timeTableDetails ["controller"] . '?eventId=' . $this->utility->encode($eventScheduleId));
            }
        }
        else {
            $this->utility->setFlashMessage('danger', $r [1]);
            redirect(SITEURL . $this->myInfo ["controller"] . "/search");
        }
    }

    /**
     * This function checks its limit for creating / editing event based on its plan for Event Type (Open MIC or
     * Showcase)
     * This method will be called from Validation checking time
     *
     * @param unknown $type type passed in POST Data
     */
    public function checkVenueLimit($type)
    {
        /* When Add Method Called */
        if ($this->router->method == "add") {
            $counts = $this->this_model->getCountsByEventType($this->pUserId, $type, date("m"));
        }
        else {
            $counts = $this->this_model->getCountsByEventType($this->pUserId, $type, date("m"), $this->editEventId);
        }
        
        $planInfo = $this->subscription_model->getCurrentPlanDetails($this->pUserId, $this->pidGroup);
        
        $allowedCount = $planInfo [0]->openmic;
        
        if ($type == "Showcase") {
            $allowedCount = $planInfo [0]->showcase;
        }
        
        if ($planInfo [0]->performers >= 0 && $this->input->post("rdaAutoAccept") == "1") {
            $this->utility->setFlashMessage("danger", $this->lang->line("error_delofault_error"));
            redirect(SITEURL . $this->myInfo ["controller"]);
        }
        
        /* Returs true if has unlimited access */
        if ($allowedCount < 0) {
            return true;
        }
        elseif ($allowedCount == "0") { /* Returs error if not has access */
            $this->utility->setFlashMessage("danger", "You are not allowed to create $type events. Please upgrade your plan. <a href='" . SITEURL . $this->myvalues->subscriptionDetails ["controller"] . "'>Click Here</a>");
        }
        elseif ($counts >= $allowedCount) { /* Returs error if used all limits */
            $this->utility->setFlashMessage("danger", "You have reached your maximum limit for $type events. Please upgrade your plan. <a href='" . SITEURL . $this->myvalues->subscriptionDetails ["controller"] . "'>Click Here</a>");
        }
        else {
            return true;
        }
        redirect(SITEURL . $this->myInfo ["controller"]);
    }

    /**
     * upcoming()
     * This method called when a fan will logged in is will be a landing page of fan user group id 4
     *
     * @return return view
     */
    public function upcoming()
    {
        $data ["controllerName"] = $this->myInfo ["controller"];
        $data ["title"] = 'Upcoming Shows';
        /* checking if cookie has any city then take first this city in model */
        if (! empty($this->input->cookie('user_city'))) {
            $cookieData = $this->input->cookie('user_city', true);
            $getfullCookie = json_decode($cookieData);
            // $city = $getfullCookie->city;
            $fullAddress = $getfullCookie->fullAddress;
            $fullAddress = explode(',', $fullAddress);
            $city = $getfullCookie->city;
            $state = $getfullCookie->state;
            $country = $getfullCookie->country;
        }
        else {
            $this->load->model($this->myvalues->profileDetails ['model'], 'profile_model');
            /* if above cookie city is empty then get user default city form member table */
            $getUserCity = $this->profile_model->getUserInformation($this->pUserId);
            $city = $getUserCity->mem_city;
            $state = $getUserCity->mem_state;
            $country = $getUserCity->mem_country;
        }
        /* check if any date get in post then get result according to date */
        if ($this->input->post('txtDate')) {
            
            /* convert date m-d-Y to Y-m-d format */
            $start_date = DateTime::createFromFormat('m-d-Y', $this->input->post('txtDate'));
            $start_date = $start_date->format('Y-m-d');
            
            $dt = $start_date . ' ' . $this->input->post('hdnTime');
            $date = date('Y-m-d H:i:s', strtotime($dt));
            $date = convertToGMT($date, "Y-m-d");
        }
        else {
            $date = date('Y-m-d', strtotime($this->my_model->datetime));
        }
        //
        $resultVenueFollowId = $this->this_model->getFollowingVenue($this->pUserId);
        
        $data ['venueFollowArray'] = array();
        
        foreach ($resultVenueFollowId as $k => $v) {
            array_push($data ['venueFollowArray'], $v->followingId);
        }
        //
        
        $data ["list"] = $this->this_model->getEventsByFanLocation($city,$state,$country,$date);
        
        $this->myView($this->folder . "/profile_upcomming_show_view", $data);
    }
}
