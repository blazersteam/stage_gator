<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rank extends General_controller
{
    public $myInfo = "";
    public $myInfoRoutes = "";

    public function __construct()
    {
        parent::__construct();
        
        $this->myInfo = $this->myvalues->rankDetails;
        $this->folder = $this->myInfo ["controller"];
        $this->load->model($this->myInfo ["model"], "this_model");
    }

    /**
     * Used for do the all ranking and attendance functionality.
     *
     * @param string $idEventScheduleId, event scheduleid
     */
    public function performerList($idEventScheduleId = "")
    {
        $idEventScheduleId = $this->utility->decode($idEventScheduleId);
        
        if (ctype_digit($idEventScheduleId)) {
            $this->load->model($this->myvalues->performerDetails ['model'], 'perfomer_model');
            
            // $data ["event"] = $this->perfomer_model->getEventById($idEvent);
            $data ["event"] = $this->perfomer_model->getEventBySchuduleId($idEventScheduleId);
            
            $data ["list"] = $this->this_model->getAcceptedUsers($idEventScheduleId);
            
            // used for check if user have right for host functionality
            $data ["permission"] = $this->this_model->checkAllPermissionForLoginUser($this->pUserId, $idEventScheduleId);
            
            $data ["title"] = "Performer List ";
            $data ["controllerName"] = $this->myInfo ["controller"];
            $explain = ' <small data-original-title="You may rank and take attendance after the event is finished." data-placement="top" class="tooltips"><img src="' . base_url() . 'assets/images/comment.png" class="comment_box"/></small>';
            $data ["page_heading"] = ucwords($data ["event"]->title) . " Performers" . $explain;
            $this->myView($this->folder . "/rank_performer_list_view", $data);
        }
        else {
            
            $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
            redirect(SITEURL . $this->myInfo ['controller']);
        }
    }

    /**
     * rankPerformer()
     * This method rank perfomer
     */
    public function rankPerformer()
    {
        $result = $this->this_model->rank_performer($this->pUserId);
       
        if($result === 'exist'){
            
            $this->utility->setFlashMessage('danger', 'This performer is already ranked');
            redirect(SITEURL.$this->myInfo ["controller"] . '/performerList/' . $this->utility->encode($this->input->post('idEvent')));
        }
        if ($result) {
            $this->utility->setFlashMessage('success ', $this->lang->line('record_updated_successfully'));
        }
        else {
            $this->utility->setFlashMessage('danger', $this->lang->line('error_record_not_updated'));
        }
        if (! $this->input->is_ajax_request()) {
            redirect(SITEURL.$this->myInfo ["controller"] . '/performerList/' . $this->utility->encode($this->input->post('idEvent')));
        }
    }
}
