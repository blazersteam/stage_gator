<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends Guest_controller
{
    public $myInfo = "";
    public $myInfoRoutes = "";

    public function __construct()
    {
        parent::__construct();
        
        if($this->input->get('source') == 'iOS'){
            $showSideNavMenu = 0;
            $cookie = array(
                'name'   => 'source',
                'value'  => 'iOS',
                'expire' => time() + 60 * 60 * 24 * 365
            );
            $this->input->set_cookie($cookie);
        }

        if ($this->session->userdata("UserData")) {
            redirect($this->session->userdata("UserData")->dashboardURL, "location");
        }
        $this->myInfo = $this->myvalues->homeDetails;
        $this->folder = $this->myInfo ["controller"];
        $this->load->model($this->myInfo ["model"], "this_model");

    }

    /**
     * index()
     * This method fetch related details of index page like
     */
    public function index()
    {
        $ip = $this->common->getRealIp();
        // get location by ip 
        $location = @file_get_contents('http://freegeoip.net/json/' . $ip);
        $locationArray = json_decode($location);
        
        $data ["city"] = ! empty($locationArray->city) ? $locationArray->city : '';
        $data ["state"] = ! empty($locationArray->region_code) ? $locationArray->region_code : '';
        $data ["country"] = ! empty($locationArray->country_code) ? $locationArray->country_code : '';
        
        $location = array_filter($data);
        // if location not getting by ip then send default chicago city
        if (empty($location)) {
            
            $location ["city"] = "Chicago";
            $location ["state"] = "IL";
            $location ["country"] = "US";
        }
        
        $data ['performers'] = $this->this_model->getPerformers();
        $eventArray = $this->this_model->getEventByLocations($location);
        $data ['events'][0] = [];
        $data ['events'][1] = [];
        if(!empty($eventArray))
        {
            $data ['events'] = array_chunk($eventArray, round(count($eventArray) / 2));
        }
        $this->load->model($this->myvalues->profileDetails ['model'], 'profile_model');
        $data ['title'] = "Welcome To StageGator";
        $data ['styles'] = $this->profile_model->getAllStyles(PERFOMER_GROUP);
        $this->load->view($this->folder . '/home_index_view', $data);
    }
}
