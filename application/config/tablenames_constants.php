<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * This file is for Keeping Tablenames in single place and can be used as CONSTANT
 */

/* Performer Related Tables */

define("TABLE_USER", "user");
define("TABLE_MEMBERS", "members");
define("TABLE_USER_STYLE", "user_style");
define("TABLE_RESET_PASSWORD", "reset_password");
define("TABLE_STYLE", "style");
define("TABLE_COUNTRIES", "countries");
define("TABLE_STATES", "states");
define("TABLE_LOCALDUNIA_TBL_CITY", "localdunia_tbl_city");
define("TABLE_LOCATIONS", "locations");
define("TABLE_EVENT", "event");
define("TABLE_EVENT_STYLE", "event_style");
define("TABLE_TIMETABLE", "timetable");
define("TABLE_FAVORITES", "favorites");
define("TABLE_MESSAGES", "messages");
define("TABLE_REGISTER", "register");
define("TABLE_TRADING", "trading");
define("TABLE_EVENT_SCHEDULES", "event_schedules");
define("TABLE_TIMEZONES", "timezones");
define("TABLE_NOTIFICATIONS_NEW", "notifications_new");
define("TABLE_SCHEDULE_ALGO", "schedule_algo");
define("TABLE_RATING", "rating");
define("TABLE_USER_PLAN", "user_plan");
define("TABLE_PLANS", "plans");
define("TABLE_PAYMENT_HISTORY", "payment_history");
define("TABLE_PAYMENT", "payments");
define("TABLE_STRIPE_ACCOUNT_DETAILS", "stripe_account_details");
define("TABLE_TIPPING_HISTORY", "tipping_history");
define("TABLE_WITHDRAWAL_HISTORY", "withdrawal_history");
define("TABLE_RECURRING_TIP_INFO", "recurring_tip_info");
define("TABLE_FAN_PERFORMER_STRIP_INFO", "fan_performer_stripe_info");

