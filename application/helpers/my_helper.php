<?php
defined('BASEPATH') or exit('No direct script access allowed');

function dFyDate($dateTime)
{
    $dateTime = convertFromGMT($dateTime);
    $line = substr($dateTime, 0, 10) == "0000-00-00" ? "" : date("d F, Y", strtotime($dateTime));
    return $line;
}

function dFyhiA($dateTime)
{
    $dateTime = convertFromGMT($dateTime);
    $line = substr($dateTime, 0, 10) == "0000-00-00" ? "" : date("d F, Y H:i", strtotime($dateTime));
    return $line;
}

function dmyhiA($dateTime)
{
    $dateTime = convertFromGMT($dateTime);
    $line = substr($dateTime, 0, 10) == "0000-00-00" ? "" : date("d/m/Y H:i:s", strtotime($dateTime));
    return $line;
}

function dateDisplay($dateTime, $format = "m/d/Y")
{
    $dateTime = convertFromGMT($dateTime);
    $line = substr($dateTime, 0, 10) == "0000-00-00" ? "" : date($format, strtotime($dateTime));
    return $format == "h:i A" ? str_replace([":00"],[""],$line) : $line;
}

function dd($data)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

function pr($data)
{
    dd($data);
}

function showBalance($balance)
{
    return number_format($balance, 2, ".", "");
}

function passwordGenerator($length = 8)
{
    $randomString = substr(str_shuffle("ABCDEFGHJKMNPQRSTUVWXYZ123456789abcdefghjkmnpqrtuvwxyz"), 0, $length);
    return $randomString;
}

/** This function checks if event is completed or not based on its endadte time 
 * We are not taking any timezone because we have saved it in GMT and Comaring it with GMT
 * 
 * @param $endDate = End Date of Event
 * @param $endTime = End Time of Event
 * 
 * */
function event_end_time_crossed($endDate, $endTime)
{
    $newEndDateTime = date("Y-m-d H:i:s", strtotime($endDate . " " . $endTime));
    if(date("Y-m-d H:i:s") >= date("Y-m-d H:i:s", strtotime($newEndDateTime)))
    {
        return true;
    }     
    return false;
}

/**
 * Used for check the user(perfomer) can apply for the event or not
 *
 * @param string $eventDate , eventschedule date
 * @param string $eventStartTime, event start time
 * @param string $timeZone , time zone from event table
 * @param string $hoursBeforeLock , hoursBeforeLock from event table
 * @param string $allowWalkin , allowWalkin from event table
 *       
 * @return boolean
 */
function event_cut_off_time_passed($eventDate, $eventStartTime, $hoursBeforeLock, $allowWalkin)
{
    if ($allowWalkin == "1") {
        return false;
    }
    
    $s_time = date("Y-m-d H:i:s",strtotime($eventDate . " " . $eventStartTime));
    $c_time = date("Y-m-d H:i:s");
    
    if (date("Y-m-d H:i:s") >= date("Y-m-d H:i:s", strtotime($s_time. "- " . $hoursBeforeLock . " hours"))) {
        return true;
    }
    else {
        return false;
    }
}

/**
 * This function is used to convert time from selected timezone to GMT
 *
 * @param datetime $dateTime - date time given from user
 * @param string $timeZone - timezone of current user either from profile or IP based
 */
function convertToGMT($dateTime,$format = "Y-m-d H:i:s", $timeZone = "auto")
{   
    $offset = TIMEZONEOFFSET;
    if($timeZone != "auto")
    {
        $key = array_search($timeZone, array_column($GLOBALS ["timeZones"], 'Code'));
        $offset = $GLOBALS ["timeZones"] [$key] ["offset"];
    }
    
    $offset =  strpos($offset, "-") === false ? "+" . $offset : $offset;
    return date($format , strtotime($dateTime . " " . $offset . " minutes"));
}

/**
 * This function is used to convert time from selected timezone to GMT
 *
 * @param datetime $dateTime - date time given from user
 * @param string $timeZone - timezone of current user either from profile or IP based
 */
function convertFromGMT($dateTime, $format="Y-m-d H:i:s", $timeZone = "auto")
{
    $format = $format == "default" ? "Y-m-d H:i:s" : $format;
    $offset = TIMEZONEOFFSET;
    if($timeZone != "auto")
    {
        $key = array_search($timeZone, array_column($GLOBALS ["timeZones"], 'Code'));
        $offset = $GLOBALS ["timeZones"] [$key] ["offset"];
    }
    
    $offset = - $offset;
    return date($format, strtotime($dateTime . " " . $offset . " minutes"));
}
