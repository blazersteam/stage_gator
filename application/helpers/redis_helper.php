<?php
if (! defined('BASEPATH'))
    exit('No direct script access allowed');

$redisObj = null;
$redisStatus = true;

/* Default Function will be called to Generate Redis Session if Redis is enabled for use */
if (USE_REDIS) {
    connectRedis(REDIS_HOST, 6379);
}

/* This function will initiate Redis Connection */
function connectRedis($host, $port)
{
    global $redisObj;
    global $redisStatus;
    $redisObj = new Redis();
    $redisStatus = true;
    // Opening a redis connection
    $redisObj->connect($host, $port);
    if (count((array) $redisObj) == 0) {
        $redisStatus = false;
    }
    return $redisObj;
}

/* This function loads respective Database in Redis */
function loadRedisDb($dbIndex)
{
    global $redisObj;
    global $redisStatus;
    if (! USE_REDIS || ! $redisStatus) {
        return false;
    }
    
    $redisObj->select($dbIndex);
    return true;
}

/* This function saves Data to Redis Variable */
function saveRedisVariable($variable, $data)
{
    try {
        global $redisObj;
        global $redisStatus;
        if (! USE_REDIS || ! $redisStatus) {
            return false;
        }
        
        $res = $redisObj->set($variable, json_encode($data));
        return $res;
    }
    catch (Exception $e) {
        keepErrorLogs($e);
    }
}

/* This function checks variable exists or not in Redis Storage */
function checkRedisVariable($variable)
{
    try {
        global $redisObj;
        global $redisStatus;
        if (! USE_REDIS || ! $redisStatus) {
            return false;
        }
        
        $res = $redisObj->exists($variable);
        return $res;
    }
    catch (Exception $e) {
        keepErrorLogs($e);
    }
}

/* This function returns data for given variable */
function getRedisVariable($variable)
{
    try {
        global $redisObj;
        global $redisStatus;
        if (! USE_REDIS || ! $redisStatus) {
            return false;
        }
        
        $res = $redisObj->get($variable);
        if ($res != false) {
            return json_decode($res);
        }
        return $res;
    }
    catch (Exception $e) {
        keepErrorLogs($e);
    }
}

/* Find Matching Keys and Return Provided Top Results */
function getRedisMatchVariables($pattern)
{
    try {
        global $redisObj;
        global $redisStatus;
        if (! USE_REDIS || ! $redisStatus) {
            return false;
        }
        
        $res = $redisObj->keys($pattern . "*");
        $result = [];
        if ($res != false) {
            foreach ($res as $k => $v) {
                $result [] = $v;
            }
        }
        return $result;
    }
    catch (Exception $e) {
        keepErrorLogs($e);
    }
}

/* This function is used to add element to array in Las posotion */
function saveRedisArrayData($variable, $data)
{
    try {
        global $redisObj;
        global $redisStatus;
        
        if (! USE_REDIS || ! $redisStatus) {
            return false;
        }
        
        $res = $redisObj->rPush($variable, json_encode($data));
        return $res;
    }
    catch (Exception $e) {
        keepErrorLogs($e);
    }
}

/* This function is used to add element to array in Las posotion */
function removeRedisArrayData($variable)
{
    try {
        global $redisObj;
        global $redisStatus;
        if (! USE_REDIS || ! $redisStatus) {
            return false;
        }
        
        $res = $redisObj->lPop($variable);
        return $res;
    }
    catch (Exception $e) {
        keepErrorLogs($e);
    }
}

/* This function is used to get data from array with provided limit */
function getRedisArrayData($variable, $limit, $offset = 0)
{
    try {
        global $redisObj;
        global $redisStatus;
        if (! USE_REDIS || ! $redisStatus) {
            return false;
        }
        
        $res = $redisObj->lrange($variable, $offset, $limit - 1);
        foreach ($res as $k => $v) {
            $res [$k] = json_decode($v);
        }
        return $res;
    }
    catch (Exception $e) {
        keepErrorLogs($e);
    }
}

function setExpire($variable, $time = 10)
{
    global $redisObj;
    global $redisStatus;
    if (! USE_REDIS || ! $redisStatus) {
        return false;
    }
    
    $redisObj->expire($variable, $time);
}

/* Keeps log of errors that occures while processing Redis request */
function keepRedisErrorLogs($e)
{
    $msg = "<li>" . $e . "</li>";
    file_put_contents("./RedisError.html", $msg, FILE_APPEND);
}