<?php

use Stripe\Plan;

/* Loading STRIPE LIBRARIES for PHP */
require_once APPPATH . "third_party/stripe-php-4.3.0/init.php";

/* Setting STRIPE'S SECRETE KEY for respective account. */
//\Stripe\Stripe::setApiKey("sk_test_q6cIQLYB0AbNuKUTk3oEC1Uc");
\Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY_TIP);

/**
 * This function returns all supported country in stripe as per url
 * https://stripe.com/docs/connect/required-verification-information
 *
 * All countries listed in this array are having 2 FILES in view folder for INDIVIDUAL , COMPANY account type to
 * create HTML FORM for Registration based on country
 *
 * NOTE : Change in This country must need change in those respective files too.
 */
function getSuppertedCountries()
{
    return [
        'US'
    ];
    return [
        "AT",
        "AU",
        "BE",
        "CA",
        "DE",
        "DK",
        "ES",
        "FI",
        "FR",
        "GB",
        "HK",
        "IE",
        "IT",
        "JP",
        "LU",
        "NL",
        "NO",
        "PT",
        "SE",
        "SG",
        "US"
    ];
}

/**
 * This function used to create account in stripe.
 * for Managed Account purpose for PAYMENT / TRANSFER / WITHDRAWAL PURPOSE
 *
 * NOTE : This is not for creating CUSTOMERS.
 *
 * @param string $country Country code of Supported Country
 * @param string $email Email address of Account Holder (Performer)
 * @param bool $managed (true / false) Want to manage account or not
 * @return array with error message or found Details
 */
function createStripeAccount($country, $email, $managed = true)
{
    
    /* validating that all requried variables are passed */
    if (empty($country) || empty($email)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    /* Validating Email is Valid or Not */
    if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return [
            "status" => false,
            "message" => "Invalid email address."
        ];
    }
    
    /* Validating Country code is supprted by Stripe or Not */
    $supportedCountries = getSuppertedCountries();
    if (! in_array(strtoupper($country), $supportedCountries)) {
        return [
            "status" => false,
            "message" => "Country not supperted."
        ];
    }
    
    /* Validating Manage Value */
    if (! is_bool($managed)) {
        return [
            "status" => false,
            "message" => "Invalid manage status."
        ];
    }
    
    try {
        /* Creating STRIPE Account by calling its function */
        $result = \Stripe\Account::create(array(
            "country" => $country,
            "managed" => $managed,
            "email" => $email
        ));
        $result = json_decode($result->__toJSON(), true);
        return [
            "status" => true,
            "accountId" => $result ["id"],
            "secret_key" => $result ["keys"] ["secret"],
            "publishable_key" => $result ["keys"] ["publishable"]
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function returns all data stored in stripe for that account
 *
 * @param string $accountId account id of connected account holder
 *       
 * @return array with success/fail result.
 */
function getStripeAccountData($accountId)
{
    /* validating that all requried variables are passed */
    if (empty($accountId)) {
        return [
            "status" => false,
            "message" => "Please provide accountId."
        ];
    }
    
    try {
        $account = \Stripe\Account::retrieve($accountId);
        $account = json_decode($account->__toJSON(), true);
        
        $finalResult ["id"] = $account ["id"];
        $finalResult ["business_name"] = $account ["business_name"];
        $finalResult ["country"] = $account ["country"];
        $finalResult ["default_currency"] = $account ["default_currency"];
        $finalResult ["email"] = $account ["email"];
        $finalResult ["legal_entity"] = $account ["legal_entity"];
        $finalResult ["metadata"] = $account ["metadata"];
        $finalResult ["tos_acceptance"] = $account ["tos_acceptance"];
        $finalResult ["transfer_schedule"] = $account ["transfer_schedule"];
        $finalResult ["verification"] = $account ["verification"];
        
        return [
            "status" => true,
            "message" => $finalResult
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

function deleteStripeAccount($accountId)
{
    /* validating that all requried variables are passed */
    if (empty($accountId)) {
        return [
            "status" => false,
            "message" => "Please provide accountId."
        ];
    }
    
    try {
        $account = \Stripe\Account::retrieve($accountId);
        $account->delete();
        return [
            "status" => true,
            "message" => "Connected account has beed deleted"
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function returns all external accounts related with passed account id
 *
 * @param string $accountId Account id of connected account holder
 */
function getStripeExternalAccounts($accountId)
{
    /* validating that all requried variables are passed */
    if (empty($accountId)) {
        return [
            "status" => false,
            "message" => "Please provide accountId."
        ];
    }
    
    try {
        
        /* Fetching Bank Accounts */
        $bankResult = \Stripe\Account::retrieve($accountId)->external_accounts->all(array(
            "object" => "bank_account",
            "limit" => 100
        ));
        $bankResult = json_decode($bankResult->__toJSON(), true);
        
        /* Fetching Card Accounts */
        $cardResult = \Stripe\Account::retrieve($accountId)->external_accounts->all(array(
            "object" => "card",
            "limit" => 100
        ));
        
        $cardResult = json_decode($cardResult->__toJSON(), true);
        
        return [
            "status" => true,
            "message" => [
                "banks" => $bankResult ["data"],
                "cards" => $cardResult ["data"]
            ]
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function adds Bankaccount to Stripe Connected Account.
 *
 * @param string $accountId - Stripe coonected account holder id
 * @param string $bankAccNo - Bank Account no of Account Holder
 * @param string $country - Country of Bank Acccount, where account lies
 * @param string $currency - Curreny accepted by Bank
 * @param string $routingNo - Routing No of Bank (Your routing number identifies the location where your account
 *        was opened.)
 */
function createStripeBankAccount($accountId, $accHolderName, $bankAccNo, $country, $currency, $routingNo = "")
{
    /* validating that all requried variables are passed */
    if (empty($accountId) || empty($accHolderName) || empty($bankAccNo) || empty($country) || empty($currency)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    /* Validating Country code is supprted by Stripe or Not */
    $supportedCountries = getSuppertedCountries();
    if (! in_array(strtoupper($country), $supportedCountries)) {
        return [
            "status" => false,
            "message" => "Country not supperted."
        ];
    }
    
    try {
        $account = \Stripe\Account::retrieve($accountId);
        $response = $account->external_accounts->create(array(
            "external_account" => [
                "object" => "bank_account",
                "account_number" => $bankAccNo,
                "country" => $country,
                "currency" => $currency,
                "routing_number" => $routingNo,
                "account_holder_name" => $accHolderName
            ]
        ));
        return [
            "status" => true,
            "message" => json_decode($response->__toJSON(), true)
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function adds Card to Stripe Connected Account.
 *
 * @param string $accountId - Stripe coonected account holder id
 * @param string $token - Token of Debit Card Generated using StripeJS with following details
 *        Card Number , Exp Month, Exp Year, CVC, Name, Currency
 */
function createStripeCardAccount($accountId, $token)
{
    /* validating that all requried variables are passed */
    if (empty($accountId) || empty($token)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    try {
        $account = \Stripe\Account::retrieve($accountId);
        $response = $account->external_accounts->create(array(
            "external_account" => $token
        ));
        
        return [
            "status" => true,
            "message" => json_decode($response->__toJSON(), true)
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function deletes stored bank acard account of requested connected account holder
 *
 * @param string $accountId - Connect account holder id
 * @param string $externalId - bank , card id of stripe
 * @return multitype:boolean string |multitype:boolean multitype:mixed |multitype:boolean NULL
 */
function deleteStripeExternalAccount($accountId, $externalId)
{
    /* validating that all requried variables are passed */
    if (empty($accountId) || empty($externalId)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    try {
        
        /* Fetching Bank Accounts */
        $account = \Stripe\Account::retrieve($accountId);
        $result = $account->external_accounts->retrieve($externalId)->delete();
        
        $result = json_decode($result->__toJSON(), true);
        
        return [
            "status" => true,
            "message" => "Record deleted successfully."
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function updates stripe account for auto transfer settings.
 *
 * @param string $accountId - account id of connected account holder
 * @param string $interval - any of these values (daily , weekly , monthly , manual)
 * @param int $delayDays - No of days older charges will be transffered to bank
 *       
 * @return multitype:boolean string |multitype:boolean multitype:mixed |multitype:boolean NULL
 */
function changeStripeAutoTransfer($accountId, $interval, $delayDays = 2)
{
    /* validating that all requried variables are passed */
    if (empty($accountId) || empty($interval)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    if (! in_array($interval, [
        "daily",
        "weekly",
        "monthly",
        "manual"
    ])) {
        return [
            "status" => false,
            "message" => "Invalid interval type."
        ];
    }
    
    if (! is_integer($delayDays) && $delayDays !== "minimum") {
        return [
            "status" => false,
            "message" => "Invalid Delay days."
        ];
    }
    try {
        
        /* Fetching Bank Accounts */
        $account = \Stripe\Account::retrieve($accountId);
        if ($interval != "manual") {
            $account->transfer_schedule->delay_days = $delayDays;
        }
        $account->transfer_schedule->interval = $interval;
        $result = $account->save();
        
        $result = json_decode($result->transfer_schedule->__toJSON(), true);
        
        return [
            "status" => true,
            "message" => "Autotransfer settings has been updated successfully."
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function charges customer Credit Card and Add payment directly to passed account holder and platform
 * (Super Admin) will get fee from that charge
 *
 * @param string $accountId - account id of connected account holder
 * @param string $token - Token of Credit Card Generated using StripeJS with basic details (Number, EXP month,
 *        Year, CVC)
 * @param double $amount - amount to be charged
 * @param string $description - a short description to remember this transaction
 * @param string $currency - a valid supporting currency type
 * @param double $applicationFee - application fee as per reqirement. will be deducted from stripe account holder
 *        to master account
 * @param array $metaData = any other custom data you want to save in arary form
 * @param string $statement_descriptor - a short 20 charachter description. will be shown in Clustomer's Bank
 *        Account Statement
 *       
 * @return multitype:boolean string |multitype:boolean mixed |multitype:boolean NULL
 */
function chargeStripeCustomerByToken($accountId, $token, $amount, $description, $currency = "usd", $applicationFee = 0.00, $metaData = null, $statement_descriptor = "")
{
    /* validating that all requried variables are passed */
    if (empty($accountId) || empty($token) || empty($amount) || empty($description)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    if (! is_numeric($applicationFee)) {
        return [
            "status" => false,
            "message" => "Invalid application fee."
        ];
    }
    
    $statement_descriptor = substr($statement_descriptor, 0, 22);
    
    try {
        $res = \Stripe\Charge::create(array(
            "amount" => $amount * 100,
            "currency" => $currency,
            "source" => $token,
            "description" => $description,
            "application_fee" => $applicationFee * 100,
            "destination" => $accountId,
            "metadata" => $metaData,
            "statement_descriptor" => $statement_descriptor
        ));
        
        $res = json_decode($res->__toJSON(), true);
        
        if (! empty($res ["status"]) && $res ["status"] == "succeeded") {
            return [
                "status" => true,
                "chargeId" => $res ["id"],
                "stripeStatus" => "succeeded",
                "message" => $res
            ];
        }
        
        elseif (! empty($res ["status"]) && $res ["status"] == "pending") {
            return [
                "status" => true,
                "chargeId" => $res ["id"],
                "stripeStatus" => "pending",
                "message" => $res
            ];
        }
        else {
            return [
                "status" => true,
                "chargeId" => $res ["id"],
                "stripeStatus" => "failed",
                "stripeError" => $res ["failure_message"],
                "message" => $res
            ];
        }
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function charges saved customer card with his passed card id and Add payment directly to passed account
 * holder
 * and platform
 * (Super Admin) will get fee from that charge
 *
 * @param string $accountId - account id of connected account holder
 * @param string $custoerId - account id of connected account holder
 * @param string $cardId - cardId of saved customers saved CC details ID found in Customer::retrieve function
 * @param double $amount - amount to be charged
 * @param string $description - a short description to remember this transaction
 * @param string $currency - a valid supporting currency type
 * @param double $applicationFee - application fee as per reqirement. will be deducted from stripe account holder
 *        to master account
 * @param array $metaData = any other custom data you want to save in arary form
 * @param string $statement_descriptor - a short 20 charachter description. will be shown in Clustomer's Bank
 *        Account Statement
 *       
 * @return multitype:boolean string |multitype:boolean mixed |multitype:boolean NULL
 */
function chargeStripeCustomerBySavedCard($accountId, $customerId, $cardId, $amount, $description, $applicationFee = 0.00, $currency = "usd", $metaData = null, $statement_descriptor = "")
{
    /* validating that all requried variables are passed */
    if (empty($accountId) || empty($customerId) || empty($cardId) || empty($amount) || empty($description)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    if (! is_numeric($applicationFee)) {
        return [
            "status" => false,
            "message" => "Invalid application fee."
        ];
    }
    
    $statement_descriptor = substr($statement_descriptor, 0, 22);
    
    try {
        $res = \Stripe\Charge::create(array(
            "amount" => $amount * 100,
            "customer" => $customerId,
            "currency" => $currency,
            "source" => $cardId,
            "description" => $description,
            "application_fee" => $applicationFee * 100,
            "destination" => $accountId,
            "metadata" => $metaData,
            "statement_descriptor" => $statement_descriptor
        ));
        $res = json_decode($res->__toJSON(), true);
        if (! empty($res ["status"]) && $res ["status"] == "succeeded") {
            return [
                "status" => true,
                "chargeId" => $res ["id"],
                "stripeStatus" => "succeeded",
                "message" => $res
            ];
        }
        
        elseif (! empty($res ["status"]) && $res ["status"] == "pending") {
            return [
                "status" => true,
                "chargeId" => $res ["id"],
                "stripeStatus" => "pending",
                "message" => $res
            ];
        }
        else {
            return [
                "status" => true,
                "chargeId" => $res ["id"],
                "stripeStatus" => "failed",
                "stripeError" => $res ["failure_message"],
                "message" => $res
            ];
        }
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function registers Customer into master account of Stripe.
 *
 * @param string $email - Email address of customer in your website
 * @param string $description - short description for customer. Like Created from website.com
 * @param array $metaData - some extra data you want to store [Group = Employee, EmployeeCode = EMP123]
 * @param string $token Token of Credit Card Generated using StripeJS with basic details (Number, EXP month,
 *        Year, CVC)
 * @return multitype:boolean string |multitype:boolean mixed |multitype:boolean NULL
 */
function createStripeCustomer($email, $description, $token = null, $metaData = null)
{
    /* validating that all requried variables are passed */
    if (empty($email) || empty($description)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    try {
        
        $createArray = array(
            "email" => $email,
            "description" => $description,
            "metadata" => $metaData
        );
        
        if (! empty($token)) {
            $createArray ["source"] = $token;
        }
        
        $res = \Stripe\Customer::create($createArray);
        $res = json_decode($res->__toJSON(), true);
        return [
            "status" => true,
            "customerId" => $res ["id"]
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function add new credit card in respective customer
 *
 * @param Strnig $customerId - customer id of that customer received from stripe
 * @param String $token - Token generate by that customer from stripe.js
 * @return multitype:boolean string |multitype:boolean mixed |multitype:boolean NULL
 */
function addStripeCustomerCard($customerId, $token)
{
    /* validating that all requried variables are passed */
    if (empty($customerId) || empty($token)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    try {
        
        $customer = \Stripe\Customer::retrieve($customerId);
        $result = $customer->sources->create(array(
            "source" => $token
        ));
        
        return [
            "status" => true,
            "message" => json_decode($result->__toJSON(), true)
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function returns all cards added for that customer.
 *
 * @param string $customerId - customer id of that customer received from stripe
 */
function getStripeCustomerCards($customerId)
{
    /* validating that all requried variables are passed */
    if (empty($customerId)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    try {
        
        $customer = \Stripe\Customer::retrieve($customerId)->sources->all(array(
            "object" => "card",
            "limit" => 100
        ));
        $cards = json_decode($customer->__toJSON(), true);
        
        return [
            "status" => true,
            "message" => $cards ["data"]
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function returns all transfers
 *
 * @param string $accountId - Stripe Account ID of Connected account holder
 * @param number $limit - Limit of records you want to display (From 1 to 100)
 * @param string $offset - Last ID of previous request's record for next group of records
 * @return multitype:boolean string | multitype:boolean array of all transaction details
 */
function getStripeTransfers($accountId, $key, $limit = 10, $offset = false)
{
    \Stripe\Stripe::setApiKey($key);
    /* validating that all requried variables are passed */
    if (empty($accountId)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    if (is_integer($limit) && $limit <= 100 && $limit >= 1) {
        
        $searchArray = [
            "limit" => $limit
        ];
        
        if (! empty($offset)) {
            $searchArray ["starting_after"] = $offset;
        }
        
        /*
         * if (! empty($fromDate)) {
         * $searchArray ["created"] ["gte"] = strtotime(date("Y-m-d 00:00:00", strtotime($fromDate)));
         * }
         *
         * if (! empty($toDate)) {
         * $searchArray ["created"] ["lte"] = strtotime(date("Y-m-d 23:59:59", strtotime($toDate)));
         * }
         */
        
        try {
            $transactions = \Stripe\BalanceTransaction::all($searchArray);
            $transactions = json_decode($transactions->__toJSON(), true);
            return [
                "status" => false,
                "message" => $transactions
            ];
        }
        catch (Exception $ex) {
            return [
                "status" => false,
                "message" => $ex->getMessage()
            ];
        }
    }
    else {
        return [
            "status" => false,
            "message" => "Invalid limit : Should be between (1 - 100)"
        ];
    }
}

function updateStripeAccountData($accountId, $data)
{
    /* validating that all requried variables are passed */
    if (empty($accountId) || empty($data)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    $finalObject = [];
    
    /* Processing each Record found in array */
    foreach ($data as $k => $v) {
        /* Storing Key and Value in variables */
        $key = $k;
        $value = $v;
        
        /* Exploding Key by __ (Double Dash) */
        $key = explode("__", $key);
        
        /* Reversing Array to convert key to Multidimensional array */
        $key = array_reverse($key);
        
        /* Created default null variables for below process */
        $lastArray = [];
        $lastKey = "";
        
        /* Excuting each record found after exploding array key */
        foreach ($key as $k => $v) {
            /*
             * If lastArray is emoty. means first time calling then we will give key and value to that variable so
             * no need to assign key at last ELSE we will simply unset lastKey and move lastArray object to its new
             * key
             */
            if (empty($lastArray)) {
                $lastArray = [
                    $v => $value
                ];
            }
            else {
                $lastArray [$v] = $lastArray;
            }
            if (! empty($lastKey)) {
                unset($lastArray [$lastKey]);
            }
            
            /* Assigning Last key to remember for removing it in next request. not for top key */
            $lastKey = $v;
        }
        
        /* Recursively mergin 2 array so, same key will be merged with new value */
        $finalObject = array_merge_recursive($finalObject, $lastArray);
    }
    
    try {
        /* Updating Stripe Account Object */
        $result = \Stripe\Account::update($accountId, $finalObject);
        
        // $result = json_decode($result->__toJSON(), true);
        
        /* Now account is updated. but may be it contains verification errors so returns true with error message */
        if (! empty($result->verification->fields_needed)) {
            return [
                "status" => true,
                "message" => "Please fill all required fields for verification",
                "fields" => $result->verification->fields_needed
            ];
        }
        
        /* Now account is updated. but may be it contains verification errors so returns true with error message */
        if (! empty($result->verification->disabled_reason)) {
            return [
                "status" => true,
                "message" => "Your verification is remaining due to : " . $result->verification->disabled_reason
            ];
        }
        
        /* Returns Success message */
        return [
            "status" => true,
            "message" => "Account updated successfully."
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

function uploadStripeFile($accountId, $fileName)
{
    /* validating that all requried variables are passed */
    if (empty($accountId)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    try {
        $fp = fopen($fileName, 'r');
        /* Uploading document to Stripe */
        $result = \Stripe\FileUpload::create(array(
            "purpose" => "identity_document",
            "file" => $fp
        ));
        $res = json_decode($result->__toJSON(), true);
        return [
            "status" => false,
            "message" => $res
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function is usde to fetch all balance transaction for particular accounrh older for specific period
 *
 * @param string $key - Stripe secret key of account holder whose tranasctions you want
 * @param number $limit - no of transacionts you want. Max 100
 * @param string $offset - Last ID of previous request's record for next group of records
 * @param string $fromDate - "Y-m-d" format date, for starting period
 * @param string $toDate - "Y-m-d" format date, for ending period
 * @return multitype:boolean string |mixed|multitype:boolean NULL
 */
function getStripeBalanceTransactions($key, $limit = 10, $offset = false, $fromDate = false, $toDate = false)
{
    /* validating that all requried variables are passed */
    if (empty($key)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    try {
        $searchArray = [
            "limit" => $limit
        ];
        
        if (! empty($offset)) {
            $searchArray ["starting_after"] = $offset;
        }
        
        if (! empty($fromDate)) {
            $searchArray ["created"] ["gte"] = strtotime(date("Y-m-d 00:00:00", strtotime($fromDate)));
        }
        
        if (! empty($toDate)) {
            $searchArray ["created"] ["lte"] = strtotime(date("Y-m-d 23:59:59", strtotime($toDate)));
        }
        
        \Stripe\Stripe::setApiKey($key);
        $res = \Stripe\BalanceTransaction::all($searchArray);
        $res = json_decode($res->__toJSON(), true);
        return [
            "status" => true,
            "message" => $res
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

function getStripeBalance($key)
{
    /* validating that all requried variables are passed */
    if (empty($key)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    try {
        \Stripe\Stripe::setApiKey($key);
        $res = \Stripe\Balance::retrieve();
        $res = json_decode($res->__toJSON(), true);
        
        $finalObject = [];
        foreach ($res ["available"] as $k => $v) {
            $finalObject ["available"] [] = [
                "Currecny" => $v ["currency"],
                "Amount" => $v ["amount"]
            ];
        }
        
        foreach ($res ["pending"] as $k => $v) {
            $finalObject ["pending"] [] = [
                "Currecny" => $v ["currency"],
                "Amount" => $v ["amount"]
            ];
        }
        
        return $finalObject;
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function is for requesting withdrawal for Stripe connect account older (Performer) to transfer his received
 * tips to bank account / debit card
 *
 * @param string $key - secret key of account holder
 * @param string $destinatioId - bank / card id of in which want to payment receive
 * @param double $amount - withdrawal amount
 * @param string $description - description of transcation
 * @param string $metaData - any other data want to save in array format
 * @param string $currency - currency of withrwal amount. in which you want to receive
 *       
 * @return multitype:boolean string |multitype:boolean unknown mixed |multitype:boolean NULL
 */
function createStripeTransfer($key, $destinatioId, $amount, $description = "", $metaData = null, $currency = "usd")
{
    /* validating that all requried variables are passed */
    if (empty($key) || empty($destinatioId) || empty($amount)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    try {
        \Stripe\Stripe::setApiKey($key);
        $res = \Stripe\Transfer::create(array(
            "amount" => $amount * 100,
            "currency" => $currency,
            "destination" => $destinatioId,
            "description" => $description,
            "metadata" => $metaData
        ));
        $res = json_decode($res->__toJSON(), true);
        return [
            "status" => true,
            "txnId" => $res ["id"],
            "transfer_status" => $res ["status"],
            "message" => $res
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function verifies validated bank account by entering 2 micro deposites
 *
 * @param string $accountId - Stripe Conect Account ID (Performer's Stripe Account Id)
 * @param string $bankId - Bank id of Bank to be verified
 * @param double $amount1 - deposited amount
 * @param double $amount2 - deposited amount
 *       
 * @return multitype:boolean string |multitype:boolean NULL
 */
function verifyStripeAccountBank($accountId, $bankId, $amount1, $amount2)
{
    /* validating that all requried variables are passed */
    if (empty($accountId) || empty($bankId) || empty($amount1) || empty($amount2)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    try {
        // get the existing bank account
        $account = \Stripe\Account::retrieve($accountId);
        $bank_account = $account->external_accounts->retrieve($bankId);
        
        // verify the account
        $bank_account->verify(array(
            'amounts' => array(
                $amount1 * 100,
                $amount2 * 100
            )
        ));
        return [
            "status" => true,
            "message" => "Request submitted successfully."
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function deletes stored card account of requested customer
 *
 * @param string $customerId - Customer id of fan
 * @param string $cardId - card id of stripe
 * @return multitype:boolean string |multitype:boolean multitype:mixed |multitype:boolean NULL
 */
function deleteStripeSavedCard($customerId, $cardId)
{
    /* validating that all requried variables are passed */
    if (empty($customerId) || empty($cardId)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    try {
        
        $account = \Stripe\Customer::retrieve($customerId);
        $result = $account->sources->retrieve($cardId)->delete();
        
        $result = json_decode($result->__toJSON(), true);
        
        return [
            "status" => true,
            "message" => "Record deleted successfully."
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function is usde to fetch detailed transaction for particular balance tranacstion
 *
 * @param string $transactionId - Stripe balance id of tranasction you want
 * @return multitype:boolean string |mixed|multitype:boolean NULL
 */
function retrieveStripeBalanceTransaction($transactionId)
{
    /* validating that all requried variables are passed */
    if (empty($transactionId)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    try {
        $res = \Stripe\BalanceTransaction::retrieve($transactionId);
        $res = json_decode($res->__toJSON(), true);
        return [
            "status" => true,
            "message" => $res
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function is used to fetch detailed transaction for application fee tranacstion
 *
 * @param string $transactionId - Stripe application fee id of tranasction you want
 * @return multitype:boolean string |mixed|multitype:boolean NULL
 */
function retrieveStripeApplicationFee($transactionId)
{
    /* validating that all requried variables are passed */
    if (empty($transactionId)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    try {
        $res = \Stripe\ApplicationFee::retrieve($transactionId);
        $res = json_decode($res->__toJSON(), true);
        return [
            "status" => true,
            "message" => $res
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function is used to fetch detailed transaction for particular charge tranacstion
 *
 * @param string $transactionId - Stripe charge id of tranasction you want
 * @return multitype:boolean string |mixed|multitype:boolean NULL
 */
function retrieveStripeChargeDetails($transactionId)
{
    /* validating that all requried variables are passed */
    if (empty($transactionId)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    try {
        $res = \Stripe\Charge::retrieve($transactionId);
        $res = json_decode($res->__toJSON(), true);
        return [
            "status" => true,
            "message" => $res
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function to get stored bank / card account of requested connected account holder based on account id &
 * external id
 *
 * @param string $accountId - Connect account holder id
 * @param string $externalId - bank , card id of stripe
 * @return multitype:boolean string |multitype:boolean multitype:mixed |multitype:boolean NULL
 */
function getStripeExternalAccountData($accountId, $externalId)
{
    /* validating that all requried variables are passed */
    if (empty($accountId) || empty($externalId)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    try {
        
        /* Fetching Bank Accounts */
        $account = \Stripe\Account::retrieve($accountId);
        $result = $account->external_accounts->retrieve($externalId);
        
        $result = json_decode($result->__toJSON(), true);
        
        return [
            "status" => true,
            "message" => $result
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function will set the this exernal account id as default based on account id &
 * external id
 *
 * @param string $accountId - Connect account holder id
 * @param string $externalId - bank , card id of stripe
 * @param boolean $makeDefault -pass true to set external account as default.
 *       
 * @return multitype:boolean string |multitype:boolean multitype:mixed |multitype:boolean NULL
 */
function setDefaultStripeExternalAccount($accountId, $externalId, $makeDefault)
{
    /* validating that all requried variables are passed */
    if (empty($accountId) || empty($externalId)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    try {
        
        /* Fetching Bank Accounts */
        $account = \Stripe\Account::retrieve($accountId);
        $result = $account->external_accounts->retrieve($externalId);
        
        $result->default_for_currency = $makeDefault;
        $resultSave = $result->save();
        $resultSave = json_decode($resultSave->__toJSON(), true);
        
        return [
            "status" => true,
            "message" => $resultSave
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}

/**
 * This function registers Customer into Performer account of Stripe.
 *
 * @param string $email - Email address of customer in your website
 * @param string $description - short description for customer. Like Created from website.com
 * @param array $metaData - some extra data you want to store [Group = Employee, EmployeeCode = EMP123]
 * @param string $token Token of Credit Card Generated using StripeJS with basic details (Number, EXP month,
 *        Year, CVC)
 * @param string $stripsecretkey - Secret Key Of Performer
 * @return multitype:boolean string |multitype:boolean mixed |multitype:boolean NULL
 */
function createStripeCustomerForPerformer($email, $description, $token = null, $metaData = null, $stripsecretkey)
{
    /* validating that all requried variables are passed */
    if (empty($email) || empty($description) || empty($stripsecretkey) || empty($token)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    try {
        
        $createArray = array(
            "email" => $email,
            "description" => $description,
            "metadata" => $metaData,
            "source" => $token
        );
        
        
        //change strip account key
        \Stripe\Stripe::setApiKey($stripsecretkey);
        
        //create customer in above account
        $res = \Stripe\Customer::create($createArray);
        $res = json_decode($res->__toJSON(), true);
        return [
            "status" => true,
            "customerId" => $res ["id"]
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}


/**
 * createPlanForConnectedAccount()
 * This Function Create Recurring Plan For This Performer account of Stripe.
 *
 * @param string $stripsecretkey - Secret Key Of Performer
 * @param string $planId - Custome Unique Create Recurring Plan Id For This Performer  
 * @param string $recurringType - Recurring Type Of This Plan  
 * @param float $amount - Amount of Plan
 * @return multitype:boolean string |multitype:boolean mixed |multitype:boolean NULL
 */
function createPlanForConnectedAccount($stripsecretkey, $planId, $recurringType, $amount, $metadata = null){

    //change strip account key
    \Stripe\Stripe::setApiKey($stripsecretkey);
    
    try {
        $plan = \Stripe\Plan::create(array(
            "name" => $planId,
            "id" => $planId,
            "interval" => $recurringType,
            "currency" => "usd",
            "amount" => $amount*100,
            "metadata" => $metadata
        ));
        
        return [
            "status" => true,
            "planId" => $plan['id']
        ];
        
    }catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
    
}

/**
 * subscriptionToPlan()
 * This function Create Subscriptions For Created Plan Of Performer account of Stripe.
 *
 * @param string $stripsecretkey - Secret Key Of Performer
 * @param string $planId - Created Recurring Plan Id For This Performer  
 * @param string $customerId - Created Customer Id For This Performer 
 * @return multitype:boolean string |multitype:boolean mixed |multitype:boolean NULL
 */
function subscriptionToPlan($stripsecretkey, $planId, $customerId, $applicationFee = 0.00, $metadata = null){
    
//     if($applicationFee){
//         $applicationFee = $applicationFee*100;
//     }
    
    //change strip account key
    \Stripe\Stripe::setApiKey($stripsecretkey);
    
    try {
        $subscription= \Stripe\Subscription::create(array(
            "customer" => $customerId,
            "plan" => $planId,
            "application_fee_percent" => $applicationFee,
            "metadata" => $metadata
        ));
        
        return [
            "status" => true,
            "subscriptionId" => $subscription['id']
        ];
        
    }catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
    
}

/**
 * This function check customer exit or not in strip.
 *
 * @param string $customerId - customer id of that customer received from stripe
 */
function getStripeCustomer($customerId, $stripsecretkey = null)
{
    //change strip account key if given in parameter
    if($stripsecretkey){
        \Stripe\Stripe::setApiKey($stripsecretkey);
    }
    
    /* validating that all requried variables are passed */
    if (empty($customerId)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    try {
        
        $customer = \Stripe\Customer::retrieve($customerId);
        $customerArr = json_decode($customer->__toJSON(), true);
        
        if(!empty($customerArr)){
            return [
                "status" => true,
                "message" => $customerArr
            ];
        }else{
            return [
                "status" => false,
                "message" => "Customer not exist."
            ];
        }
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}


/**
 * This function Cancel Stripe Subscription.
 *
 * @param string $subscriptionId - subscription id of subscription palnId from stripe
 */
function cancelStripeSubscription($subscriptionId, $stripsecretkey = null)
{
    //change strip account key if given in parameter
    if($stripsecretkey){
        \Stripe\Stripe::setApiKey($stripsecretkey);
    }
    
    /* validating that all requried variables are passed */
    if (empty($subscriptionId)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    try {
        
        $subscription = \Stripe\Subscription::retrieve($subscriptionId);
        $subscription->cancel(array('at_period_end' => true));
        $subscription = json_decode($subscription->__toJSON(), true);
        
        return [
            "status" => true,
            "message" => $subscription['status']
        ];
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}


/**
 * This function charges customer Credit Card and Add payment directly to passed account holder and platform
 * (Performer) will get fee from that charge
 *
 * @param string $accountId - account id of connected account holder
 * @param string $customerID -  source or customer is required , the source must be the ID of a source belonging to the custome
 * @param double $amount - amount to be charged
 * @param string $description - a short description to remember this transaction
 * @param string $currency - a valid supporting currency type
 * @param double $applicationFee - application fee as per reqirement. will be deducted from stripe account holder
 *        to master account
 * @param array $metaData = any other custom data you want to save in arary form
 * @param string $statement_descriptor - a short 20 charachter description. will be shown in Clustomer's Bank
 *        Account Statement
 *
 * @return multitype:boolean string |multitype:boolean mixed |multitype:boolean NULL
 */
function chargeStripeCustomerByCustomerID($accountId, $stripsecretkey, $customerID, $amount, $description, $currency = "usd", $applicationFee = 0.00, $metaData = null, $statement_descriptor = "")
{
    
    /* validating that all requried variables are passed */
    if (empty($accountId) || empty($stripsecretkey) || empty($customerID) || empty($amount) || empty($description)) {
        return [
            "status" => false,
            "message" => "Please provide all required fields."
        ];
    }
    
    //change strip account key if given in parameter
    //if($stripsecretkey){
    //    \Stripe\Stripe::setApiKey($stripsecretkey);
    //}
    
    if (! is_numeric($applicationFee)) {
        return [
            "status" => false,
            "message" => "Invalid application fee."
        ];
    }
    
    $statement_descriptor = substr($statement_descriptor, 0, 22);
    
    try {
        $res = \Stripe\Charge::create(array(
            "amount" => $amount * 100,
            "currency" => $currency,
            "customer" => $customerID, // obtained with Stripe.js
            "description" => $description,
            "application_fee" => $applicationFee * 100,
            "metadata" => $metaData,
            "statement_descriptor" => $statement_descriptor
        ),array("stripe_account" => $accountId));
        
        $res = json_decode($res->__toJSON(), true);
        
        if (! empty($res ["status"]) && $res ["status"] == "succeeded") {
            return [
                "status" => true,
                "chargeId" => $res ["id"],
                "stripeStatus" => "succeeded",
                "message" => $res
            ];
        }
        
        elseif (! empty($res ["status"]) && $res ["status"] == "pending") {
            return [
                "status" => true,
                "chargeId" => $res ["id"],
                "stripeStatus" => "pending",
                "message" => $res
            ];
        }
        else {
            return [
                "status" => true,
                "chargeId" => $res ["id"],
                "stripeStatus" => "failed",
                "stripeError" => $res ["failure_message"],
                "message" => $res
            ];
        }
    }
    catch (Exception $ex) {
        return [
            "status" => false,
            "message" => $ex->getMessage()
        ];
    }
}


/* Array
(
    [status] => 1
    [accountId] => acct_19RiwEKiytZeMqj8
    [secret_key] => sk_test_HfklZya7PGX1vc9JFKLqB8c1
    [publishable_key] => pk_test_S6ztBuv6mRpYZXCNOLwyJWKr
)
 */
    

/* [id] => file_19SO8AHicrFNx8WIi7pgdwhL
[object] => file_upload
[created] => 1482142350
[purpose] => identity_document
[size] => 4411
[type] => jpg
[url] => */