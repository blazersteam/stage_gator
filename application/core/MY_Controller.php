<?php
(defined('BASEPATH')) or exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public $currentMenuId = 0;

    function __construct()
    {
        parent::__construct();
        require_once APPPATH . 'config/tablenames_constants.php';
        require_once APPPATH . 'config/default_constants.php';
        $this->fetchTimezones();
        $this->processTimeZone();
    }

    public function myView($viewName, $data = [])
    {
        
        if(isset($this->pidGroup) && $this->pidGroup == PERFOMER_GROUP){
            $data['tipDetails'] = $this->main_model->getTipBalancePerformer($this->pUserId);
        }
        
        /* Loads HTML Tags and DOCKTYPE */
        $this->load->view("templates/html_open.php", $data);
        
        /* Loads HeaderInfo Like Title,MEta Tags and Scripts */
        /* Also Loads Javascript That Require In Header Section */
        $data ["view"] = $viewName;
        
        
        $data ["type"] = $this->session->userdata("userType") ?  : "Guest";
        $data ["title"] = isset($data ["title"]) ? $data ["title"] : (isset($this->title) ? $this->title . " : " . SITE_TITLE : SITE_TITLE);
        $this->load->view("templates/header.php", $data);
        
        /* Loads Menu For Admin or Other Users */
        /*if (! isset($data ["showMenu"]) || $data ["showMenu"] === true) {
            //$this->load->view("templates/menu.php", $data);
        }*/
      
        
        /* Loads Main Content */
        $this->load->view($viewName, $data);
        
        /* Loads footer For Admin or Other Users */
        $this->load->view("templates/footer.php", $data);
        
        /* Loads Javascript, CSS Required At Bottom Of Page */
        $this->load->view("templates/bottom_files.php", $data);
        
        /* Loads HTML Tags and DOCKTYPE */
        $this->load->view("templates/html_close.php", $data);
    }

    public function myViewAjax($viewName, $data = [], $string = false)
    {
        /* Loads Main Content */
        if ($string) {
            return $this->load->view($viewName, $data, $string);
        }
        else {
            $this->load->view($viewName, $data);
        }
    }

    public function validateData($rules, $data = [])
    {
        $config ['error_prefix'] = '<div class="error_prefix">';
        $config ['error_suffix'] = '</div>';
        $this->load->library('form_validation', $config);
        $this->form_validation->set_rules($rules);
        $this->form_validation->set_error_delimiters('<span class="help-inline text-danger">', '</span>');
        
        if (count($data) > 0)
            $this->form_validation->set_data($data);
        return ($this->form_validation->run());
    }

    public function get_validation_errors()
    {
        $error = validation_errors();
        if ($error === "")
            $error = '<div class="error_prefix">' . $this->lang->line('error_default_form_submission') . '</div>';
        return $error;
    }

    public function checkFunctionAvailability($onlyAjaxFunctions, $notAjaxFunctions = ["index"])
    {
        $method = $this->router->fetch_method();
        if (! $this->input->is_ajax_request() && in_array($method, $onlyAjaxFunctions)) {
            // die(json_encode(["status" => "danger","message" => "You are not allowed to perform this task."]));
            show_error($this->lang->line("forbidden_function"), "403", "Forbidden");
            die();
        }
        if ($this->input->is_ajax_request() && in_array($method, $notAjaxFunctions)) {
            die(json_encode([
                "status" => "danger",
                "message" => "You are not allowed to perform this task."
            ]));
            // show_error($this->lang->line("forbidden_function"), "403", "Forbidden");
            // die();
        }
    }

    public function my_jquery_pagination($url, $total, $limit, $div = "#my-content", $uriSegment = 4, $additionalParam = "load_ajax_table()")
    {
        $this->load->library("Jquery_pagination");
        $config ['base_url'] = $url;
        $config ['total_rows'] = $total;
        $config ['per_page'] = $limit;
        $config ['uri_segment'] = $uriSegment;
        $config ['num_links'] = 2;
        $config ['div'] = $div;
        $config ['additional_param'] = $additionalParam;
        $this->jquery_pagination->initialize($config);
        return $this->jquery_pagination->create_links();
    }

    /**
     * This function loads all timezones in from file or from DB query and generate file.
     */
    public function fetchTimezones()
    {
        include_once APPPATH . "/list_timezones.php";
        if (empty($timeZones)) {
            $data ["select"] = [
                "code",
                "name"
            ];
            $data ["table"] = TABLE_TIMEZONES;
            $result = $this->my_model->selectRecords($data);
            
            foreach ($result as $k => $v) {
                $name = explode(")", $v->name);
                $offset = ltrim($name [0], "(GMT");
                
                $offsetArray = str_split($offset);
                $minutes = (int) ($offsetArray [1] . $offsetArray [2]) * 60 + (int) ($offsetArray [3] . $offsetArray [4]);
                $minutes =  -($offsetArray [0] . $minutes);
                $timeZones [] = [
                    'Code' => $v->code,
                    'name' => $v->name,
                    'offset' => $minutes
                ];
            }
            @file_put_contents(APPPATH . "/list_timezones.php", '<?php $timeZones = ' . var_export($timeZones, true) . ";");
            $this->fetchTimezones();
        }
        $GLOBALS ["timeZones"] = $timeZones;
    }

    public function processTimeZone()
    {
        if (!empty($_COOKIE ["ci-timezone"])) {
            /* Fetching main key for fetching offset of that record */
            $key = array_search($_COOKIE ["ci-timezone"], array_column($GLOBALS ["timeZones"], "offset"));
            $offset = $GLOBALS ["timeZones"] [$key];
            define('TIMEZONE', $offset ['Code']);
            define('TIMEZONEOFFSET', $offset ['offset']);
        }
        else {
            define('TIMEZONE', 'America/Chicago');
            define('TIMEZONEOFFSET', "300");
        }
    }
}
