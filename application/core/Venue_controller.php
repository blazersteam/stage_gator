<?php
(defined('BASEPATH')) or exit('No direct script access allowed');

class Venue_controller extends My_Controller
{
    public $pUserId = '';
    public $pidGroup = '';

    function __construct()
    {
        
        parent::__construct();
        
        if ($this->session->userdata("UserData")) {
            
            $this->pUserId = $this->session->userdata("UserData")->user_id;
            $this->pidGroup = $this->session->userdata("UserData")->idGrp;
            if ($this->session->userdata("UserData")->iscompleted == 0) {
                if ($this->router->class !== $this->myvalues->profileDetails ["controller"] || $this->router->method !== "edit") {
                    redirect(SITEURL . $this->myvalues->profileDetails ["controller"] . "/edit", "location");
                }
            }
            
        }
        else {
            
            if ($this->input->is_ajax_request()) {
                set_status_header("401");
                exit();
            }
            redirect(SITEURL, "location");
        }
    }
}