<?php
(defined('BASEPATH')) or exit('No direct script access allowed');

class Admin_controller extends My_Controller
{
    public $pUserId = '';
    public $pidGroup = '';

    function __construct()
    {
        parent::__construct();
        
        if ($this->session->userdata("UserData") && $this->session->userdata("UserData")->idGrp == ADMIN_GROUP) {
            
            $this->pUserId = $this->session->userdata("UserData")->user_id;
            $this->pidGroup = $this->session->userdata("UserData")->idGrp;
           
        }
        else {
            
            if ($this->input->is_ajax_request()) {
                set_status_header("401");
                exit();
            }
            
            redirect(SITEURL, "location");
        }
    }
}