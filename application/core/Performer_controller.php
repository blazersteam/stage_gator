<?php
(defined('BASEPATH')) or exit('No direct script access allowed');

class Performer_controller extends My_Controller
{
    public $pUserId = '';
    public $pUserType = '';

    function __construct()
    {
        parent::__construct();
        
        if ($this->session->userdata("isLoggedIn") && $this->session->userdata("isLoggedIn") === true) {
            $this->pUserId = $this->session->userdata("userId");
            $this->pUserType = $this->session->userdata("userType");
            
            if ($this->session->userdata("UserData")->iscompleted == 0) {
                if ($this->router->class !== $this->myvalues->profileDetails ["controller"] || $this->router->method !== "edit") {
                    redirect(SITEURL . $this->myvalues->profileDetails ["controller"] . "/edit", "location");
                }
            }
        }
        else {
            if ($this->input->is_ajax_request()) {
                set_status_header("401");
                exit();
            }
            redirect(SITEURL, "location");
        }
    }
}