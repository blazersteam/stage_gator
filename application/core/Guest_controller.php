<?php
(defined('BASEPATH')) or exit('No direct script access allowed');

class Guest_controller extends My_Controller
{
    public $pUserType = "";
    public $pUserId ="";
    
    function __construct()
    {
        parent::__construct();
        
        $this->pUserId = 0;
        $this->pUserType = "Guest";
        $this->pidGroup = 0;
        
        if ($this->session->userdata("UserData")) {            
            $this->pUserId = $this->session->userdata("UserData")->user_id;
            $this->pidGroup = $this->session->userdata("UserData")->idGrp;
        }
    }
}