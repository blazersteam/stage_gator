<?php
(defined('BASEPATH')) or exit('No direct script access allowed');

class General_controller extends My_Controller
{
    public $pUserId = '';
    public $pidGroup = '';

    function __construct()
    {
        parent::__construct();
        
        if ($this->session->userdata("UserData")) {
            
            $this->pUserId = $this->session->userdata("UserData")->user_id;
            $this->pidGroup = $this->session->userdata("UserData")->idGrp;
            if ($this->session->userdata("UserData")->iscompleted == 0) {
                
                if($this->pidGroup == FAN_GROUP) {
                    if(($this->router->class == $this->myvalues->eventDetails ["controller"] && $this->router->method == "upcoming") || (($this->router->class == $this->myvalues->profileDetails ["controller"] && $this->router->method == "setCitycookie")) || (($this->router->class == $this->myvalues->tippingDetails ["controller"] && ($this->router->method == "makePaymentTip" ||$this->router->method == "checkStripeIdUser")) )|| (($this->router->class == $this->myvalues->biteDetails ["controller"] && $this->router->method == "biteUnbite"))) {
                        
                    } else {
                        if ($this->router->class !== $this->myvalues->profileDetails ["controller"] || ($this->router->method !== "edit" && $this->router->method !== "checkDuplicateProfileUrl")) {
                            redirect(SITEURL . $this->myvalues->profileDetails ["controller"] . "/edit", "location");
                        }    
                    }
                }
                elseif ($this->router->class !== $this->myvalues->profileDetails ["controller"] || ($this->router->method !== "edit" && $this->router->method !== "checkDuplicateProfileUrl")) {
                    redirect(SITEURL . $this->myvalues->profileDetails ["controller"] . "/edit", "location");
                }
            }elseif ($this->session->userdata("UserData")->tipRegistration == 0 && ($this->router->class !== $this->myvalues->profileDetails ["controller"] && $this->router->method !== "edit" || $this->router->class === $this->myvalues->profileDetails ["controller"] && $this->router->method !== "edit")) {
                
                if ($this->router->class !== $this->myvalues->tippingDetails["controller"]) {
                    redirect(SITEURL . $this->myvalues->tippingDetails["controller"], "location");
                }
            }
        }
        else {
            
            if($this->router->method !== "stripenotify")
            {
                if ($this->input->is_ajax_request()) {
                    set_status_header("401");
                    exit();
                }
                redirect(SITEURL, "location");
            }
        }
    }
}
