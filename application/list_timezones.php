<?php $timeZones = array (
  0 => 
  array (
    'Code' => 'Pacific/Pago_Pago',
    'name' => '(GMT-1100) American Samoa Time',
    'offset' => 660,
  ),
  1 => 
  array (
    'Code' => 'Pacific/Niue',
    'name' => '(GMT-1100) Niue Time',
    'offset' => 660,
  ),
  2 => 
  array (
    'Code' => 'Pacific/Midway',
    'name' => '(GMT-1100) U.S. Outlying Islands (Midway) Time',
    'offset' => 660,
  ),
  3 => 
  array (
    'Code' => 'Pacific/Rarotonga',
    'name' => '(GMT-1000) Cook Islands Time',
    'offset' => 600,
  ),
  4 => 
  array (
    'Code' => 'Pacific/Tahiti',
    'name' => '(GMT-1000) French Polynesia Time',
    'offset' => 600,
  ),
  5 => 
  array (
    'Code' => 'Pacific/Johnston',
    'name' => '(GMT-1000) U.S. Outlying Islands (Johnston) Time',
    'offset' => 600,
  ),
  6 => 
  array (
    'Code' => 'Pacific/Honolulu',
    'name' => '(GMT-1000) United States (Honolulu) Time',
    'offset' => 600,
  ),
  7 => 
  array (
    'Code' => 'Pacific/Marquesas',
    'name' => '(GMT-0930) World (Marquesas) Time',
    'offset' => 570,
  ),
  8 => 
  array (
    'Code' => 'America/Anchorage',
    'name' => '(GMT-0900) United States (Anchorage) Time',
    'offset' => 540,
  ),
  9 => 
  array (
    'Code' => 'America/Nome',
    'name' => '(GMT-0900) United States (Nome) Time',
    'offset' => 540,
  ),
  10 => 
  array (
    'Code' => 'Pacific/Gambier',
    'name' => '(GMT-0900) World (Gambier) Time',
    'offset' => 540,
  ),
  11 => 
  array (
    'Code' => 'America/Vancouver',
    'name' => '(GMT-0800) Canada (Vancouver) Time',
    'offset' => 480,
  ),
  12 => 
  array (
    'Code' => 'America/Whitehorse',
    'name' => '(GMT-0800) Canada (Whitehorse) Time',
    'offset' => 480,
  ),
  13 => 
  array (
    'Code' => 'America/Tijuana',
    'name' => '(GMT-0800) Mexico (Tijuana) Time',
    'offset' => 480,
  ),
  14 => 
  array (
    'Code' => 'America/Los_Angeles',
    'name' => '(GMT-0800) United States (Los Angeles) Time',
    'offset' => 480,
  ),
  15 => 
  array (
    'Code' => 'Pacific/Pitcairn',
    'name' => '(GMT-0800) World (Pitcairn) Time',
    'offset' => 480,
  ),
  16 => 
  array (
    'Code' => 'America/Dawson_Creek',
    'name' => '(GMT-0700) Canada (Dawson Creek) Time',
    'offset' => 420,
  ),
  17 => 
  array (
    'Code' => 'America/Edmonton',
    'name' => '(GMT-0700) Canada (Edmonton) Time',
    'offset' => 420,
  ),
  18 => 
  array (
    'Code' => 'America/Yellowknife',
    'name' => '(GMT-0700) Canada (Yellowknife) Time',
    'offset' => 420,
  ),
  19 => 
  array (
    'Code' => 'America/Hermosillo',
    'name' => '(GMT-0700) Mexico (Hermosillo) Time',
    'offset' => 420,
  ),
  20 => 
  array (
    'Code' => 'America/Mazatlan',
    'name' => '(GMT-0700) Mexico (Mazatlan) Time',
    'offset' => 420,
  ),
  21 => 
  array (
    'Code' => 'America/Denver',
    'name' => '(GMT-0700) United States (Denver) Time',
    'offset' => 420,
  ),
  22 => 
  array (
    'Code' => 'America/Phoenix',
    'name' => '(GMT-0700) United States (Phoenix) Time',
    'offset' => 420,
  ),
  23 => 
  array (
    'Code' => 'America/Belize',
    'name' => '(GMT-0600) Belize Time',
    'offset' => 360,
  ),
  24 => 
  array (
    'Code' => 'America/Regina',
    'name' => '(GMT-0600) Canada (Regina) Time',
    'offset' => 360,
  ),
  25 => 
  array (
    'Code' => 'America/Winnipeg',
    'name' => '(GMT-0600) Canada (Winnipeg) Time',
    'offset' => 360,
  ),
  26 => 
  array (
    'Code' => 'America/Costa_Rica',
    'name' => '(GMT-0600) Costa Rica Time',
    'offset' => 360,
  ),
  27 => 
  array (
    'Code' => 'Pacific/Galapagos',
    'name' => '(GMT-0600) Ecuador (Galapagos) Time',
    'offset' => 360,
  ),
  28 => 
  array (
    'Code' => 'America/El_Salvador',
    'name' => '(GMT-0600) El Salvador Time',
    'offset' => 360,
  ),
  29 => 
  array (
    'Code' => 'America/Guatemala',
    'name' => '(GMT-0600) Guatemala Time',
    'offset' => 360,
  ),
  30 => 
  array (
    'Code' => 'America/Tegucigalpa',
    'name' => '(GMT-0600) Honduras Time',
    'offset' => 360,
  ),
  31 => 
  array (
    'Code' => 'America/Merida',
    'name' => '(GMT-0600) Mexico (Merida) Time',
    'offset' => 360,
  ),
  32 => 
  array (
    'Code' => 'America/Mexico_City',
    'name' => '(GMT-0600) Mexico (Mexico City) Time',
    'offset' => 360,
  ),
  33 => 
  array (
    'Code' => 'America/Monterrey',
    'name' => '(GMT-0600) Mexico (Monterrey) Time',
    'offset' => 360,
  ),
  34 => 
  array (
    'Code' => 'America/Managua',
    'name' => '(GMT-0600) Nicaragua Time',
    'offset' => 360,
  ),
  35 => 
  array (
    'Code' => 'America/Chicago',
    'name' => '(GMT-0600) United States (Chicago) Time',
    'offset' => 360,
  ),
  36 => 
  array (
    'Code' => 'America/Nassau',
    'name' => '(GMT-0500) Bahamas Time',
    'offset' => 300,
  ),
  37 => 
  array (
    'Code' => 'America/Rio_Branco',
    'name' => '(GMT-0500) Brazil (Rio Branco) Time',
    'offset' => 300,
  ),
  38 => 
  array (
    'Code' => 'America/Iqaluit',
    'name' => '(GMT-0500) Canada (Iqaluit) Time',
    'offset' => 300,
  ),
  39 => 
  array (
    'Code' => 'America/Montreal',
    'name' => '(GMT-0500) Canada (Montreal) Time',
    'offset' => 300,
  ),
  40 => 
  array (
    'Code' => 'America/Toronto',
    'name' => '(GMT-0500) Canada (Toronto) Time',
    'offset' => 300,
  ),
  41 => 
  array (
    'Code' => 'America/Cayman',
    'name' => '(GMT-0500) Cayman Islands Time',
    'offset' => 300,
  ),
  42 => 
  array (
    'Code' => 'America/Bogota',
    'name' => '(GMT-0500) Colombia Time',
    'offset' => 300,
  ),
  43 => 
  array (
    'Code' => 'America/Guayaquil',
    'name' => '(GMT-0500) Ecuador (Guayaquil) Time',
    'offset' => 300,
  ),
  44 => 
  array (
    'Code' => 'America/Port-au-Prince',
    'name' => '(GMT-0500) Haiti Time',
    'offset' => 300,
  ),
  45 => 
  array (
    'Code' => 'America/Jamaica',
    'name' => '(GMT-0500) Jamaica Time',
    'offset' => 300,
  ),
  46 => 
  array (
    'Code' => 'America/Panama',
    'name' => '(GMT-0500) Panama Time',
    'offset' => 300,
  ),
  47 => 
  array (
    'Code' => 'America/Lima',
    'name' => '(GMT-0500) Peru Time',
    'offset' => 300,
  ),
  48 => 
  array (
    'Code' => 'America/New_York',
    'name' => '(GMT-0500) United States (New York) Time',
    'offset' => 300,
  ),
  49 => 
  array (
    'Code' => 'Pacific/Easter',
    'name' => '(GMT-0500) World (Easter) Time',
    'offset' => 300,
  ),
  50 => 
  array (
    'Code' => 'America/Havana',
    'name' => '(GMT-0500) World (Havana) Time',
    'offset' => 300,
  ),
  51 => 
  array (
    'Code' => 'America/Caracas',
    'name' => '(GMT-0430) Venezuela Time',
    'offset' => 270,
  ),
  52 => 
  array (
    'Code' => 'America/Anguilla',
    'name' => '(GMT-0400) Anguilla Time',
    'offset' => 240,
  ),
  53 => 
  array (
    'Code' => 'America/Antigua',
    'name' => '(GMT-0400) Antigua and Barbuda Time',
    'offset' => 240,
  ),
  54 => 
  array (
    'Code' => 'America/Aruba',
    'name' => '(GMT-0400) Aruba Time',
    'offset' => 240,
  ),
  55 => 
  array (
    'Code' => 'America/Barbados',
    'name' => '(GMT-0400) Barbados Time',
    'offset' => 240,
  ),
  56 => 
  array (
    'Code' => 'Atlantic/Bermuda',
    'name' => '(GMT-0400) Bermuda Time',
    'offset' => 240,
  ),
  57 => 
  array (
    'Code' => 'America/La_Paz',
    'name' => '(GMT-0400) Bolivia Time',
    'offset' => 240,
  ),
  58 => 
  array (
    'Code' => 'America/Boa_Vista',
    'name' => '(GMT-0400) Brazil (Boa Vista) Time',
    'offset' => 240,
  ),
  59 => 
  array (
    'Code' => 'America/Manaus',
    'name' => '(GMT-0400) Brazil (Manaus) Time',
    'offset' => 240,
  ),
  60 => 
  array (
    'Code' => 'America/Porto_Velho',
    'name' => '(GMT-0400) Brazil (Porto Velho) Time',
    'offset' => 240,
  ),
  61 => 
  array (
    'Code' => 'America/Tortola',
    'name' => '(GMT-0400) British Virgin Islands Time',
    'offset' => 240,
  ),
  62 => 
  array (
    'Code' => 'America/Halifax',
    'name' => '(GMT-0400) Canada (Halifax) Time',
    'offset' => 240,
  ),
  63 => 
  array (
    'Code' => 'America/Moncton',
    'name' => '(GMT-0400) Canada (Moncton) Time',
    'offset' => 240,
  ),
  64 => 
  array (
    'Code' => 'America/Curacao',
    'name' => '(GMT-0400) Curacao Time',
    'offset' => 240,
  ),
  65 => 
  array (
    'Code' => 'America/Dominica',
    'name' => '(GMT-0400) Dominica Time',
    'offset' => 240,
  ),
  66 => 
  array (
    'Code' => 'America/Santo_Domingo',
    'name' => '(GMT-0400) Dominican Republic Time',
    'offset' => 240,
  ),
  67 => 
  array (
    'Code' => 'America/Thule',
    'name' => '(GMT-0400) Greenland (Thule) Time',
    'offset' => 240,
  ),
  68 => 
  array (
    'Code' => 'America/Grenada',
    'name' => '(GMT-0400) Grenada Time',
    'offset' => 240,
  ),
  69 => 
  array (
    'Code' => 'America/Guadeloupe',
    'name' => '(GMT-0400) Guadeloupe Time',
    'offset' => 240,
  ),
  70 => 
  array (
    'Code' => 'America/Guyana',
    'name' => '(GMT-0400) Guyana Time',
    'offset' => 240,
  ),
  71 => 
  array (
    'Code' => 'America/Martinique',
    'name' => '(GMT-0400) Martinique Time',
    'offset' => 240,
  ),
  72 => 
  array (
    'Code' => 'America/Montserrat',
    'name' => '(GMT-0400) Montserrat Time',
    'offset' => 240,
  ),
  73 => 
  array (
    'Code' => 'America/Puerto_Rico',
    'name' => '(GMT-0400) Puerto Rico Time',
    'offset' => 240,
  ),
  74 => 
  array (
    'Code' => 'America/St_Kitts',
    'name' => '(GMT-0400) Saint Kitts and Nevis Time',
    'offset' => 240,
  ),
  75 => 
  array (
    'Code' => 'America/St_Lucia',
    'name' => '(GMT-0400) Saint Lucia Time',
    'offset' => 240,
  ),
  76 => 
  array (
    'Code' => 'America/St_Vincent',
    'name' => '(GMT-0400) Saint Vincent and the Grenadines Time',
    'offset' => 240,
  ),
  77 => 
  array (
    'Code' => 'America/Port_of_Spain',
    'name' => '(GMT-0400) Trinidad and Tobago Time',
    'offset' => 240,
  ),
  78 => 
  array (
    'Code' => 'America/Grand_Turk',
    'name' => '(GMT-0400) Turks and Caicos Islands Time',
    'offset' => 240,
  ),
  79 => 
  array (
    'Code' => 'America/St_Thomas',
    'name' => '(GMT-0400) U.S. Virgin Islands Time',
    'offset' => 240,
  ),
  80 => 
  array (
    'Code' => 'America/St_Johns',
    'name' => '(GMT-0330) Canada (St. John’s) Time',
    'offset' => 210,
  ),
  81 => 
  array (
    'Code' => 'Antarctica/Palmer',
    'name' => '(GMT-0300) Antarctica (Palmer) Time',
    'offset' => 180,
  ),
  82 => 
  array (
    'Code' => 'Antarctica/Rothera',
    'name' => '(GMT-0300) Antarctica (Rothera) Time',
    'offset' => 180,
  ),
  83 => 
  array (
    'Code' => 'America/Argentina/Buenos_Aires',
    'name' => '(GMT-0300) Argentina (Buenos Aires) Time',
    'offset' => 180,
  ),
  84 => 
  array (
    'Code' => 'America/Araguaina',
    'name' => '(GMT-0300) Brazil (Araguaina) Time',
    'offset' => 180,
  ),
  85 => 
  array (
    'Code' => 'America/Bahia',
    'name' => '(GMT-0300) Brazil (Bahia) Time',
    'offset' => 180,
  ),
  86 => 
  array (
    'Code' => 'America/Belem',
    'name' => '(GMT-0300) Brazil (Belem) Time',
    'offset' => 180,
  ),
  87 => 
  array (
    'Code' => 'America/Campo_Grande',
    'name' => '(GMT-0300) Brazil (Campo Grande) Time',
    'offset' => 180,
  ),
  88 => 
  array (
    'Code' => 'America/Cuiaba',
    'name' => '(GMT-0300) Brazil (Cuiaba) Time',
    'offset' => 180,
  ),
  89 => 
  array (
    'Code' => 'America/Fortaleza',
    'name' => '(GMT-0300) Brazil (Fortaleza) Time',
    'offset' => 180,
  ),
  90 => 
  array (
    'Code' => 'America/Maceio',
    'name' => '(GMT-0300) Brazil (Maceio) Time',
    'offset' => 180,
  ),
  91 => 
  array (
    'Code' => 'America/Recife',
    'name' => '(GMT-0300) Brazil (Recife) Time',
    'offset' => 180,
  ),
  92 => 
  array (
    'Code' => 'America/Santiago',
    'name' => '(GMT-0300) Chile Time',
    'offset' => 180,
  ),
  93 => 
  array (
    'Code' => 'Atlantic/Stanley',
    'name' => '(GMT-0300) Falkland Islands Time',
    'offset' => 180,
  ),
  94 => 
  array (
    'Code' => 'America/Cayenne',
    'name' => '(GMT-0300) French Guiana Time',
    'offset' => 180,
  ),
  95 => 
  array (
    'Code' => 'America/Godthab',
    'name' => '(GMT-0300) Greenland (Nuuk) Time',
    'offset' => 180,
  ),
  96 => 
  array (
    'Code' => 'America/Asuncion',
    'name' => '(GMT-0300) Paraguay Time',
    'offset' => 180,
  ),
  97 => 
  array (
    'Code' => 'America/Paramaribo',
    'name' => '(GMT-0300) Suriname Time',
    'offset' => 180,
  ),
  98 => 
  array (
    'Code' => 'America/Montevideo',
    'name' => '(GMT-0300) Uruguay Time',
    'offset' => 180,
  ),
  99 => 
  array (
    'Code' => 'America/Miquelon',
    'name' => '(GMT-0300) World (Miquelon) Time',
    'offset' => 180,
  ),
  100 => 
  array (
    'Code' => 'America/Noronha',
    'name' => '(GMT-0200) Brazil (Noronha) Time',
    'offset' => 120,
  ),
  101 => 
  array (
    'Code' => 'America/Sao_Paulo',
    'name' => '(GMT-0200) Brazil (Sao Paulo) Time',
    'offset' => 120,
  ),
  102 => 
  array (
    'Code' => 'Atlantic/South_Georgia',
    'name' => '(GMT-0200) South Georgia and the South Sandwich Islands Time',
    'offset' => 120,
  ),
  103 => 
  array (
    'Code' => 'Atlantic/Cape_Verde',
    'name' => '(GMT-0100) Cape Verde Time',
    'offset' => 60,
  ),
  104 => 
  array (
    'Code' => 'America/Scoresbysund',
    'name' => '(GMT-0100) Greenland (Ittoqqortoormiit) Time',
    'offset' => 60,
  ),
  105 => 
  array (
    'Code' => 'Atlantic/Azores',
    'name' => '(GMT-0100) Portugal (Azores) Time',
    'offset' => 60,
  ),
  106 => 
  array (
    'Code' => 'Africa/Ouagadougou',
    'name' => '(GMT+0000) Burkina Faso Time',
    'offset' => 0,
  ),
  107 => 
  array (
    'Code' => 'Africa/Abidjan',
    'name' => '(GMT+0000) Cote d’Ivoire Time',
    'offset' => 0,
  ),
  108 => 
  array (
    'Code' => 'Atlantic/Faroe',
    'name' => '(GMT+0000) Faroe Islands Time',
    'offset' => 0,
  ),
  109 => 
  array (
    'Code' => 'Africa/Banjul',
    'name' => '(GMT+0000) Gambia Time',
    'offset' => 0,
  ),
  110 => 
  array (
    'Code' => 'Africa/Accra',
    'name' => '(GMT+0000) Ghana Time',
    'offset' => 0,
  ),
  111 => 
  array (
    'Code' => 'America/Danmarkshavn',
    'name' => '(GMT+0000) Greenland (Danmarkshavn) Time',
    'offset' => 0,
  ),
  112 => 
  array (
    'Code' => 'Europe/Guernsey',
    'name' => '(GMT+0000) Guernsey Time',
    'offset' => 0,
  ),
  113 => 
  array (
    'Code' => 'Africa/Conakry',
    'name' => '(GMT+0000) Guinea Time',
    'offset' => 0,
  ),
  114 => 
  array (
    'Code' => 'Africa/Bissau',
    'name' => '(GMT+0000) Guinea-Bissau Time',
    'offset' => 0,
  ),
  115 => 
  array (
    'Code' => 'Atlantic/Reykjavik',
    'name' => '(GMT+0000) Iceland Time',
    'offset' => 0,
  ),
  116 => 
  array (
    'Code' => 'Europe/Dublin',
    'name' => '(GMT+0000) Ireland Time',
    'offset' => 0,
  ),
  117 => 
  array (
    'Code' => 'Europe/Isle_of_Man',
    'name' => '(GMT+0000) Isle of Man Time',
    'offset' => 0,
  ),
  118 => 
  array (
    'Code' => 'Europe/Jersey',
    'name' => '(GMT+0000) Jersey Time',
    'offset' => 0,
  ),
  119 => 
  array (
    'Code' => 'Africa/Monrovia',
    'name' => '(GMT+0000) Liberia Time',
    'offset' => 0,
  ),
  120 => 
  array (
    'Code' => 'Africa/Bamako',
    'name' => '(GMT+0000) Mali Time',
    'offset' => 0,
  ),
  121 => 
  array (
    'Code' => 'Africa/Nouakchott',
    'name' => '(GMT+0000) Mauritania Time',
    'offset' => 0,
  ),
  122 => 
  array (
    'Code' => 'Africa/Casablanca',
    'name' => '(GMT+0000) Morocco Time',
    'offset' => 0,
  ),
  123 => 
  array (
    'Code' => 'Europe/Lisbon',
    'name' => '(GMT+0000) Portugal (Lisbon) Time',
    'offset' => 0,
  ),
  124 => 
  array (
    'Code' => 'Atlantic/Madeira',
    'name' => '(GMT+0000) Portugal (Madeira) Time',
    'offset' => 0,
  ),
  125 => 
  array (
    'Code' => 'Atlantic/St_Helena',
    'name' => '(GMT+0000) Saint Helena Time',
    'offset' => 0,
  ),
  126 => 
  array (
    'Code' => 'Africa/Sao_Tome',
    'name' => '(GMT+0000) Sao Tome and Principe Time',
    'offset' => 0,
  ),
  127 => 
  array (
    'Code' => 'Africa/Dakar',
    'name' => '(GMT+0000) Senegal Time',
    'offset' => 0,
  ),
  128 => 
  array (
    'Code' => 'Africa/Freetown',
    'name' => '(GMT+0000) Sierra Leone Time',
    'offset' => 0,
  ),
  129 => 
  array (
    'Code' => 'Atlantic/Canary',
    'name' => '(GMT+0000) Spain (Canary) Time',
    'offset' => 0,
  ),
  130 => 
  array (
    'Code' => 'Africa/Lome',
    'name' => '(GMT+0000) Togo Time',
    'offset' => 0,
  ),
  131 => 
  array (
    'Code' => 'Europe/London',
    'name' => '(GMT+0000) United Kingdom Time',
    'offset' => 0,
  ),
  132 => 
  array (
    'Code' => 'Etc/GMT',
    'name' => '(GMT+0000) Unknown Region (GMT) Time',
    'offset' => 0,
  ),
  133 => 
  array (
    'Code' => 'Africa/El_Aaiun',
    'name' => '(GMT+0000) Western Sahara Time',
    'offset' => 0,
  ),
  134 => 
  array (
    'Code' => 'Europe/Tirane',
    'name' => '(GMT+0100) Albania Time',
    'offset' => -60,
  ),
  135 => 
  array (
    'Code' => 'Africa/Algiers',
    'name' => '(GMT+0100) Algeria Time',
    'offset' => -60,
  ),
  136 => 
  array (
    'Code' => 'Europe/Andorra',
    'name' => '(GMT+0100) Andorra Time',
    'offset' => -60,
  ),
  137 => 
  array (
    'Code' => 'Africa/Luanda',
    'name' => '(GMT+0100) Angola Time',
    'offset' => -60,
  ),
  138 => 
  array (
    'Code' => 'Europe/Vienna',
    'name' => '(GMT+0100) Austria Time',
    'offset' => -60,
  ),
  139 => 
  array (
    'Code' => 'Europe/Brussels',
    'name' => '(GMT+0100) Belgium Time',
    'offset' => -60,
  ),
  140 => 
  array (
    'Code' => 'Africa/Porto-Novo',
    'name' => '(GMT+0100) Benin Time',
    'offset' => -60,
  ),
  141 => 
  array (
    'Code' => 'Europe/Sarajevo',
    'name' => '(GMT+0100) Bosnia and Herzegovina Time',
    'offset' => -60,
  ),
  142 => 
  array (
    'Code' => 'Africa/Douala',
    'name' => '(GMT+0100) Cameroon Time',
    'offset' => -60,
  ),
  143 => 
  array (
    'Code' => 'Africa/Bangui',
    'name' => '(GMT+0100) Central African Republic Time',
    'offset' => -60,
  ),
  144 => 
  array (
    'Code' => 'Africa/Ndjamena',
    'name' => '(GMT+0100) Chad Time',
    'offset' => -60,
  ),
  145 => 
  array (
    'Code' => 'Africa/Brazzaville',
    'name' => '(GMT+0100) Congo - Brazzaville Time',
    'offset' => -60,
  ),
  146 => 
  array (
    'Code' => 'Africa/Kinshasa',
    'name' => '(GMT+0100) Congo - Kinshasa (Kinshasa) Time',
    'offset' => -60,
  ),
  147 => 
  array (
    'Code' => 'Europe/Zagreb',
    'name' => '(GMT+0100) Croatia Time',
    'offset' => -60,
  ),
  148 => 
  array (
    'Code' => 'Europe/Prague',
    'name' => '(GMT+0100) Czech Republic Time',
    'offset' => -60,
  ),
  149 => 
  array (
    'Code' => 'Europe/Copenhagen',
    'name' => '(GMT+0100) Denmark Time',
    'offset' => -60,
  ),
  150 => 
  array (
    'Code' => 'Africa/Malabo',
    'name' => '(GMT+0100) Equatorial Guinea Time',
    'offset' => -60,
  ),
  151 => 
  array (
    'Code' => 'Europe/Paris',
    'name' => '(GMT+0100) France Time',
    'offset' => -60,
  ),
  152 => 
  array (
    'Code' => 'Africa/Libreville',
    'name' => '(GMT+0100) Gabon Time',
    'offset' => -60,
  ),
  153 => 
  array (
    'Code' => 'Europe/Berlin',
    'name' => '(GMT+0100) Germany (Berlin) Time',
    'offset' => -60,
  ),
  154 => 
  array (
    'Code' => 'Europe/Busingen',
    'name' => '(GMT+0100) Germany (Busingen) Time',
    'offset' => -60,
  ),
  155 => 
  array (
    'Code' => 'Europe/Gibraltar',
    'name' => '(GMT+0100) Gibraltar Time',
    'offset' => -60,
  ),
  156 => 
  array (
    'Code' => 'Europe/Budapest',
    'name' => '(GMT+0100) Hungary Time',
    'offset' => -60,
  ),
  157 => 
  array (
    'Code' => 'Europe/Rome',
    'name' => '(GMT+0100) Italy Time',
    'offset' => -60,
  ),
  158 => 
  array (
    'Code' => 'Europe/Vaduz',
    'name' => '(GMT+0100) Liechtenstein Time',
    'offset' => -60,
  ),
  159 => 
  array (
    'Code' => 'Europe/Luxembourg',
    'name' => '(GMT+0100) Luxembourg Time',
    'offset' => -60,
  ),
  160 => 
  array (
    'Code' => 'Europe/Skopje',
    'name' => '(GMT+0100) Macedonia Time',
    'offset' => -60,
  ),
  161 => 
  array (
    'Code' => 'Europe/Malta',
    'name' => '(GMT+0100) Malta Time',
    'offset' => -60,
  ),
  162 => 
  array (
    'Code' => 'Europe/Monaco',
    'name' => '(GMT+0100) Monaco Time',
    'offset' => -60,
  ),
  163 => 
  array (
    'Code' => 'Europe/Podgorica',
    'name' => '(GMT+0100) Montenegro Time',
    'offset' => -60,
  ),
  164 => 
  array (
    'Code' => 'Europe/Amsterdam',
    'name' => '(GMT+0100) Netherlands Time',
    'offset' => -60,
  ),
  165 => 
  array (
    'Code' => 'Africa/Niamey',
    'name' => '(GMT+0100) Niger Time',
    'offset' => -60,
  ),
  166 => 
  array (
    'Code' => 'Africa/Lagos',
    'name' => '(GMT+0100) Nigeria Time',
    'offset' => -60,
  ),
  167 => 
  array (
    'Code' => 'Europe/Oslo',
    'name' => '(GMT+0100) Norway Time',
    'offset' => -60,
  ),
  168 => 
  array (
    'Code' => 'Europe/Warsaw',
    'name' => '(GMT+0100) Poland Time',
    'offset' => -60,
  ),
  169 => 
  array (
    'Code' => 'Europe/San_Marino',
    'name' => '(GMT+0100) San Marino Time',
    'offset' => -60,
  ),
  170 => 
  array (
    'Code' => 'Europe/Belgrade',
    'name' => '(GMT+0100) Serbia Time',
    'offset' => -60,
  ),
  171 => 
  array (
    'Code' => 'Europe/Bratislava',
    'name' => '(GMT+0100) Slovakia Time',
    'offset' => -60,
  ),
  172 => 
  array (
    'Code' => 'Europe/Ljubljana',
    'name' => '(GMT+0100) Slovenia Time',
    'offset' => -60,
  ),
  173 => 
  array (
    'Code' => 'Africa/Ceuta',
    'name' => '(GMT+0100) Spain (Ceuta) Time',
    'offset' => -60,
  ),
  174 => 
  array (
    'Code' => 'Europe/Madrid',
    'name' => '(GMT+0100) Spain (Madrid) Time',
    'offset' => -60,
  ),
  175 => 
  array (
    'Code' => 'Europe/Stockholm',
    'name' => '(GMT+0100) Sweden Time',
    'offset' => -60,
  ),
  176 => 
  array (
    'Code' => 'Europe/Zurich',
    'name' => '(GMT+0100) Switzerland Time',
    'offset' => -60,
  ),
  177 => 
  array (
    'Code' => 'Africa/Tunis',
    'name' => '(GMT+0100) Tunisia Time',
    'offset' => -60,
  ),
  178 => 
  array (
    'Code' => 'Europe/Vatican',
    'name' => '(GMT+0100) Vatican City Time',
    'offset' => -60,
  ),
  179 => 
  array (
    'Code' => 'Europe/Mariehamn',
    'name' => '(GMT+0200) Aland Islands Time',
    'offset' => -120,
  ),
  180 => 
  array (
    'Code' => 'Africa/Gaborone',
    'name' => '(GMT+0200) Botswana Time',
    'offset' => -120,
  ),
  181 => 
  array (
    'Code' => 'Europe/Sofia',
    'name' => '(GMT+0200) Bulgaria Time',
    'offset' => -120,
  ),
  182 => 
  array (
    'Code' => 'Africa/Bujumbura',
    'name' => '(GMT+0200) Burundi Time',
    'offset' => -120,
  ),
  183 => 
  array (
    'Code' => 'Africa/Lubumbashi',
    'name' => '(GMT+0200) Congo - Kinshasa (Lubumbashi) Time',
    'offset' => -120,
  ),
  184 => 
  array (
    'Code' => 'Asia/Nicosia',
    'name' => '(GMT+0200) Cyprus Time',
    'offset' => -120,
  ),
  185 => 
  array (
    'Code' => 'Africa/Cairo',
    'name' => '(GMT+0200) Egypt Time',
    'offset' => -120,
  ),
  186 => 
  array (
    'Code' => 'Europe/Tallinn',
    'name' => '(GMT+0200) Estonia Time',
    'offset' => -120,
  ),
  187 => 
  array (
    'Code' => 'Europe/Helsinki',
    'name' => '(GMT+0200) Finland Time',
    'offset' => -120,
  ),
  188 => 
  array (
    'Code' => 'Europe/Athens',
    'name' => '(GMT+0200) Greece Time',
    'offset' => -120,
  ),
  189 => 
  array (
    'Code' => 'Asia/Jerusalem',
    'name' => '(GMT+0200) Israel Time',
    'offset' => -120,
  ),
  190 => 
  array (
    'Code' => 'Asia/Amman',
    'name' => '(GMT+0200) Jordan Time',
    'offset' => -120,
  ),
  191 => 
  array (
    'Code' => 'Europe/Riga',
    'name' => '(GMT+0200) Latvia Time',
    'offset' => -120,
  ),
  192 => 
  array (
    'Code' => 'Asia/Beirut',
    'name' => '(GMT+0200) Lebanon Time',
    'offset' => -120,
  ),
  193 => 
  array (
    'Code' => 'Africa/Maseru',
    'name' => '(GMT+0200) Lesotho Time',
    'offset' => -120,
  ),
  194 => 
  array (
    'Code' => 'Africa/Tripoli',
    'name' => '(GMT+0200) Libya Time',
    'offset' => -120,
  ),
  195 => 
  array (
    'Code' => 'Europe/Vilnius',
    'name' => '(GMT+0200) Lithuania Time',
    'offset' => -120,
  ),
  196 => 
  array (
    'Code' => 'Africa/Blantyre',
    'name' => '(GMT+0200) Malawi Time',
    'offset' => -120,
  ),
  197 => 
  array (
    'Code' => 'Europe/Chisinau',
    'name' => '(GMT+0200) Moldova Time',
    'offset' => -120,
  ),
  198 => 
  array (
    'Code' => 'Africa/Maputo',
    'name' => '(GMT+0200) Mozambique Time',
    'offset' => -120,
  ),
  199 => 
  array (
    'Code' => 'Africa/Windhoek',
    'name' => '(GMT+0200) Namibia Time',
    'offset' => -120,
  ),
  200 => 
  array (
    'Code' => 'Asia/Gaza',
    'name' => '(GMT+0200) Palestinian Territories (Gaza) Time',
    'offset' => -120,
  ),
  201 => 
  array (
    'Code' => 'Asia/Hebron',
    'name' => '(GMT+0200) Palestinian Territories (Hebron) Time',
    'offset' => -120,
  ),
  202 => 
  array (
    'Code' => 'Europe/Bucharest',
    'name' => '(GMT+0200) Romania Time',
    'offset' => -120,
  ),
  203 => 
  array (
    'Code' => 'Europe/Kaliningrad',
    'name' => '(GMT+0200) Russia (Kaliningrad) Time',
    'offset' => -120,
  ),
  204 => 
  array (
    'Code' => 'Africa/Kigali',
    'name' => '(GMT+0200) Rwanda Time',
    'offset' => -120,
  ),
  205 => 
  array (
    'Code' => 'Africa/Johannesburg',
    'name' => '(GMT+0200) South Africa Time',
    'offset' => -120,
  ),
  206 => 
  array (
    'Code' => 'Africa/Mbabane',
    'name' => '(GMT+0200) Swaziland Time',
    'offset' => -120,
  ),
  207 => 
  array (
    'Code' => 'Asia/Damascus',
    'name' => '(GMT+0200) Syria Time',
    'offset' => -120,
  ),
  208 => 
  array (
    'Code' => 'Europe/Istanbul',
    'name' => '(GMT+0200) Turkey Time',
    'offset' => -120,
  ),
  209 => 
  array (
    'Code' => 'Europe/Kiev',
    'name' => '(GMT+0200) Ukraine (Kiev) Time',
    'offset' => -120,
  ),
  210 => 
  array (
    'Code' => 'Europe/Uzhgorod',
    'name' => '(GMT+0200) Ukraine (Uzhgorod) Time',
    'offset' => -120,
  ),
  211 => 
  array (
    'Code' => 'Europe/Zaporozhye',
    'name' => '(GMT+0200) Ukraine (Zaporozhye) Time',
    'offset' => -120,
  ),
  212 => 
  array (
    'Code' => 'Africa/Lusaka',
    'name' => '(GMT+0200) Zambia Time',
    'offset' => -120,
  ),
  213 => 
  array (
    'Code' => 'Africa/Harare',
    'name' => '(GMT+0200) Zimbabwe Time',
    'offset' => -120,
  ),
  214 => 
  array (
    'Code' => 'Antarctica/Syowa',
    'name' => '(GMT+0300) Antarctica (Syowa) Time',
    'offset' => -180,
  ),
  215 => 
  array (
    'Code' => 'Asia/Bahrain',
    'name' => '(GMT+0300) Bahrain Time',
    'offset' => -180,
  ),
  216 => 
  array (
    'Code' => 'Europe/Minsk',
    'name' => '(GMT+0300) Belarus Time',
    'offset' => -180,
  ),
  217 => 
  array (
    'Code' => 'Indian/Comoro',
    'name' => '(GMT+0300) Comoros Time',
    'offset' => -180,
  ),
  218 => 
  array (
    'Code' => 'Africa/Djibouti',
    'name' => '(GMT+0300) Djibouti Time',
    'offset' => -180,
  ),
  219 => 
  array (
    'Code' => 'Africa/Asmara',
    'name' => '(GMT+0300) Eritrea Time',
    'offset' => -180,
  ),
  220 => 
  array (
    'Code' => 'Africa/Addis_Ababa',
    'name' => '(GMT+0300) Ethiopia Time',
    'offset' => -180,
  ),
  221 => 
  array (
    'Code' => 'Asia/Baghdad',
    'name' => '(GMT+0300) Iraq Time',
    'offset' => -180,
  ),
  222 => 
  array (
    'Code' => 'Africa/Nairobi',
    'name' => '(GMT+0300) Kenya Time',
    'offset' => -180,
  ),
  223 => 
  array (
    'Code' => 'Asia/Kuwait',
    'name' => '(GMT+0300) Kuwait Time',
    'offset' => -180,
  ),
  224 => 
  array (
    'Code' => 'Indian/Antananarivo',
    'name' => '(GMT+0300) Madagascar Time',
    'offset' => -180,
  ),
  225 => 
  array (
    'Code' => 'Indian/Mayotte',
    'name' => '(GMT+0300) Mayotte Time',
    'offset' => -180,
  ),
  226 => 
  array (
    'Code' => 'Asia/Qatar',
    'name' => '(GMT+0300) Qatar Time',
    'offset' => -180,
  ),
  227 => 
  array (
    'Code' => 'Europe/Moscow',
    'name' => '(GMT+0300) Russia (Moscow) Time',
    'offset' => -180,
  ),
  228 => 
  array (
    'Code' => 'Europe/Volgograd',
    'name' => '(GMT+0300) Russia (Volgograd) Time',
    'offset' => -180,
  ),
  229 => 
  array (
    'Code' => 'Asia/Riyadh',
    'name' => '(GMT+0300) Saudi Arabia Time',
    'offset' => -180,
  ),
  230 => 
  array (
    'Code' => 'Africa/Mogadishu',
    'name' => '(GMT+0300) Somalia Time',
    'offset' => -180,
  ),
  231 => 
  array (
    'Code' => 'Africa/Juba',
    'name' => '(GMT+0300) South Sudan Time',
    'offset' => -180,
  ),
  232 => 
  array (
    'Code' => 'Africa/Khartoum',
    'name' => '(GMT+0300) Sudan Time',
    'offset' => -180,
  ),
  233 => 
  array (
    'Code' => 'Africa/Dar_es_Salaam',
    'name' => '(GMT+0300) Tanzania Time',
    'offset' => -180,
  ),
  234 => 
  array (
    'Code' => 'Africa/Kampala',
    'name' => '(GMT+0300) Uganda Time',
    'offset' => -180,
  ),
  235 => 
  array (
    'Code' => 'Europe/Simferopol',
    'name' => '(GMT+0300) Ukraine (Simferopol) Time',
    'offset' => -180,
  ),
  236 => 
  array (
    'Code' => 'Asia/Aden',
    'name' => '(GMT+0300) Yemen Time',
    'offset' => -180,
  ),
  237 => 
  array (
    'Code' => 'Asia/Tehran',
    'name' => '(GMT+0330) Iran Time',
    'offset' => -210,
  ),
  238 => 
  array (
    'Code' => 'Asia/Yerevan',
    'name' => '(GMT+0400) Armenia Time',
    'offset' => -240,
  ),
  239 => 
  array (
    'Code' => 'Asia/Baku',
    'name' => '(GMT+0400) Azerbaijan Time',
    'offset' => -240,
  ),
  240 => 
  array (
    'Code' => 'Asia/Tbilisi',
    'name' => '(GMT+0400) Georgia Time',
    'offset' => -240,
  ),
  241 => 
  array (
    'Code' => 'Indian/Mauritius',
    'name' => '(GMT+0400) Mauritius Time',
    'offset' => -240,
  ),
  242 => 
  array (
    'Code' => 'Asia/Muscat',
    'name' => '(GMT+0400) Oman Time',
    'offset' => -240,
  ),
  243 => 
  array (
    'Code' => 'Indian/Reunion',
    'name' => '(GMT+0400) Reunion Time',
    'offset' => -240,
  ),
  244 => 
  array (
    'Code' => 'Europe/Samara',
    'name' => '(GMT+0400) Russia (Samara) Time',
    'offset' => -240,
  ),
  245 => 
  array (
    'Code' => 'Indian/Mahe',
    'name' => '(GMT+0400) Seychelles Time',
    'offset' => -240,
  ),
  246 => 
  array (
    'Code' => 'Asia/Dubai',
    'name' => '(GMT+0400) United Arab Emirates Time',
    'offset' => -240,
  ),
  247 => 
  array (
    'Code' => 'Asia/Kabul',
    'name' => '(GMT+0430) Afghanistan Time',
    'offset' => -270,
  ),
  248 => 
  array (
    'Code' => 'Antarctica/Mawson',
    'name' => '(GMT+0500) Antarctica (Mawson) Time',
    'offset' => -300,
  ),
  249 => 
  array (
    'Code' => 'Indian/Kerguelen',
    'name' => '(GMT+0500) French Southern Territories Time',
    'offset' => -300,
  ),
  250 => 
  array (
    'Code' => 'Asia/Aqtau',
    'name' => '(GMT+0500) Kazakhstan (Aqtau) Time',
    'offset' => -300,
  ),
  251 => 
  array (
    'Code' => 'Asia/Aqtobe',
    'name' => '(GMT+0500) Kazakhstan (Aqtobe) Time',
    'offset' => -300,
  ),
  252 => 
  array (
    'Code' => 'Asia/Oral',
    'name' => '(GMT+0500) Kazakhstan (Oral) Time',
    'offset' => -300,
  ),
  253 => 
  array (
    'Code' => 'Indian/Maldives',
    'name' => '(GMT+0500) Maldives Time',
    'offset' => -300,
  ),
  254 => 
  array (
    'Code' => 'Asia/Karachi',
    'name' => '(GMT+0500) Pakistan Time',
    'offset' => -300,
  ),
  255 => 
  array (
    'Code' => 'Asia/Yekaterinburg',
    'name' => '(GMT+0500) Russia (Yekaterinburg) Time',
    'offset' => -300,
  ),
  256 => 
  array (
    'Code' => 'Asia/Dushanbe',
    'name' => '(GMT+0500) Tajikistan Time',
    'offset' => -300,
  ),
  257 => 
  array (
    'Code' => 'Asia/Ashgabat',
    'name' => '(GMT+0500) Turkmenistan Time',
    'offset' => -300,
  ),
  258 => 
  array (
    'Code' => 'Asia/Samarkand',
    'name' => '(GMT+0500) Uzbekistan (Samarkand) Time',
    'offset' => -300,
  ),
  259 => 
  array (
    'Code' => 'Asia/Tashkent',
    'name' => '(GMT+0500) Uzbekistan (Tashkent) Time',
    'offset' => -300,
  ),
  260 => 
  array (
    'Code' => 'Asia/Kolkata',
    'name' => '(GMT+0530) India Time',
    'offset' => -330,
  ),
  261 => 
  array (
    'Code' => 'Asia/Colombo',
    'name' => '(GMT+0530) Sri Lanka Time',
    'offset' => -330,
  ),
  262 => 
  array (
    'Code' => 'Asia/Kathmandu',
    'name' => '(GMT+0545) Nepal Time',
    'offset' => -345,
  ),
  263 => 
  array (
    'Code' => 'Asia/Katmandu',
    'name' => '(GMT+0545) Nepal Time',
    'offset' => -345,
  ),
  264 => 
  array (
    'Code' => 'Antarctica/Vostok',
    'name' => '(GMT+0600) Antarctica (Vostok) Time',
    'offset' => -360,
  ),
  265 => 
  array (
    'Code' => 'Asia/Dhaka',
    'name' => '(GMT+0600) Bangladesh Time',
    'offset' => -360,
  ),
  266 => 
  array (
    'Code' => 'Asia/Thimphu',
    'name' => '(GMT+0600) Bhutan Time',
    'offset' => -360,
  ),
  267 => 
  array (
    'Code' => 'Indian/Chagos',
    'name' => '(GMT+0600) British Indian Ocean Territory Time',
    'offset' => -360,
  ),
  268 => 
  array (
    'Code' => 'Asia/Kashgar',
    'name' => '(GMT+0600) China (Kashgar) Time',
    'offset' => -360,
  ),
  269 => 
  array (
    'Code' => 'Asia/Urumqi',
    'name' => '(GMT+0600) China (Urumqi) Time',
    'offset' => -360,
  ),
  270 => 
  array (
    'Code' => 'Asia/Almaty',
    'name' => '(GMT+0600) Kazakhstan (Almaty) Time',
    'offset' => -360,
  ),
  271 => 
  array (
    'Code' => 'Asia/Qyzylorda',
    'name' => '(GMT+0600) Kazakhstan (Qyzylorda) Time',
    'offset' => -360,
  ),
  272 => 
  array (
    'Code' => 'Asia/Bishkek',
    'name' => '(GMT+0600) Kyrgyzstan Time',
    'offset' => -360,
  ),
  273 => 
  array (
    'Code' => 'Asia/Novosibirsk',
    'name' => '(GMT+0600) Russia (Novosibirsk) Time',
    'offset' => -360,
  ),
  274 => 
  array (
    'Code' => 'Asia/Omsk',
    'name' => '(GMT+0600) Russia (Omsk) Time',
    'offset' => -360,
  ),
  275 => 
  array (
    'Code' => 'Indian/Cocos',
    'name' => '(GMT+0630) Cocos [Keeling] Islands Time',
    'offset' => -390,
  ),
  276 => 
  array (
    'Code' => 'Asia/Rangoon',
    'name' => '(GMT+0630) Myanmar [Burma] Time',
    'offset' => -390,
  ),
  277 => 
  array (
    'Code' => 'Antarctica/Davis',
    'name' => '(GMT+0700) Antarctica (Davis) Time',
    'offset' => -420,
  ),
  278 => 
  array (
    'Code' => 'Asia/Phnom_Penh',
    'name' => '(GMT+0700) Cambodia Time',
    'offset' => -420,
  ),
  279 => 
  array (
    'Code' => 'Indian/Christmas',
    'name' => '(GMT+0700) Christmas Island Time',
    'offset' => -420,
  ),
  280 => 
  array (
    'Code' => 'Asia/Jakarta',
    'name' => '(GMT+0700) Indonesia (Jakarta) Time',
    'offset' => -420,
  ),
  281 => 
  array (
    'Code' => 'Asia/Pontianak',
    'name' => '(GMT+0700) Indonesia (Pontianak) Time',
    'offset' => -420,
  ),
  282 => 
  array (
    'Code' => 'Asia/Vientiane',
    'name' => '(GMT+0700) Laos Time',
    'offset' => -420,
  ),
  283 => 
  array (
    'Code' => 'Asia/Hovd',
    'name' => '(GMT+0700) Mongolia (Hovd) Time',
    'offset' => -420,
  ),
  284 => 
  array (
    'Code' => 'Asia/Krasnoyarsk',
    'name' => '(GMT+0700) Russia (Krasnoyarsk) Time',
    'offset' => -420,
  ),
  285 => 
  array (
    'Code' => 'Asia/Novokuznetsk',
    'name' => '(GMT+0700) Russia (Novokuznetsk) Time',
    'offset' => -420,
  ),
  286 => 
  array (
    'Code' => 'Asia/Bangkok',
    'name' => '(GMT+0700) Thailand Time',
    'offset' => -420,
  ),
  287 => 
  array (
    'Code' => 'Asia/Ho_Chi_Minh',
    'name' => '(GMT+0700) Vietnam Time',
    'offset' => -420,
  ),
  288 => 
  array (
    'Code' => 'Asia/Saigon',
    'name' => '(GMT+0700) Vietnam Time',
    'offset' => -420,
  ),
  289 => 
  array (
    'Code' => 'Antarctica/Casey',
    'name' => '(GMT+0800) Antarctica (Casey) Time',
    'offset' => -480,
  ),
  290 => 
  array (
    'Code' => 'Australia/Perth',
    'name' => '(GMT+0800) Australia (Perth) Time',
    'offset' => -480,
  ),
  291 => 
  array (
    'Code' => 'Asia/Brunei',
    'name' => '(GMT+0800) Brunei Time',
    'offset' => -480,
  ),
  292 => 
  array (
    'Code' => 'Asia/Chongqing',
    'name' => '(GMT+0800) China (Chongqing) Time',
    'offset' => -480,
  ),
  293 => 
  array (
    'Code' => 'Asia/Harbin',
    'name' => '(GMT+0800) China (Harbin) Time',
    'offset' => -480,
  ),
  294 => 
  array (
    'Code' => 'Asia/Shanghai',
    'name' => '(GMT+0800) China (Shanghai) Time',
    'offset' => -480,
  ),
  295 => 
  array (
    'Code' => 'Asia/Hong_Kong',
    'name' => '(GMT+0800) Hong Kong SAR China Time',
    'offset' => -480,
  ),
  296 => 
  array (
    'Code' => 'Asia/Makassar',
    'name' => '(GMT+0800) Indonesia (Makassar) Time',
    'offset' => -480,
  ),
  297 => 
  array (
    'Code' => 'Asia/Macau',
    'name' => '(GMT+0800) Macau SAR China Time',
    'offset' => -480,
  ),
  298 => 
  array (
    'Code' => 'Asia/Kuala_Lumpur',
    'name' => '(GMT+0800) Malaysia (Kuala Lumpur) Time',
    'offset' => -480,
  ),
  299 => 
  array (
    'Code' => 'Asia/Kuching',
    'name' => '(GMT+0800) Malaysia (Kuching) Time',
    'offset' => -480,
  ),
  300 => 
  array (
    'Code' => 'Asia/Choibalsan',
    'name' => '(GMT+0800) Mongolia (Choibalsan) Time',
    'offset' => -480,
  ),
  301 => 
  array (
    'Code' => 'Asia/Ulaanbaatar',
    'name' => '(GMT+0800) Mongolia (Ulaanbaatar) Time',
    'offset' => -480,
  ),
  302 => 
  array (
    'Code' => 'Asia/Manila',
    'name' => '(GMT+0800) Philippines Time',
    'offset' => -480,
  ),
  303 => 
  array (
    'Code' => 'Asia/Irkutsk',
    'name' => '(GMT+0800) Russia (Irkutsk) Time',
    'offset' => -480,
  ),
  304 => 
  array (
    'Code' => 'Asia/Singapore',
    'name' => '(GMT+0800) Singapore Time',
    'offset' => -480,
  ),
  305 => 
  array (
    'Code' => 'Asia/Taipei',
    'name' => '(GMT+0800) Taiwan Time',
    'offset' => -480,
  ),
  306 => 
  array (
    'Code' => 'Asia/Pyongyang',
    'name' => '(GMT+0830) North Korea Time',
    'offset' => -510,
  ),
  307 => 
  array (
    'Code' => 'Asia/Jayapura',
    'name' => '(GMT+0900) Indonesia (Jayapura) Time',
    'offset' => -540,
  ),
  308 => 
  array (
    'Code' => 'Asia/Tokyo',
    'name' => '(GMT+0900) Japan Time',
    'offset' => -540,
  ),
  309 => 
  array (
    'Code' => 'Pacific/Palau',
    'name' => '(GMT+0900) Palau Time',
    'offset' => -540,
  ),
  310 => 
  array (
    'Code' => 'Asia/Khandyga',
    'name' => '(GMT+0900) Russia (Khandyga) Time',
    'offset' => -540,
  ),
  311 => 
  array (
    'Code' => 'Asia/Yakutsk',
    'name' => '(GMT+0900) Russia (Yakutsk) Time',
    'offset' => -540,
  ),
  312 => 
  array (
    'Code' => 'Asia/Seoul',
    'name' => '(GMT+0900) South Korea Time',
    'offset' => -540,
  ),
  313 => 
  array (
    'Code' => 'Asia/Dili',
    'name' => '(GMT+0900) Timor-Leste Time',
    'offset' => -540,
  ),
  314 => 
  array (
    'Code' => 'Australia/Darwin',
    'name' => '(GMT+0930) Australia (Darwin) Time',
    'offset' => -570,
  ),
  315 => 
  array (
    'Code' => 'Antarctica/DumontDUrville',
    'name' => '(GMT+1000) Antarctica (Dumont d’Urville) Time',
    'offset' => -600,
  ),
  316 => 
  array (
    'Code' => 'Australia/Brisbane',
    'name' => '(GMT+1000) Australia (Brisbane) Time',
    'offset' => -600,
  ),
  317 => 
  array (
    'Code' => 'Pacific/Guam',
    'name' => '(GMT+1000) Guam Time',
    'offset' => -600,
  ),
  318 => 
  array (
    'Code' => 'Pacific/Truk',
    'name' => '(GMT+1000) Micronesia (Chuuk) Time',
    'offset' => -600,
  ),
  319 => 
  array (
    'Code' => 'Pacific/Saipan',
    'name' => '(GMT+1000) Northern Mariana Islands Time',
    'offset' => -600,
  ),
  320 => 
  array (
    'Code' => 'Pacific/Port_Moresby',
    'name' => '(GMT+1000) Papua New Guinea Time',
    'offset' => -600,
  ),
  321 => 
  array (
    'Code' => 'Asia/Magadan',
    'name' => '(GMT+1000) Russia (Magadan) Time',
    'offset' => -600,
  ),
  322 => 
  array (
    'Code' => 'Asia/Sakhalin',
    'name' => '(GMT+1000) Russia (Sakhalin) Time',
    'offset' => -600,
  ),
  323 => 
  array (
    'Code' => 'Asia/Ust-Nera',
    'name' => '(GMT+1000) Russia (Ust-Nera) Time',
    'offset' => -600,
  ),
  324 => 
  array (
    'Code' => 'Asia/Vladivostok',
    'name' => '(GMT+1000) Russia (Vladivostok) Time',
    'offset' => -600,
  ),
  325 => 
  array (
    'Code' => 'Australia/Adelaide',
    'name' => '(GMT+1030) Australia (Adelaide) Time',
    'offset' => -630,
  ),
  326 => 
  array (
    'Code' => 'Australia/Hobart',
    'name' => '(GMT+1100) Australia (Hobart) Time',
    'offset' => -660,
  ),
  327 => 
  array (
    'Code' => 'Australia/Melbourne',
    'name' => '(GMT+1100) Australia (Melbourne) Time',
    'offset' => -660,
  ),
  328 => 
  array (
    'Code' => 'Australia/Sydney',
    'name' => '(GMT+1100) Australia (Sydney) Time',
    'offset' => -660,
  ),
  329 => 
  array (
    'Code' => 'Pacific/Kosrae',
    'name' => '(GMT+1100) Micronesia (Kosrae) Time',
    'offset' => -660,
  ),
  330 => 
  array (
    'Code' => 'Pacific/Ponape',
    'name' => '(GMT+1100) Micronesia (Pohnpei) Time',
    'offset' => -660,
  ),
  331 => 
  array (
    'Code' => 'Pacific/Noumea',
    'name' => '(GMT+1100) New Caledonia Time',
    'offset' => -660,
  ),
  332 => 
  array (
    'Code' => 'Pacific/Guadalcanal',
    'name' => '(GMT+1100) Solomon Islands Time',
    'offset' => -660,
  ),
  333 => 
  array (
    'Code' => 'Pacific/Efate',
    'name' => '(GMT+1100) Vanuatu Time',
    'offset' => -660,
  ),
  334 => 
  array (
    'Code' => 'Pacific/Norfolk',
    'name' => '(GMT+1100) World (Norfolk) Time',
    'offset' => -660,
  ),
  335 => 
  array (
    'Code' => 'Pacific/Tarawa',
    'name' => '(GMT+1200) Kiribati (Tarawa) Time',
    'offset' => -720,
  ),
  336 => 
  array (
    'Code' => 'Pacific/Kwajalein',
    'name' => '(GMT+1200) Marshall Islands (Kwajalein) Time',
    'offset' => -720,
  ),
  337 => 
  array (
    'Code' => 'Pacific/Majuro',
    'name' => '(GMT+1200) Marshall Islands (Majuro) Time',
    'offset' => -720,
  ),
  338 => 
  array (
    'Code' => 'Pacific/Nauru',
    'name' => '(GMT+1200) Nauru Time',
    'offset' => -720,
  ),
  339 => 
  array (
    'Code' => 'Asia/Anadyr',
    'name' => '(GMT+1200) Russia (Anadyr) Time',
    'offset' => -720,
  ),
  340 => 
  array (
    'Code' => 'Asia/Kamchatka',
    'name' => '(GMT+1200) Russia (Kamchatka) Time',
    'offset' => -720,
  ),
  341 => 
  array (
    'Code' => 'Pacific/Funafuti',
    'name' => '(GMT+1200) Tuvalu Time',
    'offset' => -720,
  ),
  342 => 
  array (
    'Code' => 'Pacific/Wake',
    'name' => '(GMT+1200) U.S. Outlying Islands (Wake) Time',
    'offset' => -720,
  ),
  343 => 
  array (
    'Code' => 'Pacific/Wallis',
    'name' => '(GMT+1200) Wallis and Futuna Time',
    'offset' => -720,
  ),
  344 => 
  array (
    'Code' => 'Pacific/Fiji',
    'name' => '(GMT+1300) Fiji Time',
    'offset' => -780,
  ),
  345 => 
  array (
    'Code' => 'Pacific/Enderbury',
    'name' => '(GMT+1300) Kiribati (Enderbury) Time',
    'offset' => -780,
  ),
  346 => 
  array (
    'Code' => 'Pacific/Auckland',
    'name' => '(GMT+1300) New Zealand Time',
    'offset' => -780,
  ),
  347 => 
  array (
    'Code' => 'Pacific/Fakaofo',
    'name' => '(GMT+1300) Tokelau Time',
    'offset' => -780,
  ),
  348 => 
  array (
    'Code' => 'Pacific/Tongatapu',
    'name' => '(GMT+1300) Tonga Time',
    'offset' => -780,
  ),
  349 => 
  array (
    'Code' => 'Pacific/Apia',
    'name' => '(GMT+1400) Samoa Time',
    'offset' => -840,
  ),
  350 => 
  array (
    'Code' => 'Pacific/Kiritimati',
    'name' => '(GMT+1400) World (Kiritimati) Time',
    'offset' => -840,
  ),
);