<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Timetable_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * This funtction send timetable entries with users and trading is allowed or not
     *
     * @param int $eventId - id of schdule for complete detail.
     *       
     */
    public function getEventTimetable($eventId,$view="")
    {
        $data ["select"] = [
            "t.table_id",
            "t.slot_id",
            "t.start_time",
            "t.end_time",
            "t.idPerformer",
            "p.chrName",
            "e.allowTrading",
            "e.performerCanPickTime",
            'p.stripe_account_id',
            'p.user_id',
            'p.stripe_status',
            "t.idEvent",
            "es.start_date",
            "es.end_date",
            "t.status"
        ];
        $data ["table"] = TABLE_TIMETABLE . " as t";
        
        if($view == 'venue'){
            $data ["where"] = [
                "t.idEvent" => $eventId,
                "t.status" => 'upcoming'
            ];
        }else{
            $data ["where"] = [
                "t.idEvent" => $eventId
            ];
        }
        
        $data ["order"] = "t.slot_id asc";
        $data ["join"] = [
            
            TABLE_EVENT_SCHEDULES . " as es" => [
                "es.table_id = t.idEvent",
                "INNER"
            ],
            TABLE_EVENT . " as e" => [
                "es.idEvent = e.idEvent",
                "INNER"
            ],
            
            TABLE_USER . " as p" => [
                "p.user_id = t.idPerformer",
                "LEFT"
            ]
        ];
        /* add below two joins to get paln id and check user followed or not by fan */
        if ($this->pidGroup == FAN_GROUP || $this->pidGroup == VENUE_GROUP || $this->pidGroup == PERFOMER_GROUP) {
            array_push($data ["select"], "fav.followerId", "up.plan_id");
            $data ["join"] [TABLE_FAVORITES . " as fav"] = [
                "p.user_id = fav.followingId AND fav.followerId = " . $this->pUserId,
                "LEFT"
            ];
            
            $data ["join"] [TABLE_USER_PLAN . " as up"] = [
                "p.user_id = up.user_id AND up.is_active = '1'",
                "LEFT"
            ];
             $data ['groupBy'] = 't.table_id';
        }
        
       return $data = $this->selectFromJoin($data);
         //echo $this->db->last_query(); exit;
        // die; //pr($data);die;
    }

    /**
     * This function check whether requeted user has already sent trading request and is still pending or not and
     * ont having any pending approvals also.
     * for same event schedule id
     *
     * @param int $performerId - Check for this perfomer id.
     *       
     * @return int / boolean If found record then returns slot_id else returns false
     */
    public function checkTradePending($performerId, $scheduleEventId)
    {
        $data ["select"] = [
            "idEvent",
            "requestBy",
            "requestTo",
            "requestBySlot",
            "requestToSlot"
        ];
        $data ["where"] = [
            "idEvent" => $scheduleEventId
        ];
        $data ["where_in"] = [
            "status" => [
                '2'
            ]
        ];
        $data ["group"] ["where"] = [
            "requestBy" => $performerId
        ];
        $data ["group"] ["or_where"] = [
            "requestTo" => $performerId
        ];
        $data ["table"] = TABLE_TRADING;
        $result = $this->selectRecords($data);
        return count($result) > 0 ? [
            "toSlotId" => $result [0]->requestToSlot,
            "fromSlotId" => $result [0]->requestBySlot,
            "requestTo" => $result [0]->requestTo,
            "requestFrom" => $result [0]->requestBy
        ] : false;
    }

    public function reserveSlot($performerId, $eventScheduleId, $slotId)
    {       
        /* validates perfomer's invitation is accepted or not */
        /* select idEvent from register where idPerformer=$idUser and status='Accepted' */
        $data ["select"] = [
            "idEvent"
        ];
        $data ["table"] = TABLE_REGISTER;
        $data ["where"] = [
            "idPerformer" => $performerId,
            "idEvent" => $eventScheduleId,
            "status" => "Accepted"
        ];
        $result = $this->selectRecords($data);
        
        /* Checks if it not already reserved */
        unset($data);
        $data ["select"] = [
            "idPerformer"
        ];
        $data ["where"] = [
            "idEvent" => $eventScheduleId,
            "idPerformer" => $performerId
        ];
        $data ["table"] = TABLE_TIMETABLE;
        $res = $this->selectRecords($data);
        if (count($res) > 0 && $performerId > 0) {
            return false;
        }
        
        unset($data);
        
        /* Book slot if his request is accepted */
        if (count($result) > 0 || $performerId == "-1") {
            $data ["update"] = [
                "idPerformer" => $performerId,
                "dtEdited" => $this->datetime
            ];
            $data ["where"] = [
                "slot_id" => $slotId,
                "idEvent" => $eventScheduleId,
                "idPerformer" => 0
            ];
            $data ["table"] = TABLE_TIMETABLE;
            $this->updateRecords($data);
            
            /* Checks if prior slots are empty or not */
            unset($data);
            $data ["select"] = [
                "idEvent"
            ];
            $data ["where"] = [
                "slot_id < " => $slotId,
                "idEvent" => $eventScheduleId,
                "idPerformer" => 0
            ];
            $data ["table"] = TABLE_TIMETABLE;
            $result = $this->selectRecords($data);
            
            $mainRes = $this->eventInfoByRegisterPerformerId($eventScheduleId);
            
            /* send notiifcation when record found */
            
            if (! empty($mainRes)) {
                $mainRes = $mainRes [0];
                $eventDateEmail = $mainRes->start_date . " " . $mainRes->startTime;
                $eventDateEmail = convertFromGMT($eventDateEmail, "default", $mainRes->timeZone);
                $eventDateEmail = date("jS M, Y", strtotime($eventDateEmail)) . " (" . $mainRes->timeZone . ")";
                if ($this->pidGroup == 3) {
                    $to = $mainRes->user_id;
                    $word = "own";
                }
                else {
                    $to = $performerId;
                    $word = "your";
                }
                
                $eventTitle = $mainRes->title;
                $subject = "Your time has updated!";
                $msg = $this->utility->decodeText($this->session->userdata("UserData")->chrName) . " has updated $word time for " . $this->utility->decodeText($eventTitle) . " On " . $eventDateEmail . "!";
                /* Saves message in databse using common function */
                $this->main_model->saveMessage($to, $this->pUserId, $subject, $msg);
            }
            
            if (count($result) > 0) {
                return - 1;
            }
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * This function will reset value of operformer id for slot and perform requesting
     *
     * @param unknown $performerId
     * @param unknown $slotId
     * @param int eventid = schedule id
     */
    public function resetSlot($performerId, $slotId, $eventId)
    {
        $data ["update"] = [
            "idPerformer" => 0,
            "dtEdited" => $this->datetime
        ];
        $data ["where"] = [
            "slot_id " => $slotId,
            "idPerformer" => $performerId,
            "idEvent" => $eventId
        ];
        $data ["table"] = TABLE_TIMETABLE;
        $this->updateRecords($data);
        
        /* Deletes Trading request value for same slot */
        unset($data);
        $data ["where"] = [
            "requestToSlot" => $slotId,
            "idEvent" => $eventId,
            "status" => "2"
        ];
        $data ["table"] = TABLE_TRADING;
        $this->deleteRecords($data);
        
        $data ["where"] = [
            "requestBySlot" => $slotId,
            "idEvent" => $eventId,
            "status" => "2"
        ];
        $this->deleteRecords($data);
        
        $mainRes = $this->eventInfoByRegisterPerformerId($eventId);
        
        /* send notiifcation when record found */
        
        if (! empty($mainRes)) {        
            $mainRes = $mainRes [0];
            $eventDateEmail = $mainRes->start_date . " " . $mainRes->startTime;
            $eventDateEmail = convertFromGMT($eventDateEmail, "default", $mainRes->timeZone);
            $eventDateEmail = date("jS M, Y", strtotime($eventDateEmail)) . " (" . $mainRes->timeZone . ")";
            
            if ($this->pidGroup == 3) {
                $to = $mainRes->user_id;
                $word = "own";
            }
            else {
                $to = $performerId;
                $word = "your";
            }
            $eventTitle = $mainRes->title;
            $subject = "Time rest!";
            $msg = $this->utility->decodeText($this->session->userdata("UserData")->chrName) . " has reset $word time for " . $this->utility->decodeText($eventTitle) . " on " . $eventDateEmail . "!";
            /* Saves message in databse using common function */
            $this->main_model->saveMessage($to, $this->pUserId, $subject, $msg);
        }
        return true;
    }

    /**
     * This function returns array of users that are accpeted invitation for this event schedule.
     *
     * @param int $scheduleEventId - schdeul event id
     *       
     * @return array of users
     */
    public function getAcceptedUsersForTimetable($scheduleEventId)
    {
        $data ["select"] = [
            "u.chrName",
            "u.user_id",
            "tt.table_id"
        ];
        $data ["table"] = TABLE_REGISTER . " rg";
        $data ["where"] = [
            "rg.idEvent" => $scheduleEventId,
            "rg.status" => "Accepted",
            "tt.table_id" => NULL
        ];
        $data ["join"] = [
            TABLE_USER . " u" => [
                "u.user_id = rg.idPerformer",
                "INNER"
            ],
            TABLE_TIMETABLE . " tt" => [
                "u.user_id = tt.idPerformer AND tt.idEvent = rg.idEvent",
                "LEFT"
            ]
        ];
        $result = $this->selectFromJoin($data);
        return $result;
    }

    /**
     * Used for check the list management permission, if user is performer.
     * check if user is host and have listmanagement
     *
     * @param int $userid,id of perfomer
     * @param int $scheduleEventId, event scheduleId
     *       
     * @return string , Yes/No
     */
    public function CheckListPermission($userId, $scheduleEventId)
    {
        $data ["select"] = [
            "idEvent,doList,isHost"
        ];
        $data ["where"] = [
            "idEvent" => $scheduleEventId,
            "doList" => 1,
            "isHost" => 1,
            "idPerformer" => $userId
        ];
        $data ["table"] = TABLE_REGISTER;
        $result = $this->countRecords($data);
        
        if ($result == 0) {
            return 'No';
        }
        else {
            return 'Yes';
        }
    }

    public function doTrade($idEvent, $requestSlot, $fromIdUser)
    {
        $data ["select"] = [
            "idEvent",
            "idPerformer",
            "slot_id"
        ];
        $data ["table"] = TABLE_TIMETABLE;
        $data ["where"] = [
            "idEvent" => $idEvent,
            "idPerformer" => $fromIdUser
        ];
        $fromData = $this->selectRecords($data);
        unset($data);
        
        $data ["select"] = [
            "idEvent",
            "idPerformer",
            "slot_id"
        ];
        $data ["table"] = TABLE_TIMETABLE;
        $data ["where"] = [
            "idEvent" => $idEvent,
            "slot_id" => $requestSlot
        ];
        $toData = $this->selectRecords($data);
        unset($data);
        
        if (count($fromData) == 0 || count($toData) == 0) {
            return 2;
        }
        
        $fromData = $fromData [0];
        $toData = $toData [0];
        
        $data ["select"] = [
            "*"
        ];
        $data ["table"] = TABLE_TRADING;
        $data ["where"] = [
            "idEvent" => $fromData->idEvent,
            "requestBy" => $fromData->idPerformer,
            "requestTo" => $toData->idPerformer,
            "requestToSlot" => $toData->slot_id,
            "requestBySlot" => $fromData->slot_id
        ];
        $res = $this->selectRecords($data);
        unset($data);
        
        if (count($res) != 0) {
            return 0;
        }
        
        $data ["insert"] = [
            "idEvent" => $fromData->idEvent,
            "requestBy" => $fromData->idPerformer,
            "requestBySlot" => $fromData->slot_id,
            "requestTo" => $toData->idPerformer,
            "requestToSlot" => $toData->slot_id,
            "requestToNotice" => "1"
        ];
        
        $data ["table"] = TABLE_TRADING;
        $this->insertRecord($data);
        
        $email = "<tr><td>You just received a new trading request.</td></tr>";
        $link = '<a href="' . SITEURL . $this->myvalues->tradingDetails ["controller"] . '" >Click Here</a> to view more detail.';
        
        /* $idUser, $inner_message, $toVenue = false, $link = "Please", $subject = "Notification Alert" */
        $this->utility->notification_email($toData->idPerformer, $email, false, $link, "Trading Request Received");
        
        $this->load->model($this->myvalues->eventDetails ["model"], "event_model");
        $eventDetails = $this->event_model->getEventSchedule($idEvent);
        
        $eventDateEmail = $eventDetails->start_date . " " . $eventDetails->startTime;
        $eventDateEmail = convertFromGMT($eventDateEmail, "default", $eventDetails->timeZone);
        $timezoneAbbriviation = $this->utility->getAbbreviationOfTimezone($eventDetails->timeZone); //get abbriviation of timezone
        $eventDateEmail = date("jS M, Y", strtotime($eventDateEmail)) . " (" . $timezoneAbbriviation. ")";
        
        $subject = "App Notification: Trading Request Received!";
        $msg = "You have received a trading request for your upcoming show " . $eventDetails->title . " on " . $eventDateEmail . " ! Check your StageGator Trading Requests Tab";
        $this->main_model->saveMessage($toData->idPerformer, $fromData->idPerformer, $subject, $msg);
        
        return 1;
    }

    /**
     * eventInfoByRegisterPerformerId()
     * This method return event information to send notification
     *
     * @param integer $idEvent is event schedule id
     *       
     * @return return object of event details
     */
    function eventInfoByRegisterPerformerId($idEvent)
    {
        $data ["select"] = [
            "u.user_id",
            "ev.title",
            "ev.startTime",
            "es.start_date",
            "ev.timeZone"
        ];
        $data ["table"] = TABLE_REGISTER . ' rg';
        $data ["join"] = [
            TABLE_EVENT_SCHEDULES . ' es' => [
                "es.table_id =  rg.idEvent",
                "INNER"
            ],
            TABLE_EVENT . ' ev' => [
                "ev.idEvent =  es.idEvent",
                "INNER"
            ],
            TABLE_USER . ' u' => [
                "ev.idVenue =  u.user_id",
                "INNER"
            ]
        ];
        $data ["where"] = [
            "es.table_id" => $idEvent
        ];
        $mainRes = $this->selectFromJoin($data);
        return $mainRes;
    }

    /**
     * getEventDetails()
     * This method get details of whole that we are printing in view after reserve a time by performer
     *
     * @param unknown $idEvent
     * @param unknown $slotId
     * @return unknown
     */
    public function getEventDetails($idEvent, $slotId)
    {
        $data ["select"] = [
            
            "ev.title",
            "ev.idEvent",
            "sc.table_id",
            "ev.type",
            "ev.date",
            "ev.endDate",
            "ev.startTime",
            "ev.endTime",
            "ev.descr",
            "ev.timeZone",
            "sc.start_date",
            "sc.end_date",
            "lc.address",
            "lc.city",
            "lc.state",
            "lc.country",
            "tt.start_time",
            "tt.end_time"
        ];
        
        $data ["table"] = TABLE_EVENT . " as ev";
        
        $data ["join"] = [
            TABLE_EVENT_SCHEDULES . " as sc" => [
                "ev.idEvent = sc.idEvent",
                "INNER"
            ],
            TABLE_USER . " as u" => [
                "ev.idVenue = u.user_id",
                "INNER"
            ],
            TABLE_LOCATIONS . "  lc" => [
                "lc.location_id = ev.location_id",
                "LEFT"
            ],
            TABLE_TIMETABLE . "  tt" => [
                "tt.idEvent = sc.table_id",
                "INNER"
            ]
        ];
        $data ["where"] = [
            "tt.idEvent" => $idEvent,
            "slot_id" => $slotId
        ];
        $result = $this->selectFromJoin($data);
        return $result [0];
    }
    
    
    /**
     * This function will change Status of performer
     *
     * @param $table_Id int, timetable schedule id
     * @param $slotId int, event shedule slote Id
     * @param $eventId int, event schedule id
     * @param $status string , type of performer status
     */
    public function changeStatus($table_Id, $slotId, $eventId, $status)
    {
        //change running status to performed befor start new performer on running
        if($status != 'upcoming'){
            $data ["update"] = [
                "status" => 'performed',
                "dtEdited" => $this->datetime
            ];
            $data ["where"] = [
                "idEvent" => $eventId,
                "status" => 'onstage'
            ];
            
            $data ["table"] = TABLE_TIMETABLE;
            $this->updateRecords($data);
        }
        
        //change status when start new performer on running
        $data ["update"] = [
            "status" => $status,
            "dtEdited" => $this->datetime
        ];
        $data ["where"] = [
            "slot_id " => $slotId,
            "table_Id" => $table_Id,
            "idEvent" => $eventId
        ];
        $data ["table"] = TABLE_TIMETABLE;
        $this->updateRecords($data);
        
        return true;
    }
    
    /**
     * This funtction send running timetable entries with users.
     *
     * @param int $eventId - id of schdule for complete detail.
     *
     */
    public function getRunningEventTimetable($eventId)
    {
        $data ["select"] = [
            "t.table_id",
            "t.slot_id",
            "t.start_time",
            "t.end_time",
            "t.idPerformer",
            "p.chrName",
            "e.allowTrading",
            "e.performerCanPickTime",
            'p.stripe_account_id',
            'p.user_id',
            'p.stripe_status',
            "t.idEvent",
            "es.start_date",
            "es.end_date",
            "t.status"
        ];
        $data ["table"] = TABLE_TIMETABLE . " as t";
        $data ["where"] = [
            "t.idEvent" => $eventId
        ];
        $data ["where_in"] = [
            "t.status" => array('onstage','performed')
        ];
        
        $data ["order"] = "t.slot_id asc";
        $data ["join"] = [
            
            TABLE_EVENT_SCHEDULES . " as es" => [
                "es.table_id = t.idEvent",
                "INNER"
            ],
            TABLE_EVENT . " as e" => [
                "es.idEvent = e.idEvent",
                "INNER"
            ],
            
            TABLE_USER . " as p" => [
                "p.user_id = t.idPerformer",
                "LEFT"
            ]
        ];
        /* add below two joins to get paln id and check user followed or not by fan */
        if ($this->pidGroup == FAN_GROUP || $this->pidGroup == VENUE_GROUP || $this->pidGroup == PERFOMER_GROUP) {
            array_push($data ["select"], "fav.followerId", "up.plan_id");
            $data ["join"] [TABLE_FAVORITES . " as fav"] = [
                "p.user_id = fav.followingId AND fav.followerId = " . $this->pUserId,
                "LEFT"
            ];
            
            $data ["join"] [TABLE_USER_PLAN . " as up"] = [
                "p.user_id = up.user_id AND up.is_active = '1'",
                "LEFT"
            ];
            $data ['groupBy'] = 't.table_id';
        }
        $data = $this->selectFromJoin($data);
        return $data;
    }

}

?>
