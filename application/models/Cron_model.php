<?php

class Cron_model extends My_model
{
    
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * This funtction get timetable entries based on given date and type
     *
     * @param DateTime $date - date of completed perfomance
     * @param string $type - type of perfomer who is upcoming, performed or onstage
     */
    public function getEventTimetableForCron($date=null,$type="performed")
    {
        $data ["select"] = [
            "t.table_id",
            "t.date",
            "t.start_time",
            "t.end_time",
            "t.dtAdded",
            "t.idPerformer",
            "t.idEvent",
            'p.stripe_account_id',
            'p.user_id',
            'p.stripe_status',
            "es.start_date",
            "es.end_date"
        ];
        $data ["table"] = TABLE_TIMETABLE . " as t";
        
        $data ["where"] = [
            "t.status" => $type,
            "t.date =" => $date
        ];
        
        $data ["join"] = [
            
            TABLE_EVENT_SCHEDULES . " as es" => [
                "es.table_id = t.idEvent",
                "INNER"
            ],
            TABLE_EVENT . " as e" => [
                "es.idEvent = e.idEvent",
                "INNER"
            ],
            
            TABLE_USER . " as p" => [
                "p.user_id = t.idPerformer",
                "LEFT"
            ]
        ];
        
        $datas = $this->selectFromJoin($data);
        
        // echo $this->db->last_query();
        return $datas;
        
        //exit;
        // die; //pr($data);die;
    }
    
    /**
     * getRecurringTipInfoByPerformer()
     * This method will get recurring tip info of based on performer they want to give tip
     *
     * @param int $performerId, performer's Id
     *
     * @return return data(Array)
     */
    public function getRecurringTipInfoByPerformer($performerId, $date)
    {
        $data ["select"] = ['*'];
        $data ["where"] = [
            "recurring_status" => 'Active',
            "recurringType" => 'performance',
            "performer_id" => $performerId,
            "created_date <" => $date
        ];
        
        $data ["table"] = TABLE_RECURRING_TIP_INFO;
        $res = $this->selectRecords($data);
                // echo $this->db->last_query();
        return $res;
    }
}