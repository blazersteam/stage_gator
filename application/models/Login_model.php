<?php

class Login_model extends My_model
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * authenticate()
     * This method authenticate if login successfully then return response to login controller as well as
     * validation also here
     *
     * @param string $email is user_name of user
     * @param string $password is user_password
     *
     * @return array $res with all user information
     */
    function authenticate($fbToken = false)
    {
        if (empty($fbToken)) {
            
            $checkData = [
                "u.user_name" => $this->utility->encodeText($this->input->post('txtEmail')),
                "u.user_password" => $this->utility->encodeText($this->input->post('txtPassword'))
            ];
        }
        else {
            
            $checkData = [
                "u.fb_token" => $fbToken ['id']
            ];
        }
        
        $data ["select"] = [
            'u.user_id',
            'u.user_name',
            'u.contact_person',
            'u.idGrp',
            'u.mem_id',
            'u.status_id',
            'u.statusMem',
            'u.chrName',
            'u.image',
            'u.islock',
            'u.suspended',
            'stripe_account_id',
            'stripe_status'
        ];
        
        $data ["table"] = TABLE_USER . " as u";
        
        $data ["where"] = $checkData;
        
        //for check login access level from Mobile App
        if(count($this->access_level) > 0){
            if(!empty($this->access_level['performer_venue'])){
                $data ["where_in"] = [
                    "idGrp" => $this->access_level['performer_venue']
                ];
            }else{
                $checkData["idGrp"] = $this->access_level['fan'];
                $data ["where"] = $checkData;
            }
        }else{
            
            $data ["where_in"] = [
                "idGrp" => [
                    1,
                    2,
                    3,
                    4
                ]
            ];
        }
        $data ["limit"] = 1;
        $res = $this->selectRecords($data);
        
        if (empty($res)) {
            return $this->checkduplicateFacebookEmail($fbToken);
        }
        return $res;
    }
    
    /**
     * checkduplicateFacebookEmail()
     * If got result null from authenticate method then we will check is any user have already register account
     * menually with same email
     *
     * @param unknown $fbToken
     * @return number
     */
    function checkduplicateFacebookEmail($fbToken)
    {
        if (isset($fbToken ['email']) && ! empty($fbToken ['email'])) {
            
            $data ["select"] = [
                'u.user_name'
            ];
            
            $data ["table"] = TABLE_USER . " as u";
            
            $data ["where"] = [
                "u.user_name" => $fbToken ['email']
            ];
            
            $data ["limit"] = 1;
            
            $res = $this->selectRecords($data);
            
            if (count($res) == 1) {
                $data ["where"] = [
                    "user_name" => $res [0]->user_name
                ];
                $data ["update"] ["fb_token"] = $fbToken ['id'];
                $data ["table"] = TABLE_USER;
                $this->updateRecords($data);
                return $this->authenticate($fbToken);
            }
        }
    }
    
    /**
     * getMemberandStyleStatus()
     * This method check is user has style or userhas enterd any city
     *
     * @param int $userId is current logged in user's user_id
     * @return object return a object of city and style
     */
    function getMemberandStyleStatus($userId)
    {
        $data ["select"] = [
            'mem.mem_city',
            'st.idStyle'
        ];
        
        $data ["table"] = TABLE_USER . " as u";
        
        $data ["join"] = [
            TABLE_MEMBERS . " as mem" => [
                "mem.mem_id = u.mem_id",
                "INNER"
            ],
            TABLE_USER_STYLE . " as st" => [
                "st.idUser = u.user_id",
                "LEFT"
            ]
        ];
        
        $data ["where"] = [
            
            "u.user_id" => $userId
        ];
        $res = $this->selectFromJoin($data);
        
        return isset($res [0]) ? $res [0] : [];
    }
    
    /**
     * setSessionData()
     * This method is use for update login time of user and set user object as a session
     *
     * @param $sessionData is current use object
     *
     * @return int $rtValue if user has logged in before it return 0 therwise 1 and update current time to it
     */
    function setSessionData($sessionData)
    {
        // load cookie halper
        //$this->load->helper('cookie');
        // create cookie for set welcome and welcome back messages
        if ($this->input->cookie('visit') == "true") {
            $rtValue = 1;
        }
        else {
            $rtValue = 0;
            $cookie = [
                'name' => 'visit',
                'value' => 'true',
                'expire' => time() + 60 * 60 * 24 * 600
            ];
            $this->input->set_cookie($cookie);
        }
        
        $data ["where"] = [
            "mem_id" => $sessionData->mem_id
        ];
        $data ["update"] ["mem_last_login"] = date('Y-m-d');
        $data ["table"] = TABLE_MEMBERS;
        $this->updateRecords($data);
        
        $getRedirect = '';
        
        /* if Admin login that time we set admin login url becoz this is a deiffrent module */
        if ($sessionData->idGrp == ADMIN_GROUP) {
            $getRedirect = SITEURL_ADMIN . $this->myvalues->dashboardDetails ['controller'];
        }
        /* if fan will login then redirect to upcomming shows */
        elseif ($sessionData->idGrp == FAN_GROUP) {
            $getRedirect = SITEURL . $this->myvalues->eventDetails ['controller'] . "/upcoming";
        }
        /* if performer login then redirect to my shows page */
        elseif ($sessionData->idGrp == PERFOMER_GROUP) {
            $getRedirect = SITEURL . $this->myvalues->performerDetails ['controller'] . "/shows";
        }
        /* if venue login then redirect to manage schedule page */
        elseif ($sessionData->idGrp == VENUE_GROUP) {
            $getRedirect = SITEURL . $this->myvalues->eventDetails ['controller'];
        }
        
        // $getRedirect = ($sessionData->idGrp == 2) ? SITEURL . 'profile' : (($sessionData->idGrp == 3) ? SITEURL
        // . 'profile' : SITEURL . "profile");
        $sessionData->dashboardURL = $getRedirect;
        // $this->session->set_userdata('dashboardURL', $getRedirect);
        $this->session->set_userdata("UserData", $sessionData);
        return $rtValue;
    }
    
    /**
     * checkDuplicate()
     * This method for check duplicate email
     *
     * @param string $email first parameter
     *
     * @return bool
     */
    function checkDuplicate()
    {
        $data ["where"] = [
            "user_name" => $this->utility->encodeText($this->input->post('txtEmail')),
            "status_id" => 1
        ];
        $data ["table"] = TABLE_USER;
        
        $isDuplicate = $this->isDuplicate($data);
        // return is email duplicate or not either 0 or 1
        
        return $isDuplicate;
    }
    
    /**
     * insert_reset_password()
     * This method insert token,email,created_date in reset_password table
     *
     * @param string $email first parameter
     *
     * @return return int
     */
    function insert_reset_password()
    {
        $dt = date("Y-m-d H:i:s");
        $time = strtotime($dt);
        $getUserEmail = $this->utility->encodeText($this->input->post('txtEmail'));
        $getStringToken = $getUserEmail . $time;
        
        // made a unique token for reset password
        $token = md5($getStringToken);
        
        // check we have sent a link to user already before within 4 hrs
        
        $data ["select"] = [
            "(TIME_TO_SEC('" . date('Y-m-d H:i:s') . "') - TIME_TO_SEC(created_time))/60 AS hrdiff"
        ];
        $data ["where"] = [
            'email' => $getUserEmail
        ];
        $data ["table"] = TABLE_RESET_PASSWORD;
        
        $res = $this->selectRecords($data);
        
        if (! empty($res)) {
            
            $timediff = abs($res [0]->hrdiff);
            $newTimeDiff = explode('.', $timediff);
            
            // here cheking if a links have already sent to user before within 4hr then delete old one and insert
            // new
            // link
            if ($newTimeDiff [0] < 240) {
                
                $data ["where"] = [
                    "email" => $getUserEmail
                ];
                $data ["table"] = TABLE_RESET_PASSWORD;
                $this->deleteRecords($data);
            }
        }
        
        $data ["insert"] ["email"] = $getUserEmail;
        $data ["insert"] ["token"] = $token;
        $data ["insert"] ["created_time"] = $dt;
        $data ["table"] = TABLE_RESET_PASSWORD;
        $ifSuccess = $this->insertRecord($data);
        
        // if links data save successfully in reset_password table then send an email for reset password
        if ($ifSuccess) {
            
            $data ["select"] = [
                "chrName"
            ];
            $data ["where"] = [
                "user_name" => $getUserEmail
            ];
            $data ["table"] = TABLE_USER;
            $res = $this->selectRecords($data);
            
            $person = $res [0]->chrName;
            $data ['person'] = $person;
            $data ['token'] = $token;
            // Load a view for email tamplate
            $message = $this->load->view('email_templates/recovery_template', $data, true);
            $data ["message"] = $message;
            $data ["from_title"] = EMAIL_TITLE;
            $data ["to"] = $getUserEmail;
            $data ["subject"] = $this->lang->line('email_subject_reset_your_password');
            // send mail using utilitys
            $status = $this->utility->sendMailSMTP($data);
            //$this->('MailgunCI.php');
            
            
        }
    }
    
    
    
    /**
     * check_link()
     * This method check user url request and also check the time has expired or not
     *
     * @param string $token is the token to match in database
     *
     * @return boolean false if link has been expired otherwise return true
     */
    function check_link($token = '')
    {
        $data ["select"] = [
            "email",
            "(TIME_TO_SEC('" . date('Y-m-d H:i:s') . "') - TIME_TO_SEC(created_time))/60 AS hrdiff"
        ];
        $data ["where"] = [
            "token" => $token
        ];
        $data ["table"] = TABLE_RESET_PASSWORD;
        
        $res = $this->selectRecords($data);
        
        if (! empty($res)) {
            
            $timediff = abs($res [0]->hrdiff);
            $newTimeDiff = explode('.', $timediff);
            
            // checking hours diffrence of 4hrs
            if ($newTimeDiff [0] > 240) {
                
                return false;
            }
            else {
                
                return true;
            }
        }
        else {
            
            return 'Error';
        }
    }
    
    /**
     * update_password()
     * Update password according to user requested url.first match all validation
     *
     * @param array $input is the token and new password to change
     *
     * @return array boolean true or false or validation messages
     */
    function update_password($input)
    {
        $data ["select"] = [
            "email"
        ];
        $data ["where"] = [
            "token" => $input ['token']
        ];
        $data ["table"] = TABLE_RESET_PASSWORD;
        $restoken = $this->selectRecords($data);
        $username = $restoken [0]->email;
        
        unset($data);
        $data ["where"] = [
            "user_name" => $username
        ];
        $data ["update"] ["user_password"] = md5($this->utility->encodeText($this->input->post('txtPassword')));
        $data ["update"] ["islock"] = '0';
        $data ["table"] = TABLE_USER;
        
        $result = $this->updateRecords($data);
        unset($data);
        // if password updated then delete link
        if (true) {
            
            $data ["where"] = [
                "token" => $input ['token']
            ];
            $data ["table"] = TABLE_RESET_PASSWORD;
            $this->deleteRecords($data);
            
            unset($data);
            // unset session if user block
            $data ["select"] = [
                "user_id"
            ];
            $data ["where"] = [
                "user_name" => $username
            ];
            $data ["table"] = TABLE_USER;
            $res = $this->selectRecords($data);
            $this->session->unset_userdata('user' . $res [0]->user_id);
            
            return [
                'success',
                'password_changed_successfully'
            ];
        }
    }
    
    /**
     * userLockStatus()
     * This method mantain Flag of locked user either 0 or 1
     *
     * @param String $email is for get user id from database
     *
     * @return String message
     */
    function userLockStatus($email)
    {
        $data ["select"] = [
            "user_id",
            "islock"
        ];
        $data ["where"] = [
            "user_name" => $email
        ];
        $data ["table"] = TABLE_USER;
        $res = $this->selectRecords($data);
        
        // check if user_id get then assign session key as user_id otherwise set session as 0
        $return = ($res == NULL) ? 0 : $res [0]->user_id;
        
        $checkRquestTime = $this->session->userdata('user' . $return);
        
        // check request time is less then 5 then we add 1 on each request
        if ($checkRquestTime < 5) {
            
            $this->session->set_userdata('user' . $return, 1 + $this->session->userdata('user' . $return));
            // return message of language library
            
            return 'user_authentication';
        }
        
        // if user request time more then 5 times then we will locked a user and update status in usertable isblock
        // = '1'
        else {
            
            $data ["where"] = [
                "user_name" => $email
            ];
            $data ["update"] ["islock"] = '1';
            $data ["table"] = TABLE_USER;
            $this->updateRecords($data);
            
            return 'user_account_request_complete';
        }
    }
    
    /**
     * *
     * activate_account()
     * Activation link comes from any user's email it is a encrypted key which use to ativate user's account
     *
     * @param String $access_key is encrypted key
     *
     * @return number 3 => already activated, 1=> activated successfully
     */
    function activate_account($access_key)
    {
        $data ["select"] = [
            "user_id",
            "access_key_date",
            "status_id"
        ];
        $data ["where"] = [
            "access_key" => $access_key
        ];
        $data ["table"] = TABLE_USER;
        $res = $this->selectRecords($data);
        if (count($res) == 1) {
            $res = $res [0];
            if ($res->status_id == 1) {
                return [
                    'success',
                    'account_activated_already'
                ];
            }
            
            $data ["update"] ["status_id"] = 1;
            $data ["where"] = [
                "user_id" => $res->user_id
            ];
            $data ["table"] = TABLE_USER;
            $this->updateRecords($data);
            
            return [
                'success',
                'account_activated'
            ];
        }
        else {
            return [
                'danger',
                'error_default_error'
            ];
        }
    }
    
    /**
     * change_email()
     * This method will update user login username
     *
     * @param integer $key is user change email key
     * @return array return session flash messages
     */
    function change_email($key)
    {
        $data ["select"] = [
            "new_email",
            "user_id"
        ];
        $data ["where"] = [
            "new_email_key" => $key
        ];
        $data ["table"] = TABLE_USER;
        $res = $this->selectRecords($data);
        
        if (count($res) == 1) {
            $res = $res [0];
            $data ["update"] ["user_name"] = $res->new_email;
            $data ["update"] ["new_email_key"] = NULL;
            $data ["update"] ["new_email"] = NULL;
            $data ["where"] = [
                "user_id" => $res->user_id
            ];
            $data ["table"] = TABLE_USER;
            $this->updateRecords($data);
            
            return [
                'success',
                'email_has_been_changed'
            ];
        }
        else {
            return [
                'danger',
                'error_default_error'
            ];
        }
    }
    
    /**
     * Used for update the fcmId Token value in database according to the device type
     *
     * @param string $deviceType , it may be Android / Iphone
     * @param int $fcmIdTocken, fcm tocken value
     */
    public function updateFcmTokenValue($deviceType, $fcmIdToken, $userId)
    {
        // Here first set the fcm_id to null that are set previously then update the this value
        if ($deviceType == 'Android') {
            
            $data ["update"] ["fcm_android"] = NULL;
            $data ["where"] = [
                "fcm_android" => $fcmIdToken
            ];
            $data ["table"] = TABLE_USER;
            $this->updateRecords($data);
            
            unset($data);
            
            $data ["update"] ["fcm_android"] = $fcmIdToken;
            $data ["where"] = [
                "user_id" => $userId
            ];
            $data ["table"] = TABLE_USER;
            $this->updateRecords($data);
        }
        else if ($deviceType == 'Iphone') {
            
            $data ["update"] ["fcm_iphone"] = NULL;
            $data ["where"] = [
                "fcm_iphone" => $fcmIdToken
            ];
            $data ["table"] = TABLE_USER;
            $this->updateRecords($data);
            
            unset($data);
            
            $data ["update"] ["fcm_iphone"] = $fcmIdToken;
            $data ["where"] = [
                "user_id" => $userId
            ];
            $data ["table"] = TABLE_USER;
            $this->updateRecords($data);
        }
    }
    
    /**
     * checkMemberStripAccountExit()
     * This method check is user has registerde account to strip or not
     *
     * @param int $userId is current logged in user's user_id
     * @return object return a object of city and style
     */
    function checkMemberStripAccountExit($userId)
    {
        $data ["select"] = ['mem.stripe_account_id'];
        
        $data ["table"] = TABLE_USER . " as u";
        
        $data ["join"] = [
            TABLE_STRIPE_ACCOUNT_DETAILS. " as mem" => [
                "mem.user_id = u.user_id",
                "INNER"
            ]
        ];
        
        $data ["where"] = [
            
            "mem.user_id" => $userId
        ];
        $res = $this->selectFromJoin($data);
        
        return isset($res [0]) ? $res [0] : [];
    }
    
    /**
     * generateSession()
     * This method is common method for facebook and regular login system
     *
     * @param unknown $result
     */
    function generateSession($result)
    {
        
        // check if user is locked already we are not going to login them
        if ($result->islock == 1) {
            
            $this->utility->setFlashMessage('danger', $this->lang->line('user_account_locked'));
            redirect($this->controller, 'location');
        }
        // if status id 0 then redirect to login
        if ($result->status_id == 0 && $result->idGrp != FAN_GROUP) {
            
            $this->utility->setFlashMessage('danger', $this->lang->line('user_deactive'));
            redirect($this->controller, 'location');
        }
        
        // if account is already suspended then redirect to login page
        if ($result->suspended == 1) {
            $this->utility->setFlashMessage('danger', $this->lang->line('user_suspend'));
            redirect($this->controller, 'location');
        }
        
        // if get cookie then update the fcmId Token
        
        if (! empty($this->input->cookie('fcmIdToken'))) {
            $fcmCookie = $this->utility->decode($this->input->cookie('fcmIdToken', TRUE));
            $tokenArray = explode("||||", $fcmCookie);
            if (! empty($tokenArray)) {
                $this->updateFcmTokenValue($tokenArray [0], $tokenArray [1], $result->user_id);
            }
        }
        
        // if user style or user_city got blank then redirect to profile page always
        $status = $this->getMemberandStyleStatus($result->user_id);
        $result->iscompleted = 1;
        /*
         * add one more condition when admin login that time this condition will not be true so iscompleted always
         * be 1
         */
        if ((empty($status->idStyle) || empty($status->mem_city)) && ($result->idGrp != ADMIN_GROUP)) {
            $result->iscompleted = 0;
        }
        
        // if user has performer and not registerd in strip account then redirect to tip registration page always
        $stripStatus = $this->checkMemberStripAccountExit($result->user_id);
        $result->tipRegistration = 1;
        /*
         * add one more condition when admin or fan or venue login that time this condition will not be true so iscompleted always
         * be 1
         */
        if (empty($stripStatus->stripe_account_id) && ($result->idGrp == PERFOMER_GROUP)) {
            $result->tipRegistration = 0;
        }
        
        // Update last login time and set message
        $n = $this->setSessionData($result);
        
        if ($n != 1) {
            $this->utility->setFlashMessage('success', 'Welcome to Stagegator, ' . $result->chrName . "!");
        }
        else {
            $this->utility->setFlashMessage('success', 'Welcome Back, ' . trim($result->chrName) . "!");
        }
        /*
         * add one more condition for admin login when admin login we will not check any style so this condition
         * will
         * always be false when admin log in
         */
        if ((empty($status->idStyle) || empty($status->mem_city)) && ($result->idGrp != ADMIN_GROUP && $result->idGrp != FAN_GROUP)) {
            redirect(SITEURL . $this->myvalues->profileDetails ['controller'] . '/edit');
        }
        redirect($this->session->userdata('UserData')->dashboardURL);
    }
}

?>