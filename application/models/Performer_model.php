<?php

class Performer_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * This method get event details of current event id
     *
     * @param int $id - table id of schedule table to get event whole details
     * @return Object it will return a query object to controller
     */
    function getEventBySchuduleId($id)
    {
        // $eventId = $this->getMainEventId($id);
        $data ["select"] = [
            "ev.title",
            "ev.type",
            "ev.idVenue",
            "ev.date as eventDate",
            "es.start_date as date",
            "es.end_date as endDate",
            "ev.startTime",
            "ev.endTime",
            "ev.descr",
            "ev.dtAdded",
            "ev.location_id",
            "ev.reInvite",
            "ev.autoAccept",
            "ev.autoReminder",
            "ev.timeZone",
            "ev.idEvent"
        ];
        
        $data ["where"] = [
            "es.table_id" => $id
        ];
        $data ["table"] = TABLE_EVENT_SCHEDULES . " es";
        $data ["join"] = [
            TABLE_EVENT . ' ev' => [
                "es.idEvent = ev.idEvent",
                "INNER"
            ]
        ];
        $res = $this->selectFromJoin($data);
        
        return empty($res) ? NULL : $res [0];
        /*
         * $sql = "select * from event join (select user_id, mem_city, mem_state, stateAbbrev, chrName from user
         * join members on user.mem_id=members.mem_id) as MEM on event.idVenue=MEM.user_id where idEvent=" . (int)
         * $id; return $this->db->query($sql)->row();
         */
    }

    /**
     * getMainEventId()
     * Common function to get main event id
     *
     * @param string event_scedule table's id $id
     * @return integer $res corresponds event id
     */
    function getMainEventId($id)
    {
        $data ["select"] = [
            "idEvent"
        ];
        $data ["where"] = [
            "table_id" => $id
        ];
        $data ["table"] = TABLE_EVENT_SCHEDULES;
        $res = $this->selectRecords($data);
        return $res [0]->idEvent;
    }

    /**
     * getPerformerStyleMatchingWithCurrentEvent()
     * This method reurn corresponds performer for the event
     *
     * @param integer $id is the event id
     * @return Object return query object to controller
     */
    function getPerformerStyleMatchingWithCurrentEvent($id)
    {
        $eventId = $this->getMainEventId($id);
        $data ['select'] = [
            'idStyle'
        ];
        $data ['where'] = [
            'idEvent' => $eventId
        ];
        $data ['table'] = TABLE_EVENT_STYLE;
        $res = $this->selectRecords($data);
        foreach ($res as $value) {
            $getStyle [] = $value->idStyle;
        }
        unset($data);
        $data ['select'] = [
            'idStyle',
            'nameStyle'
        ];
        $data ['where'] = [
            'idGrp' => PERFOMER_GROUP
        ];
        $data ['where_in'] = [
            'venue_style' => @$getStyle
        ];
        $data ['table'] = TABLE_STYLE;
        $res = $this->selectRecords($data);
        
        return $res;
    }

    /**
     * getPerformersInQueue()
     * this method return all performer id from register table
     *
     * @param integer $idEvent is the event id
     * @return Array $ids is all performer ids
     */
    function getPerformersInQueue($idEvent)
    {
        $data ["select"] = [
            "idPerformer"
        ];
        $data ["where"] = [
            "idEvent" => $idEvent
        ];
        $data ["where_in"] = [
            "status" => [
                "Applied",
                "Pending",
                "Accepted"
            ]
        ];
        $data ["table"] = TABLE_REGISTER;
        $res = $this->selectRecords($data);
        
        /*
         * $sql = "select idPerformer from register where idEvent=$idEvent"; $res = $this->db->query ( $sql );
         */
        $ids = array();
        foreach ($res as $r) {
            $ids [] = $r->idPerformer;
        }
        
        return $ids;
    }

    /**
     * searchPerformer()
     * This method for earch performer with city, state, name and style
     *
     * @param unknown $performer_styles_matching_with_venue
     * @return $res Performer result object
     */
    function searchPerformer($performer_styles_matching_with_venue)
    {
        if ($this->input->post()) {
            
            $style = $this->utility->encodeText($this->input->post('cmbStyle'));
            $txtName = $this->utility->encodeText($this->input->post('txtName'));
            $txtCityOrState = $this->utility->encodeText($this->input->post('txtCity'));
        }
        
        if (empty($style)) {
            foreach ($performer_styles_matching_with_venue as $allowed_performer_style) {
                $style [] = $allowed_performer_style->idStyle;
            }
        }
        
        $data ["select"] = [
            'u.user_id',
            'u.user_name',
            'u.contact_person',
            'u.idGrp',
            'u.mem_id',
            'u.status_id',
            'u.statusMem',
            'u.chrName',
            'u.islock',
            'u.suspended',
            "u.rate",
            "u.image",
            "u.url",
            'u.stripe_account_id',
            'u.stripe_status',
            "mem.mem_city",
            "mem.mem_state",
            "fav.followerId",
            "up.plan_id"
        ];
        $data ["table"] = TABLE_USER . " as u";
        
        $data ["join"] = [
            TABLE_USER_STYLE . " as us" => [
                "u.user_id = us.idUser",
                "INNER"
            ],
            TABLE_MEMBERS . " as mem" => [
                "u.mem_id = mem.mem_id",
                "INNER"
            ],
            TABLE_FAVORITES . " as fav" => [
                "us.idUser = fav.followingId AND fav.followerId = " . $this->pUserId,
                "LEFT"
            ],
            TABLE_USER_PLAN . " as up" => [
                "u.user_id = up.user_id AND up.is_active = '1'",
                "LEFT"
            ]
        ];
        if (! empty($txtName)) {
            
            // Used for search with state name
            $data ["like"] = [
                'u.chrName',
                $txtName,
                'both'
            ];
        }
        
        if (! empty($txtCityOrState)) {
            
            // Used for search with state name
            $data ["group"] ["like"] = [
                'mem.mem_city',
                $txtCityOrState,
                'both'
            ];
            $data ["group"] ["or_like"] [] = [
                'mem.mem_state',
                $txtCityOrState,
                "both"
            ];
        }
        if (! empty($style)) {
            $data ["where_in"] = [
                "idStyle" => $style
            ];
        }
        $data ["where"] = [
            "u.idGrp" => PERFOMER_GROUP,
            "u.status_id" => 1,
            "u.suspended" => 0,
            "u.user_id !=" => $this->pUserId
        ];
        $data ['order'] = 'u.rate desc,u.chrName asc';
        $data ['groupBy'] = 'us.idUser';
        $res = $this->selectFromJoin($data);
        
        return $res;
    }

    function getUserStyles($idUser)
    {
        $data ["select"] = [
            'nameStyle'
        ];
        $data ["table"] = TABLE_USER_STYLE . ' us';
        $data ["join"] = [
            TABLE_STYLE . " as st" => [
                "st.idStyle = us.idStyle",
                "INNER"
            ]
        ];
        $data ["where"] = [
            'idUser' => $idUser
        ];
        $res = $this->selectFromJoin($data);
        
        return $res;
    }

    /**
     * sendBulkInvite()
     * This method is use for invite performer and send an email notification
     *
     * @param integer $idEvent so the current event id
     * @return boolean
     */
    function sendBulkInvite($idEvent = false)
    {
        if ($this->input->post("chkstatus")) {
            $chkstatus = $this->input->post("chkstatus");
        }
        else {
            $chkstatus [] = $this->input->post("idPerformer");
            $idEvent = $this->input->post("idEvent");
        }
        // $bulk_event_invitation_table_length = $this->input->post("bulk_event_invitation_table_length");
        // $BulkInvite = $this->input->post("BulkInvite");
        
        foreach ($chkstatus as $idPerformer) {
            $idPerformer = $this->utility->decode($idPerformer);
            $data ["select"] = [
                "idRegister",
                "status"
            ];
            $data ["where"] = [
                "idEvent" => $idEvent,
                "idPerformer" => $idPerformer
            ];
            $data ["table"] = TABLE_REGISTER;
            $r = $this->selectRecords($data);
            if (count($r) == 0) {
                $dtAdded = time();
                unset($data);
                $data ["insert"] ["idEvent"] = $idEvent;
                $data ["insert"] ["idPerformer"] = $idPerformer;
                $data ["insert"] ["status"] = 'Pending';
                $data ["insert"] ["noticePerformer"] = 1;
                $data ["insert"] ["dtAdded"] = $dtAdded;
                $data ["insert"] ["RequestedByPerformer"] = 0;
                $data ["table"] = TABLE_REGISTER;
                $NewId = $this->insertRecord($data);
                unset($data);
                
                $event = $this->getEventBySchuduleId($idEvent);
                // dd($event);
                
                /* jS M, Y */
                $eventDateEmail = date("Y-m-d H:i:s", strtotime($event->date . " " . $event->startTime));
                $eventDateEmail = convertFromGMT($eventDateEmail, "default", $event->timeZone);
                $timezoneAbbriviation = $this->utility->getAbbreviationOfTimezone($event->timeZone); //get abbriviation of timezone
                $eventDateEmail = date("jS M, Y", strtotime($eventDateEmail)) . " (" . $timezoneAbbriviation . ")";
                $this->load->model($this->myvalues->profileDetails ["model"], "profile_model");
                
                $email = "You just received a booking request from &quot;" . ucwords($this->profile_model->getUserInformation($event->idVenue)->chrName) . "&quot; to perform at &quot;" . $event->title . "&quot; on " . $eventDateEmail;
                $link = '<a href="' . SITEURL . $this->myvalues->invitationDetails ['controller'] . "/received" . '" >Click Here</a> to ';
                
                $this->utility->notification_email($idPerformer, $email, false, $link, "Booking Invitation Received");
                
                $subject = "App Notification: Invitation Received!";
                $msg = "You have been invited to perform at an upcoming event! Please check your StageGator Manage Booking Requests Tab!";
                
                /* Saves message in databse using common function */
                $this->main_model->saveMessage($idPerformer, 810, $subject, $msg);
            }
            else {
                unset($data);
                if ($r [0]->status == "Applied") {
                    $data ["where"] = [
                        "idEvent" => $idEvent,
                        "idPerformer" => $idPerformer
                    ];
                    $data ["update"] ["status"] = 'Accepted';
                    $data ["table"] = "register";
                    $this->updateRecords($data);
                    
                    $subject = "App Notification: Invitation Accepted!";
                    $msg = "Your request to perform at an upcoming event has been accepted! Please check your StageGator My Shows Tab!";
                    
                    /* Saves message in databse using common function */
                    $this->main_model->saveMessage($idPerformer, 810, $subject, $msg);
                }
                elseif ($r [0]->status != "Accepted") {
                    $data ["where"] = [
                        "idEvent" => $idEvent,
                        "idPerformer" => $idPerformer
                    ];
                    $data ["update"] ["status"] = 'Pending';
                    $data ["update"] ["noticePerformer"] = 1;
                    $data ["update"] ["noticeVenue"] = 0;
                    $data ["update"] ["dtEdited"] = time();
                    $data ["update"] ["RequestedByPerformer"] = 0;
                    $data ["table"] = TABLE_REGISTER;
                    $this->updateRecords($data);
                    
                    $subject = "App Notification: Invitation Received!";
                    $msg = "You have been invited to perform at an upcoming event! Please check your StageGator Manage Booking Requests Tab!";
                    
                    /* Saves message in databse using common function */
                    $this->main_model->saveMessage($idPerformer, 810, $subject, $msg);
                }
            }
        }
        
        unset($data);
        $data ["where"] = [
            "idEvent" => $idEvent
        ];
        $data ["update"] ["recurringNotice"] = 0;
        $data ["table"] = "event";
        $this->updateRecords($data);
        
        return true;
    }

    /**
     * Used for display the perfomer profile listing information with user's upcomig event
     *
     * @param int $idUser
     *
     * @return ArrayObject , $result that contain the info about prefomer with event data
     */
    function get_performer_events($idUser)
    {
        $data ['select'] = [
            'es.table_id as scheduleId',
            'event.idEvent',
            'event.date',
            'event.startTime',
            'event.endDate',
            'es.start_date as schedule_date',
            'es.end_date',
            'event.endTime',
            'event.timeZone',
            'event.title',
            'event.status',
            'event.allowTrading',
            'event.descr',
            'register.isHost',
            'register.updateSlotNoticeToPerformer',
            'register.doList',
            "u.chrName",
            "u.url",
            "tt.table_id"
        ];
        $data ['table'] = TABLE_REGISTER . ' as register';
        $data ["join"] = [
            TABLE_EVENT_SCHEDULES . " as es" => [
                "es.table_id = register.idEvent",
                "INNER"
            ],
            TABLE_EVENT . " as event" => [
                "event.idEvent = es.idEvent",
                "INNER"
            ],
            TABLE_USER . " as u" => [
                "event.idVenue = u.user_id",
                "INNER"
            ],
            TABLE_TIMETABLE . " as tt" => [
                "tt.idEvent = es.table_id AND tt.idPerformer = register.idPerformer",
                "LEFT"
            ]
        ];
        $data ['where'] = [
            'register.idPerformer' => $idUser,
            'register.status' => 'Accepted'
        ];
        $data ['order'] = 'schedule_date';
        
        $result = $this->selectFromJoin($data);
        
        unset($data);
        
        // update the status for not display the scond time new word
        $data ["where"] = [
            "idPerformer" => $idUser
        ];
        $data ["update"] ["updateSlotNoticeToPerformer"] = 0;
        $data ["table"] = TABLE_REGISTER;
        $resultUpdate = $this->updateRecords($data);
        
        return $result;
    }

    /**
     * Used for finding the shows/ events which are applied by perfomer accepted by venue user but renking to venue
     * is
     * remaining by performer after scheduled event complete.
     *
     * @param int $idUser , user id
     *       
     * @return Array $idEvents , that contain the id of eventschedules which are applied by perfomer accepted by
     *         venue
     *         user but renking is remaining to venue user.
     */
    function get_events_pending_for_ranking_by_performer($idUser)
    {
        $idEvents = array();
        
        $data ['select'] = [
            'idEvent'
        ];
        $data ['table'] = TABLE_REGISTER;
        $data ['where'] = [
            'idPerformer' => $idUser,
            'status' => 'Accepted',
            'rank_venue' => NULL
        ];
        $result = $this->selectRecords($data);
        
        foreach ($result as $r) {
            $idEvents [] = $r->idEvent;
        }
        return $idEvents;
    }

    /**
     * Used for event schedule id , which are remainig to do ranking by host user to other perfromer user
     *
     * @param int $idUser, host user id
     *       
     * @return Array $idEvents , that contain the id of eventschedules which are remainig to do ranking by host
     *         user to other perfromer user
     */
    function get_events_panding_ranking_by_host($idUser)
    {
        $idEvents = array();
        
        $data ['select'] = [
            'idEvent'
        ];
        $data ['table'] = TABLE_REGISTER;
        $data ['where'] = [
            'idPerformer' => $idUser,
            'status' => 'Accepted',
            'rank' => NULL,
            'isHost' => 1
        ];
        $result = $this->selectRecords($data);
        
        foreach ($result as $r) {
            $idEvents [] = $r->idEvent;
        }
        return $idEvents;
    }

    /**
     * Used for cancel the application
     *
     * @param int $idUser id of user
     * @param int $idScheduleId, event schedule id
     * @param int $idEvent, event id
     *       
     * @return boolean
     */
    function cancel_application($idUser, $idScheduleId, $idEvent)
    {
        
        // $sql="update register set status='Declined' where idPerformer=$idUser and idEvent=$idEvent";
        // $this->db->query($sql);
        $data ["where"] = [
            "idPerformer" => $idUser,
            "idEvent" => $idScheduleId
        ];
        $data ["update"] ["status"] = 'Rejected';
        $data ["table"] = TABLE_REGISTER;
        $resultUpdate = $this->updateRecords($data);
        
        unset($data);
        
        // $ret = $this->db->affected_rows();
        // $sql = "update timetable set idPerformer=0 where idPerformer=$idUser and idEvent=$idEvent";
        
        $data ["where"] = [
            "idPerformer" => $idUser,
            "idEvent" => $idEvent
        ];
        $data ["update"] ["idPerformer"] = 0;
        $data ["table"] = TABLE_TIMETABLE;
        $resultTimetabelUpdate = $this->updateRecords($data);
        unset($data);
        $data ["select"] = [
            "u.chrName",
            "ev.idVenue"
        ];
        $data ["table"] = TABLE_EVENT_SCHEDULES . ' es';
        $data ["join"] = [
            TABLE_EVENT . ' ev' => [
                "es.idEvent = ev.idEvent",
                "INNER"
            ],
            TABLE_USER . ' u' => [
                "u.user_id = ev.idVenue",
                "INNER"
            ]
        ];
        $data ["where"] = [
            "es.idEvent" => $idEvent
        ];
        $data ["limit"] = 1;
        $getInfo = $this->selectFromJoin($data);
        if (! empty($getInfo)) {
            $subject = "App Notification: Invitation Canceled!";
            $msg = ucwords($getInfo [0]->chrName) . " has cancel show";
            $this->main_model->saveMessage($getInfo [0]->idVenue, $this->pUserId, $subject, $msg);
        }
        return $resultUpdate;
    }

    /**
     * Used for host check all perfomers are choosen or not for time slote in event
     *
     * @param int $idEventSchedule , event scheduleid
     * @param int $idUser id of login user
     * @return boolean
     */
    function host_check_all_performers_chosed_time_slot_for_event($idEventSchedule, $idUser)
    {
        
        // $sql = "SELECT idEvent FROM register where status='Accepted' AND idPerformer=$idUser AND isHost=1 AND
        // doList=1 AND idEvent=".$idEvent;
        $data ["select"] = [
            "idEvent"
        ];
        $data ["where"] = [
            "status" => 'Accepted',
            "idPerformer" => $idUser,
            "isHost" => 1,
            "doList" => 1,
            "idEvent" => $idEventSchedule
        ];
        
        $data ["table"] = TABLE_REGISTER;
        
        $result = $this->selectRecords($data);
        
        if (count($result) > 0) {
            
            // $sql = "select * from register where idEvent=$row->idEvent AND status='Accepted'";
            // $total_performers = $this->db->query($sql)->num_rows();
            
            unset($data);
            $data ["where"] = [
                "status" => 'Accepted',
                "idEvent" => $idEventSchedule
            ];
            $data ["table"] = TABLE_REGISTER;
            
            $total_performers = $this->countRecords($data);
            
            unset($data);
            // $sql = "SELECT * from timetable where idEvent=$row->idEvent AND idPerformer!=0 AND idPerformer!=-1";
            // $total_performers_selected_timeslot = $this->db->query($sql)->num_rows();
            
            $data ["where"] = [
                "idPerformer !=" => 0,
                "idEvent" => $idEventSchedule,
                "idPerformer !=" => - 1
            ];
            
            $data ["table"] = TABLE_REGISTER;
            $total_performers = $this->countRecords($data);
            $total_performers_selected_timeslot = $this->countRecords($data);
            
            if ($total_performers != $total_performers_selected_timeslot) {
                return true;
            }
        }
        return false;
    }

    /**
     * used for check attendance for event & perfomer user to give ranking functionality by performer
     *
     * @param int $idEventschedule
     * @param int $idUser, Performer user id
     * @return count of attendance
     */
    function check_attandance($idEventschedule, $idUser)
    {
        // $sql = "SELECT attandance FROM register where status='Accepted' AND idPerformer=$idUser AND
        // idEvent=$idEvent AND attandance='Present'";
        // $data["select"] = ["attandance"]
        $data ["where"] = [
            "status" => 'Accepted',
            "idPerformer" => $idUser,
            "idEvent" => $idEventschedule,
            "attandance" => 'Present'
        ];
        $data ["table"] = TABLE_REGISTER;
        
        $empty_slots = $this->countRecords($data);
        
        return $empty_slots;
    }

    /**
     * Used for rank the venue user by perfomer after completed the event
     *
     * @param int/float $rating , rating applied by performer
     * @param int $eventSchedule_id , id of event schedule
     * @param int $venue_id , id of venue user
     * @param int $idUser , id of performer
     *       
     * @return
     *
     */
    function rankToVenue($rating, $eventSchedule_id, $venue_id, $idUser)
    {
        
        // $sql = "update register set rank_venue='$ratting' where idEvent='$eventId' and idPerformer=$idUser and
        // status='Accepted'";
        // $this->db->query($sql);
        $data ["where"] = [
            "idEvent" => $eventSchedule_id,
            "idPerformer" => $idUser,
            "status" => 'Accepted'
        ];
        $data ["update"] ["rank_venue"] = $rating;
        $data ["table"] = TABLE_REGISTER;
        $updateResult = $this->updateRecords($data);
        
        unset($data);
        
        // $ret = $this->db->affected_rows();
        // $sql = "select COUNT(*) as total, sum(rank_venue) as rating from register where idEvent='$event_id' and
        // status='Accepted' and rank is not null";
        
        /*
         * $data ['select'] = [
         * 'count(*) as total',
         * 'sum(rank_venue) as rating'
         * ];
         * $data ['where'] = [
         * 'idEvent' => $eventSchedule_id,
         * 'status' => 'Accepted',
         * 'rank != ' => NULL
         * ];
         * $data ['table'] = TABLE_REGISTER;
         * $res = $this->selectRecords($data);
         * $res = $res [0];
         */
        // get the average for venue user
        $data ["select"] = [
            "count(reg.idRegister) as total",
            "sum(reg.rank_venue) as rating"
        ];
        $data ["table"] = TABLE_EVENT . " as ev";
        $data ["where"] = [
            "ev.idVenue" => $venue_id,
            "reg.status" => "Accepted",
            "reg.rank_venue >=" => 0
        ];
        $data ["join"] = [
            TABLE_EVENT_SCHEDULES . " as sc" => [
                "ev.idEvent = sc.idEvent",
                "INNER"
            ],
            TABLE_REGISTER . " as reg" => [
                "reg.idEvent = sc.table_id",
                "INNER"
            ]
        ];
        $res = $this->selectFromJoin($data);
        $res = $res [0];
        
        $new_rank = ($res->rating) / ($res->total);
        $new_rank = round($new_rank, 1, 1);
        
        // $sql = "update user set rating='$new_rank' where user_id=$venue_id";
        unset($data);
        
        $data ["where"] = [
            'user_id' => $venue_id
        ];
        $data ["update"] ["rate"] = $new_rank;
        $data ["table"] = TABLE_USER;
        $this->updateRecords($data);
        
        return $updateResult;
    }
    
    /*
     * public function getAllPerformnerlistBySchedulId($eventScheduleId) { $data ['select'] = [ 'es.table_id as
     * scheduleId', 'event.idEvent', 'event.date', 'event.startTime', 'event.endDate', 'es.start_date as
     * schedule_date', 'es.end_date', 'event.endTime', 'event.timeZone', 'event.title', 'event.status',
     * 'event.allowTrading', 'register.isHost', 'register.updateSlotNoticeToPerformer', 'register.doList',
     * "u.chrName", "u.url", "tt.table_id" ]; $data ['table'] = TABLE_REGISTER . ' as register'; $data ["join"] = [
     * TABLE_EVENT_SCHEDULES . " as es" => [ "es.table_id = register.idEvent", "INNER" ], TABLE_EVENT . " as event"
     * => [ "event.idEvent = es.idEvent", "INNER" ], TABLE_USER . " as u" => [ "event.idVenue = u.user_id", "INNER"
     * ], TABLE_TIMETABLE . " as tt" => [ "tt.idEvent = es.table_id AND tt.idPerformer = register.idPerformer",
     * "LEFT" ] ]; $data ['where'] = [ 'register.idPerformer' => $idUser, 'register.status' => 'Accepted' ]; $data
     * ['order'] = 'date asc'; $result = $this->selectFromJoin($data); }
     */
}

?>
