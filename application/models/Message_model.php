<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Message_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getInboxList($userId)
    {
        
        $data ["select"] = [
            "m.idMsg",
            "m.msgSubject",
            "m.msgBody",
            "m.dtAdded",
            "m.readStatus",
            "u.chrName as sender"
        ];
        $data ["table"] = TABLE_MESSAGES . ' m';
        $data ["join"] = [
            TABLE_USER . "  u" => [
                "u.user_id = m.msgFrom",
                "INNER"
            ]
        ];
        $data ["where"] = [
            "m.msgTo" => $userId,
            /* "m.msgFrom!=" => "810", */
            "m.inboxStatus" => "1"
        ];
        $data ["order"] = "m.idMsg desc";
        $res = $this->selectFromJoin($data);
        
        unset($data);
        $data["where"] = ["msgTo" => $userId , /* "msgFrom!=" => "810" */];
        $data ["update"] ["readStatus"] = 1;
        $data ["table"] = TABLE_MESSAGES;
        $result = $this->updateRecords($data);
        return $res;
    }

    public function getSenList($userId)
    {
        $data ["select"] = [
            "m.idMsg",
            "m.msgSubject",
            "m.msgBody",
            "m.dtAdded",
            "m.readStatus",
            "u.chrName as sentTo"
        ];
        $data ["table"] = TABLE_MESSAGES . ' m';
        $data ["join"] = [
            TABLE_USER . "  u" => [
                "u.user_id = m.msgTo",
                "INNER"
            ]
        ];
        $data ["where"] = [
            "m.msgFrom" => $userId,
            "m.sentStatus" => "1"
        ];
        $data ["order"] = "m.idMsg desc";
        $res = $this->selectFromJoin($data);
        
        return $res;
    }

    public function updateMessageTableStatus($statusFor,$userId)
    {
        $getAllMessage = $this->input->post("chkstatus");
        foreach ($getAllMessage as $value) {
            
            $mainValue = $this->utility->decode($value);
            if (! ctype_digit($mainValue)) {
                return false;
            }
            $checkValue [] = $mainValue;
        }
        $data ["where_in"] = [
            "idMsg" => $checkValue
        ];
        if($statusFor == "sentStatus"){
        $data ["where"] = [
            "msgFrom" => $userId
        ];
        }
        else{
            
        $data ["where"] = [
          "msgTo" => $userId
            ];
            
        }
        
        $data ["update"] [$statusFor] = 0;
        $data ["table"] = TABLE_MESSAGES;
        $result = $this->updateRecords($data);
        
        return true;
    }
}

?>
