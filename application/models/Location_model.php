<?php

class Location_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * getCurrentUserLocations()
     * this method get current user's all locations
     *
     * @param integer is current logged in userid
     * @return object is return object result of current all user's
     */
    public function getCurrentUserLocations($userId)
    {
        $data ["select"] = [
            "location_id",
            "location_title",
            "address",
            "city",
            "state",
            "country",
            "phone",
            "numSeats"
        ];
        $data ["where"] = [
            "venue_id" => $userId
        ];
        $data ["table"] = TABLE_LOCATIONS;
        return $this->selectRecords($data);
    }

    /**
     * getCurrentUserLocationsById()
     * get location by location id
     *
     * @param integer is current logged in userid
     * @return object is return object result of current all user's
     */
    public function getCurrentUserLocationsById($locationId)
    {
        
        $data ["select"] = [
            "location_id",
            "location_title",
            "address",
            "city",
            "state",
            "country",
            "phone",
            "numSeats"
        ];
        $data ["where"] = [
            "location_id" => $locationId
        ];
        $data ["table"] = TABLE_LOCATIONS;
        return $this->selectRecords($data);
    }

    /**
     * addUserLocation()
     * This method add a location and update lat of correspond address
     *
     * @param integer $userId is the current logged in user's user id
     * @return boolean return true or false
     */
    public function saveLocation($id = false, $userId)
    {
        $txtTitle = $this->utility->encodeText($this->input->post('txtTitle'));
        $txtPhone = $this->utility->encodeText($this->input->post('txtPhone'));
        $cmbCountry = $this->utility->encodeText($this->input->post('cmbCountry'));
        $cmbState = $this->utility->encodeText($this->input->post('cmbState'));
        $cmbCity = $this->utility->encodeText($this->input->post('cmbCity'));
        $txtAddress = $this->utility->encodeText($this->input->post('txtAddress'));
        $txtNumSeats = $this->utility->encodeText($this->input->post('txtNumSeats'));
        if (empty($id)) {
            $data ["insert"] ["venue_id"] = $userId;
            $data ["insert"] ["location_title"] = $txtTitle;
            $data ["insert"] ["address"] = $txtAddress;
            $data ["insert"] ["city"] = $cmbCity;
            $data ["insert"] ["state"] = $cmbState;
            $data ["insert"] ["country"] = $cmbCountry;
            $data ["insert"] ["phone"] = $txtPhone;
            $data ["insert"] ["numSeats"] = $txtNumSeats;
            $data ["table"] = TABLE_LOCATIONS;
            $id = $this->insertRecord($data);
        }
        else {
            
            $data ["where"] = [
                "location_id" => $id,
                "venue_id" => $userId
            ];
            $data ["update"] ["venue_id"] = $userId;
            $data ["update"] ["location_title"] = $txtTitle;
            $data ["update"] ["address"] = $txtAddress;
            $data ["update"] ["city"] = $cmbCity;
            $data ["update"] ["state"] = $cmbState;
            $data ["update"] ["country"] = $cmbCountry;
            $data ["update"] ["phone"] = $txtPhone;
            $data ["update"] ["numSeats"] = $txtNumSeats;
            $data ["table"] = TABLE_LOCATIONS;
            $this->updateRecords($data);
        }
        // replace all the white space with "+" sign to match with google search pattern
        $address = str_replace(" ", "+", $txtAddress);
        $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=$address";
        $response = file_get_contents($url);
        $json = json_decode($response, TRUE);
        
        // generate array object from the response from the web
        
        if(isset($json ["results"] [0]))
        {
            $latitude = $json ["results"] [0] ["geometry"] ["location"] ["lat"];
            $latitude = chop($latitude, ",");
            $longitude = $json ["results"] [0] ["geometry"] ["location"] ["lng"];
        
            if ((! empty($latitude) && ! empty($longitude)) && (isset($latitude) && isset($longitude))) {
                
                $data ["where"] = [
                    "location_id" => $id
                ];
                $data ["update"] ["latitude"] = $latitude;
                $data ["update"] ["longitude"] = $longitude;
                $data ["table"] = TABLE_LOCATIONS;
                $this->updateRecords($data);
            }
        }
        
        return $id;
    }

    /**
     * deleteUserLocation()
     * This method delete location from table
     *
     * @param integer location id
     * @param integer current logged in userid
     */
    function deleteUserLocation($id, $userId)
    {
        $data ["where"] = [
            "location_id" => $id,
            "venue_id" => $userId
        ];
        $data ["table"] = TABLE_LOCATIONS;
        $this->deleteRecords($data);
    }
}

?>
