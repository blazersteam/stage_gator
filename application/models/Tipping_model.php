<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tipping_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * getCorrespondsCountry()
     * This method return countries based on stripe countries Iso code
     *
     * @param Array $getISO is array of iso code
     * @return Object $res is countris array
     */
    public function getCorrespondsCountry($getISO)
    {
        $data ["select"] = [
            "countries_iso_code_2",
            "countries_name"
        ];
        $data ["where_in"] = [
            "countries_iso_code_2" => $getISO
        ];
        $data ["table"] = TABLE_COUNTRIES;
        $res = $this->selectRecords($data);
        return $res;
    }

    /**
     * genrateStripeAccount()
     * this method add new account in stripe we are getting here only country,email,response feild
     *
     * @return either true or return error message comes from stripe
     */
    public function genrateStripeAccount()
    {
        /*
         * before create an account we check customer id if this customer already exists or not if this is an
         * existing customer from stripe we have a account id if get
         * account id then return to controller no need to create a new account for it
         */
        $res = $this->getStripeAccountId();
        /* if this user's have already external accounts then return here to controller */
        if ($res) {
            return true;
        }
        $country = $this->utility->encodeText($this->input->post('CmbCountries'));
        $email = $this->session->userdata("UserData")->user_name;
        $response = createStripeAccount($country, $email);
        /* create a stripe account and if get status true then insert data in stripe_account_details */
        if ($response ['status']) {
            $data ["insert"] ["stripe_account_id"] = $response ['accountId'];
            $data ["insert"] ["stripe_data"] = '';
            $data ["insert"] ["user_id"] = $this->pUserId;
            $data ["insert"] ["created_datetime"] = $this->datetime;
            $data ["insert"] ["v_ip"] = $this->ip;
            $data ["insert"] ["v_secret_key"] = $response ['secret_key'];
            $data ["insert"] ["v_publishable_key"] = $response ['publishable_key'];
            $data ['table'] = TABLE_STRIPE_ACCOUNT_DETAILS;
            $this->insertRecord($data);
            unset($data);
            /* insert stripe account id in user table also */
            $data ['update'] ['stripe_account_id'] = $response ['accountId'];
            $data ["where"] = [
                "user_id" => $this->pUserId
            ];
            $data ["table"] = TABLE_USER;
            $this->updateRecords($data);
            changeStripeAutoTransfer($response ['accountId'], "manual");
            
            //Update tipRegistration Session variable After strip account created
            if($this->pidGroup == PERFOMER_GROUP){
                $userData = $this->session->userdata("UserData");
                $userData->tipRegistration = 1;
                $userData = $this->session->set_userdata("UserData", $userData);
            }
            
            return true;
        }
        else {
            return $response ['message'];
        }
    }

    /**
     * addExernalAccounts()
     * This method add a new external account in stripe getting required fields form form and create a new external
     * account
     *
     * @return either boolean true or stripe response if get any error from stripe
     */
    public function addExernalAccounts()
    {
        $postData = $this->input->post();
        
        $res = $this->getStripeAccountId();
        
        // pr($postData);die;
        
        /* checking if a card token genrated by stripe.js if get a token then create a new card in stripe */
        if (! empty($postData ['stripeToken'])) {
            $cardResponse = createStripeCardAccount($res, trim($postData ['stripeToken']));
            /* if card is not created send an error message back to controller */
            if (empty($cardResponse ['status'])) {
                
                return $cardResponse ['message'];
            }
        }
        /* if get bank information then create a new bank accountin stripe */
        if (! empty($postData ['account'])) {
            
            $accountId = $res;
            $accHolderName = $postData ['account_holder_name'];
            $bankAccNo = $postData ['account'];
            $country = $postData ['CmbCountries'];
            $currency = $postData ['currency'];
            $routingNo = $postData ['routing_number'];
            $bankResponse = createStripeBankAccount($accountId, $accHolderName, $bankAccNo, $country, $currency, $routingNo);
            /* if stripe response is null then return to controller with error message */
            if (empty($bankResponse ['status'])) {
                
                return $bankResponse ['message'];
            }
        }
        /* after creating card and bank accounts return success to controller */
        return true;
    }

    /**
     * updateStripeAccount
     * this method called when update a stripe account
     *
     * @return return response to controller
     */
    public function updateStripeAccount()
    {
        $postData = $this->input->post();
        /* call to upload verification document of user and get response its uploaded or not */
        $imageName = $this->uploadImage();
        /* get curernt user account id */
        $res = $this->getStripeAccountId();
        /* if image not uploaded return to controller from here */
        if (isset($imageName ["error"])) {
            return $imageName ["error"];
        }
        /* if get account id then proccess to update */
        if ($res) {
            
            $postData ['update'] ["tos_acceptance__date"] = strtotime($this->datetime);
            $postData ['update'] ["tos_acceptance__ip"] = $this->ip;
            $uploadResponse = uploadStripeFile($res, $imageName ['fileName']);
            /*
             * stripe image upload response if not uploaded on stripe send back to controller otherwise get image
             * id
             */
            if (empty($uploadResponse ['message'] ['id'])) {
                return $uploadResponse ['message'];
            }
            $postData ['update'] ["legal_entity__verification__document"] = $uploadResponse ['message'] ['id'];
            $response = updateStripeAccountData($res, $postData ['update']);
            /* if get response true from stripe then update data in user stripe details */
            if (! empty($response ['status'])) {
                $data ['update'] ['stripe_data'] = json_encode($postData);
                $data ['where'] = [
                    'user_id' => $this->pUserId
                ];
                $data ['table'] = TABLE_STRIPE_ACCOUNT_DETAILS;
                $this->updateRecords($data);
            }
            
            return $response ['message'];
        }
    }

    /**
     * getStripeAccountId
     * This method get logged in user's account id
     *
     * @return boolean
     */
    public function getStripeAccountId()
    {
        $data ['select'] = [
            'stripe_account_id'
        ];
        $data ['where'] = [
            'user_id' => $this->pUserId
        ];
        $data ['table'] = TABLE_STRIPE_ACCOUNT_DETAILS;
        $res = $this->selectRecords($data);
        
        return count($res) > 0 ? $res [0]->stripe_account_id : false;
    }

    /**
     * updateprofile()
     * this method upload verification document on stripe
     *
     * @return return to view
     */
    public function uploadImage()
    {
        // get user information
        $imgName = $this->pidGroup . '_' . $this->pUserId . '_' . date('Ymdhis') . '_' . rand(1000, 100000);
        
        $config ['upload_path'] = 'external/uploads/stripe';
        $config ['allowed_types'] = 'jpg|png|jpeg';
        $config ['file_name'] = $imgName;
        $this->load->library('upload', $config);
        
        if ($this->upload->do_upload("file_0")) {
            $image = $this->upload->data();
            
            return [
                "fileName" => $image ['full_path']
            ];
            // deleting old profile from file system
            // @unlink(EXTERNAL_PATH."images/profiles/".$image["file_name"]);
        }
        else {
            
            return [
                "error" => $this->upload->display_errors()
            ];
        }
    }

    /**
     * This function is used for checking if login user(fan) have stripe id & other data
     *
     * @param $userId , id of login(fan) user id
     * @return $res , that contain the emailid of user else false
     */
    public function checkStripeIdUser($userId)
    {
        $data ['select'] = [
            'stripe_customer_id',
            'user_name'
        ];
        $data ['where'] = [
            'user_id' => $userId
        ];
        $data ['table'] = TABLE_USER;
        $res = $this->selectRecords($data);
        
        if (empty($res [0]->stripe_customer_id)) {
            return $res [0]->user_name;
        }
        else {
            return true;
        }
        
        // return count($res) > 0 ? $res [0]->stripe_account_id : false;
    }

    /**
     * Used for update the stripe id based on user id in user table
     *
     * @param string $stripeId , stripe id
     *       
     * @param int $userid , id of user
     */
    public function updateStripCustomerId($stripeId, $userid)
    {
        $data ["where"] = [
            "user_id" => $userid
        ];
        $data ["update"] ["stripe_customer_id"] = $stripeId;
        $data ["table"] = TABLE_USER;
        return $this->updateRecords($data);
    }

    /**
     * getCountries
     * This method returns all countries of stripe
     */
    public function getCountries()
    {
        $allCoutries = getSuppertedCountries();
        $data ["select"] = [
            "countries_iso_code_2",
            "countries_name"
        ];
        $data ["where_in"] = [
            "countries_iso_code_2" => $allCoutries
        ];
        $data ["table"] = TABLE_COUNTRIES;
        $res = $this->selectRecords($data);
        
        return $res;
    }

    /**
     * Used for make the log in tiipping history table
     *
     * @param int $fromUserId, from id of user who give the tip (Fan)
     * @param int $toUserId , to id of user who receive the tip (Performer)
     * @param int $amount , tip amount
     * @param string $txn_id , transaction id of sucessfully giving tip
     *       
     * @return $result, last insered id.
     */
    public function makeTipplingHistoryLog($fromUserId, $toUserId, $amount, $txn_id, $stripeStatus)
    {
        $data ["insert"] ["from_user_id"] = $fromUserId;
        $data ["insert"] ["to_user_id"] = $toUserId;
        $data ["insert"] ["amount"] = $amount;
        $data ["insert"] ["txn_id"] = $txn_id;
        $data ["insert"] ["stripe_status"] = $stripeStatus;
        $data ["insert"] ["dt_created_datetime"] = date("Y-m-d H:i:s");
        $data ["insert"] ["v_ip"] = $this->input->ip_address();
        $data ["table"] = TABLE_TIPPING_HISTORY;
        $result = $this->insertRecord($data);

        return $result;
    }

    /**
     * Used for show the list of tip payment history
     *
     * @param int $userId, id of login user(fan)
     */
    public function getTipHistory($userId)
    {
        $data ['select'] = [
            'tipHistory.from_user_id',
            'tipHistory.to_user_id',
            'tipHistory.amount',
            'tipHistory.dt_created_datetime',
            'tipHistory.txn_id',
            'user.chrName',
            'user.url'
        ];
        $data ['where'] = [
            "from_user_id" => $userId
        ];
        $data ["join"] = [
            TABLE_USER . " as user" => [
                "user.user_id = tipHistory.to_user_id",
                "INNER"
            ]
        ];
        $data ['table'] = TABLE_TIPPING_HISTORY . " as tipHistory";
        $data ['order'] = 'tipHistory.dt_created_datetime desc';
        $data ['limit'] = 100;
        
        $result = $this->selectFromJoin($data);
        return $result;
    }

    /**
     * Used for get the user's stripe Account secret key.
     *
     * @param int $userId , id of user
     *       
     * @return string $result, that contain the strip accont secret key.
     */
    public function getStripeKey($userId)
    {
        $data ['select'] = [
            'v_secret_key'
        ];
        $data ['where'] = [
            'user_id' => $userId
        ];
        $data ['table'] = TABLE_STRIPE_ACCOUNT_DETAILS;
        $result = $this->selectRecords($data);
        return isset($result [0]->v_secret_key) ? $result [0]->v_secret_key : false;
    }

    /**
     * saveWithdrawalHistory()
     * This method will menage withdrawal history of current user whenever user withdrawal his money
     * this method update status in withdrawal table
     *
     * @param array $response return response after withdrowling money from stripe
     *       
     * @return return true after inserting
     */
    public function saveWithdrawalHistory($response)
    {
        $txtAmount = $this->input->post('txtAmount');
        $data ['insert'] ['dt_created_datetime'] = $this->datetime;
        $data ['insert'] ['v_ip'] = $this->ip;
        $data ['insert'] ['user_id'] = $this->pUserId;
        $data ['insert'] ['amount'] = $txtAmount;
        $data ['insert'] ['request_datetime'] = $this->datetime;
        $data ['insert'] ['withdrawal_status'] = $response ['transfer_status'];
        $data ['insert'] ['withdrawal_id'] = $response ['txnId'];
        $data ['table'] = TABLE_WITHDRAWAL_HISTORY;
        $this->insertRecord($data);
        
        return true;
    }

    /**
     * Used for delete the stripe account related data& delete the stripe account.
     *
     * @param int $userId, user id of user
     * 
     * @return boolean
     */
    public function deleteStripeAccount($userId)
    {
        // fetch the account id of related user id
        $data ['select'] = [
            'stripe_account_id'
        ];
        $data ['where'] = [
            'user_id' => $userId
        ];
        $data ['table'] = TABLE_USER;
        $resultUser = $this->selectRecords($data);
        
        if (! empty($resultUser)) {
            // make the request for delete stripe account by stripe helper loading
            $this->load->helper('stripe_connect_helper');
            
            $resultDeleteAcct = deleteStripeAccount($resultUser [0]->stripe_account_id);
            
            if ($resultDeleteAcct ['status'] == 'true') {
                
                // blank the stripe accountid & status = na
                unset($data);
                $data ["update"] ["stripe_account_id"] = "";
                $data ["update"] ["stripe_status"] = "NA";
                $data ["where"] = [
                    "user_id" => $userId
                ];
                $data ["table"] = TABLE_USER;
                $this->updateRecords($data);
                
                // delete the related user data from stripe account detail tabel
                unset($data);
                $data ["where"] = [
                    "user_id" => $userId
                ];
                $data ["table"] = TABLE_STRIPE_ACCOUNT_DETAILS;
                $resultDelete = $this->deleteRecords($data);
                
                return $resultDelete;
            }
        }
    }

    /**
     * Used for check if user have already registered the bank account with enterd data, check if entered bank
     * account data are already registered or not, used by performer only.
     * called from Tipping controller, addBankAccount method
     *
     * @param string $accountId, user's stripe account id
     * @param string $bankAccNo, user's entered Bank account no from form.
     * @param string $routingNo, user's entered routing No from form.
     *       
     * @return array $responce, that conatin the status, that is true if duplicate or found error while fetch stripe data
     *         else return false
     */
    public function checkDuplicateBankAccount($accountId, $bankAccNo, $routingNo)
    {
        // load the stripe helper
        $this->load->helper('stripe_connect_helper');
        $resultData = getStripeExternalAccounts($accountId);
        
        $responce = array();
        
        if ($resultData ['status'] == 1) {
            foreach ($resultData ['message'] ['banks'] as $bankdata) {
                if ($bankdata ['last4'] == substr($bankAccNo, - 4) && $bankdata ['routing_number'] == $routingNo) {
                    $responce ['status'] = true;
                    $responce ['message'] = 'This bank account already exist.';
                    return $responce;
                    exit();
                }
            }
            // not found duplicate
            $responce ['status'] = false;
            $responce ['message'] = '';
            return $responce;
        }
        else {
            
            $responce ['status'] = true;
            $responce ['message'] = $resultData ['message'];
            return $responce;
        }
    }

    /**
     * Used for check if user have already registered the debit card with enterd data, check if entered debit card
     * data are already registered or not,
     * used by performer only.called from Tipping controller, addNewCard method
     *
     * @param string $accountId, user's stripe account id
     * @param int $expMonthVar, user's entered debit card's exp. month from form.
     * @param int $expYearVar, user's entered debit card's exp. year from form.
     * @param int $cardNumber, user's entered debit card no from form.
     * 
     * @return array $responce, that conatin the status, that is true if duplicate or found error while fetch stripe data
     *         else return false
     */
    public function checkDuplicateDebitCard($accountId, $expMonthVar, $expYearVar, $cardNumber)
    {
        // load the stripe helper
        $this->load->helper('stripe_connect_helper');
        $resultData = getStripeExternalAccounts($accountId);       
        $responce = array();
        if ($resultData ['status'] == 1) {
            foreach ($resultData ['message'] ['cards'] as $carddata) {
                if ($carddata ['last4'] == substr($cardNumber, - 4) && $carddata ['exp_month'] == $expMonthVar && $carddata ['exp_year'] == $expYearVar ) {
                    $responce ['status'] = true;
                    $responce ['message'] = 'This debit card already exist.';
                    return $responce;
                    exit();
                }
            }
            // not found duplicate
            $responce ['status'] = false;
            $responce ['message'] = '';
            return $responce;
        }
        else {
        
            $responce ['status'] = true;
            $responce ['message'] = $resultData ['message'];
            return $responce;
        }
    }
    
    
    /**
     * saveRecurringTipInfo()
     * This method will menage recurring tip info of current user whenever user Subscribe plan from strip account
     *
     * @param int $fanId, fan's Id
     * @param int $performerId, Performer's Id
     * @param string $recurringType, Recurring Type fopr recurring process
     * @param float $amount, amount of tip
     * @param string $tipsType, Type of tip between this two option 'recurring' or 'onetime'
     * @param string $token, strip account token key
     *
     * @return return true after inserting
     */
    public function saveRecurringTipInfo($fanId, $performerId, $amount, $tipsType, $recurringType, $token)
    {
        $oldRecurringTipInfo = $this->getRecurringTipInfo($fanId, $performerId);
        
        if(!empty($oldRecurringTipInfo)){
            if($oldRecurringTipInfo[0]->recurringType != $recurringType || ($oldRecurringTipInfo[0]->recurringType == $recurringType && $oldRecurringTipInfo[0]->amount != $amount)){
                $data ["where"] = [
                    "recurring_id" => $oldRecurringTipInfo[0]->recurring_id
                ];
                $data ['update'] ['recurring_status'] = 'Cancelled';
                $data ["update"] ["updated_date"] = date("Y-m-d H:i:s");
                $data ['table'] = TABLE_RECURRING_TIP_INFO;
                $this->updateRecords($data);
            }else{
                return [
                    "status" => true,
                    "message" => ""
                ];
            }
        }
        
        unset($data);
        
        $data ['insert'] ['performer_id'] = $performerId;
        $data ['insert'] ['fans_id'] = $fanId;
        $data ['insert'] ['amount'] = $amount;
        $data ['insert'] ['tipsType'] = $tipsType;
        $data ['insert'] ['recurringType'] = $recurringType;
        $data ['insert'] ['v_ip'] = $this->ip;
        $data ["insert"] ["recurring_status"] = 'Active';
        $data ["insert"] ["created_date"] = date("Y-m-d H:i:s");
        $data ['table'] = TABLE_RECURRING_TIP_INFO;
        
        $recurringInfoId = $this->insertRecord($data);
        
        if($recurringInfoId){
            //UniqueTipId id gernate for recurring inforamation
            $UniqueTipId = $fanId.'-'.$performerId.'-'.$recurringType.'-'.$amount.'-'.$recurringInfoId;
            
            $data ["where"] = [
                "recurring_id" => $recurringInfoId
            ];
            $data ['update'] ['uniqueTipId'] = $UniqueTipId;
            $data ["update"] ["recurring_status"] = 'Active';
            $data ['table'] = TABLE_RECURRING_TIP_INFO;
            $this->updateRecords($data);
        }
        
        //create customer for performers
        $description = "Stagegator fan : " . $this->pUserId;
        $FanDetail = $this->getFanInfo($this->pUserId);
        $email = $FanDetail[0]->user_name;
        $stripsecretkey = $this->getStripeKey($performerId);
        //metadata - put some basic require inforemation to metadata
        $metadata = array('fanId'=> $fanId, 'performerId'=> $performerId, 'plan_id'=>$UniqueTipId);
        //Plan Interval Array
        //$planIntervalArr = array('weekly'=>'week','monthly'=>'month');
        $planIntervalArr = array('weekly'=>'day','monthly'=>'day');
        // round the application fee for stripe, also added the additional charges accrding to plan
        $this->load->model($this->myvalues->subscriptionDetails ['model'], 'subscriptionModel');
        $planid = $this->subscriptionModel->getCurrentPlanDetails($performerId, 3);
        $applicationFee = round(($planid [0]->tips_percentage * $amount) / 100 + $planid [0]->tips_additional, 2);
        $applicationFee = round($applicationFee/$amount * 100,2);

        //get Strip Customer Info from 'fans_performer_sxtrip_info' for check duplication
        $fansPerformerStripCustomerInfo = $this->getFanPerformerStripInfo($fanId , $performerId);
        
        $checkStripCustomer = [ "status" => false, "message" => "" ];
        $stripCustomerId = '';
        
        if(!empty($fansPerformerStripCustomerInfo) && $fansPerformerStripCustomerInfo[0]->stripe_customer_id != ""){
            
            $stripCustomerId = $fansPerformerStripCustomerInfo[0]->stripe_customer_id;
            //Check Customer Exit or Not In Helper Function (Strip)
            $checkStripCustomer = getStripeCustomer($stripCustomerId, $stripsecretkey);
        }
        
        
        if($checkStripCustomer ['status'] != true){
            
            //Customer Create Helper Function (Strip)
            $stripCustomerResponce = createStripeCustomerForPerformer($email, $description, $token, '',$stripsecretkey);
            
            if($stripCustomerResponce['status']){
                
                unset($data);
                //Insert Record New Created Strip Customer Info ( CustomerID log ) 
                $data ['insert'] ['performer_id'] = $performerId;
                $data ['insert'] ['fans_id'] = $fanId;
                $data ['insert'] ['stripe_customer_id'] = $stripCustomerResponce['customerId'];
                $data ['insert'] ['v_ip'] = $this->ip;
                $data ["insert"] ["stripe_info_status"] = 'Active';
                $data ["insert"] ["created_date"] = date("Y-m-d H:i:s");
                $data ['table'] = TABLE_FAN_PERFORMER_STRIP_INFO;
                
                //Insert Query
                $performerCustomerInfo = $this->insertRecord($data);
                
                //Create Plan For This Performer
                if(array_key_exists($recurringType, $planIntervalArr)){
                    
                    //Plane Create Helper Function (Strip)
                    $stripPlanResponce = createPlanForConnectedAccount($stripsecretkey, $UniqueTipId, $planIntervalArr[$recurringType], $amount, $metadata);
                    
                    if($stripPlanResponce['status']){
                        
                        //Subscription Created Plan Helper Function (Strip)
                        $subscriptionToPlanResponce = subscriptionToPlan($stripsecretkey, $UniqueTipId, $stripCustomerResponce['customerId'], $applicationFee, $metadata);
                        if(empty($subscriptionToPlanResponce['status'])){
                            return [
                                "status" => false,
                                "message" => $this->lang->line("error_strip_subscription_plan")
                            ];
                        }
                        //add subscription id in reurring tip info
                        if($subscriptionToPlanResponce['subscriptionId']){
                            
                            unset($data);
                            
                            $data1 ["where"] = [
                                "recurring_id" => $recurringInfoId,
                                "recurring_status" => 'Active',
                                "UniqueTipId" => $UniqueTipId
                            ];
                            $data1 ['update'] ['subscriptionId'] = $subscriptionToPlanResponce['subscriptionId'];
                            $data1 ['table'] = TABLE_RECURRING_TIP_INFO;
                            $this->updateRecords($data1);
                            
                        }
                    }else{
                        return [
                            "status" => false,
                            "message" => $this->lang->line("error_strip_create_plan")
                        ];
                    }
                }
            }else{
                return [
                    "status" => false,
                    "message" => $this->lang->line("error_strip_create_customer")
                ];
            }
            
        }else{
            
            
            //Create Plan For This Performer
            if(array_key_exists($recurringType, $planIntervalArr)){
                
                if(!empty($oldRecurringTipInfo)){
                    //Cancel previous subscription plan befor createing new plan
                    $cancelStripeSubscriptionStatus = cancelStripeSubscription($oldRecurringTipInfo[0]->subscriptionId, $stripsecretkey);
                }
                
                //Plane Create Helper Function (Strip)
                $stripPlanResponce = createPlanForConnectedAccount($stripsecretkey, $UniqueTipId, $planIntervalArr[$recurringType], $amount, $metadata);
                
                if($stripPlanResponce['status']){
                    
                    //Subscription Created Plan Helper Function (Strip)
                    $subscriptionToPlanResponce = subscriptionToPlan($stripsecretkey, $UniqueTipId, $stripCustomerId, $applicationFee, $metadata);
                    if(empty($subscriptionToPlanResponce['status'])){
                        return [
                            "status" => false,
                            "message" => $this->lang->line("error_strip_subscription_plan")
                        ];
                    }
                    //add subscription id in reurring tip info
                    if($subscriptionToPlanResponce['subscriptionId']){
                        
                        unset($data);
                        
                        $data1 ["where"] = [
                            "recurring_id" => $recurringInfoId,
                            "UniqueTipId" => $UniqueTipId
                        ];
                        $data1 ['update'] ['subscriptionId'] = $subscriptionToPlanResponce['subscriptionId'];
                        $data1 ['table'] = TABLE_RECURRING_TIP_INFO;
                        $this->updateRecords($data1);
                        
                    }
                }else{
                    return [
                        "status" => false,
                        "message" => $this->lang->line("error_strip_create_plan")
                    ];
                }
            }
        }
        
        return [
            "status" => true,
            "message" => ""
        ];
    }
    
    
    /**
     * getRecurringTipInfo()
     * This method will get recurring tip info of based on current user and which of performer they want to give tip
     *
     * @param int $fanId, fan's Id
     * @param int $performerId, Performer's Id
     *
     * @return return data(Array)
     */
    public function getRecurringTipInfo($fanId, $performerId)
    {
        $data ["select"] = ['*'];
        $data ["where_in"] = [
            "recurring_status" => 'Active',
            "fans_id" => $fanId,
            "performer_id" => $performerId
        ];
        
        $data ["table"] = TABLE_RECURRING_TIP_INFO;
        $res = $this->selectRecords($data);
        
        return $res;
    }
    
    /**
     * getFanInfo()
     * This method will get recurring tip info of based on current user and which of performer they want to give tip
     *
     * @param int $fanId, fan's Id
     * @param int $performerId, Performer's Id
     *
     * @return return data(Array)
     */
    public function getFanInfo($fanId)
    {
        $data ["select"] = ['*'];
        $data ["where"] = [
            "user_id" => $fanId
        ];
        $data ["table"] = TABLE_USER;
        $res = $this->selectRecords($data);
        
        return $res;
    }
    
    /**
     * getFanPerformerStripInfo()
     * This method will get recurring tip info of based on current user and which of performer they want to give tip
     *
     * @param int $fanId, fan's Id
     * @param int $performerId, Performer's Id
     *
     * @return return data(Array)
     */
    public function getFanPerformerStripInfo($fanId, $performerId)
    {
        $data ["select"] = ['*'];
        $data ["where"] = [
            "performer_id" => $performerId,
            "fans_id" => $fanId,
            "stripe_info_status" => 'Active'
        ];
        $data ["table"] = TABLE_FAN_PERFORMER_STRIP_INFO;
        $res = $this->selectRecords($data);
        
        return $res;
    }
    
    /**
     * Used for show the list of tip Recurring history
     *
     * @param int $userId, id of login user(fan)
     */
    public function getEnabledRecurringTip($userId)
    {
        $data ['select'] = [
            'recurringTip.recurring_id',
            'recurringTip.fans_id',
            'recurringTip.performer_id',
            'recurringTip.amount',
            'recurringTip.created_date',
            'recurringTip.uniqueTipId',
            'recurringTip.recurringType',
            'recurringTip.recurring_status',
            'recurringTip.subscriptionId',
            'user.chrName',
            'user.url'
        ];
        $data ['where'] = [
            "fans_id" => $userId
        ];
        $data ["join"] = [
            TABLE_USER . " as user" => [
                "user.user_id = recurringTip.performer_id",
                "INNER"
            ]
        ];
        $data ['table'] = TABLE_RECURRING_TIP_INFO. " as recurringTip";
        $data ['order'] = 'recurringTip.created_date desc';
        $data ['limit'] = 100;
        
        $result = $this->selectFromJoin($data);
        return $result;
    }
    
    /**
     * Used for show the list of tip Recurring history
     *
     * @param int $userId, id of login user(fan)
     */
    public function updateStripeSubscriptionStatus($recurringId, $performerId, $subscriptionId)
    {
        //get Strip Secret Key
        $stripsecretkey = $this->getStripeKey($performerId);
        
        //Cancel previous subscription plan befor createing new plan
        $cancelStripeSubscriptionStatus = cancelStripeSubscription($subscriptionId, $stripsecretkey);
        
        if($cancelStripeSubscriptionStatus['status']){
            
            $data ['update'] ['recurring_status'] = 'Cancelled';
            $data ['update'] ['updated_date'] = date("Y-m-d H:i:s");
            
            $data ['where'] = [
                'recurring_id' => $recurringId
            ];
            $data ['table'] = TABLE_RECURRING_TIP_INFO;
            $res = $this->updateRecords($data);
            
            if($res){
                return true;
            }else{
                return false;
            }
        }
    }
   
}  

