<?php

class Change_password_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * getUserInformation()
     * This method contain user information from user,members and style table
     *
     * @param unknown $userId
     */
    public function getUserInformation($userId)
    {
        $data ["select"] = [
            'u.user_id',
            'u.image',
            'u.user_name',
            'u.idGrp',
            'u.chrName',
            'u.descr',
            'mem.mem_address',
            'u.contact_person',
            'mem.mem_phone',
            'mem.mem_city',
            'mem.countries_id',
            'mem.stateAbbrev',
            'u.rate',
            'u.url',
            'u.mem_id'
        ];
        $data ["table"] = TABLE_USER . " as u";
        $data ["join"] = [
            TABLE_MEMBERS . " as mem" => [
                "mem.mem_id = u.mem_id",
                "LEFT"
            ]
        ];
    
        $data ["where"] = [
    
            "u.user_id" => $userId
        ];
        $res = $this->selectFromJoin($data);
    
        return $res [0];
    }
    
    
    /**
     * updateUserEmail()
     * this method update current user email in usertable
     *
     * @param integer $userId is current logged in user user_id
     * @return array is the message array
     */
    function updateUserEmail($userId)
    {
        $email = $this->utility->encodeText($this->input->post('txtNew_email'));
        $data ["where"] = [
            "user_name" => $email,
            "user_id !=" => $userId
        ];
        $data ["table"] = TABLE_USER;
    
        $isDuplicate = $this->isDuplicate($data);
        if ($isDuplicate) {
            return [
                'danger',
                'register_email_exists'
            ];
        }
        else {
            $key = md5(time());
            $data ["where"] = [
                "user_id" => $userId
            ];
            $data ["update"] ["new_email"] = $email;
            $data ["update"] ["new_email_key"] = $key;
            $data ["table"] = TABLE_USER;
    
            $this->sendMailNotificationTouserEmail($email, $key, $this->session->userdata("UserData")->chrName);
            $this->updateRecords($data);
            return [
                'success',
                'email_update_successfully'
            ];
        }
    }
    
    /**
     * updateUserPassoword()
     * this method update current user password
     *
     * @param integer $userId is current user id
     * @return array success message after updating user's password
     */
    function updateUserPassoword($userId)
    {
        $txtPassword = $this->utility->encodeText($this->input->post('txtPassword'));
        $data ["where"] = [
            "user_id" => $userId
        ];
        $data ["update"] ["user_password"] = md5($txtPassword);
        $data ["table"] = TABLE_USER;
        $this->updateRecords($data);
        return [
            'success',
            'password_changed_successfully'
        ];
    }
    
    /**
     * checkPassword()
     * This method for check passowrd is exist or not
     *
     * @param string $email first parameter
     *
     * @return bool
     */
    function checkPassword($currentUserId)
    {
        $data ["where"] = [
            "user_password" => md5($this->utility->encodeText($this->input->post('txtOld_password'))),
            "user_id" => $currentUserId
        ];
        $data ["table"] = TABLE_USER;
    
        $isDuplicate = $this->isDuplicate($data);
        // return is email duplicate or not either 0 or 1
    
        return $isDuplicate;
    }
    /**
     * sendMailNotificationTouserEmail()
     * This method send a password confirmation link to user new email
     *
     * @param string $newEmail new email
     * @param string user key $key
     * @param string person name who will recive this mail $person
     */
    function sendMailNotificationTouserEmail($newEmail, $key, $person)
    {
        $data ['person'] = $person;
        $data ['URL'] = SITEURL . 'change_email/' . $key;
        // Load a view for email tamplate
        $message = $this->load->view('email_templates/send_reset_email_address_email', $data, true);
        $data ["message"] = $message;
        $data ["from_title"] = EMAIL_TITLE;
        $data ["to"] = $newEmail;
        $data ["subject"] = $this->lang->line('confirmation_link');
        // send mail using utilitys
        $status = $this->utility->sendMailSMTP($data);
    }
}

?>
