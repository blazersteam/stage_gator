<?php

class Subscription_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * This function returns current plan id of passed user
     *
     * @param int $userId - User id of logged in user or required user
     */
    public function getCurrentPlan($userId, $group)
    {
       /*  $data ["select"] = [
            "plan_id"
        ];
        $data ["where"] = [
            "user_id" => $userId,
            "is_active" => "1"
        ];
        $data ["table"] = TABLE_USER_PLAN;
        $result = $this->selectRecords($data); */
        
        //return isset($result [0]) ? $result [0]->plan_id : ($group == "2" ? 4 : 1);
        //currently set default plan pro  
        return $group == "2" ? 6 : 3;
    }

    /**
     * getStripeCustomerId()
     * This method return user's stripe customer id
     *
     * @param integer $id customer id
     *       
     * @return String return customer id given by stripe
     */
    function getStripeCustomerInformation($id)
    {
        $data ['select'] = [
            '*'
        ];
        $data ['where'] = [
            'user_id' => $id,
            'is_active' => '1'
        ];
        $data ['table'] = TABLE_USER_PLAN;
        $res = $this->selectRecords($data);
        return isset($res [0]) ? $res [0] : false;
    }

    /**
     * getUserByEmailAddress()
     * This method get user by email for manage payment_hisrty table
     *
     * @param $customerStringId Stripe customer id
     * @return UserId
     *
     */
    function getUserByEmailAddress($customerStringId)
    {
        try {
            $data ['select'] = [
                'user_id'
            ];
            $data ['where'] = [
                'user_name' => $customerStringId
            ];
            $data ['table'] = TABLE_USER;
            $res = $this->selectRecords($data);
            
            return $res [0]->user_id;
        }
        catch (Exception $e) {
            $this->session->set_flashdata('red_msg', $e->getMeesage());
            die(redirect('payment/subscribtion'));
        }
    }

    /**
     * storeUserTransactionDetails()
     * this method always insert a new row in payment_history table when stripe webhook occure and event
     *
     * @param array $paymentInfo payment related information
     * @return boolean
     */
    function storeUserTransactionDetails($paymentInfo)
    {
        try {
            
            unset($data);
            for ($i = 0; $i < count($paymentInfo ['data_id']); $i ++) {
                
                $data ["insert"] ["transcatino_id"] = $paymentInfo ['transcatino_id'];
                $data ["insert"] ["data_id"] = $paymentInfo ['data_id'] [$i];
                $data ["insert"] ["user_id"] = $paymentInfo ['user_id'];
                $data ["insert"] ["plan_id"] = $paymentInfo ['plan'] [$i];
                $data ["insert"] ["amount"] = $paymentInfo ['total'] [$i];
                $data ["insert"] ["sub_total"] = $paymentInfo ['subtotal'];
                $data ["insert"] ["created_date"] = $paymentInfo ['transcation_date'];
                $data ["table"] = TABLE_PAYMENT_HISTORY;
                $this->insertRecord($data);
            }
        }
        catch (Exception $e) {
            $this->utility->setFlashMessage('danger', $e->getMeesage());
            redirect(SITEURL . $this->myvalues->subscriptionDetails ["controller"]);
        }
    }

    /**
     * createNewPlan()
     * This method insert a new plan details in user_plan table always call when a subscribtion created
     *
     * @param array $plan plan realted information
     * @return boolean always return true after insert
     */
    function createNewPlan($plan)
    {
        if (isset($plan ['is_webhook']) && $plan ['is_webhook'] == 0) {
            $data ["insert"] ["plan_id"] = $plan ['plan_id'];
            $data ["insert"] ["user_id"] = $plan ['user_id'];
            $data ['insert'] ['start_date'] = date('Y-m-d h:i:s');
            $data ['insert'] ['is_webhook'] = '0';
        }
        else {
            $data ["insert"] ["plan_id"] = $plan ['plan_id'];
            $data ["insert"] ["user_id"] = $plan ['user_id'];
            $data ["insert"] ["subs_id"] = $plan ['subs_id'];
            $data ["insert"] ["cus_id"] = $plan ['cus_id'];
            $data ["insert"] ["plan_amount"] = $plan ['plan_amount'];
            $data ['insert'] ['start_date'] = $plan ['start_date'];
        }
        $data ["table"] = TABLE_USER_PLAN;
        $this->insertRecord($data);
        return true;
    }

    /**
     * checkDuplicate()
     *
     * @param id is usbscribed user id
     * @return 0 if not duplicate or 1 for duplicate
     */
    function checkDuplicate($id)
    {
        try {
            $data ["where"] = [
                "user_id" => $id,
                "is_active" => 1
            ];
            $data ["table"] = TABLE_USER_PLAN;
            
            $isDuplicate = $this->isDuplicate($data);
            // return is email duplicate or not either 0 or 1
            return $isDuplicate;
        }
        catch (Exception $e) {
            $this->utility->setFlashMessage('danger', $e->getMeesage());
            redirect(SITEURL . $this->myvalues->subscriptionDetails ["controller"]);
        }
    }

    /**
     * updateUersPlanStatus()
     * This method plan deatails in user_plan tables when customer update his plan
     *
     * @param array $plan is plan details
     */
    function updateUersPlanStatus($plan)
    {
        try {
            unset($data);
            $data ["where"] = [
                "user_id" => $plan ['user_id'],
                "plan_id" => $plan ['old_plan_id'],
                "is_active" => "1"
            ];
            $data ["update"] ["subs_id"] = $plan ['subs_id'];
            $data ["update"] ["cus_id"] = $plan ['cus_id'];
            $data ["update"] ["plan_amount"] = $plan ['plan_amount'];
            $data ["update"] ["end_date"] = $plan ['canceled_at'];
            $data ["update"] ["is_active"] = $plan ['is_active'];
            $data ["update"] ["is_webhook"] = '1';
            $data ["table"] = TABLE_USER_PLAN;
            
            $this->updateRecords($data);
        }
        catch (Exception $e) {
            $this->utility->setFlashMessage('danger', $e->getMeesage());
            redirect(SITEURL . $this->myvalues->subscriptionDetails ["controller"]);
        }
    }

    /**
     * getPlans()
     * This function returns all active plans for passed user group
     */
    public function getPlans($userType)
    {
        $data ["select"] = [
            "*"
        ];
        $data ["where"] = [
            "user_type" => $userType
        ];
        $data ["table"] = TABLE_PLANS;
        $result = $this->selectRecords($data);
        return $result;
    }

    /**
     * This function returs details of passed plan id
     *
     * @param integer $planId
     */
    public function getPlanInfo($planId)
    {
        $data ["select"] = [
            "*"
        ];
        $data ["where"] = [
            "id" => $planId
        ];
        $data ["table"] = TABLE_PLANS;
        return $this->selectRecords($data);
    }

    /**
     * getUserPlanHistory()
     * This method return customer plan details
     *
     * @param integer $userId is current logged in user
     * @return object $res is payment history table data
     *        
     */
    public function getUserPlanHistory($userId)
    {
        
        $data ["select"] = [
            "h.id",
            "h.transcatino_id",
            "h.data_id",
            "h.user_id",
            "h.plan_id",
            "h.amount",
            "h.sub_total",
            "h.sub_total",
            "h.created_date",
            "p.plan_name"
        ];
        $data['table'] = TABLE_PAYMENT_HISTORY.' h';
        
        $data ["join"] = [
            TABLE_PLANS . " as p" => [
                "p.id = h.plan_id",
                "INNER"
            ]
        ];
        $data["where"] = ["h.user_id" => $userId];
        $res = $this->selectFromJoin($data);
        
        return $res;
    }
    
    /**
     * This function returns current plan all details for that user.
     * @param int $userId - user's id for plan details
     * @param int $groupId - user's group id
     * 
     * @return object of plan details
     */
    public function getCurrentPlanDetails($userId, $groupId)
    {
        $currentPlan = $this->getCurrentPlan($userId, $groupId);
        return $this->getPlanInfo($currentPlan);
    }
}

?>
