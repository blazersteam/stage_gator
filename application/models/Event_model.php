<?php

class Event_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * get_event_list()
     * This method get all events of current user
     *
     *
     * @param string $userId is user id of current login user
     * @param bool $returnSchedules , used for get all the data with related event schedule then pass true
     *       
     * @return Object $res all event of user
     *        
     *         Called From :
     *         1. Event Listing Of Master Events in Event Controller
     *         2. Event Listing Of Venue in Overview Controller
     *         3. Event Listing Of Venue in Performer Controller
     *         4. Event Listing of Schedules in Invitation Page's Dropdown in Invitation Controller With matching
     *         Sytle
     *         5. Event Listing of Venue in Profile Controller
     *        
     */
    public function get_event_list($userId, $returnSchedules = false, $matchingStyle = [], $homeLocation=null)
    {
        $data ["select"] = [
            "ev.title",
            "ev.type",
            "ev.idVenue",
            "ev.date",
            "ev.isRecuring",
            "ev.endDate",
            "ev.startTime",
            "ev.endTime",
            "ev.allowWalkInReal",
            "ev.allowTrading",
            "ev.hoursLockBefore",
            "ev.performerCanPickTime",
            "ev.haveHost",
            "ev.allowTrading",
            "ev.idAlgo",
            "ev.timePerPerformance",
            "ev.customTimeAllowed",
            "ev.descr",
            "ev.dtAdded",
            "ev.location_id",
            "ev.reInvite",
            "ev.autoAccept",
            "ev.autoReminder",
            "ev.timeZone",
            "ev.idEvent",
            "ev.recurringNotice",
            "u.chrName",
            "u.url",
            /* "sc.start_date",
            "sc.end_date" */
        ];
        
        $data ["table"] = TABLE_EVENT . ' ev';
        $data ["join"] = [
            TABLE_USER . "  u" => [
                "ev.idVenue = u.user_id",
                "INNER"
            ],
           /*  TABLE_EVENT_SCHEDULES . "  sc" => [
                "ev.idEvent = sc.idEvent",
                "LEFT"
            ] */
            
        ];
        
        if($homeLocation){

            $data ["join"] [TABLE_LOCATIONS . " as location"] = [
                "ev.location_id = location.location_id",
                "INNER"
            ];
        }
        
        $data ["order"] = "ev.date, ev.startTime";
        
        $data ['where'] = [
            "ev.status" => 1,
            "ev.idVenue" => $userId,
            "ev.endDate >= " => date("Y-m-d")
        ];
        
        if($homeLocation){
            $data ['where'] ["location.city" ] = ucfirst($homeLocation);
        }
        
        if ($returnSchedules) {
            $data ["join"] [TABLE_EVENT_SCHEDULES . "  sc"] = [
                "ev.idEvent = sc.idEvent",
                "LEFT"
            ];
            array_push($data ["select"], "sc.start_date", "sc.end_date");
            array_push($data ["select"], "sc.start_date as schedule_date");
            array_push($data ["select"], "sc.table_id as scheduleId");
            $data ["order"] = "sc.start_date, ev.startTime";
        }
        
        if (! empty($matchingStyle)) {
            $data ["join"] [TABLE_EVENT_STYLE . "  estyle"] = [
                "estyle.idEvent = ev.idEvent",
                "INNER"
            ];
            $data ["join"] [TABLE_STYLE . "  style"] = [
                "style.idStyle = estyle.idStyle",
                "INNER"
            ];
            
            $data ["where_in"] ["style.performer_style"] = $matchingStyle;
            $data ["where"] ["style.idGrp"] = "2";
            $data ["where"] ["sc.end_date >="] = date("Y-m-d");
        }

        $res = $this->selectFromJoin($data);
        //echo $this->db->last_query();
        return $res;
    }

    /**
     * deleteEventSchedule()
     * This method delete all events from event table with corresponds table
     *
     * @param $eventScheduleIds , that conatrin the id of event schedules
     *       
     * @return boolean
     */
    public function deleteEventSchedule($eventScheduleIds = "")
    {
        $allEventschedule = array();
        
        // if empty then get the data from post
        
        if (empty($eventScheduleIds)) {
            
            $ids = $this->input->post('chkstatus');
            
            foreach ($ids as $value) {
                
                $allEventschedule [] = $this->utility->decode($value);
            }
        }
        else {
            foreach ($eventScheduleIds as $evtscheduleid) {
                $allEventschedule [] = $evtscheduleid;
            }
        }
        $subject = "Event deleted by venue";
        $msg = $this->session->userdata('UserData')->chrName . ' has deleted #EVENT_NAME# time: #EVENT_TIME#  ( #TIME_ZONE# )';
        $this->main_model->sendNotificationToAcceptedPerformer($allEventschedule, $msg, $subject);
        $data ["where_in"] = [
            "idEvent" => $allEventschedule
        ];
        
        $data ["table"] = TABLE_REGISTER;
        $this->deleteRecords($data);
        
        $data ["table"] = TABLE_TIMETABLE;
        $this->deleteRecords($data);
        
        $data ["table"] = TABLE_TRADING;
        $this->deleteRecords($data);
        
        $data ["where_in"] = [
            "table_id" => $allEventschedule
        ];
        $data ["table"] = TABLE_EVENT_SCHEDULES;
        
        $this->deleteRecords($data);
        
        return true;
    }

    /**
     * deleteEvent()
     * This method delete all events from event table with corresponds table
     *
     * @param $eventIdArray , that conatain the encoded id of event
     * @param $postArray , that contain the id of event
     *       
     * @return boolean
     */
    public function deleteEvent($eventIdArray = "")
    {
        if (! empty($eventIdArray)) {
            $eventids = $eventIdArray;
        }
        else {
            $eventids = $this->input->post('chkstatus');
        }
        
        foreach ($eventids as $value) {
            
            unset($data);
            unset($allEventschedules);
            
            $allEventschedules = array();
            
            $resultEventSchedule = $this->getEventListByEventId($this->utility->decode($value));
            
            foreach ($resultEventSchedule as $id) {
                array_push($allEventschedules, $id->table_id);
            }
            
            $result = $this->deleteEventSchedule($allEventschedules);
            
            unset($data);
            
            $data ["where"] = [
                'idEvent' => $this->utility->decode($value)
            ];
            
            $data ["table"] = TABLE_EVENT;
            $this->deleteRecords($data);
            
            $data ["table"] = TABLE_EVENT_STYLE;
            $resultevtdelete = $this->deleteRecords($data);
        }
        
        return $resultevtdelete;
    }

    /**
     * insert_event()
     * This method insert event data in following table: event,user_style,timetable,messages
     *
     * @param string $idVenue is current user
     * @param all post paramertes of form
     *       
     * @return object return response
     */
    public function insert_event()
    {
       
        $rdaCreateType = $this->utility->encodeText($this->input->post('rdaCreateType'));
        $title = $this->utility->encodeText($this->input->post('txtName'));
        $location_id = $this->utility->encodeText($this->input->post('cmbLocation_id'));
        $type = $this->utility->encodeText($this->input->post('rdaType'));
        $style = $this->utility->encodeText($this->input->post('cmbStyle'));
        $date = $this->utility->encodeText($this->input->post('txtStartDate'));
        $isRecuring = $this->utility->encodeText($this->input->post('cmbIsRecuring'));
        $reInvite = $this->utility->encodeText($this->input->post('rdaReInvite'));
        
        // $end_date = $this->utility->encodeText($this->input->post('txtEndDate'));
        // if recurring = one day then start date = end date
        
        $start_time = $this->utility->encodeText($this->input->post('txtStart_time'));
        $start_time = str_replace(": PM","PM",$start_time);
        $start_time = str_replace(": AM","AM",$start_time);
        
        $end_time = $this->utility->encodeText($this->input->post('txtEnd_time'));
        $end_time = str_replace(": PM","PM",$end_time);
        $end_time = str_replace(": AM","AM",$end_time);
        
        if ($isRecuring == 1) {
            $end_date = $this->utility->encodeText($this->input->post('txtStartDate'));
            $tempStartDateTime = date("Y-m-d H:i:s", strtotime($date . " " . $start_time));
            $tempEndDateTime = date("Y-m-d H:i:s", strtotime($end_date . " " . $end_time));
            
            if ($tempStartDateTime > $tempEndDateTime) {
                $end_date = date("Y-m-d", strtotime($tempEndDateTime . " +1 day"));
            }
        }
        else {
            $end_date = $this->utility->encodeText($this->input->post('txtEndDate'));
        }
        
        $allowWalkin = $this->utility->encodeText($this->input->post('rdaAllowWalkin'));
        $allowTrading = $this->utility->encodeText($this->input->post('rdaAllowTrading'));
        $lockStatus = $this->utility->encodeText($this->input->post('rdaLockStatus'));
        $lockBefore = $this->utility->encodeText($this->input->post('txtLockBefore'));
        $performPickTime = $this->utility->encodeText($this->input->post('cmbPerformPickTime'));
        //$idAlgo = $this->utility->encodeText($this->input->post('rdaIdAlgo'));
        $allowHost = $this->utility->encodeText($this->input->post('rdaAllowHost'));
        $timePerPerformance = $this->utility->encodeText($this->input->post('txtTimePerPerformance'));
        $customTimeAllowed = $this->utility->encodeText($this->input->post('rdaCustomTimeAllowed'));
        $autoAccept = $this->input->post('rdaAutoAccept') ? $this->utility->encodeText($this->input->post('rdaAutoAccept')) : 0;
        $autoReminder = $this->utility->encodeText($this->input->post('rdaAutoReminder'));
        $descr = $this->utility->encodeText($this->input->post('txtDescr'));
        $timeZone = $this->utility->encodeText($this->input->post("cmbTimezone"));
        $unranked = $this->utility->encodeText($this->input->post("rdaUnranked"));
        $idVenue = $this->session->userdata("UserData")->user_id;
        // insert data in event table using my models query
        
        /* Extra data for Recurring Monthly. */
        /* fetching advancedSelection of days values in json format to store in Database */
        $advance = $isRecuring == "4" ? ($this->input->post("cmbMonthDay") ? $this->input->post("cmbMonthDay") : $this->input->post("cmbDaysInMonth")) : "";
        $advance1 = $isRecuring == "4" ? ($this->input->post("cmbMonthDay") ? "" : $this->input->post("cmbWeekInMonth")) : "";
        
        $advance = $isRecuring == "3" ? $this->input->post("chkDay") : $advance;
        
        if (! empty($advance1)) {
            foreach ($advance1 as $k1 => $v1) {
                foreach ($v1 as $k => $v) {
                    $scheduleId = $advance [$k1];
                    if (empty($finalDays [$scheduleId]) || (! empty($finalDays [$scheduleId]) && (! in_array($v, $finalDays [$scheduleId])))) {
                        $finalDays [$scheduleId] [] = $v;
                    }
                }
            }
            $scheduleType = "Days";
        }
        else {
            $scheduleType = $isRecuring == "4" ? "Dates" : ($isRecuring == "2" ? "Daily" : ($isRecuring == "1" ? "OneTime" : "Weekday"));
            $finalDays = $advance;
        }
        
//         echo $start_time;
        
        $data ["insert"] ["main_event_type"] = $rdaCreateType;
        $data ["insert"] ["title"] = $title;
        $data ["insert"] ["type"] = $type;
        $data ["insert"] ["idVenue"] = $idVenue;
        $data ["insert"] ["date"] = convertToGMT($date . " " . $start_time, 'Y-m-d');
        $data ["insert"] ["isRecuring"] = $isRecuring;
        $data ["insert"] ["endDate"] = convertToGMT($end_date . " " . $end_time, 'Y-m-d');
        $data ["insert"] ["startTime"] = convertToGMT($date . " " . $start_time, 'H:i');
        $data ["insert"] ["endTime"] = convertToGMT($date . " " . $end_time, 'H:i');
        $data ["insert"] ["allowWalkInReal"] = $allowWalkin;
        $data ["insert"] ["allowTrading"] = $allowTrading;
        $data ["insert"] ["lockStatus"] = $lockStatus;
        $data ["insert"] ["hoursLockBefore"] = $lockBefore;
        $data ["insert"] ["performerCanPickTime"] = $performPickTime;
        $data ["insert"] ["haveHost"] = $allowHost;
        $data ["insert"] ["allowTrading"] = $allowTrading;
        //$data ["insert"] ["idAlgo"] = $idAlgo;
        $data ["insert"] ["timePerPerformance"] = $timePerPerformance;
        $data ["insert"] ["customTimeAllowed"] = $customTimeAllowed;
        $data ["insert"] ["descr"] = $descr;
        $data ["insert"] ["dtAdded"] = time();
        $data ["insert"] ["location_id"] = $location_id;
        $data ["insert"] ["timeZone"] = TIMEZONE;
        $data ["insert"] ["unranked"] = $unranked;
        $data ["insert"] ["reInvite"] = $reInvite;
        $data ["insert"] ["autoAccept"] = $autoAccept;
        $data ["insert"] ["autoReminder"] = $autoReminder;
        $data ["insert"] ["v_extra_data"] = json_encode($finalDays);
        $data ["table"] = TABLE_EVENT;
        
        
        $idEvent = $this->insertRecord($data);
        $event = $this->myvalues->eventDetails ['controller'];
        // get last id and create unique url for each events
        // $url = SITEURL . $event . "/overview/" . $idEvent;
        $url = $this->utility->generateOverviewUrl($idEvent);
        $url2 = SITEURL . $event . "/event_timetable_service.php?idEvent=" . $idEvent;
        
        $share = $idEvent;
        $share_url = SITEURL . "/share/event?access=" . $this->utility->encode($share);
        unset($data);
        // update event table with url
        $data ["where"] = [
            "idEvent" => $idEvent
        ];
        $data ["update"] ["event_url"] = $url;
        $data ["update"] ["event_ws"] = $url2;
        $data ["update"] ["app_date"] = convertToGMT($date);
        $data ["update"] ["share_url"] = $share_url;
        $data ["table"] = TABLE_EVENT;
        $result = $this->updateRecords($data);
        
        foreach ($style as $s) {
            // insert multipal style with idEvent as primery key of event table
            $datas ["insert"] ["idEvent"] = $idEvent;
            $datas ["insert"] ["idStyle"] = $s;
            $datas ["table"] = TABLE_EVENT_STYLE;
            $this->insertRecord($datas);
        }
        
        unset($data);
        // select data from following id if get follower id then procceed to send a notification message
        $data ["select"] = [
            "followerId"
        ];
        $data ["where"] = [
            "followingId" => $idVenue
        ];
        $data ["table"] = TABLE_FAVORITES;
        $res = $this->selectRecords($data);
        
        foreach ($res as $r) {
            $followerId = $r->followerId;
            $subject = "App Notification: New Event Created!";
            $msg = $this->session->userdata("UserData")->chrName . "has created a new event! Check your StageGator Venue Stream Tab to find out event details!";
            /* Saves message in databse using common function */
            $this->main_model->saveMessage($r->followerId, $this->pUserId, $subject, $msg);
            
            // $idEvent = $this->insertRecord($data);
        }
        // finally return the last object
        $this->processSchedules($idEvent, $date, $end_date, $finalDays, $scheduleType, $start_time, $end_time, $timePerPerformance);
        return $idEvent;
    }

    /**
     * getEventById()
     * This method will get event row for edit an event
     *
     * @param unknown $id
     */
    public function getEventById($id)
    {
        // $idVenue = $this->session->userdata("UserData")->user_id;
        $data ["select"] = [
            "ev.main_event_type",
            "ev.title",
            "ev.type",
            "ev.idVenue",
            "ev.date",
            "ev.idEvent",
            "ev.isRecuring",
            "ev.endDate",
            "ev.startTime",
            "ev.endTime",
            "ev.allowWalkInReal",
            "ev.allowTrading",
            "ev.hoursLockBefore",
            "ev.performerCanPickTime",
            "ev.haveHost",
            "ev.allowTrading",
            "ev.idAlgo",
            "ev.timePerPerformance",
            "ev.customTimeAllowed",
            "ev.descr",
            "ev.dtAdded",
            "ev.location_id",
            "ev.reInvite",
            "ev.autoAccept",
            "ev.autoReminder",
            "ev.unranked",
            "ev.v_extra_data",
            "ev.timeZone",
            "u.user_id",
            "mem.mem_city",
            "mem.mem_state",
            "u.chrName",
            "sa.nameAlgo",
            "sc.start_date",
            "sc.end_date"
        ];
        
        $data ["table"] = TABLE_EVENT . ' ev';
        $data ["join"] = [
            TABLE_USER . "  u" => [
                "ev.idVenue = u.user_id",
                "LEFT"
            ],
            TABLE_MEMBERS . "  mem" => [
                "u.mem_id = mem.mem_id",
                "LEFT"
            ],
            TABLE_SCHEDULE_ALGO . "  sa" => [
                "sa.idAlgo = ev.idAlgo",
                "LEFT"
            ],
            TABLE_EVENT_SCHEDULES . " sc" => [
                "sc.idEvent = ev.idEvent",
                "LEFT"
            ]
        ];
        $data ["where"] = [
            "ev.idEvent" => $id
        ];
        
        $res = $this->selectFromJoin($data);
        
        return $res [0];
    }

    /**
     * getStyleByEventId()
     * This method return event style details
     *
     * @param integer $id is current eventId
     * @return $res is event style object
     */
    public function getStyleByEventId($id)
    {
        $data ["select"] = [
            "idStyle"
        ];
        $data ["where"] = [
            "idEvent" => $id
        ];
        $data ["table"] = TABLE_EVENT_STYLE;
        $res = $this->selectRecords($data);
        $return = array();
        foreach ($res as $value) {
            
            $return [] = $value->idStyle;
        }
        return $return;
    }

    /**
     * update_event()
     * update event update an event
     *
     * @param integer $id is event id which we need to update
     *       
     * @return return $res performer id to controlller
     */
    public function update_event($id)
    {
        $data ["select"] = [
            "startTime",
            "endTime",
            "timePerPerformance"
        ];
        $data ["where"] = [
            "idEvent" => $id
        ];
        $data ["table"] = TABLE_EVENT;
        $res = $this->selectRecords($data);
        $event = $res [0];
        
        $rdaCreateType = $this->utility->encodeText($this->input->post('rdaCreateType'));
        $title = $this->utility->encodeText($this->input->post('txtName'));
        $location_id = $this->utility->encodeText($this->input->post('cmbLocation_id'));
        $type = $this->utility->encodeText($this->input->post('rdaType'));
        $style = $this->utility->encodeText($this->input->post('cmbStyle'));
        $date = $this->utility->encodeText($this->input->post('txtStartDate'));
        $isRecuring = $this->utility->encodeText($this->input->post('cmbIsRecuring'));
        $reInvite = $this->utility->encodeText($this->input->post('rdaReInvite'));
        
        // if event recurring = one day then end date = start date
        
        if ($isRecuring == "1") {
            $end_date = $this->utility->encodeText($this->input->post('txtStartDate'));
        }
        else {
            $end_date = $this->utility->encodeText($this->input->post('txtEndDate'));
        }
        
        $start_time = $this->utility->encodeText($this->input->post('txtStart_time'));
        $start_time = str_replace(": PM","PM",$start_time);
        $start_time = str_replace(": AM","AM",$start_time);
        
        $end_time = $this->utility->encodeText($this->input->post('txtEnd_time'));
        $end_time = str_replace(": PM","PM",$end_time);
        $end_time = str_replace(": AM","AM",$end_time);
        
        if ($isRecuring == 1) {
            $end_date = $this->utility->encodeText($this->input->post('txtStartDate'));
            $tempStartDateTime = date("Y-m-d H:i:s", strtotime($date . " " . $start_time));
            $tempEndDateTime = date("Y-m-d H:i:s", strtotime($end_date . " " . $end_time));
            
            if ($tempStartDateTime >= $tempEndDateTime) {
                $end_date = date("Y-m-d", strtotime($tempEndDateTime . " +1 day"));
            }
        }
        else {
            $end_date = $this->utility->encodeText($this->input->post('txtEndDate'));
        }
        
        $allowWalkin = $this->utility->encodeText($this->input->post('rdaAllowWalkin'));
        $allowTrading = $this->utility->encodeText($this->input->post('rdaAllowTrading'));
        $lockStatus = $this->utility->encodeText($this->input->post('rdaLockStatus'));
        $lockBefore = $this->utility->encodeText($this->input->post('txtLockBefore'));
        if(empty($lockBefore)){
            $lockBefore = 0;
        }
        $performPickTime = $this->utility->encodeText($this->input->post('cmbPerformPickTime'));
        //$idAlgo = $this->utility->encodeText($this->input->post('rdaIdAlgo'));
        $allowHost = $this->utility->encodeText($this->input->post('rdaAllowHost'));
        $timePerPerformance = $this->utility->encodeText($this->input->post('txtTimePerPerformance'));
        $customTimeAllowed = $this->utility->encodeText($this->input->post('rdaCustomTimeAllowed'));
        $autoAccept = $this->input->post('rdaAutoAccept') ? $this->utility->encodeText($this->input->post('rdaAutoAccept')) : 0;
        $autoReminder = $this->utility->encodeText($this->input->post('rdaAutoReminder'));
        $timeZone = $this->utility->encodeText($this->input->post("cmbTimezone"));
        $descr = $this->utility->encodeText($this->input->post('txtDescr'));
        $unranked = $this->utility->encodeText($this->input->post("rdaUnranked"));
        /* Extra data for Recurring Monthly. */
        /* fetching advancedSelection of days values in json format to store in Database */
        $advance = $isRecuring == "4" ? ($this->input->post("cmbMonthDay") ? $this->input->post("cmbMonthDay") : $this->input->post("cmbDaysInMonth")) : "";
        $advance1 = $isRecuring == "4" ? ($this->input->post("cmbMonthDay") ? "" : $this->input->post("cmbWeekInMonth")) : "";
        
        $advance = $isRecuring == "3" ? $this->input->post("chkDay") : $advance;
        
        if (! empty($advance1)) {
            foreach ($advance1 as $k1 => $v1) {
                foreach ($v1 as $k => $v) {
                    $scheduleId = $advance [$k1];
                    if (empty($finalDays [$scheduleId]) || (! empty($finalDays [$scheduleId]) && (! in_array($v, $finalDays [$scheduleId])))) {
                        $finalDays [$scheduleId] [] = $v;
                    }
                }
            }
            $scheduleType = "Days";
        }
        else {
            $scheduleType = $isRecuring == "4" ? "Dates" : ($isRecuring == "2" ? "Daily" : ($isRecuring == "1" ? "OneTime" : "Weekday"));
            $finalDays = $advance;
        }
        
        unset($data);
        // insert data in event table using my models query
        $data ["where"] = [
            "idEvent" => $id
        ];
        $data ["update"] ["main_event_type"] = $rdaCreateType;
        $data ["update"] ["title"] = $title;
        $data ["update"] ["type"] = $type;
        $data ["update"] ["date"] = convertToGMT($date . " " . $start_time, "Y-m-d");
        $data ["update"] ["isRecuring"] = $isRecuring;
        $data ["update"] ["endDate"] = convertToGMT($end_date . " " . $end_time, "Y-m-d");
        $data ["update"] ["startTime"] = convertToGMT($date . " " . $start_time, 'H:i');
        $data ["update"] ["endTime"] = convertToGMT($date . " " . $end_time, 'H:i');
       
        $data ["update"] ["allowWalkInReal"] = $allowWalkin;
        $data ["update"] ["lockStatus"] = $lockStatus;
        $data ["update"] ["allowTrading"] = $allowTrading;
        $data ["update"] ["hoursLockBefore"] = $lockBefore;
        $data ["update"] ["performerCanPickTime"] = $performPickTime;
        $data ["update"] ["haveHost"] = $allowHost;
        $data ["update"] ["allowTrading"] = $allowTrading;
        //$data ["update"] ["idAlgo"] = $idAlgo;
        $data ["update"] ["timePerPerformance"] = $timePerPerformance;
        $data ["update"] ["customTimeAllowed"] = $customTimeAllowed;
        $data ["update"] ["descr"] = $descr;
        $data ["update"] ["location_id"] = $location_id;
        $data ["update"] ["timeZone"] = TIMEZONE;
        $data ["update"] ["unranked"] = $unranked;
        $data ["update"] ["reInvite"] = $reInvite;
        $data ["update"] ["autoAccept"] = $autoAccept;
        $data ["update"] ["autoReminder"] = $autoReminder;
        $data ["update"] ["v_extra_data"] = json_encode($finalDays);
        $data ["table"] = TABLE_EVENT;
        
        
        $this->updateRecords($data);
        $this->sendNotification($id);
        unset($data);
        $data ["where"] = [
            "idEvent" => $id
        ];
        $data ["table"] = TABLE_EVENT_STYLE;
        $this->deleteRecords($data);
        
        foreach ($style as $s) {
            unset($data);
            $data ["insert"] ["idEvent"] = $id;
            $data ["insert"] ["idStyle"] = $s;
            $data ["table"] = TABLE_EVENT_STYLE;
            $this->insertRecord($data);
        }
        
        $generateTimeTable = false;
        /* If timings are changed then delete all records of those schedules. or Perfformance time is changed */
        
        $newStartTime = convertToGMT($date . " " . $start_time, "h:i A");
        $newEndTime = convertToGMT($end_date . " " . $end_time, "h:i A");
        
        if ($newStartTime != $event->startTime || $newEndTime != $event->endTime || $timePerPerformance != $event->timePerPerformance) {
            $generateTimeTable = true;
            $this->deleteTimeTable($id);
        }
        
        $this->processSchedules($id, $date, $end_date, $finalDays, $scheduleType, $start_time, $end_time, $timePerPerformance, $generateTimeTable);
        
        /* Removes Host Entries if event is not having HOST */
        if ($allowHost == "0") {
            $data ["select"] = [
                "table_id"
            ];
            $data ["table"] = TABLE_EVENT_SCHEDULES;
            $data ["where"] = [
                "idEvent" => $id
            ];
            $scheduleIdsRes = $this->selectRecords($data);
            
            foreach ($scheduleIdsRes as $k => $v) {
                $scheduleIds [] = $v->table_id;
            }
            
            if (! empty($scheduleIds)) {
                unset($data);
                $data ["update"] = [
                    "isHost" => NULL
                ];
                $data ["where"] = [
                    "isHost >=" => 0
                ];
                $data ["where_in"] = [
                    "idEvent" => $scheduleIds
                ];
                $data ["table"] = TABLE_REGISTER;
                $this->updateRecords($data);
            }
        }
        
        return $res;
    }

    /**
     * sendNotification()
     * This function send notification when user event update
     */
    public function sendNotification($id)
    {
        $data ["select"] = [
            "rg.idPerformer",
            "ev.title",
            "ev.startTime",
            "es.start_date"
        ];
        $data ["table"] = TABLE_REGISTER . ' rg';
        $data ["join"] = [
            TABLE_EVENT_SCHEDULES . " as es" => [
                "es.table_id = rg.idEvent",
                "INNER"
            ],
            TABLE_EVENT . " as ev" => [
                "es.idEvent = ev.idEvent",
                "INNER"
            ]
        ];
        $data ["where"] = [
            "es.idEvent" => $id
        ];
        $result = $this->selectFromJoin($data);
        foreach ($result as $r) {
            // Add each row into our results array
            $idPerformer = $r->idPerformer;
            $subject = "App Notification: Event Edited!";
            $msg = "Your Upcoming Event " . $r->title . " " . dateDisplay($r->start_date . " " . $r->startTime) . " has been edited! Please check your StageGator Shows Tab!";
            /* Saves message in databse using common function */
            $this->main_model->saveMessage($idPerformer, $this->pUserId, $subject, $msg);
        }
    }

    /**
     * send_activate_email()
     * This method send an activation email to user wo registerd recently
     *
     * @param Array $user details email and password
     * @param String $pass user password
     * @param String $key activation key
     *       
     * @return Boolean True
     */
    public function send_activate_email($data)
    {
        $message = $this->load->view('email_templates/login_activation_template', $data, true);
        $data ["message"] = $message;
        $data ["from_title"] = EMAIL_TITLE;
        $data ["to"] = $data ['user'];
        $data ["subject"] = $this->lang->line('email_subject_stageGator_account_confirmation');
        // send mail using utilitys
        $this->utility->sendMailSMTP($data);
    }

    /**
     * This function will create schedules of add / edited events.
     * based on passed values
     *
     * @param int $eventId - Master event ID saved in EVENT Table
     * @param date $startDate - Event start date
     * @param date $endDate - Event end date
     * @param array $options - options for days or repeating every first second day etc..
     * @param string $type - (Dates / Days) Dates for day of month and Days for day of week order
     *       
     * @return bool
     */
    public function processSchedules($eventId, $startDate, $endDate, $options, $type, $start_time, $end_time, $timePerPerformance, $generateTimeTable = true)
    {
        set_time_limit(0);
        /* First we will disable all schedules of that event */
        $this->disableDeleteSchedules($eventId, "Disable");
        $lastMonth = "";
        $currentDate = $startDate;
        $endDate = $endDate == "" ? $startDate : $endDate;
        while ($currentDate <= $endDate) {
            $schduleEndDate = date("Y-m-d H:i:s", strtotime($currentDate . " " . $start_time)) > date("Y-m-d H:i:s", strtotime($currentDate . " " . $end_time)) ? date("Y-m-d", strtotime($currentDate . " +1 day")) : $currentDate;
            
            if ($type == "Dates") {
                if (in_array(date("d", strtotime($currentDate)), $options)) {
                    $this->savesSchedule($eventId, $currentDate, $schduleEndDate, $start_time, $end_time, $timePerPerformance, $generateTimeTable);
                }
            }
            elseif ($type == "Days") {
                
                /* this is for numers to string */
                $numDay = [
                    1 => "first",
                    2 => "second",
                    3 => "third",
                    4 => "fourth"
                ];
                
                /* this is for numers to string */
                $weekDays = [
                    1 => "monday",
                    2 => "tuesday",
                    3 => "wednesday",
                    4 => "thursday",
                    5 => "friday",
                    6 => "saturday",
                    7 => "sunday"
                ];
                if ($lastMonth != date("m", strtotime($currentDate))) {
                    $days = [];
                    foreach ($options as $k => $v) {
                        foreach ($v as $key => $value) {
                            $days [] = date("Y-m-d", strtotime($currentDate . " " . $numDay [$k] . " " . $weekDays [$value] . " of this month"));
                        }
                    }
                    $lastMonth = date("m", strtotime($currentDate));
                }
                
                if (in_array($currentDate, $days)) {
                    $this->savesSchedule($eventId, $currentDate, $schduleEndDate, $start_time, $end_time, $timePerPerformance, $generateTimeTable);
                }
            }
            elseif ($type == "Weekday") {
                if (in_array(date("N", strtotime($currentDate)), $options)) {
                    $this->savesSchedule($eventId, $currentDate, $schduleEndDate, $start_time, $end_time, $timePerPerformance, $generateTimeTable);
                }
            }
            elseif ($type == "OneTime" || $type == "Daily") {
                
                $this->savesSchedule($eventId, $currentDate, $schduleEndDate, $start_time, $end_time, $timePerPerformance, $generateTimeTable);
                if ($type == "OneTime") {
                    $currentDate = date("Y-m-d", strtotime($currentDate . "+1 day"));
                }
            }
            $currentDate = date("Y-m-d", strtotime($currentDate . "+1 day"));
        }
        
        /* Then we will delete all disabled schedules of that event */
        $this->disableDeleteSchedules($eventId, "Delete");
        return true;
    }

    /**
     * This method will be called from processSchedules and saves schedule if required
     *
     * @param int $eventId
     * @param date $startDate
     * @param date $endDate
     */
    protected function savesSchedule($eventId, $startDate, $endDate, $start_time, $end_time, $timePerPerformance, $generateTimeTable = true)
    {
        $startDate = convertToGMT($startDate . " " . $start_time, "Y-m-d");
        $endDate = convertToGMT($endDate . " " . $end_time, "Y-m-d");
        $data ["select"] = [
            "table_id"
        ];
        $data ["where"] = [
            "idEvent" => $eventId,
            "start_date" => $startDate,
            "end_date" => $endDate
        ];
        $data ["table"] = TABLE_EVENT_SCHEDULES;
        
        $result = $this->selectRecords($data);
        
        if (count($result) == 0) {
            unset($data);
            
            $data ["insert"] = [
                "idEvent" => $eventId,
                "start_date" => $startDate,
                "end_date" => $endDate,
                "created_date" => $this->datetime
            ];
            $data ["table"] = TABLE_EVENT_SCHEDULES;
            $scheduleId = $this->insertRecord($data);
            $this->addTimetableEntries($start_time, $end_time, $timePerPerformance, $scheduleId, $startDate);
        }
        else {
            unset($data);
            $data ["update"] = [
                "b_is_disabled" => 0
            ];
            $data ["where"] = [
                "idEvent" => $eventId,
                "start_date" => $startDate,
                "end_date" => $endDate
            ];
            $data ["table"] = TABLE_EVENT_SCHEDULES;
            $this->updateRecords($data);
            if ($generateTimeTable) {
                $this->addTimetableEntries($start_time, $end_time, $timePerPerformance, $result [0]->table_id, $startDate);
            }
        }
    }

    /**
     * This method will be called from processSchedules to first disabled and then delete all disabled methods that
     * are removed
     *
     * @param int $eventId
     * @param date $action
     */
    protected function disableDeleteSchedules($eventId, $action)
    {
        if ($action == "Disable") {
            $data ["update"] = [
                "b_is_disabled" => 1
            ];
            $data ["where"] = [
                "idEvent" => $eventId
            ];
            $data ["table"] = TABLE_EVENT_SCHEDULES;
            $this->updateRecords($data);
        }
        elseif ($action == "Delete") {
            $data ["select"] = [
                "table_id"
            ];
            $data ["where"] = [
                "idEvent" => $eventId,
                "b_is_disabled" => "1"
            ];
            $data ["table"] = TABLE_EVENT_SCHEDULES;
            $result = $this->selectRecords($data);
            
            foreach ($result as $k => $v) {
                $this->deleteEventSchedule([
                    $v->table_id
                ]);
            }
        }
    }

    /**
     * this funtction returns all timezzones from our database to disaply in form
     */
    public function getTimezones()
    {
        $data ["select"] = [
            "code",
            "name"
        ];
        $data ["table"] = TABLE_TIMEZONES;
        return $this->selectRecords($data);
    }

    /**
     * Used for listing the event for perfomer which are matching skills
     *
     * @param array $array , searching post array
     * @param array $event_styles_matching_with_user , taht contain the list of skills
     */
    public function search_event($array, $event_styles_matching_with_user)
    {
        $data ['select'] = [
            'evt_schedule.idEvent',
            'evt_schedule.table_id',
            'evt_schedule.start_date',
            'evt_schedule.end_date',
            'evt.title',
            'evt.idVenue',
            'evt.startTime',
            'evt.endTime',
            'evt.haveHost',
            'evt.timeZone',
            'evt.hoursLockBefore',
            'evt.allowWalkInReal',
            'evt.lockStatus',
            'evt.allowTrading',
            'evt.location_id',
            'evt.timePerPerformance',
            'user.chrName',
            'user.url',
            'location.city',
            'location.state'
        ];
        
        $data ['table'] = TABLE_EVENT_SCHEDULES . ' as  evt_schedule';
        
        $data ["join"] = [
            TABLE_EVENT . " as evt" => [
                "evt.idEvent = evt_schedule.idEvent",
                "INNER"
            ],
            TABLE_EVENT_STYLE . " as evt_style" => [
                "evt.idEvent = evt_style.idEvent",
                "INNER"
            ],
            TABLE_USER . " as user" => [
                "evt.idVenue = user.user_id",
                "INNER"
            ],
            TABLE_LOCATIONS . " as location" => [
                "evt.location_id = location.location_id",
                "INNER"
            ]
        ];
        
        $data ['where'] = [
            'evt_schedule.start_date >=' => convertToGMT($this->datetime, "Y-m-d"),
            'evt_schedule.b_is_disabled' => 0
        ];
        
        if (! empty($array)) {
            
            // Used for search with state name
            $data ["group"] ["like"] = [
                'location.state',
                $array ['city'],
                'both'
            ];
            
            // Used for search with city name
            $data ["group"] ["or_like"] [] = [
                'location.city',
                $array ['city'],
                'both'
            ];
            
            // used for seach with showname(event title)
            $data ["and_like"] [] = [
                'evt.title',
                $array ['searchName'],
                'both'
            ];
            
            // used for seach with Agency title
            $data ["and_like"] [] = [
                'user.chrName',
                $array ['searchVenueName'],
                'both'
            ];
        }
        
        // if multiple search style get then add that in query else all style search
        
        if (isset($array ['style']) && ! empty($array ['style'])) {
            $data ["where_in"] = [
                'evt_style.idStyle' => $array ['style']
            ];
        }
        else {
            $data ["where_in"] = [
                'evt_style.idStyle' => $event_styles_matching_with_user
            ];
        }
        
        // $data ["groupBy"] = 'evt_schedule.idEvent';
        $data ["groupBy"] = 'evt_schedule.table_id';
        
        $data ["order"] = 'evt_schedule.start_date asc';
        
        $result = $this->selectFromJoin($data);
        
        return $result;
    }

    /**
     * Used for apply the event by perfomer
     *
     * @param int $idEventScheduleId , event schedule id
     * @param int $idUser , id of user
     */
    public function performer_apply_event($idEventScheduleId, $idUser)
    {
        // check for already applied event
        unset($data);
        $data ["select"] = [
            "idRegister",
            "status"
        ];
        $data ["where"] = [
            "idEvent" => $idEventScheduleId,
            "idPerformer" => $idUser
        ];
        $data ["table"] = TABLE_REGISTER;
        $checkDuplicate = $this->selectRecords($data);
        if (count($checkDuplicate) > 0) {
            $rtvalue [] = false;
            $rtvalue [] = "Your previous invite has been '" . $checkDuplicate [0]->status . "', so you can not apply again.";
            return $rtvalue;
        }
        $rtvalue = array();
        $curr_date1 = date('Y-m-d');
        
        unset($data);
        // fetch the event id from event schedule
        $data ["select"] = [
            'idEvent',
            'start_date',
            'end_date'
        ];
        $data ["where"] = [
            'table_id' => $idEventScheduleId
        ];
        $data ["table"] = TABLE_EVENT_SCHEDULES;
        $resultEventId = $this->selectRecords($data);
        
        $eventId = $resultEventId [0]->idEvent;
        
        // fectch all data from event id
        unset($data);
        $data ["select"] = [
            '*'
        ];
        $data ["where"] = [
            'idEvent' => $eventId
        ];
        $data ["table"] = TABLE_EVENT;
        $resultEvent = $this->selectRecords($data);
        $resultEvent = $resultEvent [0];
        
        /* Checks IF Perfomer has not crossed allowed LIMIT as per plan for particular event type */
        $resultLimit = $this->checkPerformerLimit($resultEvent->type);
        if ($resultLimit !== true) {
            $rtvalue [] = false;
            $rtvalue [] = $resultLimit;
            return $rtvalue;
        }
        
        if ($resultEvent->lockStatus == 1) {
            $rtvalue [] = false;
            $rtvalue [] = "Event Signup Cut-off time passed.";
            return $rtvalue;
        }
        else if ($resultEvent->allowWalkInReal == 1 && event_end_time_crossed($resultEventId [0]->end_date, $resultEvent->endTime)) {
            $rtvalue [] = false;
            $rtvalue [] = "Cannot Apply for Past Events!";
            return $rtvalue;
        }
        else if (event_cut_off_time_passed($resultEventId [0]->start_date, $resultEvent->startTime, $resultEvent->hoursLockBefore, $resultEvent->allowWalkInReal)) {
            $rtvalue [] = false;
            $rtvalue [] = "Event Sign-up Cut-off time passed.";
            return $rtvalue;
        }
        
        // load the profile model for get the user's name
        $this->profileInfo = $this->myvalues->profileDetails;
        $this->load->model($this->profileInfo ["model"], "profile_model");
        $userInfo = $this->profile_model->getUserInformation($idUser);
        
        unset($data);
        
        $data ["select"] = [
            "status"
        ];
        $data ["where"] = [
            'idEvent' => $idEventScheduleId,
            'idPerformer' => $idUser
        ];
        $data ["where_in"] = [
            'status' => [
                'Pending',
                'Accepted'
            ]
        ];
        $data ["table"] = TABLE_REGISTER;
        $countRegister = $this->selectRecords($data);
        
        // have panding status
        if (count($countRegister) > 0) {
            
            unset($data);
            
            if ($countRegister [0]->status == "Pending") {
                // update the status
                
                $data ["where"] = [
                    "idEvent" => $idEventScheduleId,
                    "idPerformer" => $idUser
                ];
                $data ["update"] ["status"] = 'Accepted';
                $data ["table"] = TABLE_REGISTER;
                
                $resultUpdate = $this->updateRecords($data);
                
                $rtvalue [] = true;
                $rtvalue [] = "Your were already invited! Your application has been accepted!";
                
                unset($data);
                
                // Insert all data in message table
                $subject = "App Notification: Invitation Accepted!";
                $msg = ucwords($this->utility->decodeText($userInfo->chrName)) . " has applied to perform at your upcoming show " . $this->utility->decodeText($resultEvent->title) . "! Please check your StageGator Booking Requests Received Tab!";
                
                /* Saves message in databse using common function */
                $this->main_model->saveMessage($resultEvent->idVenue, $this->pUserId, $subject, $msg);
            }
            else {
                $rtvalue [] = true;
                $rtvalue [] = "Your application has been already accepted!";
            }
            return $rtvalue;
        }
        else if ($resultEvent->autoAccept == 1) {
            
            // make entry in register table
            unset($data);
            $data ["insert"] ["idEvent"] = $idEventScheduleId;
            $data ["insert"] ["idPerformer"] = $idUser;
            $data ["insert"] ["noticeVenue"] = 1;
            $data ["insert"] ["status"] = 'Accepted';
            $data ["insert"] ["dtAdded"] = time();
            $data ["table"] = TABLE_REGISTER;
            
            $resultInsertId = $this->insertRecord($data);
            
            $rtvalue [] = true;
            $rtvalue [] = "Your application has been accepted!";
            
            $subject = "App Notification: Invitation Accepted!";
            $msg = ucwords($this->utility->decodeText($userInfo->chrName)) . " has applied to perform at your upcoming show " . $this->utility->decodeText($resultEvent->title) . "! Please check your StageGator Booking Requests Received Tab!";
            
            // make entry in message table
            unset($data);
            /* Saves message in databse using common function */
            $this->main_model->saveMessage($resultEvent->idVenue, $this->pUserId, $subject, $msg);
            
            $resultEvent = $this->getEventSchedule($idEventScheduleId);
            
            $eventDateEmail = $resultEventId[0]->start_date . " " . $resultEvent->startTime;
            $eventDateEmail = convertFromGMT($eventDateEmail, "default", $resultEvent->timeZone);
            $timezoneAbbriviation = $this->utility->getAbbreviationOfTimezone($resultEvent->timeZone); //get abbriviation of timezone
            $eventDateEmail = date("jS M, Y", strtotime($eventDateEmail)) . " (" . $timezoneAbbriviation. ")";
            
            $email = "&quot;" . ucwords($this->utility->decodeText($userInfo->chrName)) . "&quot; has accepted to perform at &quot;" . $this->utility->decodeText($resultEvent->title) . "&quot; on " . $eventDateEmail;
            $link = '<a href="' . SITEURL . $this->myvalues->invitationDetails ['controller'] . "/received" . '" >Click Here</a> to ';
            $this->utility->notification_email($resultEvent->idVenue, $email, false, $link, "New Booking Request");
            return $rtvalue;
        }
        
        unset($data);
        // make entry in register table
        $data ["insert"] ["idEvent"] = $idEventScheduleId;
        $data ["insert"] ["idPerformer"] = $idUser;
        $data ["insert"] ["noticeVenue"] = 1;
        $data ["insert"] ["dtAdded"] = time();
        $data ["table"] = TABLE_REGISTER;
        $resultRegisterInsertId = $this->insertRecord($data);
        
        unset($data);
        
        $data ["where"] = [
            "isHost" => 1,
            "idEvent" => $idEventScheduleId
        ];
        $data ["table"] = TABLE_REGISTER;
        $resultCountRegister = $this->countRecords($data);
        
        if ($resultCountRegister != 0) {
            unset($date);
            $data ["where"] = [
                "idRegister" => $resultRegisterInsertId
            ];
            $data ["update"] ["isHost"] = NULL;
            $data ["table"] = TABLE_REGISTER;
            $resultUpdate = $this->updateRecords($data);
        }
        
        $rtvalue [] = true;
        $rtvalue [] = "Application Submitted, Waiting for Venue Approval";
        
        /* jS M, Y */
        $MainIdEvent = $this->getMainEventId($idEventScheduleId);
        $resultEvent = $this->getEventById($MainIdEvent);
        
        $eventDateEmail = $resultEventId [0]->start_date . " " . $resultEvent->startTime;
        $eventDateEmail = convertFromGMT($eventDateEmail, "default", $resultEvent->timeZone);
        $timezoneAbbriviation = $this->utility->getAbbreviationOfTimezone($resultEvent->timeZone); //get abbriviation of timezone
        $eventDateEmail = date("jS M, Y", strtotime($eventDateEmail)) . " (" . $timezoneAbbriviation. ")";
        
        $email = "&quot;" . ucwords($this->utility->decodeText($userInfo->chrName)) . "&quot; has requested to perform at &quot;" . $this->utility->decodeText($resultEvent->title) . "&quot; on " . $eventDateEmail;
        $link = '<a href="' . SITEURL . $this->myvalues->invitationDetails ['controller'] . "/received" . '" >Click Here</a> to ';
        $this->utility->notification_email($resultEvent->idVenue, $email, false, $link, "New Booking Request");
        
        unset($data);
        
        $subject = "App Notification: Application Received!";
        $msg = ucwords($this->utility->decodeText($userInfo->chrName)) . " has applied to perform at your upcoming show " . $this->utility->decodeText($resultEvent->title) . "! Please check your StageGator Booking Requests Received Tab!";
        
        /* Saves message in databse using common function */
        $this->main_model->saveMessage($resultEvent->idVenue, $this->pUserId, $subject, $msg);
        
        return $rtvalue;
    }

    /**
     * Used for getting the following venue
     *
     * @param int $userId , id of user
     *       
     * @return ArrayObject $result , that contain the id which user has follow
     */
    public function getFollowingVenue($userId)
    {
        $data ['select'] = [
            'followingId'
        ];
        $data ['where'] = [
            'followerId' => $userId
        ];
        $data ['table'] = TABLE_FAVORITES;
        
        $result = $this->selectRecords($data);
        
        return $result;
    }

    /**
     * Used for get the all related event schedule from event id
     * called from share controller & event controller
     *
     * @param int $eventId , id of event
     *       
     */
    public function getEventListByEventId($id)
    {
        $data ["select"] = [
            "ev.idEvent",
            "ev.title",
            "ev.type",
            "ev.idVenue",
            "ev.date",
            "ev.isRecuring",
            "ev.endDate",
            "ev.startTime",
            "ev.endTime",
            "ev.descr",
            "ev.dtAdded",
            "ev.location_id",
            "ev.timeZone",
            "ev.recurringNotice",
            "ev.allowTrading",
            "ev.timePerPerformance",
            "ev.haveHost",
            "sc.table_id",
            "sc.start_date",
            "sc.end_date"
        ];
        
        $data ["table"] = TABLE_EVENT . " as ev";
        
        $data ["join"] = [
            TABLE_EVENT_SCHEDULES . " as sc" => [
                "ev.idEvent = sc.idEvent",
                "INNER"
            ]
        ];
        
        $data ["where_in"] = [
            "ev.idEvent" => $id
        ];
        
        $data ["order"] = "sc.start_date, ev.startTime";
        
        $res = $this->selectFromJoin($data);
        
        return $res;
    }

    /**
     * getTimeTableForPublicReview()
     * This method whole time table of a event with user information
     *
     * @param integer $idEvent is the current event id
     *       
     * @param unknown $idEvent
     */
    public function getTimeTableForPublicReview($idEvent)
    {
        $data ["select"] = [
            "time.*",
            "u.chrName",
            "u.url",
           /*  "u.changeTip" */
        ];
        $data ["table"] = TABLE_TIMETABLE . " time";
        $data ["order"] = "time.slot_id asc";
        $data ["join"] = [
            TABLE_USER . " u" => [
                "time.idPerformer=u.user_id",
                "INNER"
            ]
        ];
        $data ["where"] = [
            "time.idEvent" => $idEvent
        ];
        $res = $this->selectFromJoin($data);
        return $res;
        
        /*
         * $sql = "select timetable.*, user.chrName, user.url from timetable JOIN user ON idPerformer=user_id where
         * idEvent=$idEvent order by slot_id"; return $this->db->query($sql);
         */
    }

    /**
     * sendInvitationReminder()
     * This method send reminder mail to performer
     *
     * @param unknown $ids
     * @param unknown $idVenue
     * @return boolean
     */
    public function sendInvitationReminder($ids = NULL, $idEvent = NULL, $idVenue = NULL)
    {
        if ($this->input->post()) {
            $PostData = $this->input->post();
        }
        else {
            $PostData = [
                'idEvent' => $idEvent,
                'chkstatus' => $ids
            ];
        }
        
        if (isset($PostData ['chkstatus']) && ! empty($PostData ['chkstatus'])) {
            
            for ($i = 0; $i < count($PostData ['chkstatus']); $i ++) {
                
                if ($this->input->post()) {
                    $stringEncodedValue = $this->utility->decode($PostData ['chkstatus'] [$i]);
                    unset($stringEncodedArray);
                    $stringEncodedArray = Explode('||', $stringEncodedValue);
                    $idEvent = $stringEncodedArray [0];
                    $idRegister = $stringEncodedArray [1];
                }
                else {
                    $idRegister = $this->utility->decode($PostData ['chkstatus'] [$i]);
                    $idEvent = $this->utility->decode($PostData ['idEvent'] [$i]);
                }
                
                if (ctype_digit($idRegister) && ctype_digit($idEvent)) {
                    
                    $res = $this->reminderCommonMethod($idEvent, $idRegister);
                    $mainData = $this->getEventById($res->idEvent);
                    
                    // jS M, Y
                    $eventDateEmail = $res->eventScheduleStartDate . " " . $mainData->startTime;
                    $eventDateEmail = convertFromGMT($eventDateEmail, "default", $mainData->timeZone);
                    $timezoneAbbriviation = $this->utility->getAbbreviationOfTimezone($mainData->timeZone); //get abbriviation of timezone
                    $eventDateEmail = date("jS M, Y", strtotime($eventDateEmail)) . " (" . $timezoneAbbriviation . ")";
                    $email = "This is a reminder that &quot;" . ucwords($mainData->chrName) . "&quot; invited you to perform at &quot;" . $mainData->title . "&quot; on " . $eventDateEmail . ". Please confirm if you are performing at this event";
                    
                    $this->utility->notification_email($res->idPerformer, $email, false, "Please", "Booking Invitation Reminder");
                    $data ["update"] ["ReminderSentOn"] = time();
                    $data ["where"] = [
                        "idRegister" => $idRegister
                    ];
                    $data ["table"] = TABLE_REGISTER;
                    $this->updateRecords($data);
                    
                    $subject = "App Notification: Reminder Received!";
                    $msg = "This is a reminder: " . ucwords($mainData->chrName) . " has invited you to perform at their upcoming event " . $mainData->title . "! Please check your StageGator Booking Requests Received Tab!";
                    // Saves message in databse using common function
                    $this->main_model->saveMessage($res->idPerformer, $this->pUserId, $subject, $msg);
                }
            }
            return true;
        }
        else {
            return false;
        }
    }

    public function venueUpdateRequest($status, $idRegister, $idEvent, $idPerformer)
    {
        // fetch the eventschedule id to check event date passed
        $data ["select"] = [
            "idEvent"
        ];
        $data ["where"] = [
            "idRegister" => $idRegister
        ];
        $data ["table"] = TABLE_REGISTER;
        $resultEventScheduleId = $this->selectRecords($data);
        // this function used for check if event date passed
        $checkEventPassed = $this->main_model->CheckEventPassed($resultEventScheduleId [0]->idEvent);
        
        $data ["update"] ["status"] = $status;
        $data ["update"] ["noticeVenue"] = $this->pidGroup == "3" ? 1 : 0;
        $data ["update"] ["noticePerformer"] = $this->pidGroup == "3" ? 0 : 1;
        $data ["where"] = [
            "idRegister" => $idRegister
        ];
        $data ["table"] = TABLE_REGISTER;
        $this->updateRecords($data);
        unset($data);
        $event = $this->reminderCommonMethod($idEvent, $idRegister);
        $scheduleId = $this->getEventIdByRegisterId($idRegister);
        $mainData = $this->getEventSchedule($scheduleId);
        
        /* jS M, Y */
        $eventDateEmail = $mainData->start_date . " " . $mainData->startTime;
        $eventDateEmail = convertFromGMT($eventDateEmail, "default", $mainData->timeZone);
        $timezoneAbbriviation = $this->utility->getAbbreviationOfTimezone($mainData->timeZone); //get abbriviation of timezone
        $eventDateEmail = date("jS M, Y", strtotime($eventDateEmail)) . " (" . $timezoneAbbriviation. ")";
        
        $actionExecuterName = ucwords($mainData->chrName);
        
        if ($this->pidGroup == "3") {
            $this->load->model($this->myvalues->profileDetails ["model"], "profile_model");
            $userData = $this->profile_model->getUserInformation($idPerformer);
            $actionExecuterName = ucwords($userData->chrName);
        }
        
        $userEmailId = $this->pidGroup == "3" ? $event->idVenue : $idPerformer;
        $email = "&quot;" . $actionExecuterName . "&quot; has &quot;$status&quot; invitation for  &quot;" . $mainData->title . "&quot; on " . $eventDateEmail;
        $link = '<a href="' . SITEURL . $this->myvalues->invitationDetails ['controller'] . "/received" . '" >Click Here</a> to ';
        $this->utility->notification_email($userEmailId, $email, false, $link, "Booking Request Updated");
        $subject = "App Notification: Application $status!";
        $msg = $this->session->userdata("UserData")->chrName . " has " . $status . " " . $mainData->title . "  " . dateDisplay($mainData->start_date . " " . $mainData->startTime, 'Y-m-d h:i A');
        /* Saves message in databse using common function */
        $this->main_model->saveMessage($userEmailId, $this->pUserId, $subject, $msg);
        
        return true;
    }

    public function performerUpdateRequest($status, $idRegister, $idEvent, $idPerformer)
    {
        $data ["update"] ["status"] = $status;
        $data ["update"] ["noticeVenue"] = $this->pidGroup == "3" ? 1 : 0;
        $data ["update"] ["noticePerformer"] = $this->pidGroup == "3" ? 0 : 1;
        $data ["where"] = [
            "idRegister" => $idRegister,
            "idPerformer" => $idPerformer
        ];
        
        $data ["table"] = TABLE_REGISTER;
        $this->updateRecords($data);
        unset($data);
        
        $event = $this->reminderCommonMethod($idEvent, $idRegister);
        $scheduleId = $this->getEventIdByRegisterId($idRegister);
        $mainData = $this->getEventSchedule($scheduleId);
        
        $actionExecuterName = ucwords($mainData->chrName);
        if ($this->pidGroup == "3") {
            $this->load->model($this->myvalues->profileDetails ["model"], "profile_model");
            $userData = $this->profile_model->getUserInformation($idPerformer);
            $actionExecuterName = ucwords($userData->chrName);
        }
        
        if ($status == "Rejected") {
            $status = "Declined";
        }
        
        /* jS M, Y */
        $eventDateEmail = $mainData->start_date . " " . $mainData->startTime;
        $eventDateEmail = convertFromGMT($eventDateEmail, "default", $mainData->timeZone);
        $timezoneAbbriviation = $this->utility->getAbbreviationOfTimezone($mainData->timeZone); //get abbriviation of timezone
        $eventDateEmail = date("jS M, Y", strtotime($eventDateEmail)) . " (" . $timezoneAbbriviation. ")";
        
        $email = "&quot;" . $actionExecuterName . "&quot; has &quot;$status&quot; your booking invitation to perform at &quot;" . $mainData->title . "&quot; on " . $eventDateEmail;
        
        $link = '<a href="' . SITEURL . $this->myvalues->invitationDetails ['controller'] . "/sent" . '" >Click Here</a> to ';
        $userEmailId = $this->pidGroup == "3" ? $mainData->idVenue : $idPerformer;
        
        $this->utility->notification_email($userEmailId, $email, false, $link, "Booking Request Updated");
        
        if (strcmp('Accepted', $status) === 0) {
            
            $subject = "App Notification: Invitation " . $status;
            $msg = "Your Invitation to " . ucwords($actionExecuterName) . " to perform at the upcoming show " . $mainData->title . " has been Accepted!";
        }
        else if (strcmp('Declined', $status) === 0) {
            $subject = "App Notification: Invitation " . $status;
            $msg = "Your Invitation to " . ucwords($actionExecuterName) . " to perform at the upcoming show " . $mainData->title . " has Canceled!";
        }
        $this->main_model->saveMessage($userEmailId, $this->pUserId, $subject, $msg);
        return true;
    }

    /**
     * reminderCommonMethod()
     * this method is common method for event cancalation and sendreminder
     *
     * @param unknown $idRegister
     * @return return object
     */
    public function reminderCommonMethod($idEvent, $idRegister)
    {
        $data ["select"] = [
            "ev.idVenue",
            "ev.idEvent",
            "ev.date"
        ];
        $data ["table"] = TABLE_EVENT . ' ev';
        $data ["where"] = [
            "ev.idEvent" => $idEvent
        ];
        $res = $this->selectRecords($data);
        $res = $res [0];
        unset($data);
        $data ["select"] = [
            'r.idPerformer',
            'es.start_date'
        ];
        $data ["where"] = [
            "r.idRegister" => $idRegister
        ];
        $data ["table"] = TABLE_REGISTER . ' r';
        $data ["join"] = [
            TABLE_EVENT_SCHEDULES . " es" => [
                "es.table_id = r.idEvent",
                "INNER"
            ]
        ];
        $performer = $this->selectFromJoin($data);
        $res->idPerformer = $performer [0]->idPerformer;
        $res->eventScheduleStartDate = $performer [0]->start_date;
        
        return $res;
    }

    /**
     * This function will create slots based on timings provided.
     *
     * @param time $start_time - start time of event
     * @param time $end_time - end time of event
     * @param time $timePerPerformance - minutes to perform
     * @param int $idEvent - schedule id
     * @param int $date date of event
     */
    public function addTimetableEntries($start_time, $end_time, $timePerPerformance, $idEvent, $date)
    {
        // count time scaduling and insert it to timetable table with idEvent priemry key of event table
        $stime = strtotime($start_time);
        $etime = strtotime($end_time);
        $totalmin = ($etime - $stime) / 60;
        $totalmin = ($totalmin < 0) ? (1440 + $totalmin) : $totalmin;
        $totalRows = $totalmin / $timePerPerformance;
        $tstart = $stime;
        
        for ($i = 1; $i <= $totalRows; $i ++) {
            $tend = date("H:i:s", strtotime("+$timePerPerformance minutes", $tstart));
            $tendst = date("h:i:s", strtotime("+$timePerPerformance minutes", $tstart));
            $tstartst = date("h:i:s", $tstart);
            $tstart = date("H:i:s", $tstart);
            
            unset($data);
            $data ["insert"] ["idEvent"] = $idEvent;
            $data ["insert"] ["slot_id"] = $i;
            $data ["insert"] ["date"] = convertToGMT($date, "Y-m-d");
            $data ["insert"] ["start_time"] = convertToGMT($date . " " . $tstart, "H:i:s");
            $data ["insert"] ["end_time"] = convertToGMT($date . " " . $tend, "H:i:s");
            $data ["insert"] ["start_time_st"] = convertToGMT($date . " " . $tstartst, "h:i:s");
            $data ["insert"] ["end_time_st"] = convertToGMT($date . " " . $tendst, "h:i:s");
            $data ["table"] = TABLE_TIMETABLE;
            $this->insertRecord($data);
            $tstart = strtotime($tstart);
            $tstart = strtotime(date("H:i:s", strtotime("+$timePerPerformance minutes", $tstart)));
        }
    }

    /**
     * This function finds all schedules and deletes those timetable entries and send NOtification to all related
     * users.
     *
     * @param int $id - Master event id to be deleted
     */
    public function deleteTimeTable($id)
    {
        
        /* delete all timetable entries for master event using join with schedules */
        $this->db->query("DELETE " . TABLE_TIMETABLE . " FROM " . TABLE_TIMETABLE . " INNER JOIN " . TABLE_EVENT_SCHEDULES . " es ON es.table_id = " . TABLE_TIMETABLE . ".idEvent WHERE es.idEvent = ?", [
            $id
        ]);
    }

    /**
     * getMainEventId()
     * Common function to get main event id
     *
     * @param string event_scedule table's id $id
     * @return integer $res corresponds event id
     */
    public function getMainEventId($id)
    {
        $data ["select"] = [
            "idEvent"
        ];
        $data ["where"] = [
            "table_id" => $id
        ];
        $data ["table"] = TABLE_EVENT_SCHEDULES;
        $res = $this->selectRecords($data);
        return $res [0]->idEvent;
    }

    /**
     * Used for get all details for event schedule related to event
     * called from only share controller
     *
     * @param int $id , event schedule id
     *       
     * @return ArrayObject $result, event schedule data
     */
    public function getEventSchedule($idEventSchedule)
    {
        $data ["select"] = [
            "ev.idEvent",
            "ev.title",
            "ev.type",
            "ev.idVenue",
            "ev.date",
            "ev.isRecuring",
            "ev.endDate",
            "ev.startTime",
            "ev.endTime",
            "ev.descr",
            "ev.dtAdded",
            "ev.location_id",
            "ev.timeZone",
            "ev.recurringNotice",
            "ev.allowTrading",
            "ev.hoursLockBefore",
            "ev.performerCanPickTime",
            "ev.allowWalkInReal",
            "ev.timePerPerformance",
            "ev.haveHost",
            "sc.table_id",
            "sc.start_date",
            "sc.end_date",
            "u.chrName",
            "sa.nameAlgo"
        ];
        
        $data ["table"] = TABLE_EVENT . " as ev";
        
        $data ["join"] = [
            TABLE_EVENT_SCHEDULES . " as sc" => [
                "ev.idEvent = sc.idEvent",
                "INNER"
            ],
            TABLE_USER . " as u" => [
                "ev.idVenue = u.user_id",
                "INNER"
            ],
            TABLE_SCHEDULE_ALGO . "  sa" => [
                "sa.idAlgo = ev.idAlgo",
                "LEFT"
            ]
        ];
        
        $data ["where_in"] = [
            "sc.table_id" => $idEventSchedule
        ];
        
        $data ["order"] = "sc.start_date";
        
        $result = $this->selectFromJoin($data);
        
        return $result [0];
    }

    /**
     * This function returns created events count based on its status and ID of Event
     *
     * @param int $userId - venue's id
     * @param int $type - event's type
     * @param int $month - No of Month
     * @param int $eventId - Id of Event if it is editing
     */
    public function getCountsByEventType($userId, $type, $month, $eventId = 0)
    {
        $data ["where"] = [
            "idVenue" => $userId,
            "MONTH(FROM_UNIXTIME(dtAdded))" => $month,
            "idEvent !=" => $eventId,
            "type" => $type
        ];
        $data ["table"] = TABLE_EVENT;
        return $this->countRecords($data);
    }

    /**
     * This function returns created events count based on its status and ID of Event
     *
     * @param int $userId - venue's id
     * @param int $type - event's type
     * @param int $month - No of Month
     * @param int $eventId - Id of Event if it is editing
     */
    public function getCountsByEventTypeForPerformer($userId, $type, $month)
    {
        $data ["select"] = [
            "ev.idEvent"
        ];
        $data ["table"] = TABLE_REGISTER . " rg";
        $data ["join"] = [
            TABLE_EVENT_SCHEDULES . " es" => [
                "es.table_id = rg.idEvent",
                "INNER"
            ],
            TABLE_EVENT . " ev" => [
                "es.idEvent = ev.idEvent",
                "INNER"
            ]
        ];
        $data ["where"] = [
            "rg.idPerformer" => $userId,
            "ev.type" => $type,
            "MONTH(FROM_UNIXTIME(rg.dtAdded))" => $month
        ];
        $data ["where_in"] = [
            "rg.status" => [
                "Applied",
                "Accepted"
            ]
        ];
        $result = $this->selectFromJoin($data);
        return count($result);
    }

    /**
     * This function checks its limit for creating / editing event based on its plan for Event Type (Open MIC or
     * Showcase)
     * This method will be called from Validation checking time
     *
     * @param unknown $type type passed in POST Data
     */
    public function checkPerformerLimit($type)
    {
        /* When Add Method Called */
        $counts = $this->getCountsByEventTypeForPerformer($this->pUserId, $type, date("m"));
        
        $planInfo = $this->subscription_model->getCurrentPlanDetails($this->pUserId, $this->pidGroup);
        
        $allowedCount = $planInfo [0]->openmic;
        
        if ($type == "Showcase") {
            $allowedCount = $planInfo [0]->showcase;
        }
        
        /* Returs true if has unlimited access */
        if ($allowedCount < 0) {
            return true;
        }
        elseif ($allowedCount == "0") { /* Returs error if not has access */
            return "You are not allowed to apply for $type events. Please upgrade your plan.  <a href='" . SITEURL . $this->myvalues->subscriptionDetails ["controller"] . "'>Click Here</a>";
        }
        elseif ($counts >= $allowedCount) { /* Returs error if used all limits */
            return "You have reached your maximum limit for $type events. Please upgrade your plan.  <a href='" . SITEURL . $this->myvalues->subscriptionDetails ["controller"] . "'>Click Here</a>";
        }
        else {
            return true;
        }
    }

    /**
     * Used for checking the event schedule deleted or not
     *
     * @param int $eventScheduleId, event schedule id
     *       
     */
    public function CheckEventSchduleDelete($eventScheduleId)
    {
        $data ["where"] = [
            "table_id" => $eventScheduleId
        ];
        $data ["table"] = TABLE_EVENT_SCHEDULES;
        $result = $this->countRecords($data);
        return $result;
    }

    /**
     * Used for checking the event deleted or not
     *
     * @param int $eventId, event id
     *       
     */
    public function CheckEventDelete($eventId)
    {
        $data ["where"] = [
            "idEvent" => $eventId
        ];
        $data ["table"] = TABLE_EVENT;
        $result = $this->countRecords($data);
        return $result;
    }

    /**
     * Used for checking the event related register table data
     *
     * @param int $registerId, regisetr id of register tabel
     *       
     */
    public function CheckEventRegisterIdDelete($registerId)
    {
        $data ["where"] = [
            "idRegister" => $registerId
        ];
        $data ["table"] = TABLE_REGISTER;
        $result = $this->countRecords($data);
        return $result;
    }

    /**
     * getEventIdByRegisterId()
     * This method return idEvent which is schedule id of event from register table
     *
     * @param integer $idRegister is register table primery key
     *       
     * @return if get idEvent then return idEvent otherwise return NULLs
     */
    public function getEventIdByRegisterId($idRegister)
    {
        $data ["select"] = [
            "idEvent"
        ];
        $data ["table"] = TABLE_REGISTER;
        $data ["where"] = [
            "idRegister" => $idRegister
        ];
        $res = $this->selectRecords($data);
        return ! empty($res) ? $res [0]->idEvent : NULL;
    }

    /**
     * getEventsByFanLocation
     * this method fetch data according to fan location or current city or used to get result according to date
     *
     * @param string $location is city name of user we always get city here
     * @param string $date fan can search event by by date if date value not get then it will take default datetime
     *       
     * @return Return all event records according to where condition
     */
    public function getEventsByFanLocation($city, $state, $country, $date)
    {
        $data ['select'] = [
            'es.table_id as scheduleId',
            'event.idEvent',
            'event.idVenue',
            'event.date',
            'event.startTime',
            'event.endDate',
            'es.start_date as schedule_date',
            'es.end_date',
            'event.endTime',
            'event.timeZone',
            'event.title',
            'event.status',
            'event.allowTrading',
            'event.hoursLockBefore',
            'event.allowWalkInReal',
            'event.lockStatus',
            'event.descr',
            "u.chrName",
            "u.url",
            "lc.city",
            "lc.state",
            "STR_TO_DATE(concat(es.start_date,'  ',event.startTime),'%Y-%m-%d %h:%i%:%s') as sortTime"
        ];
        $data ['table'] = TABLE_EVENT . ' as event';
        $data ["join"] = [
            TABLE_EVENT_SCHEDULES . " as es" => [
                "event.idEvent = es.idEvent",
                "INNER"
            ],
            TABLE_USER . " as u" => [
                "event.idVenue = u.user_id",
                "INNER"
            ],
            TABLE_LOCATIONS . " as lc" => [
                "event.location_id = lc.location_id",
                "INNER"
            ]
        ];
        
        $data ['where'] = [
            "lc.city" => $city,
            "lc.state" => $state,
            "lc.country" => $country,
            'es.start_date =' => $date
        ];
        
        //JB Group
        $data ["groupBy"] = ['event.idVenue'];
        
        $data ['order'] = 'sortTime';
        
        $result = $this->selectFromJoin($data);
//         echo $this->db->last_query();
        return $result;
    }
}

?>
