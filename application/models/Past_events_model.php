<?php

class Past_events_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * getPastEventList()
     * This method get past event list of venue
     * @param string $userId is user id of current login user
     * 
     * @return Object $res all event of user
     */
    public function getPastEventList($userId)
    {
        $data ["select"] = [
            "ev.idEvent",
            "ev.title",
            "ev.type",
            "ev.idVenue",
            "ev.date",
            "ev.isRecuring",
            "ev.endDate",
            "ev.startTime",
            "ev.endTime",
            "ev.allowWalkInReal",
            "ev.allowTrading",
            "ev.hoursLockBefore",
            "ev.performerCanPickTime",
            "ev.haveHost",
            "ev.allowTrading",
            "ev.idAlgo",
            "ev.timePerPerformance",
            "ev.customTimeAllowed",
            "ev.descr",
            "ev.dtAdded",
            "ev.location_id",
            "ev.reInvite",
            "ev.autoAccept",
            "ev.autoReminder",
            "ev.timeZone",
            "ev.idEvent",
            "ev.recurringNotice",
            "u.chrName",
            "u.url",
            "min(sc.end_date) as end_date"
            /* "sc.start_date",
            "sc.end_date" */
        ];
        $data ["table"] = TABLE_EVENT . ' ev';
        $data ["join"] = [
            TABLE_USER . "  u" => [
                "ev.idVenue = u.user_id",
                "INNER"
            ],
            TABLE_EVENT_SCHEDULES . "  sc" => [
                "ev.idEvent = sc.idEvent",
                "LEFT"
            ]
        ];
        $data ["order"] = "ev.date, ev.startTime";
        $data ['where'] = [
            "ev.status" => 1,
            "ev.idVenue" => $userId,
            "sc.end_date <= " => date("Y-m-d")
        ];
        $data ["groupBy"] = 'ev.idEvent';
        $res = $this->selectFromJoin($data);
        return $res;
    }
   /**
    * getEventSheduleList()
    * This method get event shedules from master event
    * @param interger $id is master event id
    * 
    * @return $res  event shedulelist return shedule list 
    */
    public function getEventSheduleList($id)
    {
        $data ["select"] = [
            "ev.idEvent",
            "ev.title",
            "ev.type",
            "ev.idVenue",
            "ev.date",
            "ev.isRecuring",
            "ev.endDate",
            "ev.startTime",
            "ev.endTime",
            "ev.descr",
            "ev.dtAdded",
            "ev.location_id",
            "ev.timeZone",
            "ev.recurringNotice",
            "ev.allowTrading",
            "ev.timePerPerformance",
            "ev.haveHost",
            "sc.table_id",
            "sc.start_date",
            "sc.end_date"
        ];
        
        $data ["table"] = TABLE_EVENT . " as ev";
        
        $data ["join"] = [
            TABLE_EVENT_SCHEDULES . " as sc" => [
                "ev.idEvent = sc.idEvent",
                "INNER"
            ]
        ];
        
        $data ["where_in"] = [
            "ev.idEvent" => $id
        ];
        $data ['where'] = [
            "sc.end_date <= " => date("Y-m-d")
        ];
        $data ["order"] = "sc.start_date, ev.startTime";
        
        $res = $this->selectFromJoin($data);
        
        return $res;
    }
    
    
    /**
     * getPerformerPastEvents()
     * Used for display the perfomer profile listing information with user's upcomig event
     *
     * @param int $idUser
     *
     * @return ArrayObject , $result that contain the info about prefomer with event data
     */
    function getPerformerPastEvents($idUser)
    {
        $data ['select'] = [
            'es.table_id as scheduleId',
            'event.idEvent',
            'event.date',
            'event.startTime',
            'event.endDate',
            'es.start_date as schedule_date',
            'es.end_date',
            'event.endTime',
            'event.timeZone',
            'event.title',
            'event.status',
            'event.allowTrading',
            'event.descr',
            'register.isHost',
            'register.updateSlotNoticeToPerformer',
            'register.doList',
            "u.chrName",
            "u.url",
            "tt.table_id"
        ];
        $data ['table'] = TABLE_REGISTER . ' as register';
        $data ["join"] = [
            TABLE_EVENT_SCHEDULES . " as es" => [
                "es.table_id = register.idEvent",
                "INNER"
            ],
            TABLE_EVENT . " as event" => [
                "event.idEvent = es.idEvent",
                "INNER"
            ],
            TABLE_USER . " as u" => [
                "event.idVenue = u.user_id",
                "INNER"
            ],
            TABLE_TIMETABLE . " as tt" => [
                "tt.idEvent = es.table_id AND tt.idPerformer = register.idPerformer",
                "LEFT"
            ]
        ];
        $data ['where'] = [
            'register.idPerformer' => $idUser,
            'register.status' => 'Accepted',
            "es.end_date <= " => date("Y-m-d")
        ];
        $data ['order'] = 'schedule_date';
    
        $result = $this->selectFromJoin($data);
    
        unset($data);
    
       
        return $result;
    }
    

   
}

?>
