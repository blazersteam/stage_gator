<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trading_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_trading_request_to_me($idUser)
    {
        $data ["select"] = [
            "td.*",
            "u.chrName",
            "ev.title",
            "time1.date as showDate1",
            "time1.start_time as start_time1",
            "time1.end_time as end_time1",
            "time2.date as showDate2",
            "time2.start_time as start_time2",
            "time2.end_time as end_time2",
            "es.start_date as scheduleStartDate",
            "ev.startTime as scheduleStartTime",
        ];
        $data ["table"] = TABLE_TRADING . " as td";
        $data ["join"] = [
            TABLE_USER . " as u" => [
                "u.user_id = td.requestBy",
                "INNER"
            ],
            TABLE_TIMETABLE . " as time2" => [
                "time2.slot_id = td.requestBySlot AND time2.idEvent = td.idEvent",
                "INNER"
            ],
            TABLE_TIMETABLE . " as time1" => [
                "time1.slot_id = td.requestToSlot AND time1.idEvent = td.idEvent",
                "INNER"
            ],
            TABLE_EVENT_SCHEDULES . " as es" => [
                "td.idEvent = es.table_id",
                "INNER"
            ],
            TABLE_EVENT . " as ev" => [
                "ev.idEvent = es.IdEvent",
                "INNER"
            ]
        ];
        $data ["where"] = [
            "requestTo" => $idUser
        ];
        $data ["order"] = "requestToNotice";
        $result = $this->selectFromJoin($data);
        
        unset($data);
        $data ["update"] = [
            "requestToNotice" => "0"
        ];
        $data ["where"] = [
            "requestTo" => $idUser
        ];
        $data ["table"] = TABLE_TRADING;
        $this->updateRecords($data);
        
        return $result;
    }

    public function get_trading_request_from_me($idUser)
    {
        $data ["select"] = [
            "td.*",
            "u.chrName",
            "ev.title",
            "time1.date as showDate1",
            "time1.start_time as start_time1",
            "time1.end_time as end_time1",
            "time2.date as showDate2",
            "time2.start_time as start_time2",
            "time2.end_time as end_time2",
            "es.start_date as scheduleStartDate",
            "ev.startTime as scheduleStartTime",
        ];
        $data ["table"] = TABLE_TRADING . " as td";
        $data ["join"] = [
            TABLE_USER . " as u" => [
                "u.user_id = td.requestTo",
                "INNER"
            ],
            TABLE_TIMETABLE . " as time1" => [
                "time1.slot_id = td.requestToSlot AND time1.idEvent = td.idEvent",
                "INNER"
            ],
            TABLE_TIMETABLE . " as time2" => [
                "time2.slot_id = td.requestBySlot AND time2.idEvent = td.idEvent",
                "INNER"
            ],
            TABLE_EVENT_SCHEDULES . " as es" => [
                "td.idEvent = es.table_id",
                "INNER"
            ],
            TABLE_EVENT . " as ev" => [
                "ev.idEvent = es.IdEvent",
                "INNER"
            ]
        ];
        $data ["where"] = [
            "requestBy" => $idUser
        ];
        $data ["order"] = "requestByNotice";
        $result = $this->selectFromJoin($data);
        
        unset($data);
        $data ["update"] = [
            "requestByNotice" => "0"
        ];
        $data ["where"] = [
            "requestBy" => $idUser
        ];
        $data ["table"] = TABLE_TRADING;
        $this->updateRecords($data);
        
        return $result;
    }

    function accept_trade($trade_id, $idUser)
    {
        $data ['select'] = [
            "idEvent"
        ];
        $data ['where'] = [
            "trade_id" => $trade_id
        ];
        $data["table"] = TABLE_TRADING;
        $resultEventScheduleId = $this->selectRecords($data);
        
        // this function used for check if event date passed
        
        $checkEventPassed = $this->main_model->CheckEventPassed($resultEventScheduleId[0]->idEvent);
        
        $A = $idUser;
        $sql = "select * from trading where trade_id= ? and requestTo= ? and status=2";
        $trade = $this->db->query($sql, [
            $trade_id,
            $A
        ]);
        
        if ($trade->num_rows() == 0) {
            return 0;
        }
        $trade = $trade->row();
        $X = $trade->requestToSlot;
        $Y = $trade->requestBySlot;
        $B = $trade->requestBy;
        
        $sql = "update trading set status=3, requestToNotice=1 where requestBy=? and requestBySlot=? and status=2";
        $this->db->query($sql, [
            $A,
            $X
        ]);
        
        $sql = "update trading set status=0, requestByNotice=1 where requestTo= ? and requestToSlot= ? and status=2";
        $this->db->query($sql, [
            $A,
            $X
        ]);
        
        $sql = "update trading set status=3, requestToNotice=1 where requestBy=? and requestBySlot=? and status=2";
        $this->db->query($sql, [
            $B,
            $Y
        ]);
        
        $sql = "update trading set status=0, requestByNotice=1 where requestTo=? and requestToSlot=? and status=2";
        $this->db->query($sql, [
            $B,
            $Y
        ]);
        
        $sql = "update trading set status=1, requestByNotice=1 where trade_id=?";
        $this->db->query($sql, [
            $trade_id
        ]);
        
        $sql = "update timetable set idPerformer= ? where idEvent= ? and slot_id=?";
        $parameters = [
            $trade->requestTo,
            $trade->idEvent,
            $trade->requestBySlot
        ];
        
        $this->db->query($sql, $parameters);
        
        $sql = "update timetable set idPerformer=? where idEvent= ? and slot_id=?";
        $this->db->query($sql, [
            $trade->requestBy,
            $trade->idEvent,
            $trade->requestToSlot
        ]);
        
        /*
         * $this->load->model($this->myvalues->profileDetails ["model"], "profile_model");
         * $profileName = $this->profile_model->getUserInformation($A);
         *
         * $this->load->model($this->myvalues->eventDetails ["model"], "event_model");
         * $eventName = $this->event_model->getEventSchedule($trade->idEvent);
         *
         * $subject = "App Notification: Trade Request Accepted!";
         * $msg = "Your Trade Request to trade slots with " . ucwords($profileName->chrName) . " at " .
         * $eventName->title . " has been accepted!";
         * $this->main_model->saveMessage($X, $A, $subject, $msg);
         */
        $this->sendMessageForTrading($B, $trade->idEvent, "Accepted");
        
        return 1;
    }

    function cancel_trade($trade_id, $idUser)
    {
        $data ['select'] = [
            "idEvent"
        ];
        $data ['where'] = [
            "trade_id" => $trade_id
        ];
        $data["table"] = TABLE_TRADING;
        $resultEventScheduleId = $this->selectRecords($data);
        
        // this function used for check if event date passed
        $checkEventPassed = $this->main_model->CheckEventPassed($resultEventScheduleId[0]->idEvent);
        
        $A = $idUser;
        $sql = "select * from trading where trade_id= ? and requestBy= ? and status=2";
        $trade = $this->db->query($sql, [
            $trade_id,
            $A
        ]);
        
        if ($trade->num_rows() == 0) {
            return 0;
        }
        $trade = $trade->row();
        $X = $trade->requestToSlot;
        $Y = $trade->requestBySlot;
        $B = $trade->requestBy;
        $this->sendMessageForTrading($B, $trade->idEvent, "Cancelled");
        $sql = "update trading set status=3, requestToNotice=1 where trade_id=? and requestBy= ? and status=2";
        $this->db->query($sql, [
            $trade_id,
            $idUser
        ]);
        return $this->db->affected_rows();
    }

    function reject_trade($trade_id, $idUser)
    {
        $data ['select'] = [
            "idEvent"
        ];
        $data ['where'] = [
            "trade_id" => $trade_id
        ];
        $data["table"] = TABLE_TRADING;
        $resultEventScheduleId = $this->selectRecords($data);
        
        // this function used for check if event date passed
        $checkEventPassed = $this->main_model->CheckEventPassed($resultEventScheduleId[0]->idEvent);
        
        $sql = "select * from trading where trade_id=?";
        $res = $this->db->query($sql, [
            $trade_id
        ])->row();
        $this->sendMessageForTrading($res->requestBy, $res->idEvent, "Cancelled");
        $sql = "update trading set status=0, requestByNotice=1 where trade_id= ? and requestTo=? and status=2";
        $this->db->query($sql, [
            $trade_id,
            $idUser
        ]);
        return $this->db->affected_rows();
    }

    /**
     * sendMessageForTrading()
     * This method send trading related messages
     * 
     * @param integer $to is send to id
     * @param integer $idEvent is schedule id
     */
    function sendMessageForTrading($to, $idEvent, $status)
    {
        $data ["select"] = [
            "u.user_id",
            "ev.title",
            "ev.startTime",
            "es.start_date",
            "ev.timeZone"
        ];
        $data ["table"] = TABLE_REGISTER . ' rg';
        $data ["join"] = [
            TABLE_EVENT_SCHEDULES . ' es' => [
                "es.table_id =  rg.idEvent",
                "INNER"
            ],
            TABLE_EVENT . ' ev' => [
                "ev.idEvent =  es.idEvent",
                "INNER"
            ],
            TABLE_USER . ' u' => [
                "ev.idVenue =  u.user_id",
                "INNER"
            ]
        ];
        $data ["where"] = [
            "es.table_id" => $idEvent
        ];
        $mainRes = $this->selectFromJoin($data);
        $mainRes = $mainRes [0];
        $eventDateEmail = $mainRes->start_date . " " . $mainRes->startTime;
        $eventDateEmail = convertFromGMT($eventDateEmail, "default", $mainRes->timeZone);
        $eventDateEmail = date("jS M, Y", strtotime($eventDateEmail)) . " (" . $mainRes->timeZone . ")";
        $subject = "Trading request has" . $status;
        $msg = "Your trade request for " . $mainRes->title . " on " . $eventDateEmail ." has been " . $status . " by " . $this->session->userdata("UserData")->chrName;
        $this->main_model->saveMessage($to, $this->pUserId, $subject, $msg);
    }
}  

