<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Host_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Used for fetch the list of event credted by venue user
     *
     * @param int $userId ,id of user
     *       
     */
    public function get_venue_events($userId)
    {
        // $sql = "select * from event where idVenue=$idUser and status=1 order by date asc";
        // return $this->db->query ( $sql );
        $data ["select"] = [
            "evt.title",
            "evt.idEvent",
            "evt.startTime",
            "evt.endTime",
            "evt.date",
            "evtSchedule.start_date as scheduleDate",
            "evtSchedule.end_date",
            "evtSchedule.table_id"
        ];
        
        $data ["table"] = TABLE_EVENT . " as evt";
        
        $data ["join"] = [
            TABLE_EVENT_SCHEDULES . " as evtSchedule" => [
                "evtSchedule.idEvent = evt.idEvent",
                "INNER"
            ]
        ];
        
        $data ["where"] = [
            "evt.idVenue" => $userId,
            "evt.status" => 1,
            "evt.haveHost" => 1
        ];
        
        $data ["order"] = 'evtSchedule.start_date';
        
        $result = $this->selectFromJoin($data);
        return $result;
    }

    /**
     * This function returns array of users that are accpeted invitation for this event schedule.
     *
     * @param int $scheduleEventId - schdeul event id
     *       
     * @return array of users
     */
    public function getAcceptedUsers($scheduleEventId)
    {
        $data ["select"] = [
            "u.chrName",
            "u.user_id",
            "rg.isHost"
        ];
        $data ["table"] = TABLE_REGISTER . " rg";
        $data ["where"] = [
            "rg.idEvent" => $scheduleEventId,
            "rg.status" => "Accepted"
        ];
        $data ["join"] = [
            TABLE_USER . " u" => [
                "u.user_id = rg.idPerformer",
                "INNER"
            ]
        ];
        $result = $this->selectFromJoin($data);
        
        return $result;
    }

    /**
     * Used for invite the host
     *
     * @param int $idEventScheduleId , event schedule id
     * @param int $idPerformer, id of perfomer user
     * @param int $idVenue, id of venue user
     * @return number
     */
    function invite_host($idEventScheduleId, $idPerformer, $idVenue)
    {
        $data ["where"] = [
            "idEvent" => $idEventScheduleId,
            "isHost" => 1,
            "status" => 'Accepted'
        ];
        
        $data ["table"] = TABLE_REGISTER;
        
        $resultCount = $this->countRecords($data);
        
        if ($resultCount != 0) {
            return - 1;
        }
        
        unset($data);
        
        // $sql = "select * from register where idPerformer=$idPerformer and status='Accepted' and isHost is null
        // and idEvent=$idEvent and idEvent in (select idEvent from event where idVenue=$idVenue and haveHost=1)";
        
        $data ["select"] = [
            "reg.idRegister"
        ];
        $data ["where"] = [
            "reg.idEvent" => $idEventScheduleId,
            "reg.isHost" => NULL,
            "reg.status" => 'Accepted',
            "reg.idPerformer" => $idPerformer
        ];
        $data ["table"] = TABLE_REGISTER . " as reg";
        
        $resultRegisterData = $this->selectRecords($data);
        
        if (count($resultRegisterData) == 1) {
            
            unset($data);
            
            $data ["where"] = [
                "idRegister" => $resultRegisterData [0]->idRegister
            ];
            $data ["update"] ["isHost"] = 2;
            $data ["update"] ["hostNoticePerformer"] = 1;
            $data ["table"] = TABLE_REGISTER;
            $this->updateRecords($data);
            
            unset($data);
            
            $data ["select"] = [
                "evt.*",
                "evtSchedule.start_date as scheduleDate"
            ];
            $data ["where"] = [
                "evtSchedule.table_id" => $idEventScheduleId
            ];
            
            $data ["table"] = TABLE_EVENT_SCHEDULES . " as evtSchedule";
            
            $data ["join"] = [
                TABLE_EVENT . " as evt" => [
                    "evt.idEvent  = evtSchedule.idEvent",
                    "INNER"
                ]
            ];
            
            $eventInfo = $this->selectFromJoin($data);
            
            // load the profile model for get the venue info.
            
            $this->load->model($this->myvalues->profileDetails ['model'], 'profile_model');
            $venueInfo = $this->profile_model->getUserInformation($eventInfo [0]->idVenue);
            
            /* jS M, Y */
            $eventDateEmail = $eventInfo [0]->scheduleDate . " " . $eventInfo [0]->startTime;
            $eventDateEmail = convertFromGMT($eventDateEmail, "default", $eventInfo [0]->timeZone);
            $timezoneAbbriviation = $this->utility->getAbbreviationOfTimezone($eventInfo [0]->timeZone); //get abbriviation of timezone
            $eventDateEmail = date("jS M, Y", strtotime($eventDateEmail)) . " (" . $timezoneAbbriviation . ")";
            
            $emailData = "&quot;" . ucwords($this->utility->decodeText($venueInfo->chrName)) . "&quot; has invited you to host &quot;" . $this->utility->decodeText($eventInfo [0]->title) . "&quot; on " . $eventDateEmail;
            $link = '<a href="' . SITEURL . $this->myInfo ["controller"] . "/hostReceived" . '" >Click Here</a> to ';
            $this->utility->notification_email($idPerformer, $emailData, false, $link, "New Host Invitation");
            
            $subject = "App Notification: Hosting Invitation Received!";
            $msg = ucwords($this->utility->decodeText($venueInfo->chrName)) . " has invited you to host the upcoming event " . $this->utility->decodeText($eventInfo [0]->title) . " on " . $eventDateEmail . "! Please check your StageGator Hosting Invitations Tab!";
            
            /* Saves message in databse using common function */
            $this->main_model->saveMessage($idPerformer, 810, $subject, $msg);
            
            return 1;
        }
        else {
            return 0;
        }
    }
    
    // called from host manage ment page
    function get_host_requests($idVenue)
    {
        /*
         * $sql = "select R.*, user.chrName from (select event.title, event.idVenue,event.date, register.* from
         * register join event on register.idEvent=event.idEvent where idVenue=$idVenue and haveHost=1 and
         * isHost=1) as R join user where R.idPerformer=user.user_id order by R.hostNoticeVenue desc, idRegister
         * desc";
         */
        $data ["select"] = [
            "register.startTime",
            "register.idEvent",
            "register.isHost",
            "register.doAccept",
            "register.doRank",
            "register.doAttandance",
            "register.doList",
            "register.idRegister",
            "event.title",
            "event.idVenue",
            "event.date",
            "event.idEvent as eventId",
            "u.chrName",
            "u.user_id",
            "u.url",
            "es.start_date as eventScheduleDate",
            "es.end_date"
        ];
        $data ["where"] = [
            "event.idVenue" => $idVenue,
            "haveHost" => 1,
            "isHost" => 1
        ];
        $data ["table"] = TABLE_REGISTER . " as register";
        $data ["join"] = [
            TABLE_EVENT_SCHEDULES . " as es" => [
                "es.table_id = register.idEvent",
                "INNER"
            ],
            TABLE_EVENT . " as event" => [
                "event.idEvent = es.idEvent",
                "INNER"
            ],
            TABLE_USER . " as u" => [
                "u.user_id = register.idPerformer",
                "INNER"
            ]
        ];
        
        $data ["order"] = "register.hostNoticeVenue desc, register.idRegister desc";
        
        $result = $this->selectFromJoin($data);
        
        // $sql = "update register set hostNoticeVenue=0 where idEvent in (select idEvent from event where
        // idVenue=$idVenue)";
        
        $sql = "update " . TABLE_REGISTER . " as reg INNER JOIN " . TABLE_EVENT_SCHEDULES . " as es on es.table_id = reg.idEvent INNER JOIN " . TABLE_EVENT . " as event on event.idEvent = es.idEvent set reg.hostNoticeVenue=0 where event.idVenue=" . $idVenue;
        
        $resultupdate = $this->db->query($sql);
        
        return $result;
    }

    /**
     * Used by venue user to cancel the host that are assigned already
     *
     * @param int $idRegister, register table id
     * @param int $idUser, venue user id
     *       
     * @return number
     */
    function cancel_host_invite($idRegister, $idUser)
    {
        $data ["where"] = [
            "idRegister" => $idRegister
        ];
        $data ["update"] ["isHost"] = 3;
        $data ["update"] ["hostNoticePerformer"] = 1;
        $data ["table"] = TABLE_REGISTER;
        $resultUpdate = $this->updateRecords($data);
        
        if ($resultUpdate == 1) {
            return 1;
        }
        else {
            return 0;
        }
    }

    /**
     * Used for assign the right by venue user
     *
     * @param Array $array , post array
     * @param int $idUser, userid
     */
    function assign_rights($idUser)
    {
        $approval = (! empty($this->input->post('approval'))) ? 1 : 0;
        $ranking = (! empty($this->input->post('ranking'))) ? 1 : 0;
        $attandance = (! empty($this->input->post('attandance'))) ? 1 : 0;
        $list = (! empty($this->input->post('list'))) ? 1 : 0;
        $registerId = $this->utility->decode($this->input->post('hdnRegId'));
        
        unset($data);
        $data ["where"] = [
            "idRegister" => $registerId
        ];
        $data ["update"] ["doAccept"] = $approval;
        $data ["update"] ["doRank"] = $ranking;
        $data ["update"] ["doAttandance"] = $attandance;
        $data ["update"] ["doList"] = $list;
        $data ["table"] = TABLE_REGISTER;
        $this->updateRecords($data);
        $eventInfo = $this->eventInfoByRegisterId($registerId);
        
        $mainRes = ! empty($eventInfo) ? $eventInfo : "";
        
        $eventDateEmail = $mainRes->start_date . " " . $mainRes->startTime;
        $eventDateEmail = convertFromGMT($eventDateEmail, "default", $mainRes->timeZone);
        $eventDateEmail = date("jS M, Y", strtotime($eventDateEmail)) . " (" . $mainRes->timeZone . ")";
        $subject = "App Notification: Host Permissions Changed!";
        $msg = "Your hosting permissions on an upcoming show! " . $mainRes->title . " " . $eventDateEmail . "Please check your StageGator Account!";
        unset($data);
        $data ["select"] = [
            "idPerformer"
        ];
        $data ["where"] = [
            "idRegister" => $registerId
        ];
        $data ["table"] = TABLE_REGISTER;
        $res = $this->selectRecords($data);
        if (! empty($res)) {
            /* Saves message in databse using common function */
            $this->main_model->saveMessage($res [0]->idPerformer, $this->pUserId, $subject, $msg);
        }
        return true;
    }

    /**
     * Used by perfomer to display the host request
     *
     * @param int $idUser, id of perfomer user
     */
    function hostReceivedRequests($idUser)
    {
        $data ["select"] = [
            "register.hostNoticePerformer",
            "register.idRegister",
            "register.isHost",
            "event.title",
            "event.date",
            "event.startTime",
            "event.endTime",
            "event.timeZone",
            "es.start_date as scheduleDate",
            "es.end_date",
            "es.table_id"
        ];
        $data ["where"] = [
            "idPerformer" => $idUser,
            "isHost !=" => NULL,
            "isHost !=" => 0
        ];
        $data ["table"] = TABLE_REGISTER . " as register";
        $data ["join"] = [
            
            TABLE_EVENT_SCHEDULES . " es" => [
                "register.idEvent = es.table_id",
                "INNER"
            ],
            TABLE_EVENT . " event" => [
                "es.idEvent = event.idEvent",
                "INNER"
            ]
        ];
        $data ["order"] = "hostNoticePerformer desc, idRegister desc";
        $result = $this->selectFromJoin($data);
        
        // $sql = "update register set hostNoticePerformer=0 where idPerformer=$idUser";
        
        unset($data);
        
        $data ["where"] = [
            "idPerformer" => $idUser
        ];
        $data ["update"] ["hostNoticePerformer"] = 0;
        $data ["table"] = TABLE_REGISTER;
        $this->updateRecords($data);
        
        return $result;
    }

    /**
     * used for accept the host request by perfomer
     *
     * @param int $idRegister , id from register tabel
     *       
     * @return number
     */
    function accept_host($idRegister)
    {
        // fetch the eventschedule id to check event date passed
        $data ["select"] = [
            "idEvent"
        ];
        $data ["where"] = [
            "idRegister" => $idRegister
        ];
        $data ["table"] = TABLE_REGISTER;
        $resultEventScheduleId = $this->selectRecords($data);
        
        // this function used for check if event date passed
        $checkEventPassed = $this->main_model->CheckEventPassed($resultEventScheduleId [0]->idEvent);
        
        unset($data);
        
        // $sql = "select * from register where idEvent=(select idEvent from register where idRegister=$idRegister)
        // and isHost=1 and status='Accepted'";
        $data ["where"] = [
            "idRegister" => $idRegister,
            "isHost" => 1,
            "status" => 'Accepted'
        ];
        $data ["table"] = TABLE_REGISTER;
        $resultCountRegister = $this->countRecords($data);
        
        if ($resultCountRegister != 0) {
            return 0;
        }
        unset($data);
        
        // update the register table
        $data ["where"] = [
            "idRegister" => $idRegister,
            "isHost" => 2
        ];
        $data ["update"] ["isHost"] = 1;
        $data ["update"] ["hostNoticeVenue"] = 1;
        $data ["table"] = TABLE_REGISTER;
        $resultUpdate = $this->updateRecords($data);
        
        if($resultUpdate === false)
        {
            return 0;
        }
        
        unset($data);
        // fetch the register data
        
        $data ["select"] = [
            'idRegister',
            'idEvent',
            'idPerformer'
        ];
        $data ["where"] = [
            "idRegister" => $idRegister
        ];
        $data ["table"] = TABLE_REGISTER;
        
        $resultRegister = $this->selectRecords($data);
        $resultRegister = $resultRegister [0];
        
        unset($data);
        
        // $sql = "update register set isHost=3 where idEvent=$row->idEvent and isHost=2 and
        // idRegister!=$row->idRegister";
        
        // update the register table
        $data ["where"] = [
            "idRegister!=" => $resultRegister->idRegister,
            "isHost" => 2,
            "idEvent" => $resultRegister->idEvent
        ];
        $data ["update"] ["isHost"] = NULL;
        $data ["table"] = TABLE_REGISTER;
        $resultRegisterUpdate = $this->updateRecords($data);
        
        unset($data);
        
        // $sql = "select * from timetable where idEvent=$row->idEvent and idPerformer=$row->idPerformer";
        // $user_current_slot = $this->db->query($sql);
        
        // get the data from TimeTable
        
        $data ["select"] = [
            "slot_id",
            "table_id"
        ];
        $data ["where"] = [
            "idEvent" => $resultRegister->idEvent,
            "idPerformer" => $resultRegister->idPerformer
        ];
        $data ["table"] = TABLE_TIMETABLE;
        $user_current_slot = $this->selectRecords($data);
        
        if (count($user_current_slot) == 0) {
            // Slot for current user not decided yet
            
            // $sql = "select * from timetable where idEvent=$row->idEvent and slot_id=1";
            // $slot1 = $this->db->query($sql)->row();
            
            unset($data);
            $data ["select"] = [
                "idPerformer",
                "table_id"
            ];
            $data ["where"] = [
                "idEvent" => $resultRegister->idEvent,
                "slot_id" => 1
            ];
            $data ["table"] = TABLE_TIMETABLE;
            
            $slot1 = $this->selectRecords($data);
            $slot1 = $slot1 [0];
            
            if ($slot1->idPerformer == 0) {
                // First Slot is Empty
                // $sql = "update timetable set idPerformer=$row->idPerformer where table_id=$slot1->table_id";
                unset($data);
                
                // update the timetable table
                $data ["where"] = [
                    "table_id" => $slot1->table_id
                ];
                $data ["update"] ["idPerformer"] = $resultRegister->idPerformer;
                $data ["table"] = TABLE_TIMETABLE;
                $this->updateRecords($data);
                
                unset($data);
                // $sql = "update register set updateSlotNoticeToPerformer=1 where idRegister=$idRegister";
                // update the register table
                $data ["where"] = [
                    "idRegister" => $idRegister
                ];
                $data ["update"] ["updateSlotNoticeToPerformer"] = 1;
                $data ["table"] = TABLE_REGISTER;
                $this->updateRecords($data);
            }
            else {
                // First slot is not emtpy, look for first available empty slot
                // $sql = "select * from timetable where idEvent=$row->idEvent and idPerformer=0";
                // $empty_slot = $this->db->query($sql)->row();
                unset($data);
                $data ["select"] = [
                    "table_id"
                ];
                $data ["where"] = [
                    "idEvent" => $resultRegister->idEvent,
                    "idPerformer" => 0
                ];
                $data ["table"] = TABLE_TIMETABLE;
                $empty_slot = $this->selectRecords($data);
                
                if(empty($empty_slot)) {
                    $this->hostMessages($idRegister,"Accepted");
                    return 1;
                }
                $empty_slot = $empty_slot [0];
                
                unset($data);
                // $sql = "update timetable set idPerformer=$row->idPerformer where table_id=$slot1->table_id";
                // update the timetable table
                $data ["where"] = [
                    "table_id" => $slot1->table_id
                ];
                $data ["update"] ["idPerformer"] = $resultRegister->idPerformer;
                $data ["table"] = TABLE_TIMETABLE;
                $this->updateRecords($data);
                
                // $sql = "update register set updateSlotNoticeToPerformer=1 where idRegister=$idRegister";
                unset($data);
                $data ["where"] = [
                    "idRegister" => $idRegister
                ];
                $data ["update"] ["updateSlotNoticeToPerformer"] = 1;
                $data ["table"] = TABLE_REGISTER;
                $this->updateRecords($data);
                
                unset($data);
                // $sql = "update timetable set idPerformer=$slot1->idPerformer where
                // table_id=$empty_slot->table_id";
                $data ["where"] = [
                    "table_id" => $empty_slot->table_id
                ];
                $data ["update"] ["idPerformer"] = $slot1->idPerformer;
                $data ["table"] = TABLE_TIMETABLE;
                $this->updateRecords($data);
                
                unset($data);
                // $sql = "update register set updateSlotNoticeToPerformer=1 where idEvent=$row->idEvent and
                // idPerformer=$slot1->idPerformer and status='Accepted'";
                $data ["where"] = [
                    "idEvent" => $resultRegister->idEvent,
                    "idPerformer" => $slot1->idPerformer,
                    "status" => "Accepted"
                ];
                $data ["update"] ["updateSlotNoticeToPerformer"] = 1;
                $data ["table"] = TABLE_REGISTER;
                $this->updateRecords($data);
            }
        }
        else {
            $user_current_slot = $user_current_slot [0];
            if ($user_current_slot->slot_id != 1) {
                
                unset($data);
                // $sql = "select * from timetable where idEvent=$row->idEvent and slot_id=1";
                // $slot1 = $this->db->query($sql)->row();
                $data ["select"] = [
                    "idPerformer",
                    "table_id"
                ];
                $data ["where"] = [
                    "idEvent" => $resultRegister->idEvent,
                    "slot_id" => 1
                ];
                $data ["table"] = TABLE_TIMETABLE;
                
                $slot1 = $this->selectRecords($data);
                $slot1 = $slot1 [0];
                
                if ($slot1->idPerformer == 0) {
                    // First Slot is Empty
                    unset($data);
                    // $sql = "update timetable set idPerformer=$row->idPerformer where table_id=$slot1->table_id";
                    
                    $data ["where"] = [
                        "table_id" => $slot1->table_id
                    ];
                    $data ["update"] ["idPerformer"] = $resultRegister->idPerformer;
                    $data ["table"] = TABLE_TIMETABLE;
                    $this->updateRecords($data);
                    
                    unset($data);
                    // $sql = "update register set updateSlotNoticeToPerformer=1 where idRegister=$idRegister";
                    $data ["where"] = [
                        "idRegister" => $idRegister
                    ];
                    $data ["update"] ["updateSlotNoticeToPerformer"] = 1;
                    $data ["table"] = TABLE_REGISTER;
                    $this->updateRecords($data);
                    
                    unset($data);
                    // $sql = "update timetable set idPerformer=0 where table_id=$user_current_slot->table_id";
                    $data ["where"] = [
                        "table_id" => $user_current_slot->table_id
                    ];
                    $data ["update"] ["idPerformer"] = 0;
                    $data ["table"] = TABLE_TIMETABLE;
                    $this->updateRecords($data);
                }
                else { // First slot is not emtpy, Trade Slots
                    
                    unset($data);
                    // $sql = "update timetable set idPerformer=$row->idPerformer where table_id=$slot1->table_id";
                    $data ["where"] = [
                        "table_id" => $slot1->table_id
                    ];
                    $data ["update"] ["idPerformer"] = $resultRegister->idPerformer;
                    $data ["table"] = TABLE_TIMETABLE;
                    $this->updateRecords($data);
                    
                    unset($data);
                    // $sql = "update register set updateSlotNoticeToPerformer=1 where idRegister=$idRegister";
                    $data ["where"] = [
                        "idRegister" => $idRegister
                    ];
                    $data ["update"] ["updateSlotNoticeToPerformer"] = 1;
                    $data ["table"] = TABLE_REGISTER;
                    $this->updateRecords($data);
                    
                    unset($data);
                    // $sql = "update timetable set idPerformer=$slot1->idPerformer where
                    // table_id=$user_current_slot->table_id";
                    
                    $data ["where"] = [
                        "table_id" => $user_current_slot->table_id
                    ];
                    $data ["update"] ["idPerformer"] = $slot1->idPerformer;
                    $data ["table"] = TABLE_TIMETABLE;
                    $this->updateRecords($data);
                    
                    unset($data);
                    // $sql = "update register set updateSlotNoticeToPerformer=1 where idEvent=$row->idEvent and
                    // idPerformer=$slot1->idPerformer and status='Accepted'";
                    $data ["where"] = [
                        "idEvent" => $resultRegister->idEvent,
                        "idPerformer" => $slot1->idPerformer,
                        "status" => 'Accepted'
                    ];
                    $data ["update"] ["updateSlotNoticeToPerformer"] = 1;
                    $data ["table"] = TABLE_REGISTER;
                    $this->updateRecords($data);
                }
            }
        }
        
        $this->hostMessages($idRegister, "Accepted");
        
        return 1;
    }

    /**
     * Used for reject the invitation host request by perfomer
     *
     * @param int $idRegister , id from register tabel
     * @return unknown
     */
    function reject_host($idRegister)
    {
        // fetch the eventschedule id to check event date passed
        $data ["select"] = [
            "idEvent"
        ];
        $data ["where"] = [
            "idRegister" => $idRegister
        ];
        $data ["table"] = TABLE_REGISTER;
        $resultEventScheduleId = $this->selectRecords($data);
        
        // this function used for check if event date passed
        $checkEventPassed = $this->main_model->CheckEventPassed($resultEventScheduleId [0]->idEvent);
        
        // update the register tabel
        $data ["where"] = [
            "idRegister" => $idRegister
        ];
        $data ["update"] ["isHost"] = 4;
        $data ["update"] ["hostNoticeVenue"] = 1;
        $data ["table"] = TABLE_REGISTER;
        $resultUpdate = $this->updateRecords($data);
        $this->hostMessages($idRegister, "Rejected");
        return $resultUpdate;
    }

    /**
     * Used for check the reanking is completed for event or not
     * called from view part
     *
     * @param int $evenScheduleid, event schedule id
     * @return boolean
     */
    function check_ranking_completed_for_event($evenScheduleid)
    {
        $sql = "select * from " . TABLE_REGISTER . " where (attandance='N/A' or rank is NULL) and status='Accepted' and idEvent=" . $evenScheduleid;
        $res = $this->db->query($sql)->num_rows();
        if ($res > 0) {
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * hostMessages()
     * This method send status related message when a performer accept or reject a request
     * 
     * @param integer $idRegister is register table's primery key
     *       
     * @param return the email status $status
     */
    public function hostMessages($idRegister, $status)
    {
        $mainRes = $this->eventInfoByRegisterId($idRegister);
        if (! empty($mainRes)) {
            $eventDateEmail = $mainRes->start_date . " " . $mainRes->startTime;
            $eventDateEmail = convertFromGMT($eventDateEmail, "default", $mainRes->timeZone);
            $timezoneAbbriviation = $this->utility->getAbbreviationOfTimezone($mainRes->timeZone); //get abbriviation of timezone
            $eventDateEmail = date("jS M, Y", strtotime($eventDateEmail)) . " (" . $timezoneAbbriviation . ")";
            $to = $mainRes->idVenue;
            
            $eventTitle = $mainRes->title;
            $subject = "App Notification: Host Request " . $status . "!";
            $msg = $this->utility->decodeText($this->session->userdata("UserData")->chrName) . " has " . $status . " your request to  Host " . $this->utility->decodeText($eventTitle) . "  " . $eventDateEmail . "!";
            
            /* Saves message in databse using common function */
            $this->main_model->saveMessage($to, $this->pUserId, $subject, $msg);
            // send mail 
            $subjectEmail = "Host Request " . $status . "!";
            $this->utility->notification_email($to, $msg, false, "Please", $subjectEmail);
        }
    }

    /**
     * eventInfoByRegisterId()
     * This method return
     * 
     * @param unknown $idRegister
     * @return unknown
     */
    function eventInfoByRegisterId($idRegister)
    {
        $this->load->model($this->myvalues->eventDetails ['model'], 'event_model');
        $idEvent = $this->event_model->getEventIdByRegisterId($idRegister);
        $getEvent = $this->event_model->getEventSchedule($idEvent);
        return $getEvent;
    }
}  

