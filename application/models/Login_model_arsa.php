<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * checkSuperAdminLogin()
     * This function is used to Super Admin Login process
     *
     * @param string $emailAddress unique emailAddress for user
     * @param string $password password for user
     */
    public function checkSuperAdminLogin($emailAddress, $password)
    {
        
        // define result array
        $result = [];
        
        // authenticate emailAddress and password and get data
        $data ["select"] = [
            "i_id",
            "v_username",
            "v_person_name",
            "e_status"
        ];
        $data ['where'] = [
            'v_username' => $emailAddress,
            'v_password' => $this->utility->encode($password)
        ];
        $data ["table"] = TABLE_MASTER_USERS;
        
        $result = $this->selectRecords($data);
        
        // set default error message
        $returnMsg = $this->lang->line('error_invalid_email_address_password');
        
        // update entry for dt_last_login_datetime if super admin login
        if (! empty($result [0]->i_id)) {
            
            // status active than process for update login and session set
            if ($result [0]->e_status == "Active") {
                
                $dataUpdate ['table'] = TABLE_MASTER_USERS;
                
                $dataUpdate ['where'] = [
                    'i_id' => $result [0]->i_id
                ];
                
                $dataUpdate ['update'] = [
                    'dt_last_login_datetime' => $this->datetime,
                    'v_ip' => $this->ip
                ];
                
                $this->updateRecords($dataUpdate);
                
                // set super admin data in session
                $superAdminData = [
                    'userId' => $result [0]->i_id,
                    'username' => $result [0]->v_username,
                    'userType' => 'SuperAdmin',
                    'isLoggedIn' => TRUE,
                    'dashboardURL' => SITEURL . $this->myvalues->superAdminDashboardDetails ["controller"]
                ];
                
                // set data in session
                $this->session->set_userdata($superAdminData);
                
                // set message blank
                $returnMsg = true;
            }
            else if ($result [0]->e_status == "Inactive") {
                
                // account is inactive set message for disable account
                $returnMsg = $this->lang->line('error_disabled_account');
            }
        }
        
        // return message
        return $returnMsg;
    }

    /**
     * checkAdminLogin()
     * This function is used to Admin Login process
     *
     * @param string $emailAddress unique emailAddress for user
     * @param string $password password for user
     */
    public function checkAdminLogin($emailAddress, $password)
    {
        
        // define result array
        $result = [];
        
        // authenticate emailAddress and password and get data
        $data ["select"] = [
            "emp.i_id",
            "emp.v_email",
            "emp.i_branch_id",
            'CONCAT_WS(" ", emp.v_first_name,emp.v_last_name) AS username',
            "emp.e_role",
            "emp.e_status",
            "emp.i_employee_group_id",
            "branch.e_status As branchstatus",
            "employeegroup.e_status AS employeestatus"
        ];
        // join for get the branch status & employee group
        $data ["join"] = [
            TABLE_MASTER_BRANCHES . " As branch" => [
                "emp.i_branch_id = branch.i_id",
                "LEFT"
            ],
            TABLE_MASTER_EMPLOYEE_GROUP . " As employeegroup" => [
                "emp.i_employee_group_id = employeegroup.i_id",
                "LEFT"
            ]
        ];
        
        $data ["table"] = TABLE_MASTER_EMPLOYEES . " As emp";
        
        $data ['where'] = [
            'emp.v_email' => $emailAddress,
            'emp.v_password' => $this->utility->encode($password)
        ];
        
        $data ["order"] = " FIELD( emp.e_status ,'Inactive','Active','Deleted')";
        
        $result = $this->selectFromJoin($data);
        /* $result = $this->selectRecords($data); */
        
        // set default error message
        $returnMsg = $this->lang->line('error_invalid_email_address_password');
        
        // make entry for dt_last_login_datetime if super admin login
        if (! empty($result [0]->i_id)) {
            
            // currently for admin branch status is blank because branch id = 0 for admin
            // if branch status is active then do login
            if ($result [0]->branchstatus != "Deleted" && $result [0]->branchstatus != "Inactive" && $result [0]->employeestatus != "Deleted" && $result [0]->employeestatus != "Inactive") {
                
                // status active than process for update login and session set
                if ($result [0]->e_status == "Active") {
                    
                    // insert data into login history table
                    $dataInsert ['table'] = TABLE_LOGIN_HISTORY;
                    
                    $dataInsert ['insert'] = [
                        'i_branch_id' => $result [0]->i_branch_id,
                        'i_user_id' => $result [0]->i_id,
                        'e_role' => $result [0]->e_role,
                        'dt_created_datetime' => $this->datetime,
                        'v_ip' => $this->ip
                    ];
                    
                    // currently we are not enter the data in history tabel
                    $response = $this->insertRecord($dataInsert);
                    
                    // set array for admin & employee data in session
                    // set the controller based on role
                    $controller = ($result [0]->e_role == 'Admin') ? $this->myvalues->adminDashboardDetails ["controller"] : $this->myvalues->employeeDashboardDetails ["controller"];
                    
                    $adminData = [
                        'userId' => $result [0]->i_id,
                        'username' => $result [0]->username,
                        'userType' => $result [0]->e_role,
                        'isLoggedIn' => TRUE,
                        'branchId' => $result [0]->i_branch_id,
                        'dashboardURL' => SITEURL . $controller,
                        'groupId' => $result [0]->i_employee_group_id
                    ];
                    
                    // set data in session
                    $this->session->set_userdata($adminData);
                    
                    // set message blank
                    $returnMsg = true;
                }
                else if ($result [0]->e_status == "Inactive") {
                    
                    // account is inactive set message for disable account
                    $returnMsg = $this->lang->line('error_disabled_account');
                }
            }
            else if ($result [0]->branchstatus == "Deleted" || $result [0]->employeestatus == "Deleted") {
                // Branch is Deleted set message invalid email password
                $returnMsg = $this->lang->line('error_invalid_email_address_password');
            }
            else if ($result [0]->branchstatus == "Inactive" || $result [0]->employeestatus == "Inactive") {
                // Branch is inactive set message for disable account
                $returnMsg = $this->lang->line('error_disabled_account');
            }
        }
        
        // return message
        return $returnMsg;
    }

    /**
     * validateSuperAdminUser()
     * This function is used to Super Admin Forogt Password process
     *
     * @param string $emailAddress unique emailAddress for user
     */
    public function validateSuperAdminUser($emailAddress)
    {
        
        // define result array
        $result = [];
        
        // authenticate emailAddress and get data
        $data ["select"] = [
            'i_id',
            'e_status'
        ];
        $data ['where'] = [
            'v_username' => $emailAddress
        ];
        $data ["table"] = TABLE_MASTER_USERS;
        
        $result = $this->selectRecords($data);
        
        // set default error message
        $returnMsg = $this->lang->line('error_invalid_email_address');
        
        // send email for reset password if user is active or send error message if deleted or inactive
        if (! empty($result [0]->i_id)) {
            
            // status active than process for send mail for rest password
            if ($result [0]->e_status == "Active") {
                
                $userData = [
                    'userId' => $result [0]->i_id,
                    'timestamp' => $this->datetime,
                    'hostName' => SITEURL
                ];
                
                $resetToken = $this->utility->encode(json_encode($userData));
                
                $resetURL = anchor(SITEURL . $this->myInfoRoutes ['resetPassword'] ['controller'] . '?token=' . $resetToken, SITEURL . $this->myInfoRoutes ['resetPassword'] ['controller'] . '?token=' . $resetToken);
                
                // Email content
                $emailContent = $this->load->view('email_template/forgot_password', [], true);
                
                $search = [
                    '#RESET_LINK#'
                ];
                $replace = [
                    $resetURL
                ];
                $emailFinalData = str_replace($search, $replace, $emailContent);
                
                // send reset url in mail
                $emailData ['to'] = $emailAddress;
                $emailData ['from'] = SITE_EMAIL;
                $emailData ['from_title'] = EMAIL_TITLE;
                $emailData ['subject'] = $this->lang->line('error_reset_your_password');
                $emailData ['extraData'] = "";
                $emailData ['message'] = $emailFinalData;
                $mailResponse = $this->utility->sendMailSMTP($emailData); // call send mail function+
                
                unset($dataUpdate);
                // update updated datetime in table
                $dataUpdate ['table'] = TABLE_MASTER_USERS;
                
                $dataUpdate ['where'] = array(
                    'i_id' => $result [0]->i_id
                );
                
                $dataUpdate ['update'] = [
                    'dt_updated_datetime' => $this->datetime,
                    'v_ip' => $this->ip
                ];
                
                $this->updateRecords($dataUpdate);
                
                // set message for successfully mail sent
                if ($mailResponse) {
                    
                    $returnMsg = true;
                }
            }
            else if ($result [0]->e_status == "Inactive") {
                
                // account is inactive set message for disable account
                $returnMsg = $this->lang->line('error_disabled_account');
            }
        }
        
        // return message
        return $returnMsg;
    }

    /**
     * validateAdminUser()
     * This function is used to Admin/Employee Forogt Password process
     *
     * @param string $emailAddress unique emailAddress for user
     */
    public function validateAdminUser($emailAddress)
    {
        
        // define result array
        $result = [];
        
        // authenticate emailAddress and get data
        $data ["select"] = [
            'i_id',
            'e_status'
        ];
        $data ['where'] = [
            'v_email' => $emailAddress
        ];
        $data ["table"] = TABLE_MASTER_EMPLOYEES;
        
        $result = $this->selectRecords($data);
        
        // set default error message
        $returnMsg = $this->lang->line('error_invalid_email_address');
        
        // send email for reset password if user is active or send error message if deleted or inactive
        if (! empty($result [0]->i_id)) {
            
            // status active than process for send mail for reset password
            if ($result [0]->e_status == "Active") {
                
                $userData = [
                    'userId' => $result [0]->i_id,
                    'timestamp' => $this->datetime,
                    'hostName' => SITEURL
                ];
                
                $resetToken = $this->utility->encode(json_encode($userData));
                
                $resetURL = anchor(SITEURL . $this->myInfoRoutes ['resetPassword'] ['controller'] . '?token=' . $resetToken, SITEURL . $this->myInfoRoutes ['resetPassword'] ['controller'] . '?token=' . $resetToken);
                
                // Email content
                $emailContent = $this->load->view('email_template/forgot_password', [], true);
                
                $search = [
                    '#RESET_LINK#'
                ];
                $replace = [
                    $resetURL
                ];
                $emailFinalData = str_replace($search, $replace, $emailContent);
                
                // send reset url in mail
                $emailData ['to'] = $emailAddress;
                $emailData ['from'] = SITE_EMAIL;
                $emailData ['from_title'] = EMAIL_TITLE;
                $emailData ['subject'] = $this->lang->line('error_reset_your_password');
                $emailData ['extraData'] = "";
                $emailData ['message'] = $emailFinalData;
                $mailResponse = $this->utility->sendMailSMTP($emailData); // call send mail function+
                
                unset($dataUpdate);
                // update updated datetime in table
                $dataUpdate ['table'] = TABLE_MASTER_EMPLOYEES;
                
                $dataUpdate ['where'] = array(
                    'i_id' => $result [0]->i_id
                );
                
                $dataUpdate ['update'] = [
                    'dt_updated_datetime' => $this->datetime,
                    'v_ip' => $this->ip
                ];
                
                $this->updateRecords($dataUpdate);
                
                // set message for successfully mail sent
                if ($mailResponse) {
                    
                    $returnMsg = true;
                }
            }
            else if ($result [0]->e_status == "Inactive") {
                
                // account is inactive set message for disable account
                $returnMsg = $this->lang->line('error_disabled_account');
            }
        }
        
        // return message
        return $returnMsg;
    }

    /**
     * resetSuperAdminPassword()
     * This function is used to Super Admin reset password process
     *
     * @param string $resetToken resetToken for user
     */
    public function resetSuperAdminPassword($userId, $timeStamp)
    {
        $returnMsg = $userId;
        // check user data and match last updated time
        $data ["select"] = [
            'i_id',
            'dt_updated_datetime'
        ];
        
        $data ['where'] = [
            'i_id' => $userId,
            'e_status' => 'Active'
        ];
        
        $data ["table"] = TABLE_MASTER_USERS;
        
        $result = $this->selectRecords($data);
        
        // check last updated time greater than token timestamp
        if (! empty($result [0]->dt_updated_datetime) && strtotime($result [0]->dt_updated_datetime) > strtotime($timeStamp)) {
            // set error message for expire token
            $returnMsg = $this->lang->line('error_reset_token_expired');
        }
        
        // return message
        return $returnMsg;
    }

    /**
     * resetAdminPassword()
     * This function is used to Admin/Employee reset password process
     *
     * @param string $resetToken resetToken for user
     */
    public function resetAdminPassword($userId, $timeStamp)
    {
        $returnMsg = $userId;
        
        // check user data and match last login time
        $data ["select"] = [
            'i_id',
            'dt_updated_datetime'
        ];
        $data ['where'] = [
            'i_id' => $userId,
            'e_status' => 'Active'
        ];
        $data ["table"] = TABLE_MASTER_EMPLOYEES;
        
        $result = $this->selectRecords($data);
        
        // check last updated time greater than reset timestamp
        if (! empty($result [0]->dt_updated_datetime) && strtotime($result [0]->dt_updated_datetime) > strtotime($timeStamp)) {
            
            // set error message for expire token
            $returnMsg = $this->lang->line('error_reset_token_expired');
        }
        
        // return message
        return $returnMsg;
    }

    /**
     * superAdminChangePassword()
     * This function is used to Super Admin reset password process
     *
     * @param string $userId userId for user
     * @param string $password password for user
     */
    public function superAdminChangePassword($userId, $password)
    {
        
        // define result array
        $returnMsg = false;
        
        // update password and updated datetime in table
        $dataUpdate ['table'] = TABLE_MASTER_USERS;
        
        $dataUpdate ['where'] = array(
            'i_id' => $userId
        );
        
        $dataUpdate ['update'] = [
            'v_password' => $this->utility->encode($password),
            'dt_updated_datetime' => $this->datetime,
            'v_ip' => $this->ip
        ];
        
        $updatePassword = $this->updateRecords($dataUpdate);
        
        // check password updated or not
        if ($updatePassword) {
            
            $returnMsg = true;
        }
        
        // return message
        return $returnMsg;
    }

    /**
     * adminChangePassword()
     * This function is used to Admin/Employee reset password process
     *
     * @param string $userId userId for user
     * @param string $password password for user
     */
    public function adminChangePassword($userId, $password)
    {
        
        // define result flag
        $returnMsg = false;
        
        // update password and updated datetime in table
        $dataUpdate ['table'] = TABLE_MASTER_EMPLOYEES;
        
        $dataUpdate ['where'] = array(
            'i_id' => $userId
        );
        
        $dataUpdate ['update'] = [
            'v_password' => $this->utility->encode($password),
            'dt_updated_datetime' => $this->datetime,
            'v_ip' => $this->ip
        ];
        
        $updatePassword = $this->updateRecords($dataUpdate);
        
        // check password updated or not
        if ($updatePassword) {
            
            $returnMsg = true;
        }
        
        // return message
        return $returnMsg;
    }

    /**
     * function is used to check store is active or not for login user
     * 
     * @return unknown
     */
    public function checkStoereActive()
    {
        $data ['select'] = [
            'i_id'
        ];
        if (STORE_INDEX != 0) {
            $data ['where'] = [
                'i_id' => STORE_INDEX,
                'e_status !=' => 'Deleted',
                'e_status !=' => 'Inactive'
            ];
        }
        
        $data ['table'] = DEFAULT_SUPER_ADMIN_DATABASE . '.' . TABLE_STORE_DETAILS;
        
        $result = $this->selectRecords($data);
        
        return $result;
    }
}
