<?php

class Webhook_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * updateAccountStatus
     * This method called when webhook called and a stripe user account status will be update
     *
     * @param array $data is array value set in controller
     * @return after update records send response to controller
     */
    public function updateAccountStatus($data)
    {
        $data ['update'] ['stripe_reason'] = $data ['reason'];
        $data ['update'] ['stripe_status'] = $data ['status'];
        $data ['update'] ['stripe_timestamp'] = $data ['created'];
        
        $data ['where'] = [
            'stripe_account_id' => $data ['accountId'],
            'stripe_timestamp < ' => $data ['created']
        ];
        $data ['table'] = TABLE_USER;
        $res = $this->updateRecords($data);
        
        return $res;
    }

    /**
     * getUserInfoByStripeAccountId
     * This method simply return user email address and name by account id
     *
     * @param string $accountId stripe account id
     *       
     * @return either return data result if get or return null
     */
    public function getUserInfoByStripeAccountId($accountId)
    {
        $data ['select'] = [
            'user_name',
            'chrName'
        ];
        $data ['where'] = [
            'stripe_account_id' => $accountId
        ];
        $data ['table'] = TABLE_USER;
        $res = $this->selectRecords($data);
        
        return count($res) > 0 ? $res [0] : NULL;
    }

    /**
     * updateChargeStatus
     * This method is used when a tipping status get in controller we update it where tnx id is below txn id
     *
     * @param array $data is array value created in controller
     *       
     * @return update response
     */
    public function updateChargeStatus($data)
    {
        $data ['update'] ['stripe_status'] = $data ['status'];
        $data ['where'] = [
            'txn_id' => $data ['txnId']
        ];
        $data ['table'] = TABLE_TIPPING_HISTORY;
        $res = $this->updateRecords($data);
        
        return $res;
    }

    /**
     * getUserTippingUserInfo
     * this method get performer and fan id to get their info
     *
     * @param is charge id $chargeId
     * @return return either result of ids or return null
     */
    public function getUserTippingUserInfo($chargeId)
    {
        $data ['select'] = [
            'from_user_id',
            'to_user_id'
        ];
        $data ['where'] = [
            'txn_id' => $chargeId
        ];
        $data ['table'] = TABLE_TIPPING_HISTORY;
        $res = $this->selectRecords($data);
        
        return count($res) > 0 ? $res [0] : NULL;
    }

    /**
     * updateStripeBalance
     * this method update stripe balance in user table it always update available balance and pending balance first
     * + both and update
     *
     * @param string $userId is user id where stripe balance will update
     * @param integer $amount is amount to be update
     *       
     * @return boolean always true after update
     */
    public function updateStripeBalance($userId, $amount, $created)
    {
        $data ['update'] ['d_stripe_balance'] = $amount;
        $data ['update'] ['stripe_timestamp'] = $created;
        $data ['where'] = [
            'user_id' => $userId
        ];
        $data ['table'] = TABLE_USER;
        $this->updateRecords($data);
        
        return true;
    }
    

    /**
     * Used for get the user's stripe Account secret key.
     *
     * @param int $userId , id of user
     *       
     * @return string $result, that contain the strip accont secret key.
     */
    public function getStripeKey($accountId)
    {
        $data ['select'] = [
            'v_secret_key',
            'user_id'
        ];
        $data ['where'] = [
            'stripe_account_id' => $accountId
        ];
        $data ['table'] = TABLE_STRIPE_ACCOUNT_DETAILS;
        $result = $this->selectRecords($data);
        
        return $result [0];
    }

    /**
     * Used for update the last webhook responce time stemp for log purpuse
     * & also this is called from saveAccountUpdateResponse method only
     *
     * @param string $accountId stripe account id
     * @param string $webhookTimestemp stripe webhook time stemp at which webhook created
     *       
     * @return boolean $resultUpdate
     */
    public function updateWebhookLog($accountId, $webhookTimestemp)
    {
        /*
         * $data ['update'] ['last_webhook_datetime'] = $webhookTimestemp;
         * $data ['table'] = TABLE_STRIPE_ACCOUNT_DETAILS;
         * $data ['where'] = [
         * 'last_webhook_datetime <' => $webhookTimestemp,
         * 'stripe_account_id' => $accountId
         * ];
         * $resultUpdate = $this->updateRecords($data);
         */
        $resultUpdate = $this->db->query("update " . TABLE_STRIPE_ACCOUNT_DETAILS . " set last_webhook_datetime = '" . $webhookTimestemp . "' WHERE  (last_webhook_datetime < " . $webhookTimestemp . " OR last_webhook_datetime is null) AND stripe_account_id ='" . $accountId ."'");
        return $resultUpdate;
    }
    
    
    /**
     * updateAddChargeStatus()
     * This method is used when a recurring tipping status get in controller we update it where tnx id
     *
     * @param array $data is array value created in controller
     *
     * @return update response
     */
    public function updateAddChargeStatus($datas)
    {
        
        $data ['select'] = ['*'];
        $data ['where'] = [
            'from_user_id' => $datas['fromUserID'],
            'to_user_id' => $datas['toUserId'],
            'txn_id' => $datas['txnId']
        ];
        $data ['table'] = TABLE_TIPPING_HISTORY;
        $recordsFound = $this->selectRecords($data);
        
        unset($data);
        
        if(!empty($recordsFound)){
        
            $data ['update'] ['stripe_status'] = $datas['status'];
            $data ['where'] = [
                'txn_id' => $datas['txnId']
            ];
            $data ['table'] = TABLE_TIPPING_HISTORY;
            $res = $this->updateRecords($data);
            
        }else{
            
            $data ['insert'] ['from_user_id'] = $datas['fromUserID'];
            $data ['insert'] ['to_user_id'] = $datas['toUserId'];
            $data ['insert'] ['amount'] = $datas['amount'];
            $data ['insert'] ['txn_id'] = $datas['txnId'];
            $data ["insert"] ["dt_created_datetime"] = date("Y-m-d H:i:s");
            $data ["insert"] ["is_active"] = '1';
            $data ['insert'] ['v_ip'] = $this->ip;
            $data ['insert'] ['stripe_status'] = $datas['status'];
            $data ['table'] = TABLE_TIPPING_HISTORY;
            $res = $this->insertRecord($data);
        }
        
        return $res;
    }
    
    /**
     * updateSubscriptionStatus()
     * This method is used when a plan subscription status get in controller we update it where subscription id
     *
     * @param array $data is array value created in controller
     *
     * @return update response
     */
    public function updateSubscriptionStatus($datas)
    {
        
        $data ['where'] = [
            'subscriptionId' => $datas['subId'],
            'performer_id' => $datas['toUserId'],
            'fans_id' => $datas['fromUserID']
        ];
        $data ['update'] ['recurring_status'] = $datas['status'];
        $data ["update"] ["updated_date"] = date("Y-m-d H:i:s");
        $data ['update'] ['v_ip'] = $this->ip;
        $data ['table'] = TABLE_RECURRING_TIP_INFO;
        $res = $this->updateRecords($data);
       
        return $res;
    }
    
    /**
     * Used for get the user's stripe Account secret key.
     *
     * @param int $userId , id of user
     *
     * @return string $result, that contain the strip accont secret key.
     */
    public function getStripeKeyByUserId($userId)
    {
        $data ['select'] = [
            'v_secret_key',
            'stripe_account_id'
        ];
        $data ['where'] = [
            'user_id' => $userId
        ];
        $data ['table'] = TABLE_STRIPE_ACCOUNT_DETAILS;
        $result = $this->selectRecords($data);
        
        return $result [0];
    }
}
?>
