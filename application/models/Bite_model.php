<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bite_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * biteUnbite()
     * This method manage bite or unbite a performer if bite then insert new to to table
     * if unbite then delete correponds row from table
     *
     * @param $idUser current login user's id
     * @param $idFollowing is performer id which will be bite or unbite
     *       
     * @return boolean if user bites performer then return true otherwise return false
     */
    function biteUnbite($idUser, $idFollowing, $biteUnbite)
    {
        if ($biteUnbite == "Bite") {
            $data ["select"] = [
                "relationshipId"
            ];
            $data ["where"] = [
                "followerId" => $idUser,
                "followingId" => $idFollowing
            ];
            $data ["table"] = 'favorites';
            $res = $this->selectRecords($data);
            // if count get 0 means user going to follow a performer
            if (count($res) < 1) {
                
                unset($data);
                $data ["insert"] ["followerId"] = $idUser;
                $data ["insert"] ["followingId"] = $idFollowing;
                $data ["table"] = 'favorites';
                $this->insertRecord($data);
                
                $this->load->model($this->myvalues->profileDetails ["model"], "profile_model");
                
                $res = $this->profile_model->getUserInformation($idUser);
                $account_type = $res->account_type;
               
                
                $resultFollowing = $this->profile_model->getUserInformation($idFollowing);
                $usrGroupFollowing = $resultFollowing->idGrp;
                
                // for fetch the plan of user data
                $this->load->model($this->myvalues->subscriptionDetails ['model'], 'subscription_model');
                $resultFollowingUserPlanID = $this->subscription_model->getCurrentPlan($idFollowing, $usrGroupFollowing);
                
                $subject = "App Notification: You Have Been Bit!";
                // check the plan if not beginner then 
                // set the msg for upgrade the plan to see who have bitten the user
                if (($usrGroupFollowing == VENUE_GROUP && $resultFollowingUserPlanID == 4) || ($usrGroupFollowing == PERFOMER_GROUP && $resultFollowingUserPlanID == 1)) {                
                    $msg = $this->lang->line('msg_bite_plan_upgrade');         
                }else{
                    $chrName = $res->chrName;
                    $msg = $chrName . " has bitten you!";                    
                }
               
                /* Saves message in databse using common function */
                $this->main_model->saveMessage($idFollowing, $this->pUserId, $subject, $msg);
            }
            
            return [
                "status" => true,
                "message" => "User bitten!"
            ];
        }
        else {
            // if count get greater than 0 it means user going to unbite a performer
            
            unset($data);
            $data ["where"] = [
                "followerId" => $idUser,
                "followingId" => $idFollowing
            ];
            $data ["table"] = 'favorites';
            $this->deleteRecords($data);
            // echo $this->db->last_query();die;
            return [
                "status" => true,
                "message" => "User unbitten!"
            ];
        }
    }

    /**
     * getFollowed()
     * This method get followed user of current login user
     *
     * @param integer $idUser is current login user id
     * @return Object of all followed user list
     */
    function getFollowed($idUser)
    {
        $data ["select"] = [
            "*"
        ];
        $data ["where"] = [
            "followerId" => $idUser
        ];
        $data ["table"] = TABLE_FAVORITES;
        $res = $this->selectRecords($data);
        
        return $res;
    }

    /**
     *
     * This method contain user information from user,members and style table
     *
     * @param unknown $userId
     */
    public function getBittenUser($userId)
    {
        $data ["select"] = [
            'u.user_id',
            'u.chrName',
            'mem.mem_address',
            'mem.mem_city',
            'mem.mem_state',
            'mem.stateAbbrev',
            'u.url',
            'u.image'
        ];
        $data ["table"] = TABLE_USER . " as u";
        $data ["join"] = [
            TABLE_MEMBERS . " as mem" => [
                "mem.mem_id = u.mem_id",
                "INNER"
            ],
            
            TABLE_FAVORITES . " as fav" => [
                "fav.followingId = u.user_id",
                "INNER"
            ]
        ];
        
        $data ["where"] = [
            
            "fav.followerId" => $userId
        ];
        $res = $this->selectFromJoin($data);
        return $res;
    }

    /**
     * Used for getting the list of user who have bitten the venue / performer user
     * 
     * @param int $userId, id of login user (venue / performer)
     * 
     * @return ArrayObject $res, that conatain the following user's id 
     */
//     public function getBittenMe($userId)
//     {
//         $data ["select"] = [
//             'u.user_id',
//             'u.chrName',
//             'mem.mem_address',
//             'mem.mem_city',
//             'mem.stateAbbrev',
//             'u.url',
//             'u.image',
//             'u.idGrp',
//             'fav.followerId',
//             'user_pln.is_active'
//         ];
//         $data ["table"] = TABLE_USER . " as u";
//         $data ["join"] = [
//             TABLE_MEMBERS . " as mem" => [
//                 "mem.mem_id = u.mem_id",
//                 "INNER"
//             ],
            
//             TABLE_FAVORITES . " as fav" => [
//                 "fav.followerId = u.user_id",
//                 "INNER"
//             ],
            
//             TABLE_USER_PLAN. " as user_pln" => [
//                 "user_pln.user_id = fav.followingId",
//                 "INNER"
//             ]  
//         ];
        
//         $data ["where"] = [
//             "fav.followingId" => $userId,
//             "is_active" => '1'
//         ];
//         $res = $this->selectFromJoin($data); 
//         echo $this->db->last_query();
//         return $res;
//     }

    
    public function getBittenMe($userId)
    {
        $data ["select"] = [
            'u.user_id',
            'u.chrName',
            'mem.mem_address',
            'mem.mem_city',
            'mem.mem_state',
            'mem.stateAbbrev',
            'u.url',
            'u.image',
            'u.idGrp',
            'fav.followerId'
        ];
        $data ["table"] = TABLE_USER . " as u";
        $data ["join"] = [
            TABLE_MEMBERS . " as mem" => [
                "mem.mem_id = u.mem_id",
                "INNER"
            ],
            
            TABLE_FAVORITES . " as fav" => [
                "fav.followerId = u.user_id",
                "INNER"
            ]
            
        ];
        
        $data ["where"] = [
            "fav.followingId" => $userId,
        ];
        $res = $this->selectFromJoin($data);
        return $res;
    }
}

?>
