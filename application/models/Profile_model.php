<?php

class Profile_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * getUserInformation()
     * This method contain user information from user,members and style table
     *
     * @param unknown $userId
     */
    public function getUserInformation($userId)
    {
        $data ["select"] = [
            'u.user_id',
            'u.image',
            'u.user_name',
            'u.idGrp',
            'u.chrName',
            'u.descr',
            'mem.mem_address',
            'u.contact_person',
            'mem.mem_phone',
            'mem.mem_city',
            'mem.mem_country',
            'mem.mem_state',
            'mem.mem_phone_visibility',
            'u.rate',
            'u.rating',
            'u.url',
            'u.stripe_account_id',
            'u.stripe_customer_id',
            'u.mem_id',
            'u.image_url',
            'u.account_type',
            "u.sendNotificationsEmail",
            "u.allowInbox",
            "u.allowTextNotifications",            
            "u.stripe_status",
            "u.fcm_android",
            "u.fcm_iphone"
        ];
        $data ["table"] = TABLE_USER . " as u";
        $data ["join"] = [
            TABLE_MEMBERS . " as mem" => [
                "mem.mem_id = u.mem_id",
                "LEFT"
            ]
        ];
        
        $data ["where"] = [
            
            "u.user_id" => $userId
        ];
        $res = $this->selectFromJoin($data);
        
        return ! empty($res) ? $res [0] : NULL;
    }

    public function getUserIdByUrl($url)
    {
        $data ["select"] = [
            "user_id",
            "idGrp"
        ];
        $data ["where"] = [
            "url" => $url
        ];
        $data ["table"] = TABLE_USER;
        $res = $this->selectRecords($data);
        return $res;
    }

    /**
     * getUserStyle()
     * This method return user styles of current logged in user's
     *
     * @param integer $userId is user_id
     * @return return style object
     */
    function getUserStyle($userId)
    {
        unset($data);
        $data ['select'] = [
            "idStyle"
        ];
        $data ["table"] = TABLE_USER_STYLE;
        $data ["where"] = [
            "idUser" => $userId
        ];
        $styleId = $this->selectRecords($data);
        
        foreach ($styleId as $id) {
            
            $idStyle [] = $id->idStyle;
        }
        
        return ! empty($idStyle) ? $idStyle : [];
    }

    /**
     * getUserStyle()
     * This method return user styles of current logged in user's
     *
     * @param integer $userId is user_id
     * @return return style object
     */
    function getUserStyleForOverview($userId)
    {
        $idStyle = $this->getUserStyle($userId);
        $styleName = [];
        if (count($idStyle) > 0) {
            $data ['select'] = [
                "idStyle",
                "nameStyle"
            ];
            $data ["table"] = TABLE_STYLE;
            $data ["where_in"] = [
                "idStyle" => $idStyle
            ];
            $styleName = $this->selectRecords($data);
        }
        return $styleName;
    }

    /**
     * getAllStyles()
     * This method get all styles from style table
     *
     * @return Array return all style in array
     *        
     */
    function getAllStyles($idGrp = false)
    {
        $data ['select'] = [
            "idStyle",
            "nameStyle"
        ];
        $data ["where"] = [
            "idGrp" => $idGrp
        ];
        $data ["table"] = TABLE_STYLE;
        return $this->selectRecords($data);
    }

    /**
     * update_profile()
     * This method update current login user profile
     *
     * @param int $iduser is current logged in userId
     * @param int $idmem current login users member table id
     * @param Array $this->input->post() contain all input fild
     *       
     * @return number return number as per condition
     *        
     */
    function updateProfile($iduser, $idmem)
    {
        $name = $this->utility->encodeText($this->input->post('txtName'));
        $contact_person = $this->utility->encodeText($this->input->post('txtContact_person'));
        $style = $this->utility->encodeText($this->input->post('cmbStyle'));
        $phone = $this->utility->encodeText($this->input->post('txtPhone'));
        $rdaPhonvisiblity = $this->pidGroup == 3 ? $this->utility->encodeText($this->input->post('rdaPhonvisiblity')) : ($this->pidGroup == 2 ? 'Public' : 'Private');
        $country = $this->utility->encodeText($this->input->post('cmbCountry'));
        $state = $this->utility->encodeText($this->input->post('cmbState'));
        $city = $this->utility->encodeText($this->input->post('cmbCity'));
        $address = $this->utility->encodeText($this->input->post('txtAddress', true));
//         $url = $this->utility->encodeText($this->input->post('txtUrl'));
        $descr = $this->utility->encodeText($this->input->post('txtDescr'));
        
        if (isset($contact_person)) {
            $data ["update"] ["contact_person"] = $contact_person;
        }
        
        $data ["where"] = [
            "user_id" => $iduser
        ];
        $data ["update"] ["descr"] = $descr;
        $data ["update"] ["chrName"] = $name;
//         $data ["update"] ["url"] = $url;
        $data ["table"] = 'user';
        $this->updateRecords($data);
        
        unset($data);
        $data ["where"] = [
            "mem_id" => $idmem
        ];
        $data ["update"] ["mem_phone"] = $phone;
        $data ["update"] ["mem_city"] = $city;
        $data ["update"] ["mem_address"] = $address;
        $data ["update"] ["mem_state"] = $state;
        $data ["update"] ["mem_country"] = $country;
        $data ["update"] ["mem_phone_visibility"] = $rdaPhonvisiblity;
        
        $data ["table"] = TABLE_MEMBERS;
        $this->updateRecords($data);
        unset($data);
        $data ["where"] = [
            "idUser" => $iduser
        ];
        $data ["table"] = TABLE_USER_STYLE;
        $this->deleteRecords($data);
        
        foreach ($style as $s) {
            
            $data ["insert"] ["idStyle"] = $s;
            $data ["insert"] ["idUser"] = $iduser;
            $data ["table"] = TABLE_USER_STYLE;
            $this->insertRecord($data);
        }
        
        $userData = $this->session->userdata('UserData');
        if (isset($contact_person)) {
            $userData->contact_person = $contact_person;
        }
        $userData->chrName = $name;
        $this->session->set_userdata('UserData', $userData);
        $this->session->set_userdata("iscompleted", 1);
    }

    /**
     * checkPassword()
     * This method for check passowrd is exist or not
     *
     * @param string $email first parameter
     *       
     * @return bool
     */
    function checkPassword()
    {
        $data ["where"] = [
            "user_passowrd" => md5($this->utility->encodeText($this->input->post('txtOld_password')))
        ];
        $data ["table"] = TABLE_USER;
        
        $isDuplicate = $this->isDuplicate($data);
        // return is email duplicate or not either 0 or 1
        
        return $isDuplicate;
    }

    function change_image($image, $idUser)
    {
        $url = EXTERNAL_PATH . "images/profiles/" . $image;
        
        $data ["where"] = [
            "user_id" => $idUser
        ];
        $data ["update"] ["image"] = $image;
        $data ["update"] ["image_url"] = $url;
        $data ["update"] ["user_id"] = $idUser;
        $data ["table"] = TABLE_USER;
        $this->updateRecords($data);
    }

    /**
     * update_settings()
     * This method setting up user profile
     *
     * @param integer $idUser is current logged in userid
     * @return boolean
     */
    function updateSettings($idUser)
    {
        $notificationEmail = $this->input->post('rdaNotificationEmail');
        $allowInbox = $this->input->post('rdaAllowInbox');
        $allowTextNotifications = $this->input->post('rdaAllowTextNotifications');
        
        // value must be less than 1
        if (($notificationEmail > 1) || ($allowInbox > 1) || ($allowTextNotifications > 1)) {
            
            return false;
        }
        
        $data ["where"] = [
            "user_id" => $idUser
        ];
        $data ["update"] ["sendNotificationsEmail"] = $notificationEmail;
        $data ["update"] ["allowInbox"] = $allowInbox;
        $data ["update"] ["allowTextNotifications"] = $allowTextNotifications;
        $data ["table"] = TABLE_USER;
        $this->updateRecords($data);
        
        return true;
    }

    /**
     * Used for check if profile url is already exist for other user or not
     *
     * @param string $profileUrl , profile url
     * @param int $currentUserId, current user id
     *       
     * @return boolean
     */
    public function checkDuplicateProfileUrl($profileUrl, $currentUserId)
    {
        $data ["where"] = [
            "url" => $profileUrl,
            "user_id !=" => $currentUserId
        ];
        $data ["table"] = TABLE_USER;
        $isDuplicate = $this->isDuplicate($data);
        return $isDuplicate;
    }

    /**
     * Used for delete the user's account
     *
     * @param int $userId , id of user
     * @param int $groupId, id of user group
     */
    public function deleteAccount($userId, $groupId)
    {
        if ($groupId == PERFOMER_GROUP || $groupId == VENUE_GROUP || $groupId == FAN_GROUP) {
            
            // delete the entry from messages table
            $data ["where"] = [
                "msgTo" => $userId
            ];
            $data ["table"] = TABLE_MESSAGES;
            $this->deleteRecords($data);
            
            $data ["where"] = [
                "msgFrom" => $userId
            ];
            $this->deleteRecords($data);
            
            // delete the entry from payment table
            $data ["where"] = [
                "idUser" => $userId
            ];
            $data ["table"] = TABLE_PAYMENT;
            $this->deleteRecords($data);
            
            // delete the entry from user style
            unset($data);
            $data ["where"] = [
                "idUser" => $userId
            ];
            $data ["table"] = TABLE_USER_STYLE;
            $this->deleteRecords($data);
            
            // delete the entry from members table
            unset($data);
            $data ["select"] = [
                "mem_id"
            ];
            $data ["where"] = [
                "user_id" => $userId
            ];
            $data ["table"] = TABLE_USER;
            $resultMember = $this->selectRecords($data);
            
            $meberIdArray = array();
            
            foreach ($resultMember as $member) {
                array_push($meberIdArray, $member->mem_id);
            }
            
            unset($data);
            $data ["where_in"] = [
                "mem_id" => $meberIdArray
            ];
            $data ["table"] = TABLE_MEMBERS;
            $this->deleteRecords($data);
            
            // delete the entry from user table
            $data ["where"] = [
                "user_id" => $userId
            ];
            $data ["table"] = TABLE_USER;
            $this->deleteRecords($data);
            
            // delete the bite/unbite
            unset($data);
            $data ["where"] = [
                "followerId" => $userId
            ];
            $data ["table"] = TABLE_FAVORITES;
            $this->deleteRecords($data);
        }
        
        if ($groupId == PERFOMER_GROUP) {
            
            // delete the entry from register table
            unset($data);
            $data ["where"] = [
                "idPerformer" => $userId
            ];
            $data ["table"] = TABLE_REGISTER;
            $this->deleteRecords($data);
            
            // delete the entry from trading table
            $data ["where"] = [
                "requestBy" => $userId
            ];
            $data ["table"] = TABLE_TRADING;
            $this->deleteRecords($data);
            
            $data ["where"] = [
                "requestTo" => $userId
            ];
            $this->deleteRecords($data);
            
            // update the time table entry
            unset($data);
            $data ["update"] ["idPerformer"] = 0;
            $data ["where"] = [
                "idPerformer" => $userId
            ];
            $data ["table"] = TABLE_TIMETABLE;
            $this->updateRecords($data);
        }
        elseif ($groupId == VENUE_GROUP) {
            
            // get event id related to venue userid
            unset($data);
            $data ["select"] = [
                "idEvent"
            ];
            $data ["where"] = [
                "idVenue" => $userId
            ];
            $data ["table"] = TABLE_EVENT;
            $resultEventId = $this->selectRecords($data);
            
            $eventIdArray = array();
            $eventEncodedIdArray = array();
            
            if (! empty($resultEventId)) {
                
                foreach ($resultEventId as $eventId) {
                    array_push($eventIdArray, $eventId->idEvent);
                    array_push($eventEncodedIdArray, $this->utility->encode($eventId->idEvent));
                }
                
                // code for delete the event & event schedule
                $this->load->model($this->myvalues->eventDetails ["model"], "event_model");
                $this->event_model->deleteEvent($eventEncodedIdArray);
                
                // delete the entry from trading table related to event id
                unset($data);
                $data ["where_in"] = [
                    "idEvent" => $eventIdArray
                ];
                $data ["table"] = TABLE_TRADING;
                $this->deleteRecords($data);
                
                // delete the entry from register table related to event id
                $data ["table"] = TABLE_REGISTER;
                $this->deleteRecords($data);
                
                // delete the entry from timetable table related to event id
                $data ["table"] = TABLE_TIMETABLE;
                $this->deleteRecords($data);
            }
            
            // delete the entry from event table related to venue user id
            unset($data);
            $data ["where"] = [
                "idVenue" => $userId
            ];
            $data ["table"] = TABLE_EVENT;
            $this->deleteRecords($data);
        }
        
        $this->db->query("DELETE FROM ci_sessions_new where data like '%user_id\";s:". strlen($userId) .":\"$userId\";%'");
    }
  
    
}

?>
