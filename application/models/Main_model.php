<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * getAllCountries()
     * This method returns all countries name from countries table
     *
     * @return Object of countries result set
     */
    function getAllCountries()
    {
        $data ['select'] = [
            "countries_id",
            "countries_iso_code_2",
            "countries_name"
        ];
        $data ["table"] = TABLE_COUNTRIES;
        return $this->selectRecords($data);
    }

    /**
     * getState()
     * This method return stats according to country request
     *
     * @param unknown $country
     * @return HTML to controller
     */
    function getState($country)
    {
        $data ["select"] = [
            "state_abbrev",
            "state_name"
        ];
        $data ["where"] = [
            "countries_id" => $country
        ];
        $data ["table"] = TABLE_STATES;
        return $this->selectRecords($data);
    }

    /**
     * getCityByState()
     * This method get all cities as per according states
     *
     * @param unknown $country
     * @param unknown $state
     *
     * @return return all city array to controller
     */
    function getCityByState($country, $state)
    {
        $data ["select"] = [
            "city_name_org"
        ];
        $data ["where"] = [
            "country_id" => $country,
            "region" => $state
        ];
        $data ["table"] = TABLE_LOCALDUNIA_TBL_CITY;
        return $this->selectRecords($data);
    }

    /* Validates Incoming webservices Request Wheather They are Passed all required Parameters or Not */
    public function validateRequest($required, $provided)
    {
        foreach ($required as $k => $v) {
            if (! array_key_exists($v, $provided))
                return $this->lang->line("error_specify_all_params");
            if ($v == "type") {
                if (! in_array($provided [$v], [
                    "Android",
                    "Java",
                    "Desktop"
                ]))
                    return $this->lang->line("error_invalid_platform");
            }
        }
        return true;
    }

    public function generateToken($userId, $data)
    {
        $string = implode("||", $data);
        $string = "{{" . $string . "}}";
        $string = substr($userId, 0, 10) . $string;
        $string = $string . substr($userId, - 10, 10);
        return md5(sha1($string));
    }

    /**
     * Used for check the record not deleted, return false if deleted else true
     *
     * @param int $id, id of record
     * @param string $tableName, name of table
     *       
     * @return boolean
     */
    public function checkRecordDeleted($id, $tableName)
    {
        $data ["select"] = [
            'e_status'
        ];
        
        $data ["where"] = [
            "i_id = " => $id
        ];
        
        $data ["table"] = $tableName;
        
        $result = $this->selectRecords($data);
        
        if ($result [0]->e_status == "Deleted") {
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * This function is used to add notification entry in noticication_new table
     *
     * @param array $toUserIds - array of all notified users
     * @param string $message - message
     * @param string $redirectURL - URL of link, will be called when user click on That notification message,
     * @return boolean
     */
    public function addNotification($toUserIds, $message, $redirectURL = "")
    {
        foreach ($toUserIds as $k => $v) {
            $data ["insert"] = [
                "notifyTo" => $v,
                "notifyMessage" => $message,
                "redirectURL" => $redirectURL,
                "readStatus" => 0,
                "dateAdded" => $this->datetime,
                "dateUpdated" => "0000-00-00 00:00:00"
            ];
            $data ["table"] = TABLE_MESSAGES_NEW;
            $this->insertRecord($data);
            unset($data);
            return true;
        }
    }

    /**
     * This function is one for 2 functionalities.
     * 1. Returns array of notifications for same user and mark same as read notification
     * 2. Returns count of nunread notifications
     *
     * @param int $userId - ID of Target User
     * @param boolean $onlyCounts - true for want only count
     * @return boolean / int based on your requirement.
     */
    public function getNotifications($userId, $onlyCounts = false)
    {
        $data ["select"] = [
            "notifyMessage",
            "redirctURL"
        ];
        $data ["table"] = TABLE_NOTIFICATIONS_NEW;
        $records = $this->selectRecords($data);
        unset($data);
        
        if ($onlyCounts) {
            $data ["update"] = [
                "readStatus" => 1,
                "dateUpdated" => $this->datetime
            ];
            $data ["where"] = [
                "notifyTo" => $userId,
                "readStatus" => 0
            ];
            $data ["table"] = TABLE_NOTIFICATIONS_NEW;
            $this->updateRecords($data);
            return true;
        }
        else {
            return count($records);
        }
    }

    /**
     * selectStyleMatchingWithAllUser()
     * This method get style from style table which is matching with current user its for venue,performer
     *
     * @param int $idUser is current login userid
     * @param int $userGroupType is idGrp of user which compare with other user's style
     * @param string ,$style is column name for fetch the result
     *       
     * @return $res Object with matching style
     */
    function selectStyleMatchingWithAllUser($idUser, $userGroupType, $style)
    {
        $data ["select"] = [
            "idStyle"
        ];
        $data ["where"] = [
            "idUser" => $idUser
        ];
        $data ["table"] = TABLE_USER_STYLE;
        $res = $this->selectRecords($data);
        
        foreach ($res as $value) {
            $getIdStyle [] = $value->idStyle;
        }
        unset($data, $res);
        
        $data ["select"] = [
            "idStyle",
            "nameStyle"
        ];
        if ($userGroupType == PERFOMER_GROUP && $style == 'performer_style') {
            $data ["where_in"] = [
                'idStyle' => $getIdStyle
            ];
        }
        else {
            $data ["where_in"] = [
                $style => $getIdStyle
            ];
        }
        $data ["where"] = [
            "idGrp" => $userGroupType
        ];
        
        $data ["table"] = TABLE_STYLE;
        $res = $this->selectRecords($data);
        
        return $res;
    }

    /**
     * This is common method for saving Messages to database
     *
     * @param int $toUser - Id of Recevier User
     * @param int $fromUser - Id of sender User
     * @param string $subject - Subject for message
     * @param string $message - message to be sent
     * @return boolean
     */
    public function saveMessage($toUser, $fromUser, $subject, $message)
    {
        $data ["insert"] ["msgTo"] = $toUser;
        $data ["insert"] ["msgFrom"] = $fromUser;
        $data ["insert"] ["msgSubject"] = urlencode($subject);
        $data ["insert"] ["msgBody"] = urlencode($message);
        $data ["insert"] ["readStatus"] = 0;
        $data ["insert"] ["dtAdded"] = time();
        $data ["insert"] ["appMsgSubject"] = $this->utility->encodeText($subject);
        $data ["insert"] ["appMsgBody"] = $this->utility->encodeText($message);
        $data ["table"] = TABLE_MESSAGES;
        $lastInsetId = $this->insertRecord($data);
        if ($lastInsetId > 0) {
            $this->sendNotificationToDevice($toUser, $message);
        }
        return $lastInsetId;
    }

    /**
     * sendNotificationToDevice
     * this method send notification on device to user
     *
     * @param integer $toUser is user id whom we need to send
     * @param integer $message is correspond message
     */
    public function sendNotificationToDevice($toUser, $message)
    {
        $data ['select'] = [
            "fcm_android",
            "fcm_iphone",
            "allowTextNotifications"
        ];
        $data ["table"] = TABLE_USER;
        $data ["where"] = [
            "user_id" => $toUser
        ];
        $res = $this->selectRecords($data);
        $res = isset($res [0]) ? $res [0] : [];
        
        if (! empty($res->fcm_android) && ! empty($res->allowTextNotifications) && $res->allowTextNotifications == 1) {
            $this->utility->sendGCM($message, $res->fcm_android, 'android');
        }
        
        if (! empty($res->fcm_iphone) && ! empty($res->allowTextNotifications) && $res->allowTextNotifications == 1) {
            $this->utility->sendGCM($message, $res->fcm_iphone, 'ios');
        }
        
        return true;
    }

    /**
     * Used for get the event schedule id for which the event ranking is panding
     *
     * @param int $idUser, venue user id
     * @param int $eventId, evevnt id, if event id is not empty then this function returns event schedule id
     *       
     * @return ArrayObject $EventScheduleIds, that conatin the event schedules id
     */
    function get_pending_ranking_event($idUser = '', $eventId = '')
    {
        if (empty($eventId)) {
            unset($data);
            $data ['select'] = [
                "*"
            ];
            $data ["where"] = [
                "date <=" => date('Y-m-d'),
                "unranked" => 0,
                "idVenue" => $idUser,
                "status" => 1
            ];
            $data ["table"] = TABLE_EVENT;
            $events = $this->selectRecords($data);
            
            $idEvents = array();
            $idEventSchedule = array();
            $EventScheduleIds = array();
            
            if (count($events) > 0) {
                foreach ($events as $e) {
                    $idEvents [] = $e->idEvent;
                }
                if (count($idEvents) > 0 || true) {
                    // $idEvents = implode(",", $idEvents);
                    
                    // fetch the event scheduleid from event schedule tabel
                    unset($data);
                    $data ["select"] = [
                        'es.table_id',
                        'es.end_date',
                        'ev.endTime'
                    ];
                    $data ["table"] = TABLE_EVENT_SCHEDULES . " as es";
                    $data ["join"] = [
                        TABLE_EVENT . " as ev" => [
                            "es.idEvent = ev.idEvent",
                            "INNER"
                        ]
                    ];
                    $data ["where_in"] = [
                        'ev.idEvent' => $idEvents
                    ];
                    $resultEvtScheduleid = $this->selectFromJoin($data);
                    
                    foreach ($resultEvtScheduleid as $id) {
                        if (event_end_time_crossed($id->end_date, $id->endTime)) {
                            $idEventSchedule [] = $id->table_id;
                        }
                    }
                    
                    $idEventSchedule = implode(",", $idEventSchedule);
                    $EventScheduleIds = array();
                    
                    if (! empty($idEventSchedule)) {
                        $sql = "select DISTINCT(reg.idEvent) from register as reg where (reg.attandance='N/A' or reg.attandance='Present') and reg.rank is NULL and reg.status='Accepted' and reg.idEvent in ($idEventSchedule)";
                        
                        $rtValue = $this->db->query($sql);
                        
                        foreach ($rtValue->result() as $rt) {
                            $EventScheduleIds [] = $rt->idEvent;
                        }
                    }
                }
            }
            
            return $EventScheduleIds;
        }
        else {
            
            $eventArray = array();
            
            unset($data);
            
            $data ["select"] = [
                'table_id'
            ];
            $data ["table"] = TABLE_EVENT_SCHEDULES;
            $data ["where"] = [
                "idEvent" => $eventId
            ];
            $result = $this->selectRecords($data);
            
            foreach ($result as $id) {
                $eventArray [] = $id->table_id;
            }
            
            return $eventArray;
        }
    }

    /**
     * getUnreadMessage()
     * This method get unread message counts of current message
     *
     * @return return count of undread messages
     */
    public function getUnreadMessage()
    {
        $data ["where"] = [
            "msgTo" => $this->pUserId,
            "readStatus" => 0
        ];
        $data ["table"] = TABLE_MESSAGES;
        
        return $this->countRecords($data);
    }

    /**
     * sendNotificationToAcceptedPerformer()
     * This method send notification mail to all performer when event shedule would be delete
     *
     * @param $idEvent is schedual id of event
     * @param string $msg is message to send performer
     * @param string $subject is subject of message
     */
    public function sendNotificationToAcceptedPerformer($idEvent, $msg = "", $subject = "")
    {
        $data ["select"] = [
            "rg.idPerformer",
            "es.start_date",
            "ev.title",
            "ev.startTime",
            "ev.timeZone"
        ];
        $data ["table"] = TABLE_REGISTER . ' rg';
        $data ["join"] = [
            TABLE_EVENT_SCHEDULES . " as es" => [
                "es.table_id = rg.idEvent",
                "INNER"
            ],
            TABLE_EVENT . " as ev" => [
                "ev.idEvent = es.idEvent",
                "INNER"
            ]
        ];
        $data ["where_in"] = [
            "rg.idEvent" => $idEvent
        ];
        $data ["where"] = [
            "rg.status" => "Accepted"
        ];
        $res = $this->selectFromJoin($data);
        if (! empty($res)) {
            $fromUser = $this->pUserId;
            foreach ($res as $sendValue) {
                $hastTag = [
                    "#EVENT_NAME#",
                    "#EVENT_TIME#",
                    "#TIME_ZONE#"
                ];
                $hastTagValue = [
                    $sendValue->title,
                    convertFromGMT($sendValue->start_date . ' ' . $sendValue->startTime, "Y-m-d h:i A", $sendValue->timeZone),
                    $sendValue->timeZone
                ];
                $msg = str_replace($hastTag, $hastTagValue, $msg);
                $this->saveMessage($sendValue->idPerformer, $fromUser, $subject, $msg);
            }
        }
    }

    /**
     * Used for check if event cutoff time passed & also check if event date is passed
     *
     * @param int $eventScheduleId
     * @param boolean $checkCutofftime, if true then check for cutoff time
     * @param boolean $returnVar, if true then return result in true/ false
     *       
     * @return boolean, return true if event time or cutoff time passed else false
     */
    public function CheckEventPassed($eventScheduleId, $checkCutofftime = FALSE, $returnVar = FALSE)
    {
        $returnData = false;
        $this->load->library('user_agent');
        
        $this->load->model($this->myvalues->eventDetails ['model'], 'event_model');
        $resultEventData = $this->event_model->getEventSchedule($eventScheduleId);
        
        // if event time passed then return true
        if (event_end_time_crossed($resultEventData->end_date, $resultEventData->endTime)) {
            
            if ($returnVar === true) {
                return true;
                exit();
            }
            else {
                $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
                if (! empty($this->agent->referrer())) {
                    redirect($this->agent->referrer());
                }
                else {
                    redirect($this->session->userdata('dashboardURL'));
                }
            }
        }
        else {
            // check for cutofftime
            if ($checkCutofftime) {
                
                if (event_cut_off_time_passed($resultEventData->start_date, $resultEventData->startTime, $resultEventData->hoursLockBefore, $resultEventData->allowWalkInReal)) {
                    if ($returnVar === true) {
                        return true;
                        exit();
                    }
                    else {
                        $this->utility->setFlashMessage('danger', $this->lang->line('error_default_error'));
                        if (! empty($this->agent->referrer())) {
                            redirect($this->agent->referrer());
                        }
                        else {
                            redirect($this->session->userdata('dashboardURL'));
                        }
                    }
                }
            }
        }
        
        // event date & cutoff time not passed,
        if ($returnVar === true) {
            return false;
            exit();
        }
    }

    /**
     * Used for display the tip balance of performer
     *
     * @param int $userId, user id of performer
     *       
     * @return int $result, that contain the tip stripe balance of performer
     */
    public function getTipBalancePerformer($userId)
    {
        $data ['select'] = [
            'd_stripe_balance',
            'stripe_account_id',
            'stripe_status'
        ];
        $data ['where'] = [
            'user_id' => $userId
        ];
        $data ['table'] = TABLE_USER;
        $result = $this->selectRecords($data);
        return $result [0];
    }

    /**
     * Used for get all information of particular user,
     * this called from the apple_payment controller
     * 
     * @param int $userId, userid of performer
     *  
     * @return int $result, that contain all information of user     
     */
    public function getUserInfo($userId)
    {
        $data ['select'] = [
           '*'
        ];
        $data ['where'] = [
            'user_id' => $userId
        ];
        $data ['table'] = TABLE_USER;
        $result = $this->selectRecords($data);
        return $result [0];
    }
}
