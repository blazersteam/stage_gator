<?php

class Rank_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * This function returns array of users that are accpeted invitation for this event schedule.
     *
     * @param int $scheduleEventId - schdeul event id
     *       
     * @return array of users
     */
    public function getAcceptedUsers($scheduleEventId)
    {
        $data ["select"] = [
            "u.chrName",
            "u.user_id",
            "rg.attandance",
            "rg.rank",
            "rg.idRegister",
            "rg.idPerformer",
            "rg.idEvent",
            "rg.isHost",
            "u.rate",
            "rt.rating"
        ];
        $data ["table"] = TABLE_REGISTER . " rg";
        $data ["where"] = [
            "rg.idEvent" => $scheduleEventId,
            "rg.status" => "Accepted"
        ];
        $data ["join"] = [
            TABLE_USER . " u" => [
                "u.user_id = rg.idPerformer",
                "INNER"
            ],
            /* join rating table to show rate only what user rank to performer individualy */
            TABLE_RATING . " rt" => [
                "rt.user_id = rg.idPerformer AND rt.event_id = rg.idEvent",
                "LEFT"
            ],
          
        ];
        $result = $this->selectFromJoin($data);
        
        return $result;
    }

    /**
     * rank_performer()
     * this method ranked a performer
     *
     * @param intrger $idUser is current user id
     * @return interiger $new_rank is rakned value
     */
    function rank_performer($idUser)
    {         
        $rating = $this->utility->encodeText($this->input->post('rating'));
        /* if (empty($rating)) {
            return false;
        } */
        $userId = $this->utility->encodeText($this->input->post('performer'));
        $eventId = $this->utility->encodeText($this->input->post('idEvent'));
        $attandance = $this->utility->encodeText($this->input->post('attandance'));
        
        $data['select'] = ['id'];
        $data['where'] = ['event_id' => $eventId,'user_id' => $userId];
        $data['table'] = TABLE_RATING;
        $res = $this->selectRecords($data);
        
        /* this condition will check if this performer is already ranked from other browser if yes then return from here */
        if(count($res) > 0){
            return 'exist';
        }
        unset($data);
        $floor = floor($rating);
        $fraction = $rating - $floor;
        
        if ($fraction > 0 && $fraction <= .5) {
            $fraction = .5;
        }
        else if ($fraction > .5 && $fraction <= 1) {
            $fraction = 1;
        }
        
        $finalRate = $floor + $fraction;
        
        
        // $sql = "insert into rating set event_id = $eventId, user_id = $userId, rating = $finalRate";
        $data ["insert"] ["event_id"] = $eventId;
        $data ["insert"] ["user_id"] = $userId;
        $data ["insert"] ["rating"] = $finalRate;
        $data ["table"] = 'rating';
        
        $RatingId = $this->insertRecord($data);
        unset($data);
        // $this->db->query ( $sql );
        // $RatingId = $this->db->insert_id ();
        
        // $sql = "SELECT count( * ) AS total_time, SUM( rating ) AS avg FROM rating WHERE user_id ='$userId'";
        // $totalEvent = $this->db->query ( $sql )->row ()->total_time;
        // $totalRating = $this->db->query ( $sql )->row ()->avg;
        $data ["select"] = [
            "count(0) as total_time",
            "SUM(rating) as avg"
        ];
        $data ["where"] = [
            "user_id" => $userId
        ];
        $data ["table"] = 'rating';
        $res = $this->selectRecords($data);
        $totalEvent = $res [0]->total_time;
        $totalRating = $res [0]->avg;
        $totelRate = 0.0;
        $suffix = 0;
        $v = floor($totalEvent / 7);
        
        $suffix = $v * 5;
        $totelRate = ($totalRating + $suffix) / $totalEvent;
        
        // $sql = "update user set rate = '$totelRate' where user_id='$userId' ";
        // $this->db->query ( $sql );
        
        // $ret = $this->db->affected_rows ();
        unset($data);
        $data ["where"] = [
            "user_id" => $userId
        ];
        $data ["update"] ["rate"] = $totelRate;
        $data ["table"] = TABLE_USER;
        $this->updateRecords($data);
        
        /*
         * $sql = "update register set attandance = '$attandance' $RATING
         * where idPerformer='$performer' and idEvent=$idEvent and status='Accepted' and
         * idEvent in (select idEvent from event where idVenue=$idUser)";
         * $this->db->query ( $sql );
         */
        
        unset($data);
        $data ["where"] = [
            "idPerformer" => $userId,
            "idEvent" => $eventId,
            "status" => "Accepted"
        ];
        $data ["update"] ["rank"] = $rating;
        $data ["update"] ["attandance"] = $attandance;
        $data ["table"] = TABLE_REGISTER;
        $resultUpdate = $this->updateRecords($data);
        
        //return $rating;
        
        return $resultUpdate;
        
        // $ret = $this->db->affected_rows ();
        // $sql = "select count(*) as total_accepted from register where idPerformer='$performer' and
        /*
         * status='Accepted'";
         * //$total_accpeted = $this->db->query ( $sql )->row ()->total_accepted;
         * unset($data);
         * $data["select"] = ["count(0) as total_accepted"];
         * $data["where"] = ["idPerformer" => $userId,"status" => "Accepted"];
         * $data["table"] = TABLE_REGISTER;
         * $res = $this->selectRecords($data);
         * $total_accpeted = $res[0]->total_accepted;
         *
         * //$sql = "select count(*) as total_present from register where idPerformer='$performer' and
         * status='Accepted' and attandance='Present'";
         * //$total_present = $this->db->query ( $sql )->row ()->total_present;
         * unset($data);
         * $data["select"] = ["count(0) as total_present","avg(rank) as avg_rating"];
         * $data["where"] = ["idPerformer" => $userId,"status" => "Accepted","attandance" => "Present"];
         * $data["table"] = TABLE_REGISTER;
         * $res = $this->selectRecords($data);
         * $total_present = $res[0]->total_present;
         *
         * $attandance_percent = number_format ( $total_present / $total_accpeted, 2 );
         * $attandance_weight = number_format ( $attandance_percent * 20, 2 );
         *
         * // $sql = "select avg(rank) as avg_rating from register where idPerformer='$performer' and
         * status='Accepted' and attandance='Present'";
         *
         *
         * $avg_rating = number_format ( $res[0]->avg_rating, 2 );
         * $rating_percent = number_format ( $avg_rating / 5, 2 );
         * $rating_weight = number_format ( $rating_percent * 50, 2 );
         *
         * //$sql = "select count(*) as total_events from event where idEvent<=$idEvent";
         * unset($data);
         * $data["select"] = ["count(0) as total_events"];
         * $data["where"] = ["idEvent <=" => $eventId];
         * $data["table"] = TABLE_EVENT;
         * $res = $this->selectRecords($data);
         * $total_events = $res[0]->total_events;
         * $open_mic_percent = number_format ( $total_present / $total_events, 2 );
         * $open_mic_weight = number_format ( $open_mic_percent * 30, 2 );
         *
         * $total_weight = $attandance_weight + $rating_weight + $open_mic_weight;
         * $new_rank = round ( $total_weight * 5 / 100, 1, 1 );
         *
         * // $sql = "update user set rating='$new_rank' where user_id=$performer";
         * // $this->db->query ( $sql );
         *
         * echo $new_rank;
         * die;
         * unset($data);
         * $data ["where"] = [
         * "user_id" => $userId
         * ];
         * $data ["update"] ["rating"] = $new_rank;
         * $data ["table"] = TABLE_USER;
         * $this->updateRecords($data);
         */
    }

    /**
     * Used for fetch the all permission related to host for login user
     *
     * @param int $userId , id of user
     * @param int $idEventScheduleId , id of event schedule
     */
    function checkAllPermissionForLoginUser($userId, $idEventScheduleId)
    {
        // all permission for venue user
        $permission = new stdClass();
        $permission->doAccept = 0;
        $permission->doRank = 0;
        $permission->doList = 0;
        $permission->doAttandance = 0;
        // if perfomer group
        if ($this->pidGroup == PERFOMER_GROUP) {
            
            $data ["select"] = [
                "doAccept",
                "doRank",
                "doList",
                "doAttandance"
            ];
            $data ["table"] = TABLE_REGISTER;
            $data ["where"] = [
                "idEvent" => $idEventScheduleId,
                "status" => "Accepted",
                "isHost" => 1,
                "idPerformer" => $userId
            ];
            
            $result = $this->selectRecords($data);
            // pr($result[0]); exit;
            return !empty($result [0]) ? $result [0] : $permission;
        }
        else if ($this->pidGroup == VENUE_GROUP) {
            
            // all permission for venue user
            $permission = new stdClass();
            $permission->doAccept = 1;
            $permission->doRank = 1;
            $permission->doList = 1;
            $permission->doAttandance = 1;
            
            return $permission;
        }
    }
}

?>
