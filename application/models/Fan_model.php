<?php

class Fan_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * getPerformerStream()
     * This method get registered event which is accepted by venue
     *
     * @param unknown $idUser is current login user id
     * @return $res2 object of register table data
     */
    function getPerformerStream($idUser)
    {
        $data ["select"] = [
            "rg.idEvent",
            "ev.title",
            "es.table_id",
            "es.start_date",
            "es.end_date",
            "ev.startTime",
            "ev.endTime",
            "ev.descr",
            "u.chrName"
        ];
        $data ['table'] = TABLE_REGISTER . ' rg';
        $data ["join"] = [
            TABLE_FAVORITES . " fev" => [
                "fev.followingId = rg.idPerformer",
                "INNER"
            ],
            TABLE_USER . " u" => [
                "u.user_id = rg.idPerformer",
                "INNER"
            ],
            TABLE_EVENT_SCHEDULES . " es" => [
                "es.table_id = rg.idEvent",
                "INNER"
            ],
            TABLE_EVENT . " ev" => [
                "ev.idEvent = es.idEvent",
                "INNER"
            ]
        ];
        $data ["where"] = [
            'rg.status' => 'Accepted',
            "fev.followerId" => $idUser,
            "es.start_date >=" => date('Y-m-d')
        ];
        $data ["order"] = 'es.start_date';
        $res = $this->selectFromJoin($data);
        
        return $res;
    }

    /**
     * getVenueStream()
     * This method returns event data object which is venue's event if fan already bite any venue
     *
     * @param $idUser is current user login id
     *       
     * @return object result of event
     */
    function getVenueStream($idUser)
    {
        $data ["select"] = [
            "es.idEvent",
            "ev.title",
            "es.table_id",
            "es.start_date",
            "es.end_date",
            "ev.startTime",
            "ev.endTime",
            "ev.descr",
            "u.chrName"
        ];
        $data ['table'] = TABLE_EVENT . ' ev';
        $data ["join"] = [
            TABLE_FAVORITES . " fev" => [
                "fev.followingId = ev.idVenue",
                "INNER"
            ],
            TABLE_USER . " u" => [
                "u.user_id = ev.idVenue",
                "INNER"
            ],
            TABLE_EVENT_SCHEDULES . " es" => [
                "es.idEvent = ev.idEvent",
                "INNER"
            ]
        ]
        ;
        $data ["where"] = [
            "fev.followerId" => $idUser,
            "es.start_date >=" => date('Y-m-d')
        ];
        $data ["order"] = 'es.start_date';
        $res = $this->selectFromJoin($data);
        
        return $res;
    }


    /**
     * getMyStream()
     * This method returns combinded data object of performer(accepted) and venue(fan bite venue)
     *
     * @param $idUser is current user login id
     *
     * @return object result of event
     */
    function getMyStream($idUser)
    {
        $performerQuery = $this->getPerformerStream($idUser);
        $fanQuery = $this->getVenueStream($idUser);
        $result = array_merge($performerQuery,$fanQuery);
        usort($result, array($this,'date_compare'));
        return $result;
    }
    
    function date_compare($a, $b)
    {
        $t1 = strtotime($a->start_date);
        $t2 = strtotime($b->start_date);
        return $t1 - $t2;
    }
}

?>
