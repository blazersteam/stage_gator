<?php

class Invitation_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function cancelInvite($performerId, $registerId)
    {}

    /**
     * venueInviteEvents()
     * This method get invited performer listing
     *
     * @param unknown $userId
     *
     * @return return performer result as a object
     */
    function venueInviteEvents($userId, $requestByPerformer = 0, $eventAccess = [])
    {
        $data ["select"] = [
            "ev.idEvent",
            "ev.date",
            "u.chrName",
            "u.url",
            "ev.startTime",
            "ev.endTime",
            "ev.timeZone",
            "ev.title",
            "rg.idPerformer",
            "rg.noticeVenue",
            "rg.noticePerformer",
            "rg.idRegister",
            "rg.status",
            "rg.ReminderSentOn",
            "rg.dtAdded as dtApplied",
            "es.start_date",
            "es.table_id",
            "es.end_date"
        ];
        
        $data ['table'] = TABLE_REGISTER . ' rg';
        $data ["join"] = [
            TABLE_EVENT_SCHEDULES . " es" => [
                "rg.idEvent = es.table_id",
                "INNER"
            ],
            TABLE_EVENT . " ev" => [
                "es.idEvent = ev.idEvent",
                "INNER"
            ],
            TABLE_USER . "  u" => [
                "rg.idPerformer = u.user_id",
                "INNER"
            ]
        ];
        
        $data ["where"] = [
            "rg.RequestedByPerformer" => $requestByPerformer,
            "rg.status!=" => "Cancelled"
        ];
        
        if (! empty($eventAccess)) {
            $data ["where_in"] = [
                "es.table_id" => $eventAccess
            ];
            $data ["where"] ["rg.idPerformer !="] = $userId;
        }
        else {
            $data ["where"] ["ev.idVenue"] = $userId;
        }
        
        if ($this->pidGroup == "3") {
            unset($data ["where"] ["ev.idVenue"]);
            if (empty($eventAccess)) {
                $data ["where"] ["rg.idPerformer"] = $userId;
            }
        }
        
        $data ['order'] = "ev.title, rg.noticeVenue desc, date desc, dtApplied desc";
        
        $res = $this->selectFromJoin($data);
        
        // Update the noticeperformer to not display new notification second time at performer side
        // when user want to see the Awaiting Approval From Venue.
        
        if ($this->pidGroup == "3") {
            unset($data);
            
            /* $data ["where"] = [
                "idPerformer" => $userId,
                "RequestedByPerformer" => $requestByPerformer
            ];
            $data ["update"] ["hostNoticePerformer"] = 0;
            $data ["table"] = TABLE_REGISTER;
            $this->updateRecords($data); */
        }
        
        return $res;
    }

    public function getHostSchedules($idHost)
    {
        $data ["select"] = [
            "idEvent"
        ];
        $data ["where"] = [
            "idPerformer" => $idHost,
            "status" => 'Accepted',
            'isHost' => 1,
            'doAccept' => 1
        ];
        $data ["table"] = TABLE_REGISTER;
        
        $result = $this->selectRecords($data);
        
        foreach ($result as $r) {
            $idHostEvents [] = $r->idEvent;
        }
        return ! empty($idHostEvents) ? $idHostEvents : [
            0
        ];
    }

    /**
     * This function returns total accepted + pending Invitation from Venue side so we can compare it with allowed
     * performer count based on its plan
     *
     * @param int $venueId user_id of venue
     *       
     * @return int total reqests
     */
    public function getAcceptPendingCount($venueId, $scheduleId)
    {
        $data ["select"] = [
            "rg.idEvent"
        ];
        $data ["table"] = TABLE_REGISTER . " rg";
        $data ["join"] = [
            TABLE_EVENT_SCHEDULES . " es" => [
                "es.table_id = rg.idEvent",
                "INNER"
            ],
            TABLE_EVENT . " ev" => [
                "es.idEvent = ev.idEvent",
                "INNER"
            ]
        ];
        
        $data ["where"] = [
            "es.table_id" => $scheduleId,
            "ev.idVenue" => $venueId
        ];
        $data ["where_in"] = [
            "rg.status" => [
                "Accepted",
                "Pending"
            ]
        ];
        return count($this->selectFromJoin($data));
    }

    /**
     * This function returns schedule id based on register id
     * 
     * @param int $id registration table id
     */
    public function getScheduleFromRegisterId($id)
    {
        $data ["select"] = [
            "idEvent"
        ];
        
        $data["where"] = ["idRegister" => $id];
        $data["table"] = TABLE_REGISTER;
        return $this->selectRecords($data);
    }
}

?>
