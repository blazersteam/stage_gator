<?php

class Home_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * getEventByLocations()
     * This function search event by locations
     *
     * @param $loc is array of locations
     *       
     * @return $res is event result object
     */
    public function getEventByLocations($location)
    {
        $query = "SELECT `ev`.`title`, `es`.`start_date`, `ev`.`startTime`, `ev`.`endTime`, `loc`.`city` FROM " . TABLE_EVENT . " `ev` INNER JOIN " . TABLE_EVENT_SCHEDULES . " `es` ON `es`.`idEvent` = `ev`.`idEvent` INNER JOIN " . TABLE_LOCATIONS . " `loc` ON `ev`.`location_id` = `loc`.`location_id` WHERE `es`.`start_date` >= '" . date('Y-m-d') . "'";
        $query .= " ORDER BY es.start_date asc, CASE ";
        if (! empty($location ["city"])) {
            $query .= "WHEN loc.city= '" . $location ["city"] . "' THEN 1 ";
        }
        
        if (! empty($location ["state"])) {
            $query .= "WHEN loc.state= '" . $location ["state"] . "' THEN 2 ";
        }
        
        if (! empty($location ["country"])) {
            $query .= "WHEN loc.country= '" . $location ["country"] . "' THEN 3";
        }
        $query .= " END LIMIT 6";
        
        $result = $this->db->query($query)->result();
        return $result;
    }

    /**
     * getPerformers()
     * This method get performers
     */
    public function getPerformers()
    {
        $data ["select"] = [
            "u.chrName",
            "u.image",
            "u.image_url",
            "u.descr",
            "u.rate",
            "mem.mem_city",
            "st.nameStyle"
        ];
        $data ["table"] = TABLE_USER . ' u';
        $data ["join"] = [
            TABLE_MEMBERS . " mem" => [
                "mem.mem_id = u.mem_id",
                "INNER"
            ],
            TABLE_USER_STYLE . " us" => [
                "us.idUser = u.user_id",
                "INNER"
            ],
            TABLE_STYLE . " st" => [
                "st.idStyle = us.idStyle",
                "INNER"
            ]
        ];
        $data ["where"] = [
            "u.image !=" => '',
            "u.descr !=" => ''
        ];
        $data ["order"] = 'u.rate DESC';
        $data ["groupBy"] = 'u.user_id';
        $data ["limit"] = 4;
        $res = $this->selectFromJoin($data);
        return $res;
    }
}

?>
