<?php

class Register_model extends My_model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * createFacebookUser()
     * This method create a user in database it is for login with facebook
     *
     * @param Array $userData is facebook user full profile array
     * @param integer $idGrp is user type fan,performer,venue
     * @return Object return row of current user
     */
    function createFacebookUser($userData, $idGrp)
    {
		 
        $data ["insert"] ["mem_phone"] = '';
        $data ["insert"] ["mem_city"] = '';
        $data ["insert"] ["mem_address"] = '';
        $data ["insert"] ["countries_id"] = '';
        $data ["insert"] ["stateAbbrev"] = '';
        $data ["insert"] ["radius_travel"] = 1;
        $data ["insert"] ["mem_datecreated"] = date("Y-m-d");
        $data ["insert"] ["mem_phone_visibility"] = $idGrp == "2" ? "Public" : "Private";
        $data ["table"] = TABLE_MEMBERS;
        $mem_id = $this->insertRecord($data);
        
        unset($data);
        $dtAdded = time();
        
        if (! empty($userData ['picture'] ['url'])) {
            $imageUrl = $userData ['picture'] ['url'];
            $getFile = file_get_contents($imageUrl);
            $imageName = md5($dtAdded) . '.jpg';
            $status = file_put_contents("./external/images/profiles/" . $imageName, $getFile);
            $imageName = $status > 0 ? $imageName : '';
            $imageUrl = EXTERNAL_PATH . 'images/profiles/' . $imageName;
        }
        else {
            $imageUrl = '';
            $imageName = '';
        }
        
        $data ["insert"] ["user_name"] = ! empty($userData ['email']) ? $userData ['email'] : '';
        $data ["insert"] ["user_password"] = '';
        $data ["insert"] ["chrName"] = $userData ['name'];
        $data ["insert"] ["idGrp"] = $idGrp;
        $data ["insert"] ["url"] = $dtAdded;
        $data ["insert"] ["mem_id"] = $mem_id;
        $data ["insert"] ["access_key_date"] = $dtAdded;
        $data ["insert"] ["status_id"] = 1;
        $data ["insert"] ["image_url"] = $imageUrl;
        $data ["insert"] ["image"] = $imageName;
        $data ["insert"] ["ip"] = $this->input->ip_address();
        $data ["insert"] ["created_via"] = 'PORTAL';
        $data ["insert"] ["access_key"] = md5($dtAdded);
        $data ["insert"] ["fb_token"] = $userData ['id'];
        $data ["table"] = TABLE_USER;
        $user_id = $this->insertRecord($data);
        
        if ($user_id) {
            
            return $userData ['id'];
        }
    }

    /**
     * checkDuplicate()
     * This method for check duplicate email
     *
     * @param string $email first parameter
     *       
     * @return bool
     */
    function checkDuplicate()
    {
        $email = $this->utility->encodeText($this->input->post('username'));
        $data ["where"] = [
            "user_name" => $email
        ];
        $data ["table"] = TABLE_USER;
        
        $isDuplicate = $this->isDuplicate($data);
        // return is email duplicate or not either 0 or 1
        
        return $isDuplicate;
    }

    /**
     * create_user()
     * This method create a user and and store data in following tables: members,user,user_style
     * first check validation and check duplicate email after insert data in tables and send a mail to user
     *
     * @param Array $this->input->post() is get all post parameters
     *       
     * @return integer $user_id return userid to controller
     */
    function create_user()
    {
        
        
        $name = $this->utility->encodeText($this->input->post('txtName'));
        $contactPerson = $this->utility->encodeText($this->input->post('txtContact_person'));
        $type = $this->utility->encodeText($this->input->post('cmbType'));
        $phone = $this->utility->encodeText($this->input->post('txtPhone'));
        $email = $this->utility->encodeText($this->input->post('txtEmail'));
        $password = $this->utility->encodeText($this->input->post('txtPassword'));
       
        unset($data);
        $data ["insert"] ["mem_phone"] = $phone;
        $data ["insert"] ["mem_city"] = '';
        $data ["insert"] ["mem_address"] = '';
        $data ["insert"] ["countries_id"] = '';
        $data ["insert"] ["stateAbbrev"] = '';
        $data ["insert"] ["radius_travel"] = '';
        $data ["insert"] ["mem_datecreated"] = date("Y-m-d");
        $data ["insert"] ["mem_phone_visibility"] = $type == "2" ? "Public" : "Private";
        $data ["table"] = TABLE_MEMBERS;
        $mem_id = $this->insertRecord($data);
        
        $dtAdded = time();
        unset($data);
        $data ["insert"] ["user_name"] = $email;
        $data ["insert"] ["user_password"] = md5($password);
        $data ["insert"] ["chrName"] = $name;
        if (! empty($contactPerson)) {
            $data ["insert"] ["contact_person"] = $contactPerson;
        }
        $data ["insert"] ["idGrp"] = $type;
        $data ["insert"] ["url"] = $dtAdded;
        $data ["insert"] ["mem_id"] = $mem_id;
        $data ["insert"] ["access_key_date"] = $dtAdded;
        $data ["insert"] ["status_id"] = 0;
        $data ["insert"] ["ip"] = $this->ip;
        $data ["insert"] ["created_via"] = 'PORTAL';
        $data ["insert"] ["access_key"] = md5($dtAdded);
        $data ["table"] = TABLE_USER;
        $user_id = $this->insertRecord($data);
        
        unset($data);
        $data ['username'] = $name;
        $data ['user'] = $email;
        $data ['key'] = md5($dtAdded);
        $data ['URL'] = SITEURL . $this->myvalues->loginDetails ['controller'] . '/activate_account/' . md5($dtAdded);
        $data ['act'] = ($type == 2) ? 'Venue' : (($type == 3) ? 'Performer' : 'Fan');
        $this->send_activate_email($data);
        
        if($user_id){
            
            $uname = trim(preg_replace('/[\W]+/', '-', strtolower($name)), '-');
            $url = $this->generateUserUrl($uname, $user_id);
            
            $data ["where"] = [
                "user_id" => $user_id
            ];
            $data ["update"] ["url"] = $url;
            $data ["table"] = TABLE_USER;
            $urlUpdate = $this->updateRecords($data);
        }
        return $user_id;
    }

    /**
     * send_activate_email()
     * This method send an activation email to user wo registerd recently
     *
     * @param Array $user details email and password
     * @param String $pass user password
     * @param String $key activation key
     *       
     * @return Boolean True
     */
    function send_activate_email($data)
    {
        $message = $this->load->view('email_templates/login_activation_template', $data, true);
        $data ["message"] = $message;
        $data ["from_title"] = EMAIL_TITLE;
        $data ["to"] = $data ['user'];
        $data ["subject"] = $this->lang->line('email_subject_stageGator_account_confirmation');
        // send mail using utilitys
        $this->utility->sendMailSMTP($data);
    }
    
    function checkUserUrl($url, $uid){
        
        $data ["where"] = [
            "url" => $url
        ];
        $data ["table"] = TABLE_USER;
        
        $isDuplicate = $this->isDuplicate($data);
        // return is email duplicate or not either 0 or 1
        $result[0] = $isDuplicate;
        $result[1] = $uid;
        return $result;
    }
    
    function generateUserUrl($name, $uid=null,$rnd=null)
    {
        
        $url = $name;
        
        if($rnd){
            $url = $name. '-'.$rnd;
        }
        
        $urlCheck = $this->checkUserUrl($url,$uid);
       
        $nurl = $url;
        if(!empty($urlCheck[0])){
           return $this->generateUserUrl($url, '', $urlCheck[1]);
            
        }
        
        return $nurl;
        
    }

}

?>