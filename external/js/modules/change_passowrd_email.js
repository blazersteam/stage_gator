$('#change_email').click(function() {
	status = $(this).prop('checked');
	if (status == 'true') {
		$("input[type='password'],#btnChangePassword").prop('disabled', true);
		$("#new_email,#mailSubmit").prop('disabled', false);

	} else {
		$("input[type='password'],#btnChangePassword").prop('disabled', false);
		$("#new_email,#mailSubmit").prop('disabled', true);
	}

});

$(document).ready(function() {

	$("input[type = 'password']").keydown(function(e) {
		if (e.keyCode == 32)
			return false;
	});

	$("input[type = 'password']").on('paste', function(e) {
		return false;
	});

	$("#new_email,#mailSubmit").prop('disabled', true);
	$("#form").validate({
		rules : {
			required : {
				required : true,
			// maxlength: 50
			},
			txtNew_email : {
				required : true,
				email : true
			},
			txtOld_password : {
				required : true,
				minlength : 6
			},
			txtPassword : {
				required : true,
				minlength : 6
			},
			txtConfirmPassword : {
				required : true,
				minlength : 6,
				equalTo : "#password"
			},

		},
		messages : {
			txtConfirmPassword : {
				equalTo : 'Password and confirm password does not match.'
			}

		},
		errorClass : "help-inline text-danger",
		errorElement : "span",
		highlight : function(element, errorClass, validClass) {
			$(".ci-validations").css('display', 'none');
			$(element).parents('.form-group').addClass('has-error');
		},
		unhighlight : function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-error');
			$(element).parents('.form-group').addClass('has-success');
		}
	});

});
