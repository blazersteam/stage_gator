$(document).ready(function(){


	
	$(".chosen-select").chosen();
	$(".tooltips").tooltip();


   
     $.validator.setDefaults({ ignore: ":hidden:not(.chosen-select)" })
	// Add US Phone Validation
		jQuery.validator.addMethod('phoneUS', function(phone_number, element) {
		    phone_number = phone_number.replace(/\s+/g, ''); 
		    return this.optional(element) || phone_number.length > 9 &&
		        phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
		}, 'Please enter a valid phone number.');
	$("#form").validate({
			rules:{
				required:{
					required:true,
					//maxlength: 50						
				},
				txtTitle:{
					required:true
					
				},
				txtPhone: {
					required: true,
					phoneUS: true
				},
			
				cmbCountry:{
					required: true	
				},
				cmbState:{
					required: true	
				},
				cmbCity: {
					required: true
					
				},
				txtAddress: {
					required: true
				}
//				txtNumSeats: {
//					required: true
//				}
			},
			errorClass: "help-inline text-danger",
			errorElement: "span",
			highlight:function(element, errorClass, validClass) {
				$(element).parents('.form-group').addClass('has-error');
			},
			errorPlacement: function (error, element) {
	            //check whether chosen plugin is initialized for the element
	            if (element.data().chosen) {
	                element.next().after(error);
	            } else {
	                element.after(error);
	            }
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).parents('.form-group').removeClass('has-error');
				$(element).parents('.form-group').addClass('has-success');
			}
			
		});
	
});
/*function change_country(id){

	url = SITEURL+'resources/getStateList/'+id;
	$.get(url, {}, function(response) {

		$('#state_list').html(response);
		$("#allstate option[value='"+VenueState+"']").attr("selected","selected").change();
		$(".chosen-select").chosen();
		var state = (VenueState == '')?$("#allstate option").eq(1).val():VenueState;
		change_state(state);
	});
}
function change_state(state){
	country = $('#country').val();
	url = SITEURL+'resources/getCityList/'+country+'/'+state;
	$.get(url, {}, function(response) {
		$('#city_list').html(response);
		$("#cityList option[value='"+VenueCity+"']").attr("selected","selected");
		$(".chosen-select").chosen();
	});

	
}*/
jQuery(function($){
	$('input[name = "txtPhone"]').mask("999-999-9999");
});
$("#locality").focus(function(){
	  //$("#locality").val('');
	});

$(function () {	
    $("#locality").on('keyup keydown keypress', function (event) {
    	if(event.keyCode != 13 && event.keyCode != 9 && event.keyCode != 16)
    {  
    		$("#administrative_area_level_1,#country").val('');
    }
        if (event.keyCode == 13) {
          return false;
    }
    });
})

//code for not allow the dot in fileds. only allows int value 
$(".onlydecimal").on("keypress keyup blur",function (e) {    
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			return false;
		}
	
 });