$( document ).ready(function() {
	
	$('.raty').raty({
		halfShow : true,
	  score: function() {
	    return $(this).attr('data-score');
	  },
	  readOnly: function() {
	    return 'true becomes readOnly' == 'true becomes readOnly';
	  }
});
  $('.attandance').on('change',function(){
			status = $(this).val();
			rankId = $(this).attr('id-rank');
			if (status==2){
				$('#'+rankId).removeAttr("readonly");
				$('.raty#'+rankId).raty({
				  score: function() {
				    return $(this).attr('data-score');
				  },
				  click: function(score, evt) {
					  var rankIdToPlaceValue = $(this).attr('id');
					  //alert(rankIdToPlaceValue);
					  $('#hidden-'+rankIdToPlaceValue).val(score);  
				  }
				});
			} else {
				$('#'+rankId).attr("readonly",true);
				$('.raty#'+rankId).raty({
				  score: function() {
				    return $(this).attr('data-score');
				  },
				  click: function(score, evt) {
				  },
				  readOnly: function() {
				    return 'true becomes readOnly' == 'true becomes readOnly';
				  }
				});
			}

			
	  });
	
	  $('.updateAllRakingBtn').click(function (){

		  if (!confirm('Are you sure you want to save attendance/ranking?')){
				 return false;
			 } else {
				 var img = '<img src="'+EXTERNAL_PATH+'images/loading.gif"  style="width: 16px;"/> Please Wait...';
			 }
			 $('#updateAllRakingBtnSpan').html(img);
			 $.ajaxSetup({async: false});
			  url = SITEURL+CONTROLLER_NAME+'/rankPerformer';
			  $('form').each(function (){
				data = $(this).serializeArray();
				//console.log(data);
				$.post(url,data, function(){
					
				}).fail(function(newData) {
				    if(newData.status == '401'){
				    	window.location.reload();
				        return true;
				    }
				});
			  });
			  $.ajaxSetup({async: true});
		        	   
			  location.reload();
		  });

});

$("select[name = 'attandance']").change(function(){
	var getId = $(this).attr('id-rank');
	var splited = getId.split('-');
	var id = 'submitButton-'+splited[1];
	if($(this).val() != ''){
		$('#'+id).removeAttr('disabled');
	}else{
		$('#'+id).attr('disabled','disabled');
	}
	});
