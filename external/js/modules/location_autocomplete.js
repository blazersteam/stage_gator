  // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete, autocomplete1;
      var componentForm = {
        
    	locality: 'long_name',
        administrative_area_level_1: 'long_name',
        country: 'long_name'
       };
      var componentForm1 = {       
    		  administrative_area_level_1: 'long_name',
    		  country: 'long_name'
    		 };
      function initAutocomplete() {
    	  
        // Create the autocomplete object, restricting the search to geographical
        // location types.
    	  var options = {
    			  types: ['(cities)']
    			};
    	  if(document.getElementById('locality')){
    	  autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('locality')),
            options
        );
    	  autocomplete.addListener('place_changed', fillInAddress);
    	  }
    	  if(document.getElementById('locality_1'))
    		  {
    	  autocomplete1 = new google.maps.places.Autocomplete(
    	            /** @type {!HTMLInputElement} */(document.getElementById('locality_1')),
    	            options
    	        );
    	  autocomplete1.addListener('place_changed', fillInAddress1);
    		  }
        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        
        
      }
      
      
      function fillInAddress() {    	
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }
        
        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
            
          }
        }
        
      }
      
      function fillInAddress1() {
    	  
          // Get the place details from the autocomplete object.
          var place = autocomplete1.getPlace();
          for (var component in componentForm1) {
            document.getElementById(component + "_1").value = '';
            document.getElementById(component + "_1").disabled = false;
          }
          
          // Get each component of the address from the place details
          // and fill the corresponding field on the form.
          
          if(typeof place.address_components === 'undefined'){
        	  	$("#err-enter-proper-location").show();
        	  	
        	  }else{
        		  
        		$("#err-enter-proper-location").hide();
        		
		          for (var i = 0; i < place.address_components.length; i++) {
		            var addressType = place.address_components[i].types[0];
		            if (componentForm1[addressType]) {
		              var val = place.address_components[i][componentForm1[addressType]];
		              console.log(addressType + "_1");
		              document.getElementById(addressType + "_1").value = val;
		              
		              
		            }
		          }
		          //submit the form that is in footer.php file
		          $('#btnSubmitCity').click();
        	  }
         
        }
      

