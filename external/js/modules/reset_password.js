$(document).ready(function(){
        $("#pwdform").validate({
			rules:{
				
				txtPassword:{
					required: true,
					minlength:6
				},
				txtConfirm_password:{
					required:true,
					minlength:6,
					equalTo:"#password"
				}
			},
			errorClass: "help-inline text-danger",
			errorElement: "span",
			highlight:function(element, errorClass, validClass) {
				$(element).parents('.form-group').addClass('has-error');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).parents('.form-group').removeClass('has-error');
				$(element).parents('.form-group').addClass('has-success');
			}
		});
        
        $("input[type='password']").on({
        	  keydown: function(e) {
        	    if (e.which === 32)
        	      return false;
        	  },
        	  change: function() {
        	    this.value = this.value.replace(/\s/g, "");
        	  }
        	});

});