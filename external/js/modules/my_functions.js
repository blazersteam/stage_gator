$(document).on("click", ".toggleBiteUnbite", function() {
	biteUnbite = $(this).text();
	reverseBiteUnbite = $(this).text() == "Bite" ? "Unbite" : "Bite";
	thisitm = this;
	$.ajax({
		url : SITEURL + "bite/biteUnbite/" + $(this).attr("data-following-id") + "/" + biteUnbite,
		method: "GET",
	}).done(function(responseText) {
		result = $.parseJSON(responseText);
		if(result.status === true)
		{
			$(".myMessage").removeClass("alert-danger").addClass('alert-success').text(result.message).show();
			
			var userid = $(thisitm).attr('data-userid');
			var stripAccountExit = $(thisitm).attr('data-account-exit');
			if($(thisitm).text() == 'Bite' && typeof userid !== "undefined" && stripAccountExit != ""){
		        bootbox.confirm("You want to give tip to this performer?", function(result) {
		        	//	console.log(userid);
		        	if(result === true ){
		        		$("#tipper_popup_"+userid).trigger('click');
		        	}
		        });
			}
			
		} else {
			$(".myMessage").addClass("alert-danger").removeClass('alert-success').text(result.message).show();
		}
		$(thisitm).text(reverseBiteUnbite);
		if($(thisitm).hasClass("btn-success"))
		{
			$(thisitm).removeClass("btn-success").addClass("btn-warning");
		} else {
			$(thisitm).removeClass("btn-warning").addClass("btn-success");
		}
	}).fail(function(newData) {
	    if(newData.status == '401'){
	    	window.location.reload();
	        return true;
	   }
	    });
});

//For not allow the forward slash
$(document).on("keydown", "input,textarea", function(e){
    if(!e.shiftKey && e.keyCode == 220) {
        e.preventDefault();
        return false;
    }
});

//For not allow the forward slash
$(document).on("blur", "input,textarea", function(e){
    $(this).val($(this).val().replace("/\\/g",""));
});


function checkTimezone()
{
	createCookie("ci-timezone", new Date().getTimezoneOffset(), 365);
}

//Cookies
function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";               

    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}
$(document).ready(function(){
	window.setInterval(function(){ checkTimezone(); }, 500 );
});

function blockUiDisplay()
{
	$.blockUI({ css: { 
         border: 'none', 
         padding: '15px', 
         backgroundColor: '#000', 
         '-webkit-border-radius': '10px', 
         '-moz-border-radius': '10px', 
         opacity: .5, 
         color: '#fff',
         width: '36%',
         baseZ: 1000, 
     }, message : 'Please wait...' });
}