function confirm_delete_events(){
	if (!Checkbox("frm", "chkstatus[]")){
		alert("Please select an Event to delete!");
		return false;	
	}
	return confirm('This will delete the event(s) permanently. Are you sure?');
}
function Checkbox(TheForm, Field){
	var obj = document.forms[TheForm].elements[Field];
	var res = false;
	if(obj.length > 0){
		for(var i=0; i < obj.length; i++){
			if(obj[i].checked == true){
				res = true;
			}
		}
	}
	else{
		if(obj.checked == true){
				res = true;
		}
	}
	return (res);
}

function setAll(){
	if(frm.chkAll.checked == true){
		checkAll("frm", "chkstatus[]");
	}
	else{
		clearAll("frm", "chkstatus[]");
	}
}	

function checkAll(TheForm, Field){
	var obj = document.forms[TheForm].elements[Field];
	if(obj.length > 0){
		for(var i=0; i < obj.length; i++){
			obj[i].checked = true;
		}
	}
	else{
		obj.checked = true;
	}
}
function clearAll(TheForm, Field){
	var obj = document.forms[TheForm].elements[Field];
	if(obj.length > 0){
		for(var i=0; i < obj.length; i++){
			obj[i].checked = false;
		}
	}
	else{
		obj.checked = false;
	}
}

