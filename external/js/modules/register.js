$(document)
		.ready(
				function() {

					// Add US Phone Validation
					/*jQuery.validator
							.addMethod(
									'phoneUS',
									function(phone_number, element) {
										phone_number = phone_number.replace(
												/\s+/g, '');
										return this.optional(element)
												|| phone_number.length > 9
												&& phone_number
														.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
									}, 'Please enter a valid phone number.');*/

					$("#form").validate(
							{
								rules : {
									required : {
										required : true,
									// maxlength: 50
									},
									txtName : {
										required : true
									},
									txtContact_person : {
										required : true
									},
									txtEmail : {
										required : true,
										email : true
									},
									txtPassword : {
										required : true,
										minlength : 6
									},
									txtConfirmPassword : {
										required : true,
										minlength : 6,
										equalTo : "#password"
									},
								/*
								 * hiddenRecaptcha: { required: function() {
								 * if(grecaptcha.getResponse() == '') { return
								 * true; } else { return false; } } }
								 */
								},
								errorClass : "help-inline text-danger",
								errorElement : "span",
								highlight : function(element, errorClass,
										validClass) {
									$(element).parents('.form-group').addClass(
											'has-error');
								},
								unhighlight : function(element, errorClass,
										validClass) {
									$(element).parents('.form-group')
											.removeClass('has-error');
									$(element).parents('.form-group').addClass(
											'has-success');
								}
							});
				});

// bind keypress for accept no space
$('#password,#confirmPassword').bind('keypress', function(e) {
	if (e.which === 32) {
		return false;
	}
});
function verifyEmail(email) {

	if (email == '') {
		$('#response').hide();
		return;
	}
	$('#response').show();
	$('#response')
			.html(
					'<span class="glyphicon glyphicon-search"></span> Checking Email...');
	var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if (!pattern.test(email)) {
		// $('#response').html('<span class="glyphicon
		// glyphicon-remove"></span> Invalid Email Address');
		$('#response').html('');
		return;
	}
	url = SITEURL + CONTROLLER_NAME + '/checkDuplicateEmail';

	$.post(url, {
		username : email,
		_csrf : $('input[name = "_csrf"]').val()
	}, function(response) {
		if (response == 0) {
			$('#response').html('<span>Available</span>');
		} else {

			$('#response').html(
					'<span style = "color:red;">Already Registered</span> ');
		}
	});
}

function check_form() {
	var googleCaptchaResponse = grecaptcha.getResponse();
	
	if (googleCaptchaResponse.trim() == "") {
		$("#googleCaptcha").html("Please verify that you are a human").show();
		return false;
	}
	flag = false;
	if (confirm_pass()) {
		email = $('#email').val();
		$.ajaxSetup({
			async : false
		});
		url = SITEURL + CONTROLLER_NAME + '/checkDuplicateEmail';
		$.post(url, {
			username : email,
			_csrf : $('input[name = "_csrf"]').val()
		}, function(response) {
			if (response == 0) {
				$('#frmsubmit').attr('disabled', true);
				$('#response').html(
						'<span style= "color:red;">Already Registered</span>');
				flag = true;

			}
		});
		$.ajaxSetup({
			async : true
		});
	}
	return flag;
}
jQuery(function($){
	$('input[name = "txtPhone"]').mask("999-999-9999");
});
