/**
 * js used by index.php view for login or register with diffrent diffrent user
 * 
 * 
 */

/**
 * showForm() This function show HTML elements accordinly user
 * 
 * @param getAction
 *            get a user or a user action
 */

$(document).ready(
		function() {
			
			$("#myform").validate({
				rules : {

					txtEmail : {
						required : true,
						email : true,

					},
					txtPassword : {
						required : true,
						minlength : 6
					}

				},
				messages : {
					txtEmail : 'Please enter a valid email address',
					txtPassword : 'Please enter password',

				}
			/*
			 * errorClass: "help-inline text-danger", errorElement: "span",
			 * highlight:function(element, errorClass, validClass) {
			 * $(element).parents('.form-group').addClass('has-error'); },
			 * unhighlight: function(element, errorClass, validClass) {
			 * $(element).parents('.form-group').removeClass('has-error');
			 * $(element).parents('.form-group').addClass('has-success'); }
			 */
			});

		});

$("input[type='password']").on({
	keydown : function(e) {
		$("#submitbtn").removeAttr('disabled');
		if (e.which === 32)
			return false;
	},
	change : function() {
		this.value = this.value.replace(/\s/g, "");
	}
});

// this form set password value encrypted
function submit_login() {

	if ($("#myform").valid()) {
		$("#submitbtn").attr('disabled', true);
		if ($(".error").text() == '') {
			$('#password').val(CryptoJS.MD5($('#password').val()));
		}
		return true;
	} else {
		return false;
	}
}
