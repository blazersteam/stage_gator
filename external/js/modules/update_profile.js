$(document).ready(function(){
	$(".chosen-select").chosen();


	 CKEDITOR.replace( 'editor1', {
	        filebrowserBrowseUrl: EXTERNAL_PATH+'js/ckeditorbasic/ckfinder/ckfinder.html',
	        filebrowserImageBrowseUrl: EXTERNAL_PATH+'js/ckeditorbasic/ckfinder/ckfinder.html?Type=Images',
	        filebrowserFlashBrowseUrl: EXTERNAL_PATH+'js/ckeditorbasic/ckfinder/ckfinder.html?Type=Flash',
	        filebrowserUploadUrl: EXTERNAL_PATH+'js/ckeditorbasic/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
	        filebrowserImageUploadUrl: EXTERNAL_PATH+'js/ckeditorbasic/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	        filebrowserFlashUploadUrl: EXTERNAL_PATH+'js/ckeditorbasic/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
	        toolbarGroups: [
				{"name":"basicstyles","groups":["basicstyles"]},
				{"name":"links","groups":["links"]},
				{"name":"paragraph","groups":["list"]},
				{"name":"about","groups":["about"]}
			]
	    });

	 $.validator.setDefaults({ ignore: ":hidden:not(.chosen-select)" })
	// Add US Phone Validation
		/*jQuery.validator.addMethod('phoneUS', function(phone_number, element) {
		    phone_number = phone_number.replace(/\s+/g, ''); 
		    return this.optional(element) || phone_number.length > 9 &&
		        phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
		}, 'Please enter a valid phone number.');*/
     
  // method for alphabetical,numeric,underscore
 	$.validator.addMethod("alphanumericUnderscoreDash", function(value, element) {
 		return this.optional(element) || /^([a-zA-Z0-9\_\-])*$/i.test(value);
 	}, "allowed only alphanumeric, underscore, dash characters");

		
	$("#formProfileUpdate").validate({
			rules:{
				required:{
					required:true,
					//maxlength: 50						
				},
				txtName:{
					required:true
					
				},
				txtContact_person:{
					required:true
				},
				cmbStyle:{
					required: true
					
				},
				txtPhone: {
					required: true,
					/*phoneUS: true*/
				},
				rdaPhonvisiblity:{
					required: true
				},
				cmbCountry:{
					required: true	
				},
				cmbState:{
					required: true	
				},
				cmbcity: {
					required: true
					
				},
				txtAddress: {
					required: true
				},
				txtUrl:{			
					required: true,
					alphanumericUnderscoreDash: true,
					remote: {
	                    url: SITEURL+CONTROLLER_NAME+'/checkDuplicateProfileUrl',
	                    type: "get",
	                    statusCode: {
	                        401:function() { window.location.reload(); }
	                      },
	                }  		
				}
				
			},
			messages: {	        	
				txtUrl: {
	        		remote: "this url already exist, please enter another"
	            }
	        },
			errorClass: "help-inline text-danger",
			errorElement: "span",
			highlight:function(element, errorClass, validClass) {
                                 $(".ci-validations").css('display','none');
				$(element).parents('.form-group').addClass('has-error');
			},
			errorPlacement: function (error, element) {
				
				//check whether chosen plugin is initialized for the element
	            if (element.data().chosen) { //or if (element.next().hasClass('chosen-container')) {
	                element.next().after(error);
	            } else {
	                element.after(error);
	            }
			},
			unhighlight: function(element, errorClass, validClass) {
				
				$(element).parents('.form-group').removeClass('has-error');
				$(element).parents('.form-group').addClass('has-success');
			},
			  submitHandler: function(form) {
				    // do other things for a valid form

				    form.submit();

				  }

			
			
			
		});
	

});
/*function change_country(id){

	url = SITEURL+'resources/getStateList/'+id;
	$.get(url, {}, function(response) {
		$('#state_list').html(response);
		$(".chosen-select").chosen();
	});
	var state = $("#allstate option").eq(1).val();
	change_state(state);
	
}
function change_state(state){
	country = $('#country').val(); 
	url = SITEURL+'resources/getCityList/'+country+'/'+state;
	$.get(url, {}, function(response) {
		$('#city_list').html(response);
		$(".chosen-select").chosen();
	});
}*/
jQuery(function($){
	$('input[name = "txtPhone"]').mask("999-999-9999");
});
$("#locality").focus(function(){
	  //$("#locality").val('');
	});

$(function () {	
    $("#locality").on('keyup keydown keypress', function (event) {
    	if(event.keyCode != 13 && event.keyCode != 9 && event.keyCode != 16)
    {  
    		$("#administrative_area_level_1,#country").val('');
    }
        if (event.keyCode == 13) {
          return false;
    }
    });
})