function pickTimeChange(state) {
	if (state == 0) {
		// $('#schedule').slideDown();
		$('.scheduleAlgorithm').prop('disabled', false);
	} else {
		// $('#schedule').slideUp();
		$('.scheduleAlgorithm').prop('disabled', true);
	}
}
$(document)
		.ready(
				function() {
					$(".mainRepeatingDiv").find('select')
							.prop('disabled', true);
					$(".chosen-select").chosen("destroy");
					$(".chosen-select").chosen();
					$("select").change();
					$(".tooltips").tooltip();
					$("input[name='rdaAllowWalkin']:checked").change();

					CKEDITOR
							.replace(
									'txtDescr',
									{
										filebrowserBrowseUrl : EXTERNAL_PATH
												+ 'js/ckeditorbasic/ckfinder/ckfinder.html',
										filebrowserImageBrowseUrl : EXTERNAL_PATH
												+ 'js/ckeditorbasic/ckfinder/ckfinder.html?Type=Images',
										filebrowserFlashBrowseUrl : EXTERNAL_PATH
												+ 'js/ckeditorbasic/ckfinder/ckfinder.html?Type=Flash',
										filebrowserUploadUrl : EXTERNAL_PATH
												+ 'js/ckeditorbasic/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
										filebrowserImageUploadUrl : EXTERNAL_PATH
												+ 'js/ckeditorbasic/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
										filebrowserFlashUploadUrl : EXTERNAL_PATH
												+ 'js/ckeditorbasic/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
										toolbarGroups: [
													{"name":"basicstyles","groups":["basicstyles"]},
													{"name":"paragraph","groups":["list","indent"]},
													{"name":"links","groups":["links"]}
												]
									});

					var ableToSubmitEventForm = 1;
					/*
					 * $("#event_form").on("submit", function(e) {
					 * e.preventDefault(); var style_ok = false; var location_ok =
					 * false; $.validator.setDefaults({ ignore:
					 * ":hidden:not(.chosen-select)" })
					 * 
					 * if ($('#cmbStyle').val()<=0){
					 * $("#eventStyleErrorLable").text('Please select style!');
					 * $('html,body').animate({ },'fast'); ableToSubmitEventForm =
					 * 0; } else { $("#eventStyleErrorLable").text('');
					 * ableToSubmitEventForm = 1; style_ok = true; }
					 * console.log($('#cmbLocation_id').val()); if
					 * ($('#cmbLocation_id').val()==''){
					 * $("#eventLocationDropDownLabel").text('Please select show
					 * location!'); $('html,body').animate({ },'fast');
					 * ableToSubmitEventForm = 0; } else {
					 * $("#eventLocationDropDownLabel").text('');
					 * ableToSubmitEventForm = 1; location_ok = true; }
					 * 
					 * if (location_ok && style_ok){ return true; } else {
					 * return false; } });
					 */

					jQuery.validator.addMethod("greaterStart", function(value,
							element, params) {
						return this.optional(element)
								|| value > $(params).val();
					}, 'Must be greater than start date.');

					/*jQuery.validator.addMethod("needsSelection", function (value, element) {
						console.log(element);
				        var count = $(element).find('option:selected').length;
				        return false;
				    },'select value.');*/
					
					jQuery.validator.addMethod("lessthen24",
							function(value, element) {

								var txtLockBefore = $(
										"input[name='txtLockBefore']").val();

								if ($("input[name='rdaAllowWalkin']:checked")
										.val() == "1") {
									return true;
								} else {
									if (parseInt(txtLockBefore) <= 24) {
										return true;
									} else {
										return false;
									}
								}

							}, 'Cut-off Time must be less than 24.');

					$.validator.setDefaults({
						ignore : ":hidden:not(.chosen-select)"
						
					});
					$("#event_form")
							.validate(
									{
										rules : {
											required : {
												required : true,
											// maxlength: 50
											},
											txtName : {
												required : true,
											},
											cmbLocation_id : {
												required : true,
											},
											rdaType : {
												required : true,
											},
											cmbMonthDay : {
												required : {
													depends : function(element) {
														return $("#cmbIsRecuring").val() == "4" ;
													}
												}												
											},
											chkDay: {
										        required: true,
										    },
											cmbStyle : {
												required : true,
											},
											cmbIsRecuring : {
												required : true,
											},
											timePerPerformance : {
												required : true,
												number : true,
												maxlength : 10,
												minStrictMinutes : 1
											},

											cmbWeekInMonth : {
												required : true,
											},
											lockBefore : {
												required : true,
												number : true,
												maxlength : 2,
												minStrictPositive : -1
											},
											end_time : {
												required : true,
												notEqualTo : '#start_time'
											},
											rdaCreateType : {
												required : true,
											},
											rdaType : {
												required : true,
											},
											txtStartDate : {
												required : true
											},
											txtEndDate : {
												required : true,
												greaterStart : "#txtStartDate"
											},

											cmbRecurringDay : {
												required : true,
											},
											cmbRecurringDayInWeek : {
												required : true,
											},
											txtStart_time : {
												required : true,
											},
											txtStart_time : {
												required : true,
											},
											txtEnd_time : {
												required : true,
											},
											txtLockBefore : {
												required : {
													depends : function(element) {
														return $(
																"input[name='rdaAllowWalkin']:checked")
																.val() == "1" ? false
																: true;
													}
												},
												lessthen24 : "txtLockBefore"
											},

											txtDescr : {
												required : true,
											}
										},

										errorClass : "help-inline text-danger",
										errorElement : "span",
										highlight : function(element,
												errorClass, validClass) {
											$(element).parents('.form-group')
													.addClass('has-error');
										},
										errorPlacement: function (error, element) {
											//alert(error.text());
								            //check whether chosen plugin is initialized for the element
								            if (element.data().chosen) { 
								                element.next().after(error);
								            } else {
								                element.after(error);
								            }
										},
										unhighlight : function(element,
												errorClass, validClass) {
											$(element).parents('.form-group')
													.removeClass('has-error');
											$(element).parents('.form-group')
													.addClass('has-success');
										}

									});
				});

$("#event_form").submit(function() {
	if ($("select[name='cmbWeekInMonth[]']").attr('disabled') != 'disabled') {
		var selectName = $("select[name='cmbWeekInMonth[]']");
		var totallength = selectName.length;
		for (i = 0; i < totallength; i++) {
			var newt = selectName.eq(i).val();
			if (newt == null) {
				$(this).parents('.form-group').addClass('has-error');
				$("#eventDays").text('Please select a day');
				// alert('please select a day');
				return false;
			}
		}
	}
	return true;
});
/*
 * $("#dateEnd").datepicker({startDate:new Date(),format: "yyyy-mm-dd"});
 * 
 * $(function() { $('#datepicker2,#datepicker3').datetimepicker({ format: 'HH:mm
 * PP', pick12HourFormat: true, pickDate: false, pickSeconds: false, language:
 * 'en' }); });
 */
$('.lockEvent').click(function() {
	if ($(this).val() == 1) {
		$('.trading').prop("disabled", true);
	} else {
		$('.trading').prop("disabled", false);
	}
});

$('.allowWalkin').click(function() {
	if ($(this).val() == 1) {
		$('#lockBefore').prop("readonly", true);
	} else {
		$('#lockBefore').prop("readonly", false);
	}
});
$("#cmbIsRecuring").on(
		"change",
		function() {
			$("#advanceEvent,#advanceEventforappend").hide()
			if ($("#cmbIsRecuring").val() == 1) {
				$('#selectMonth,#weekdays,#endDateDiv').hide();
				$('#selectMonth,#weekdays').find("input,select").prop(
						"disabled", true);
			}
			if ($("#cmbIsRecuring").val() == 2) {
				$("#endDateDiv").show();
				$('#selectMonth,#weekdays').hide();
				$('#selectMonth,#weekdays').find("input,select").prop(
						"disabled", true);
			}
			if ($("#cmbIsRecuring").val() == 3) {	
				
				$("#weekdays").show();
				$("#endDateDiv").show();
				$("#selectMonth").hide();				
				$('#weekdays').find("input,select").prop("disabled", false);
				//$('#selectMonth').find("input,select").prop("disabled", true);
			}
			if ($("#cmbIsRecuring").val() == 4) {
				$("#weekdays").hide();
				$("#selectMonth,#endDateDiv").show();
				$('#selectMonth').find("input,select").prop("disabled", false);
				$('#weekdays').find("input,select").prop("disabled", true);
			}

		});

var rowCount = 1;
var occurrings = false;
var daysList = false;
$(document).on("click", '.advanceSelect', function(e) {
	e.preventDefault();
	var getID = $(this).attr('id');
	if (getID == "byweek") {
		$("#eventStyleDropDown1").prop('disabled', true);
		$(".mainRepeatingDiv").find('select').prop('disabled', false).change();
		$("select[name = 'cmbWeekInMonth']").prop('disabled', false);
		$(".chosen-select").chosen("destroy");
		$(".chosen-select").chosen();
		$("#advanceEvent").show();
		$("#selectMonth").hide();
		$("#advanceRecurring").show();
	} else {
		$("#eventStyleDropDown1").prop('disabled', false);
		$("select[name = 'cmbWeekInMonth']").prop('disabled', true);
		$(".mainRepeatingDiv").find('select').prop('disabled', true);
		$(".chosen-select").chosen("destroy");
		$(".chosen-select").chosen();
		$("#selectMonth").show();
		$("#advanceRecurring").hide();
	}

});


$('#txtEndDate').datetimepicker({
	ignoreReadonly : true,
	minDate : 'now',
	useCurrent : false,
	format : 'YYYY-MM-DD'
});

//$('#txtStart_time,#txtEnd_time').datetimepicker({
//	ignoreReadonly : true,
//	useCurrent : false,
//	format : 'hh:mm A'
//
//});

$('.txtStart_time, .txtEnd_time').clockpicker({ 
	twelvehour: true, 
	donetext: 'Done' 					
});

$(document).on(
		"change",
		"input[name='rdaAllowWalkin']",
		function() {

			if ($(this).val() == 0) {
				$("input[name='txtLockBefore']").prop("readonly", false)
						.change();
			} else {
				$("input[name='txtLockBefore']").prop("readonly", true).val("")
						.change();
			}

		});

//code for not allow the dot in fileds. only allows int value 
$(".onlydecimal").on("keypress keyup blur",function (e) {    
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			return false;
		}
	
 });