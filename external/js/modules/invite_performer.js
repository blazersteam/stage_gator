$(document).ready(function() {
	$(".chosen-select").chosen();

	$.validator.setDefaults({
		ignore : ":hidden:not(.chosen-select)"
	})
	$("#invite_form").validate({
		rules : {
			required : {
				required : true,
			// maxlength: 50
			},
			idEvent : {
				required : true
			}

		},
		errorClass : "help-inline text-danger",
		errorElement : "span",
		highlight : function(element, errorClass, validClass) {
			$(".ci-validations").css('display', 'none');
			$(element).parents('.form-group').addClass('has-error');
		},
		unhighlight : function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-error');
			$(element).parents('.form-group').addClass('has-success');
		}

	});

});
