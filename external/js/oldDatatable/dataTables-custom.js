var dataTableInstance = '';
$('document').ready(function(){
	//data table
	$('#example').dataTable({
		"sPaginationType": "bootstrap",
		"bPaginate": false,
		"bInfo":false
	});
	
	$('#event_search').dataTable();
	
	$('#my_ratings').dataTable();
	
	dataTableInstance = $('.dataTable').dataTable({
		"sPaginationType": "bootstrap",
		 "dom": '<"top"lfp<"clear">>rt<"bottom"ip<"clear">>'
	});
	
});