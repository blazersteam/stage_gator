// Author: Vijay Kumar
// Template: Cascade - Flat & Responsive Bootstrap Admin Template
// Version: 1.0
// Bootstrap version: 3.0.0
// Copyright 2013 bootstrapguru
// www: http://bootstrapguru.com
// mail: support@bootstrapguru.com
// You can find our other themes on: https://bootstrapguru.com/themes/



// jQuery $('document').ready(); function 
$('document').ready(function(){

	$('.nav-input-search').focus();

  	// Sidebar dropdown
	$('ul.nav-list').accordion();
 	
	$('.settings-toggle').click(function(e){
		e.preventDefault();
		$('.right-sidebar').toggleClass('right-sidebar-hidden');
	});

	$('.left-sidebar .nav > li > ul > li.active').parent().css('display','block');
	
	$('.left-sidebar .nav > li a span').hover(function(){
		var icon=$(this).parent().find('i');
		icon.removeClass('animated shake').addClass('animated shake');
		var wait = window.setTimeout( function(){
			icon.removeClass('animated shake');
			
		},
			1300
		);
	});
	//$(".site-holder").niceScroll({cursorcolor:"#54728c"});  // The document page (html)
	//$(".left-sidebar").niceScroll({cursorcolor:"#54728c"});  // The document page (html)

	 //$(".left-sidebar").niceScroll({cursorcolor:"#54728c"});  // The document page (html)
	// $(".right-sidebar-holder").niceScroll({cursorcolor:"#54728c"});  // The document page (html)


	$('.btn-nav-toggle-responsive').click(function(){
		$('.left-sidebar').toggleClass('show-fullsidebar');
	});

	$('li.nav-toggle > button').click(function(e){
		e.preventDefault();
		$('.hidden-minibar').toggleClass("hide");
		$('.site-holder').toggleClass("mini-sidebar");
		if($('.toggle-left').hasClass('fa-angle-double-left')){ $('.toggle-left').removeClass('fa-angle-double-left').addClass('fa-angle-double-right'); }
		else { $('.toggle-left').removeClass('fa-angle-double-right').addClass('fa-angle-double-left'); }
	

		if($('.site-holder').hasClass('mini-sidebar'))
		{    
			$('.sidebar-holder').tooltip({
		      selector: "a",
		      container: "body",
		      placement: "right"
		    });
		    $('li.submenu ul').tooltip('destroy');
		 }
		 else
		 {
			$('.sidebar-holder').tooltip('destroy');
		 }
		

	});

		if($('.site-holder').hasClass('mini-sidebar'))
		{    
			$('.sidebar-holder').tooltip({
		      selector: "a",
		      container: "body",
		      placement: "right"
		    });
		    $('li.submenu').tooltip('destroy');
		 }
		 else
		 {
			$('.sidebar-holder').tooltip('destroy');
		 }

	$('.show-info').click(function(){
		$('.page-information').toggleClass('hidden');
	});


	// PANELS

	// panel close
	$('.panel-close').click(function(e){
		e.preventDefault();
		$(this).parent().parent().parent().parent().fadeOut();
	});

	$('.panel-minimize').click(function(e){
		e.preventDefault();
		var $target = $(this).parent().parent().parent().next('.panel-body');
		if($target.is(':visible')) { $('i',$(this)).removeClass('fa-chevron-up').addClass('fa-chevron-down'); }
		else { $('i',$(this)).removeClass('fa-chevron-down').addClass('fa-chevron-up'); }
		$target.slideToggle();
	});
	
	$('.panel-settings').click(function(e){
		e.preventDefault();
		$('#myModal').modal('show');
	});

	$('.fa-hover').click(function(e){
		e.preventDefault();
		var valued= $(this).find('i').attr('class');
		$('.modal-title').html(valued);
		$('.icon-show').html('<i class="' + valued + ' fa-5x "></i>&nbsp;&nbsp;<i class="' + valued + ' fa-4x "></i>&nbsp;&nbsp;<i class="' + valued + ' fa-3x "></i>&nbsp;&nbsp;<i class="' + valued + ' fa-2x "></i>&nbsp;&nbsp;<i class="' + valued + ' "></i>&nbsp;&nbsp;');
		$('.modal-footer span.icon-code').html('"' + valued + '"');
		$('#myModal').modal('show');
	});
});
